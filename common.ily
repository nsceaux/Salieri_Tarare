\header {
  copyrightYear = "2017"
  composer = "Antonio Salieri"
  poet = "Pierre-Augustin Caron de Beaumarchais"
}

%% LilyPond options:
%%  urtext  if true, then print urtext score
%%  part    if a symbol, then print the separate part score
#(ly:set-option 'original-layout (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
#(ly:set-option 'use-ancient-clef (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'print-footnotes (not (symbol? (ly:get-option 'part))))

%% Use rehearsal numbers
#(ly:set-option 'use-rehearsal-numbers #t)

%% Staff size
#(set-global-staff-size
  (cond ((eqv? #t (ly:get-option 'urtext)) 14)
        ((not (symbol? (ly:get-option 'part))) 16)
        (else 18)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking
     (cond ((eqv? (ly:get-option 'part) 'reduction) ly:optimal-breaking)
           ((symbol? (ly:get-option 'part)) ly:page-turn-breaking)
           (else ly:optimal-breaking)))
}

\layout {
  reference-incipit-width = #(* 1/2 mm)
}

\language "italiano"
\include "nenuvar-lib.ily"
\setPath "ly"
\opusPartSpecs
#`((trombe "Trompettes" () (#:notes "trombe"))
   (corni "Cors" () (#:notes "corni"))
   (tromboni "Trombones" () (#:notes "tromboni" #:clef "alto"))
   (timpani "Timbales" () (#:notes "timpani" #:clef "bass"))
   
   (flauti "Flûtes" () (#:notes "flauti"))
   (oboi "Hautbois" () (#:notes "oboi"))
   (clarinetti "Clarinettes" () (#:notes "clarinetti"))
   (fagotti "Bassons" ((basso "Basses")) (#:clef "bass" #:notes "fagotti"))

   (violino1 "Violon I" ()  (#:notes "violini" #:tag-notes violino1))
   (violino2 "Violon II" () (#:notes "violini" #:tag-notes violino2))
   (viola "Altos" () (#:notes "viola" #:clef "alto"))
   (basso "Basses" () (#:notes "basso" #:clef "bass"))
   (reduction "Réduction" () (#:score "score-reduction")))

\opusTitle "Tarare"

\paper {
  % de la place en bas de page pour les annotations
  last-bottom-spacing.padding = #3
}

\layout {
  indent = #(if (symbol? (ly:get-option 'part))
                smallindent
                largeindent)
  short-indent = #(if (symbol? (ly:get-option 'part))
                      0
                      (* 8 mm))
  ragged-last = ##f
}

\layout {
  \context {
    \Voice
    \override Script.avoid-slur = #'inside
    \override DynamicTextSpanner.style = #'none
  }
  \context {
    \DrumVoice
    \override DynamicTextSpanner.style = #'none
  }
}

\header {
  maintainer = \markup {
    Nicolas Sceaux,
    \with-url #"http://www.lestalenslyriques.com" \line {
      Les Talens Lyriques – Christophe Rousset.
    }
  }
  license = "Creative Commons Attribution-ShareAlike 4.0 License"
  tagline = \markup\sans\abs-fontsize #8
  \override #'(baseline-skip . 0) \vcenter {
    \right-column\bold {
      \with-url #"http://nicolas.sceaux.free.fr" {
        \concat { Éditions \tagline-vspacer }
        \concat { Nicolas \tagline-vspacer }
        \concat { Sceaux \tagline-vspacer }
      }
    }
    \abs-fontsize #9 \with-color #(x11-color 'grey40) \raise #-0.7 \musicglyph #"clefs.petrucci.f"
    \column {
      \line { \tagline-vspacer \copyright }
      \smaller\line {
        \tagline-vspacer
        Sheet music from
        \with-url #"http://nicolas.sceaux.free.fr"
        http://nicolas.sceaux.free.fr
        typeset using \with-url #"http://lilypond.org" LilyPond
        on \concat { \today . }
      }
      \smaller\line {
        \tagline-vspacer \license
        — free to download, distribute, modify and perform.
      }
    }
  }

}

corniInstr = \with {
  instrumentName = "Cors"
  shortInstrumentName = "Cor."
}
trombeInstr = \with {
  instrumentName = "Trompettes"
  shortInstrumentName = "Tr."
}
tromboniInstr = \with {
  instrumentName = "Trombones"
  shortInstrumentName = "Trb."
}
flautoInstr = \with {
  instrumentName = "Flûte"
  shortInstrumentName = "Fl."
}
flautiInstr = \with {
  instrumentName = "Flûtes"
  shortInstrumentName = "Fl."
}
oboiInstr = \with {
  instrumentName = "Hautbois"
  shortInstrumentName = "Htb."
}
clarinettiInstr = \with {
  instrumentName = "Clarinettes"
  shortInstrumentName = "Cl."
}
fagottiInstr = \with {
  instrumentName = "Bassons"
  shortInstrumentName = "Bn."
}

violiniInstr = \with {
  instrumentName = "Violons"
  shortInstrumentName = "Vn."
}
altoInstr = \with {
  instrumentName = "Altos"
  shortInstrumentName = "Alt."
}
violaInstr = \with {
  instrumentName = "Altos"
  shortInstrumentName = "Alt"
}
bassoInstr = \with {
  instrumentName = "Basses"
  shortInstrumentName = "B."
}
violoncelliInstr = \with {
  instrumentName = "Violoncelles"
  shortInstrumentName = "Vc."
}
cbInstr = \with {
  instrumentName = "Contrebasse"
  shortInstrumentName = "Cb."
}
bcbInstr = \with {
  instrumentName = \markup\center-column {
    Basses Contrabasse
  }
  shortInstrumentName = \markup\center-column { B. Cb. }
}
vccbInstr = \with {
  instrumentName = \markup\center-column {
    Violoncelles Contrabasse
  }
  shortInstrumentName = \markup\center-column { Vc. Cb. }
}
timpaniInstr = \with {
  instrumentName = "Timbales"
  shortInstrumentName = "Timb."
}

choeurInstr = \with {
  instrumentName = \markup\smallCaps Chœur
  shortInstrumentName = \markup\smallCaps Ch.
}

natureInstr = \with {
  instrumentName = \markup\smallCaps La Nature
  shortInstrumentName = \markup\smallCaps Nat.
}
genieInstr = \with {
  instrumentName = \markup\smallCaps Le Génie du Feu
  shortInstrumentName = \markup\smallCaps Gén.
}

trill = #(make-articulation "trill")

#(define-markup-command (tacet layout props) ()
   (interpret-markup layout props #{ \markup {
  \hspace#2 \pad-to-box #'(0 . 0) #'(-2 . 1) \small TACET }
                                             #}))

%% Cues
cueTreble =
#(define-music-function (parser location cue-name music) (string? ly:music?)
   #{ \cueDuringWithClef $cue-name #CENTER "treble" $music #})

cue =
#(define-music-function (parser location cue-name music) (string? ly:music?)
   #{ \cueDuring $cue-name #CENTER $music #})

cueTransp =
#(define-music-function (parser location cue-name pitch music)
     (string? ly:pitch? ly:music?)
   #{ \transposedCueDuring $cue-name #CENTER $pitch $music #})


quoteViolinoI =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'violino1 { \includeNotes "violini" }
#}))

quoteViolinoII =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'violino2 { \includeNotes "violini" }
#}))

quoteBasso =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'basso { \includeNotes "basso" }
#}))

