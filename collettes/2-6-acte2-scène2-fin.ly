\version "2.19.80"
\include "../common.ily"

\paper { ragged-last-bottom = ##t }
\header { tagline = ##f }
\layout { indent = 0 short-indent = 0 ragged-last = ##t }

decouper = \markup\column {
  \override #'(fill-with-dots . #t) \line { }
  \vspace#1
}

global = {
  \set Score.currentBarNumber = 17 \bar ""
  \key do \major \time 4/4 s2. \tempo "poco lento" s4 s1*2 \bar "|."
}

violonI = {
  \clef "treble" r2 r4 <sol' sol>4\f |
  <mi'! dod'>2 q |
  re'8 sold4 si! re'4 re'8 |
}

violonII = {
  \clef "treble" r2 r4 <sol' sol>4\f |
  <mi'! dod'>2 q |
  re'8 sold4 si! re'4 sold8 |
}

viola = {
  \clef "alto" r2 r4 sol\f |
  la!-.( la-. la-. la-.) |
  sold1 |
}

voix = {
  \clef "bass" si!4 si8 do' sol sol r4 |
  R1*2 |
}

paroles = \lyricmode {
  la ven -- gean -- ce.
}

basso = {
  \clef "bass" r2 r4 sol,\f |
  la,!-.( la,-. la,-. la,-.) |
  si,!1 |
}

%%%%%%

\pieceTocNb "2-6" \markup\null
\markup\vspace#1

\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff <<
        \new Staff << \global \violonI >>
        \new Staff << \global \violonII >>
      >>
      \new Staff << \global \viola >>
    >>
    \new Staff \withLyrics <<
      \global \voix
    >> \paroles
    \new Staff << \global \basso >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff << \global \violonI >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff << \global \violonII >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff << \global \viola >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff << \global \basso >>
  >>
  \layout { }
}
