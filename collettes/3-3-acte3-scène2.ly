\version "2.19.80"
\include "../common.ily"

\paper { ragged-last-bottom = ##t }
\header { tagline = ##f }
\layout { indent = 10\mm short-indent = 0 ragged-last = ##t }

decouper = \markup\column {
  \override #'(fill-with-dots . #t) \line { }
  \vspace#1
}

global = {
  \override Score.BarNumber.break-visibility = #all-visible
  \set Score.currentBarNumber = #26 \bar ""
  \tag #'all \key mib \major \override Staff.TimeSignature.stencil = ##f \time 4/4 s1*2
  \set Score.currentBarNumber = #37 \stopStaff s4
}

htbI = {
  \clef "treble" sib''2 reb''' |
  do'''4 r r2 |
}
htbII = {
  \clef "treble" sol''2 sib'' |
  lab''4 r r2 |
}

cors = {
  \clef "treble" << sol''4 \\ sol' >> r r2 | R1
}

fagotti = {
  \clef "bass" <<
    { sib2\repeatTie reb' | do'4 } \\
    { sol2 sib | lab4 }
  >> r4 r2
}

violonI = {
  \clef "treble" sib''8. sib''16 sib''8. sib''16 reb'''8. reb'''16 reb'''8. reb'''16 |
  do'''4 r r2 |
}

violonII = {
  \clef "treble" sib''8. sol''16 sol''8. sol''16 sib''8. sib''16 sib''8. sib''16 |
  lab''4 r r2 |
}

viola = {
  \clef "alto" <sib sol'>8. sol'16 sol'8. sol'16 mi'8. mi'16 mi'8. mi'16 |
  fa'4 r r2 |
}

voix = {
  \clef "bass" R1 |
  r8 fa fa sol lab4 lab8 lab |
}

paroles = \lyricmode {
  d’un œil ar -- dant me -- su -
}

basso = {
  \clef "bass" mib8. mib16 mib8. mib16 mi8. mi16 mi8. mi16 |
  fa4 r r2 |
}

%%%%%%

\pieceTocNb "3-3" \markup\null
\markup\vspace#1

\score {
  <<
    \new StaffGroup <<
      \new Staff \with { instrumentName = "Htb." } << \global \partcombine \htbI \htbII >>
      \new Staff \with {
        instrumentName = \markup\center-column { Tr. Cor. }
      } << \keepWithTag #'() \global \cors >>
      \new Staff \with { instrumentName = "Bn." } << \global \fagotti >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { instrumentName = "Vn." } <<
        \new Staff << \global \violonI >>
        \new Staff << \global \violonII >>
      >>
      \new Staff \with { instrumentName = "Alt." } << \global \viola >>
    >>
    \new Staff \with { instrumentName = "Ur." } \withLyrics <<
      \global \voix
    >> \paroles
    \new Staff \with { instrumentName = "B." } << \global \basso >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new GrandStaff \with { instrumentName = "Htb." } <<
      \new Staff << \global \htbI >>
      \new Staff << \global \htbII >>
    >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with {
      instrumentName = \markup\center-column { Tr. Cor. }
    } << \keepWithTag #'() \global \cors >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with { instrumentName = "Bn." } << \global \fagotti >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with { instrumentName = "Vn1." } << \global \violonI >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with { instrumentName = "Vn2." } << \global \violonII >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with { instrumentName = "Alt." } << \global \viola >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with { instrumentName = "B." } << \global \basso >>
  >>
  \layout { }
}
