\version "2.19.80"
\include "../common.ily"

\paper { ragged-last-bottom = ##t }
\header { tagline = ##f }
\layout { indent = 0 short-indent = 0 ragged-last = ##t }

decouper = \markup\column {
  \override #'(fill-with-dots . #t) \line { }
  \vspace#1
}

global = {
  \override Score.BarNumber.break-visibility = #all-visible
  \key do \major \time 4/4
  \set Score.currentBarNumber = 1 s1*3
  \set Score.currentBarNumber = 23
}

violonI = {
  \clef "treble" si'!1 |
  si'2:16\cresc si':16 |
  si'4-! r4 r <si sold'>4\f |
}

violonII = {
  \clef "treble" sold'1 |
  sold'2:16\cresc sold':16 |
  sold'4-! r r <si sold'>4\f |
}

viola = {
  \clef "alto" sold'1 |
  sold'2:16\cresc sold':16 |
  sold'4-! r r mi'\f |
}

voix = {
  \clef "bass" <>^\markup\character Atar r4 r8 si,! mi mi16 mi mi8 mi16 sold |
  fad8 fad r4 si! r8 si |
  sold8 sold16 sold sold8 la mi mi r4 |
}

paroles = \lyricmode {
  Par -- tout il a donc l’a -- van -- ta -- ge.
  Ah ! mon cœur en fré -- mit de ra -- ge.
}

basso = {
  \clef "bass" mi!1 |
  mi2:16\cresc mi:16 |
  re4-! r r mi\f |
}

%%%%%%

\pieceTocNb "3-5" \markup\null
\markup\vspace#1

\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff <<
        \new Staff << \global \violonI >>
        \new Staff << \global \violonII >>
      >>
      \new Staff << \global \viola >>
    >>
    \new Staff \withLyrics <<
      \global \voix
    >> \paroles
    \new Staff << \global \basso >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff << \global \violonI >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff << \global \violonII >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff << \global \viola >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff << \global \basso >>
  >>
  \layout { }
}
