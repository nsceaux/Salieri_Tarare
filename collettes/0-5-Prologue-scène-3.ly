\version "2.19.80"
\include "../common.ily"

\paper { ragged-last-bottom = ##t }
\header { tagline = ##f }

decouper = \markup\column {
  \override #'(fill-with-dots . #t) \line { }
  \vspace#1
}

global = {
  \key do \major \time 4/4 s1*4 s2. \tempo "Maestoso"
}

violonI = {
  \clef "treble" <sol mi'!>2.\fp r4 |
  R1 |
  sol'1\p~ |
  <sol' sib'>1 |
  la'4 r r la8.\f la16 |
  re'4 fad'8.( mi'16 mi'4 la') |
}

violonII = {
  \clef "treble" <do' mi'!>2.\fp r4 |
  R1 |
  mi'1\p~ |
  mi'~ |
  mi'4 r r la8.\f la16 |
  re'4 la2 <dod' mi'>4 |
}

voix = {
  \clef "bass" <>^\markup\character Le Génie du feu
  r4 r8 r16 sol sol8 sol sol sol |
  do'4 r8 sol16 sol sol4 fa8 sol |
  mi mi r sol16 sol sol4 sol8 la |
  sib4. sib8 sib sib sib sib |
  la4
  \clef "treble" <>^\markup\character La Nature
  mi'8. mi'16 la'4 r |
  R1 |
}

paroles = \lyricmode {
  Su -- bi -- te -- ment for -- més s’ils pou -- vaient nous ins -- trui -- re
  du pre -- mier in -- té -- rêt qui les oc -- cu -- pe tous.

  Par -- lez- leur.
}

basso = {
  \clef "bass" do2.\fp r4 |
  R1 |
  do1\p~ |
  do |
  dod4 r r la,8.\f la,16 |
  re4 re( dod la,) |
}

%%%%%%

\pieceTocNb "0-5" \markup\null
\markup\vspace#1

\score {
  \new ChoirStaff <<
    \new GrandStaff \with { instrumentName = "Violons" } <<
      \new Staff << \global \violonI >>
      \new Staff << \global \violonII >>
    >>
    \new Staff \withLyrics <<
      \global \voix
    >> \paroles
    \new Staff \with { instrumentName = "Basses" } << \global \basso >>
  >>
  \layout { short-indent = 0 ragged-last = ##t }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff << \global \violonI >>
  >>
  \layout { indent = 0 }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff << \global \violonII >>
  >>
  \layout { indent = 0 }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff << \global \basso >>
  >>
  \layout { indent = 0 }
}
