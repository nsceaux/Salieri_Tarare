\version "2.19.80"
\include "../common.ily"

\paper { ragged-last-bottom = ##t }
\header { tagline = ##f }
\layout { indent = 5\mm short-indent = 0 ragged-last = ##t system-count = 1 }

decouper = \markup\column {
  \override #'(fill-with-dots . #t) \line { }
  \vspace#1
}

global = {
  \set Score.currentBarNumber = 10 \bar ""
  \key do \major \override Staff.TimeSignature.stencil = ##f \time 4/4 s1 \bar "||"
  \tag #'all \key re \major s1*4 \bar "|."
}

flutes = {
  \clef "treble" R1 |
  <<
    { fad''1 |
      sol'' |
      fad''2 sol'' |
      la'' sol'' | } \\
    { red''1 |
      mi'' |
      red''2 mi'' |
      fad'' mi'' | }
  >>
}

htb = {
  \clef "treble" r2 << si'2 \\ re' >> |
  << { red''1 | mi'' | } \\ { fad'1 | sol' } >> |
  r8*2/3 si' si' si' si' si' si'2~ |
  si'1 |
}

cors = {
  \clef "treble" R1 |
  << mi''1 \\ mi' >> |
  R1*3 |
}

fagotti = {
  \clef "bass" R1 |
  si,1~ |
  si, |
  <<
    { fad2 sol | la sol | } \\
    { red2 mi | fad mi | }
  >>
}

violonI = {
  \clef "treble" r2 <si' re' sol> |
  la'8*2/3( fad' red' si red' fad') si'( red'' mi'' fad'' red'' la') |
  sol'( si mi' sol' si' mi'') sol''( si'' sol'' mi'' si' sol') |
  si( red' fad' si' fad' red') si( mi' sol' si' sol' mi') |
  si( fad' la' si' la' fad') si( mi' sol' si' sol' mi') |
}

violonII = {
  \clef "treble" r2 <si' re' sol> |
  <fad' la'>1 |
  sol' |
  fad'2 sol' |
  la' sol' |
}

viola = {
  \clef "alto" r2 sol' |
  <red' fad'>1 |
  mi' |
  red'2 mi' |
  fad' mi' |
}

voix = {
  \clef "treble" sol'4 sol'8 r r2 |
  R1*4 |
}

paroles = \lyricmode {
  res -- te.
}

basso = {
  \clef "bass" r2 sol |
  <<
    { la8*2/3 fad red si, red fad si4. la8 |
      sol8*2/3 si, mi sol si sol mi4~ mi8*2/3 sol mi | } \\
    { si,1 | si, | }
  >>
  si,1~ |
  si, |
}

%%%%%%

\pieceTocNb "5-18" \markup\null
\markup\vspace#1

\score {
  <<
    \new StaffGroup <<
      \new Staff \with { instrumentName = "Fl." } << \global \flutes >>
      \new Staff \with { instrumentName = "Htb." } << \global \htb >>
      \new Staff \with { instrumentName = \markup\center-column { Cors en ré } } <<
        \keepWithTag #'() \global \cors
      >>
      \new Staff \with { instrumentName = "Bn." } << \global \fagotti >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { instrumentName = "Vn." } <<
        \new Staff << \global \violonI >>
        \new Staff << \global \violonII >>
      >>
      \new Staff \with { instrumentName = "Alt." } << \global \viola >>
    >>
    \new Staff \withLyrics <<
      \global \voix
    >> \paroles
    \new Staff \with { instrumentName = "B." } << \global \basso >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with { instrumentName = "Fl." } << \global \flutes >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with { instrumentName = "Htb." } << \global \htb >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with { instrumentName = \markup\center-column { Cor. en ré } } <<
      \keepWithTag #'() \global \cors
    >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with { instrumentName = "Bn." } << \global \fagotti >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with { instrumentName = "Vn1." } << \global \violonI >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with { instrumentName = "Vn2." } << \global \violonII >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with { instrumentName = "Alt." } << \global \viola >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff \with { instrumentName = "B." } << \global \basso >>
  >>
  \layout { }
}
