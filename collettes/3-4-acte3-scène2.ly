\version "2.19.80"
\include "../common.ily"

\paper { ragged-last-bottom = ##t }
\header { tagline = ##f }
\layout { indent = 0 short-indent = 0 ragged-last = ##t }

decouper = \markup\column {
  \override #'(fill-with-dots . #t) \line { }
  \vspace#1
}

global = {
  \set Score.currentBarNumber = 20 \bar ""
  \key sib \major
  \override Staff.TimeSignature.stencil = ##f \time 4/4 s1 \bar "|."
}

violonI = {
  \clef "treble" si'!2 r4 si'\f |
}

violonII = {
  \clef "treble" sol'2 r4 sol'\f |
}

viola = {
  \clef "alto" re'!2 r4 sol' |
}

voix = {
  \clef "bass" si!4 si8 do' sol4 r |
}

paroles = \lyricmode {
  - saire ex -- pi -- rait.
}

basso = {
  \clef "bass" fa2 r4 sol |
}

%%%%%%

\pieceTocNb "3-4" \markup\null
\markup\vspace#1

\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff <<
        \new Staff << \global \violonI >>
        \new Staff << \global \violonII >>
      >>
      \new Staff << \global \viola >>
    >>
    \new Staff \withLyrics <<
      \global \voix
    >> \paroles
    \new Staff << \global \basso >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff << \global \violonI >>
  >>
  \layout { }
}

\decouper

\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \voix
    >> { \set fontSize = #-2 \paroles }
    \new Staff << \global \violonII >>
  >>
  \layout { }
}
