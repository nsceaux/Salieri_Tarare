\version "2.19.80"
\include "common.ily"

\paper {
  %% plus d'espace entre titre et musique
  markup-system-spacing.padding = #2.5 % default: 0.5
  system-system-spacing.padding = #2.5 % default: 1
  %% de la place entre dernier system et copyright
  last-bottom-spacing.padding = #3
}

%% pour avoir la place de noter les coups d'archet :
\layout {
  %% plus d'espace entre les paroles et la ligne de l'instrument
  \context {
    \Lyrics
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.minimum-distance = #8
  }
  %% Plus d'espace entre les indications de tempo et la portée
  \context {
    \Score
    \override MetronomeMark.padding = #2.5 % default 0.8
  }
  %% Plus d'espace entre les notes et les nuances
  \context {
    \Voice
    \override DynamicText.Y-offset = #(scale-by-font-size -3) % default -0.6
    \override Hairpin.Y-offset = #(scale-by-font-size -3)
    \override DynamicTextSpanner.Y-offset = #(scale-by-font-size -2.5)
  }
}

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = "Tarare"
    date = "1787"
  }
  \markup \null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PROLOGUE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 0-1
\actn "Prologue"
\scene "Scène Première" "Scène I"
\pieceToc\markup\wordwrap {
  Ouverture – La Nature, chœur des Vents :
  \italic { C’est assez troubler l’univers }
}
\includeScore "AAAouverture"
\partNoPageTurn#'(violino1 violino2 viola)
\newBookPart#'(tromboni timpani)
%% 0-2
\scene "Scène Deuxième" "Scène II"
\pieceToc\markup\wordwrap {
  Le Génie du Feu, la Nature :
  \italic { De l’orbe éclatant du soleil }
}
\includeScore "ABAgenieNature"
%% 0-3
\pieceToc\markup\wordwrap {
  La Nature : \italic { Froids humains, non encor vivants }
}
\includeScore "ABBnature"
\newBookPart#'(reduction)
%% 0-4
\scene "Scène Troisième" "Scène III"
\pieceToc\markup\wordwrap {
  Chœur : \italic { Quel charme inconnu nous attire ? }
}
\includeScore "ACAchoeur"
\newBookPart#'(reduction)
%% 0-5
\pieceToc\markup\wordwrap {
  Récit : \italic { Privés des doux liens qui donne la naissance }
}
\includeScore "ACBrecit"
\partNoPageTurn#'(viola)
\newBookPart#'(reduction)
% 0-6
\pieceToc\markup\wordwrap {
  La Nature : \italic { Brillant Soleil, en vain la Nature est féconde }
}
\includeScore "ACCnature"
%% 0-7
\pieceToc\markup\wordwrap {
  Le Génie du Feu : \italic { Gloire à l’éternelle sagesse }
}
\includeScore "ACDgenie"
\newBookPart#'(reduction)
%% 0-8
\pieceToc\markup\wordwrap {
  Récit : \italic { Un mot encor ; c’est une ombre femelle }
}
\includeScore "ACErecit"
%% 0-9
\pieceToc\markup\wordwrap {
  La Nature : \italic { Qu'un jeune cœur, malaisément }
}
\includeScore "ACFnature"
\newBookPart#'(reduction)
%% 0-10
\pieceToc\markup\wordwrap {
  Récit : \italic { Que sont ces deux superbes ombres }
}
\includeScore "ACGrecit"
%% 0-11
\pieceToc\markup\wordwrap {
  La Nature, chœur des Ombres : \italic { Futurs mortels, prosternez-vous }
}
\includeScore "ACHnature"
\includeScore "ACIchoeur"
\newBookPart#'(reduction)
%% 0-12
\pieceToc\markup\wordwrap {
  Récit : \italic { Sois l'empereur Atar, despote de l'Asie }
}
\includeScore "ACJrecit"
%% 0-13
\pieceToc\markup\wordwrap {
  La Nature : \italic { Enfants, embrassez-vous, égaux par la nature }
}
\includeScore "ACKnature"
%% 0-14
\pieceToc\markup\wordwrap {
  Chœur : \italic { Ô bienfaisante déité }
}
\includeScore "ACLchoeur"
%% 0-15
\pieceToc\markup\wordwrap {
  Récit : \italic { C'est assez. Éteignons en eux }
}
\includeScore "ACMrecit"
\partNoPageTurn#'(viola)
%% 0-16
\pieceToc\markup\wordwrap {
  La Nature : \italic { Tels qu'une vapeur élancée }
}
\includeScore "ACNnature"
%% 0-17
\pieceToc\markup\wordwrap {
  Chœur : \italic { Gloire à l'éternelle sagesse }
}
\includeScore "ACOchoeur"
\actEnd FIN DU PROLOGUE
\partBlankPageBreak #'(oboi basso)
\newBookPart#'()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ACTE 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 1-1
\act "Acte Premier"
\scene "Scène Première" "Scène I"
\pieceToc "Ouverture"
\includeScore "BAAouverture"
%% 1-2
\pieceToc\markup\wordwrap {
  Récit : \italic { Laisse-moi, Calpigi }
}
\includeScore "BABrecit"
%% 1-3
\pieceToc\markup\wordwrap {
  Calpigi : \italic { Il est vrai, son nom adoré. } Récit
}
\includeScore "BACcalpigi"

\scene "Scène deuxième" "Scène II"
\sceneDescription\markup\wordwrap-center {
  Les précédents, Altamort.
}
%% 1-4
\pieceToc\markup\wordwrap {
  Recit : \italic { Mais qu’annonce Altamort à mon impatience }
}
\includeScore "BBArecit"

\scene "Scène Troisième" "Scène III"
%% 1-5
\pieceToc\markup\wordwrap {
  Chœur d’esclaves du sérail : \italic {
    Dans les plus beaux lieux de l'Asie
  }
}
\includeScore "BCAchoeur"
\partNoPageTurn#'(viola)
%% 1-6
\pieceToc\markup\wordwrap {
  Récit : \italic { O sort affreux, dont l’horreur me poursuit }
}
\includeScore "BCBrecit"
%% 1-7
\pieceToc\markup\wordwrap {
  Atar : \italic { Je suis heureux, vous êtes ranimée. } Récit
}
\includeScore "BCCatar"

\scene "Scène Quatrième" "Scène IV"
%% 1-8
\pieceToc\markup\wordwrap {
  Récit : \italic {
    Qui voulez-vous, Seigneur, auprès d’elle qu’on mette ?
  }
}
\includeScore "BDArecit"
%% 1-9
\pieceToc\markup\wordwrap {
  Spinette : \italic { Oui, Seigneur, je veux la réduire. } Récit
}
\includeScore "BDBspinette"

\scene "Scène Cinquième" "Scène V"
%% 1-10
\pieceToc\markup\wordwrap {
  Récit : \italic {
    Seigneur, c'est ce guerrier, du peuple la merveille…
  }
}
\includeScore "BEArecit"

\scene "Scène Sixième" "Scène VI"
%% 1-11
\pieceToc\markup\wordwrap {
  Atar, Tarare : \italic { Que me veux-tu, brave soldat }
}
\includeScore "BFArecit"
%% 1-12
\pieceToc\markup\wordwrap {
  Atar : \italic { Reçois en pur don ce palais. } Récit
}
\includeScore "BFBatar"
%% 1-13
\pieceToc\markup\wordwrap {
  Tarare : \italic { Astasie est une déesse. }
}
\includeScore "BFCtarare"
%% 1-14
\pieceToc\markup\wordwrap {
  Récit : \italic { Quoi, soldat ! pleurer une femme }
}
\includeScore "BFDrecit"
%% 1-15
\pieceToc\markup\wordwrap {
  Atar : \italic { Qu’as-tu donc fait de ton mâle courage ? }
}
\includeScore "BFEatar"
\partNoPageTurn#'(violino1 violino2)
%% 1-16
\pieceToc\markup\wordwrap {
  Tarare : \italic { Seigneur, si j'ai sauvé ta vie }
}
\includeScore "BFFtarare"

\scene "Scène Septième" "Scène VII"
%% 1-17
\pieceToc\markup\wordwrap {
  Récit : \italic { Que veux-tu, Calpigi ? }
}
\includeScore "BGArecit"
%% 1-18
\pieceToc\markup\wordwrap {
  Tarare : \italic { Charmante Irza, qu’est-ce donc qui t’arrête ? }
}
\includeScore "BGBtarare"
%% 1-19
\pieceToc\markup\wordwrap {
  Atar, Tarare, Altamort, Calpigi :
  \italic { Brave Altamort, avant le point du jour }
}
\includeScore "BGCatar"

\scene "Scène Huitième" "Scène VIII"
%% 1-20
\pieceToc\markup\wordwrap {
  Atar : \italic { Vertu farouche et fière }
}
\includeScore "BHAatar"
\actEnd\markup { FIN DU \concat { 1 \super er } ACTE }
\newBookPart#'()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ACTE 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\act "Acte Deuxième"
\scene "Scène Première" "Scène I"
%% 2-1
\pieceToc\markup\wordwrap {
  Récit : \italic { Seigneur, le grand-prêtre Arthénée }
}
\includeScore "CAArecit"
\partNoPageTurn#'(violino1 violino1 viola basso)

\scene "Scène Deuxième" "Scène II"
\sceneDescription\markup\wordwrap-center {
  Arthénée, Atar.
}
%% 2-2
\pieceToc\markup\wordwrap {
  Récit : \italic { Les sauvages d'un autre monde }
}
\includeScore "CBArecit"
%% 2-3
\pieceToc\markup\wordwrap {
  Arthénée : \italic { Qu'il faut combattre }
}
\includeScore "CBBarthenee"
%% 2-4
\pieceToc\markup\wordwrap {
  Récit : \italic { Apprends-moi donc, ô chef des Brames ! }
}
\includeScore "CBCrecit"
%% 2-5
\pieceToc\markup\wordwrap {
  Arthénée : \italic { Ah ! d'une antique absurdité. } Récit
}
\includeScore "CBDarthenee"
%% 2-6
\pieceToc\markup\wordwrap {
  Arthénée : \italic { Crains de payer de ta couronne. } Récit
}
\includeScore "CBEarthenee"
%% 2-7
\pieceToc\markup\wordwrap {
  Atar : \italic { Apprends-moi donc, ô chef des Brames ! }
}
\includeScore "CBFatar"
\partNoPageTurn#'(viola violino1 violino2 basso)

\scene "Scène Troisième" "Scène III"
%% 2-8
\pieceToc\markup\wordwrap {
  Arthénée : \italic { Ô politique consommée }
}
\includeScore "CCAarthenee"
\partNoPageTurn#'(viola)

\scene "Scène Quatrième" "Scène IV"
\sceneDescription\markup\wordwrap-center { Tarare seul. }
%% 2-9
\pieceToc\markup\wordwrap {
  Tarare : \italic { De quel nouveau malheur suis-je encor menacé }
}
\includeScore "CDAtarare"

\scene "Scène Cinquième" "Scène V"
\sceneDescription\markup\wordwrap-center {
  Calpigi déguisé, couvert d’une cape, et Tarare.
}
%% 2-10
\pieceToc\markup\wordwrap {
  Récit : \italic { Tarare ! connais-moi }
}
\includeScore "CEArecit"
\includeScore "CEBcalpigi"

\scene "Scène Sixième" "Scène VI"
\sceneDescription\markup\wordwrap-center {
  Tarare seul.
}
%% 2-11
\pieceToc\markup\wordwrap {
  Tarare : \italic { J’irai, oui, j’oserai }
}
\includeScore "CFAtarare"

\scene "Scène Septième" "Scène VII"
%% 2-12
\pieceToc\markup\wordwrap {
  Récit : \italic { Sur un choix important le ciel est consulté }
}
\includeScore "CGArecit"
%% 2-13
\pieceToc\markup\wordwrap {
  Arthénée : \italic { Ainsi qu'une abeille }
}
\includeScore "CGBarthenee"
%% 2-14
\pieceToc\markup\wordwrap {
  Récit : \italic { Tout le peuple, mon fils, sous nos voûtes arrive }
}
\includeScore "CGCrecit"

\scene "Scène Huitième" "Scène VIII"
%% 2-15
\pieceToc "Marche"
\includeScore "CHAmarche"
\newBookPart#'(tromboni)
%% 2-16
\pieceToc\markup\wordwrap {
  Arthénée, chœur  : \italic { Prêtres du grand Brama ! Roi du Golfe Persique ! }
}
\includeScore "CHBarthenee"
%% 2-17
\pieceToc\markup\wordwrap {
  Tarare : \italic { Qui veut la gloire }
}
\includeScore "CHCtarare"
%% 2-18
\pieceToc\markup\wordwrap {
  Récit : \italic { Je ne puis soutenir la clameur importune }
}
\includeScore "CHDrecit"
%% 2-19
\pieceToc\markup\wordwrap {
  Tarare : \italic { Apprends, fils orgueilleux des prêtres }
}
\includeScore "CHEtarare"
%% 2-20
\pieceToc\markup\wordwrap {
  Récit : \italic { Sans le respect d’Atar, vil objet de ma haine }
}
\includeScore "CHFrecit"
%% 2-21
\pieceToc\markup\wordwrap {
  Chœur : \italic { Brama ! si la vertu t’es chère }
}
\includeScore "CHGchoeur"
\actEnd\markup { FIN DU \concat { 2 \super e } ACTE }
\newBookPart#'()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ACTE 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\act "Acte Troisième"
\scene "Scène Première" "Scène I"
%% 3-1
\pieceToc\markup\wordwrap {
  Récit : \italic { Les jardins éclairés ! des bostangis ! pourquoi ? }
}
\includeScore "DAArecit"

\scene "Scène Deuxième" "Scène II"
%% 3-2
\pieceToc\markup\wordwrap {
  Récit : \italic { Avant que ma fête commence }
}
\includeScore "DBArecit"
%% 3-3
\pieceToc\markup\wordwrap {
  Urson : \italic { Tarare seul arrive au rendez-vous }
}
\includeScore "DBBurson"
\newBookPart#'(timpani tromboni)
%% 3-4
\pieceToc\markup\wordwrap {
  Urson : \italic { Ne crains rien, superbe Altamort }
}
\includeScore "DBCurson"
%% 3-5
\pieceToc\markup\wordwrap {
  Atar : \italic { Partout il a donc l'avantage }
}
\includeScore "DBDatar"
\newBookPart#'(clarinetti)

\scene "Scène Troisième" "Scène III"
%% 3-6
\pieceToc ""
\includeScore "DCAritournelle"
%% 3-7
\pieceToc\markup\wordwrap {
  Récit : \italic { Calpigi, quel spectacle ai-je pour ma sultane ? }
}
\includeScore "DCBrecit"

\scene "Scène Quatrième" "Scène IV"
%% 3-8
\pieceToc "Marche"
\includeScore "DDAmarche"
%% 3-9
\pieceToc\markup\wordwrap {
  Chœur : \italic { Peuple léger mais généreux }
}
\includeScore "DDBchoeur"
%% 3-10
\pieceToc "Minuetto"
\includeScore "DDCmenuet"
%% 3-11
\pieceToc ""
\includeScore "DDDdanse"
%% 3-12
\pieceToc\markup\wordwrap {
  Duo dialogué : \italic { Galants qui courtisez les belles }
}
\includeScore "DDEduo"
%% 3-13
\pieceToc ""
\includeScore "DDFdanse"
%% 3-14
\pieceToc\markup\wordwrap {
  Spinette, un paysan : \italic { Dans nos vergers délicieux }
}
\includeScore "DDGspinette"
\includeScore "DDHastasie"
%% 3-15
\pieceToc\markup\wordwrap {
  Air. Astasie : \italic { Ô mon Tarare, ô mon époux }
}
\includeScore "DDIair"
%% 3-16
\pieceToc\markup\wordwrap {
  Spinette, un paysan : \italic { Dans nos vergers délicieux }
}
\reIncludeScore "DDGspinette" "DDJspinette"
\newBookPart#'(violino2)
%% 3-17
\pieceToc\markup\wordwrap {
  Récit : \italic { Saluez tous la belle Irza }
}
\includeScore "DDKrecit"
\partNoPageTurn#'(violino2)
%% 3-18
\pieceToc\markup\wordwrap {
  Chœur : \italic { Saluons tous la belle Irza }
}
\includeScore "DDLchoeur"
%% 3-19
\pieceToc ""
\includeScore "DDMdanse"
%% 3-20
\pieceToc\markup\wordwrap {
  Récit : \italic { Calpigi, ta fête est charmante }
}
\includeScore "DDNrecit"
\newBookPart#'(oboi)
%% 3-21
\pieceToc\markup\wordwrap {
  Calpigi, chœur : \italic { Je suis natif de Ferrare }
}
\includeScore "DDOcalpigi"
%% 3-22
\pieceToc\markup\wordwrap {
  Astasie, Spinette, Atar, chœur : \italic { Tarare ! }
}
\includeScore "DDPtous"

\scene "Scène Cinquième" "Scène V"
%% 3-23
\pieceToc\markup\wordwrap {
  Récit : \italic { Ô Tarare ! }
}
\includeScore "DEArecit"
%% 3-24
\pieceToc\markup\wordwrap {
  Tarare : \italic { Au sein de la profonde mer }
}
\includeScore "DEBtarare"
%% 3-25
\pieceToc\markup\wordwrap {
  Tarare, Calpigi : \italic { Je suis sauvé, grâce à ton cœur }
}
\includeScore "DECduo"
\partNoPageTurn#'(viola)

\scene "Scène Sixième" "Scène VI"
%% 3-26
\pieceToc\markup\wordwrap {
  Atar, Calpigi : \italic { On vient ; c'est le sultan }
}
\includeScore "DFArecit"
\newBookPart#'(violino2)
\scene "Scène Septième" "Scène VII"
%% 3-27
\pieceToc\markup\wordwrap {
  Tarare : \italic { Dieu tout-puissant ! tu ne trompas jamais }
}
\includeScore "DGAtarare"
\actEnd\markup { FIN DU \concat { 3 \super e } ACTE }
\markupCond #(and (member (*part*) '(trombe corni oboi flauti fagotti)) #t)
\markup\vspace#35
\markupCond #(and (member (*part*) '(clarinetti)) #t)
\markup\vspace#20
\markupCond #(and (member (*part*) '(violino2)) #t)
\markup\vspace#10
\newBookPart#'(oboi clarinetti flauti fagotti
                    violino1 viola basso 
	            corni trombe tromboni timpani)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ACTE 4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\act "Acte Quatrième"
\scene "Scène Première" "Scène I"
%% 4-1
\pieceToc\markup\wordwrap {
  Astasie, Spinette : \italic { Spinette, comment fuir de cette horrible enceinte ? }
}
\includeScore "EAArecit"
%% 4-2
\pieceToc\markup\wordwrap {
  Récit : \italic { Un grand roi vous invite à faire son bonheur }
}
\includeScore "EABrecit"

\scene "Scène Deuxième" "Scène II"
%% 4-3
\pieceToc\markup\wordwrap {
  Calpigi, Spinette, Astasie : \italic { Belle Irza, l’empereur ordonne }
}
\includeScore "EBArecit"

\scene "Scène Troisième" "Scène III"
%% 4-4
\pieceToc\markup\wordwrap {
  Astasie, Spinette : \italic { Ô ma compagne ! ô mon amie ! }
}
\includeScore "ECArecit"
\newBookPart#'(viola)
%% 4-5
\pieceToc\markup\wordwrap {
  Spinette : \italic { Je partage votre détresse }
}
\includeScore "ECBspinette"

\scene "Scène Quatrième" "Scène IV"
%% 4-6
\pieceToc\markup\wordwrap {
  Récit : \italic { Spinette, allons, point de faiblesse }
}
\includeScore "EDArecit"

\scene "Scène Cinquième" "Scène V"
%% 4-7
\pieceToc\markup\wordwrap {
  Récit : \italic { Cette femme est à toi, muet }
}
\includeScore "EEArecit"

\scene "Scène Sixième" "Scène VI"
%% 4-8
\pieceToc\markup\wordwrap {
  Spinette, Tarare : \italic { Comme il est laid }
}
\includeScore "EFArecit"
%% 4-9
\pieceToc\markup\wordwrap {
  Tarare, Spinette : \italic { Étranger dans Ormus, hier on me vint dire }
}
\includeScore "EFBtarare"
%% 4-10
\pieceToc\markup\wordwrap {
  Duo : \italic { Ami, ton courage m'éclaire }
}
\includeScore "EFCduo"

\scene "Scène Septième" "Scène VII"
%% 4-11
\pieceToc\markup\wordwrap {
  Urson, Calpigi, chœurs : \italic { Marchez, soldats, doublez le pas }
}
\includeScore "EGAchoeur"
%% 4-12
\pieceToc\markup\wordwrap {
  Récit : \italic { Urson, expliquez-vous }
}
\includeScore "EGBrecit"

\scene "Scène Huitième" "Scène VIII"
%% 4-13
\pieceToc\markup\wordwrap {
  Récit : \italic { Sur deux têtes la foudre, et l'on m'ose nommer }
}
\includeScore "EHArecit"
%% 4-14
\pieceToc\markup\wordwrap {
  Calpigi : \italic { Vas ! l’abus du pouvoir suprême }
}
\includeScore "EHBcalpigi"
\actEnd\markup { FIN DU \concat { 4 \super e } ACTE }
\newBookPart#'()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ACTE 5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\act "Acte Cinquième"
\scene "Scène Première" "Scène I"
%% 5-1
\pieceToc\markup\wordwrap {
  Atar : \italic { Fantôme vain ! idole populaire }
}
\includeScore "FAAatar"
\partNoPageTurn#'(violino2)

\scene "Scène Deuxième" "Scène II"
%% 5-2
\pieceToc\markup\wordwrap {
  Arthénée, Atar : \italic { Que veux-tu, roi d'Ormus ? et quel nouveau malheur }
}
\includeScore "FBArecit"

\scene "Scène Troisième" "Scène III"
%% 5-3
\pieceToc\markup\wordwrap {
  Récit : \italic { Approche, malheureux ! viens subir le supplice }
}
\includeScore "FCArecit"
%% 5-4
\pieceToc\markup\wordwrap {
  Tarare, Atar : \italic { Je ne puis mourir qu'une fois }
}
\includeScore "FCBtarare"

\scene "Scène Quatrième" "Scène IV"
%% 5-5
\pieceToc\markup\wordwrap {
  Récit : \italic { Ainsi donc, abusant de vos charmes }
}
\includeScore "FDArecit"
%% 5-6
\pieceToc\markup\wordwrap {
  Chœur, Arthénée : \italic { Avec tes décrets infinis }
}
\includeScore "FDBchoeur"
%% 5-7
\pieceToc\markup\wordwrap {
  Récit : \italic { Ne m’impute pas, étranger }
}
\includeScore "FDCrecit"
%% 5-8
\pieceToc\markup\wordwrap {
  Astasie, Atar : \italic { Ô tigre ! mes dédains ont trompé ton attente }
}
\includeScore "FDDastasie"
%% 5-9
\pieceToc\markup\wordwrap {
  Trio : \italic { Le trépas nous attend }
}
\includeScore "FDEtrio"
\partNoPageTurn#'(violino1 violino2 viola)

\scene "Scène Cinquième" "Scène V"
%% 5-10
\pieceToc\markup\wordwrap {
  Chœur : \italic { Atar, défends-nous, sauve-nous }
}
\includeScore "FEAchoeur"
\partNoPageTurn#'(fagotti violino1 violino2 viola basso)
\scene "Scène Sixième" "Scène VI"
%% 5-11
\pieceToc\markup\wordwrap {
  Chœur : \italic { Tarare, Tarare, Tarare. } Récit.
}
\includeScore "FFAchoeur"
\partNoPageTurn#'(violino1 violino2 viola basso)

\scene "Scène Septième" "Scène VII"
%% 5-12
\pieceToc\markup\wordwrap {
  Calpigi, Tarare, chœur :
  \italic { Tous les torts de son règne, un seul mot les répare }
}
\includeScore "FGAchoeur"
%% 5-13
\pieceToc\markup\wordwrap {
  Tarare : \italic { Le trône est pour moi sans appas }
}
\includeScore "FGBtarare"

\scene "Scène Huitième" "Scène VIII"
%% 5-14
\pieceToc\markup\wordwrap {
  Urson : \italic { Non, par mes mains, le peuple entier }
}
\includeScore "FHArecit"
%% 5-15
\pieceToc\markup\wordwrap {
  Arthénée, chœur : \italic { Tarare, il faut céder }
}
\includeScore "FHBchoeur"

\scene "Scène Neuvième" "Scène IX"
%% 5-16
\pieceToc\markup\wordwrap {
  Tarare : \italic { Enfants, vous m'y forcez, je garderai ces fers }
}
\includeScore "FIArecit"
%% 5-17
\pieceToc\markup\wordwrap {
  Chœur général : \italic { Quel plaisir de nos cœurs s’empare }
}
\includeScore "FIBchoeur"
\partNoPageTurn#'(violino1 violino2 viola basso)
\scene "Scène Dernière" "Scène X"
\sceneDescription\markup\wordwrap-center {
  Tous les précédents, excepté le Grand Prêtre.
}
%% 5-18
\pieceToc\markup\wordwrap {
  Récit : \italic { Nature, quel exemple imposant et funeste }
}
\includeScore "FJArecit"
%% 5-19
\pieceToc\markup\wordwrap {
  Chœur, la Nature, le Génie du feu : \italic { De ce grand bruit, de cet éclat }
}
\includeScore "FJBchoeur"
\actEnd\markup\column { FIN \vspace#1 }

\scene "[Annexe]" "[Annexe]"
%% 5-20
\pieceToc\markup\wordwrap {
  Récit : \italic { Les fiers Européans marchent vers ces états }
}
\includeScore "FKArecit"
%% 5-21
\pieceToc\markup\wordwrap {
  Urson, Calpigi, deux femme, chœur : \italic { Roi, nous mettons la liberté }
}
\includeScore "FKBchoeur"
\actEnd FIN
