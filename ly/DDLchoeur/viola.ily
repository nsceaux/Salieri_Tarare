\clef "alto" \ru#16 { re'8 re'' } |
re' re'' re' re'' re' fad' fad' fad' |
mi'4 mi' la' la' |
\ru#16 { re'8 re'' } |
re'2 <la mi'>8*2/3\p q q q q q |
<la fad'>\f q q q4 la'8*2/3\p dod'' si' la' si' dod'' |
re''\f re'' re'' re''4 si8*2/3\ff si' si' si' si' si' |
sol'2.*2/3:8 la':8 |
re'4 r <la mi'>8*2/3\p q q q q q |
<la fad'>\f q q q4 la'8*2/3\p dod'' si' la' si' dod'' |
re''-\sug\f re'' re'' re''4 si8*2/3\ff si' si' si' si' si' |
sol'2.*2/3:8 la':8 |
re':8 si8*2/3 si' si' si' si' si' |
sol'2.*2/3:8 la':8 |
\ru#24 { re'8 re'' } |
re'4 r r2 |
