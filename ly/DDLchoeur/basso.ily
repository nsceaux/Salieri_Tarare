\clef "bass" \ru#16 { re8 re' } |
re re' re re' re fad fad fad |
mi4 mi la la |
\ru#16 { re8 re' } |
re2 <<
  \twoVoices #'(fagotto1 fagotto2 fagotti) <<
    { mi'8*2/3 mi' mi' mi' mi' mi' |
      fad' fad' fad' fad'4 }
    { la8*2/3 la la la la la |
      la la la la4 }
    { \clef "tenor" s2-\sug\p | s2-\sug\f \clef "bass" }
  >>
  \tag #'basso {
    dod8*2/3\p dod dod dod dod dod |
    re\f re re re4
  }
>> la8*2/3\p dod' si la si dod' |
re'\f re' re' re'4 si,8*2/3\ff si si si si si |
sol2.*2/3:8 la:8 |
re4 r <<
  \twoVoices #'(fagotto1 fagotto2 fagotti) <<
    { mi'2 | }
    { la2 | }
    { \clef "tenor" s2-\sug\p \clef "bass" }
  >>
  \tag #'basso {
    dod8*2/3\p dod dod dod dod dod |
  }
>>
re8*2/3\f re re re4 la8*2/3\p dod' si la si dod' |
re'\f re' re' re'4 si,8*2/3\ff si si si si si |
sol2.*2/3:8 la:8 |
re:8 si,8*2/3 si si si si si |
sol2.*2/3:8 la:8 |
\ru#24 { re8 re' } |
re4 r r2 |
