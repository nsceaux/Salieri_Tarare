\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Trompettes et Cors en Ré }
    } <<
      \keepWithTag #'() \global
      \keepWithTag #'corni \includeNotes "corni"
    >>
    \new DrumStaff \with {
      \override StaffSymbol.line-count = #1
      instrumentName = \markup\center-column { Tambour et Cimballes }
      drumStyleTable = #percussion-style
    } << \global \includeNotes "tambour" >>
  >>
  \layout { indent = \largeindent }
}
