<<
  \tag #'(vdessus basse) {
    \clef "soprano/treble" r2 r8 la' la' la' |
    re''4 re'' re'' fad'' |
    re''2 r8 la' la'8. la'16 |
    re''4 re'' re'' fad'' |
    re''2 re''8 re'' re'' re'' |
    sol''4 sol'' mi'' mi'' |
    fad''2 r8 la' la' la' |
    re''4 re'' re'' fad'' |
    re''2 re''8 la' la' la' |
    re''4 re'' re'' fad'' |
    re''2 mi''4 mi''8 mi'' |
    fad''4 fad'' sol'' sol'' |
    fad''4 fad'' r8 fad'' fad'' fad'' |
    mi''4 mi'' la'' la'' |
    fad''2 mi''4 mi''8 mi'' |
    fad''4 fad'' sol'' sol'' |
    fad'' fad'' r8 fad'' fad'' fad'' |
    mi''4 mi'' la'' la'' |
    fad''2 r8 fad'' fad'' fad'' |
    mi''4 mi'' la'' la'' |
    re''2 r |
  }
  \tag #'vhaute-contre {
    \clef "alto/G_8" r2 r8 la la la |
    re'4 re' re' fad' |
    re'2 r8 la la8. la16 |
    re'4 re' re' fad' |
    re'2 re'8 la' la' la' |
    sol'4 sol' la' la' |
    la'2 r8 la la la |
    re'4 re' re' fad' |
    re'2 re'8 la la la |
    re'4 re' re' fad' |
    re'2 la'4 la'8 la' |
    la'4 la' la' la' |
    la' la' r8 fad' fad' fad' |
    sol'4 sol' mi' mi' |
    fad'2 la'4 la'8 la' |
    la'4 la' la' la' |
    la' la' r8 fad' fad' fad' |
    sol'4 sol' mi' mi' |
    fad'2 r8 fad' fad' fad' |
    sol'4 sol' mi' mi' |
    fad'2 r |
  }
  \tag #'vtaille {
    \clef "tenor/G_8" r2 r8 la la la |
    re'4 re' re' fad' |
    re'2 r8 la la8. la16 |
    re'4 re' re' fad' |
    re'2 re'8 re' re' re' |
    si4 si dod' dod' |
    re'2 r8 la la la |
    re'4 re' re' fad' |
    re'2 re'8 la la la |
    re'4 re' re' fad' |
    re'2 dod'4 dod'8 dod' |
    re'4 re' mi' mi' |
    re' re' r8 re' re' re' |
    re'4 re' dod' dod' |
    re'2 dod'4 dod'8 dod' |
    re'4 re' mi' mi' |
    re' re' r8 re' re' re' |
    re'4 re' dod' dod' |
    re'2 r8 re' re' re' |
    re'4 re' dod' dod' |
    re'2 r |
  }
  \tag #'vbasse {
    \clef "bass" r2 r8 la la la |
    re'4 re' re' la |
    fad2 r8 la la8. la16 |
    re'4 re' re' la |
    fad2 fad8 fad fad fad |
    mi4 mi la la |
    re'2 r8 la la la |
    re'4 re' re' la |
    fad2 fad8 la la la |
    re'4 re' re' la |
    fad2 la4 la8 la |
    re'4 re' la la |
    re' re' r8 si si si |
    sol4 sol la la |
    re2 la4 la8 la |
    re'4 re' la la |
    re' re' r8 si si si |
    sol4 sol la la |
    re'2 r8 si si si |
    sol4 sol la la |
    re2 r |
  }
>>
R1*6 |
