\clef "treble" \transposition re
\twoVoices #'(corno1 corno2 corni) <<
  { do'2. }
  { do'2. }
>> r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''4 do'' do'' mi'' |
    do''2~ do''8 sol' sol' sol' |
    do''4 do'' do'' mi'' |
    do''2~ do''8 do'' do'' do'' |
    re''4 re''8. re''16 re''4 re''8. re''16 |
    mi''2~ mi''8 sol' sol' sol' |
    do''4 do'' do'' mi'' |
    do''2~ do''8 sol' sol' sol' |
    do''4 do'' do'' mi'' |
    do''2 re''4 re''8 re'' |
    \tuplet 3/2 { mi''8 mi'' mi'' } mi''4 re'' re''8 re'' |
    \tuplet 3/2 { mi''8 mi'' mi'' } mi''4 mi''2 |
    re'' re'' |
    do''4 }
  { mi'4 mi' mi' sol' |
    mi'2~ mi'8 mi' mi' mi' |
    mi'4 mi' mi' sol' |
    mi'2~ mi'8 do'' do'' do'' |
    re''4 re''8. re''16 sol'4 sol'8. sol'16 |
    do''2~ do''8 mi' mi' mi' |
    mi'4 mi' mi' sol' |
    mi'2~ mi'8 mi' mi' mi' |
    mi'4 mi' mi' sol' |
    mi'2 sol'4 sol'8 sol' |
    \tuplet 3/2 { sol'8 sol' sol' } sol'4 sol' sol'8 sol' |
    \tuplet 3/2 { sol'8 sol' sol' } sol'4 do''2 |
    do'' sol' |
    mi'4 }
  { s1*9 | s2 s-\sug\p | s2-\sug\f s-\sug\p | s-\sug\f s-\sug\ff }
>> r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''4 re''8 re'' |
    \tuplet 3/2 { mi''8 mi'' mi'' } mi''4 re'' re''8 re'' |
    \tuplet 3/2 { mi''8 mi'' mi'' } mi''4 mi''2 |
    re''1 |
    mi'' |
    re''1 |
    do''2. \tuplet 3/2 { sol'8 sol' sol' } |
    do''4 do'' do'' mi'' |
    do''2. \tuplet 3/2 { sol'8 sol' sol' } |
    do''4 do'' do'' mi'' |
    do''4. mi''8 do''4. mi''8 |
    do''4. mi''8 do'' mi'' do'' mi'' |
    do''4 }
  { sol'4 sol'8 sol' |
    \tuplet 3/2 { do''8 do'' do'' } do''4 sol' sol'8 sol' |
    \tuplet 3/2 { do''8 do'' do'' } do''4 do''2 |
    do'' sol' |
    do''1 |
    do''2 sol' |
    mi'2. \tuplet 3/2 { sol'8 sol' sol' } |
    mi'4 mi' mi' sol' |
    mi'2. \tuplet 3/2 { sol'8 sol' sol' } |
    mi'4 mi' mi' sol' |
    mi'4. sol'8 mi'4. sol'8 |
    mi'4. sol'8 mi' sol' mi' sol' |
    mi'4 }
  { s2-\sug\p | s2-\sug\f s-\sug\p | s-\sug\f s-\sug\ff | }
>> r4 r2 |
