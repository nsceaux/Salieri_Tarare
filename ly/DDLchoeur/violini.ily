\clef "treble" << { re'2~ re'8 } \\ { re'2~ re'8 } >> <<
  \tag #'violino1 {
    la''8 la'' la'' |
    re'''4 re''' re''' fad''' |
    re'''2~ re'''8 la'' la'' la'' |
    re'''4 re''' re''' fad''' |
    re'''2~ re'''8 la'' la'' la'' |
    sol''4 <sol'' si'>8. q16 <dod'' mi''>4 q8. q16 |
    <re' la' fad''>2~ <la' fad''>8 <fad'' la''>[ q q] |
    re'''4 re''' re''' fad''' |
    re'''2~ re'''8 la'' la'' la'' |
    re'''4 re''' re''' fad''' |
    re'''2
  }
  \tag #'violino2 {
    << { la'8 la' la' } \\ { fad'8 fad' fad' } >> |
    <la fad' re''>4 <fad' re''> q <la' fad''> |
    <la fad' re''>2~ <fad' re''>8 <fad' la'> q q |
    <la fad' re''>4 <fad' re''> q <la' fad''> |
    <la fad' re''>2~ <fad' re''>8 <fad' la'> q q |
    si'4 <si' sol''>8. q16 <dod'' mi''>4 q8. q16 |
    <re' la' fad''>2~ <la' fad''>8 <fad' la'>[ q q] |
    <la fad' re''>4 <fad' re''> q <la' fad''> |
    <la fad' re''>2~ <fad' re''>8 <fad' la'> q q |
    <la fad' re''>4 <fad' re''> q <re' la' fad''> |
    <la fad' re''>2
  }
>> la''8*2/3\p si'' la'' sol'' fad'' mi'' |
re''\f re'' re'' re''4 la''8*2/3\p dod''' si'' la'' si'' dod''' |
<re''' re''>\f q q q4 si8*2/3\ff <fad'' re'''>[ q] q[ q q] |
<mi'' re'''>2.*2/3:8 <mi'' dod'''>:8 |
<fad'' re'''>4 r la''8*2/3\p si'' la'' sol'' fad'' mi'' |
re''\f re'' re'' re''4 la''8*2/3\p dod''' si'' la'' si'' dod''' |
<re'' re'''>-\sug\f q q q4 si8*2/3\ff <fad'' re'''> q q q q |
<mi'' re'''>2.*2/3:8 <mi'' dod'''>:8 |
<fad'' re'''>:8 q:8 |
<mi'' re'''>:8 <mi'' dod'''>:8 |
<re'' re'''>2~ q4 <<
  \tag #'violino1 {
     \tuplet 3/2 { la''8 si'' dod''' } |
     re'''4 re''' re''' fad''' |
     re'''2. \tuplet 3/2 { la''8 si'' dod''' } |
     re'''4 re''' re''' fad''' |
     re'''4. fad'''8 re'''4. fad'''8 |
     re'''4. fad'''8 re''' fad''' re''' fad''' |
     re'''4 r r2 |
  }
  \tag #'violino2 {
    \tuplet 3/2 { la'8 si' dod'' } |
    re''4 <re' la' fad''> q la'' |
    <re' la' fad''>2. \tuplet 3/2 { la'8 si' dod'' } |
    re''4 <re' la' fad''>4 q <la' la''> |
    <re' la' fad''>4. la''8 fad''4. la''8 |
    fad''4. <la'' la'>8 <la' fad''> <la' la''> <la' fad''> <la' la''> |
    <re' la' fad''>4 r r2 |
  }
>>
