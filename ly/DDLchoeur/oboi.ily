\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re'2~ re'8 la' la' la' |
    re''4 re'' re'' fad'' |
    re''2~ re''8 la' la' la' |
    re''4 re'' re'' fad'' |
    re''2~ re''8 la'' la'' la'' |
    sol''4 sol''8. sol''16 mi''4 mi''8. mi''16 |
    fad''2~ fad''8 la' la' la' |
    re''4 re'' re'' fad'' |
    re''2~ re''8 la' la' la' |
    re''4 re'' re'' fad'' |
    re''2 }
  { re'2~ re'8 la' la' la' |
    fad'4 fad' fad' la' |
    fad'2~ fad'8 fad' fad' fad' |
    fad'4 fad' fad' la' |
    fad'2~ fad'8 la' la' la' |
    si'4 si'8. si'16 dod''4 dod''8. dod''16 |
    re''2~ re''8 fad' fad' fad' |
    fad'4 fad' fad' la' |
    fad'2~ fad'8 fad' fad' fad' |
    fad'4 fad' fad' la' |
    la'2 }
>> r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''8*2/3 fad'' fad'' fad''4 sol''2 |
    fad''8*2/3 fad'' fad'' fad''4 re'''2 |
    re''' dod''' |
    re'''4 }
  { la'8*2/3 la' la' la'4 mi''2 |
    la'8*2/3 la' la' la'4 fad''2 |
    mi''1 |
    fad''4 } |
  { s2-\sug\f s-\sug\p | s-\sug\f s-\sug\ff | }
>> r4 r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''8*2/3 fad'' fad'' fad''4 sol''2 |
    fad''8*2/3 fad'' fad'' fad''4 re'''2 |
    re''' dod''' |
    re'''1 |
    re'''2 dod''' |
    re'''2. }
  { la'8*2/3 la' la' la'4 mi''2 |
    re''8*2/3 re'' re'' re''4 fad''2 |
    mi''1 |
    fad''1 |
    mi''1 |
    fad''2. }
  { s2-\sug\f s-\sug\p | s-\sug\f s-\sug\ff | }
>> \tuplet 3/2 { la'8 si' dod'' } |
re''4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 re'' fad'' |
    re''2. }
  { fad'4 fad' la' |
    fad'2. }
>> \tuplet 3/2 { la'8 si' dod'' } |
re''4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 re'' fad'' |
    re''4. fad''8 re''4. fad''8 |
    re''4. fad''8 re'' fad'' re'' fad'' |
    re''4 }
  { fad'4 fad' la' |
    fad'4. la'8 fad'4. la'8 |
    fad'4. la'8 fad' la' fad' la' |
    fad'4 }
>> r4 r2 |
