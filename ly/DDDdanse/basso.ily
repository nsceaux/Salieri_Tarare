\clef "bass" r8 re\ff mi fad |
sol1~ |
sol~ |
sol~ |
sol~ |
sol2 do |
re~ re8\noBeam re8-\sug\ff-! mi-! fad-! |
sol1~ |
sol~ |
sol~ |
sol~ |
sol4 do re re, |
sol,2~ sol,8\noBeam sol8-!\ff fad-! mi-! |
re1~ |
re~ |
re2 re8\noBeam la,-!\ff si,-! dod-! |
re1~ |
re~ |
re~ |
re\p~ |
re~ |
re~ |
re |
mi4 do re re, |
sol,2~ sol,8\noBeam sol-\sug\ff fad mi |
sol,2 r |
r r4 sol,\p |
re2 r |
r2 r4 re-\sug\p |
sol2 r |
r r4 sib,\mf |
fa2 r |
r r4 fa\f |
sib2 r |
sib,8\ff re fa re sib, mib sol mib |
sib,2 r |
sib,8 re fa re sib, mib sol mib |
sib,2 r4 <>^"Violoncelli" si4-\sug\f( |
do'2.-\sug\p) la4\f( |
sib!2.)\p sol4( |
mib re do) fa |
sib,2 r4 <>^"tutti" si\f |
do'2.\p la4\f |
sib2.\p re4\f |
mib fa sol la |
sib2 sol |
mib fa |
sib, fa |
sib, fa |
sib, r |
r r4 sib,\p |
fa2 fa:8\f |
fa:8 fa:8 |
sib4 r\fermata r2 |
r r4 sol,\p |
re2 r |
r r4 re |
sol2 mib4\f mib |
fa2 re |
mib r |
R1 |
r2 do8\ff re mib do |
re re' re re' re re' re re' |
re re' re re' re re' re re' |
re re' re re' re re' re re' |
re2 r |
sol,8\ff sib, re sib, sol, do mib do |
sol,2 r |
sol,8 sib, re sib, sol, do mib do |
sol,2. <>^"Violoncelli" si4(\f |
do'2.)\p dod'4(\f |
re'2)\p r |
R1 |
r2 r4 <>^"tutti" si,\f |
do2.\p dod4\f |
re2.\p re4\f |
mi fad sol la |
sol sib fad re |
sol sib fad re |
sol sol sol, sol, |
do do re re |
sol sib re' sib |
sol sib re' sib |
sol2
