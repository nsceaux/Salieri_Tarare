\clef "treble" r2 |
r si''4.(\sf la''16 sol'') |
fad''4( sol'' mi'' sol'') |
re''8 r \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4-! mi''4.(\sf fad''16 sol'') |
    re''8 r re''4-! mi''4.(\sf fad''16 sol'') |
    re''4 }
  { si'4 do''2-\sug\sf |
    si' do''-\sug\sf |
    si'4 }
>> re''4 mi''8( re'') do''-! si'-! |
la'( sol') fad'-! mi'-! re'4 r |
r2 si''4.(\sf la''16 sol'') |
fad''4( sol'' mi'' sol'') |
re''8 r \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4-! mi''4.(\sf fad''16 sol'') |
    re''8 r re''4-! mi''4.(\sf fad''16 sol'') |
    re''4 mi''8 do'' si'4 la' | }
  { si'4 do''2-\sug\sf |
    si' do''-\sug\sf |
    si'4 do''8 la' sol'4 fad' | }
>> 
sol'2 r |
r \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4.\trill\f si'16 la' |
    si'8 re'' mi'' fad'' sol'' si'' la'' sol'' |
    fad''8 sol'' fad'' mi'' re''4 }
  { fad'2-\sug\f |
    sol'~ sol'8 re'' do'' si' |
    la' si' la' sol' fad'4 }
>> r4 |
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4.\trill\f si'16 la' |
    si'8 re''[ mi'' fad''] sol''2~ |
    sol''8 mi'' fad'' sol'' la'' sol'' fad'' mi'' |
    fad''\p la'' sol'' fad'' mi'' sol'' fad'' mi'' |
    re''( sol'') si''-! re''-! do''( fad'') la''-! do''-! |
    si'4 r r2 |
    r4 la''8 sol'' fad'' mi'' re'' do'' |
    si'4 mi''8 do'' si'4 la' | }
  { fad'2-\sug\f |
    sol'4 r r2 |
    R1*5 |
    r4 sol' sol' fad' | }
>>
sol'2 r |
sol' r |
r r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 | \grace { mib''16[ re'' do''] } re''2 }
  { sib'4 | la'2 }
  { s4\p }
>> r2 |
r r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 | \grace { mib''16[ re'' do''] } re''2 }
  { la'4 | sib'2 }
  { s4\p }
>> r2 |
r2 r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''4 | \grace { sol''16 fa'' mi'' } fa''2 }
  { re''4 | do''2 }
  { s4\mf }
>> r2 |
r r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''4 | \grace { sol''16 fa'' mib'' } fa''2 }
  { do''4 | re''2 }
  { s4\f }
>> r2 |
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''2 sol'' | fa'' }
  { re''2 mib'' | re'' }
>> r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''2 sol'' | fa'' }
  { re''2 mib'' | re'' }
>> r2 |
R1*3 |
r2 r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4 |
    mib''2. fa''4 |
    re''2. sib''4 |
    sol''( la'' sib'' do''') |
    re'''2 sib'' |
    do''' la'' |
    sib''4 fa''2 fa''4 |
    fa'' fa''2 fa''4 |
    fa''2 }
  { re''4 |
    sol'2. do''4 |
    fa'2. sib'4 |
    sol'( la' sib' do'') |
    re''2 re'' |
    sol'' do'' |
    re'' do''8 re'' mib'' do'' |
    re'' mib'' fa'' re'' do'' re'' mib'' do'' |
    re''2 }
  { s4-\sug\f | s2.-\sug\p s4-\sug\f | s2.-\sug\p s4-\sug\f }
>> r2 |
r r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''4 |
    fa''2\trill fa''8 sol'' la'' sib'' |
    do'''2. do''4 |
    re'' }
  { re''4 |
    do''2 la'8 sib' do'' re'' |
    mib''4 do'' la'2 |
    sib'4 }
  { s4\p | s2 s\f }
>> r4\fermata r2 |
r r4 <>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 | re''2\trill }
  { sib'4 | la'2 }
>> r2 |
r r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 |
    re''2\trill sol''4 sol'' |
    lab''2 fa'' |
    sol'' }
  { la'4 |
    sib'2 sib'4 sib' |
    sib'2 sib' |
    sib' }
  { s4 | s2 s\f }
>> r2 |
R1 |
r2 <>\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4 sol'' |
    fad''2 sib''4 sib'' |
    la''2 sol''4 sol'' |
    fad''2 sib''4 sib'' |
    la''2 }
  { mib''4 mib'' |
    la'2 sol'4 sol' |
    fad'2 sib'4 sib' |
    la'2 sol'4 sol' |
    fad'2 }
>> r |
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2 mib'' | re'' }
  { sib'2 do'' | sib' }
>> r |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2 mib'' | re'' }
  { sib'2 do'' | sib' }
>> r |
R1*3 |
r2 r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''4 |
    mi''2. sol''4 |
    fad''2. fad''4 |
    sol''( la'' sib'' do''') |
    sib'' sol'' la'' do''' |
    sib'' sol'' la'' do''' |
    sib''2 sol'' |
    la'' fad'' | }
  { re''4 |
    sol'2. mi''4 |
    la'2. re''4 |
    re'''1~ |
    re'''~ |
    re'''~ |
    re''' |
    mib''2 la' | }
  { s4-\sug\f | s2.-\sug\p s4-\sug\f | s2.-\sug\p s4-\sug\f }
>>
sol'8 la' sib' do'' re'' do'' sib' la' |
sol' la' sib' do'' re'' do'' sib' la' |
sol'2
