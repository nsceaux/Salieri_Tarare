\tag #'all \key sol \major
\tempo "Allegro" \midiTempo#160
\time 2/2 \partial 2 s2 s1*12
\bar ".|:" s1*11 \alternatives s1 { \tag #'all \key sol \minor s2 }
\bar ".|:" \grace s8 s2 s1*23 s2 \bar ":|.|:"
\grace s8 s2 s1*33 s2 \bar ":|."
