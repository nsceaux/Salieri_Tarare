\clef "treble" \transposition sol
r2 |
<>-\sug\ff \twoVoices #'(corno1 corno2 corni) <<
  { do''1~ | do''~ | do''~ | do''~ | do''2 }
  { do'1~ | do'~ | do'~ | do'~ | do'2 }
>> r2 |
R1 |
<>-\sug\ff \twoVoices #'(corno1 corno2 corni) <<
  { do''1~ | do''~ | do''~ | do'' | do''2 mi''4 re'' | do''2 }
  { do'1~ | do'~ | do'~ | do' | do'2 do''4 sol' | mi'2 }
>> r2 |
sol'1-\sug\f~ |
sol'~ |
sol'2 r |
sol'1-\sug\f~ |
sol'~ |
sol'~ |
sol'\p~ |
sol'~ |
sol' |
sol' |
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 mi''4 re'' | do''2 }
  { do''2 do''4 sol' | do'2 }
>> r2 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 }
  { do' }
>> r2 |
R1*23 |
r2 r |
R1*33 |
r2
