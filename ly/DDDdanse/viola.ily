\clef "alto" r8 re\ff-! mi-! fad-! |
sol2 re'4.(-\sug\sf do'16 si) |
la4( si do'2) |
si( do')-\sug\sf |
si( do')-\sug\sf |
si4 sol do'2 |
re'~ re'8\noBeam re-\sug\ff-! mi-! fad-! |
sol2 re'4.(-\sug\sf do'16 si) |
la4( si do'2) |
si( do')-\sug\sf |
si( do')-\sug\sf |
si4 do' re' do' |
si2~ si8\noBeam sol-\sug\ff-! fad-! mi-! |
re2 fad |
sol~ sol8 re' do' si |
la si la sol fad\noBeam la\ff-! si-! dod'-! |
re'2 fad |
sol~ sol8\noBeam sol-! la-! si-! |
do'2 dod' |
re'\p do'! |
si4 sol la fad |
sol1 |
la8 sol fad sol la sol fad la |
sol2 sol4 fad |
sol2~ sol8\noBeam sol\ff fad mi |
sol2 r |
r r4 sol'-\sug\p |
fad'2 r |
r2 r4 fad'-\sug\p |
sol'2 r |
r r4 sib'-\sug\mf |
la'2 r |
r r4 la'-\sug\f |
sib'8 sib do' re' mib' fa' sol' la' |
sib'-\sug\ff <sib fa'> q q <sib sol'>2:8 |
<sib fa'>8\noBeam sib do' re' mib' fa' sol' la' |
sib' <sib fa'> q q <sib sol'>2:8 |
<sib fa'>2. si4-\sug\f( |
do'2.-\sug\p) la4-\sug\f( |
sib!2.-\sug\p) sol'4( |
mib' re' do') fa |
sib2 r4 re''-\sug\f |
sol'2.-\sug\p do''4-\sug\f |
fa'2.-\sug\p re4-\sug\f |
mib fa sol la |
sib2:8 re':8 |
sol':8 do':8 |
re'8 fa' fa' fa' fa'2:8 |
fa':8 fa':8 |
fa'2 r |
r r4 sib'-\sug\p |
la'2 la8-\sug\f sib do' re' |
mib'4 do' la2 |
sib4 r\fermata r2 |
r r4 sol'-\sug\p |
fad'2 r |
r r4 fad' |
sol'2 mib'4-\sug\f mib' |
fa'2 re' |
mib'2 r |
R1 |
r2 do'8\ff re' mib' do' |
re' re'' re' re'' re' re'' re' re'' |
re' re'' re' re'' re' re'' re' re'' |
re' re'' re' re'' re' re'' re' re'' |
re'2 r |
<sol sol'>2:8 q:8 |
q8\noBeam sol la sib do' re' mi' fad' |
sol'2:8 sol':8 |
sol'2. \slurDashed si4(-\sug\f |
do'2.)-\sug\p dod'4(-\sug\f |
re'2.)-\sug\p fad'4 | \slurSolid
sol'( sib' do'' la') |
sib'8( la' sib' la') sib'4 si-\sug\f |
do'2.-\sug\p dod'4-\sug\f |
re'2.-\sug\p re'4-\sug\f |
mi' fad' sol' la' |
sol' sib' fad' re' |
sol' sib' fad' re' |
sol' sol' sol sol |
do' do' re' re' |
sol'8 la' sib' do'' re'' do'' sib' la' |
sol' la' sib' do'' re'' do'' sib' la' |
sol'2
