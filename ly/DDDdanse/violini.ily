\clef "treble" r8 re'-!\ff mi'-! fad'-! |
sol'2 <<
  \tag #'violino1 {
    <si' si''>4.\sf( la''16 sol'') |
    fad''4( sol'' mi'' sol'') |
    re''8 r re''4-! mi''4.(\sf fad''16 sol'') |
    re''8 r re''4-! mi''4.(\sf fad''16 sol'') |
    re''4 re'' mi''8( re'') do''-! si'-! |
    la'( sol') fad'-! mi'-! re'
  }
  \tag #'violino2 {
    re'4.(\sf do'16 si) |
    la4( si do'2) |
    si2( do')\sf |
    si2( do')\sf |
    si4 sol' do''8( si') la'-! sol'-! |
    fad'( mi') re'-! dod'-! re'
  }
>> re'[-!\ff mi'-! fad'-!] |
sol'2 <<
  \tag #'violino1 {
    <si' si''>4.\sf( la''16 sol'') |
    fad''4( sol'' mi'' sol'') |
    re''8 r re''4-! mi''4.(\sf fad''16 sol'') |
    re''8 r re''4-! mi''4.(\sf fad''16 sol'') |
    re''4 mi''8 do'' si'4 la' |
  }
  \tag #'violino2 {
    re'4.(\sf do'16 si) |
    la4( si do'2) |
    si2( do')\sf |
    si2( do')\sf |
    si4 do''8 la' sol'4 fad' |
  }
>>
sol'4.\trill fad'16 sol' <<
  \tag #'violino1 {
    sol4 r |
    r2 do''4.\trill\f si'16 la' |
    si'8 re''-! mi''-! fad''-! sol''-! si''-! la''-! sol''-! |
    fad''-! sol''-! fad''-! mi''-! re''4 r |
    r2 do''4.\trill\f si'16 la' |
    si'8 re''[ mi'' fad''] sol''2~ |
    sol''8 mi'' fad'' sol'' la'' sol'' fad'' mi'' |
    fad''\p la'' sol'' fad'' mi'' sol'' fad'' mi'' |
    re''( sol'') si''-! re''-! do''( fad'') la''-! do''-! |
  }
  \tag #'violino2 {
    sol8 sol'-\sug\ff[-! fad'-! mi'-!] |
    re'1~ |
    re' |
    re'2~ re'8 la[-!\ff si-! dod'-!] |
    re'1~ |
    re'2~ re'8 sol-![ la-! si-!] |
    do'2 dod' |
    re'-\sug\p do'! |
    si4 sol la do'' |
  }
>>
si'8( re'') mi''-! re''-! do''-! si'-! la'-! sol'-! |
fad' sol' la' sol' fad' mi' re' do' |
si sol' mi' do' si4 la8 sol16 la |
<<
  \tag #'violino1 {
    sol2 r |
  }
  \tag #'violino2 {
    sol2~ sol8 sol'[\ff fad' mi'] |
  }
>>
sol2
%% mineur
\grace la'8 sol'\p fad' sol' la' |
sib' sol' sib' sol' re' sol' sib' sol' |
la' fad' re' fad' la' sib' la' sib' |
do'' la' do'' la' re' la' do'' la' |
sib' sol' re' sol' \grace do'' sib'\cresc la' sib' do'' |
re'' sib' re'' sib' fa'! sib' re'' sib' |
do'' la' fa' la' \grace re'' do'' sib' do'' re'' |
mib'' do'' mib'' do'' la' do'' mib'' do'' |
re'' sib' do'' re'' mib'' fa'' sol'' la'' |
<<
  \tag #'violino1 {
    sib''2\ff <mib' sib' sol''> |
    <re' sib' fa''>8\noBeam
  }
  \tag #'violino2 {
    sib''8-\sug\ff <re' sib'>8 q q <mib' sib'>2:8 |
    <re' sib'>8\noBeam
  }
>> sib'8 do'' re'' mib'' fa'' sol'' la'' |
<<
  \tag #'violino1 {
    sib''2 <mib' sib' sol''> |
    <re' sib' fa''>2. sol''4(\f |
    fa''8\p mib'' re'' mib'') mib''4 fa''(\f |
    mib''8\p re'' do'' re'' re''4) mib'' |
    \grace re''8 do''4 sib' la'8 sib' do'' la' |
    sib' do'' re'' mib'' fa''4
  }
  \tag #'violino2 {
    sib''8 <re' sib'> q q <mib' sib'>2:8 |
    <re' sib'>2. re''4(\f |
    sol'2.)\p do''4-\sug\f( |
    fa'2.-\sug\p) sib'4( |
    sol' fa' mib'2) |
    re' r4
  }
>> sol''4\f |
mib''2.:8\p fa''8\f fa'' |
re''2.:8\p sib''8\f sib'' |
sol'' sol'' la'' la'' sib'' sib'' do''' do''' |
re'''2:8 <<
  \tag #'violino1 {
    sib''2:8 |
    do''':8 la'':8 |
    sib''8 do''' re''' sib'' la'' sib'' do''' la'' |
    sib'' do''' re''' sib'' la'' sib'' do''' la'' |
    sib''2
  }
  \tag #'violino2 {
    re''2:8 |
    sol'':8 do'':8 |
    re''8 mib'' fa'' re'' do'' re'' mib'' do'' |
    re'' mib'' fa'' re'' do'' re'' mib'' do'' |
    re''2
  }
>> \grace do''8 sib'\p la' sib' do'' |
re'' sib' re'' sib' fa' sib' re'' sib' |
do''4 r fa''8\f sol'' la'' sib'' |
do''' sib'' la'' sol'' fa'' mib'' re'' do'' |
re''4 r\fermata \grace la'8 sol'\p fad' sol' la' |
sib' sol' sib' sol' re' sol' sib' sol' |
la' fad' re' fad' la' sib' la' sib' |
do'' la' do'' la' re' la' do'' la' |
sib'4 r <mib' sib' sol''>4\f q |
<re' sib' lab''>2 \grace sol''8 fa''4 mib''8 fa'' |
sol''2 <<
  \tag #'violino1 {
    sol''4\p sol'' |
    si'2 re''4 mib''8 fa'' |
    mib''2
  }
  \tag #'violino2 {
    mib'8-\sug\p sol' mib' sol' |
    re' fa' re' fa' si sol' si sol' |
    do'2
  }
>> <mib'' sol''>4\ff q |
<re' la' fad''>2 <<
  \tag #'violino1 {
    <re'' sib''>4 q |
    <re'' la''>2
  }
  \tag #'violino2 {
    <re' sib' sol''>4 q |
    <re' la' fad''>2
  }
>> <re' sib' sol''>4 q |
<re' la' fad''>2 <<
  \tag #'violino1 {
    <re' sib' sib''>4 q |
    <re' la' la''>2 r8 re''\ff mi'' fad'' |
    sol''2 <do' sol' mib''>2 |
    <sib sol' re''>8\noBeam
  }
  \tag #'violino2 {
    <re' sib' sol''>4 q |
    <re' la' fad''>2 r8 re'-\sug\ff mi' fad' |
    sol' <re' sib> q q <do' mib'> q q q |
    <sib re'>\noBeam
  }
>> sol'8 la' sib' do'' re'' mi'' fad'' |
<<
  \tag #'violino1 {
    sol''2 <mib'' sol' do'> |
    <sib sol' re''>2. fa''4(\f |
    fa''8\p mi'' re'' mi'') mi''4 sol''(\f |
    sol''8\p fad'' mi'' fad'') fad''4 la''( |
    sib'' sol'' la'' fad'') |
    sol''8( fad'' sol'' fad'') sol''4 fa''\f |
    mi''8\p mi'' mi'' mi'' mi'' mi'' sol''\f sol'' |
    <la' fad''>2:8\p q4 fad''8\f fad'' |
    sol'' sol'' la'' la'' sib'' sib'' do''' do''' |
    sib'' sib'' sol'' sol'' la'' la'' do''' do''' |
    sib'' sib'' sol'' sol'' la'' la'' do''' do''' |
    sib''2:8 sol'':8 |
    la'':8 fad'':8 |
  }
  \tag #'violino2 {
    sol''8 <re' sib> q q <do' mib'>2:8 |
    <sib re'>2. re''4(\f |
    sol'2.)\p mi''4(\f |
    la'2.)\p re''4 |
    re''1~ |
    re''2. re''4-\sug\f |
    sol'2.-\sug\p mi''4-\sug\f |
    la'2.-\sug\p <re'' re'''>8-\sug\f q |
    q2:8 q:8 |
    q:8 q:8 |
    q:8 q:8 |
    q8 re'' re'' re'' sib'2:8 |
    mib'':8 <la' fad''>:8 |
  }
>>
sol''8 la'' sib'' do''' re''' do''' sib'' la'' |
sol'' la'' sib'' do''' re''' do''' sib'' la'' |
sol''2
