\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Trompettes \small en Mi♭ }
      \haraKiri
    } <<
      \keepWithTag #'() \global \keepWithTag #'trombe \includeNotes "trombe"
      { s2 s1*24 s2 \break \grace s8 <>^\markup Trompettes }
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Cors \small en Sol }
      \haraKiri
    } <<
      \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni"
    >>
  >>
  \layout { indent = \largeindent }
}
