\clef "treble" \transposition mib
r2 |
R1*24 |
r2 r |
\noHaraKiri R1*8 |
<>-\sug\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { sol''2 mi'' | re'' }
  { sol'2 do'' | sol' }
>> r |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { re''2 mi'' | re'' }
  { sol' do'' | sol' }
>> r |
R1*3 |
r2 r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''4 | mi''2. re''4 | re''2 }
  { mi''4 | mi''2. re''4 | re''2 }
  { s4\f s2.\p s4\f | s2\p }
>> r2 |
R1 |
r2 <>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'2 |
    do'' re'' |
    sol'' re'' |
    sol'' re'' |
    sol'' }
  { sol'2 |
    do'' re'' |
    sol' re'' |
    sol' re'' |
    sol' }
>> r2 |
R1*3 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { re''4 }
  { sol' }
>> r4\fermata r2 |
R1*3 |
r2 <>\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''4 mi'' | re''1 | mi''2 }
  { do''4 do'' | re''2 sol' | do'' }
>> r2 |
R1*6 |
<>-\sug\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2 mi'' | mi'' }
  { mi'2 mi' | mi' }
>> r2 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2 mi'' | mi'' }
  { mi'2 mi' | mi' }
>> r2 |
R1*3 |
r2 r4 mi''-\sug\f |
mi''2-\sug\p r |
R1*6 |
<>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2 }
  { mi' }
>> r2 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2 }
  { mi' }
>> r2 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2 }
  { mi' }
>>
