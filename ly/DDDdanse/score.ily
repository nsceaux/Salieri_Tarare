\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \oboiInstr } <<
        \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \small en Mi♭ }
        shortInstrumentName = "Tr."
        \haraKiri
      } <<
        \keepWithTag #'() \global \keepWithTag #'trombe \includeNotes "trombe"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small en Sol }
        shortInstrumentName = "Cor."
        \haraKiri
      } <<
        \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      \new Staff \with { \bassoInstr } <<
        \global \includeNotes "basso"
        \origLayout {
          s2 s1*6\break s1*8\pageBreak
          s1*6\break s1*7\pageBreak
          s1*7\break s1*8\pageBreak
          s1*8\break s1*8\pageBreak
          s1*8\break s1*8\pageBreak
        }
        \modVersion {
          s2 s1*24 s2 \break \grace s8
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
