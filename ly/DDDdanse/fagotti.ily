\clef "bass" r8 re\ff mi fad |
sol2 re'4.\sf do'16 si |
la4( si do'2) |
si( do')\sf |
si( do')\sf |
si4 sol do do' |
re2~ re8\noBeam re\ff-! mi-! fad-! |
sol2 re'4.(\sf do'16 si) |
la4 si do'2 |
si( do')\sf |
si( do')\sf |
si4 do' re' do' |
si2~ si8\noBeam sol-\sug\ff-! fad-! mi-! |
re2 fad |
sol~ sol8 re' do' si |
la si la sol fad\noBeam la,\ff-! si,-! dod-! |
re2 fad |
sol~ sol8\noBeam sol-! la-! si-! |
do'2 dod' |
re'-\sug\p do'! |
si4 sol la fad |
sol1 |
la8 sol fad sol la sol fad la |
sol2 sol4 fad |
sol2~ sol8\noBeam sol\ff fad mi |
sol2 r |
R1*8 |
sib,8\ff re fa re sib, mib sol mib |
sib,2 r |
sib,8 re fa re sib, mib sol mib |
sib,2 r |
R1*3 |
r2 r4 \sugNotes {
  si4-\sug\f |
  do'2.-\sug\p la4-\sug\f |
}
sib2.-\sug\p re4\f |
mib fa sol la |
sib2 sol |
mib fa |
sib, fa |
sib, fa |
sib, r |
R1 |
r2 la8-\sug\f sib do' re' |
mib'4 do' la2 |
sib4 r\fermata r2 |
R1*3 |
r2 mib4-\sug\f mib |
fa2 re |
mib r |
R1 |
r2 do8\ff re mib do |
re1~ |
re~ |
re~ |
re2 r |
sol,8\ff sib, re sib, sol, do mib do |
sol,2 r |
sol,8 sib, re sib, sol, do mib do |
sol,2 r |
R1*3 |
r2 r4 si,\f |
do2.\p dod4\f |
re2.-\sug\p re4-\sug\f |
mi fad sol la |
sol sib fad re |
sol sib fad re |
sol sol sol, sol, |
do do re re |
sol sib re' sib |
sol sib re' sib |
sol2
