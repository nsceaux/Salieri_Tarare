As -- ta -- sie est u -- ne dé -- es -- se,
est u -- ne dé -- es -- se.
Dans mon cœur sou -- vent com -- bat -- tu,
sa voix sen -- si -- ble, en -- chan -- te -- res -- se,
fai -- sait tri -- om -- pher la ver -- tu.
D’une ar -- deur tou -- jours re -- nais -- san -- te,
j’of -- frais sans cesse à sa beau -- té,
sans cesse à sa beau -- té tou -- chan -- te,
l’en -- cens pur de la vo -- lup -- té,
l’en -- cens pur de la vo -- lup -- té.
El -- le te -- nait mon âme ac -- ti -- ve
jus -- que dans le sein du re -- pos :
Ah ! faut- il que ma voix plain -- ti -- ve
en vain, en vain la de -- mande aux é -- chos,
en vain la de -- mande aux é -- chos ?
