\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso)
   (fagotti #:system-count 4)
   (clarinetti #:score-template "score-clarinetti")
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#}))
