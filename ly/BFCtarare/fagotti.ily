\clef "bass" r4 |
R2.*4 |
do'2. |
do' |
do'4. mi8([\< si, do])\! |
re4. si,8([\< do re])\! |
mi4~ mi8. mi16-\sug\f mi8. mi16 |
<>^\markup\tiny { Source : \italic sol }
fa8.-\sug\ff fa16 fa8. fa16 fa8. fa16 |
la4( sol8.) sol,16 sol,8. sol,16 |
sol,2 r4 |
R2.*3 |
\clef "tenor" r4 r8 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { si4 re'8 | mi'2 do'4~ | do'\fermata si }
  { sol4 si8 | do'2 la4~ | la\fermata sol }
  { s4.-\sug\cresc | s2. | s4-\sug\f }
>> r4 |
R2. |
\clef "bass" r4 r si,-\sug\f |
do re re, |
sol,8. sol,16-\sug\f sol,4 r |
\clef "tenor" r8 <>-\sug\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'4 re' re'8 | }
  { si4 si si8 | }
>>
r8 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mi'4 mi' mi'8 | }
  { do'4 do' do'8 | }
>>
R2.*2 |
<>-\sug\mf \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mi'2. | re' | }
  { do'2. | re' | }
  { s2. | s2.-\sug\cresc <>\! }
>>
R2.*2 |
r4 r <>-\sug\sf \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'8. sol'16 | sol'4 fad' fa' | mi'2 }
  { si4 | do'2 re'4 | do'2 }
  { s4 | s2 s4-\sug\p }
>> r4 |
\clef "bass" sol2. |
do4 r r |
