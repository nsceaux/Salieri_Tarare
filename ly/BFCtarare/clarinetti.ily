\clef "treble" r4 |
R2.*4 |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { do''2 mi''4 |
    fa''2. |
    mi''4. do''8([\< re'' mi''])\! |
    fa''4. re''8([\< mi'' fa''])\! |
    sol''2 do''4 |
    la''2 fa''8. re''16 |
    do''4( si'8.) sol''16 sol''8. sol'16 |
    sol'2 }
  { mi'2 sol'4 |
    la'2. |
    sol'4. r8 r4 |
    R2. |
    r4 r do'' |
    do''2 la'8. fa'16 |
    mi'4( re'8.) si'16 si'8. sol'16 |
    sol'2 }
  { s2.*4 | s2 s4-\sug\f | s2.-\sug\ff }
>> r4 |
R2.*3 |
r4 r8 \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { si'4 re''8 |
    mi''4~ mi''16 re'' do'' si' do'' re'' mi'' fad'' |
    fad''8.\fermata sol''16 re''4 re''( |
    mi'') }
  { sol'4 si'8 |
    do''4~ do''16 si' do'' si' do'' re'' mi'' do'' |
    do''4\fermata si' sol'~ |
    sol' }
  { s4.-\sug\cresc | s2. | s2-\sug\f s4-\sug\p }
>> r4 r |
r4 r \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { re''16( mi'' fad'' sol'' |
    fad'' mi'' fad'' sol'') si'4. la'8 |
    sol'8. si''16 sol''4 }
  { sol'4~ |
    sol' sol'4. fad'8 |
    sol'8. si'16 si'4 }
  { s4-\sug\f | s2. | s8. s16-\sug\f }
>> r4 |
<>-\sug\p \ru#3 {
  r16
  \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
    { sol''16 sol'' sol'' }
    { si'16 si' si' }
  >>
}
\ru#3 {
  r16
  \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
    { sol''16 sol'' sol'' }
    { do''16 do'' do'' }
  >>
}
<>-\sug\pp \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { re''2. | mi''2 }
  { re''2. | mi''2 }
>> r4 |
r8 <>-\sug\mf \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { la'4 si'16( do'' re'' mi'' fa'' re'') | do''8. si'16 si'4 r | }
  { do'4 la' la'8 | re'2. | }
  { s8 s2 | s2.-\sug\cresc <>\! }
>>
R2.*2 |
r4 r <>\sf \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { re''8. sol''16 |
    sol''4( fad'' fa'') |
    mi''4. sol''16 mi'' la'' fa'' mi'' re'' |
    do''2 si'4 |
    do'' }
  { si'4 |
    do''2( re''4) |
    sol'2 fa'4 |
    mi'2 re'4 |
    mi'4 }
  { s4 | s2 s4-\sug\p }
>> r4 r |
