\clef "treble"
<<
  \tag #'violino1 {
    do''8.\p re''16 |
    mi''8 mi''4 mi''8 re''16( mi'' fa'' re'') |
    do''8. si'16 si'8 si' si' si' |
    r sol''4\fp mi''8 re'' mi''16 fa'' |
    do''2( si'4) |
    do''2 do'''8. si''16 |
    la''4. la''8 \grace re''' do''' si''16 la'' |
    sol''4. do''8([\< re'' mi''])\! |
    fa''8.( re''16) si'8 re''([\< mi'' fa''])\! |
    sol''8. mi''16 do''8. do''16\f do''8. do''16 |
    <fa' do'' la''>2\ff \grace sol''8 fa'' mi''16 re'' |
    do''4( si'8.) sol''16 sol''8. <sol' si>16 |
    q2 si'8.\p do''16 |
    re''8 re''4 re''8~ re''16 sol'' fad'' mi'' |
    re''8([ do'']) do'' do''( re'' mi'') |
    mi'' re''4 do''16 si' la'8 re'' |
    do''4( si'16)\cresc sol' si' sol' si' sol' re'' si' |
    mi''8. mi''16 mi''16 re'' do'' si' do'' re'' mi'' fad'' |
    fad''8.\f\fermata sol''16 re''4 \tuplet 3/2 { sol''8\p re'' si' } |
    \tuplet 3/2 { mi'' do'' la' } sol'4. fad'8 |
  }
  \tag #'violino2 {
    mi'8.-\sug\p fa'16 |
    sol'8 sol'4 sol'8 la'8. fa'16 |
    mi'8. re'16 re'8 re' re' re' |
    r sol'4-\sug\fp sol'8 la'8. fa'16 |
    mi'( sol' mi' sol' mi' sol' mi' sol' fa' sol' fa' sol') |
    mi' sol' do'' sol' mi' sol' do'' sol' mi' sol' do'' sol' |
    fa' la' do'' la' fa' la' do'' la' fa' la' do'' la' |
    mi' sol' do'' sol' mi'8 do'([\< re' mi'])\! |
    fa'4. re'8([\< mi' fa'])\! |
    sol'4~ sol'8. sol'16-\sug\f sol'8. sol'16 |
    la'8.-\sug\ff <fa' do''>16 q8. q16 la'8. fa'16 |
    mi'4( re'8.) <re' si'>16 q8. <si re'>16 |
    q2 sol'8.-\sug\p la'16 |
    si'8 si'4 si' si'8 |
    mi'2~ mi'8 si' |
    la' la'4 re' re'8 |
    re'4~ re'16-\sug\cresc si re' si re' si si sol |
    do'8. do'16 do' re' mi' re' mi' re' do' do'' |
    do''4-\sug\f\fermata si' sol'-\sug\p |
    \tuplet 3/2 { sol'8 mi' do' } si4. la8 |
  }
>>
r16 sol(\f si re' sol' re' sol' si') <<
  \tag #'violino1 {
     re''16( mi'' fad'' sol'' |
     fad'' mi'' fad'' sol'') si'4. la'8 |
     sol'8. <si' si''>16\f <si' sol''>4 r |
     r8 si'4\p fa''!8( re'' si') |
     si'8. do''16 do''8 sol'16. sol'32 do''8( mi'') |
     re''(\pp sol'' re'' si' sol' fa'!) |
     \grace fa' mi'2 do''8.\mf si'16 |
     la'8 la'4 si'16( do'' re'' mi'' fa'' re'') |
     do''8.\cresc si'16 si'8 re''4 sol''8~ |
     sol''8.\p fad''16 fad''8 fad'' r fa'' |
     mi''8 mi''4 mi''8 \grace sol'' fa'' mi''16 re'' |
     do''2 si'8.\sf sol''16 |
     sol''8. fad''16 fad''8 fad'' r fa''\p |
     mi'' mi''4( sol''16 mi'' la'' fa'' mi'' re'') |
     do''8 do''4 do''8 si'8. si'16 |
     do''4 r r |
  }
  \tag #'violino2 {
    re''8 sol'~ |
    sol'4 sol'4. fad'8 |
    sol'8. <re' si'>16-\sug\f q4 r |
    re'16(\p sol' si' sol') re'( sol' si' sol') re'( sol' si' sol') |
    sol'( mi' do' mi') sol'( mi' do' sol) sol( do') do'( mi') |
    sol2.-\sug\pp~ |
    sol2 sol'4-\sug\mf |
    la'16 do'8 la la la la' la'16 |
    r16 re'8-\sug\cresc re' si' si' si' si'16 |
    do''8-\sug\p do'4 do'8 re' re' |
    sol8 sol'4 sol'8 do'' la'16 fa' |
    mi'( sol' mi' sol' mi' sol' mi' sol' re' si re' si) |
    do'8 do''4 do''8 re''16(-\sug\p si' sol' sol') |
    sol'( mi' do' mi' sol' do'' mi'' do'') la'8. fa'16 |
    mi'( sol' do'' sol' mi' do'' sol' mi') re'( sol si re') |
    mi'4 r r |
  }
>>
