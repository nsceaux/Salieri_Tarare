\clef "tenor/G_8" <>^"Ardemment" ^\markup\italic chanté do'8. re'16 |
mi'4. mi'8 re'16[ mi'] fa' re' |
do'8.[ si16] si4 r8 sol |
sol'4.( mi'8) re' mi'16[ fa'] |
do'2(\melisma \grace mi'8) re'4\melismaEnd |
do'2 do'8 si |
la4. la8 \grace re' do' si16 la |
sol4. do'8 re' mi' |
fa'8.[ re'16] si8 re' mi' fa' |
sol'8.[ mi'16] do'8. do'16
\footnoteHere#'(0 . 0) \markup {
  Source : \score {
    \new Staff \withLyrics {
      \tinyQuote \clef "tenor" \time 3/4
      sol'8.[ mi'16] do'8. do'16 do'8. do'16 | la'2
    } \lyrics { -res -- se fai -- sait triom -- pher }
    \layout { \quoteLayout }
  }
}
do'8. do'32 do' |
la'2 \grace sol'8 fa' mi'16[ re'] |
\grace do'8 si2 r4 |
r r si8 do' |
re'4. re'8 re'16[ sol'] fad' mi' |
re'8[ do'] do' do' re' mi' |
\grace mi'8 re'4. do'16[ si] la8 re' |
si2 r8 re' |
mi' mi' mi'16[ re'] do'[ si] do'[ re'] mi'[ fad'] |
fad'8.[\fermata sol'16] re'4 \tupletUp\tuplet 3/2 { sol'8[ re'] si } |
\tuplet 3/2 { mi' do' la } sol4. la8 |
\grace la8 si2 re'16[ mi'] fad'[ sol'] |
fad'[ mi'] fad' sol' si4. la8 |
sol2 re'8 re'16 do' |
si4. fa'8 \grace mi' re' do'16[ si] |
si8[ re'16 do'] do'8 sol16. sol32 do'8 mi' |
re'4.( si8) sol8. fa16 |
mi2 <>^"Serré" do'8. si16 |
la4. si16 do' re'[ mi'] fa'[ re'] |
do'8[ si] si4 r8 sol' |
sol'4( fad'4.) fa'8 |
mi'2 \grace sol'8 fa' mi'16[ re'] |
do'2 si8. sol'16 |
\grace sol'8 fad'2 r8 fa' |
mi'2 la'16[ fa'] mi'[ re'] |
do'2 re'8. do'16 |
do'2 r4 |
