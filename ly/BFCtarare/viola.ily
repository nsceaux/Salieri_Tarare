\clef "alto" r4 |
do'4-\sug\p do' fa |
sol sol fa |
mi-\sug\fp mi fa |
sol sol sol |
do' do' do' |
do' do' do' |
do' do'8 mi'([\< si do'])\! |
re'4. si8([\< do' re'])\! |
mi'4~ mi'8. mi'16 mi'8.-\sug\f mi'16 |
fa'8.-\sug\ff fa16 fa8. fa16 fa8. fa16 |
sol4~ sol8. sol16 sol8. sol16 |
sol2 r4 |
sol4-\sug\p sol sol |
la la4. sol8 |
fad4 fad fad |
sol8 sol sol-\sug\cresc sol sol sol |
sol2.:8 |
sol2-\sug\f\fermata si4-\sug\p |
do' re' re |
sol2 si4-\sug\f |
do' re'2 |
si8. sol16-\sug\f sol4 r |
si16(-\sug\p re' sol' re') si( re' sol' re') si( re' sol' re') |
sol( do' mi' do') sol( do' mi' do') sol( do') do'( mi') |
\once\tieDashed sol2.-\sug\pp~ |
sol2 mi4-\sug\mf |
fa16 fa8 fa fa fa fa fa16 |
sol-\sug\cresc sol8 sol sol sol sol sol16 |
la2-\sug\p( si4) |
do'2:8 la8 fa |
sol2.:8 |
la2( si4-\sug\p) |
do'2:8 fa8 fa |
sol <sol mi'>4 q8 <sol re'>8. q16 |
do'4 r r |
