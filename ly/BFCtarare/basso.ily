\clef "bass" r4 |
do\p do fa |
sol sol fa |
mi\fp mi fa |
sol sol sol, |
do do do |
do do do |
do4. mi8([\< si, do])\! |
re4. si,8([\< do re])\! |
mi4~ mi8. mi16\f mi8. mi16 |
fa8.\ff fa16 fa8. fa16 fa8. fa16 |
sol4~ sol8. sol,16 sol,8. sol,16 |
sol,2 r4 |
sol\p sol sol |
la la4. sol8 |
fad4 fad fad |
sol2.:8\cresc |
sol:8 |
sol2-\sug\f\fermata\! si,4\p |
do re re |
sol,2 si,4\f |
do re re |
sol8. sol,16-\sug\f sol,4 r |
fa!\p fa fa |
mi mi mi |
si,2.-\sug\pp |
do2 mi4\mf |
fa8 fa fa2:8 |
sol2.:8\cresc |
la2\p( si4) |
do'2:8 la8 fa |
sol2.:8 |
la2( si4)\p |
do'2:8 fa8 fa |
sol2:8 sol,8 sol, |
do4 r r |
