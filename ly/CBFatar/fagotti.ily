\clef "bass"
<>-\sug\ff \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sol1~ | sol~ | sol | sol~ |
    sol | sol( | la2) si | do'2. }
  { mi1~ | mi | re4 mi fa re | mi1~ |
    mi | mi( | fa2) fa | mi2. }
  { s1*3 s2 s-\sug\p }
>> r4 |
R1*3 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sib1 | sib2 sib | sib sib | do' dod' |
    re'4 re' sib sol | }
  { sol1 | sol2 sol | sol sol | sol sol |
    fad4 re' sib sol | }
  { s1 | s2-\sug\fp s-\sug\fp | s-\sug\fp s-\sug\fp | s1-\sug\f }
>>
re8 mi!16 fad sol la si dod' re' dod' re' dod' re' dod' re' dod' |
re'2 r |
sol,8\p sol sol sol sol2:8 |
sol:8 sol:8 |
fad:8 fa:8 |
mi8 do mi do mi do mi do |
fa2:8 fa:8 |
sol:8 sol,:8 |
do8\ff mi16 sol do'8 do' do' do' do' si16 la |
sold8\fp sold la la si si la la |
sold2:8 sold:8 |
la8\cresc la sold sold la la sold sold |
la4 mi' do' la |
mi4 mi\ff fa re |
do1*1/2 s2-\sug\p |
R1*6 |
r2 r8 sol,-\sug\f la, si, |
do4 do'2 si4 |
do'1*1/2 s2-\sug\p |
fa1-\sug\cresc |
sol |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sol1 | sol | sol | sol2 }
  { mi1 | mi | re4 mi fa re | mi2 }
  { s1-\sug\ff }
>> r2 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { do'1 |
    la2 sol |
    sol do' |
    fa sol |
    do4. do8 do4 do }
  { sol1 |
    fa2 sol |
    sol do' |
    fa sol |
    do4. do8 do4 do | }
>>
\custosNote do2
