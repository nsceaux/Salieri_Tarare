\clef "treble" \transposition do
\tag #'trombe <>^"à 2"-\sug\ff
\twoVoices#'(tromba1 tromba2 trombe) <<
  { do''4 do''8. do''16 do''4 do'' |
    sol'2. do'4 |
    sol' sol'8. sol'16 sol'4 sol' |
    do''2. }
  { mi'4 mi'8. mi'16 mi'4 mi' |
    mi'2. do'4 |
    sol' sol'8. sol'16 sol'4 sol' |
    mi'2. }
>> r4 |
r <>\p \twoVoices#'(tromba1 tromba2 trombe) <<
  { mi''8. mi''16 mi''4 mi'' | }
  { sol'8. sol'16 sol'4 sol' | }
>>
r4 \twoVoices#'(tromba1 tromba2 trombe) <<
  { mi''8. mi''16 mi''4 }
  { sol'8. sol'16 sol'4 }
>> r4 |
R1 |
r4 \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''8. do''16 do''4 }
  { mi'8. mi'16 mi'4 }
>> r4 |
R1 |
r8 \twoVoices#'(tromba1 tromba2 trombe) <<
  { re''16 re'' re''8 re'' re''2:8 | }
  { re''16 re'' re''8 re'' re''2:8 | }
>>
re''1 |
sol' |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { sol'2 sol' | sol' sol' | }
  { sol'2 sol' | sol' sol' | }
  { s2-\sug\fp s-\sug\fp | s-\sug\fp s-\sug\fp | }
>>
sol'1-\sug\f |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { re''4 re'' re'' re'' | }
  { re''4 re'' re'' re'' | }
>>
re''1 |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { re''2 }
  { re'' }
>> r2 |
R1*6 |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { do'8 mi'16 sol' do''8 do'' do''2:8 | mi''2 }
  { do'8 mi'16 sol' do''8 do'' do''2:8 | mi'2 }
  { s1-\sug\ff | s2-\sug\fp }
>> r2 |
r8 \twoVoices#'(tromba1 tromba2 trombe) <<
  { mi''16 mi'' mi''8 mi'' mi''2:8 | mi''1~ | mi'' | mi''4 }
  { mi'16 mi' mi'8 mi' mi'2:8 | mi'1~ | mi' | mi'4 }
  { s2.. | s1-\sug\cresc | s\! }
>> r4 r2 |
do'1*1/2-\sug\ff s2-\sug\p |
R1*5 |
r4 \twoVoices#'(tromba1 tromba2 trombe) <<
  { re''8. re''16 re''4 re'' | do''2 }
  { sol'8. sol'16 sol'4 sol' | mi'2 }
>> r2 |
r4 \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''8. do''16 do''4 re'' | mi'' do'' sol' mi' | }
  { mi'8. mi'16 mi'4 sol' | mi' do'' sol' mi' | }
  { s2.-\sug\f | s2 s-\sug\p }
>>
do'1-\sug\cresc |
sol |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { do''4 do''8. do''16 do''4 do'' |
    sol'2. do'4 |
    sol' sol'8. sol'16 sol'4 sol' |
    mi'4. do'8 mi'4 sol' | }
  { mi'4 mi'8. mi'16 mi'4 mi' |
    mi'2. do'4 |
    sol' sol'8. sol'16 sol'4 sol' |
    mi'4. do'8 mi'4 sol' | }
  { s1-\sug\ff }
>>
do''1 |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { do''2 sol' |
    mi' mi'' |
    re'' mi''4 re'' |
    do''4. do'8 do'4 do' |
    do'2 }
  { do''2 sol' |
    mi' do'' |
    do'' do''4 sol' |
    do''4. do'8 do'4 do' |
    do'2 }
>>
