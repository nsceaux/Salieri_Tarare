\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso #:system-count 5)
   (fagotti #:system-count 5
            #:music , #{
\quoteBasso "CBFbasso"
s1*31
<<
  { \mmRestDown s1*6 \mmRestCenter }
  \new CueVoice {
    <>^\markup Basso
    do2:8 si,:8 |
    do:8 do:8 |
    do:8 do:8 |
    do8 do' do' do' do'2:8 |
    fa:8 fa:8 |
    sol:8 sol,:8 |
    la,2~ la,8
  }
>>
#})
   (oboi #:score-template "score-oboi" #:system-count 6)
   (trombe #:score "score-tr-timp")
   (timpani #:score "score-tr-timp")
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #}))
