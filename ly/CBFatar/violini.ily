\clef "treble"
<<
  \tag #'violino1 {
    <sol mi' do''>4\ff <mi' do''>8. q16 q4 q |
    <sol sol'>2. sol'16 do'' mi'' sol'' |
    sol''4 sol''8. sol''16 sol''4 sol'' |
    <sol mi' do'' mi''>2. sol''8.\p sol''16 |
    do'''16-! do'''( si'' do''') re'''( do''' si'' do''') re'''( do''' si'' do''') re'''( do''' si'' do''') |
    sol''-! sol''( fad'' sol'') la''( sol'' fad'' sol'') la''( sol'' fad'' sol'') la''( sol'' fad'' sol'') |
    la''-! la''( sold'' la'' si'' la'' sold'' la'') si''-! si''( la'' si'' do''' si'' la'' si'') |
    do''' do''' si'' do''' re''' do''' si'' do''' re''' do''' si'' do''' mi''' fa''' re''' mi''' |
    do'''-\sug\fp do''' si'' do''' re''' do''' si'' do''' do''' re''' si'' do''' la'' si'' sol'' la'' |
    fad''16 fad'' mi'' fad'' sol'' fad'' mi'' fad'' la'' la'' sold'' la'' si'' la'' sold'' la'' |
    re''' la'' fad'' re'' re''' la'' fad'' re'' re''' la'' fad'' re'' re''' la'' fad'' re'' |
    sib'' sib'' la'' sib'' do''' sib'' la'' sib'' do''' sib'' la'' sib'' do''' sib'' la'' sib'' |
    mib'''\fp mib''' re''' mib''' fa''' mib''' re''' mib''' sib''\fp sib'' la'' sib'' do''' sib'' la'' sib'' |
    sol''\fp sol'' fa'' sol'' lab'' sol'' fa'' sol'' mib''8\fp mib'''16 mib''' mib'''8 re''' |
    do'''16\f do''' do''' do''' do'''4:16 dod'''2:16 |
    re'''16 re''' re''' re'''
  }
  \tag #'violino2 {
    <sol mi'>16-\sug\ff q q q q4:16 q2:16 |
    q2:16 q4:16 mi'16 sol' do'' mi'' |
    re'' sol' re'' sol' mi'' sol' mi'' sol' fa'' sol' fa'' sol' re'' sol' re'' sol' |
    \rt#4 { mi'' sol' } \rt#4 { mi''-\sug\p sol' } |
    \rt#4 { mi'' sol' } \rt#4 { mi'' sol' } |
    \rt#4 { mi'' sol' } \rt#4 { mi'' sol' } |
    fa''2:16 \rt#4 { re''16 fa'' }
    \rt#4 { mi'' sol' } mi'' sol' mi'' sol' sol'' sol' fa'' sol' |
    \rt#4 { mi''-\sug\fp sol' } mi''8 re'' do'' si' |
    la'16 la' la' la' la'4:16 fad''16 fad' fad' fad' fad'4:16 |
    fad'16 la' re'' la' fad' la' re'' la' fad' la' re'' la' fad' la' re'' la' |
    <sol' sol>2:16 q:16 |
    sib16-\sug\fp sol' sib sol' sib sol' sib sol' sib-\sug\fp sol' sib sol' sib sol' sib sol' |
    \rt#4 { sib-\sug\fp sol' } \rt#4 { sib-\sug\fp sol' } |
    <sib sol'>16-\sug\f sol'' sol'' sol'' sol''4:16 sol''2:16 |
    fad''16 re''' re''' re'''
  }
>> re'''4:16 sib'':16 sol'':16 |
re''8 mi''!16 fad'' sol'' la'' si'' dod''' re''' dod''' re''' dod''' re''' dod''' re''' dod''' |
re'''2 <<
  \tag #'violino1 {
    r8 re''\p mi''! fad'' |
    sol''4. sol''8 si''4. si''8 |
    re'''8. si''16 sol''8 sol'' sol'' sol'' sol'' sol'' |
    la''16 la'' sol'' la'' si'' la'' sol'' la'' si'' si'' la'' si'' do''' si'' la'' si'' |
    do'''8 do''16 do''' do'' do''' do'' do''' \rt#4 { do'' do''' } |
  }
  \tag #'violino2 {
    r2 |
    \rt#4 { re'16\p si'! } \rt#4 { re'16 si' } |
    \rt#4 { re'16 si' } \rt#4 { re'16 si' } |
    do''2:16 re'':16 |
    \ru#4 { sol'8:16 mi':16 } |
  }
>>
\rp#4 { la'16 do'' re'' do'' } |
\rp#2 { sol' do'' mi'' do'' } \rp#2 { sol' si' re'' si' } |
do''8\ff mi''16 sol'' do'''8 do''' do''' do''' <<
  \tag #'violino1 {
    do'''8 la'' |
    mi'''16\fp mi'' mi'' mi'' mi''4:16 mi''2:16 |
    mi'':16 mi''16 <mi''' mi''> q q q4:16 |
    q2:16\cresc q:16
    q:16 q:16\! |
    <mi'' mi'''>4
  }
  \tag #'violino2 {
    re''8 la' |
    <<
      { si'4:16 do'':16 re'':16 do'':16 | } \\
      { mi'4:16-\sug\fp mi'4:16 mi'4:16 mi'4:16 | }
    >>
    <mi' si'>16 <si mi'> q q q2:16 <<
      { mi''4:16 |
        mi'':16 mi'':16 mi'':16 mi'':16 |
        mi''16 } \\
      { si'4:16 |
        do'':16-\sug\cresc si':16 do'':16 si':16 |
        do''16\! }
    >> <do'' la''>16 q q q4:16 q2:16 |
    << mi''4 \\ si' >>
  }
>> mi'4:16\ff fa':16 re':16 |
do'2~ do'8 <<
  \tag #'violino1 {
    mi''8\p mi'' fa'' |
    sol''4. sol'8 sol'4. sol''8 |
    mi''2 r8 sol'' sol'' sol'' |
    la''4. la''8 la''4 si'' |
    \rt#4 { mi''16 do''' }
  }
  \tag #'violino2 {
    do'8-\sug\p do' re' |
    \rt#4 { mi'16 sol } \rt#4 { re' sol } |
    \rt#4 { mi' sol } \rt#4 { mi' do'' } |
    fa'16 mi' fa' sol' la' fa' mi' fa' la' fa' mi' fa' re' mi' fa' re' |
    mi'8 mi''16 do''' mi'' do''' mi'' do'''
  }
>> \rt#4 { mi''16 do''' } |
\rt#4 { re'' do''' } \rt#4 { re'' do''' } |
\rt#4 { re'' si'' } \rt#4 { re'' si'' } |
la''16 mi'' do'' mi'' la'' mi'' do'' mi'' <<
  \tag #'violino1 {
    la''8 sol''![\f la'' si''] |
    do'''4. do'''8 do'''4 re''' |
    mi'''4:16 mi'''4:16 mi'''2:16\p |
    re'''2:16\cresc re''':16\! |
    mi'''2:16 re''':16 |
    do'''4 do'''8.\ff do'''16 do'''4 do''' |
    sol''2. sol'16 do'' mi'' sol'' |
    sol''4 sol''8. sol''16 sol''4 sol'' |
    mi''2 r |
    sol''16-! do'''( si'' do''') re'''( do''' si'' do''') re'''( do''' si'' do''') re'''( do''' si'' do''') |
    la''-! fa''( mi'' fa'' sol'' fa'' mi'' fa'') re''!-! si''( la'' si'' do''' si'' la'' si'') |
    do'''-! do'''( si'' do''' re''' do''' si'' do''') mi'''( do''' si'' do''') mi'''( do''' si'' do''') |
    la'' do''' si'' do''' la'' do''' si'' do''' sol'' mi'' re'' mi'' re'' sol'' fad'' sol'' |
    do''4.
  }
  \tag #'violino2 {
    la'8 sol'![-\sug\f la' si'] |
    do''16 sol' mi' sol' do'' sol' mi' sol' do'' sol' mi' do' sol' re' si sol |
    do'8 <mi'' do'''>16 q q4:16 q2:16-\sug\p |
    <re'' do'''>:16-\sug\cresc q:16 |
    <mi'' do'''>:16 <re'' si''>:16 |
    <do'' do'''>8 mi''16[-\sug\ff sol'] mi'' sol' mi'' sol' \rt#4 { mi'' sol' } |
    \rt#4 { mi'' sol' } \rt#4 { mi'' sol' } |
    re''16 sol' re'' sol' mi'' sol' mi'' sol' fa'' sol' fa'' sol' re'' sol' re'' sol' |
    mi''2 r |
    sol'16 sol'' sol'' sol'' sol''4:16 sol''2:16 |
    la''16 la' la' la' la'4:16 si'16-! re''( do'' re'' mi'' re'' do'' re'') |
    mi''-! mi''( re'' mi'' fa'' mi'' re'' mi'') sol''( mi'' re'' mi'') sol''( mi'' re'' mi'') |
    fa'' la'' sol'' la'' fa'' la'' sol'' la'' <do'' mi'>4:16 <re' si'>4:16 |
    <do'' mi'>4.
  }
>> do'8 do'4 do' |
\custosNote do'2
