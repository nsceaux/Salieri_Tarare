\clef "bass" <>_"chanté" ^\markup\italic Fièrement R1*3 |
r2 r4 sol8 sol |
do'4 do'8 do' do'4 do' |
sol2. sol4 |
la4 la8 la si4 si8 si |
do'2 r4 mi'8 re' |
do'4 do'8 do' do'4. la8 |
fad4 fad r re8 re |
re'4. re'8 re'4 re'8 re' |
sib2 r4 sib8 sib |
mib'4 mib'8 mib' sib4. sib8 |
sol4 sol r mib'8 re' |
do'4. do'8 dod'4 dod'8 dod' |
re'4 re' sib sol8 sol |
re2 r |
r r8 re mi! fad |
sol4. sol8 si!4. si8 |
re'4 sol r8 sol sol sol |
la4. la8 si4. si8 |
do'4 do' r do'8 do' |
fa2 la4. la8 |
sol2. sol4 |
do2 r4 do'8 la |
mi'4 mi8 mi mi4 mi |
mi mi si si |
do' si do' si8 si |
do'4 mi' do' la8 la |
mi2 r |
r r8 mi mi fa |
sol4. sol8 sol4. sol8 |
mi4 mi r8 sol sol sol |
la4 la la si |
do' do' r do'8 do' |
fa2 fa4. fa8 |
sol2. sol4 |
la2 r8 sol la si |
do'4. do'8 do'4 re' |
mi' do' sol mi |
la2 do'4. la8 |
sol2. sol4 |
do2 r |
R1*8 |
s2
