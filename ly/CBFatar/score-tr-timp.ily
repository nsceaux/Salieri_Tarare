\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Trompettes \small en Ut }
    } <<
      \keepWithTag #'() \global
      \keepWithTag #'trombe \includeNotes "trombe"
    >>
    \new Staff \with { instrumentName = \markup Timbales } <<
      \keepWithTag #'() \global \includeNotes "timpani"
    >>
  >>
  \layout {
    indent = \largeindent
  }
}
