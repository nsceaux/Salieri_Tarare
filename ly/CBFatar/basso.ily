\clef "bass" do8\ff do do do do2:8 |
do:8 do:8 |
si,8 si, do do re re si, si, |
do2:8 do:8\p |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
do:8\fp do:8 |
re:8 re:8 |
re:8 re:8 |
mib:8 mib:8 |
mib:8\fp mib:8\fp |
mib?:8\fp mib:8\fp |
mib:8\f mib:8 |
re4 re' sib sol |
re8 mi!16 fad sol la si dod' re' dod' re' dod' re' dod' re' dod' |
re'2 r |
sol,8\p sol sol sol sol2:8 |
sol:8 sol:8 |
fad:8 fa:8 |
mi8 do mi do mi do mi do |
fa2:8 fa:8 |
sol:8 sol,:8 |
do8\ff mi16 sol do'8 do' do' do' do' si16 la |
sold8\fp sold la la si si la la |
sold2:8 sold:8 |
la8\cresc la sold sold la la sold sold |
la4 mi' do' la |
mi4 mi\ff fa re |
do2:8 do:8\p |
do:8 si,:8 |
do:8 do:8 |
do:8 do:8 |
do8 do' do' do' do'2:8 |
fa:8 fa:8 |
sol:8 sol,:8 |
la,2~ la,8 sol,!\f la, si, |
do do' do' do' do' do' si si |
do' do do do do2:8\p |
fa:8\cresc fa,8 fa fa fa |
sol2:8 sol,:8 |
do:8\ff do:8 |
do:8 do:8 |
si,8 si, do do re re si, si, |
do2 r |
mi2:8 mi:8 |
fa:8 sol:8 |
do:8 do:8 |
fa:8 sol8 sol sol, sol, |
do4. do8 do4 do |
\custosNote do2
