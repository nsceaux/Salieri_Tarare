\clef "treble"
<>-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { do''4 do''8. do''16 do''4 do'' | sol'2. sol'16 do'' mi'' sol'' | }
  { do''4 do''8. do''16 do''4 do'' | sol'2. sol'16 do'' mi'' sol'' | }
>>
\twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''4 sol''8. sol''16 sol''4 sol'' | mi''2. }
  { re''4 mi'' fa'' re'' | mi''2. }
>> r4 |
r <>\p \twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''8. mi''16 mi''4 mi'' | }
  { sol'8. sol'16 sol'4 sol' | }
>>
r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''8. mi''16 mi''4 mi'' |
    fa''1 |
    mi''2. sol''8 si'' |
    do'''2~ do'''8 si'' la'' sol'' |
    fad''2 la'' |
    fad''1 |
    sol'' |
    sol''2 sol'' |
    sol'' sol'' |
    sol''1 |
    fad''4 re'' sib' sol' |
    re'8 mi'!16 fad' sol' la' si' dod'' re'' dod'' re'' dod'' re'' dod'' re'' dod'' |
    re''2 }
  { sol'8. sol'16 sol'4 sol' |
    la'2 si' |
    do''2. mi''8 re'' |
    mi''2~ mi''8 re'' do'' si' |
    la'2 fad''! |
    la'4 la'2 re''4 |
    sib'1 |
    sib'2 sib' |
    sib' sib' |
    do'' dod'' |
    re''4 re'' sib' sol' |
    re'8 mi'!16 fad' sol' la' si' dod'' re'' dod'' re'' dod'' re'' dod'' re'' dod'' |
    re''2 }
  { s2. s1*2 s1-\sug\fp | s1*3 |
    s2-\sug\fp s-\sug\fp | s2-\sug\fp s-\sug\fp | s1-\sug\f }
>> r2 |
R1*3 |
r2 r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''4 |
    re''1 |
    mi''2 re'' |
    do''8 mi''16 sol'' do'''8 do''' do'''4 do''8 mi'' |
    mi''1 |
    mi''2 mi'' |
    mi''1 |
    mi''4 la''2 la''4 |
    mi'' mi' fa' re' | }
  { do''4 |
    do''1 |
    do''2 si' |
    do''8 mi''16 sol'' do'''8 do''' do'''4 do''8 mi'' |
    si'4 do'' re'' do'' |
    si'2 si' |
    do''4 si' do'' si' |
    do'' do''2 do''4 |
    si'4 mi' fa' re' | }
  { s4 s1*2 | s1-\sug\ff | s-\sug\fp | s1 | s-\sug\cresc | s\! | s4 s2.-\sug\ff }
>>
do'1*1/2 s2-\sug\p |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''2 re'' |
    mi''1 |
    fa'' |
    mi'' |
    re'' |
    re''2. sol''4 |
    mi''2~ mi''8 sol'' la'' si'' |
    do'''4 sol''8 sol'' sol''4 sol'' |
    do'''1 |
    do''' |
    do'''2 si'' |
    do'''4 do'''8. do'''16 do'''4 do''' |
    sol''2. sol'16 do'' mi'' sol'' |
    sol''4 sol''8. sol''16 sol''4 sol'' |
    mi''4. do'8 mi'4 sol' |
    sol''1 |
    la''2 re'' |
    mi'' do''' |
    la'' sol''4 si'' |
    do'''4. do'8 do'4 do' | }
  { sol'2 sol' |
    sol'1 |
    la'2 si' |
    do''1 |
    do'' |
    si'2. si'4 |
    do''2~ do''8 sol' la' si' |
    do''4 mi''8 mi'' mi''4 re'' |
    mi''1 |
    re'' |
    mi''2 re'' |
    mi''4 mi''8. mi''16 mi''4 mi'' |
    mi''2. sol'16 do'' mi'' sol'' |
    re''4 mi''8. mi''16 fa''4 re'' |
    mi''4. do'8 mi'4 sol' |
    do''1 |
    do''2 si' |
    do'' mi'' |
    fa'' mi''4 re'' |
    mi''4. do'8 do'4 do' | }
  { s1*6 | s2 s8 s4.-\sug\f | s1 | s2 s-\sug\p |
    s1-\sug\cresc | s1 | s-\sug\ff }
>> \custosNote do'2
