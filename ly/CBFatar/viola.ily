\clef "alto" <mi' do''>16-\sug\ff q q q q4:16 q2:16 |
<sol mi'>2:16 q:16 |
si16 sol' si sol' do' sol' do' sol' re' sol' re' sol' si sol' si sol' |
do'2:8 do':8-\sug\p |
do':8 do':8 |
do':8 do':8 |
do':8 do':8 |
do':8 do':8 |
do':8\fp do':8 |
la8 re' re' re' re'2:8 |
re':8 re':8 |
mib':8 mib':8 |
mib':8\fp mib':8\fp |
mib'?:8\fp mib':8\fp |
mib':8\f mib':8 |
re'4 re'' sib' sol' |
re'8 mi'!16 fad' sol' la' si' dod'' re'' dod'' re'' dod'' re'' dod'' re'' dod'' |
re''2 r |
sol8\p sol' sol' sol' sol'2:8 |
sol':8 sol':8 |
fad':8 fa':8 |
mi'8 do' mi' do' mi' do' mi' do' |
fa'2:8 fa':8 |
sol':8 sol:8 |
do'8\ff mi'16 sol' do''8 do'' do'' do'' do'' si'16 la' |
sold'8\fp sold' la' la' si' si' la' la' |
sold'2:8 sold':8 |
la'8\cresc la' sold' sold' la' la' sold' sold' |
la'4 mi'' do'' la' |
mi'4 mi'\ff fa' re' |
do'2:8 do':8\p |
do':8 si:8 |
do':8 do':8 |
do':8 do':8 |
do'8 do'' do'' do'' do''2:8 |
fa':8 fa':8 |
sol':8 sol:8 |
la2~ la8 sol\f la si |
do' do'' do'' do'' do'' do'' si' si' |
do'' do' do' do' do'2:8\p |
la'2:16-\sug\cresc la':16 |
sol':16 sol':16 |
<mi' do''>:16-\sug\ff q:16 |
q:16 q:16 |
si16 sol' si sol' do' sol' do' sol' re' sol' re' sol' si sol' si sol' |
do'2 r |
sol':16 sol':16 |
la':16 re':16 |
<sol mi'>:16 q:16 |
<la fa'>:16 <sol mi'>4:16 <sol re'>:16 |
<sol mi'>4. do'8 do'4 do' |
\custosNote do'2
