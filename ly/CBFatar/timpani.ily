\clef "bass" do4-\sug\ff do8. do16 do4 do |
do2 r4 do8. do16 |
sol,4 sol,8. sol,16 sol,4 sol, |
do2 r |
R1*3 |
r4 do8. do16 do4 r |
R1*4 |
sol,4 r sol, r |
sol, r sol, r |
sol,1-\sug\f |
do2:16 sol,:16 |
do:16 do:16 |
do4 r r2 |
R1*6 |
do2:16-\sug\ff do4 r |
R1*5 |
do2-\sug\ff r |
R1*6 |
r2 r8 sol,16-\sug\f sol, sol,8 sol, |
do4 r r2 |
do4 r r2 |
R1*2 |
do4-\sug\ff do8. do16 do4 do |
do2 r4 do8. do16 |
sol,4 sol,8. sol,16 sol,4 sol, |
do2 r |
do4 do8. do16 do4 do |
do4 do8. do16 sol,4 sol, |
do2:16 do:16 |
do:16 sol,:16 |
do4. do8 do4 do |
do2