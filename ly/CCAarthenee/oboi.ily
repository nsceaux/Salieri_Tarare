\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do'2 }
  { do' }
>> r2 |
<<
  \tag #'(oboe1 oboi) {
    R1*5 |
    <>^"solo" sib'1~ |
    sib'~ |
    sib'~ |
    sib'2~ sib'8 r r4 |
    R1 |
  }
  \tag #'oboe2 { R1*10 }
>>
R1*3 |
r2 r4 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 | mib''1 | mib''4 }
  { mib'4 | sol'1 | do''4 }
>> r4 r2 |
R1*2 |
r2 r4 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 | fa''1 | fa''4 }
  { fa'4 | do''1 | re''4 }
>> r4 r2 |
R1*2 |
r2 r4 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { mib''4 |
    re''2 fad'' |
    sol''1~ |
    sol''2 fad'' |
    sol''4 sol'8. sol'16 sol'4 sol' |
    sol'2 }
  { sol'4 |
    sol'2 do'' |
    sib'1 |
    la'1 |
    sol'4 sol'8. sol'16 sol'4 sol' |
    sol'2 }
  { s4 | s1 | s-\sug\cresc | s | s-\sug\f }
>> r2 |
R1*2 |
<>-\sug\fp \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''1~ | fad''2 }
  { la'1~ | la'2 }
>> r2 |
R1*2 |
r2\fermata r8 sol'-\sug\f sol' sol' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4 sol''8 sol'' sol''4 do''' |
    si'' sol''\fermata re''4. re''8 |
    mib''1 |
    re'' |
    do''2 }
  { si'4 si'8 si' do''4 mib'' |
    re''4 si'\fermata si'4. si'8 |
    do''1 |
    do''2. si'4 |
    do''2 }
>> r2 |
R1*13 |
