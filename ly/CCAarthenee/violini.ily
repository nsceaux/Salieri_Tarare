\clef "treble" do'2~ do'8.\p sol16 mi'8. do'16 |
sol'2. mi'8. do'16 |
si2~ si8. sol16 re'8. si16 |
sol'2. re'8. mib'16 |
do'2~ do'8. do'16 do'8. do'16 |
fa'4. fa'8 fa'4. mib'8 |
<<
  \tag #'violino1 {
    re'4 sib sib sib |
    sib1~ |
    sib~ |
    sib2~ sib8.
  }
  \tag #'violino2 {
    re'2 mib' |
    fa'4 fa'2 sol'4 |
    lab'2. sol'4 |
    fa'2~ fa'8.
  }
>> sib16 re'8. mib'16 |
fa'4. lab8 lab4.\trill sib16 lab |
sol1 |
<<
  \tag #'violino1 { R1*2 | \allowPageTurn r2 }
  \tag #'violino2 { sib1\p | mib' | do'2 }
>> r8. mib'16\f lab'8. do''16 |
sib'4. reb'8 reb'4.\trill do'16 reb' |
<<
  \tag #'violino1 {
    do'2 r |
    R1*2 |
    r2    
  }
  \tag #'violino2 {
    do'4 do'\p do' do' |
    fa'1 |
    lab'2 fa'4. mib'8 |
    re'2
  }
>> r8. fa'16\f sib'8. re''16 |
do''4. mib'8 mib'4.\trill re'16 mib' |
<<
  \tag #'violino1 {
    re'2 r |
    R1*2 |
    r2
  }
  \tag #'violino2 {
    re'4 re'\p re' re' |
    sol'1~ |
    sol' |
    mib'2
  }
>> r8. sol'16\f do''8. mib''16 |
<<
  \tag #'violino1 {
    re''2 do' |
    sib\cresc re'4 sol |
    la'2
  }
  \tag #'violino2 {
    sol'2 fad'4 fad' |
    sol'\cresc re''8 sib' sol'4 re'' |
    <la' sol''>2
  }
>> <re' la' fad''>2 |
<re' sib' sol''>4\f sol8 sol sol4 sol |
sol2 r |
R1*2 |
<<
  \tag #'violino1 {
    re''8 \grace sol''8 fad''16 mi''?32 fad'' la''?8 fad'' re'' la'? fad'? la' |
    re'1~ |
    re'~ |
    re'~ |
    re'2\fermata
  }
  \tag #'violino2 {
    <fad' la'>1-\sug\fp~ |
    q8 r re' re' fad'4 la' |
    do''1~ |
    do''4. do''8 do''4. re''8 |
    sib'2\fermata
  }
>> r8 sol'\f sol' sol' |
<sol re' si'>4 si'8 si' <sol mib' do''>4 <sol mib' mib''> |
<sol re' si' sol''>4 sol\fermata sol''4. sol''8 |
<mib'' do'''>2 q4. q8 |
<re'' do'''>4 q8 q q4 <re'' si''> |
do'2\p~ do'8. sol16 mib'8. do'16 |
sol'2. mib'8. do'16 |
si2~ si8. sol16 si8. re'16 |
sol'2. mib'8. re'16 |
do'1 |
<<
  \tag #'violino1 {
    fa'2 fa'4 sol' |
    lab'2. fa'4 |
    fa'1 |
    solb'2~ solb'8. lab16 do'8. mib'16 |
    lab'2. solb'4 |
    fa'2. lab8 reb' |
    fa'4 fa' fa' fa' |
    fab'2. sold8 dod' |
    mi'4 mi' mi' mi' |
  }
  \tag #'violino2 {
    do'1~ |
    do'~ |
    do' |
    <do' mib'>~ |
    q2. mib'4 |
    reb'1~ |
    reb'4 reb' reb' reb' |
    reb'1 |
    r4 dod' dod' sold |
  }
>>
