\tag #'all \key do \major
\tempo "Andante Sostenuto" \midiTempo#140
\time 2/2 s1*11 \bar "||"
\tag #'all \key mib \major s1*14
\tempo "Andante con Moto" s1*11 s2
\tempo "Maëstoso" s2 s1*4
\tempo "Primo Tempo" s1*14 \bar "|."
