Ô po -- li -- ti -- que con -- som -- mé -- e !
Je tiens le se -- cret de l’É -- tat ;
je fais mon fils chef de l’ar -- mé -- e ;
à mon tem -- ple je rend l’é -- clat,
aux au -- gu -- res leur re -- nom -- mé -- e.
Pon -- ti -- fes, pon -- ti -- fes a -- droits !
re -- mu -- ez le cœur de vos Rois.
Quand les rois crai -- gnent,
les Bra -- mes rè -- gnent ;
la ti -- are a -- gran -- dit ses droits.
Et qui sait si mon fils, un jour maî -- tre du mon -- de…
