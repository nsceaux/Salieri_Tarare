\clef "bass"
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do2 }
  { do }
>> r2 |
R1*5 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re2 mib | fa4 fa2 sol4 | lab2. sol4 | fa1~ |
    fa2 sib | sib1 }
  { sib,2 do | re4 re2 mib4 | fa2. mib4 | re1~ |
    re2 re | mib1 | }
>>
R1*2 |
r2 r8. mib16-\sug\f^"Tutti" lab8. do'16 |
sib4. reb8 reb4.\trill do16 reb |
do2 r |
R1*2 |
r2 r8. fa16-\sug\f sib8. re'16 |
do'4. mib8 mib4.\trill re16 mib |
re4 r r2 |
R1*2 |
r2 r8. sol16-\sug\f do'8. mib'16 |
re'2 fad |
sol-\sug\cresc \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib2 | la la | sol4 }
  { sol2 | sol fad | sol4 }
  { s2 s1 s4-\sug\f }
>> r4 r2 |
r sol8-\sug\f lab sib do' |
re'2 re'4. re'8 |
re'2 re'4. re'8 |
re'1-\sug\fp~ |
re'4 r r2 |
r4 re-\sug\f fad la? |
fad,2 fad |
r2\fermata r8 sol-\sug\f sol sol |
sol4 sol8 sol mib4 do |
sol, sol\fermata sol4. sol8 |
do2 lab4. lab8 |
fa2. sol4 |
do2 r |
R1*13 |
