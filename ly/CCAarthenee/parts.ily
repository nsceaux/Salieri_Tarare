\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso)
   (fagotti #:system-count 5)
   (oboi #:score-template "score-oboi" #:system-count 4)
   (corni #:tag-global ()
          #:tag-notes corni
          #:instrument "Cors in Mi♭")
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#}))
