\clef "bass" R1*6 |
<>^\markup\italic { Ton très profond }
sib2 sib4. sib8 |
sib2. sib4 |
sib2. sib4 |
sib,4 sib,8 r r2 |
R1 |
r2 r4 sib, |
mib2 mib4 mib |
mib2 mib4 mib |
do2 r |
R1 |
<>^\markup\italic { Toujours en serrant le mouvement }
r4 do do do |
fa1 |
lab2 fa4. mib8 |
re2 re4 r |
R1 |
r2 re4 re |
sol2 sol4. sol8 |
sol2. sol4 |
mib2 r |
r fad4 fad |
sol2. sol4 |
la2 re'4. re'8 |
sol2 sol4 r |
r2 r4 sol |
re'2 re'4. re'8 |
re'2 re'4. re'8 |
re'1 |
r4 re8 re fad4 la? |
do'1 |
do'2. re'4 |
sib2\fermata r8 sol sol sol |
si4 si8 si do'4. do'8 |
sol4 sol\fermata sol4. sol8 |
do'2 lab4 lab |
fa2. sol4 |
do2 r |
R1*3 |
r2 do4 do |
fa2 fa4 sol |
lab2. fa4 |
fa2 fa4 fa8 fa |
solb4 solb r2 |
<<
  R1*5
  \tag #'recit { s4*20
    _\markup\right-align\italic {
      Il voit arriver Tarare ; il se retire dans le Temple.
    }
  }
>>
