\clef "alto" do'1-\sug\p~ |
do'8.\mf sol16 do'8. sol16 mi2 |
re1~ |
re8. sib16 sib8. la16 sol2 |
sol fa! |
do'1 |
sib2 do' |
re'4 re'2 mib'4 |
fa'2. mib'4 |
re'1~ |
re'2 sib |
sib1 |
sol-\sug\p |
sib |
mib2. lab4-\sug\f |
mib'1 |
mib'4 mib\p mib mib |
do'1 |
do' |
<>_\markup\tiny\center-align { [source : mi♭] }
fa2. sib4-\sug\f |
fa'1 |
fa'4 fa-\sug\p fa fa |
re'1 |
si2 re' |
sol2. do'4-\sug\f |
re'2 do' |
sib-\sug\cresc re' |
mib' re' |
sol4-\sug\f sol8 sol sol4 sol |
sol2 sol8\f lab sib do' |
re'2 re'4. re'8 |
re'2 re'4. re'8 |
re'1-\sug\fp~ |
re'4 la8 la re'4 fad' |
la'1~ |
la'4. la'8 la'4. la'8 |
sol'2\fermata r8 sol'-\sug\f sol' sol' |
sol'4 sol'8 sol' mib'4 do' |
sol sol'\fermata sol'4. sol'8 |
do'2 lab'4. lab'8 |
fa'2. sol'4 |
do'1\p~ |
do'8.\mf sol16 do'8. sol16 mib2 |
re1~ |
re8. sib!16 sib8. lab16 sol2 |
sol fa |
do'1~ |
do'~ |
do'~ |
do'~ |
do'2. lab4 |
lab1~ |
lab4 lab lab lab |
lab1 |
r4 sold sold sold |

