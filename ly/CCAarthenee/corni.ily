\clef "treble" \transposition mib
R1*6 |
\twoVoices #'(corno1 corno2 corni) <<
  { <<
      \tag #'corno1 R1*4
      \tag #'corni s1*4
    >>
    sol'2 re'' |
    do''1 |
  }
  { \tag #'corni <>^\markup { \concat { 2 \super o } solo }
    sol'1~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'2 sol' |
    mi'1 | }
>>
<<
  \tag #'(corno1 corni) {
    <>^\markup { Primo solo }
    do''1-\sug\p~ |
    << { do''1~ | do''1~ | } { s2 s1\< s4 s-\sug\f } >>
    do''1~ |
    do''\p |
    re''~ |
    re''~ |
    re''\<~ |
    << re''1~ { s4-\sug\f } >>
    re''1\p |
    mi''~ |
    mi''~ |
    mi''\< |
    mi''2-\sug\f fad'' |
    sol''1 |
    R1 |
  }
  \tag #'corno2 { R1*16 }
>>
r4 <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { mi''8. mi''16 mi''4 mi'' | mi''2 }
  { mi'8. mi'16 mi'4 mi' | mi'2 }
>> r2 |
R1*6 |
r2\fermata r8 <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { mi''8 mi'' mi'' |
    mi''4 mi''8 mi'' mi''4 mi'' |
    mi'' mi''\fermata mi''4. mi''8 |
    do''2 fa''4. fa''8 |
    re''2. mi''4 |
    mi'' }
  { mi'8 mi' mi' |
    mi'4 mi'8 mi' mi'4 mi' |
    mi' mi'\fermata mi'4. mi'8 |
    do''2 fa''4. fa''8 |
    re''2. mi''4 |
    mi'' }
>> r4 r2 |
R1*13 |
