\clef "bass" do1\p~ |
do |
sol, |
sol,8. sol,16 sol,8. la,16 sib,2 |
la,!1 |
lab, |
sib,~ |
sib,~ |
sib,~ |
sib,~ |
sib,2 re |
mib1 |
mib,\p |
sol, |
lab,2. lab4\f |
sol2 sol, |
lab,1~ |
lab,4 lab,\p lab, lab, |
fa,2 lab, |
sib,2. sib4\f |
la2 la, |
sib,1~ |
sib,4 si,\p si, si, |
sol,2 si, |
do2. do'4\f |
sib,2 la,\p |
sol,\cresc sib, |
do re |
sol,4 sol,8\f sol, sol,4 sol, |
sol,2 sol,8\f lab, sib, do |
re2 re4. re8 |
re2 re4. re8 |
re1\fp~ |
re~ |
re4 re8\f re fad4 la? |
fad,2 fad |
sol2\fermata r8 sol\f sol sol |
sol4 sol8 sol mib4 do |
sol, sol\fermata sol4. sol8 |
do2 lab4. lab8 |
fa2. sol4 |
do1\p |
do |
sol, |
sol,8. sol,16 sol,8. la,16 sib,!2 |
la,1 |
lab,!2 lab,4 sol, |
fa,1 |
lab,~ |
lab, |
do8. lab,16 do8. mib16 lab4 do |
reb1 |
reb!4 reb reb reb |
reb1 |
r4 dod4 dod dod |
