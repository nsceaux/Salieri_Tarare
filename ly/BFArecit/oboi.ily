\clef "treble" r8^"solo" mi''4-\sug\ff mi'' mi'' do''8 |
\once\slurDashed si'( la' sold' la') la'2 |
r8 la'( fa'' mi'') re''-! do''-! si'-! la'-! |
sold'4.. \twoVoices #'(oboe1 oboe2 oboi) <<
  { sold''16 sold''4.. sold''16 | sold''4 }
  { si'16 si'4.. si'16 | si'4 }
>> r4 r2 |
R1 |
r4^"solo" mi''2-\sug\fp do''4 |
\appoggiatura si'8 la'2 mi''4. la''8 |
\grace sol''8 fa''2 r4 r8 fa'' |
fa''4. re''8 si'4. mi''8 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la'4 la' sol'! fa' | }
  { la'4 la'-\sug\f sol' fa' | }
>>
mi'1 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re'4 } { re'4 }
>> r4 r2 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi'4( sol' do'') |
    \once\slurDashed mi''2( fa''4 sol'') |
    la'2 re'' |
    mib''2.-\sug\fp do''4 |
    si' }
  { mi'4(-\sug\f sol' do'') |
    \once\slurDashed mi''2( fa''4 sol'') |
    la'2 la'-\sug\p~ |
    la'2. la'4 |
    re'4 }
>> r4 r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''1 | re''2 red'' | mi'' }
  { la'1-\sug\fp | si'2-\sug\fp la'-\sug\fp | sol'-\sug\p }
>> r2 |
r4^"solo" sol''2 sol''4 |
la''-\sug\cresc( fa'' do'' la') |
fa'1-\sug\fp |
mi'4 r r2 |
R1*4 |
