\score {
  \new StaffGroupNoBar <<
    \new Staff \with { \oboiInstr } << \global \includeNotes "oboi" >>
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'tous \includeNotes "voix"
    >> \keepWithTag #'tous \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr 
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*6\break s1*7\break s1*5\pageBreak
        s1*3\break s1*3 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
