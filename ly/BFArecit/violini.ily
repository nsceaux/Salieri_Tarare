\clef "treble" <do' mi' la'>8\ff <<
  \tag #'violino1 {
    mi''4 mi'' mi'' do''8 |
    si'( la' sold' la') la'2 |
    r8 la'( fa'' mi'') re''-! do''-! si'-! la'-! |
    sold'4.. <si' sold''>16 q4.. <si'' si'>16 |
    q4 r r2 |
    R1 |
    r4 mi''2\fp do''4 |
    \grace si'8 la'2 mi''4. la''8 |
    \grace sol''8 fa''2 r4 r8 fa'' |
    fa''4. re''8 si'4. mi''8 |
    la'4
  }
  \tag #'violino2 {
    <do' mi'>4 q q q8 |
    <do' la'>8 do'4 do' do' do'8 |
    do' la([ fa' mi']) re'-! do'-! si-! la-! |
    sold4.. <si' sold''>16 q4.. q16 |
    q4 r r2 |
    R1 |
    la8\fp <do' mi'>4 q q q8~ |
    q q4 q q q8 |
    <re' fa'>8 q4 q q q8 |
    q <fa' re''>4 q8 <si sold'>8 q4 q8 |
    <la' la>4
  }
>> la'4:16\f sol'!:16 fa':16 |
<<
  \tag #'violino1 {
    mi'8 r do''4(\p mi'' do'') |
    si'4\fp si'8-! do''-! re''4\fp mi''8 fa'' |
    mi''4
  }
  \tag #'violino2 {
    mi'16 sol'[-\sug\p sol' sol'] sol'2.:16 |
    <re' fa'>8\fp q[ q <do' mi'>] <si re'>-\sug\fp <si sol'>[ q q] |
    <do' sol'>4
  }
>> mi'4:16\f sol':16 do'':16 |
<<
  \tag #'violino1 {
    mi''2( fa''4 sol'') | la'2
  }
  \tag #'violino2 {
    \once\slurDashed mi'2( fa'4 sol') | la2
  }
>> <re' re''>4.\p q8 |
<mib'' mib'>16\fp <do' mib'> q q q4:16 q2:16 |
<si re'>4 r8 sol'16\ff la' \grace do''8 si' la'16 sol' \grace do''8 si' la'16 sol' |
<<
  \tag #'violino1 {
    re''2:16\fp re'':16 |
    re'':16\fp red'':16\fp |
    mi''2. mi''8\p re'' |
    do''8 do''4 do'' do'' do''8 |
    do'' do''4\cresc do''\< la' la'8 |
    fa''4.\fp re''8 si'4. la'8 |
    sold'4
  }
  \tag #'violino2 {
    la'2:16-\sug\fp la':16 |
    si':16-\sug\fp la':16-\sug\fp |
    sol'1-\sug\p |
    sol'8 sol'4 sol' sol' sol'8 |
    la' la'4 la'\< do' do'8 |
    la'8-\sug\fp la'4 fa' fa' fa'8 |
    mi'4
  }
>> r4 r2 |
R1 |
r4 <<
  \tag #'violino1 {
    la'4(\f la' la') |
    <sol' sib'>1\fp~ |
    q2.\fermata r4 |
  }
  \tag #'violino2 {
    <do' mi'>4(-\sug\f q q) |
    q1-\sug\fp~ |
    q2.\fermata r4 |
  }
>>
