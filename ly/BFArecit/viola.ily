\clef "alto" la8-\sug\ff la4 la la la8 |
fa' la4 la la la8 |
la la([ fa' mi']) re'8-! do'-! si-! la-! |
sold4.. mi'16 mi'4.. mi'16 |
mi'4 r r2 |
R1 |
la8-\sug\fp la4 la la la8~ |
la la4 la la la8~ |
la la4 la la la8~ |
la la'4 re''8 mi' mi'4 mi'8 |
do'4 la':16\f sol'!:16 fa':16 |
mi'16 mi'[-\sug\p mi' mi'] mi'2.:16 |
re'8-\sug\fp re'[ re' do'] si-\sug\fp si[ si si] |
do' r mi4(-\sug\f sol do') |
mi' do( re mi) |
fa2 la'4.-\sug\p la'8 |
la'16-\sug\fp la la la la4:16 la2:16 |
\footnoteHere#'(0 . 0) \markup {
  Source : \score {
    { \clef "alto" \tinyQuote
      la'16 la la la la4:16 la2:16 | mi4 }
    \layout { \quoteLayout }
  }
}
sol4 r8 sol16-\sug\ff la \grace do'8 si la16 sol \grace do'8 si la16 sol |
re'2:16-\sug\fp re'2:16 |
si':16-\sug\fp si':16-\sug\fp |
si'1-\sug\p |
do'8 do'4 do' do' do'8 |
do' do'4 do'\< fa' fa'8 |
fa'-\sug\fp fa'4 re' re' re'8 |
si4 r r2 |
R1 |
r4 do'(-\sug\f do' do') |
do'1-\sug\fp~ |
do'2.\fermata r4 |
