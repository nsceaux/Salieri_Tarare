Que me veux- tu, bra -- ve sol -- dat ?
\tag #'tous {
Ô mon roi ! prends pi -- tié de mon af -- freux é -- tat.
En plei -- ne paix, un a -- va -- re cor -- sai -- re
com -- ble sur moi les hor -- reurs de la guer -- re.
Tous mes jar -- dins sont ra -- va -- gés,
mes es -- cla -- ves sont é -- gor -- gés,
l’hum -- ble toit de mon As -- ta -- si -- e
est con -- su -- mé par l’in -- cen -- di -- e…
}
Grâce au Ciel, mes ser -- ments vont ê -- tre dé -- ga -- gés !
Sol -- dat qui m’as sau -- vé la vi -- e,
