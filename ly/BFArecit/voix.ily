\clef "bass" R1*4 |
<>^\markup\character Atar
r2 r8 si si si |
sold4 sold8 sold16 la si4 r\fermata |
<<
  \tag #'tous {
    \ffclef "tenor/G_8" <>^\markup\character-text Tarare douloureusement
    r4 mi'2 do'4 |
    la2 mi'4. la'8 |
    \grace sol'8 fa'2 r4 r8 fa' |
    fa'4. re'8 si4. mi'8 |
    la2 r |
    r4 do' mi'4. do'8 |
    si4 si8 do' re'4 mi'8 fa' |
    mi'4 mi' r2 |
    mi'2 fa'4 sol' |
    la2 re'4. re'8 |
    mib'2. re'8 do' |
    si4 sol r2 |
    <>^\markup { Récit \italic { vite et fort } }
    r8 la la la re' re' re' re' |
    si4 r8 si16 si red'8 red'16 red' red'8 red' |
    <>^\markup { \concat { 1 \super o } Tempo \italic { avec douleur } }
    mi'2. mi'8 re' |
    do'4 do'8 do' do'4. do'8 |
    do'4 do' r8 la la la |
    fa'4. re'8 si4. la8 |
    sold sold \ffclef "bass"
  }
  \tag #'basse { R1*18 r4 }
>>
<>^\markup\character Atar
si8 si sold4 r8 mi16 fad |
sold4. si8 sold sold sold la |
mi4 r r2 |
r4 r8 sol sib4. sib8 |
sib sib do' re' mi\fermata mi r4 |
