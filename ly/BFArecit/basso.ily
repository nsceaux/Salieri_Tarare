\clef "bass" <>^\markup "Bassons avec les B." la,4\ff do mi la |
fa fa fa8 fa, la, do |
fa4 fa fa re |
mi4. mi8 mi4. mi8 |
mi4 r r2 |
R1 |
<>^"Fag. tacet" la,4\fp la, la, la, |
la, la, la, la, |
re re re re |
re re mi mi |
la, la\f sol! fa |
mi <>^"Bassons seuls"
\clef "tenor" <<
  { sol'2 sol'4 | fa'4. mi'8 re'4. sol'8 | sol'4 } \\
  { mi'2 mi'4 | re'4.\fp do'8 si4.\fp si8 | do'4 }
>> r4 r2 |
\clef "bass" <>^"tutti" r4 do(\f re mi) |
fa2 fa4.\p fa8 |
fad2:8\fp fad:8 |
sol4. sol16\ff la \grace do'8 si la16 sol \grace do'8 si la16 sol |
fad2:16\fp fad:16 |
sol:16\fp fad:16\fp |
mi1\p |
mi4 mi mi mi |
fa\cresc fa fa fa |
re\fp re re re |
mi r r2 |
R1 |
r4 do(\f do do) |
do1\fp~ |
do2.\fermata r4 |
