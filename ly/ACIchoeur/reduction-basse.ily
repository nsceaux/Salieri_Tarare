\clef "bass"
sib,8 <sib, re fa> q q |
fa <fa la do'> q q |
sib8 <sib, re fa> q q |
fa <fa la do'> q q |
sib,8 sib[ re' sib] |
fa sib re fa |
mib4. re8 |
do4 re8 mib |
re8. re16 mib8. mib16 |
fa8 fa fa, fa, |
sib, sib[ re' sib] |
fa sib re fa |
mib4. re8 |
do4 re8 mib |
re4 mib |
fa4 fa, |
r8 <sib, re fa> q q |
fa <fa la do'> q q |
sib8 <sib, re fa> q q |
fa <fa la do'> q q |
<sib re' fa'>2:16 |
q:16 |
<sib, re fa>2:16 |
q:16 |
