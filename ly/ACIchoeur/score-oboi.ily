\score {
  \new GrandStaff <<
    \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
    \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
  >>
  \layout { indent = \noindent }
}
