\score {
  \new GrandStaff \with { instrumentName = "Flûtes" } <<
    \new Staff << \global \keepWithTag #'flauto1 \includeNotes "flauti" >>
    \new Staff << \global \keepWithTag #'flauto2 \includeNotes "flauti" >>
  >>
  \layout { indent = \largeindent}
}
