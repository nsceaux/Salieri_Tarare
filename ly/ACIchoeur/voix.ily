<<
  \tag #'(nature basse) {
    \clef "soprano/treble" sib'4 r |
    R2*23 |
  }
  \tag #'vdessus {
    \clef "soprano/treble"
    <>^\markup\italic Légèrement
    ^\markup\italic { Les Ombres d’Atar et de Tarare se prosternent }
    R2*10 |
    r8 <<
      { \voiceOne re''8 re'' re'' |
        re'' re'' re'' re'' |
        do''4. re''8 |
        mib''4 re''8 do'' |
        fa''4 sol''8 mib'' |
        re''4.( do''8) |
        sib'4 \oneVoice
      }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sib'8 sib' sib' |
        sib' sib' sib' sib' |
        sol'4. fa'8 |
        mib'4 fa'8 sol' |
        fa'4 mib'8 do'' |
        sib'4.( la'8) |
        sib'4
      }
    >> r4 |
    r8 fa'' fa'' fa'' |
    re''4 r |
    r8 fa'' fa'' fa'' |
    re''4 r |
    R2*3 |
  }
  \tag #'vhaute-contre {
    \clef "alto/G_8"
    R2 |
    r8 fa' fa' fa' |
    re'4 r |
    r8 fa' fa' fa' |
    re'4 r |
    R2*12 |
    r8 do' do' do' |
    re'4 r |
    r8 do' do' do' |
    re'4 r |
    R2*3 |
  }
  \tag #'vtaille {
    \clef "tenor/G_8"
    R2 |
    r8 la la la |
    sib4 r |
    r8 la la la |
    sib re' re' re' |
    re' re' re' re' |
    do'4. re'8 |
    mib'4 re'8 do' |
    fa'4 sol'8 mib' |
    re'4.( do'8) |
    sib4 r |
    R2*6 |
    r8 la la la |
    sib4 r |
    r8 la la la |
    sib4 r |
    R2*3 |
  }
  \tag #'vbasse {
    \clef "bass/bass"
    r8 <<
      { \voiceOne re'8 re' re' |
        do'4 \oneVoice r4 |
        r8 \voiceOne re' re' re' |
        do'4 \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sib8 sib sib |
        la4 s |
        s8 sib sib sib |
        la4
      }
    >> r4 |
    r8 sib sib sib |
    sib sib sib sib |
    sol4. fa8 |
    mib4 fa8 sol |
    fa4 mib8 do' |
    sib4.( la8) |
    sib4 r |
    R2*5 |
    r8 <<
      { \voiceOne re'8 re' re' |
        do'4 \oneVoice r4 |
        r8 \voiceOne re' re' re' |
        do'4 \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sib8 sib sib |
        la4 s |
        s8 sib sib sib |
        la4
      }
    >> r4 |
    R2*4 |
  }
>>
