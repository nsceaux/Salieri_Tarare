\clef "treble"
sib'16-. fa'-. sol'-. la'-. sib'-. do''-. re''-. sib'-. |
do'' fa' la' do'' fa'' mib'' re'' do'' |
re'' fa' sol' la' sib' do'' re'' sib' |
do'' fa' la' do'' fa'' mib'' re'' do'' |
<sib' re''>2:16 |
q:16 |
<sol' do''>4:16 q8. <fa' re''>16 |
<mib' mib''>4 <fa' re''>8 <sol' do''> |
fa''8. fa''16 <mib'' sol''>8. <do'' mib''>16 |
<sib' re''>4:16 q8 <la' do''> |
<sib' re''>2:16 |
q2:16 |
<sol' do''>4:16 q8. <fa' re''>16 |
<mib' mib''>4 <fa' re''>8 <sol' do''> |
<re'' fa''>4 <mib'' sol''>8. <do'' mib''>16 |
<sib' re''>4:16 q8. <la' do''>16 |
sib'16 fa'[ sol' la'] sib' do'' re'' sib' |
do'' fa' la' do'' fa'' mib'' re'' do'' |
re'' fa' sol' la' sib' do'' re'' sib' |
do'' fa' la' do'' fa'' mib'' re'' do'' |
re'' sib'' re''' sib'' fa'' fa'' sib'' fa'' |
re'' fa'' sib'' fa'' re'' re'' fa'' re'' |
<< { s8 s\cresc } \rt#4 { sib'16 fa' } >> |
\rt#4 { sib' fa'\! } |
