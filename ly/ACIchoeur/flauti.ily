\clef "treble"
<<
  \tag #'flauto1 {
    r16 fa''-. sol''-. la''-. sib''-. do'''-. re'''-. sib''-. |
    do''' fa'' la'' do''' fa''' mib''' re''' do''' |
    re''' fa'' sol'' la'' sib'' do''' re''' sib'' |
    do''' fa'' la'' do''' fa''' mib''' re''' do''' |
    re'''8 r r4 |
    R2*5 |
    r16 re''' re''' re''' re'''4:16 |
    re'''2:16 |
    do'''4 do'''8. re'''16 |
    mib'''4 re'''8 do''' |
    fa'''4 sol'''8. mib'''16 |
    re'''16 re''' re''' re''' re'''8. do'''16 |
    sib''16 fa''[ sol'' la''] sib'' do''' re''' sib'' |
    do''' fa'' la'' do''' fa''' mib''' re''' do''' |
    re''' fa'' sol'' la'' sib'' do''' re''' sib'' |
    do''' fa'' la'' do''' fa''' mib''' re''' do''' |
    sib'' sib'' re''' sib'' fa'' fa'' sib'' fa'' |
    re'' fa'' sib'' fa'' re'' re'' fa'' re'' |
    sib'8 sib' sib' sib' |
    sib' sib' sib' sib' |
  }
  \tag #'flauto2 {
    R2*10 |
    r16 sib'' sib'' sib'' sib''4:16 |
    sib''2:16 |
    sol''4 sol''8. fa''16 |
    mib''4 fa''8 sol'' |
    fa''4 mib''8. do'''16 |
    sib''16 sib'' sib'' sib'' sib''8. la''16 |
    sib''4 r |
    R2*5 |
    r8 re' re' re' |
    re' re' re' re' |
  }
>>
