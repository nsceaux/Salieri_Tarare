\clef "tenor" r8 <<
  { re'8[ re' re'] | do'4 } \\
  { sib8[ sib sib] | la4 }
>> r4 |
r8 <<
  { re'8[ re' re'] | do'4 } \\
  { sib8[ sib sib] | la4 }
>> r4 |
<<
  { re'2~ | re' | do'4. re'8 | mib'4 re'8 do' |
    fa'4 sol'8. mib'16 | re'4. do'8 | sib4 } \\
  { sib2~ | sib | sol4. fa8 | mib4 fa8 sol |
    fa4 mib8. do'16 | sib4. la8 | sib4 }
>> r4 |
R2*5 |
r8 <<
  { re'8[ re' re'] | do'4 } \\
  { sib8[ sib sib] | la4 }
>> r4 |
r8 <<
  { re'8[ re' re'] | do'4 } \\
  { sib8[ sib sib] | la4 }
>> r4 |
<<
  { re'2~ | re'~ | re'4 } \\
  { sib2~ | sib~ | sib4 }
>> r4 |
R2 |
