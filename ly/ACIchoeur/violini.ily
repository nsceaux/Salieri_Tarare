\clef "treble"
<<
  \tag #'violino1 {
    sib'16-. fa'-. sol'-. la'-. sib'-. do''-. re''-. sib'-. |
    do'' fa' la' do'' fa'' mib'' re'' do'' |
    re'' fa' sol' la' sib' do'' re'' sib' |
    do'' fa' la' do'' fa'' mib'' re'' do'' |
    re'' re'' re'' re'' re'' re'' re'' re'' |
    re''2:16 |
    do''16 do'' do'' do'' do''8. re''16 |
    mib''4 re''8 do'' |
    fa''8. fa''16 sol''8. mib''16 |
    re''16 re'' re'' re'' re''8 do'' |
    sib'16 re'' re'' re'' re''4:16 |
    re''2:16 |
    do''4:16 do''8. re''16 |
    mib''4 re''8 do'' |
    fa''4 sol''8. mib''16 |
    re''16 re'' re'' re'' re''8 do'' |
    sib'16 fa'[ sol' la'] sib' do'' re'' sib' |
    do'' fa' la' do'' fa'' mib'' re'' do'' |
    re'' fa' sol' la' sib' do'' re'' sib' |
    do'' fa' la' do'' fa'' mib'' re'' do'' |
    re'' sib'' re''' sib'' fa'' fa'' sib'' fa'' |
    re'' fa'' sib'' fa'' re'' re'' fa'' re'' |
    sib' fa' sib'\cresc fa' sib' fa' sib' fa' |
    sib' fa' sib' fa' sib' fa' sib' fa'\! |
  }
  \tag #'violino2 {
    r16 re' re' re' re' re' re' re' |
    do'2:16 |
    re':16 |
    do':16 |
    sib16 sib' sib' sib' sib' sib' sib' sib' |
    sib'2:16 |
    sol'16 sol' sol' sol' sol'8. fa'16 |
    mib'4 fa'8 sol' |
    fa'8. fa'16 mib'8. do''16 |
    sib'16 sib' sib' sib' sib'8 la' |
    sib'2:16 |
    sib':16 |
    sol'4:16 sol'8. fa'16 |
    mib'4 fa'8 sol' |
    fa'4 mib'8. do''16 |
    sib'16 sib' sib' sib' sib'8 la' |
    sib'16 re'[ re' re'] re' re' re' re' |
    do'2:16 |
    re':16 |
    do':16 |
    r16 <fa' re'> q q q4:16 |
    q2:16 |
    q2:16 |
    q2:16 |
  }
>>
