\score {
  \new StaffGroup <<
    \new Staff <<
      <>^"Violoncelle"
      \global \keepWithTag #'violoncelli \includeNotes "basso"
    >>
    \new Staff <<
      <>^"Basso"
      \global \keepWithTag #'basso \includeNotes "basso"
    >>
  >>
  \layout { indent = \noindent }
}
