\score {
  \new ChoirStaff <<
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
    } <<
      <>^"Violino I"
      \global \keepWithTag #'violino1 \includeNotes "violini"
    >>
  >>
  \layout {
    system-count = #(or (*system-count*) #f)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
    indent = \noindent
    \context {
      \Score
      \remove "Metronome_mark_engraver"
    }
  }
}
