\tag #'(nature basse) {
  nous.
}
\tag #'(vbasse vtaille vhaute-contre) {
  Quit -- tons nos jeux, ac -- cour -- rons tous :
}
\tag #'(vbasse vtaille) {
  deux de nos frè -- res à ge -- noux
  re -- çoi -- vent l’ar -- rêt de leur vi -- e.
}
\tag #'vdessus {
  deux de nos frè -- res à ge -- noux
  re -- çoi -- vent l’ar -- rêt de leur vi -- e.
}
Quit -- tons nos jeux, ac -- cour -- rons tous.
