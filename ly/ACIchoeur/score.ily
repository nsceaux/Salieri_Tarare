\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \flautiInstr } <<
        \new Staff << \global \keepWithTag #'flauto1 \includeNotes "flauti" >>
        \new Staff << \global \keepWithTag #'flauto2 \includeNotes "flauti" >>
      >>
      \new GrandStaff \with { \oboiInstr } <<
        \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
      >>
      \new Staff \with { \fagottiInstr } << \global \includeNotes "fagotti" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      instrumentName = \markup\center-column\smallCaps { Chœur des Ombres }
      shortInstrumentName = \markup\character Ch.
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with { \natureInstr \haraKiri } \withLyrics <<
      \global \keepWithTag #'nature \includeNotes "voix"
    >> \keepWithTag #'nature \includeLyrics "paroles"
    \new StaffGroup <<
      \new Staff \with {
        \violoncelliInstr
        \consists "Metronome_mark_engraver"
      } << \global \keepWithTag #'violoncelli \includeNotes "basso" >>
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'basso \includeNotes "basso"
        \origLayout {
          s2*5\pageBreak
          s2*7\pageBreak
          s2*6\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
