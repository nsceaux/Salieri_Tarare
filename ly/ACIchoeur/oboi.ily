\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 }
  { sib' }
>> r4 |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''8[ fa'' fa''] | re''4 }
  { la'8[ la' la'] | sib'4 }
>> r4 |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''8[ fa'' fa''] | re''4 }
  { la'8[ la' la'] | sib'4 }
>> r4 |
R2*5
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 |
    fa'8 sib' re' fa' |
    mib'4. re'8 |
    do'4 re'8 mib' |
    re'4 mib'8. mib'16 |
    fa'4. fa'8 |
    sib'4 r | }
  { sib'2 | R2*6 | }
>>
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''8[ fa'' fa''] | re''4 }
  { la'8[ la' la'] | sib'4 }
>> r4 |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''8[ fa'' fa''] | re''2~ | re'' | re''4 }
  { la'8[ la' la'] | sib'2~ | sib' | sib'4 }
>> r4 |
R2 |
