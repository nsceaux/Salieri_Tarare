\clef "alto" r16 sib sib sib sib sib sib sib |
la2:16 |
sib:16 |
la:16 |
r8 sib[ re' sib] |
fa sib re fa |
mib4. re8 |
do4 re8 mib |
re8. re16 mib8. mib16 |
fa8[ fa fa fa] |
sib sib'[ re'' sib'] |
fa' sib' re' fa' |
mib'4. re'8 |
do'4 re'8 mib' |
re'4 mib' |
fa' fa |
sib16 sib[ sib sib] sib sib sib sib |
la2:16 |
sib:16 |
la:16 |
sib:16 |
sib:16 |
sib:16 |
sib:16 |
