\piecePartSpecs
#`((reduction)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet#})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#})

   (flauti #:score "score-flauti")
   (oboi #:score "score-oboi")
   (fagotti #:clef "tenor" #:indent 0)
   
   (violino1 #:indent 0)
   (violino2 #:indent 0)
   (viola #:indent 0)
   (basso #:score "score-bassi")
   (silence #:score "score-silence"))
