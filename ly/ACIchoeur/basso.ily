<<
  \tag #'violoncelli {
    \clef "tenor" r16 fa' fa' fa' fa'4:16 |
    fa'2:16 |
    fa'2:16 |
    fa'2:16 |
    fa'8 \clef "bass"
  }
  \tag #'basso {
    \clef "bass"
    sib,8 sib,[ sib, sib,] |
    fa fa[ fa fa] |
    sib sib,[ sib, sib,] |
    fa fa[ fa fa] |
    sib,
  }
>> sib[ re' sib] |
fa sib re fa |
mib4. re8 |
do4 re8 mib |
re re mib mib |
fa fa fa, fa, |
sib,4 r |
R2*5 |
<<
  \tag #'violoncelli {
    \clef "tenor" r16 fa' fa' fa' fa'4:16 |
    fa'2:16 |
    fa'2:16 |
    fa'2:16 | \clef "bass"
  }
  \tag #'basso {
    r8 sib,[ sib, sib,] |
    fa fa[ fa fa] |
    sib sib,[ sib, sib,] |
    fa[ fa fa fa] |
  }
>>
sib16 sib sib sib sib4:16 |
sib2:16 |
sib16 sib, sib,\cresc sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib, sib, sib, sib,\! |
