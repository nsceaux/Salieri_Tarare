\piecePartSpecs
#`((violino1)
   (violino2)
   (viola #:system-count 3)
   (basso)
   (fagotti #:tag-notes fagotti #:score-template "score-part-voix")
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
