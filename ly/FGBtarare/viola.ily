\clef "alto" r8 |
re'4-\sug\f re |
sol la |
re8 re'16.[ re'32] re'8 re' |
re'4 re'-\sug\p( |
mi' fad') |
sol'8 sol'[\f re' si] |
sol2\fermata |
sol'4\p fad' |
mi' sol'8-\sug\mf sol'16. fad'32 |
mi'8. mi16 mi16.-\sug\f fad32 sol16. sold32 |
la8 la16.[ la32] la8 la |
la4\fermata r |
lad'\f fad'8. fad'16 |
si'4 r |
fad16-\sug\p la fad la fad la fad la |
\rt#4 { fad la } |
\rt#4 { sol si } |
sol si re' si sol si re' si |
fad la re' la fad la re' la |
\rt#4 { sol si } |
la8 la4 la8 |
