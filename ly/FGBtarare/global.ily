\key re \major
\tempo "Andante con moto, mais majestueux"
\time 2/4 \partial 8 s8 s2*6 s4
\tempo "Plus lent" s4 s2*4 s4
\tempo "Primo Tempo" s4 s2*9 \bar "||"
