Le trône est pour moi sans ap -- pas :
je ne suis point né vo -- tre maî -- tre.
Vou -- loir ê -- tre ce qu’on n’est pas,
c’est re -- non -- cer à tout ce qu’on peut ê -- tre.
Je vous ser -- vi -- rai de mon bras :
mais lais -- sez- moi fi -- nir en paix ma vi -- e
dans la re -- traite a -- vec mon As -- ta -- si -- -
