\clef "bass" r8 |
R2*14 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la2 | la | si | }
  { fad2 | fad | sol | }
>>
R2*4 |
