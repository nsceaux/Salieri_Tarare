\clef "tenor/G_8" la8 |
re'4 fad'8. re'16 |
si4 dod'8. dod'16 |
re'4 r |
r8 re'16 re' re'8 mi' |
do'4 do'8. re'16 |
si8 si r4 |
r r8 si16 si |
si8 si red' red'16 red' |
mi'4 r16 mi' mi' fad' |
sol'8. sol'16 sol' fad' mi' re' |
la'8 la r4 |
r4\fermata dod'16 dod' dod' dod' |
fad'4 fad'8 fad' |
re'4 r16 si si dod' |
re'8 re' re' re' |
fad'4. re'8 |
si si r4 |
si4 re'8 si |
la4. re'8 |
si mi' sol' mi' |
re'4.\( mi'8\) |
