\clef "bass" r8 |
re4\f re |
sol, la, |
re8 re16.[ re32] re8 re |
re4 re\p( |
mi fad) |
sol8 sol[\f re si,] |
sol,2\fermata |
sol4\p fad |
mi r |
r mi16.\f fad32 sol16. sold32 |
la8 la,16.[ la,32] la,8 la, |
la,4\fermata r |
lad\f fad8. fad16 |
si4 r |
fad16(\p la fad la fad la fad la) |
\rt#4 { fad la } |
\rt#4 { sol si } |
sol16 si re' si sol si re' si |
fad la re' la fad la re' la |
\rt#4 { sol si } |
la8 la la la, |
