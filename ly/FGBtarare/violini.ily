\clef "treble" r8 |
<<
  \tag #'violino1 {
    <la fad' re''>4\f fad''8. re''16 |
    si'4 dod''8. dod''16 |
    re''8
  }
  \tag #'violino2 {
    fad'4-\sug\f la'8. fad'16 |
    sol'4 mi'8. mi'16 |
    fad'8
  }
>> re'16.[ re'32] re'8 re' |
re' <<
  \tag #'violino1 {
    re''16.[\p re''32] re''8 mi'' |
    do''4 do''8. re''16 |
    si'8
  }
  \tag #'violino2 {
    fad'16.-\sug\p[ fad'32] fad'4( |
    sol' la') |
    re'8
  }
>> \grace la'8 sol'16[\f fad'32 sol'] re'8 si |
sol2\fermata |
<<
  \tag #'violino1 {
    r8 si'\p red''8. red''16 |
    mi''8. si'16\mf mi''8. fad''16 |
    sol''8. sol''16 sol''16.\f fad''32 mi''16. re''32 |
    <dod'' la''>8
  }
  \tag #'violino2 {
    r4 r16. sol'32-\sug\p si'16. la'32 |
    sol'4 si'8-\sug\mf si'16. la'32 |
    sol'8. sol'16 sol'16.-\sug\f la'32 si'16. si'32 |
    << mi''8 \\ dod'' >>
  }
>> la16.[ la32] la8 la |
la4\fermata dod''8 dod''16. dod''32 |
<<
  \tag #'violino1 {
    fad''4\f fad''8. fad''16 |
    re''4. si'16\p dod'' |
    re''8 re'' re'' re'' |
    \grace re'' fad''4. re''8 |
    \grace dod'' si' si'4 si'8 |
    si'4 \grace mi''8 re'' dod''16 si' |
    la'8 la''([ fad'' re'']) |
    si' mi''([ sol'' mi'']) |
    re''4.( dod''8) |
  }
  \tag #'violino2 {
    dod''4-\sug\f lad'8. lad'16 |
    si'4 r |
    la'2-\sug\p~ |
    la' |
    re'~ |
    re'~ |
    re' |
    re'8 sol'([ si' sol']) |
    fad'16( la' fad' la' fad' la' sol' la') |
  }
>>
