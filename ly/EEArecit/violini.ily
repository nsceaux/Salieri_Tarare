\clef "treble" re'4.\f la8 re'8. la16 re'8. fad'16 |
mi'8. dod'16 la8. la16 la8. dod'16 mi'8. la'16 |
fad'4. la'8 re''8. la'16 fad'8. fad'16 |
sol'4 si'16 la' sol' fad' mi' re' dod' si la dod' mi' la' |
fad'4 r r2 |
r8 fad'\f la' re'' si'16 re'' dod'' si' dod'' mi'' re'' dod'' |
re''8 fad'' re'' la' si'4 \grace re'8 dod' si16 dod' |
re'4. la8 la4 la |
la2 r |
