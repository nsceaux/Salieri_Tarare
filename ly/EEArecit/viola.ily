\clef "alto" <fad la>4-\sug\f q q q |
<mi la> q q q |
<fad la> q q re' |
si r dod' r |
re' r r2 |
re'4-\sug\f fad' sol' la' |
si'8 re'' si' fad' sol'4 la' |
re'4. fad8 fad4 fad |
fad2 r |
