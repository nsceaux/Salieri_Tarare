\piecePartSpecs
#`((violino1 #:score-template "score-part-voix" #:system-count 2)
   (violino2 #:score-template "score-part-voix" #:system-count 2)
   (viola #:score-template "score-part-voix" #:system-count 2)
   (basso #:score-template "score-part-voix" #:system-count 2)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
