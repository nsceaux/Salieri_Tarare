\clef "alto/G_8" R1*4 |
\tag #'recit <>^\markup\italic { à Tarare d’un ton sévère et très affecté. }
r4 r8 la16 la la8 la16 la re'8. la16 |
fad4 r r2-\tag#'recit ^\markup\italic { (il sort) } |
R1*3 |
