\clef "bass" re4\f re re re |
dod dod dod dod |
re re fad re |
mi r dod r |
re r r2 |
re4\f fad sol la |
si8 re' si fad sol4 la |
re4. re8 re4 re |
re2 r |
