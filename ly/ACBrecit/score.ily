\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \keepWithTag #'basso \includeNotes "basso"
      \origLayout {
        s1*3\break s1*3 s2 \bar "" \pageBreak
        s2 s1*4\break s1*5\break s1*5\pageBreak
        s1*3\break s1*3 s2 \bar "" \break
      }
    >>
  >>
  \layout { system-count = 8 }
  \midi { }
}
