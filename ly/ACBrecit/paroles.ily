Pri -- vés des doux li -- ens que don -- ne la nais -- san -- ce ;
Quels se -- ront leurs rangs et leurs soins ?
Et com -- ment pour -- voir aux be -- soins
d’une aus -- si sou -- dai -- ne crois -- san -- ce ?

J’a -- mu -- se vos yeux un mo -- ment,
de leur for -- me pré -- ma -- tu -- ré -- e ;
s’ils pou -- vaient ai -- mer seu -- le -- ment,
vous re -- ver -- riez le règne heu -- reux d’As -- tré -- e.

Quel in -- té -- rêt peut les oc -- cu -- per tous ?

Nul, je crois.

Qu’ê -- tes- vous ? et que de -- man -- dez- vous ?

Nous ne de -- man -- dons pas, nous som -- mes.

Qui vous a mis au rang des hom -- mes ?

Qui l’a vou -- lu, que nous im -- porte à nous ?

Comme ils sont froids, sans pas -- si -- ons, sans gouts !
Que leur i -- gno -- rance est pro -- fon -- de !

Ah ! je les ai for -- més sans vous.
