\clef "bass" R1*3 |
mi!2 fa4 r |
reb1~ |
reb~ |
reb2 do4 r8\fermata r |
fa4-\sug\p r r fa |
sol r r2 |
do4 r r do |
fa4 r8 do16\f re32 mi fa8. do16 la,8. do16 |
fa,2 r |
fa2-\sug\p re4. fa8 |
sol2 si, |
do4 mi fa fa |
sol2:8-\sug\f sol,:8 |
do4 r r2 |
re4 r r2 |
r4 re(\f dod) la, |
re1~ |
re2~ re |
sol,4 r r2 |
R1*2 |
mi4 r r2 |
R1*2 |
fa4 r r2 |
r8 r16 mib mib4 r2 |
r re~ |
re1 |
