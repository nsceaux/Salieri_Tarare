\clef "treble" R1*3 |
<>^"sans sourdines" <<
  \tag #'violino1 { do''2~ do''4 }
  \tag #'violino2 { sol'2 lab'4 }
>> r4 |
<<
  \tag #'violino1 { sib'1~ | sib'~ | sib'2 do''4 }
  \tag #'violino2 { fa'1~ | fa'~ | fa'2 mi'!4 }
>> r8\fermata <>\p <<
  \tag #'violino1 {
    do'''8 |
    do'''4 la''!8. sib''16 do'''4 re'''8. do'''16 |
    sib''4. sib''16 do''' re'''8. do'''16 sib''8. la''16 |
    sol''4 sol''8. sol''16 do'''4. sib''8 |
    la''4
  }
  \tag #'violino2 {
    la'!8 |
    la'4 fa'8. sol'16 la'4 sib'8. la'16 |
    sol'4. sol'16 la' sib'8. la'16 sol'8. fa'16 |
    mi'4 mi'8. mi'16 <mi' sol>8. q16 q8. q16 |
    <fa' la>4
  }
>> r8 do''16\f re''32 mi'' fa''8. do''16 la'8. do''16 |
<fa' la>2 <<
  \tag #'violino1 {
    la'8.\p la'16 la'8. la'16 |
    re''2 fa''8 la''16 sol'' fa'' mi'' re'' do'' |
    si'2 re''4 mi''8. fa''16 |
    mi''4. do''8 la'8. re''16 fa''8. re''16 |
    \rp#3 { mi'16\f sol' do'' sol' } re' si' re'' si' |
    <do'' mi'>4
  }
  \tag #'violino2 {
    r2 |
    << la'2 \\ fa'-\sug\p >> la'8 fa'16 sol' la' sol' fa' mi' |
    re'2 sol' |
    sol'8. mi'16 do'8. do'16 do'8. fa'16 la'8. fa'16 |
    mi'4-\sug\f <mi' sol>2 <sol fa'>4 |
    <mi' sol>4
  }
>> r4 r2 |
<< la'4 \\ fad' >> r r2 |
r4 <<
  \tag #'violino1 {
    fad'8.\f mi'16 mi'4 la' |
    <fad' la>1~ |
    q2~ q |
    sol'4
  }
  \tag #'violino2 {
    la2-\sug\f <dod' mi'>4 |
    <fad' re'>1~ |
    q2~ q |
    si4
  }
>> r4 r2 |
R1*2 |
<<
  \tag #'violino1 { do''4 }
  \tag #'violino2 { sol'4 }
>> r4 r2 |
R1*2 |
<<
  \tag #'violino1 { la'4 }
  \tag #'violino2 { do'4 }
>> r4 r2 |
r8 r16 <<
  \tag #'violino1 { do''16 do''4 }
  \tag #'violino2 { la'16 la'4 }
>> r2 |
r <<
  \tag #'violino1 { sib'2~ | sib'1 }
  \tag #'violino2 { fa'2~ | fa'1 }
>>
