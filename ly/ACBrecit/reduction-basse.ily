\clef "bass" R1*3 |
mi!2 fa4 r |
<reb fa>1~ |
q~ |
q2 <sol do>4 r8\fermata r |
fa4 r r fa |
sol r r2 |
do4 r r do |
<fa la>4 r8 do16 re32 mi fa8. do16 la,8. do16 |
<fa fa,>2 r |
<fa la>2 re4. fa8 |
sol2 si, |
do4 mi fa fa |
sol2:8 <sol, mi>4:8 <sol, fa>:8 |
<do sol>4 r r2 |
re4 r r2 |
r4 <la re>~ <la dod> la, |
<re la>1~ |
q2~ q |
sol,4 r r2 |
R1*2 |
mi4 r r2 |
R1*2 |
fa4 r r2 |
r8 r16 mib mib4 r2 |
r re~ |
re1 |
