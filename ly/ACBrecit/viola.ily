\clef "alto" R1*3 |
do'2~ do'4 r |
fa1~ |
fa~ |
fa2 sol4 r8\fermata la'-\sug\p |
la'4 fa'8. sol'16 la'4 sib'8. la'16 |
sol'4. sol'16 la' sib'8. la'16 sol'8. fa'16 |
do'4 r r do' |
do' r8 do'16\f re'32 mi' fa'8. do'16 la8. do'16 |
fa2 r |
fa'-\sug\p re'4. fa'8 |
sol'2 si |
do'4 mi' fa' fa |
sol'2:8-\sug\f sol:8 |
do'4 r r2 |
re'4 r r2 |
r4 re'8.-\sug\f dod'16 dod'4 la |
re'1~ |
re'2~ re' |
re'4 r r2 |
R1*2 |
do'4 r r2 |
R1*2 | \allowPageTurn
fa'4 r r2 |
r8 r16 fa' fa'4 r2 |
r re'~ |
re'1 |
