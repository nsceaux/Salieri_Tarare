\clef "treble" R1*3 |
<sol' do''>2~ <lab' do''>4 r4 |
<fa' sib'>1~ |
q~ |
q2 <mi' do''>4 r8\fermata <>\p <la''! do'''>8 |
<la'' do'''>4 <fa'' la''>8. <sol'' sib''>16 <la'' do'''>4 <sib'' re'''>8. <la'' do'''>16 |
<sol'' sib''>4. <sol'' sib''>16 <la'' do'''> <sib'' re'''>8. <la'' do'''>16 <sol'' sib''>8. <fa'' la''>16 |
<mi'' sol''>4 q8. q16 << { do'''4. sib''8 } \\ { q8. q16 q8. q16 } >> |
<fa' la''>4 r8 do''16\f re''32 mi'' fa''8. do''16 la'8. do''16 |
<fa' la>2 la'8.\p la'16 la'8. la'16 |
re''2 <la'' fa''>8 q16 sol'' q <sol'' mi''> <fa'' re''> <mi'' do''> |
<re'' si'>2 <la' re''>4 mi''8. fa''16 |
<sol'' mi''>8. mi''16 do''8. do''16 <do'' la'>8. <fa'' re''>16 <la'' fa''>8. <fa'' re''>16 |
\rp#3 { mi'16\f sol' do'' sol' } re' si' re'' si' |
<do'' mi'>4 r4 r2 |
<la' fad' re'>4 r r2 |
r4 <re' fad'>8.\f <dod' mi'>16 <dod' mi'>4 <dod' mi' la'> |
<fad' re'>1~ |
q2~ q |
<si re' sol'>4 r4 r2 |
R1*2 |
<sol' do''>4 r4 r2 |
R1*2 |
<do' la'>4 r4 r2 |
r8 r16 <fa' la' do''>16 q4 r2 |
r <fa' sib'>2~ |
q1 |
