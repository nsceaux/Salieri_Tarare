\clef "bass" <>_\markup\italic parlé
^\markup\character-text Le Génie Récit le plus simple
r4 r8 mib mib mib mib mib |
lab4. lab8 do' do' sib do' |
lab lab r4 lab8 lab lab sib |
do'4 do'8 sol lab4 r8 fa16 fa |
sib4. sib8 reb'4 do'8 reb' |
sib4 r fa8 fa fa fa |
sib4 sib8 fa sol8 sol
\ffclef "soprano/treble" <>^\markup\character-text La Nature Légèrement
r8 do'' |
do''4 la'!8. sib'16 do''4 re''8. do''16 |
sib'2 r4 sib'8. la'16 |
sol'4 sol'8. sol'16 do''4. sib'8 |
la'4 fa' r2 |
r la'8 la' la' la' |
re''2 fa''4. re''8 |
si'2 re''4 mi''8. fa''16 |
mi''4. do''8 la'8. re''16 fa''8. re''16 |
do''2.( re''4) |
do''4
\ffclef "bass" <>^\markup\character-text Le Génie Récit simplement
r16 sol sol sol do'8 do' do'16 do' do' la |
fad4 r
\ffclef "soprano/treble" <>^\markup\character La Nature
re''4 r8 re'' |
la'4 r r2 |
\ffclef "bass" <>^\markup\character-text Le Génie Fièrement aux Ombres
r2 r4 la8 la |
re'4 r8 la16 la re'4 re'8 la |
si4
\ffclef "bass" <>^\markup\character-text L'Ombre d'Altamort sans aucune couleur
r8 sol16 sol sol4 sol8 si |
sol4 r8 sol re4 re8 r |
\ffclef "bass" <>^\markup\character Le Génie
r8 re' re' re' si8. si16 re'8 sol |
do'4 do'8 r
\ffclef "bass" <>^\markup\character-text L'Ombre d'Arthénée sans aucune couleur
r8 sol sol sol |
do'4 r8 sol mi mi mi fa |
sol4 r
\ffclef "bass" <>^\markup\character-text Le Génie à la Nature, avec étonnement
r8 do' do' do' |
la4 r16 la la la fa4 r8 fa |
la4 r8 la do'4 re'8 mib' |
la4 la8 sib fa4 fa8 r |
\ffclef "soprano/treble" <>^\markup\character-text La Nature modestement, au Génie
sib'4 r8 fa'16 sol' lab'8 lab' lab' sib' |

