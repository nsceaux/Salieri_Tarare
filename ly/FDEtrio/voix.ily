<<
  \tag #'(astasie basse) {
    \clef "soprano/treble" R2. |
    r4 r sib'8. sib'16 |
    mib''4 mib''4. mib''8 |
    lab'2 r8 lab' |
    fa''4. fa''8 fa'' fa'' |
    mib''4 do'' r |
    do'' do''8 do'' do'' do'' |
    reb''2 reb''8. do''16 |
    sib'4 mib''4. reb''8 |
    do''8. mib''16 fa''8. fa''16 fa''8. fa''16 |
    sol'4 <<
      \tag #'basse { s4 \ffclef "soprano/treble" <>^\markup\character Astasie }
      \tag #'astasie { r4 }
    >> r16*2/3 sib' sib' sib' sib' sib' |
    mi''8 mi''16 fa'' sol''4 sib'8. do''16 |
    lab'8 lab' r4 re''8 do'' |
    si'2 si'8. si'16 |
    do''2. |
    re''4 re''8. re''16 sol''8. fa''16 |
    mib''4 mib'' r |
    re''4 fa''8 re'' do'' si' |
    do''2 mib''8 do'' |
    si'2 si'8. si'16 |
    do''2. |
    re''?4 re''8. re''16 sol''8. fa''16 |
    mib''4 mib'' r |
    re''4 fa''8 re'' do'' si' |
    do'' sol'' sol'' fa'' fa'' mib'' |
    re'' lab'' sol'' fa'' mib'' re'' |
    do'' sol'' sol'' fa'' fa'' mib'' |
  }
  \tag #'tarare {
    \clef "tenor/G_8" R2. |
    r4 r sib8. sib16 |
    sol4 sol4. sol8 |
    lab2 r8 lab |
    reb'4. reb'8 reb' reb' |
    do'4 lab r |
    lab lab8 lab lab lab |
    sib2 sib8. lab16 |
    sol4 sol4. sib8 |
    lab8. lab16 reb'8. reb'16 reb'8. reb'16 |
    mib4 r r |
    R2.*7 |
    r4 r sol'8 mib' |
    re'2 re'8. re'16 |
    mib'2. |
    si4 si8. si16 si8. si16 |
    do'4 do' r |
    fa' lab'8 fa' mib' re' |
    mib' mib' mib' re' re' do' |
    fa' fa' mib' re' do' si |
    do' mib' mib' re' re' do' |
  }
  \tag #'(atar basse) {
    <<
      \tag #'basse { s2.*10 s16*2/3 \ffclef "bass" <>^\markup\character Atar }
      \tag #'atar { \clef "bass" R2.*10 | r16*2/3 }
    >> sib16*2/3 sib sib sib sib mib'4 <<
      \tag #'basse {  }
      \tag #'atar {
        r4 |
        R2.*2 |
        r8^\markup\italic { d’une voix concentrée } r16 sol sol8 sol, r4 |
        r8 r16 sol do'8. mib'16 sol4 |
        R2. |
        r8. sol16 do'8. sib16 lab8. sol16 |
        fa8 fa r fa16 fa sol8 sol16 sol |
        do8. do'16 do'8 do r4 |
        r8. sol16 si8. re'?16 sol4 |
        r8. sol16 do'8. mib'16 sol4 |
        R2. |
        r8. sol16 do'8. sib16 lab8. sol16 |
        fa8 fa r fa16 fa sol8 sol16 sol |
        do'4 r r |
        r8. fa'16 sol'8. sol16 sol8. sol16 |
        do'4 r r |
      }
    >>
  }
>>