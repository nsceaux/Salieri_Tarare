\clef "treble"
\twoVoices#'(oboe1 oboe2 oboi) <<
  { do''2 mib''8. do''16 | sib'2. | }
  { lab'2 do''8. lab'16 | sol'2. | }
>>
R2.*2 |
<>-\sug\mf \twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''2. | mib'' | }
  { reb'' | do'' | }
>>
R2.*5 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''2. | fa''8 fa''([ lab'' fa'' re'' do'']) | si'4 }
  { sib'!2. | lab' | re'?4 }
  { s2.\fp | s-\sug\f | s4-\sug\p }
>> r4 r |
R2.*4 |
\tag #'(oboi oboe1) <>^\markup { Clarinettes seules }
r8 \twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''4 sol'' sol''8 | sol''2.~ | sol''~ | sol'' | sol''4 }
  { sol'4 sol' sol'8 | sol'2.~ | sol'~ | sol' | sol'4 }
>> r4 r |
R2.*2 |
\tag #'(oboi oboe1) <>^\markup\center-align [tutti]
r8 <>-\sug\mf \twoVoices#'(oboe1 oboe2 oboi) <<
  { lab''8( sol'' fa'' mib'' re''!) | do''4 }
  { fa''8( mib'' re''? do'' si') | do''4 }
>> r4 r |
