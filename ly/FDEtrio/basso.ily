\clef "bass" r4 lab lab, |
mib2. |
mib4 mib mib |
lab2 r4 |
r8 reb(\mf fa lab reb' reb) |
lab2. |
lab4\p lab lab |
sol mib2 |
mib4 mib mib |
lab8. do'16\f reb8. reb16 reb8. reb16 |
mib8. mib16 mib4 r |
do4\fp~ do8. do16 do8. do16 |
fa2.\f |
sol4\p r r |
sol r r |
sol, r r |
do8. sol16\f do'8. sib16 lab8. sol16 |
fa2\pp sol4 |
do4 r r |
sol r r |
sol r r |
sol r r |
do8. sol16\f do'8. sib16 lab8. sol16 |
fa2\p sol4 |
do r r |
fa,8. \clef "tenor" fa'16\f sol'8. sol16\p sol8. sol16 |
\clef "bass" do4 r r |
