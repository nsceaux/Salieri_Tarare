\clef "treble" R2.*2 |
<<
  \tag #'violino1 {
    sib'4-.( sib'-. sib'-.) |
    lab'8 mib'([\mf lab' do'' mib'' lab']) |
    fa''2. |
    mib''8( lab'' do''' lab'' lab'' mib'') |
  }
  \tag #'violino2 {
    sol'4-.( sol'-. sol'-.) |
    lab'8 do'-\sug\mf([ mib' lab' do'' lab']) |
    lab'2~ lab'8 reb'' |
    do''2. |
  }
>>
<>\p \ru#3 { mib'16( lab' do'' lab') } |
mib'( sib' reb'' sib') sol'( sib' reb'' sib') reb''( sib' do'' lab') |
\ru#3 { sol'( sib' mib'' sib') } |
do''8. mib'16\f <<
  \tag #'violino1 {
    fa''8. fa''16 fa''8. fa''16 |
  }
  \tag #'violino2 {
    reb''8. reb''16 reb''8. reb''16 |
  }
>>
<sol' sib>8. q16 q4 r |
<<
  \tag #'violino1 {
    <sol' sib' mi''>8.\fp do''16 sol''8. sib'16 sib'8. do''16 |
    lab'8 fa''([\f lab'' fa'' re''\p do'']) |
    r16 si'-\sug\p si' si' si'2:16 |
    r16 do'' do'' do'' do''2:16 |
    r16 re'' re'' re'' re''2:16 |
    mib''2.\mf |
    re''4\p fa''8( re'' do'' si') |
    r16 do'' do'' do'' do''2:16 |
    r16 si' si' si' si'2:16 |
    r16 do'' do'' do'' do''2:16 |
    r16 re'' re'' re'' re''2:16 |
    mib''2. |
    re''4\p fa''8( re'' do'' si') |
    do''([ sol'']) sol''( fa'') fa''( mib'') |
    re''8 lab''\mf([ sol'' fa'' mib'' re'']) |
    do''[ sol'']\p sol''( fa'') fa''( mib'') |
    
  }
  \tag #'violino2 {
    <sol' sib'>8.-\sug\fp q16 q8. sol'16 sol'8. mi'16 |
    fa'2.-\sug\f |
    r16 re'-\sug\p re' re' re'2:16 |
    r16 mib' mib' mib' mib'2:16 |
    r16 si' si' si' si'2:16 |
    do''2.-\sug\mf |
    lab'2-\sug\p( sol'8) fa' |
    r16 mib' mib' mib' mib'2:16 |
    r16 re'? re' re' re'2:16 |
    r16 mib' mib' mib' mib'2:16 |
    r16 si' si' si' si'2:16 |
    do''2. |
    lab'2-\sug\p( sol'8) fa' |
    mib'([ mib'']) mib''( re'') re''( do'') |
    lab' fa''-\sug\mf([ mib'' re'' do'' si']) |
    do''[ mib'']-\sug\p mib''( re'') re''( do'') |
    
  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>
