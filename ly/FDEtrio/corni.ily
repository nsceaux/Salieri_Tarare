\clef "treble" R2.*13 |
<>-\sug\p \twoVoices#'(corno1 corno2 corni) <<
  { sol'2.~ | sol'~ | sol' | do'4 }
  { sol2.~ | sol~ | sol | do'4 }
>> r4 r |
R2.*2 |
\twoVoices#'(corno1 corno2 corni) <<
  { sol'2.~ | sol'~ | sol' | do'4 }
  { sol2.~ | sol~ | sol | do'4 }
>> r4 r |
R2.*2 |
r4 r8. <>\mf \twoVoices#'(corno1 corno2 corni) <<
  { sol'16 sol'8. sol'16 | do'4 }
  { sol'16 sol'8. sol'16 | do'4 }
>> r4 r |
