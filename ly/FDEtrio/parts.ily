\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso)
   (oboi #:score "score-oboi")
   (clarinetti #:score "score-oboi")
   (fagotti #:score-template "score-part-voix" #:tag-notes fagotti)
   (corni #:score-template "score-part-voix" #:tag-notes corni
          #:tag-global () #:music ,#{ s2.*13 <>^"Cors in C" #})
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
