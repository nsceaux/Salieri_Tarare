\clef "alto" R2.*2 |
mib'4 mib' mib' |
lab'2 r4 |
r8 reb'(\mf fa' lab' reb'' reb') |
lab'2. |
lab'4\p lab' lab' |
sol' mib'2 |
mib'4 mib' mib' |
lab'8. do''16\f reb'8. reb'16 reb'8. reb'16 |
mib'8. mib'16 mib'4 r |
do'4\fp~ do'8. do'16 do'8. do'16 |
lab'2.-\sug\f |
r16 sol'-\sug\p sol' sol' sol'2:16 |
r16 sol' sol' sol' sol'2:16 |
r16 sol' sol' sol' sol'2:16 |
sol'4 r r |
fa'2-\sug\p( mib'8) re'? |
r16 sol' sol' sol' sol'2:16 |
r16 sol' sol' sol' sol'2:16 |
r16 sol' sol' sol' sol'2:16 |
r16 sol' sol' sol' sol'2:16 |
sol'4 r r |
fa'2-\sug\p( mib'8) re'? |
<<
  { \slurDashed do'( sol') sol'([ fa') fa'( mib']) |
    re' lab'([ sol' fa' mib' re']) |
    do'[ sol'] sol'[( fa') fa'( mib')] | \slurSolid } \\
  { do' mib' mib'[ re' re' do'] |
    lab fa'-\sug\mf[ mib' re' do' si] |
    do'[ mib']-\sug\p mib'[ re' re' do'] | }
>>
