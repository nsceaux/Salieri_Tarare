\clef "tenor"
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { do'2 mib'8. do'16 | sib2.~ | sib | lab4 }
  { lab2 do'8. lab16 | sol2.~ | sol | lab4 }
>> r4 r |
<>-\sug\mf \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { fa'2. | mib' | }
  { reb'2. | do' | }
>>
<>^"Solo" mib'2.-\sug\p~ |
mib'~ |
mib' |
mib'4 r r |
R2. |
\clef "bass" \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sib2. | lab | }
  { sol2. | fa | }
  { s2.-\sug\fp | s-\sug\f }
>>
R2.*12 |
\clef "tenor" r8. fa'16\f sol'8. sol16\p sol8. sol16 |
do'4 r r |
