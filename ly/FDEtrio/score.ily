\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        instrumentName = \markup\center-column { Hautbois et Clarinettes }
        shortInstrumentName = \markup\center-column { Htb. Cl. }
        \consists "Metronome_mark_engraver"
      } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small in C }
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff <<
      \new Staff \with {
        instrumentName = \markup\character Astasie
        shortInstrumentName = \markup\character As.
      } \withLyrics <<
        \global \keepWithTag #'astasie \includeNotes "voix"
      >> \keepWithTag #'astasie \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\character Tarare
        shortInstrumentName = \markup\character Ta.
      } \withLyrics <<
        \global \keepWithTag #'tarare \includeNotes "voix"
      >> \keepWithTag #'tarare \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\character Atar
        shortInstrumentName = \markup\character At.
      } \withLyrics <<
        \global \keepWithTag #'atar \includeNotes "voix"
      >> \keepWithTag #'atar \includeLyrics "paroles"
    >>
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s2.*5\break s2.*5\pageBreak
        s2.*3\break s2.*5\pageBreak
        s2.*5\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
