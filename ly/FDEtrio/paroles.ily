\tag #'(astasie tarare basse) {
  Le tré -- pas nous at -- tend…
  en -- core u -- ne mi -- nu -- te,
  et notre a -- mour cons -- tant
  ne se -- ra plus en butte
  aux coups d’un noir sul -- tan.
}
\tag #'(atar basse) {
  Ar -- rê -- tez un mo -- ment !
}
\tag #'(astasie basse) {
  Je me frappe à l’ins -- tant
  que sa loi s’ex -- é -- cu -- te.
  Sur ton cœur pal -- pi -- tant,
  tu sen -- ti -- ras ma chu -- te,
  et tu mour -- ras con -- tent.
}
\tag #'atar {
  O ra -- ge ! af -- freux tour -- ment !
  C’est moi, c’est moi qui lut -- te,
  et leur cœur est con -- tent.
  O ra -- ge ! af -- freux tour -- ment ! af -- freux tour -- ment !
  C’est moi, c’est moi qui lut -- te,
  et leur cœur est con -- tent.
  O rage ! af -- freux tour -- ment !
}
\tag #'(astasie basse) {
  Sur ton cœur pal -- pi -- tant
  tu sen -- ti -- ras ma chu -- te,
  et tu mour -- ras con -- tent.
  Tu sen -- ti -- ras ma chute,
  et tu mour -- ras con -- tent.
  Tu sen -- ti -- ras ma
}
\tag #'tarare {
  Sur mon cœur pal -- pi -- tant
  je sen -- ti -- rai ta chu -- te,
  et je mour -- rai con -- tent.
  Je sen -- ti -- rai ta chute,
  et je mour -- rai con -- tent.
  Je sen -- ti -- rai ta
}
