\clef "bass" si4(-\sug\f la sold fad) |
mi1-\sug\p |
la4-\sug\f( sold fad mi) |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'1 |
    si2 la |
    sold red' |
    mi'2.\fermata }
  { la1 |
    sold2 fad |
    mi la |
    sold2.\fermata }
  { s1-\sug\p }
>> r4 |
R2.*2 |
r4 dod'8.-\sug\ff re'16 re'8.\trill dod'32 re' |
mi'4 r r |
R2.*2 |
r4 sold8.-\sug\ff la16 la8.\trill sold32 la |
si2 r4 |
R2. |
sold,8( sid, red\cresc sold mi dod) |
sold,(\f sid, red sold mi dod) |
sold,2 r4 |
R2.*5 |
\clef "tenor" fa'2.-\sug\f~ |
fa'8. \clef "bass" fa16-\sug\ff fa8 fa fa fa |
mi2.\fermata |
