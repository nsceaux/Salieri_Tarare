\clef "tenor/G_8" R1*6 |
r2 r4 mi |
<>^\markup\italic { Lent et bien douloureux }
la4. la8 dod'8. mi'16 |
re'4. si16 la sold8 mi'16 si |
dod'4 r r |
r4 mi'4. sol8 |
fad2 re'8. dod'16 |
si8. si16 si8 si dod'8. la16 |
sold4 sold r4 |
r r si8. re'16 |
dod'8. dod'16 dod'8 mi' red' dod' |
sold2. |
sold |
r4 r r8^"Récit" sold16 sold |
dod'4. dod'8 mi' red'16 dod' |
si4. si8 si dod' |
la2 la8 la16 si |
sold8 sold r4 r8 mi |
sold8 sold16 la si4 si8. dod'16 |
re'2. |
R2. |
r4 r r8 mi |
