\clef "bass" si,4(\f la, sold, fad,) |
mi,1\p |
la4(\f sold fad mi) |
red1\p |
mi~ |
mi~ |
mi2.\fermata r4 |
la,2.\fp |
si,2\fp mi4 |
la,8 r la,8.\ff si,16 si,8. si,16 |
dod2.\p |
re2.~ |
re2 red4 |
mi8 r mi8.\ff fad16 fad8. fad16 |
sold2. |
la8.-\sug\p la,16 la4 la, |
sold,8( sid, red\cresc sold mi dod) |
sold,(\f sid, red sold mi dod) |
sold,2 r4 |
r16 mi\mf mi8 r4 r |
r16 red red8 r4 r |
R2. |
r16 mi mi8 r4 r |
R2. |
fa2.-\sug\f~ |
fa8. fa16-\sug\ff fa8 fa fa fa |
mi2.\fermata |
