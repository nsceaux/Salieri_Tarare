\clef "treble"
<<
  \tag #'violino1 {
    re'1\f~ |
    re'8(\mf sold' si' sold' re'' si' sold' re') |
    dod'1\f |
    do'4.\p do'8 do'4 do' |
    si2( red'\sf |
    mi' la') |
    sold'2.\fermata r4 |
    r8. la'16\f la'2 |
    r8. sold'16 sold'2 |
    la'4
  }
  \tag #'violino2 {
    si1-\sug\f~ |
    si-\sug\p |
    la-\sug\f |
    la4.-\sug\p la8 la4 la |
    sold2( la-\sug\sf |
    sold red') |
    mi'2.\fermata r4 |
    r8. <dod' mi'>16-\sug\f q2 |
    r8. <<
      { mi'16 mi'2 | } \\
      { re'16 re'2 | }
    >>
    <dod' mi'>4
  }
>> dod'8.\ff re'16 re'8.\trill dod'32 re' |
<<
  \tag #'violino1 {
    mi'2.-\sug\p |
    re'8 re'[( fad' la' re'' dod'')] |
    si' si'4 si'8 dod'' la' |
    sold'
  }
  \tag #'violino2 {
    mi'4-\sug\p mi''4.(\sf sol''8) |
    fad'' re'[ fad' fad' fad' fad'] |
    fad'2 fad'4 |
    si8
  }
>> r sold8.\ff la16 la8.\trill sold32 la |
si8 si([ mi' sold']) <<
  \tag #'violino1 {
    si'8.\p re''16 |
    dod''8 dod''4 mi''8 red'' dod'' |
    sold'16 red'8 red'\cresc red' red'16 mi' mi'8 mi'16 |
    red'\f red'8 red' red' red'16 mi' mi'8 mi'16 |
    red'2. |
  }
  \tag #'violino2 {
    mi'4-\sug\p |
    mi'8 mi'4 mi'8 fad' fadd' |
    sold'16 sid8 sid-\sug\cresc sid sid16 dod' dod'8 dod'16 |
    sid-\sug\f sid8 sid sid sid16 dod' dod'8 dod'16 |
    sid2. |
  }
>>
<<
  \tag #'violino1 {
    r16 dod''\mf dod''8 r4 r |
    r16 si' si'8 r4 r |
  }
  \tag #'violino2 {
    r16 sold'-\sug\mf sold'8 r4 r |
    r16 fad' fad'8 r4 r |
  }
>>
R2. |
<<
  \tag #'violino1 {
    r16 si' si'8 r4 r |
  }
  \tag #'violino2 {
    r16 sold' sold'8 r4 r |
  }
>>
R2. |
r8 <<
  \tag #'violino1 {
    re'''8\f~ re'''16( si'' sold'' fa'') re''( si' sold' fa') |
    re'4~ re'16. <si re'>32\ff q16. q32 q16. q32 q16. q32 |
    q2.\fermata
  }
  \tag #'violino2 {
    re''8-\sug\f~ re''16( si' sold' fa') re'( si sold fa') |
    si4~ si16. sold32-\sug\ff sold16. sold32 sold16. sold32 sold16. sold32 |
    sold2.\fermata |
  }
>>
