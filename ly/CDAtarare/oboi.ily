\clef "treble"
\twoVoices#'(oboe1 oboe2 oboi) <<
  { re''1~ |
    re'' |
    dod'' |
    do'' |
    si'2( red'' |
    mi'' la'') |
    sold''2.\fermata }
  { si'1~ |
    si' |
    la' |
    la' |
    sold'2 fad' |
    sold' red'' |
    mi''2.\fermata }
  { s1\f s\p s\f s\p s2 s\f }
>> r4 |
R2.*3 |
r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''2 | fad''4 }
  { mi''2 | re''4 }
>> r4 r |
R2.*2 |
r4 r <>-\sug\p \twoVoices#'(oboe1 oboe2 oboi) <<
  { re''4 | dod'' }
  { si'4 | la' }
>> r4 r |
r8 \twoVoices#'(oboe1 oboe2 oboi) <<
  { red''4 red''8\noBeam mi'' mi''( |
    red''?) red''4 red''8 mi''8. mi''16 |
    red''2. | }
  { sid'4 sid'8\noBeam dod'' dod''( |
    sid') sid'4 sid'8 dod''8. dod''16 |
    sid'2. | }
  { s8*5-\sug\cresc | s1-\sug\f }
>>
R2.*5 |
<>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { re''2.~ | re''4 }
  { si'2.~ | si'4 }
>> r4 r |
R2.^\fermataMarkup |
