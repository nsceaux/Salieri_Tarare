\key fad \minor \tempo "Plus lent" \midiTempo#120
\time 2/2 \midiTempo#100 s1*7 \bar "|."
\time 3/4 \segnoMark s2.*12 \bar "|." \fineMark
\midiTempo#100 s2.*8 \bar "|." \endMark "[Dal Segno]"
