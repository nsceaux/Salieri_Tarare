\score {
  <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \includeNotes "voix"
    >> { \set fontSize = -2 \includeLyrics "paroles" }
    \new GrandStaff <<
      \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
      \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
    >>
  >>
  \layout { }
}
