\clef "alto" sold4-\sug\f( la si la) |
sold1-\sug\p |
la2.-\sug\f sold4 |
fad4.-\sug\p fad8 fad4 fad |
sold2 fad-\sug\sf |
mi fad' |
si2.\fermata r4 |
la2.-\sug\fp |
si2-\sug\fp si4 |
la4 la8.-\sug\ff si16 si8. si16 |
dod'2.-\sug\p |
re'2.~ |
re'2 red'4 |
mi'8 r sold8.-\sug\ff la16 la8.\trill sold32 la |
si2 mi'4-\sug\p |
la8 mi4 mi8 fad fadd |
sold16 sold8 sold-\sug\cresc sold sold sold sold16 |
sold-\sug\f sold8 sold sold sold sold sold16 |
sold2. |
r16 mi'\mf mi'8 r4 r |
r16 red' red'8 r4 r |
R2. |
r16 mi' mi'8 r4 r |
R2. |
<si sold'>2.-\sug\f~ |
q8. fa16-\sug\ff fa8 fa fa fa |
mi2.\fermata |
