\clef "treble" \transposition fa
R1*4 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 re'' mi''2 | re'' do''4~ do''8 }
  { sol'4 sol' do''2 | re'' do''4~ do''8 }
>> r8 |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''8 mi''16. mi''32 mi''8 mi'' fad''4 }
  { mi''8 mi''16. mi''32 mi''8 mi'' fad''4 }
>> r4 |
R1*4 |
