\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        \consists "Metronome_mark_engraver"
        \flautiInstr
      } << \global \keepWithTag #'flauti \includeNotes "flauti" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Hautbois et Clarinettes }
        shortInstrumentName = \markup\center-column { Htb. Cl. }
      } << \global \keepWithTag #'oboi \includeNotes "flauti" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \small en Ut }
        shortInstrumentName = "Tr."
        \haraKiriFirst
      } <<
        { s1*4 <>^"Trompettes en Ut" }
        \keepWithTag#'() \global \keepWithTag #'trombe \includeNotes "trombe"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small en Fa }
        shortInstrumentName = "Cor."
        \haraKiriFirst
      } <<
        { s1*4 <>^"Cors en Fa" }
        \keepWithTag#'() \global \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with { \timpaniInstr \haraKiri } <<
        \keepWithTag#'() \global \includeNotes "timpani"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { 
          \consists "Metronome_mark_engraver"
        } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Bassons Basses }
      shortInstrumentName = \markup\center-column { Bn. B. }
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout { s1*3\pageBreak s1*4\break }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" } 
  }
  \midi { }
}
