\clef "treble"
R1*4 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 do''8. do''16 do''4 }
  { do''4 do''8. do''16 do''4 }
>> r4 |
R1 |
r2 r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''4 | mi''8 re''16. re''32 re''8 re'' mi''4 }
  { mi''4 | mi''8 re''16. re''32 re''8 re'' mi''4 }
>> r4 |
R1*4 |
