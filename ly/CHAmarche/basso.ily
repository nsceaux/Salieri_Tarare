\clef "bass" la,8\f la,16. la,32 la,8 do mi8. mi,16 mi8 re |
do8. do16 sold,8. sold,16 la,8 mi16.[ mi32] mi8 mi |
mi8 re16. do32 re4. do16. si,32 do8 la, |
si,8 sold, la,16. si,32 do16. re32 mi4 mi,8 r |
r8 do mi do fa do la, fa, |
mi,4 mi re~ re8 r |
r8 re fa mi16 re do8 si, do la, |
fa4 fa, mi,8 mi mi16. re32 do16. si,32 |
la,8\p\noBeam la,16. la,32 la,8 do mi8. mi,16 mi8 re |
do8. do16 sold,8. sold,16 la,8\noBeam mi16. mi32 mi8 mi |
mi re16. do32 re4. do16. si,32 do8 la, |
re4 mi la,16 do mi la la,8 r |
