\clef "alto" la8\f la16. la32 la8 do' mi'8. mi16 mi'8 re' |
do'8. do'16 sold8. sold16 la8 mi'16.[ mi'32] mi'8 mi' |
mi'8 re'16. do'32 re'4. do'16. si32 do'8 la |
si8 sold la16. si32 do'16. re'32 mi'4 mi8 r |
r8 do' mi' do' fa' do' la fa |
sol2 fa4~ fa8 r |
r8 re' fa' mi'16 re' do'8 si do' la |
fa2 mi8 mi' mi'16. re'32 do'16. si32 |
la8\p\noBeam la16. la32 la8 do' mi'8. mi16 mi'8 re' |
do'8. do'16 sold8. sold16 la8\noBeam mi'16. mi'32 mi'8 mi' |
mi' re'16. do'32 re'4. do'16. si32 do'8 la |
re'4 mi' la16 do' mi' la' la8 r |
