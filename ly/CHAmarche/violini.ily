\clef "treble" <mi' do'' la''>8-\sug\f la16.[ la32] la8 do' mi'4. <<
  \tag #'violino1 {
    sold'8 |
    la'8. la'16 si'8. si'16 do''8 si'4 r8 |
  }
  \tag #'violino2 {
    si8 |
    mi'8. mi'16 mi'8. mi'16 la'8 sold'4 r8 |
  }
>>
fa'4. re'16 fa' mi'4. do'16. mi'32 |
re'8-! si-! do'-! la-! <<
  \tag #'violino1 { mi'8. fa'16 mi'8 r | }
  \tag #'violino2 { sold4~ sold8 r | }
>>
<sol mi' do''>4 do' la la' |
sib'8 sol' mi' dod' re' \grace mi'16 re'32 dod' re' mi' re'8 la |
fa'4 sold la mi'~ |
mi'8 re'16 do' si8 la mi'4 r |
r8 la16.-\sug\p la32 la8 do' mi'4. <<
  \tag #'violino1 {
    sold'8 |
    la'8. la'16 si'8. si'16 do''8 si'4 r8 |
  }
  \tag #'violino2 {
    si8 |
    mi'8. mi'16 mi'8. mi'16 la'8 sold'4 r8 |
  }
>>
fa'4. re'16 fa' mi'4. la'8 |
si8( fa') mi'( sold) la8. do'16 la8 r |
