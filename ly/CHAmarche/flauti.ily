\clef "treble" r8 <>-\sug\f <<
  \tag #'(flauto1 oboe1 flauti oboi) \new Voice {
    \tag #'(flauti oboi) \voiceOne
    la'16. la'32 la'8 do'' mi''4. sold''8 |
    la''8. la''16 si''8. si''16 do'''8 si''4
  }
  \tag #'(flauto2 oboe2 flauti oboi) \new Voice {
    \tag #'(flauti oboi) \voiceTwo
    la'16. la'32 la'8 do'' mi''4. si'8 |
    mi''8. mi''16 mi''8. mi''16 la''8 sold''4
  }
>> r8 |
<<
  \tag #'(flauto1 oboe1 flauti oboi) \new Voice {
    \tag #'(flauti oboi) \voiceOne
    fa''4. re''16. fa''32 mi''4. do''16. mi''32 |
    re''8 si' do'' la' mi''8. fa''16 mi''8
  }
  \tag #'(flauto2 oboe2 flauti oboi) \new Voice {
    \tag #'(flauti oboi) \voiceTwo
    fa''4. re''16. fa''32 mi''4. do''16. mi''32 |
    re''8 si' do'' la' sold'4~ sold'8
  }
>> r8 |
<<
  \tag #'(flauto1 flauto2 flauti) {
    \twoVoices#'(flauto1 flauto2 flauti) <<
      { do'''2 la'' | }
      { mi''2 la'' | }
    >>
    <>^\markup\center-align "a 2"
    sib''8 sol''' mi''' dod''' re''' re'''32 dod''' re''' mi''' re'''8 r |
    fa'''4 sold'' la'' mi'''~ |
    mi'''8 re'''16. do'''32 si''8 la'' mi'''4 r |
    r8 <>\p \twoVoices#'(flauto1 flauto2 flauti) <<
      { la'16. la'32 la'8 do'' mi''4. sold''8 |
        la''8. la''16 si''8. si''16 do'''8 si''4 }
      { la'16. la'32 la'8 do'' mi''4. si'8 |
        mi''8. mi''16 mi''8. mi''16 la''8 sold''4 }
    >> r8 |
    <>^"a 2" fa''4. re''16 fa'' mi''4. la''8 |
    si'8 fa'' mi'' sold' la'8. do''16 la'8 r |
  }
  \tag #'(oboe1 oboe2 oboi) {
    <>^"a 2" do'''4 do'' la' la'' |
    sib''8 sol'' mi'' dod'' re'' \grace mi''16 re''32 dod'' re'' mi'' re''8 la' |
    fa''4 sold' la' mi'' |
    mi''8 re''16. do''32 si'8 la' mi''4 r |
    r8 la'16.-\sug\p la'32 la'8 do'' mi''2~ |
    mi''~ mi''4 r |
    r2 r8 la'16 sold' la'4 |
    si'8 fa'' mi'' sold' la'8. do''16 la'8 r |
  }
>>
