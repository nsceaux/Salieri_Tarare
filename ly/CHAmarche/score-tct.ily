\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Trompettes \small en Ut }
    } <<
      \keepWithTag#'() \global \keepWithTag #'trombe \includeNotes "trombe"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Cors \small en Fa }
    } <<
      \keepWithTag#'() \global \keepWithTag #'corni \includeNotes "corni"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Timballes }
    } <<
      \keepWithTag#'() \global \includeNotes "timpani"
    >>
  >>
  \layout {
    indent = \largeindent
    system-count = 2
  }
}
