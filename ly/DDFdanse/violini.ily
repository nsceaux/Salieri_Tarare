\clef "treble"
<<
  \tag #'violino1 {
    la'4\mf mi' r8 dod'' |
    dod''8.( la'16) si'4 r |
    si'4( mi'') r8 re'' |
    re''8.( si'16) dod''4 r |
    mi''4. la''8( fad'' re'') |
    dod''4. fad''8( re'' si') |
    la'4( re'') r8 dod'' |
    dod''4( si') r |
  }
  \tag #'violino2 {
    dod'4-\sug\mf dod' r8 mi' |
    mi'4 mi' r |
    sold' sold' r8 si' |
    si'4( la') r |
    R2. |
    mi'4. la'8( fad' re') |
    dod'4( si) r8 la |
    la4( sold) r |
  }
>>
si8( mi' sold'4.) fad'16 mi' |
red'8( fad' la'4) r |
sold'8 si'
<<
  \tag #'violino1 {
    re''!4. dod''16 si' |
    la'8( dod'') mi''4
  }
  \tag #'violino2 {
    re''!4 re' |
    dod' dod'
  }
>> r4 |
<re' la' fad''>4\f re'8 <<
  \tag #'violino1 {
    fad''8(\p re'' si') |
    sold'8.( si'16) mi'4 r8 si' |
    dod''8. mi''16 la'4. si'8 |
    si'2( dod''8) r |
  }
  \tag #'violino2 {
    la'8-\sug\p( fad') fad' |
    si4 si r |
    r dod'4. sold'8 |
    sold'2( la'8) r |
  }
>>
<re' la' fad''>4\f re'8 <<
  \tag #'violino1 {
    fad''8( re'' si') |
    sold'8.( si'16) mi'4 r8 si' |
    dod''8. mi''16 la'4. sold'8 |
    la'16*2/3 fad' mi' re' dod' si la4 r\fermata |
  }
  \tag #'violino2 {
    la'8( fad') fad' |
    si4 si r |
    r dod'4.( si8) |
    la4 r r\fermata |
  }
>>
