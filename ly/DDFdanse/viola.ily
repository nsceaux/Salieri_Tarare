\clef "alto" la16*2/3(\mf dod') mi' la'-! mi'-! dod'-! la8 la'-! dod''-! la'-! |
\grace si'8 la'16*2/3 sold' la' dod'' si' la' sold'( la' si') mi' mi' mi' mi'16-! sold'-! si'-! mi''-! |
mi'16*2/3( sold' si') si' si' si' sold'( si' mi'') mi'' mi'' mi'' mi'' si' la' sold' fad' mi' |
mi'( sold' fad' mi' fad' sold') la'( sold' la') si'( la' si') dod''( si' dod'') re''( dod'' re'') |
dod''( re'' mi'') la' la' la' dod''( re'' mi'') la' la' la' re' mi' fad' sold' la' si' |
la'( dod'' mi'') la' la' la' dod''( re'' mi'') la' la' la' la' si' la' sold' la' si' |
mi' mi'' re'' dod'' si' la' sold' si' la' sold' fad' mi' la' mi' re' dod' si la |
la dod' mi' la' dod'' mi'' mi''[ red'' mi''] si'([ la' si']) sold'([ fad' sold']) mi'([ red' mi']) |
si( mi' sold') si' si' si' si'8 si-! si-! si-! |
si16*2/3 red''( dod'') si'-! red''( dod'') si'-! red''( dod'') si'-! red''( dod'') si'-! red''( dod'' si' dod'' red'') |
mi''( si' sold') mi' mi' mi' mi'2:8 |
mi'16*2/3[ mi'' dod''] la'[ dod'' la'] mi'[ la' mi'] dod'[ mi' dod'] la16 la'( sold'! sol') |
fad'16(\f la' re'' dod'' re'' dod'' re'' dod'' re''\p dod'' re'' dod'') |
re''16*2/3 dod'' si' la' sold' fad' sold' si' la' sold' la' si' mi' sold' si' mi'' mi'' mi'' |
mi''16( red'' mi'' red'' mi'' red'' mi'' red'' mi'' re'' dod'' si') |
si'16*2/3-! re''( dod'' si' dod'' re'') mi'-! re''( dod'' si' dod'' re'') dod''( re'' mi'') la' la' la' |
fad'16( la' re'' dod'' re'' dod'' re'' dod'' re'' dod'' re'' dod'') |
re''16*2/3 dod'' si' la' sold' fad' sold' si' la' sold' la' si' mi'( sold' si') mi'' mi'' mi'' |
mi''16 red'' mi'' red'' mi'' red'' mi'' red'' mi''( re'' dod'' si') |
la'16*2/3 fad' mi' re' dod' si la4 r\fermata |
