\clef "bass" la,4\mf la, r |
la, mi r |
mi mi r |
mi la, r |
r8 la la,2 |
r8 la la,2 |
r4 sold,( la,) |
mi mi r |
mi mi r |
si, si, r |
mi mi r |
la la, r |
re2\f re4\p mi mi r |
r mi mi |
mi2 la,8 r |
re2\f re4 |
mi mi r |
la, mi mi |
la, la, r\fermata |
