\score {
  \new StaffGroup <<
    \new GrandStaff \with { \violiniInstr } <<
      \new Staff <<
        \global \keepWithTag #'violino1 \includeNotes "violini"
      >>
      \new Staff <<
        \global \keepWithTag #'violino2 \includeNotes "violini"
      >>
    >>
    \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    \new Staff \with { \bassoInstr } <<
      \global \includeNotes "basso"
      \origLayout {
        s2.*4\pageBreak
        s2.*4\break s2.*6\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
