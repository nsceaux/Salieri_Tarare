<<
  \tag #'(vdessus1 basse) {
    \clef "soprano/treble" <>^\markup\italic { aux danseurs }
    re''8 |
    re''4. si'16[ do''] re''8 re'' re'' re'' |
    re''8.[ sol''16] re''4. re''8 \grace mi'' re'' do''16[ si'] |
    la'4 sol' la'4. re''8 |
    si'4. \tag #'basse \ffclef "soprano/treble"
  }
  \tag #'vdessus2 { \clef "soprano/treble" r8 | R1*3 | r4 r8 }
>>
<<
  \tag #'vdessus1 { r4 r8 | R2.*5 | r8 }
  \tag #'(vdessus2 basse) {
    r8 r dod'' |
    re''4.~ re''4 la'8 |
    fad''4 mi''8 re''4 dod''8 |
    si'4 si'8 r4 r8 |
    r4 r8 si' dod'' red'' |
    mi''4 sol''8 si'4 dod''8 |
    <<
      \tag #'vdessus2 { re''4*1/2 }
      \tag #'basse { re''8 \ffclef "soprano/treble" }
    >>
  }
>>
<<
  \tag #'(vdessus1 basse) {
    re''8 re'' do''16 re'' mi''4 \grace re''8 do'' si'16[ do''] |
    re''4 si'8 si' do'' la'16 do'' si'8 la'16 sol' |
    re''4. \tag #'basse \ffclef "soprano/treble"
  }
  \tag #'vdessus2 { s8 r4 r2 | R1 | r4 r8 }
>>
<<
  \tag #'vdessus1 { r4 r8 | R2.*4 | r8 }
  \tag #'(vdessus2 basse) {
    re''4 re''8 |
    mi'' re'' do'' sol''[ fad''] mi'' |
    re''4 re''8 r4 r8 |
    r8 r re'' mi''8. fad''16 sol''8 |
    si'4( re''8) la'4 re''8 |
    <<
      \tag #'vdessus2 { si'4*1/2 }
      \tag #'basse { si'8 \ffclef "soprano/treble" }
    >>
  }
>>
<<
  \tag #'(vdessus1 basse) {
    re''8 re'' do''16 re'' mi''4 \grace re''8 do'' si'16[ do''] |
    re''4 si'8 si' do'' la'16 do'' si'8 la'16 sol' |
    re''4. \tag #'basse \ffclef "soprano/treble"
  }
  \tag #'vdessus2 { s8 r4 r2 | R1 | r4 r8 }
>>
<<
  \tag #'vdessus1 { r4 r8 | R2.*4 | r8 r }
  \tag #'(vdessus2 basse) {
    re''4 re''8 |
    mi'' re'' do'' sol''[ fad''] mi'' |
    re''4 re''8 r4 r8 |
    r8 r re'' mi''8. fad''16 sol''8 |
    si'4( re''8) la'4 re''8 |
    <<
      \tag #'vdessus2 { si'4.*2/3 }
      \tag #'basse { si'4 \ffclef "soprano/treble" }
    >>
  }
>>
<<
  \tag #'(vdessus1 basse) {
    re''8 sol''8. fad''16 sol''8 |
    <<
      \tag #'vdessus1 { re''4.*2/3 }
      \tag #'basse { re''4 \ffclef "soprano/treble" }
    >>
  }
  \tag #'vdessus2 { s8 r4 r8 | r8 r }
>>
<<
  \tag #'vdessus1 {
    s8 <>^\markup\italic { Le pas des quatre danseurs reprend et s’achève. }
    r4 r8 | R2.*2 |
  }
  \tag #'(vdessus2 basse) {
    re''8 mi''8. fad''16 sol''8 |
    si'4( re''8) la'4 re''8 |
    sol'4. r4 r8 |
  }
>>
