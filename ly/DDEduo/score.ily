\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff <<
      \new Staff \with {
        instrumentName = \markup\center-column {
          \smallCaps Spinette en Bergère coquette
        }
      } \withLyrics <<
        \global \keepWithTag #'vdessus1 \includeNotes "voix"
      >> \keepWithTag #'vdessus1 \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\center-column {
          Une Bergère sensible
        }
      } \withLyrics <<
        \global \keepWithTag #'vdessus2 \includeNotes "voix"
      >> \keepWithTag #'vdessus2 \includeLyrics "paroles"
    >>
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s8 s1*3 s2.\pageBreak
        s2.*5 s1\break s1 s2.*2\break s2.*3 s1 s2 \bar "" \pageBreak
        s2 s2.*4\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
