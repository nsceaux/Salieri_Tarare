\clef "bass" r8 |
sol1\p~ |
sol4 sol sol, sol, |
re1 |
<>^"Violoncelli" sol4-\sug\mf( fad8 sol4) mi8\p |
re2. |
r8 \slurDashed re'(\sf dod') r si(\sf la) | \slurSolid
sol r r sol r r |
sol4. r4 r8 |
sol4. sol4 la8 |
re1_"[Tutti]"~ |
re |
re4. \clef "tenor" <>_"Violoncelli" si4 si8 |
do'( re' mi') mi'( re' do') |
si4. r4 r8 |
\clef "bass" si,4. do |
re2. |
sol,1_"Tutti"~ |
sol,2 fad,4 sol,16-! la,-! si,-! do-! |
re4. \clef "tenor" <>_"Violoncelli" si4 si8 |
do'( re' mi') mi'( re' do') |
si4. r4 r8 |
\clef "bass" si,4. do |
re2. |
sol,4. r4 r8 |
si,4.( do) |
re2. |
sol4. r4 r8 |
