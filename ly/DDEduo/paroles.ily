\tag #'(vdessus1 basse) {
  Ga -- lants qui cour -- ti -- sez les bel -- les,
  sa -- chez brus -- quer un doux mo -- ment.
}
\tag #'(vdessus2 basse) {
  A -- mants qui sou -- pi -- rez pour el -- les,
  es -- pé -- rez tout du sen -- ti -- ment.
}
\tag #'(vdessus1 basse) {
  Toute oc -- ca -- si -- on non sai -- si -- e,
  s’é -- chappe et se perd sans re -- tour.
}
\tag #'(vdessus2 basse) {
  Sans re -- tour pour la fan -- tai -- si -- e ;
  mais el -- le re -- naît pour l’a -- mour.
}
\tag #'(vdessus1 basse) {
  Toute oc -- ca -- si -- on non sai -- si -- e,
  s’é -- chappe et se perd sans re -- tour.
}
\tag #'(vdessus2 basse) {
  Sans re -- tour pour la fan -- tai -- si -- e ;
  mais el -- le re -- naît pour l’a -- mour.
}
\tag #'(vdessus1 basse) {
  Non, non, sans re -- tour.
}
\tag #'(vdessus2 basse) {
  Mais el -- le re -- naît pour l’a -- mour.
}
