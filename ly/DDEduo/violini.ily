\clef "treble"
<<
  \tag #'violino1 {
    re''8\p |
    re''4. si'16 do'' re''8 re'' re'' re'' |
    re''8. sol''16 re''4. re''8 \grace mi'' re'' do''16 si' |
    \grace si'8 la'4 sol' la'4. re''8 |
    si'4(\mf la'8 si'4) dod''8\p |
    re''4 mi''16*2/3 re'' dod'' re''8 la' la' |
    fad''4( mi''8) re''4( dod''8) |
    si' re''([\sf dod'']) si' \once\slurDashed re''([\sf dod'']) |
    si'4. si'8( dod'' red'') |
    mi''4 sol''8 si'4 dod''8 |
    re'' re'' \grace mi'' re'' do''16 re'' mi''4 \grace re''8 do'' si'16 do'' |
    re''-! mi''-! re''-! do''-! si'8 si' do'' la'16 do'' \grace do''8 si' la'16 sol' |
    fad'4. re''4 re''8 |
    mi''( re'' do'') sol''( fad'' mi'') |
    re''4
  }
  \tag #'violino2 {
    si8-\sug\p |
    si4. sol16 la si8 si si si |
    si4 si4. si8 si do'16 re' |
    do'4 si fad'2 |
    sol'4-\sug\mf( re'8 re'4) sol'8-\sug\p |
    fad'4.~ fad'8 fad' fad' |
    fad'4( mi'8) re'4( dod'8) |
    si re'-\sug\sf([ dod']) si re'-\sug\sf([ dod']) |
    sol4. r4 r8 |
    si'4. sol'4 mi'8 |
    re'4 si( do' la) |
    si16 do' si la sol8 sol' la' re'16 la' sol-! la-! si-! do'-! |
    re'4. sol'4 sol'8 |
    sol'4.~ sol'4 sol'8 |
    sol'4
  }
>> re''8\mf sol''8. la''16 si''8 |
<<
  \tag #'violino1 {
    re''4 re''8\p mi''8. fad''16 sol''8 |
    si'4( re''8) la'4( re''8) |
    si' re'' \grace mi'' re'' do''16-! re''-! mi''4 \grace re''8 do'' si'16-! do''-! |
    re''-! mi''-! re''-! do''-! si'8 si' do''8 la'16 do''
  }
  \tag #'violino2 {
    re''4. sol'-\sug\p~ |
    sol' fad' |
    sol'4 si( do' la) |
    si16 do' si la sol8 sol' la'4
  }
>> \grace do''8 si' la'16 sol' |
fad'4. <<
  \tag #'violino1 {
    re''4 re''8 |
    mi''( re'' do'') sol''( fad'' mi'') |
    re''4
  }
  \tag #'violino2 {
    sol'4.~ |
    sol'2. |
    sol'4
  }
>> re''8 sol''8. la''16 si''8 |
<<
  \tag #'violino1 {
    re''4 re''8 mi''8. fad''16 sol''8 |
    si'4( re''8) la'4 re''8 |
    si'4
  }
  \tag #'violino2 {
    re''4. sol'~ |
    sol' fad' |
    sol'4
  }
>> re''8\mf sol''8. la''16 si''8 |
<<
  \tag #'violino1 {
    re''4 re''8\p mi''8. fad''16 sol''8 |
    si'4( re''8) la'4( re''8) |
  }
  \tag #'violino2 {
    re''4. sol'-\sug\p |
    sol' fad' |
  }
>>
sol'4. r4 r8 |
