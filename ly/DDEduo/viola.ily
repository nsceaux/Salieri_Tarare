\clef "alto" si8-\sug\p |
si4. sol16 la si8 si si si |
si4 si4. si8 si do'16 re' |
do'4 si fad'2 |
sol4-\sug\mf( fad8 sol4) mi'8-\sug\p |
re'2. |
r8 re'-\sug\sf( dod') r si-\sug\sf( la) |
sol r r sol r r |
sol4. r4 r8 |
sol4. sol4 la8 |
re4 si( do' la) |
si16 do' si la sol8 sol fad4 sol16-! la-! si-! do'-! |
re'4. si4 si8 |
do'( re' mi') mi'( re' do') |
si4. r4 r8 |
si4. do' |
re'2. |
sol4 si( do' la) |
si16 do' si la sol8 re' re'4 sol16-! la-! si-! do'-! |
re'4. si4 si8 |
do'( re' mi') mi'( re' do') |
si4. r4 r8 |
si4. do' |
re'2. |
sol4. r4 r8 |
si4.( do') |
re'2. |
sol4. r4 r8 |
