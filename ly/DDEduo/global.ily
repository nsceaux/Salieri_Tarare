\key sol \major
\tempo "Allegretto"
\time 4/4 \midiTempo#96 \partial 8 s8 s1*3
\time 6/8 \midiTempo#128  \tempo "Andante un poco sostenuto"s2.*6
\time 4/4 \midiTempo#96 \tempo\markup { \concat { 1 \super o } Tempo } s1*2
\time 6/8 \midiTempo#128 s4. \tempo\markup { \concat { 2 \super o } Tempo } s4. s2.*4
\time 4/4 \midiTempo#96 \tempo\markup { \concat { 1 \super o } Tempo } s1*2
\time 6/8 \midiTempo#128 s4. \tempo\markup { \concat { 2 \super o } Tempo } s4. s2.*8 \bar "|."
