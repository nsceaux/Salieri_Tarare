\clef "treble" r2 |
<<
  \tag #'violino1 {
    r8 re''\p r re'' r re'' r mi'' |
    r re'' r re'' r re'' r mi'' |
    r fad' r la' r re'' r la' |
    r si' r si' r re'' r re'' |
    r dod'' r dod'' r dod'' r dod'' |
    r re'' r re'' r re'' la'8. la'16 |
    re''4. re''8 re''4 fad''8. la''16 |
    la''4\sf( sol''2) sol''8\p mi'' |
    dod''4 mi''8. mi''16 la''4. sol''8 |
    fad''8 fad'' r fad'' r fad'' r fad'' |
    r fad'' r fad'' r fad'' r mi'' |
    r re'' r re'' r re'' re''8. mi''16 |
    fad''2\cresc fad''4. sol''8 |
    la''8 la''4 la'' la'' la''8 |
    do'''1\f\fermata |
    si''4
  }
  \tag #'violino2 {
    r8 si'-\sug\p r si' r si' r do'' |
    r si' r si' r si' r do'' |
    r la' r fad' r fad' r fad' |
    r sol' r sol' r la' r la' |
    r sol' r sol' r sol' r sol' |
    r <fad' la'> r q q4 r |
    <fad' la'>2:16 q:16 |
    <<
      { la':16 la':16 | la':16 la':16 | } \\
      { sol':16-\sug\sf sol':16-\sug\p | sol':16 sol':16 | }
    >>
    <fad' la'>2:16 q8 q r la' |
    r dod'' r dod'' r dod'' r dod'' |
    r si' r si' r si' r si' |
    <>^"Serré" do''!2:16-\sug\cresc do'':16 |
    <re' do''>2:16 q:16 |
    <la' fad''>1\fermata -\sug\f |
    <si' sol''>4
  }
>> r r2 |
R1 |
r2 <sol re' si'>2 |
