\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { r8 re'' re'' re'' |
    re''( sol'') sol'' sol'' \grace la'' sol''4( fad''8 mi'') |
    mi''( re'') re''4 r8 re''4 mi''8 |
    r8 fad'4 la' re'' la'8 |
    r8 si'4 re''8 r la'4 re''8 |
    r8 dod''4 mi'' sol'' dod''8 |
    r re''4 fad''8 re''4 r | }
  { r2 R1*6 }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''1 | dod'' | dod''?2 mi'' | la'~ la'4 }
  { fad'1 | sol' | sol'2 la''4. sol''8 | fad''2~ fad''4 }
  { s1 | s-\sug\sf | s-\sug\p }
>> r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { r8 fad''( mi'' re'') dod''4. mi''8 |
    mi''( re'') re'' re'' re''4 re''8. mi''16 |
    fad''2 fad''4. sol''8 |
    la''1 |
    la''\fermata |
    sol''4 }
  { R1*2 |
    do''1~ |
    do'' |
    do''\fermata |
    si'4 }
  { s1*2 | s1-\sug\cresc | s1 | s-\sug\f }
>> r4 r2 |
R1*2 |
