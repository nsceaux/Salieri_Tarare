\clef "bass" r2 |
sol8\p r sol r sol r sol r |
sol r sol r sol r do r |
re r re r re r re r |
sol r sol r fad r fad r |
mi r mi r la, r la, r |
re r re r re4 r |
re2:8 re:8 |
mi:8\sf mi:8\p |
mi:8 dod:8 |
re:8 re8 r re r |
lad,8 r lad, r lad, r lad, r |
si, r si, r si, r si, r |
la2:8\cresc la:8 |
re:8 re:8 |
re1\fermata\f |
sol4 r r2 |
R1 |
r2 sol |
