\clef "tenor/G_8"
<<
  \tag #'tarare {
    <>^\markup\character-text Tarare exalté
    r8 re' re' re' |
    sol'4 sol'8 sol' sol'4 fad'8 mi' |
    re'4 re' r8 re' re' mi' |
    fad4 la8 la re'4 re'8 la |
    si4 si r8 re' re' re' |
    dod'4 mi'8 mi' sol'4 sol'8 dod' |
    re'4 re' r la8 la |
    re'4. re'8 re'4 fad'8 la' |
    sol'2. sol'8 mi' |
    dod'4 mi'8 mi' la'4. sol'8 |
    fad'4 fad' r fad'8 fad' |
    fad'4 mi'8 re' dod'4. mi'8 |
    re'2 r4 re'8 mi' |
    <>^\markup\italic { Calpigi lui fait un signe négatif }
    fad'2 fad'4. sol'8 |
    la'1 |
    la'2.\fermata do'4 |
    si2
  }
  \tag #'basse { r2 R1*15 r2 }
>> r8^"Récit" si8 si do' |
re'4. fa'8 fa' si si do' |
sol sol r4 r2 |
