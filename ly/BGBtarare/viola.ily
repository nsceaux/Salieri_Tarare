\clef "alto" r2 |
sol'8-\sug\p r sol' r sol' r sol' r |
sol' r sol' r sol' r do' r |
re' r re' r re' r re' r |
sol' r sol' r fad' r fad' r |
mi' r mi' r la r la r |
re' r re' r re'4 r |
re'2:16 re':16 |
mi':16-\sug\sf mi':16-\sug\p |
mi':16 mi':16 |
re':16 re'8 re' r re' |
r fad' r fad' r fad' r fad' |
r fad' r fad' r fad' r fad' |
la'2:16-\sug\cresc la':16 |
<fad' la'>:16 q:16 |
q1\fermata -\sug\f |
sol'4 r r2 |
R1 |
r2 sol' |
