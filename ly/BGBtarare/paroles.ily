\tag #'tarare {
  Char -- mante Ir -- za, qu’est- ce donc qui t’ar -- rê -- te ?
  Le fils des Dieux n’est- il pas ta con -- quê -- te ?
  Le fils des Dieux n’est- il pas ta con -- quê -- te ?
  Puis -- se- t-il trou -- ver dans tes yeux
  ce pur feu dont il é -- tin -- cel -- le !
  Rends, Ir -- za, rends mon maître heu -- reux,
  rends, Ir -- za, rends mon maî -- tre heu -- reux…
}
…si tu le peux sans ê -- tre cri -- mi -- nel -- le.
