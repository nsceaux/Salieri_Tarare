\score {
  \new StaffGroupNoBar <<
    \new GrandStaff \with { \oboiInstr } <<
      \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
      \new Staff \with { \haraKiriFirst } <<
        \global \keepWithTag #'oboe2 \includeNotes "oboi"
      >>
    >>
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'tarare \includeNotes "voix"
    >> \keepWithTag #'tarare \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr 
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s2 s1\break s1*5\pageBreak
        s1*5 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
