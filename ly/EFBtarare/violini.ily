\clef "treble" r4 |
do'16(\p si do' si) do'8 r do'16 si do' si do'8 r |
si16 lad si lad si8 r si16 lad si lad si8 r |
re'16 dod' re' dod' re'8 r re'16 dod' re' dod' re'8 r |
<<
  \tag #'violino1 {
    mi'16 red' mi' red' mi'8 r mi'16 red' mi' red' mi'8 r |
    mi'16 red' mi' red' mi'8 r mi'16 red' mi' red' mi'8 r |
    fad'16 mi' fad' mi' fad'8 r fad'16 mi' fad' mi' fad'8 r |
    fad'16 mi' fad' mi' fad'8 r fad'16 mi' fad' mi' fad'8 r |
    sol'16 fad' sol' fad' sol'8 r sol'16 fad' sol' fad' sol'8 r |
    si'16 sol' si' sol' si'8 r si'16 sol' si' sol' si'8 r |
    si'16 sol' si' sol' si'8 r si'16 sol' si' sol' si'8 r |
    re''16 dod'' re'' dod'' re''8 r si'16 lad' si' lad' si'8 si' |
    si'4
  }
  \tag #'violino2 {
    do'16 si do' si do'8 r do'16 si do' si do'8 r |
    do'16 si do' si do'8 r do'16 si do' si do'8 r |
    la16 sold la sold la8 r la16 sold la sold la8 r |
    la16 sold la sold la8 r la16 sold la sold la8 r |
    si16 lad si lad si8 r si16 lad si lad si8 r |
    re'16 si re' si re'8 r re'16 si re' si re'8 r |
    re'16 si re' si re'8 r re'16 si re' si re'8 r |
    si16 lad si lad si8 r re'16 dod' re' dod' re'8 re' |
    sold'4
  }
>> r4 r2 |
R1^\fermataMarkup |
<<
  \tag #'violino1 {
    r2 r8 sold'-! si'-! re''-! |
    do''4 r r8 do'' mi'' re'' |
    do''4 r r8 mi' sol' sib' |
    la'4 r r8 la' fa' la' |
    re'' re'' fa'' mi'' re'' re'16 re' re'8 do'' |
    si'4 r r8 si' re'' sol'' |
    mi''4 r r8 sol'' mi'' do'' |
    si'4 r r sol'8. sol'16 |
    do''16\fp re'' do'' si' do''8. do''16 mi''4. sol''8 |
    la'2\fp~ la'8 la'-! do''-! fa''-! |
    mi''4:16\cresc do'':16 si'2:16\f |
    do''4
  }
  \tag #'violino2 {
    <si mi'>8 q q q q2:8 |
    <do' mi'>:8 q8 mi' mi' fa' |
    sol' <mi' do''> q q q2:8 |
    <do'' fa'>:8 q:8 |
    la'8 la' la' la' la'2:8 |
    sol'8 <si re'>[ q q] q2:8 |
    <do' mi'>:8 q:8 |
    <si re'>:8 q4 r |
    sol'2:16-\sug\fp sol':16 |
    do':16-\sug\fp do'16 do' fa' fa' la' la' do'' do'' |
    do''4:16-\sug\cresc mi':16 re'2:16\f |
    mi'4
  }
>> r4 r2 |
R1 |
R1^\fermataMarkup |
