\clef "alto" r4 |
mi16(-\sug\p sol mi sol) mi8 r mi16 sol mi sol mi8 r |
fa16 sol fa sol fa8 r fa16 sol fa sol fa8 r |
re16 sol re sol re8 r re16 sol re sol re8 r |
r sol sol4 sol8 sol sol r |
sol8 sol sol r do4 r |
re r re r |
re r re r |
sol r sol r |
sol r sol r |
sol r sol r |
sol r sol r |
si r r2 |
R1^\fermataMarkup |
sold2:8 sold:8 |
la:8 la8 do' do' re' |
mi' sol' sol' sol' sol'2:8 |
la':8 la':8 |
r8 fa' re' mi' fa'4 fad' |
sol'8 sol[ sol sol] sol2:8 |
sol:8 sol:8 |
sol:8 sol4 r |
mi'2:8-\sug\fp mi':8 |
fa':8-\sug\fp fa'8 fa' fa' la' |
sol'2:8-\sug\cresc sol:8-\sug\f |
do'4 r r2 |
R1 |
R1^\fermataMarkup |
