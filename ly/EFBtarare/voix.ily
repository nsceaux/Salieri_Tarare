\clef "tenor/G_8" <<
  \tag #'basse { r4 | R1*11 | r2 }
  \tag #'recit {
    <>^\markup\character-text Tarare Ton respectueux
    sol8 sol |
    do'2 mi'4. do'8 |
    si2 r |
    re'4 re' re'4. sol8 |
    mi'4 mi' r mi'8 mi' |
    mi'4 do'8 do' do'4. do'8 |
    la4 la8 la la la la la |
    re'4 re'8 re' re'4 re'8 re' |
    si2 r4 sol |
    si si8 si si4 si8 do' |
    re'2 r4 re'8 re' |
    re'4. si8 si4 si8 si |
    sold4 sold
  }
>>
\ffclef "soprano/treble" <>^\markup\character-text Spinette (à part)
si'4 si'8 si' |
mi''2 r4\fermata r8^\markup\italic haut si' |
mi''4 si'8^\markup\italic ton décidé si' si'4 do''8 re'' |
do''2 <<
  \tag #'basse { r2 | R1*9 | r4 }
  \tag #'recit {
    r8 do'' mi'' re'' |
    do''4. do''8 do''4. do''8 |
    do''4 do'' r la' |
    re''8 re'' fa'' mi'' re''4. do''8 |
    si'2 r4 re''8 re'' |
    mi''4. do''8 sol''4 mi''8 do'' |
    si'4 re'' r^\markup\italic { elle lui ôte son masque } sol'8 sol' |
    do''4 do''8 do'' mi''4. sol''8 |
    la'2 r8 la' do'' fa'' |
    mi''4. sol''8 si'4. re''8 |
    do''4
  }
>> r8\fermata <>^\markup Récit sol'16 sol' do''4 do''8 mi'' |
do''4 r
\ffclef "tenor/G_8" <>^\markup\character Tarare
do'8. do'16 do'8 sib16 do' |
la4 r\fermata r2 |
