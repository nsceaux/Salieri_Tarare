\clef "bass" r4 |
do-\sug\p r do r |
re r re r |
si, r si, r |
do r do r |
do r do r |
re r re r |
re r re r |
sol r sol r |
sol r sol r |
sol r sol r |
sol r fa! r |
mi r r2 |
R1^\fermataMarkup |
r8 mi sold si mi'4 r |
r8 la mi do la,4 r |
r8 mi sol do' do4 r |
r8 fa la do' fa4 r |
r8 fa re mi fa4 fad |
r8 sol re si, sol,4 r |
r8 sol mi do sol,4 r |
r8 sol, si, re sol4 r |
mi2:8\fp mi:8 |
fa:8\fp fa8 fa fa la |
sol2:8\cresc sol,:8\f |
do4 r r2 |
R1 |
R1^\fermataMarkup |
