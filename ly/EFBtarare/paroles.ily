\tag #'recit {
  É -- tran -- ger dans Or -- mus, hier on me vint di -- re
  que le maî -- tre de cet em -- pi -- re
  don -- nait à son a -- mante u -- ne fête au sé -- rail…
  J’ai cru, sous ce vile at -- ti -- rail,
  dans la nuit pou -- voir m’in -- tro -- dui -- re…
}
Ah ! quel bon -- heur !
Eh bien, cu -- ri -- eux é -- tran -- ger,
\tag #'recit {
  quand le dé -- sir de me con -- naî -- tre
  t’en -- gage en un si grand dan -- ger,
  à mes yeux crains- tu de pa -- raî -- tre ?
  Ce n’est point sous ce masque af -- freux
  qu’un im -- pru -- dent peut être heu -- reux.
}
C’est un hom -- me char -- mant.

Ah ! fuy -- ons de ces lieux.
