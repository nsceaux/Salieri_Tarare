\clef "bass" mib'8\fp re' mib' do' sib do' lab sib |
sol lab sib lab sol mib fa sol |
lab do' lab sol fa lab fa mib |
re mib fa mib re sib, do re |
mib4 mib\ff sol mib |
re re sib, re |
mib mib\p sol mib |

re re sib, re |
si,1\f |
sol,4\f r sol, r |
sol, r sol, r |
do r r2\fermata |
do2:8\p do:8 |
do:8 do:8 |
si,:8\fp si,:8 |
si,:8 si,:8 |
si,:8 si,:8 |
do:8 do:8 |
mib'8\p do' la fad mib'\cresc do' la fad |
mib' do' la fad mib' do' la fad |
mib'\f do' la fad mib' do' la fad |
mib'4 r fad4.\p fad8 |
sol4 r sol, r |
do r re r |
sol2 r4 r8 sol\p |
mib' do' la fad mib'\cresc do' la fad |
mib' do' la fad mib' do' la fad |
mib'\f do' la fad mib' do' la fad |
mib'4 r fad4.\p fad8 |
sol4 r sol, r |
do r re r |
sol,2 r\fermata |
mib'8\fp re' mib' do' sib do' sib lab |
sol lab sib lab sol mib fa sol |
lab do' lab sol fa sol lab la |
sib fa re fa sib, do re sib, |
mib4 mib\f sol mib |
re re sib, re |
mib\p mib sol mib |
re re sib, re |
reb1\f |
do2\p sib,4 sib |
lab4 sol fa mib |
re2 r2\fermata |
mib2:8\pp mib:8 |
mib:8 mib:8 |
mib:8 mib:8 |
lab:8\cresc lab:8 |
lab:8 lab:8 |
sib:8\f sib:8 |
sib:8 lab:8 |
sol2.\fermata r4 |
mib2:8\p mib:8 |
mib:8 mib:8 |
mib:8 mib:8 |
lab:8 lab:8 |
lab:8 lab:8 |
sib:8\f sib:8 |
sib,:8 sib,:8 |
mib4.\fp mib8 reb4.\fp reb8 |
do4.\fp do8 sib,4.\fp sib,8 |
lab,4.\fp lab,8 la,4.\fp la,8 |
sib,2:8\f sib,:8 |
sib,:8 sib,:8 |
mib4 mib sol mib |
sib,1 |
mib4 mib sol mib |
sib,1 |
mib4 sib, mib sol |
sib mib' sib sol |
mib4. mib8 mib4 mib |
mib2 r |
