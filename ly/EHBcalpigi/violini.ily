\clef "treble" mib''16\fp mib'' re'' re'' mib'' mib'' do'' do'' sib'8:16 do'':16 lab':16 sib':16 |
sol':16 lab':16 sib':16 lab':16 sol':16 mib':16 fa':16 sol':16 |
lab':16 do'':16 lab':16 sol':16 fa':16 lab':16 fa':16 mib':16 |
re':16 mib':16 fa':16 mib':16 re':16 sib:16 do':16 re':16 |
mib'4 sib'16\ff mib'' sol'' sib'' sib'' sol'' mib'' sib' sib'' sol'' mib'' sib' |
sib''4 sib'16 do'' re'' mib'' fa'' mib'' re'' do'' sib' lab' sol' fa' |
sol'4 <<
  \tag #'violino1 {
    sib'16\p mib'' sol'' sib'' sib'' sol'' mib'' sib' sib'' sol'' mib'' sib' |
    sib''4 sib'16 do'' re'' mib'' fa'' mib'' re'' do'' sib' lab' sol' fa' |
    <sol' sol>1\f |
  }
  \tag #'violino2 {
    sol'4-\sug\p mib' sol' |
    fa' fa' re' fa' |
    <re' sol>1-\sug\f |
  }
>>
<sol re' si'>4\f r <si sol' re''> r |
<re' si' sol''> r <sol' re'' si''> r |
<sol' mib'' do'''>4 r r2\fermata |
<<
  \tag #'violino1 {
    sol'2:16\p sol':16 |
    sol':16 sol':16 |
    lab':16\fp lab':16 |
    sol':16 sol':16 |
    sol':16 sol':16 |
    sol':16 sol':16 |    
  }
  \tag #'violino2 {
    mib'2:16-\sug\p mib':16 |
    mib':16 mib':16 |
    <re' fa'>:16-\sug\fp q:16 |
    q:16 q:16 |
    q:16 q:16 |
    mib':16 mib':16 |
  }
>>
mib''8:16\p do'':16 la':16 fad':16 mib'':16\cresc do'':16 la':16 fad':16 |
mib'':16 do'':16 la':16 fad':16 mib'':16 do'':16 la':16 fad':16 |
mib'':16\f do'':16 la':16 fad':16 mib'':16 do'':16 la':16 fad':16 |
mib''4 r <<
  \tag #'violino1 {
    re''4.\p re''8 |
    sib'4. sib'8 sib'4. sib'8 |
    la'4. la'8 la'4. la'8 |
    sib'2
  }
  \tag #'violino2 {
    la'4.-\sug\p la'8 |
    sol'4. sol'8 sol'4. sol'8 |
    sol'4. sol'8 fad'4. fad'8 |
    sol'2
  }
>> r4 r8 sol'\p |
mib''8:16 do'':16 la':16 fad':16 mib'':16\cresc do'':16 la':16 fad':16 |
mib'':16 do'':16 la':16 fad':16 mib'':16 do'':16 la':16 fad':16 |
mib'':16\f do'':16 la':16 fad':16 mib'':16 do'':16 la':16 fad':16 |
mib''4 r <<
  \tag #'violino1 {
    re''4.\p re''8 |
    sib'4. sib'8 sib'4. sib'8 |
    la'4. la'8 la'4. la'8 |
    sol'2
  }
  \tag #'violino2 {
    la'4.-\sug\p la'8 |
    sol'4. sol'8 sol'4. sol'8 |
    sol'4. sol'8 fad'4. fad'8 |
    sol'2
  }
>> r2\fermata |
<<
  \tag #'violino1 {
    <mib'' sol''>2:16\fp q:16 |
    q:16 q:16 |
    << { fa'':16 fa'':16 } \\ { mib'':16 mib'':16 } >> |
    <re'' fa''>:16 q:16 |
    <sol'' mib''>4
  }
  \tag #'violino2 {
    mib''8:16-\sug\fp re'':16 mib'':16 do'':16 sib':16 do'':16 sib':16 lab':16 |
    sol':16 lab':16 sib':16 lab':16 sol':16 mib':16 fa':16 sol':16 |
    lab':16 do'':16 lab':16 sol':16 fa':16 sol':16 lab':16 la':16 |
    sib':16 fa':16 re':16 fa':16 sib:16 do':16 re':16 sib:16 |

    mib'4
  }
>> sib'16\f mib'' sol'' sib'' \ru#2 { sib'' sol'' mib'' sib' } |
sib''4 sib'16 do'' re'' mib'' fa'' mib'' re'' do'' sib' lab' sol' fa' |
sol'4 <<
  \tag #'violino1 {
    sib'16\p mib'' sol'' sib'' sib'' sol'' mib'' sib' sib'' sol'' mib'' sib' |
    sib''4 sib'16 do'' re'' mib'' fa'' mib'' re'' do'' sib' lab' sol' fa' |
    sol'2\f sol''4 sol'' |
    lab''2\p sol''4-! sol''-! |
    fa''4-! mib''-! re''-! do''-! |
    sib'2
  }
  \tag #'violino2 {
    sol'4-!-\sug\p mib'-! sol'-! |
    fa'-! fa'-! re'-! fa'-! |
    <sol' sib>1\f |
    mib'2\p re'4-! re'-! |
    do'-! sib-! lab-! sol8 lab |
    sib2
  }
>> r\fermata |

<<
  \tag #'violino1 {
    r4 mib'-.\p sol'-. sib'-. |
    mib''8 mib''4 mib'' mib'' mib''8~ |
    mib'' mib''4 mib'' mib'' mib''8 |
  }
  \tag #'violino2 {
    <sol' sib>2:16\pp q:16 |
    q16 <sol' sib'> q q q4:16 q2:16 |
    q:16 q:16 |
  }
>>
do''8\cresc reb''16( mib'' fa'' sol'' lab'' sib'') do'''( lab'' do''' lab'' do''' lab'' do''' lab'') |
do'''( lab'' do''' lab'' do''' lab'' do''' lab'') do'''( sib'' lab'' sol'' fa'' mib'' re''! do'') |
sib' <mib'' sol''>[\f q q] q4:16 q2:16 |
q:16 <<
  \tag #'violino1 {
    <re'' fa''>4. sib''8 |
    <sib'' sib'>2.\fermata
  }
  \tag #'violino2 {
    <re'' fa''>2:16 |
    mib''2.\fermata
  }
>> r4 |
<<
  \tag #'violino1 {
    r4 mib'-!\p sol'-! sib'-! |
    mib''8 mib''4 mib'' mib'' mib''8~ |
    mib'' mib''4 mib'' mib'' mib''8 |
  }
  \tag #'violino2 {
    <sib sol'>2:16-\sug\p q:16 |
    sib16 <sol' sib'> q q q4:16 q2:16 |
    q:16 q:16 |
  }
>>
do''8 reb''16( mib'' fa'' sol'' lab'' sib'' do''' lab'' do''' lab'' do''' lab'' do''' lab'') |
do'''( lab'' do''' lab'' do''' lab'' do''' lab'' do''' sib'' lab'' sol'' fa'' mib'' re''! do'') |
sib'\f <mib'' sol''>[ q q] q4:16 q2:16 |
q:16 <re'' fa''>:16 |
<<
  \tag #'violino1 {
    <mib'' sol''>8\fp mib''[ mib'' mib''] <mib'' mib'>\fp mib''[ mib'' mib''] |
    lab''8\fp lab''[ lab'' lab''] <sol'' mib''>\fp q[ q q] |
    <<
      { fa''8 fa''[ fa'' fa''] fa'' fa''[ fa'' fa''] } \\
      { mib''\fp mib''[ mib'' mib''] mib''\fp mib''[ mib'' mib''] }
    >>
  }
  \tag #'violino2 {
    <mib'' sol'>8-\sug\fp <sol' sib'>[ q q] q-\sug\fp q[ q q] |
    <mib'' mib'>-\sug\fp <lab' mib''>[ q q] sib'-\sug\fp sib'[ sib' sib'] |
    do''-\sug\fp do''[ do'' do''] do''-\sug\fp do''[ do'' do''] |
  }
>>
<<
  { sol''8 sol''4 sol'' sol'' sol''8 |
    fa'' fa''4 fa'' fa'' fa''8 | } \\
  { mib''8\f mib''4 mib'' mib'' mib''8 |
    re'' re''4 re'' re'' re''8 | }
>>
mib''4 sib'16 mib'' sol'' sib'' sib'' sol'' mib'' sib' sib'' sol'' mib'' sib' |
sib''4 re''16 mib'' fa'' sol'' lab'' sol'' fa'' mib'' re'' do'' sib' lab' |
sol'4 sib'16 mib'' sol'' sib'' sib'' sol'' mib'' sib' sib'' sol'' mib'' sib' |
sib''4 re'16 mib' fa' sol' lab' sol' fa' mib' re' do' sib lab |
sol4 sib mib' sol' |
sib' mib'' sol'' sib'' |
mib'''4. <mib' sol>8 q4 q |
q2 r |
