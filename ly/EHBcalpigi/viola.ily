\clef "alto" mib'16-\sug\fp mib' re' re' mib' mib' do' do' sib8:16 do':16 lab:16 sib:16 |
sol:16 lab:16 sib:16 lab:16 sol:16 mib:16 fa:16 sol:16 |
lab:16 do':16 lab:16 sol:16 fa:16 lab:16 fa:16 mib:16 |
re:16 mib:16 fa:16 mib:16 re:16 sib16 sib do'8:16 re':16 |
mib'8 sib[-\sug\ff sib sib] sib2:8 |
sib:8 sib:8 |
<< sib2:8 { s4 s-\sug\p } >> sib2:8 |
sib:8 sib:8 |
si1\f |
<sol re' si'>4-\sug\f r q r |
q r q r |
<sol mib' do''>4 r r2\fermata |
do':16\p do':16 |
do':16 do':16 |
si:16-\sug\fp si:16 |
si:16 si:16 |
si:16 si:16 |
do':16 do':16 |
mib''8\p do'' la' fad' mib''\cresc do'' la' fad' |
mib'' do'' la' fad' mib'' do'' la' fad' |
mib''\f do'' la' fad' mib'' do'' la' fad' |
mib''4 r4 re'4.\p re'8 |
re'4. re'8 re'4. re'8 |
mib'4. mib'8 re'4. re'8 |
sol'2 r4 r8 sol'\p |
mib'' do'' la' fad' mib''\cresc do'' la' fad' |
mib'' do'' la' fad' mib'' do'' la' fad' |
mib''\f do'' la' fad' mib'' do'' la' fad' |
mib''4 r4 re'4.\p re'8 |
re'4. re'8 re'4. re'8 |
mib'4. mib'8 re'4. re'8 |
sib2 r\fermata |
mib'8:16-\sug\fp re':16 mib':16 do':16 sib:16 do':16 sib:16 lab:16 |
sol:16 lab:16 sib:16 lab:16 sol:16 mib:16 fa:16 sol:16 |
lab:16 do':16 lab:16 sol:16 fa:16 sol:16 lab:16 la:16 |
sib:16 fa:16 re:16 fa:16 sib:16 do':16 re':16 sib:16 |
mib' sib sib-\sug\f sib sib2:8 |
sib:8 sib:8 |
sib:8-\sug\p sib:8 |
sib:8 sib:8 |
<sol mib'>1-\sug\f |
<lab mib'>2-\sug\p r4 sib |
lab sol fa mib |
fa2 r\fermata |
mib8-\sug\pp mib' mib' mib' mib'2:8 |
mib':8 mib':8 |
mib':8 mib':8 |
<mib' do''>8\cresc q4 q q q8 |
q do''16( lab' do'' lab' do'' lab' do'' sib' lab' sol' fa' mib' re' do') |
sib2:8-\sug\f sib:8 |
sib:8 sib:8 |
sib2.\fermata r4 |
<sol mib'>2:16-\sug\p q:16 |
q:16 q:16 |
q:16 q:16 |
<lab mib'>8 <mib' do''>4 q q q8 |
do''16( lab' do'' lab' do'' lab' do'' lab' do'' sib' lab' sol' fa' mib' re' do') |
sib2:8-\sug\f sib:8 |
sib:8 sib:8 |
mib'8-\sug\fp mib'[ mib' mib'] reb'-\sug\fp reb'[ reb' reb'] |
do'-\sug\fp do'[ do' do'] sib-\sug\fp sib[ sib sib] |
lab-\sug\fp lab[ lab lab] la-\sug\fp la[ la la] |
sib2:8-\sug\f sib:8 |
sib:8 sib:8 |
mib'4 sol' mib' sol' |
<fa' re'>1 |
mib'4 sol mib sol |
re1 |
mib4 sib mib' sol' |
sib' mib'' sib' sol' |
mib'4. sib8 sib4 sib |
sib2 r |
