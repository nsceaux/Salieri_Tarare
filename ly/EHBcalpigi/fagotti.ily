\clef "bass" R1*4 |
r4 mib-\sug\ff sol mib |
re re sib, re |
mib r r2 |
R1 |
si,1-\sug\f |
sol,4-\sug\f r sol, r |
sol, r sol, r |
do r r2\fermata |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sol1 | sol | lab( | sol)~ | sol | sol | }
  { mib1 | mib | fa~ | fa~ | fa | mib | }
  { s1*2\p | s1-\sug\fp }
>>
R1*2 |
mib'8-\sug\f do' la fad mib' do' la fad |
mib'4 r r2 |
R1*5 |
mib'8-\sug\f do' la fad mib' do' la fad |
mib'4 r r2 |
R1*2 |
R1^\fermataMarkup |
R1*4 |
r4 mib\f sol mib |
re re sib, re |
mib r r2 |
R1 |
reb1-\sug\f |
R1*2 |
r2 r\fermata |
R1*3 |
\clef "tenor" \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { do'1 | fa' | sol'~ | sol'2 fa' | }
  { lab1 | do' | mib'~ | mib'2 re' | }
  { s1*2-\sug\cresc | s1-\sug\f }
>>
mib'2.\fermata r4 |
R1*3 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { do'1( | fa') | sol'~ | sol'2 fa' | }
  { lab1( | do') | mib'~ | mib'2 re' | }
  { s1*2 | s1-\sug\f }
>>
\clef "bass" mib4.\fp mib8 reb4.\fp reb8 |
do4.\fp do8 sib,4.\fp sib,8 |
lab,4.\fp lab,8 la,4.\fp la,8 |
sib,1-\sug\f~ |
sib, |
mib4 mib sol mib |
sib,1 |
mib4 mib sol mib |
sib,1 |
mib4 sib, mib sol |
sib mib' sib sol |
mib4. mib8 mib4 mib |
mib2 r |
