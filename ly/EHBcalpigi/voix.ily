\clef "alto/G_8" <>^"Chanté" mib'2. r8 sib |
mib'4 mib'8 mib' sol'4. mib'8 |
do'4 do' r8 do' do' do' |
sib4. sib8 fa'4 sib' |
sol'2 r |
r r4 sib8 sib |
mib'4 mib'8 mib' mib'4 mib' |
fa'1 |
r2 re'4 re' |
sol'2 sol'4 sol' |
sol'2. sol'4 |
mib'4 do' r2\fermata |
r2 r4 mib'8 re' |
do'4. sol8 sol4 sol8 sol |
lab2. lab4 |
sol2 sol4. sol8 |
re'2 re'4. fa'8 |
mib'4 mib' r r8 do' |
mib'4. mib'8 mib'4. mib'8 |
mib'2. mib'4 |
mib'4 la r2 |
r re'4. re'8 |
sib2. sib4 |
la2 re'4. re'8 |
sib2 r4 r8 sol |
mib'4. mib'8 mib'4. mib'8 |
mib'2. mib'4 |
mib'4 la r2 |
r re'4. re'8 |
sib2. sib4 |
la2 re'4. re'8 |
sol2 r\fermata |
mib'2 r4 r8 sib |
mib'4 mib'8 mib' sol'4. mib'8 |
do'4 do' r8 do' do' do' |
sib4. sib8 fa'4 sib' |
sol'2 r |
r r4 sib8 sib |
mib'4 mib'8 mib' mib'4 mib' |
fa'1 |
r2 sol'4 sol' |
lab'2 sol'4 sol' |
fa'4 mib'8 mib' re'4 do' |
sib sib r\fermata sib8 sib |
sol4 mib8 mib sol4 sib |
mib'1~ |
mib'2 mib'4 mib' |
do'2 do'4 do' |
fa'2. lab'4 |
sol'1~ |
sol'2\melisma fa'4. sib'8\melismaEnd |
sib'2 r4\fermata sib8 sib |
sol4 mib8 mib sol4 sib |
mib'1~ |
mib'2 mib'4 mib' |
do'2 do'4 do' |
fa'2. lab'4 |
sol'1\melisma |
sol'2( fa')\melismaEnd |
sol'2 mib'4 mib' |
lab'2 sol'4 sol' |
fa'2 fa' |
sib'1~ |
sib' |
mib'4 r r2 |
R1*7 |
