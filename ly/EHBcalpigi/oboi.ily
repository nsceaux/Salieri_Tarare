\clef "treble"
<>-\sug\fp \twoVoices#'(oboe1 oboe2 oboi) <<
  { mib''2. }
  { mib'' }
>> r4 |
R1*3 |
r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { sib''4 sib'' sib'' |
    sib''4. fa''8 fa''4 fa'' |
    sol'' sib'' sib'' sib'' |
    sib''4. fa''8 fa''4 fa'' |
    sol'1 | }
  { sib'4 sib' sib' |
    sib'4. sib'8 sib'4 sib' |
    sib' sib' sib' sib' |
    sib'4. sib'8 sib'4 sib' |
    re'1 | }
  { s2.-\sug\ff | s1 | s4 s2.-\sug\p | s1 | s-\sug\f }
>>
\twoVoices#'(oboe1 oboe2 oboi) <<
  { si'4 s re'' s |
    sol'' s si'' s |
    do''' }
  { re'4 s si'! s |
    si' s re'' s |
    mib'' }
  { s4-\sug\f r s r | s r s r | }
>> r4 r2\fermata |
R1*8 |
\tag #'oboi <>^"unis" mib''8-\sug\f do'' la' fad' mib'' do'' la' fad' |
mib''4 r r2 |
R1*5 |
mib''8-\sug\f do'' la' fad' mib'' do'' la' fad' |
mib''4 r r2 |
R1*2 |
R1^\fermataMarkup |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''1 | }
  { mib'' | }
>>
R1*3 |
r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { sib''4 sib'' sib'' |
    sib''4. fa''8 fa''4 fa'' |
    sol'' sib'' sib'' sib'' |
    sib''4. fa''8 fa''4 fa'' |
    sol''1 | }
  { sib'4 sib' sib' |
    sib'4. sib'8 sib'4 sib' |
    mib'' sib' sib' sib' |
    sib'4. sib'8 sib'4 sib' |
    sib'1 | }
  { s2.-\sug\f | s1 | s1-\sug\p | s1 | s1-\sug\f }
>>
R1*2 |
r2 r\fermata |
r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { mib'4 sol' sib' |
    mib'' sib'2 sib'4 |
    sib'1 |
    do'' |
    fa''2. lab''4 |
    sol''1 |
    sol''2 fa''4. sib''8 |
    sib''2.\fermata }
  { mib'4 sol' sib' |
    mib'' sol'2 sol'4 |
    sol'1 |
    lab' |
    do''2. lab'4 |
    mib''1 |
    mib''2 re''4. fa''8 |
    sib'2.\fermata }
  { s2.-\sug\p | s1*2 | s1*2-\sug\cresc | s1-\sug\f }
>> r4 |
r4 <>\p \twoVoices#'(oboe1 oboe2 oboi) <<
  { mib'4 sol' sib' |
    sib'1 |
    sib' |
    do'' |
    fa''2. lab''4 |
    sol''1~ |
    sol''2 fa'' |
    sol''4. mib''8 mib''4. mib''8 |
    lab''4. lab''8 sol''4. sol''8 |
    fa''4. fa''8 fa''4. fa''8 |
    sol''1 |
    fa'' |
    mib''4 sib'' sib'' sib'' |
    sib''1 |
    sib''4 sib'' sib'' sib'' |
    sib''1 |
    sib''4 }
  { mib'4 sol' sib' |
    sol'1 |
    sol' |
    lab' |
    do''2. lab'4 |
    mib''1~ |
    mib''2 re'' |
    mib''4. sol'8 sol'4. sol'8 |
    lab'4. mib''8 mib''4. mib''8 |
    mib''4. mib''8 mib''4. mib''8 |
    mib''1 |
    re'' |
    mib''4 sib' sib' sib' |
    sib'1 |
    mib''4 sib' sib' sib' |
    re''1 |
    mib''4 }
  { s2. | s1*4 | s1*2-\sug\f | s2-\sug\fp s-\sug\fp |
    s2-\sug\fp s-\sug\fp | s-\sug\fp s-\sug\fp | s1\f }
>> r4 r2 |
r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { mib'4 sol' sib' |
    mib''4. mib''8 mib''4 mib'' |
    mib''2 }
  { mib'4 sol' sib' |
    mib''4. sol'8 sol'4 sol' |
    sol'2 }
>> r2 |
