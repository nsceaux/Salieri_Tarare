\clef "treble" \transposition mib
<>-\sug\fp \twoVoices#'(corno1 corno2 corni) <<
  { do''2. }
  { do'' }
>> r4 |
R1*3 |
r4 \twoVoices#'(corno1 corno2 corni) <<
  { mi''4 mi'' mi'' |
    re''4. re''8 re''4 re'' |
    mi'' mi'' mi'' mi'' |
    re''4. re''8 re''4 re'' |
    mi''1 | }
  { do''4 do'' do'' |
    sol'4. sol'8 sol'4 sol' |
    do'' do'' do'' do'' |
    sol'4. sol'8 sol'4 sol' |
    mi'1 | }
  { s2.-\sug\ff | s1 | s4 s2.\p | s1 | s1\f }
>>
<>\f \twoVoices#'(corno1 corno2 corni) <<
  { mi''4 s mi'' s |
    mi'' s mi'' s |
    do'' }
  { mi' s mi' s |
    mi' s mi' s |
    mi' }
  { s r s r | s r s r | }
>> r4 r2\fermata |
R1*19 |
R1^\fermataMarkup
\twoVoices#'(corno1 corno2 corni) <<
  { mi''1 | }
  { do'' | }
>>
R1*3 |
r4 \twoVoices#'(corno1 corno2 corni) <<
  { mi''4 mi'' mi'' |
    re''4. re''8 re''4 re'' |
    mi'' mi'' mi'' mi'' |
    re''4. re''8 re''4 re'' |
    mi''1 | }
  { do''4 do'' do'' |
    sol'4. sol'8 sol'4 sol' |
    do'' do'' do'' do'' |
    sol'4. sol'8 sol'4 sol' |
    do''1 | }
  { s2.-\sug\f | s1 | s1\p | s1 | s\f }
>>
R1*2 |
r2 r\fermata |
<>\p \twoVoices#'(corno1 corno2 corni) <<
  { do''1~ | do''~ | do'' | }
  { do'1~ | do'~ | do' | }
>>
R1*2 |
<>-\sug\f \twoVoices#'(corno1 corno2 corni) <<
  { mi''1 |
    mi''2 re''4. sol''8 |
    sol''2.\fermata }
  { do''1 |
    do''2 sol' |
    do''2.\fermata }
>> r4 |
<>-\sug\p \twoVoices#'(corno1 corno2 corni) <<
  { do''1~ | do'' | do'' | }
  { do'~ | do' | do' }
>>
R1*2 |
<>\f \twoVoices#'(corno1 corno2 corni) <<
  { mi''1~ |
    mi''2 re'' |
    do''4. do''8 do''4. do''8 |
    do''4. do''8 do''4. do''8 |
    re''4. re''8 re''4. re''8 |
    mi''1 |
    re'' |
    do''4 mi'' mi'' mi'' |
    re''1 |
    mi''4 mi'' mi'' mi'' |
    re''1 |
    do''4 sol' do'' mi'' |
    sol'' do''' sol'' mi'' |
    do''4. do''8 do''4 do'' |
    do''2 }
  { do''1~ |
    do''2 sol' |
    do''4. do''8 do''4. do''8 |
    do''4. do''8 do''4. do''8 |
    do''4. do''8 do''4. do''8 |
    do''1 |
    sol' |
    mi'4 do'' do'' do'' |
    sol'1 |
    do''4 do'' do'' do'' |
    sol'1 |
    mi'4 sol do' mi' |
    sol' do'' sol' mi' |
    do'4. mi'8 mi'4 mi' |
    mi'2 }
  { s1*2 | s2\fp s\fp | s2\fp s\fp | s2\fp s\fp | s1-\sug\f }
>> r2 |


\twoVoices#'(corno1 corno2 corni) <<
  {
  }
  {
  }
>>
