\score {
  \new StaffGroup <<
    \new Staff <<
      \global \keepWithTag #'trombone1 \includeNotes "tromboni"
      { \quoteViolinoI "AAAviolin"
        \quoteViolinoII "AAAviolinII"
        s1*2
        <>_\markup\tiny "Vln" \cueTreble "AAAviolin" {
          \mmRestUp s1 \mmRestCenter s1 \mmRestDown s1 \mmRestCenter s1
          \mmRestDown s1 \mmRestCenter s1 \mmRestDown s1*4 \mmRestUp s1*12
        }
        \break s1*3
        \cueTreble "AAAviolin" { \mmRestDown s1*2 \mmRestUp s1*4 }
        s1*3
        \cueTreble "AAAviolinII" { \mmRestDown s1*2 \mmRestUp s1*4 }
        s1*3
        \cueTreble "AAAviolin" {
          \mmRestDown s1*3 \mmRestUp s1*3 \mmRestDown s1*3
        }
        s1
        \cueTreble "AAAviolin" s1
        s1
        \cueTreble "AAAviolin" { \mmRestUp s1*2 \mmRestDown s1*2 }
        s1*5 \mmRestUp
        \cueTreble "AAAviolin" s1*2
        s1*5
        \cueTreble "AAAviolin" { \mmRestDown s1*4 \mmRestUp s1*3 }
        s1*5 \mmRestDown
        \cueTreble "AAAviolin" s1
        s1*6
        \cueTreble "AAAviolin" { s1 \mmRestUp s1 \mmRestCenter } \pageBreak
      }
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'trombone2 \includeNotes "tromboni"
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'trombone3 \includeNotes "tromboni"
    >>
  >>
  \layout { }
}
