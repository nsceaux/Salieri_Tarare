\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Trompettes en Ut }
    } <<
      \keepWithTag #'() \global
      \includeNotes "trombe"
      { \quoteViolinoI "AAAviolin"
        \quoteViolinoII "AAAviolinII"
        \mmRestUp
        <>^"[a 2]" s1*12
        <>_\markup\tiny "Vln" \cue "AAAviolin" s1*2
        <>^"[a 2]" s1*3
        \cue "AAAviolin" s1
        <>^"[a 2]" s1*2
        \cue "AAAviolin" s1*2
        s1*2 <>^"[a 2]" s1*3
        \cue "AAAviolin" { \mmRestDown s1*2 \mmRestUp s1*3 \mmRestDown s1 }
        <>^"[a 2]" s1*3
        \cue "AAAviolinII" { s1*2 \mmRestUp s1*4 }
        <>^"[a 2]" s1*3
        \cue "AAAviolin" { \mmRestDown s1*2 }
        <>^"[a 2]" s1*2
        \cue "AAAviolin" {
          \mmRestUp s1*2
          \mmRestDown s1*3
          \mmRestUp s1
          \mmRestDown s1
          \mmRestUp s1
          \mmRestDown s1*10
          \mmRestUp s1
          \mmRestDown s1*10
          \mmRestUp s1*2
          }
        <>^"[a 2]" s1*5
        \cue "AAAviolin" { \mmRestDown s1*3 }
        s1*4
        \cue "AAAviolin" { s1 \mmRestUp s1 }
        <>^"[a 2]" s1*4
        \cue "AAAviolin" { \mmRestDown s1*3 }
        <>^"[a 2]" s1*16 <>^"[a 2]" s1*4
        \cue "AAAviolin" { \mmRestUp s1*7 }
        <>^"[a 2]" s1*2
        \cue "AAAviolin" s1*6
      }
    >>
    \new Staff \with {
      \haraKiriFirst
      \tinyStaff 
      \remove "Page_turn_engraver"
    } <<
      \global \includeNotes "timpani"
    >>
  >>
  \layout { indent = \largeindent }  
}
