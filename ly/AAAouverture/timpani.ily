\clef "bass" R1*61 |
sib,2:16-\sug\ff sib,:16 |
sib,:16 sib,:16 |
sib,:16 sib,:16 |
sib,:16 sib,:16 |
sib,4 r r2 | \allowPageTurn
R1*2 |
sib,2:16-\sug\ff sib,:16 |
sib,:16 sib,:16 |
sib,:16 sib,:16 |
sib,:16 sib,:16 |
sib,4 r r2 |
R1*4 |
R1^\fermataMarkup |
R1*2 |
re2:16-\sug\ff re:16 |
re:16 re:16 |
re:16 re:16 |
re:16 re:16 |
re4 r r2 |
R1*16 |
re2:16\ff re:16 |
re:16 re:16 |
re4 r r2 |
re1:16 |
re4 r r2 |
sib,4 r r2 |
sib,4 r r2 |
sib,4 r r2 |
sib,2:16\p sib,2:16 |
sib,2:16\cresc sib,2:16 |
sib,2:16 sib,2:16 |
sib,4\! r r2 |
R1*23 |
