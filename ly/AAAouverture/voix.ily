<<
  \tag #'(recit basse) {
    \clef "soprano/treble" R1*112 |
    r2 r4 <>^\markup\character-text La Nature Récit mi''8 mi'' |
    la''4. mi''8 mi''4 re''8 mi'' |
    dod''2 dod''4 dod''8 re'' |
    mi''4. mi''8 mi''4 mi''8 fa'' |
    sol''2 mi''4. fa''8 |
    re''2 re''8 r re'' re'' |
    fad''2 fad''8 fad'' fad'' sol'' |
    re''2 sol''8 sol'' sol'' sol'' |
    lab''!2 <>^\markup\italic a piacere re''4 mib'' |
    sib'2 sib'4
    \tag #'recit { r4 | R1*14 | }
  }
  \tag #'(vhaute-contre basse) {
    <<
      \tag #'vhaute-contre {
        \clef "alto/G_8" R1*121 |
        r2 r4
      }
      \tag #'basse {
        s1*121 s2. \clef "alto/G_8"
      }
    >> <>^\markup\character Chœur sib4 |
    sib sib8 sib sib4 sib8 sib |
    sib2~ sib8 sib sib sib |
    re'4 re' re'4. re'8 |
    mib'2 mib'8 r mib'8. mib'16 |
    reb'2 reb'4. reb'8 |
    reb'?2. reb'4 |
    do'1^\f |
    r4 fa'^\p fa' re' |
    si1 |
    do'2 do'4 do' |
    sol1 |
    sol2 r |
    R1*2
  }
  \tag #'vtaille {
    \clef "tenor/G_8" R1*121 |
    r2 r4 fa |
    sol sol8 sol sol4 sol8 sol |
    lab2~ lab8 lab lab lab |
    lab4 lab lab4. lab8 |
    sol2 sol8 r sol8. sol16 |
    sib2 sib4. sib8 |
    sib2. sib4 |
    lab1^\f |
    r4 lab^\p lab lab |
    sol1 |
    sol2 sol4 do' |
    sol1 |
    sol2 r |
    R1*2
  }
  \tag #'vbasse {
    \clef "bass/bass" R1*121 |
    r2 r4 re |
    mib mib8 mib mib4 mib8 mib |
    fa2~ fa8 fa fa fa |
    fa4 fa fa4. fa8 |
    mib2 mib8 r mib8. mib16 |
    sol2 sol4. sol8 |
    sol2. sol4 |
    lab1^\f |
    r4 fa^\p fa fa |
    fa1 |
    mib2 mib4 do |
    sol1 |
    sol2 r |
    R1*2
  }
>>

