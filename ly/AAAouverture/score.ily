\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \flautiInstr } <<
        \global \includeNotes "flauti"
        { s1 \beginMark "La Nuit" s1*7
          s2. \beginMark "On lève la Toile" }
      >>
      \new Staff \with { \oboiInstr } << \global \includeNotes "oboi" >>
      \new Staff \with { \fagottiInstr } << \global \includeNotes "fagotti" >>
    >>
    \new StaffGroup <<
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \smaller en mi♭ }
        shortInstrumentName = "Cor."
      } << \keepWithTag #'corni \global \includeNotes "corni" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \smaller en ut }
        shortInstrumentName = "Tr."
      } << \keepWithTag #'trompettes \global \includeNotes "trombe" >>
      \new Staff \with { \tromboniInstr } << \global \includeNotes "tromboni" >>
      \new Staff \with { \timpaniInstr } << \global \includeNotes "timpani" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'recit \includeNotes "voix"
      >> \keepWithTag #'recit \includeLyrics "paroles"
    >>
    \new StaffGroup <<
      \new Staff \with { \violoncelliInstr \haraKiriFirst } <<
        \global \keepWithTag #'violoncelli \includeNotes "basso"
      >>
      \new Staff \with {
        \bassoInstr
        \consists "Metronome_mark_engraver"
      } <<
        \global \keepWithTag #'basso \includeNotes "basso"
        \origLayout {
          s1*8\break s1*6\pageBreak
          s1*10\break s1*6\pageBreak
          s1*7\break s1*6\pageBreak
          s1*6\break s1*6\pageBreak
          s1*6\break s1*6\pageBreak
          s1*6\break s1*6\pageBreak
          s1*7\break s1*5\pageBreak
          s1*6\break s1*7\pageBreak
          s1*8\pageBreak
          s1*6\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*6\break s1*7\pageBreak
          \grace s8. s1*7\break s1*7\pageBreak
          s1*7\break s1*6\pageBreak
          s1*5\break \grace s8 s1*5\pageBreak
          s1*6\break s1*5\break s1*4\pageBreak
          s1*5\break s1*5\pageBreak
          s1*5\break s1*5\pageBreak
          s1*4\break s1*4\break
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
