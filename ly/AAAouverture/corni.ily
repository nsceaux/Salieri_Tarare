\transposition mib
\clef "treble" mi''1~ |
mi''~ |
mi''~ |
mi''~ |
mi''-\sug\cresc~ |
mi''~ |
mi''-\sug\cresc~ |
mi'' |
mi''-\sug\p |
mi''-\sug\cresc |
<< { mi''2 mi'' | mi'' mi'' } \\
  { mi''2-\sug\f mi'' | mi'' mi'' | } >>
R1*12 |
<do'' mi''>1\ff |
q |
<< mi''2 \\ do'' >> r |
R1*6 |
<mi'' mi'>1-\sug\ff~ |
q~ |
q2 r |
R1*6 |
<do'' mi'>1-\sug\ff~ |
q~ |
q2 r |
R1*9 |
q1 |
R1 |
<do'' mi'>1 |
R1*4 |
<do'' mi''>1-\sug\ff |
<sol' re''> |
<do'' mi''> |
<sol' re''> |
<< { do''2 mi'' | fa'' re'' | sol'' sol' | } \\
  { do''2 mi'' | fa'' re'' | sol'' sol' | } >>
<do'' mi''>1-\sug\ff |
<re'' sol'> |
<do'' mi''> |
<re'' sol'> |
<< { do''2 mi'' | fa'' } \\
  { do''2-\sug\ff mi''-\sug\ff | fa'' }
>> r2 |
R1*3 |
R1^\fermataMarkup |
R1*7 |
mi''1 |
fa''4 r r2 |
R1 |
<< { do''2 do'' | do'' do'' | do'' do'' | do''2 }
  \\ { do''2\ff do''\ff | do''\ff do''\ff | do''\ff do''\ff | do''2-\sug\ff }
>> r2 |
R1*2 |
<do'' do'''>1\ff~ |
q |
R1*2 |
re''1\ff~ |
re'' |
<< { mi''2 mi'' } \\ { mi''2 mi'' } >> |
<mi'' sol''>1-\sug\ff~ |
q |
fad''4 r r2 |
<mi'' sol''>1 |
<< fad''4 \\ fad'' >> r r2 |
r4 <mi'' sol''>2-\sug\sf q4 |
r4 <mi'' sol''>2-\sug\sf q4 |
r4 <mi'' sol''>2-\sug\sf q4 |
q1\p~ |
q\cresc~ |
q\! |
<< fad''4 \\ fad'' >> r r2 |
R1*7 |
<< { re''2 re''4 mi'' } \\ { sol'2 sol'4 do'' } >>
r8 << { sol' sol' sol' sol' sol' sol' sol' }
  \\ { sol' sol' sol' sol' sol' sol' sol' } >> |
sol'1~ |
sol'~ |
sol'~ |
sol' |
mi''\f~ |
mi'' |
re''1\f~ |
re''\p |
<mi'' mi'>1~ |
q~ |
q~ |
q |
q-\sug\ff |
q |
