\tag #'violoncelli \startHaraKiri
\clef "bass" R1*8 |
do8\p do do do do2:8\cresc |
do:8 do:8 |
do:8\f do:8 |
do:8 do:8 |
do2.( si,4\p |
la, si, do dod) |
re2:8 re:8\cresc |
re:8 re:8 |
re2.\f do!4\p |
si,( do re red)\cresc |
mi2:8 mi:8 |
mi:8 mib:8 |
re:8\f fad:8 |
sol:8\fp sol:8\fp |
sol:8\fp sol:8\fp |
fa:8\fp fa:8\fp |
mib8\ff do do do do2:8 |
do:8 do:8 |
do2 r |
R1*4 |
<<
  \tag #'violoncelli {
    \stopHaraKiri do'4( sib la sol) |
    fad1 | \startHaraKiri
  }
  \tag #'basso { R1*2 }
>>
sol2:8\ff sol:8 |
sol:8 sol:8 |
<<
  \tag #'violoncelli {
    \stopHaraKiri \clef "tenor"
    sol2~ sol8 la16 sib do' re' mi' fad' |
    sol'2. fa'!4\p |
    mib'( re' do' sib!) |
    la( sol fad \clef "bass" mib) |
    re( do sib, la,) |
    sol,( fa re do) |
    si,1 | \startHaraKiri
  }
  \tag #'basso { sol,2 r | R1*6 | }
>>
do2:8\ff do:8 |
do:8 do:8 |
do2~ do8 re16 mib fa sol la si |
<<
  \tag #'violoncelli {
    \stopHaraKiri \slurDashed do'2.( sib!8*2/3 do' sib) |
    lab2.( sol8*2/3 lab sol) | \slurSolid
    fa( sol fa mib fa mib re mib re do re do) |
    si,( do si, lab,! sib, lab,) sol,2 | \startHaraKiri
  }
  \tag #'basso {
    do'2. sib!4 |
    lab2. sol4 |
    fa( mib re do) |
    si, lab,! sol,2 |
  }
>>
sol,1 |
do2 r |
<<
  \tag #'violoncelli {
    \stopHaraKiri fa8( sol16 lab sib do' re' mib') fa'4-! mib'-! |
    re'1 | \startHaraKiri
  }
  \tag #'basso { R1*2 } >>
mib8( fa16 sol lab sib do' re') mib'4 mib' |
mib1 |
mib8( fa16 sol lab sib do' re') mib'4 mib' |
mib1 |
<<
  \tag #'violoncelli {
    \stopHaraKiri mib8\p( fa16 sol lab sib do' re') mib'4 mib' |
    mib8( fa16 sol lab sib do' re') mib'4 mib' | \startHaraKiri
  }
  \tag #'basso { R1*2 }
>>
mib8*2/3(\p fa sol) \once\slurDashed sib,( do re) mib(\cresc fa sol) \once\slurDashed sib,( do re) |
mib fa sol sib, do re mib fa sol sib, do re |
mib4\ff sib sol mib |
re8 mib fa mib re sib, do re |
mib4 sib sol mib |
re8 mib fa mib re sib, do re |
mib4 r sol r |
lab r fa r |
sib4 r sib, r |
mib\ff sib sol mib |
re8 mib fa mib re sib, do re |
mib4 sib sol mib |
re8 mib fa mib re sib, do re |
mib2\ff sol\ff |
<<
  \tag #'violoncelli {
    \stopHaraKiri \clef "tenor" lab2~ lab8 sib16 do' re' mib' fa' sol' |
    lab'2. sol'4 |
    fa'2-\sug\sf \clef "bass" \startHaraKiri
  }
  \tag #'basso { lab2 r | R1 | fa2\sf }
>> la2\sf |
fad2\sf~ fad8 sol16 la sib do' re' mi' |
fad'2.\fermata re4\p |
<<
  \tag #'violoncelli {
    \stopHaraKiri sol8*2/3( la sib) re( mi fad) sol(\cresc la sib) re( mi fad) |
    \startHaraKiri
  }
  \tag #'basso {
    sol4 re sol\cresc re |
  }
>>
sol8*2/3( la sib) re( mi fad) sol( la sib) re( mi fad) |
sol4\ff re' sib sol |
fad8 sol la sol fad re mi fad |
sol4 re' sib sol |
fad8 sol la sol fad re mi fad |
sol2:8\p sol:8 |
sol:8 sol:8 |
<< lab!:8 { s4 s\cresc } >> lab2:8 |
lab:8 lab:8 |
lab2\ff sol\ff |
lab\ff sib\ff |
lab\ff sol\ff |
lab,8 sib,16 do reb mib fa sol lab2:8 |
lab4 r r2 |
lab,8 sib,16 do reb mib fa sol lab2:8 |
lab:8\ff lab:8 |
lab:8 lab:8 |
sol:8\p sol:8 |
sol:8 sol:8 |
fa:8\ff fa:8 |
fa:8 fa:8 |
mi:8\fp mib:8\fp |
re:8\ff re:8 |
re:8 re:8 |
re:8 re:8 |
re:8 re:8 |
re:8 re:8 |
mib4 mib'2\sf mib'4 |
re re'2\sf re'4 |
dod dod'2\sf dod'4 |
dod2:8\p\< dod:8 |
dod:8\cresc dod:8 |
dod:8 dod:8 |
dod:16\fp dod:16 |
dod:16 dod:16 |
dod:16 dod:16 |
dod:16 dod:16 |
dod:16 dod:16 |
re:16 re:16 |
do!:16 do:16 |
sib,:16 sib,:16 |
sib,2 r |
r r4 sib,\p |
mib16(\< fa sol lab sib\> lab sol fa) mib4\! r4 |
sib,16(\< do re mib fa\> mib re do) sib,4\! r4 |
sib,16(\< do re mib fa\> mib re do) sib,4\! r4 |
mib16(\< fa sol lab sib\> lab sol fa) mib4\! r4 |
mi16(\ff\< fa sol lab sib\> lab sol fa) mi4\! r4 |
mi16(\< fa sol lab sib\> lab sol fa) mi4\! r4 |
fa16\ff sol lab! sib do' sib lab sol fa sol lab sib do' sib lab sol |
fa4 r r2 |
sol,16\p la, si, do re do si, la, sol,4 r |
do16 re mib fa sol fa mib re do4 r |
sol,16 la, si, do re do si, la, sol,4 r |
do16 re mib fa sol fa mib re do4 r |
sol,16\ff la, si, do re do si, la, sol,4 r |
do16 re mib fa sol fa mib re do4 r |
