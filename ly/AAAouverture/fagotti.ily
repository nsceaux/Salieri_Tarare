\clef "bass" do1~ |
do~ |
do~ |
do~ |
do\cresc~ |
do~ |
do\cresc~ |
do |
<< { do'1 | do'~ | do'2 do' | do' do' | }
  \\ { do1-\sug\p | do-\sug\cresc~ | do2-\sug\f do | do do | } >>
do'2.( si4\p |
la si do' dod') |
<< re'1~  { s2 s-\sug\cresc } >> |
re'1 |
re'2.(\f do'4)\p |
si( do' re' red')-\sug\cresc |
mi'1~ |
mi'2 mib' |
re'-\sug\f fad' |
sol' r |
R1*2 |
<< { do'1~ | do' | do'2 }
  \\ { do1-\sug\ff~ | do | do2 }
>> r2 |
R1*6 |
<< { do'1~ | do'~ | do'2 }
  \\ { do1-\sug\ff~ | do~ | do2 }
>> r2 |
R1*6 |
do1\ff~ |
do~ |
do2~ do8 re16 mib fa sol la si |
do'2.( sib!4) |
lab2.( sol8*2/3 lab sol) |
fa( sol fa mib fa mib re mib re do re do) |
si,4 lab,! sol,2 |
sol,1 |
do2 r |
R1*3 |
<sol sib>1 |
R1 |
<sol sib>1 |
R1*2 |
mib4-\sug\p sib, mib-\sug\cresc sib, |
mib8*2/3 fa sol sib, do re mib fa sol sib, do re |
mib4\ff sib sol mib |
re8 mib fa mib re sib, do re |
mib4 sib sol mib |
re8 mib fa mib re sib, do re |
mib4 r sol r |
lab r fa r |
sib4 r sib, r |
mib\ff sib sol mib |
re8 mib fa mib re sib, do re |
mib4 sib sol mib |
re8 mib fa mib re sib, do re |
mib2-\sug\ff sol-\sug\ff |
lab2 r |
R1*3 |
R1^\fermataMarkup |
R1*2 |
sol4\ff re' sib sol |
fad8 sol la sol fad re mi fad |
sol4 re' sib sol |
fad8 sol la sol fad re mi fad |
sol4 r r2 |
R1*3 |
\clef "tenor" mib'2-\sug\ff mib'-\sug\ff |
mib'-\sug\ff mib'-\sug\ff |
mib'-\sug\ff mib'-\sug\ff |
mib'-\sug\ff r |
R1 |
\clef "bass" lab,8 sib,16 do reb mib fa sol lab2:8 |
<do' mib'>1\ff~ |
q |
do'8*2/3(\p re' mib') sol( la si) do'( re' mib') sol( la si) |
\slurDashed do'( re' mib') sol( la si) do'( re' mib') sol( sib mib') | \slurSolid
reb'1-\sug\ff |
\clef "tenor" fa' |
mi'2 mib' |
re'!1-\sug\ff |
re' |
re'2. do'4 |
<<
  { sib4 sol'2 sol'4 |
    fad' re'2 do'4 |
    sib sol'2 sol'4 } \\
  { sib4 sib2 sib4 |
    la re'2 do'4 |
    sib mib'2-\sug\sf mib'4 | }
>>
r4 << { re'2 re'4 } \\ { re'2-\sug\sf re'4 } >> |
r4 << { dod'2 dod'4 } \\ { dod'2-\sug\sf dod'4 } >> |
dod'1-\sug\p~ |
dod'-\sug\cresc~ |
dod'~ |
dod'-\sug\p~ |
dod' |
dod'~ |
dod'~ |
dod' |
re' |
do'! |
sib~ |
sib |
\tieDashed << { sib2~ sib4 } \\ { sib2~ sib4 } >> r4 | \tieSolid
\clef "bass" \sugNotes { mib16(\< fa sol lab sib\> lab sol fa) mib4\! r4 } |
sib,16(\< do re mib fa\> mib re do) sib,4\! r4 |
sib,16(\< do re mib fa\> mib re do) sib,4\! r4 |
mib16(\< fa sol lab sib\> lab sol fa) mib4\! r4 |
mi16(\ff\< fa sol lab sib\> lab sol fa) mi4\! r4 |
mi16(\< fa sol lab sib\> lab sol fa) mi4\! r4 |
fa16-\sug\ff sol lab! sib do' sib lab sol fa sol lab sib do' sib lab sol |
fa4 r r2 |
R1*6 |
