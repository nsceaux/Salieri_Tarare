\score {
  \new StaffGroup <<
    \new Staff \with { \violoncelliInstr \haraKiriFirst } <<
      \global \keepWithTag #'violoncelli \includeNotes "basso"
    >>
    \new Staff << \global \keepWithTag #'basso \includeNotes "basso" >>
  >>
  \layout {
    short-indent = 5\mm
  }
}
