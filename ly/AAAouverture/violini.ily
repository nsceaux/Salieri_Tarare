\clef "treble" R1*2 |
sol'16(\p la' sol' fa') mi'( fa' mi' re') do'4 r |
R1 |
do''16( mi'' re''\cresc do'') si'( la' sol' fa') mi'4 r |
R1 |
mi''16( sol'' fa''\cresc mi'' re'' do'' si' la') sol'4 r |
R1 |
sol''8(\p la''16 si'' do''' si'' do''' si'' \rt#4 { do''')\cresc si'' } |
\rt#4 { do''' si'' } \rt#4 { do''' si'' } |
do'''\f mi''' re''' do''' si'' la'' sol'' fa'' mi'' do''' si'' la'' sol'' fa'' mi'' re'' |
do'' sol'' fa'' mi'' re'' do'' si' la' sol' do'' si' la' sol' fa' mi' re' |
do'2.( si4)\p |
la4:16 si:16 do':16 dod':16 |
re'2:16 re':16\cresc |
re'2:16 re'2:16 |
re'2.(\f do'!4)\p |
si4:16 do':16 re':16 red':16\cresc |
mi'2:16 mi':16 |
mi':16 mib':16 |
re':16\f fad':16 |
<<
  \tag #'violino1 {
    <sol' sol>16\fp sol'[ sol' sol'] sol'4:16 << sol'2:16 sol4\fp >> |
    <lab lab'>16\fp lab'[ lab' lab'] lab'4:16 << lab'2:16 lab4\fp >> |
    <re' si'>16\fp si'[ si' si'] si'4:16 << si'2:16 re'4\fp >> |
    <mib' do''>2:16\ff q:16 |
    q:16 q:16 |
    do''2~ do''8 re''16 mib'' fa'' sol'' la'' si'' |
    do'''2. sib''!8*2/3\p( do''' sib'' |
    lab''! sib'' lab'' sol'' lab'' sol'' fa'' sol'' fa'' mib'' fa'' mib'') |
    re'' mib'' re'' do'' re'' do'' si' do'' si' lab'! sib' lab' |
    sol' lab' sol' fa' sol' fa' mib' fa' mib' re' mib' re' |
    do'16 mib' mib' mib' mib'4:16 mib'2:16 |
    re'8 mi'16 fad' sol' lab' si' dod'' re''4 do'' |
    sib'16\ff <sib re'>[ q q] q4:16 q2:16 |
    q:16 q:16 |
    q2 r |
    R1*6 |
    do'2:16\ff do':16 |
    do':16 do':16 |
    do'2 r |
    do''2~ do''8 re''16 mib'' fa'' sol'' lab'' sib'' |
    do'''2. sib''8*2/3( do''' sib'' |
    lab'' sib'' lab'' sol'' lab'' sol'' fa'' sol'' fa'' mib'' fa'' mib'') |
    re''( mib'' re'' do'' re'' do'' si' do'' si' lab'! sib' lab') |
    \once\slurDashed sol'( lab' sol' fa' sol' fa' mib' fa' mib' re' mib' re') |
    do'2 r |
    lab'8( sib'16 do'' re'' mib'' fa'' sol'') lab''4-! sol''-! |
    fa''1 |
    sol''8( lab''16 sib'' do''' re''' mib''' fa''') sol'''4 sol''' |
    <sol mib'>1 |
    sol''8( lab''16 sib'' do''' re''' mib''' fa''') sol'''4 sol''' |
    <sol mib'>1 |
  }
  \tag #'violino2 {
    sol'16\fp <si re'>16[ q q] q4:16 q2:16 |
    <do' mib'>2:16\fp q:16\fp |
    <sol re'>2:16\fp q:16\fp |
    <sol mib'>2:16\ff q:16 |
    q:16 q:16 |
    do'2~ do'8 re'16 mib' fa' sol' la' si' |
    do''2. sib'!4(-\sug\p |
    lab'! sol' fa' mib') |
    re'( do' si lab!) |
    sol4 fa'8*2/3 sol' fa' mib' fa' mib' re' mib' re' |
    do'4 r r2 |
    R1 |
    <sol sol'>2:16\ff q:16 |
    q:16 q:16 |
    \once\tieDashed q2~ sol'8 la'16 sib' do'' re'' mi'' fad'' |
    sol''2. fa''!8*2/3\p sol'' fa'' |
    mib''( fa'' mib'' re'' mib'' re'' do'' re'' do'' sib'! do'' sib') |
    la'( sib' la' sol' la' sol' fad' sol' fad' mib'! fa' mib') |
    re'( mib' re' do' re' do' sib do' sib la sib la) |
    sol16 lab! lab lab lab4:16 lab2:16 |
    sol8 la16 si do' re' mi' fad' sol'4 fa' |
    mib'16\ff sol sol sol sol4:16 sol2:16 |
    sol2:16 sol:16 |
    sol2 r |
    R1*2 |
    re''2~ re''8 mib''16 fa'' sol'' la'' si'' do''' |
    re'''2. do'''8*2/3 re''' do''' |
    si''( do''' si'' lab''! sib'' lab'' sol'' lab'' sol'' fa'' sol'' fa'') |
    mib''8( fa''16 sol'' lab'' sib'' do''' re''') mib'''4-! re'''-! |
    do'''1 |
    sib'8( do''16 re'' mib'' fa'' sol'' lab'') sib''4-! lab''-! |
    sol''1 |
    sib'8( do''16 re'' mib'' fa'' sol'' lab'') sib''4 sib'' |
    <sol mib'>1 |
    sib'8( do''16 re'' mib'' fa'' sol'' lab'') sib''4 sib'' |
  }
>>
mib'8\p( fa'16 sol' lab' sib' do'' re'') mib''4 mib'' |
mib'8( fa'16 sol' lab' sib' do'' re'') mib''4 mib'' |
mib''8*2/3(\p fa'' sol'') sib'( do'' re'') mib''(\cresc fa'' sol'') sib'( do'' re'') |
mib'' fa'' sol'' sib' do'' re'' mib'' fa'' sol'' sib' do'' re'' |
mib''\ff <<
  \tag #'violino1 {
    <sib' sib''>8*2/3 q q q q q2.*2/3:8 |
    q:8 q:8 |
    q:8 q:8 |
    q:8 q:8 |
  }
  \tag #'violino2 {
    <sib' sol''>8*2/3 q q q q q2.*2/3:8 |
    <sib' lab''>:8 q:8 |
    <sib' sol''>:8 q:8 |
    <sib' lab''>:8 q:8 |
  }
>>
sol''16 lab'' sib'' lab'' sol'' fa'' mib'' re'' mib'' fa'' sol'' fa'' mib'' re'' do'' sib' |
do'' re'' mib'' re'' do'' sib' lab' sol' lab' sib' do'' sib' lab' sol' fa' mib' |
re' do' sib do' re' mib' fa' sol' lab' sol' fa' mib' re' do' sib lab |
sol8*2/3 <>\ff <<
  \tag #'violino1 {
    <sib' sib''>8*2/3 q q q q q2.*2/3:8 |
    q:8 q:8 |
    q:8 q:8 |
    q:8 q:8 |
  }
  \tag #'violino2 {
    <sib' sol''>8*2/3 q q q q q2.*2/3:8 |
    <sib' lab''>:8 q:8 |
    <sib' sol''>:8 q:8 |
    <sib' lab''>:8 q:8 |
  }
>>
sol''16 lab'' sib'' lab'' sol'' fa'' mib'' re'' mib'' fa'' sol'' fa'' mib'' re'' do'' sib' |
do''2~ do''8 re''16 mib'' fa'' sol'' lab'' sib'' |
do'''2. sib''4 |
la''16 sib'' do''' sib'' la'' sol'' fa'' mi'' fa'' sol'' la'' sol'' fa'' mib'' re'' mib'' |
re''2~ re''8 mib''!16 fad'' sol'' la'' sib'' do''' |
re'''2.\fermata re'4\p |
sol'8*2/3( la' sib') re'( mi' fad') sol'(\cresc la' sib') re'( mi' fad') |
sol'( la' sib') re'( mi' fad') sol'( la' sib') re'( mi' fad') |
sol'8*2/3\ff <<
  \tag #'violino1 {
    <re'' re'''>8*2/3 q q q q q2.*2/3:8 |
    q:8 q:8 |
    q:8 q:8 |
    q:8 q:8 |
    q8
  }
  \tag #'violino2 {
    <re'' sib''>8*2/3 q q q q q2.*2/3:8 |
    <re'' do'''>:8 q:8 |
    <re'' sib''>:8 q:8 |
    <re'' do'''>:8 q:8 |
    <re'' sib''>8
  }
>> sol''8\p sol'' sol'' sol''2:8 |
sol'':8 sol'':8 |
lab''!16( sol'' lab'' sol'' lab''\cresc sol'' lab'' sol'' \rt#4 { lab''16\< sol'') } |
\rt#4 { lab''16( sol'' } \rt#4 { lab''16 sol'')\! }
lab''16(\ff sol'' fa'' mib'' reb'' do'' sib' lab') reb''(\ff do'' sib' lab' sol' fa' mib' reb') |
do'\ff reb' mib' fa' sol' lab'! sib' do'' reb''\ff do'' sib' do'' reb'' mib'' fa'' sol'' |
lab''!\ff sol'' fa'' mib'' reb'' do'' sib' lab' reb''\ff do'' sib' lab' sol' fa' mib' reb' |
do'8 reb'16 mib' fa' sol' lab' sib' do''2:8 |
<<
  \tag #'violino1 {
    do''8 reb''16 mib'' fa'' sol'' lab'' sib'' do'''2:8 |
  }
  \tag #'violino2 {
    lab8 sib16 do' reb' mib' fa' sol' lab'2:8 |
  }
>>
lab8 sib16 do' reb' mib' fa' sol' lab'2:8 |
lab'16(\ff do'' mib'' do'' mib'' do'' mib'' do'' \rt#4 { mib'' do'') } |
\rt#4 { mib''( do'' } \rt#4 { mib'' do'') } |
\rt#4 { mib''(\p do'' } \rt#4 { mib'' do'') } |
\rt#4 { mib'' do'' } mib'' do'' mib'' do'' mib'' sib' mib'' sib' |
<<
  \tag #'violino1 {
    reb''\ff lab'' lab'' lab'' lab''4:16 lab''2:16 |
    lab'':16 lab'':16 |
    sol'':16\fp sol'':16\fp |
    <sib'! sol''>:16\ff q:16 |
  }
  \tag #'violino2 {
    <reb''>2:16\ff q:16 |
    q:16 q:16 |
    q:16\fp q:16\fp |
    <re' re''!>:16\ff q:16 |
  }
>>
<sib' sol''>:16 q:16 |
<la' fad''>16 re' re' re' re'4:16 re':16 do':16 |
sib16 sol la sib do' re' mi' fad' sol' la' sib' do'' re'' mi'' fad'' sol'' |
fad'' re' re' re' re'2:16 do'4:16 |
<<
  \tag #'violino1 {
    sib4 <sib' sib''>2\sf q4 |
    re'4 <re'' re'''>2\sf q4 |
    dod'4 <mi'' mi'''>2\sf q4 |
    <mi'' mi'''>2:16\p q:16 |
    q:16\cresc q:16 |
    q:16 q:16 |
    <mi'' mi'''>2:16\fp q:16 |
    q:16 q:16 |
    q:16 q:16 |
    q:16 q:16 |
    q:16 q:16 |
    re''':16 re''':16 |
    re''':16 re''':16 |
    re''':16 re''':16 |
    re'''2 r |
    sib16\p sib sib sib sib4:16 sib2:16 |
    sib:16 sib:16 |
    sib:16 sib:16 |
    sib:16 sib:16 |
    sib2.:16 mib'4:16 |
    reb'2:16\ff reb':16 |
    reb':16 reb':16 |
    do'16\ff
  }
  \tag #'violino2 {
    sib4 <sib' sol''>2-\sug\sf q4 |
    re'4 <sib' sol''>2-\sug\sf q4 |
    dod' <sib' sol''>2-\sug\sf q4 |
    <sib' sol''>2:16-\sug\p q:16 |
    q:16-\sug\cresc q:16 |
    q:16 q:16 |
    <la' sol''>:16\fp q:16 |
    q:16 q:16 |
    q:16 q:16 |
    q:16 q:16 |
    q:16 q:16 |
    <la' fa''>:16 q:16 |
    <la' fad''>:16 q:16 |
    <sib'! sol''>:16 q:16 |
    <re' sib'! lab''!>2 r |
    r r4 sib-\sug\p |
    sol2:16 sol:16 |
    lab:16 lab:16 |
    lab:16 lab:16 |
    sol:16 sol:16 |
    sib:16\ff sib:16 |
    sib:16 sib:16 |
    lab!16-\sug\ff
  }
>> <do'' lab''>16 q q q4:16 q2:16 |
q16 <<
  \tag #'violino1 {
    do'16\p do' do' do'4:16 do'2:16 |
    si:16 si:16 |
    do':16 do':16 |
    si:16 si:16 |
    do':16 do':16 |
    si:16\ff si:16 |
    do':16 do':16 |
  }
  \tag #'violino2 {
    lab16\p lab lab lab4:16 lab2:16 |
    sol:16 sol:16 |
    sol:16 sol:16 |
    sol:16 sol:16 |
    sol:16 sol:16 |
    sol:16-\sug\ff sol:16 |
    sol:16 sol:16 |
  }
>>
