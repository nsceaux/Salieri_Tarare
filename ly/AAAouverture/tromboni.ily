\clef "alto" R1*24 |
<\tag #'trombone3 sol \tag #'trombone2 do' \tag #'trombone1 mib'>1-\sug\ff |
q |
q2 r |
R1*6 |
<\tag #'trombone3 sib \tag #'trombone2 re' \tag #'trombone1 sol'>1-\sug\ff~ |
q~ |
q2 r |
R1*6 |
\clef "bass" <\tag #'trombone3 do \tag #'trombone2 mib \tag #'trombone1 do'>1\ff~ |
q~ |
q2 r |
R1*9 |
\clef "alto" <\tag #'trombone3 sol \tag #'trombone2 sib \tag #'trombone1 mib'>1 |
R1 |
q1 |
R1*4 |
<\tag #'trombone3 mib' \tag #'trombone2 sol' \tag #'trombone1 sib'>1-\sug\ff |
<\tag #'trombone3 fa' \tag #'trombone2 lab' \tag #'trombone1 sib'> |
<\tag #'trombone3 mib' \tag #'trombone2 sol' \tag #'trombone1 sib'> |
<\tag #'trombone3 fa' \tag #'trombone2 lab' \tag #'trombone1 sib'> |
<\tag #'trombone3 mib' \tag #'trombone2 sol' \tag #'trombone1 sib'>4 r r2 |
R1*2 |
<\tag #'trombone3 mib' \tag #'trombone2 sol' \tag #'trombone1 sib'>1\ff |
<\tag #'trombone3 fa' \tag #'trombone2 lab' \tag #'trombone1 sib'> |
<\tag #'trombone3 mib' \tag #'trombone2 sol' \tag #'trombone1 sib'> |
<\tag #'trombone3 fa' \tag #'trombone2 lab' \tag #'trombone1 sib'> |
<\tag #'trombone3 mib' \tag #'trombone2 sol' \tag #'trombone1 sib'>4 r r2 |
R1*7 | \allowPageTurn
<\tag #'trombone3 sib \tag #'trombone2 re' \tag #'trombone1 sol'>1-\sug\ff |
<\tag #'trombone3 do' \tag #'trombone2 re' \tag #'trombone1 la'> |
<\tag #'trombone3 sib \tag #'trombone2 re' \tag #'trombone1 sol'> |
<\tag #'trombone3 do' \tag #'trombone2 re' \tag #'trombone1 la'> |
<\tag #'trombone3 sib \tag #'trombone2 re' \tag #'trombone1 sol'>4 r r2 |
R1 |
<< <\tag #'trombone3 lab! \tag #'(trombone1 trombone2) lab'!>1~ { s4 s-\sug\cresc } >> |
q1 |
<\tag #'trombone3 do' \tag #'trombone2 mib' \tag #'trombone1 lab'>2-\sug\ff
<\tag #'trombone3 reb' \tag #'trombone2 mib' \tag #'trombone1 sib'>-\sug\ff |
<\tag #'trombone3 do' \tag #'trombone2 mib' \tag #'trombone1 lab'>-\sug\ff
<\tag #'trombone3 sib \tag #'trombone2 mib' \tag #'trombone1 sol'>-\sug\ff |
<\tag #'trombone3 do' \tag #'trombone2 mib' \tag #'trombone1 lab'>-\sug\ff
<\tag #'trombone3 reb' \tag #'trombone2 mib' \tag #'trombone1 sib'>-\sug\ff |
<\tag #'trombone3 do' \tag #'trombone2 mib' \tag #'trombone1 lab'>2-\sug\ff r2 |
R1*2 | \allowPageTurn
<\tag #'trombone3 lab \tag #'trombone2 do' \tag #'trombone1 mib'>1-\sug\ff |
<\tag #'trombone3 lab_( \tag #'trombone2 do'~ \tag #'trombone1 mib'~> |
<\tag #'trombone3 sol) \tag #'trombone2 do' \tag #'trombone1 mib'>~ |
<\tag #'trombone3 sol \tag #'trombone2 do' \tag #'trombone1 mib'>2
_\tag #'trombone3 -\markup\tiny { [Source : la♭] } ~ q4 r |
<\tag #'trombone3 lab \tag #'trombone2 reb' \tag #'trombone1 lab'>1-\sug\ff~ |
q |
<\tag #'trombone3 sol \tag #'trombone2 reb' \tag #'trombone1 sol'>2 q |
<\tag #'trombone3 sib \tag #'trombone2 re'! \tag #'trombone1 sol'>1-\sug\ff |
<\tag #'trombone3 sib \tag #'trombone2 re' \tag #'trombone1 sol'> |
<\tag #'trombone3 la \tag #'trombone2 re' \tag #'trombone1 fad'>4 r r2 |
<\tag #'trombone3 re' \tag #'trombone2 sol' \tag #'trombone1 sib'>1 |
<\tag #'trombone3 re' \tag #'trombone2 fad' \tag #'trombone1 la'>4 r r2 |
r4 <\tag #'trombone3 mib' \tag #'trombone2 sol' \tag #'trombone1 sib'>2-\sug\sf q4 |
r4 <\tag #'trombone3 re' \tag #'trombone2 sol' \tag #'trombone1 sib'>2-\sug\sf q4 |
r4 <\tag #'trombone3 mi' \tag #'trombone2 sol' \tag #'trombone1 sib'>2-\sug\sf q4 |
q1-\sug\p~ |
q-\sug\cresc~ |
<\tag #'trombone3 mi'~ \tag #'trombone2 sol'~ \tag #'trombone1 sib'(> |
<\tag #'trombone3 mi' \tag #'trombone2 sol' \tag #'trombone1 la')>-\sug\p~ |
q |
q~ |
q~ |
q |
<\tag #'trombone3 re' \tag #'trombone2 fa' \tag #'trombone1 la'> |
<\tag #'trombone3 re' \tag #'trombone2 fad' \tag #'trombone1 la'> |
<\tag #'trombone3 re' \tag #'trombone2 sol' \tag #'trombone1 sib'> |
<\tag #'trombone3 re' \tag #'trombone2 fa' \tag #'trombone1 lab'!>2 r |
\clef "bass" R1 |
<\tag #'trombone3 mib!( \tag #'trombone2 sol( \tag #'trombone1 sib!~>1\p |
<\tag #'trombone3 fa) \tag #'trombone2 lab!) \tag #'trombone1 sib>~ |
q |
<\tag #'trombone3 mib \tag #'trombone2 sol \tag #'trombone1 sib> |
<\tag #'trombone3 sol \tag #'trombone2 sib \tag #'trombone1 reb'>\ff~ |
q |
<\tag #'trombone3 fa \tag #'trombone2 lab! \tag #'trombone1 do'>4
\clef "alto" <\tag #'trombone3 do' \tag #'trombone2 fa' \tag #'trombone1 lab'>2.~ |
q4 r r2 |
R1*6 |
