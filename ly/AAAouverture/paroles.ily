\tag #'(recit basse) {
  C’est as -- sez trou -- bler l’u -- ni -- vers,
  Vents fu -- ri -- eux, ces -- sez d’a -- gi -- ter l’air et l’on -- de.
  C’est as -- sez. Re -- pre -- nez vos fers ;
  que le seul Zé -- phyr règne au mon -- de.
}
\tag #'(choeur basse) {
  Ne tour -- men -- tons plus l’u -- ni -- vers :
  ces -- sons d’a -- gi -- ter l’air et l’on -- de.
  Mal -- heu -- reux ! re -- pre -- nons nos fers,
  l’heu -- reux Zé -- phyr seul règne au mon -- de.
}
\tag #'(recit basse) {
  De l’orbe é -- cla -- tant du so -- leil,
  ad -- mi -- rant des cieux la struc -- tu -- re,
  je vous ai vue, bel -- le Na -- tu -- re,
  dis -- po -- ser sur la terre un su -- perbe ap -- pa -- reil.
  
  Gé -- nie ar -- dent de la sphère en -- flam -- mé -- e,
  par qui la mienne est a -- ni -- mé -- e,
  à mes tra -- vaux don -- nez quel -- ques mo -- ments.
  De tou -- tes les ra -- ces pas -- sé -- es,
  dans l’im -- men -- si -- té dis -- per -- sé -- es,
  je ras -- sem -- ble les é -- lé -- ments,
  pour en for -- mer u -- ne ra -- ce pro -- chai -- ne
  de la ri -- sible es -- pèce hu -- mai -- ne,
  aux dé -- pens des ê -- tres vi -- vants.

  Ce pou -- voir ab -- so -- lu que vous a -- vez sur el -- le,
  l’e -- xer -- cez- vous aus -- si sur les in -- di -- vi -- dus ?

  Oui, si je des -- cen -- dais à quel -- que soins per -- dus !
  Mais, pour moi, qu’est u -- ne par -- cel -- le,
  à tra -- vers ces fou -- les d’hu -- mains,
  que je ré -- pands à plei -- nes mains
  sur cet -- te ter -- re, pour y naî -- tre,
  bril -- ler un ins -- tant, dis -- pa -- raî -- tre,
  lais -- sant à des hom -- mes nou -- veaux
  pres -- sés comme eux, dans la car -- riè -- re,
  de main en main, les courts flam -- beaux
  de leur ex -- is -- tence é -- phé -- mè -- re ?
  
  Au moins, vous em -- ploy -- ez des é -- lé -- ments plus purs,
  pour for -- mer les puis -- sants et les grands d’un em -- pi -- re ?
  
  C’est leur lan -- gage, il faut bien en sou -- ri -- re :
  un noble or -- gueil les en rend pres -- que surs.
  Mais voy -- ez com -- me la Na -- tu -- re
  les ver -- se par mil -- liers, sans choix et sans me -- su -- re.
}
