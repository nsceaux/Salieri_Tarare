\clef "treble" R1*6 |
<sol' mi''>1-\sug\cresc~ |
q |
<mi'' sol''>-\sug\p |
q-\sug\cresc |
q2-\sug\f q |
q q |
R1*2 |
<< { re''1~ | re''~ | re''2 r } { s2 s\cresc s1 s2\! } >> |
R1 |
mi''1\cresc~ |
mi''2 mib'' |
re''-\sug\f fad'' |
sol'' sol'' |
lab'' lab'' |
si'' si'' |
<< { do''1 | do'' | do''2 }
  \\ { mib'1-\sug\ff | mib' | mib'2 }
>> r2 |
R1*6 |
sol'1-\sug\ff~ |
sol'~ |
sol'2 r |
R1*6 |
do'1-\sug\ff~ |
do'~ |
do'2 r |
<<
  { <>^\markup\italic Solo do''~ do''8 re''16 mib'' fa'' sol'' lab'' sib'' |
    do'''2.( sib''4 |
    lab'' sol'' fa'' mib'' |
    re'' do'' si' lab'!) |
    \once\slurDashed sol'( fa' mib' re') |
    do'2 r | }
  \new Voice { \voiceTwo R1*6 }
>>
R1*5 |
mib'1 |
R1*4 |
<< { sib''1~ | sib''~ | sib''~ | sib''~ | sib''4 }
  \\ { sol''1-\sug\ff | lab'' | sol'' | lab'' | sol''4 }
>> r4 r2 |
R1*2 |
<< { sib''1 | sib'' | sib'' | sib'' | sib''4 }
  \\ { sol''1\ff | lab'' | sol'' | lab'' | sol''4 }
>> r4 r2 |
do''2~ do''8 re''16 mib'' fa'' sol'' lab'' sib'' |
do'''2. sib''4 |
la'' r r2 |
R1 |
R1^\fermataMarkup |
R1*2 |
<< { sib''1 | do''' | sib'' | do''' | sib''4 }
  \\ { re''1-\sug\ff~ | re''~ | re''~ | re''~ | re''4 }
>> r4 r2 |
R1 |
lab''!1 |
lab'' |
<mib'' do'''>2-\sug\ff <sib'' mib''>-\sug\ff |
<do''' mib''>-\sug\ff <reb''' mib''>-\sug\ff |
<mib'' do'''>2-\sug\ff <sib'' mib''>-\sug\ff |
<do''' mib''>2-\sug\ff r |
<<
  { do''8 reb''16 mib'' fa'' sol'' lab'' sib'' do'''2:8 | do'''4 } \\
  { lab'8 sib'16 do'' reb''! mib'' fa'' sol'' lab''2:8 | lab''4 }
>> r4 r2 |
<do''' mib''>1-\sug\ff |
q |
do''8*2/3(\f re'' mib'') sol'( la' si') do''( re'' mib'') sol'( la' si') |
do''( re'' mib'') sol'( la' si') do''( re'' mib'') sol'( sib' mib'') |
<<
  { reb''4 lab''2 lab''4 | lab''1 |
    sol''2 sol'' | sol''1 | sol'' | fad''4 } \\
  { reb''4-\sug\ff reb''2 reb''4 | reb''1 |
    reb''2 reb'' | re''!4-\sug\ff sib'2 sib'4 | sib'1 | la'4 }
>> r4 r2 |
<sol'' sib''>1 |
<fad'' la''>4 r r2 |
r4 <sol'' sib''>2-\sug\sf q4 |
r4 <sol'' sib''>2-\sug\sf q4 |
r4 <sol'' sib''>2-\sug\sf q4 |
<sol'' sib''>1\p~ |
q\cresc~ |
q\! |
<la'' sol''>-\sug\p~ |
q |
q~ |
q |
q~ |
<la'' fa''> |
<fad'' la''> |
<re'' sol''> |
<< { lab''!2 lab''4 sol'' | \once\tieDashed fa''2~ fa''4 }
  \\ { re''2 fa''4 mib'' | \once\tieDashed re''2~ re''4 }
>> r4 |
R1*6 |
<lab'' do''>1-\sug\f~ |
q4 r r2 |
R1*6 |
