\clef "treble" R1*2 |
sol'16(\p la' sol' fa') mi'( fa' mi' re') do'4 r |
R1 |
do''16( mi'' re''\cresc do'') si'( la' sol' fa') mi'4 r |
R1 |
mi''16( sol'' fa''\cresc mi'' re'' do'' si' la') sol'4 r |
R1 |
sol''8(\p la''16 si'' do''' si'' do''' si'' \rt#4 { do''')\cresc si'' } |
\rt#4 { do''' si'' } \rt#4 { do''' si'' } |
do'''\f mi''' re''' do''' si'' la'' sol'' fa'' mi'' do''' si'' la'' sol'' fa'' mi'' re'' |
do'' sol'' fa'' mi'' re'' do'' si' la' sol' do'' si' la' sol' fa' mi' re' |
do'2.( si4)\p |
la4:16 si:16 do':16 dod':16 |
<< <re'' re'''>1 { s2 s\cresc } >> |
q1 |
q2.\f do'!4\p |
si4:16 do':16 re':16 red':16\cresc |
<mi'' mi'''>1 |
q2 <mib'' mib'''> |
<re'' re'''>\f fad'' |
sol''\fp sol''\fp |
<do'' lab''>\fp q\fp |
<re'' si''>\fp q\fp |
<mib' do''>2:16\ff q:16 |
q:16 q:16 |
q2~ \voiceOne do''8 re''16 mib'' fa'' sol'' la'' si'' |
do'''2. sib''!8*2/3\p( do''' sib'' |
lab''! sib'' lab'' sol'' lab'' sol'' fa'' sol'' fa'' mib'' fa'' mib'') |
re'' mib'' re'' do'' re'' do'' \oneVoice si' do'' si' lab'! sib' lab' |
sol' lab' sol' fa' sol' fa' mib' fa' mib' re' mib' re' |
do'16 mib' mib' mib' mib'4:16 mib'2:16 |
re'8 mi'16 fad' sol' la' si' dod'' re''4 do'' |
<sib' do'' sol'>1\ff |
q |
q2~ sol'8 la'16 sib' \voiceOne do'' re'' mi'' fad'' |
sol''2. fa''!8*2/3\p sol'' fa'' |
mib''( fa'' mib'' re'' mib'' re'' do'' re'' do'' sib'! do'' sib') |
\oneVoice la'( sib' la' sol' la' sol' fad' sol' fad' mib'! fa' mib') |
re'( mib' re' do' re' do' sib do' sib la sib la) |
sol16 lab! lab lab lab4:16 lab2:16 |
sol8 la16 si do' re' mi' fad' sol'4 fa' |
<do' mib'>2:16\ff q:16 |
q:16 q:16 |
q2 r |
do''2~ do''8 re''16 mib'' fa'' sol'' lab'' sib'' |
do'''2. sib''8*2/3 do''' sib'' |
lab'' sib'' lab'' sol'' lab'' sol'' fa''8 mib''16 fa'' sol'' la'' si'' do''' |
re'''8*2/3 mib''' re''' do''' re''' do''' si'' do''' si'' <<
  { do'''8*2/3 re''' do''' |
    si'' do''' si'' lab''! sib'' lab'' sol'' lab'' sol'' fa'' sol'' fa'' |
    mib''8 fa''16 sol'' lab'' sib'' do''' re''' mib'''4-! re'''-! | } \\
  { lab'!8*2/3 sib' lab' |
    sol' lab' sol' fa' sol' fa' mib' fa' mib' re' mib' re' |
    do'8( re'16 mib' fa' sol' la' si') do''4-! sib'-! | }
>>
lab'8( sib'16 do'' re'' mib'' fa'' sol'') lab''4-! sol''-! |
sib'8( do''16 re'' mib'' fa'' sol'' lab'') sib''4-! lab''-! |
sol''8( lab''16 sib'' do''' re''' mib''' fa''') sol'''4 sol''' |
sib'8( do''16 re'' mib'' fa'' sol'' lab'') sib''4 sib'' |
sol''8( lab''16 sib'' do''' re''' mib''' fa''') sol'''4 sol''' |
sib'8( do''16 re'' mib'' fa'' sol'' lab'') sib''4 sib'' |
mib'8\p( fa'16 sol' lab' sib' do'' re'') mib''4 mib'' |
mib'8( fa'16 sol' lab' sib' do'' re'') mib''4 mib'' |
mib''8*2/3(\p fa'' sol'') sib'( do'' re'') mib''(\cresc fa'' sol'') sib'( do'' re'') |
mib'' fa'' sol'' sib' do'' re'' mib'' fa'' sol'' sib' do'' re'' |
<sib' mib'' sol'' sib''>8*2/3\ff q q q q q q2.*2/3:8 |
<sib' fa'' lab'' sib''>:8 q:8 |
<sib' mib'' sol'' sib''>:8 q:8 |
<sib' fa'' lab'' sib''>:8 q:8 |
sol''16 lab'' sib'' lab'' sol'' fa'' mib'' re'' mib'' fa'' sol'' fa'' mib'' re'' do'' sib' |
do'' re'' mib'' re'' do'' sib' lab' sol' lab' sib' do'' sib' lab' sol' fa' mib' |
re' do' sib do' re' mib' fa' sol' lab' sol' fa' mib' re' do' sib lab |
sol8*2/3 <>\ff <sib' mib'' sol'' sib''> q q q q q2.*2/3:8 |
<sib' fa'' lab'' sib''>:8 q:8 |
<sib' mib'' sol'' sib''>:8 q:8 |
<sib' fa'' lab'' sib''>:8 q:8 |
sol''16 lab'' sib'' lab'' sol'' fa'' mib'' re'' mib'' fa'' sol'' fa'' mib'' re'' do'' sib' |
do''2~ do''8 re''16 mib'' \voiceOne fa'' sol'' lab'' sib'' |
do'''2. sib''4 |
la''16 sib'' do''' sib'' la'' sol'' fa'' mi'' \oneVoice fa'' sol'' la'' sol'' fa'' mib'' re'' mib'' |
re''2~ re''8 mib''!16 fad'' sol'' la'' sib'' do''' |
re'''2.\fermata re'4\p |
sol'8*2/3( la' sib') re'( mi' fad') sol'(\cresc la' sib') re'( mi' fad') |
sol'( la' sib') re'( mi' fad') sol'( la' sib') re'( mi' fad') |
sol'8*2/3\ff <re'' sol'' sib'' re'''>8*2/3 q q q q q2.*2/3:8 |
<re'' la'' do''' re'''>:8 q:8 |
<re'' sol'' sib'' re'''>:8 q:8 |
<re'' la'' do''' re'''>:8 q:8 |
<re'' sol'' sib'' re'''>8
sol''8\p sol'' sol'' sol''2:8 |
sol'':8 sol'':8 |
lab''!16( sol'' lab'' sol'' lab''\cresc sol'' lab'' sol'' \rt#4 { lab''16)\< sol'' } |
\rt#4 { lab''16( sol'' } \rt#4 { lab''16 sol'')\! }
lab''16(\ff sol'' fa'' mib'' reb'' do'' sib' lab') reb''(\ff do'' sib' lab' sol' fa' mib' reb') |
do'\ff reb' mib' fa' sol' lab'! sib' do'' reb''\ff do'' sib' do'' reb'' mib'' fa'' sol'' |
lab''!\ff sol'' fa'' mib'' reb'' do'' sib' lab' reb''\ff do'' sib' lab' sol' fa' mib' reb' |
do'8 reb'16 mib' fa' sol' lab' sib' do''2:8 |
do''8 \voiceOne reb''16 mib'' fa'' sol'' lab'' sib'' do'''2:8 |
\oneVoice lab8 sib16 do' reb' mib' fa' sol' lab'2:8 |
lab'16(\ff do'' mib'' do'' mib'' do'' mib'' do'' \rt#4 { mib'' do'') } |
\rt#4 { mib''( do'' } \rt#4 { mib'' do'') } |
\rt#4 { mib''(\p do'' } \rt#4 { mib'' do'') } |
\rt#4 { mib'' do'' } mib'' do'' mib'' do'' mib'' sib' mib'' sib' |
<reb'' lab''>2:16\ff q2:16 |
q:16 q:16 |
<reb'' sol''>:16\fp q:16\fp |
<sib'! sol''>:16\ff q:16 |
<sib' sol''>:16 q:16 |
<la' fad''>16 re' re' re' re'4:16 re':16 do':16 |
sib16 sol la sib do' re' mi' fad' sol' la' sib' do'' re'' mi'' fad'' sol'' |
fad'' re' re' re' re'2:16 do'4:16 |
r4 <sib' mib'' sol'' sib''>2\sf q4 |
r4 <re'' sol'' sib'' re'''>2\sf q4 |
r4 <mi'' sol'' sib'' mi'''>2\sf q4 |
<sol'' mi'''>2:16\p q:16 |
q:16\cresc q:16 |
q:16 q:16 |
<sol'' mi'''>2:16\fp q:16 |
q:16 q:16 |
q:16 q:16 |
q:16 q:16 |
q:16 q:16 |
<fa'' re'''>:16 q:16 |
<fad'' re'''>:16 q:16 |
<sol'' re'''>:16 q:16 |
<fa'' lab''! re'''>2 <re''' lab'' fa''>4 << mib'''4 \\ { sol''8 mib'' } >> |
<< <sib'' fa'' re''>2 \\ sib'2:16\p >> sib'2:16 |
<mib' sol' sib'>:16 q:16 |
<fa' lab' sib'>:16 q:16 |
q:16 q:16 |
<mib' sol' sib'>2.:16 <sol' sib' mib''>4:16 |
<sol' sib' reb''>2:16\ff q:16 |
q:16 q:16 |
<do'' fa'' lab''>2:16\ff q2:16 |
<fa' lab' do''>2:16\p q:16 |
<fa' sol' si'>2:16 q:16 |
<mib' sol' do''>:16 q:16 |
<re' sol' si'>:16 q:16 |
<mib' sol' do''>:16 q:16 |
<re' sol' si'>:16\ff q:16 |
<mib' sol' do''>:16 q:16 |
