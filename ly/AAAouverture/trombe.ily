\clef "treble" mi'1~ |
mi'~ |
mi'~ |
mi'~ |
mi'\cresc~ |
mi'~ |
mi'\cresc~ |
mi' |
mi'-\sug\p |
mi'-\sug\cresc |
mi'2-\sug\f mi' |
mi' mi' |
R1*2 |
<< { re''1~ | re''1~ | re''2 } \\ { s2 s-\sug\cresc | s1 | re''2\! } >> r |
R1 |
<< { mi''1-\sug\cresc~ | mi''2\! } \\ { s1 | mi''2 } >> r |
R1*2 |
<< { do''2 do'' | re'' re'' | } \\ { do''2 do'' | re'' re'' | } >>
do''1\ff |
do'' |
<< do''2 \\ do'' >> r |
R1*6 |
<< { sol'1-\sug\ff~ | sol'~ | sol'2 } \\ { s1*2 sol'2 } >> r |
R1*6 |
<< { do'1-\sug\ff~ | do'~ | do'2 } \\ { s1*2 do'2 } >> r |
R1*2 |
re''1 |
re'' |
R1*31 |
re''1-\sug\ff~ |
re''~ |
re''~ |
re''~ |
re''4 r r2 |
R1*3 |
<<
  { do''2-\sug\ff sol'-\sug\ff | do''-\sug\ff sol'-\sug\ff |
    do''-\sug\ff sol'-\sug\ff | do''2-\sug\ff } | \\
  { do''2 sol' | do'' sol' | do'' sol' | do''2 } |
>> r2 |
R1*2 |
do''1-\sug\ff |
do'' |
<< { do''4 sol' do'' sol' | do''2~ do''4 }
  \\ { do''4\f sol' do'' sol' | do''2~ do''4 }
>> r4 |
R1*3 |
re''1-\sug\ff |
re'' |
re''4 r r2 |
re''1~ |
<< re''4 \new Voice { \voiceOne re'' } >> r r2 |
r4 << { sol''2 sol''4 } \\ { sol'2-\sug\sf sol'4 } >> |
r4 << { re''2 re''4 } \\ { re''2-\sug\sf re''4 } >> |
r4 << { mi''2 mi''4 } \\ { mi''2-\sug\sf mi''4 } >> |
<mi'' mi'>1-\sug\p~ |
q-\sug\cresc~ |
q |
<mi' mi''>1\p~ |
q |
q~ |
q |
q^( |
<<
  { \voiceOne re'')~ | re''~ | re''~ | re''2 \oneVoice }
  \new Voice { \voiceTwo s1*3 | re''2 }
>> r |
R1*7 |
do''1-\sug\f~ |
do''4 r r2 |
R1*6 |
