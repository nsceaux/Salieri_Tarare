\clef "alto" R1*8 |
do'8\p do' do' do' do'2:8\cresc |
do':8 do':8 |
do':8\f do':8 |
do':8 do':8 |
do'2.( si4\p |
la si do' dod') |
re'2:8 re':8\cresc |
re':8 re':8 |
re'2.\f do'!4\p |
si4( do' re' red')\cresc |
mi'2:8 mi':8 |
mi':8 mib':8 |
re':8\f fad':8 |
sol':8\fp sol':8\fp |
sol':8\fp sol':8\fp |
fa':8\fp fa':8\fp |
<sol mib'>:16\ff q:16 |
q:16 q:16 |
q r |
R1*2 |
re'4( do' si lab!) |

sol( fa mib re) |
do2 do'4 sib |
la1 |
sol16-\sug\ff <sol sol'> q q q4:16 q2:16 |
q:16 q:16 |
q r |
R1*6 |
<mib do'>2:16-\sug\ff q:16 |
q:16 q:16 |
q4 do~ do8 re16 mib fa sol la si |
do'2.( sib!8*2/3 do' sib) |
lab2.( sol8*2/3 lab sol) |
fa( sol fa mib fa mib re mib re do re do) |
re4 lab! sol2 |
sol1 |
do'8( re'16 mib' fa' sol' la' si') do''4-! sib'-! |
lab'1 |
sib' |
sib' |
sol8( lab16 sib do' re' mib' fa') sol'4 sol' |
sib1 |
sol8( lab16 sib do' re' mib' fa') sol'4 sol' |
mib8\p( fa16 sol lab sib do' re') mib'4 mib' |
mib8( fa16 sol lab sib do' re') mib'4 mib' |
mib'8*2/3(\p fa' sol') \once\slurDashed sib( do' re') mib'(\cresc fa' sol') \once\slurDashed sib( do' re') |
mib' fa' sol' sib do' re' mib' fa' sol' sib do' re' |
mib'4\ff sib' sol' mib' |
re'8 mib' fa' mib' re' sib do' re' |
mib'4 sib' sol' mib' |
re'8 mib' fa' mib' re' sib do' re' |
mib'4 r sol' r |
lab' r fa' r |
sib'4 r sib r |
mib'\ff sib' sol' mib' |
re'8 mib' fa' mib' re' sib do' re' |
mib'4 sib' sol' mib' |
re'8 mib' fa' mib' re' sib do' re' |
mib'2\ff sol'\ff |
lab2~ lab8 sib16 do' re' mib' fa' sol' |
lab'2. sol'4 |
fa'2-\sug\sf la-\sug\sf |
fad2~ fad8 sol16 la sib do' re' mi' |
fad'2.\fermata re4\p |
sol8*2/3( la sib) re( mi fad) sol(\cresc la sib) re( mi fad) |
sol( la sib) re( mi fad) sol( la sib) re( mi fad) |
sol4\ff re' sib sol |
fad8 sol la sol fad re mi fad |
sol4 re' sib sol |
fad8 sol la sol fad re mi fad |
sol2:8\p sol:8 |
sol:8 sol:8 |
lab'!16( sol' lab' sol' lab'\cresc sol' lab' sol' \rt#4 { lab'\< sol') } |
\rt#4 { lab'( sol' } \rt#4 { lab' sol')\! } |
lab'8-\sug\ff << { mib'4 mib'8 mib' mib'4 mib'8 |
    mib' mib'4 mib'8 mib' mib'4 mib'8 |
    mib' mib'4 mib'8 mib' mib'4 mib'8 |
    mib'16 }
  \\ { do'4 do'8 reb' reb'4 reb'8 |
    do'8 do'4 do'8 reb' reb'4 reb'8 |
    do' do'4 do'8 reb' reb'4 reb'8 | |
    do'16 }
>> lab sib do' reb' mib' fa' sol' lab'2:8 |
lab'4 r r2 |
lab8 sib16 do' reb' mib' fa' sol' lab'2:8 |
lab':8-\sug\ff lab':8 |
lab':8 lab':8 |
sol':8-\sug\p sol':8 |
sol':8 sol':8 |
lab'!:16-\sug\ff lab':16 |
lab':16 lab':16 |
sol':16-\sug\fp sol':16-\sug\fp |
<sol' sib'>8-\sug\ff q4 q q q8 |
q8 q4 q q q8 |
<fad' la'>2:16 q:16 |
<sol' sib'>2:16 q:16 |
<fad' la'>:16 q:16 |
sol4 <mib' mib''>2-\sug\sf q4 |
re' re''2\sf re''4 |
dod'4 dod''2\sf dod''4 |
dod'2:8\p\< dod':8 |
dod':8\cresc dod':8 |
dod':8 dod':8 |
dod':8\fp dod':8 |
dod':8 dod':8 |
dod':8 dod':8 |
dod':8 dod':8 |
dod':8 dod':8 |
re':8 re':8 |
do'!:8 do':8 |
sib:8 sib:8 |
sib2 r |
r2 r4 sib-\sug\p |
mib2:16 mib:16 |
fa:16 fa:16 |
fa:16 fa:16 |
mib:16 mib:16 |
sol:16-\sug\ff sol:16 |
sol:16 sol:16 |
fa16-\sug\ff sol lab! sib do' sib lab sol fa sol lab sib do' sib lab sol |
fa2:16-\sug\p fa:16 |
fa:16 fa:16 |
mib:16 mib:16 |
re:16 re:16 |
mib:16 mib:16 |
re:16-\sug\ff re:16 |
mib:16 mib:16 |
