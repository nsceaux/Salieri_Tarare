\clef "treble" R1*8 |
sol''8(\p la''16 si'' do''' si'' do''' si'' \rt#4 { do''')\cresc si'' } |
\rt#4 { do''' si'' } \rt#4 { do''' si'' } |
do'''\f mi''' re''' do''' si'' la'' sol'' fa'' mi'' do''' si'' la'' sol'' fa'' mi'' re'' |
do'' sol'' fa'' mi'' re'' do'' si' la' sol' do''' si'' la'' sol'' fa'' mi'' re'' |
do''2 r |
R1 |
<< re'''1~ { s2 s-\sug\cresc } >> |
re'''1~ |
re'''2 r |
R1 |
mi'''\cresc~ |
mi'''2 mib''' |
re'''-\sug\f fad'' |
sol''\! r |
R1*25 |
<<
  { <>^\markup\italic Solo re''2~ re''8 mib''16 fa'' sol'' la'' si'' do''' |
    re'''2. do'''4 |
    si''( lab''! sol'' fa'') |
    mib''8( fa''16 sol'' lab'' sib'' do''' re''') mib'''4-! re'''-! |
    do'''1 |
  }
  \new Voice { \voiceTwo R1*5 }
>>
R1 |
sol''8( lab''16 sib'' do''' re''' mib''' fa''') sol'''4 sol''' |
mib'1 |
sol''8( lab''16 sib'' do''' re''' mib''' fa''') sol'''4 sol''' |
mib'1 |
R1*4 |
<< { sib''1~ | sib''~ | sib''~ | sib''~ | sib''4 }
  \\ { sol''1-\sug\ff | lab'' | sol'' | lab'' | sol''4 }
>> r4 r2 |
R1*2 |
<< { sib''1 | sib'' | sib'' | sib'' | sib''4 }
  \\ { sol''1-\sug\ff | lab'' | sol'' | lab'' | sol''4 }
>> r4 r2 |
do''2~ do''8 re''16 mib'' fa'' sol'' lab'' sib'' |
do'''2. sib''4 |
la'' r r2 |
re''2~ re''8 mib''!16 fad'' sol'' la'' sib'' do''' |
re'''2 r\fermata |
R1*2 |
<< { re'''1~ | re'''~ | re'''~ | re'''~ | re'''4 }
 \\ { sib''1-\sug\ff | do''' | sib'' | do''' | sib''4 }
>> r4 r2 |
R1*3 |
<< { do'''2 sib'' | do''' reb''' | do''' sib'' | do''' }
  \\ { mib''\ff mib''\ff | mib''\ff mib''\ff | mib''\ff mib''\ff | mib''\ff }
>> r2 |
<<
  { do''8 reb''16 mib'' fa'' sol'' lab'' sib'' do'''2:8 | do'''4 } \\
  { lab'8 sib'16 do'' reb''! mib'' fa'' sol'' lab''2:8 | lab''4 }
>> r4 r2 |
<do''' mib'''>1\ff~ |
q1 |
do'''8*2/3\f( re''' mib''') sol''( la'' si'') do'''( re''' mib''') sol''( la'' si'') |
do'''( re''' mib''') sol''( la'' si'') do'''( re''' mib''') sol''( sib'' mib''') |
<lab''! reb'''>1-\sug\ff~ |
q~ |
<reb''' sol''>2 q |
<sol'' re'''!>1-\sug\ff |
q |
<fad'' re'''>4 r r2 |
<sib'' sol''>1 |
<la'' fad''>4 r r2 |
r4 <sib'' sol''>2-\sug\sf q4 |
r <re''' sib''>2-\sug\sf q4 |
r <mi''' sib''>2-\sug\sf q4 |
<mi''' sib''>1\p~ |
q\cresc~ |
q ~ |
<mi''' la''>\p~ |
q ~ |
q ~ |
q ~ |
\once\tieDown q~ |
<re''' la''>~ |
q~ |
\tieUp <re''' sol''>~ |
<< { \voiceOne re'''2 re'''4 mib'''! |
    sib''2~ sib''4 \oneVoice }
  \new Voice {
    \voiceTwo lab''!2 lab''4 sol''8 mib'' |
    re''2~ re''4
  }
>> r4 | \allowPageTurn
R1*6 |
<fa''' lab''>1-\sug\f~ |
q4 r r2 |
R1*6 |
