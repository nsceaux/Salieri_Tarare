\clef "bass" <do mi sol>1~ |
q |
q~ |
q |
q~ |
q |
q~ |
q |
<do mi sol do'>2:8 q:8 |
q2:8 q:8 |
q2:8 q:8 |
q2:8 q:8 |
do2.( si,4 |
la, si, do dod) |
<re re'>2:8 q:8 |
q:8 q:8 |
q2. do!4 |
si,( do re red) |
<mi mi'>2:8 q:8 |
q:8 <mib mib'>:8 |
<re re'>:8 <fad fad'>:8 |
<sol si re'>:16 q:16 |
<sol lab do' mib'>:16 q:16 |
<fa si re'>:16 q:16 |
<do sol>2:8 q:8 |
q:8 q:8 |
<do sol do'>2~  do'8 \change Staff = "dessus" \voiceTwo re'16 mib' fa' sol' la' si' |
do''2.( sib'!4 |
lab'! sol' fa' mib') |
re'( do' \change Staff = "basse" \oneVoice si lab!) | |
sol( fa mib re) |
do'4( sib <la do'> <sol sib>) |
<fad la>1 |
<sol re'>2:16 q:16 |
q:16 q:16 |
q2~ sol8 \change Staff = "dessus" \voiceTwo la16 sib do' re' mi' fad' |
sol'2. fa'!4 |
mib'( re' do' sib!) |
\change Staff = "basse" \oneVoice la( sol fad mib) |
re( do sib, la,) |
sol,( fa re do) |
si,1
<do sol>2:16 q:16 |
q:16 q:16 |
q2~ do8 re16 mib fa sol la si |
do'2. sib!8*2/3 do' sib |
lab2. sol8*2/3 lab sol |
fa sol fa mib fa mib re mib re do re do |
si, do si, lab,! sib, lab, sol,4 s |
sol1 |
s1 |
fa8( sol16 lab sib do' re' mib') fa'4-! mib'-! |
<sib re' fa'>1 |
mib8( fa16 sol lab sib do' re') mib'4 mib' |
sol8( lab16 sib do' re' mib' fa') sol'4 sol' |
mib8( fa16 sol lab sib do' re') mib'4 mib' |
sol8( lab16 sib do' re' mib' fa') sol'4 sol' |
mib8( fa16 sol lab sib do' re') mib'4 mib' |
mib8( fa16 sol lab sib do' re') mib'4 mib' |
mib8*2/3( fa sol) sib, do re mib( fa sol) sib, do re |
mib fa sol sib, do re mib fa sol sib, do re |
mib4\ff sib sol mib |
re8 mib fa mib re sib, do re |
mib4 sib sol mib |
re8 mib fa mib re sib, do re |
mib4 r sol r |
lab r fa r |
sib4 r sib, r |
mib\ff sib sol mib |
re8 mib fa mib re sib, do re |
mib4 sib sol mib |
re8 mib fa mib re sib, do re |
mib2\ff sol\ff |
lab2~ lab8 sib16 do' \change Staff = "dessus" \voiceTwo re' mib' fa' sol' |
lab'2. sol'4 |
fa'2 \change Staff = "basse" la2\sf |
fad2\sf~ fad8 sol16 la sib do' re' mi' |
fad'2.\fermata re4 |
sol8*2/3( la sib) re( mi fad) sol( la sib) re( mi fad) |
sol8*2/3( la sib) re( mi fad) sol( la sib) re( mi fad) |
sol4 re' sib sol |
fad8 sol la sol fad re mi fad |
sol4 re' sib sol |
fad8 sol la sol fad re mi fad |
sol2:8 sol:8 |
sol:8 sol:8 |
lab!:8 lab2:8 |
lab:8 lab:8 |
<lab, do mib>8 q4 q8 <sol, sib, reb mib>8 q4 q8 |
<lab, do mib>8 q4 q8 <sib, reb mib sol>8 q4 q8 |
<lab, do mib>8 q4 q8 <sol, sib, reb mib>8 q4 q8 |
<lab, do mib>8 sib,16 do reb mib fa sol lab2:8 |
lab8 \change Staff = "dessus" \voiceTwo sib16 do' reb' mib' fa' sol' lab'2:8 |
\change Staff = "basse" \oneVoice lab,8 sib,16 do reb mib fa sol lab2:8 |
<lab do' mib'>:8\ff q:8 |
q:8 q:8 |
do'8*2/3( re' mib') sol( la si) do'( re' mib') sol( la si) |
do' re' mib' sol la si do' re' mib' sol sib mib' |
fa2:8 fa:8 |
fa:8 fa:8 |
mi:8 mib:8 |
<re! sol sib>8 q4 q4 q4 q8 |
q8 q4 q4 q4 q8 |
<re fad la>2:8 q:8 |
<re sol sib>:8 q:8 |
<re fad la>:8 q:8 |
<mib sol sib>4 mib'2 mib'4 |
<re sib> re'2 re'4 |
<dod sib> dod'2 dod'4 |
<dod sib>2:8 q:8 |
q:8 q:8 |
q:8 q:8 |
<dod la>:8\fp q:8 |
q:8 q:8 |
q:8 q:8 |
q:8 q:8 |
q:8 q:8 |
<re la>:8 q:8 |
<do! la>:8 q:8 |
<sib, sib>:16 q:16 |
q2 r |
r2 r4 sib, |
mib16(\< fa sol lab sib\> lab sol fa) mib4\! r4 |
sib,16(\< do re mib fa\> mib re do) sib,4\! r4 |
sib,16(\< do re mib fa\> mib re do) sib,4\! r4 |
mib16(\< fa sol lab sib\> lab sol fa) mib4\! r4 |
mi16(\< fa sol lab sib\> lab sol fa) mi4\! r4 |
mi16(\< fa sol lab sib\> lab sol fa) mi4\! r4 |
fa16 sol lab! sib do' sib lab sol fa sol lab sib do' sib lab sol |
fa4 r r2 |
sol,16 la, si, do re do si, la, sol,4 r |
do16 re mib fa sol fa mib re do4 r |
sol,16 la, si, do re do si, la, sol,4 r |
do16 re mib fa sol fa mib re do4 r |
sol,16 la, si, do re do si, la, sol,4 r |
do16 re mib fa sol fa mib re do4 r |
