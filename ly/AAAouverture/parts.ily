\piecePartSpecs
#`((reduction)
   (flauti #:score-template "score-part-voix"
           #:music , #{
\quoteViolinoI "AAAviolini"
\quoteViolinoII "AAAviolinII"
s1*26 <>^\markup\tiny "Vln"
\cue "AAAviolin" {
  \mmRestDown s1*3
  \mmRestUp s1*3
  \mmRestDown s1
  \mmRestUp s1*3
}
\cue "AAAviolinII" { \mmRestDown s1*2 \mmRestUp s1*4 }
\cue "AAAviolin" {
  s1*3 \mmRestDown s1*2 \mmRestCenter
}
               #} )


   (oboi #:score-template "score-part-voix")
   (fagotti #:score-template "score-part-voix")
   
   (violino1)
   (violino2)
   (viola)
   (basso #:score "score-basso")

   (trombe #:score "score-trombe")
   (tromboni #:score "score-tromboni")
   (timpani #:music , #{
\quoteViolinoI "AAAviolin"
\quoteViolinoII "AAAviolinII"
s1*2 <>^\markup\tiny "Vln"
\cueTreble "AAAviolin" {
  \mmRestUp s1
  \mmRestCenter s1
  \mmRestDown s1
  \mmRestCenter s1
  \mmRestDown s1
  \mmRestCenter s1
  \mmRestDown s1*3
  \mmRestUp s1*10
  \mmRestDown s1*2
  \mmRestUp s1*3
  \mmRestDown s1*3
  \mmRestUp s1*3
  \mmRestDown s1
  \mmRestUp s1*3
}
\cueTreble "AAAviolinII" { \mmRestDown s1*2 \mmRestUp s1*4 }
\cueTreble "AAAviolin" {
  s1*3 \mmRestDown s1*3 \mmRestUp s1*3 \mmRestDown s1*3 \mmRestUp s1
  \mmRestDown s1 \mmRestUp s1*3 \mmRestDown s1*2
}
s1*5
\cueTreble "AAAviolin" { \mmRestUp s1*2 }
s1*5
\cueTreble "AAAviolin" { \mmRestDown s1*5 \mmRestUp s1*2 }
s1*5
\cueTreble "AAAviolin" { \mmRestDown s1*8 \mmRestUp s1 \mmRestDown s1*7 }
\mmRestCenter
                        #})
   (corni  #:tag-global ()
           #:instrument "Cors en mi♭"
           #:music , #{
\quoteViolinoI "AAAviolin"
\quoteViolinoII "AAAviolinII"
<>^"[a 2]" s1*12
<>^\markup\tiny "Vln"
\cueTransp "AAAviolin" do' { \mmRestUp s1*9 \mmRestDown s1*2 \mmRestUp s1 }
s1*3
\cueTransp "AAAviolin" do' { \mmRestDown s1*2 \mmRestUp s1*3 \mmRestDown s1 }
s1*3
\cueTransp "AAAviolinII" do' { \mmRestDown s1*2 \mmRestUp s1*4 }
s1*3
\cueTransp "AAAviolin" do' { \mmRestDown s1*3 \mmRestUp s1*3 \mmRestDown s1*3 }
s1
\cueTransp "AAAviolin" do' s1
s1
\cueTransp "AAAviolin" do' s1*4
s1*13
\cueTransp "AAAviolin" do' { s1*3 \mmRestUp s1*3 \mmRestDown s1*5 }
<>^"[a 2]" s1*2
\cueTransp "AAAviolin" do' s1
s1*4
\cueTransp "AAAviolin" do' { s1 \mmRestUp s1 }
s1*2
\cueTransp "AAAviolin" do' { \mmRestDown s1*2 }
<>^"[a 2]" s1*15
\cueTransp "AAAviolin" do' s1*7
s1*2 <>^"[a 2]" s1*14 << mi''4 \\ mi' >>
                       #}))
