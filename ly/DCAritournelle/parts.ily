\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso #:tag-notes basso)
   (oboi #:tag-notes oboi #:system-count 5)
   (clarinetti #:notes "oboi" #:tag-notes oboi #:system-count 5)
   (flauti #:notes "oboi" #:tag-notes oboi #:system-count 5)
   (fagotti #:notes "basso" #:tag-notes fagotti)
   (corni #:instrument , #{ \markup\center-column { Cors \small en La } #}
                           #:tag-global ())
   (timpani #:notes "tambour" #:instrument "Tambour")
   (silence #:on-the-fly-markup , #{\markup\tacet #}))
