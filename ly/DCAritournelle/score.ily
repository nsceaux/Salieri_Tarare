\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        instrumentName = \markup\center-column { Hautbois Clarinettes et Flûte }
        shortInstrumentName = \markup\center-column { Htb. Cl. Fl. }
        \consists "Metronome_mark_engraver"
      } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small in La }
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "basso"
      >>
      \new Staff \with {
        instrumentName = "Tambour"
        shortInstrumentName = "Tamb."
      } << \global \includeNotes "tambour" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'basso \includeNotes "basso"
        \origLayout { s2*8\pageBreak s2*10\break \grace s8 }
      >>
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
