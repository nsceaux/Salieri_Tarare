\clef "treble"
<<
  \tag #'violino1 {
    <mi' dod'' la''>4.\ff \grace si''8 la''16 sold''32 la'' |
    mi''4. \grace fad''8 mi''16 red''32 mi'' |
    dod''4. \grace re''8 dod''16 si'32 dod'' |
    la'4. la'8 |
    sold'[( fad' mi') mi''-!] |
    dod''[( si' la') la''-!] |
    sold''[-! fad''-! mi''-! re''-!] |
    dod''[ si' la' sold'] |
  }
  \tag #'violino2 {
    <mi' dod''>16-\sug\ff q q q q4:16 |
    q2:16 |
    q:16 |
    <mi' la'>16 <mi' dod'> q q q8 la'16 la' |
    sold'8:16[ fad':16 mi':16 mi'':16] |
    dod''16 dod'' si' si' la' la' la'' la'' |
    sold''8:16[ fad'':16 mi'':16 re'':16] |
    dod'':16[ si':16 la':16 sold':16] |
  }
>>
fad'8. red''16 red''8.\trill dod''32 red'' |
mi''8 mi''16. mi''32 mi''8 mi'' |
<<
  \tag #'violino1 {
    <mi' dod'' la''>4. \grace si''8 la''16 sold''32 la'' |
    mi''4. \grace fad''8 mi''16 red''32 mi'' |
    dod''4. \grace re''8 dod''16 si'32 dod'' |
    la'4. la'8 |
  }
  \tag #'violino2 {
    <mi' dod''>16 q q q q4:16 |
    q2:16 |
    q:16 |
    <mi' la'>16 <mi' dod'> q q q8 r |
  }
>>
do''4\sf( si'8) la' |
la'[( sold') sold' mi'']( |
red''[\sf do'' si' la')] |
<<
  \tag #'violino1 {
    la'4( sold'8) mi' |
    \grace mi''8 re''! dod''16 re'' mi''8 re'' |
    dod''4. dod''8 |
    \grace dod''8 si' la'16 si' dod''8 si' |
    la'4. dod''8 |
    fad''[ dod'' la' fad'] |
    dod''-![ sold'!-! mid'-! dod'-!] |
    fad'-![ la'-! dod''-! fad''-!] |
    dod''4. mid''8 |
    mid''8. sold''16 sold''8. dod'''16 |
    dod'''2\fermata |
  }
  \tag #'violino2 {
    mi'16\ff <mi' mi''> q q q4:16 |
    q2:16 |
    q16 dod''[ dod'' dod''] dod''4:16 |
    dod''2:16 |
    dod''16 <la' la''> q q q8 dod''16 dod'' |
    fad''8:16[ dod'':16 la':16 fad':16] |
    dod'':16[ sold'!:16 mid':16 dod':16] |
    fad':16[ la':16 dod'':16 fad'':16] |
    dod''4. sold'8 |
    sold'8. mid''16 mid''8. mid''16 |
    mid''2\fermata |
  }
>>
