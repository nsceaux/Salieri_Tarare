\clef "treble" \transposition la
<>-\sug\ff \twoVoices#'(corno1 corno2 corni) <<
  { do''8 do''16. do''32 do''4~ |
    do''8 do''16. do''32 do''4~ |
    do''8 do''16. do''32 do''4~ |
    do''8 do''16. do''32 do''8 }
  { mi'8 mi'16. mi'32 mi'4~ |
    mi'8 mi'16. mi'32 mi'4~ |
    mi'8 mi'16. mi'32 mi'4~ |
    mi'8 mi'16. mi'32 mi'8 }
>> r8 |
R2*6 |
\twoVoices#'(corno1 corno2 corni) <<
  { do''8 do''16. do''32 do''4~ |
    do''8 do''16. do''32 do''4~ |
    do''8 do''16. do''32 do''4~ |
    do''8 do''16. do''32 do''8 }
  { mi'8 mi'16. mi'32 mi'4~ |
    mi'8 mi'16. mi'32 mi'4~ |
    mi'8 mi'16. mi'32 mi'4~ |
    mi'8 mi'16. mi'32 mi'8 }
>> r8 |
R2*3 |
r8 <>-\sug\ff \twoVoices#'(corno1 corno2 corni) <<
  { sol''16 sol'' sol''8 sol'' |
    sol''2 |
    sol''8 mi''16. mi''32 mi''8 mi'' |
    mi''2~ |
    mi''8 do''16. do''32 do''8 }
  { sol'16 sol' sol'8 sol' |
    sol'2 |
    sol'8 mi'16. mi'32 mi'8 mi' |
    mi'2~ |
    mi'8 do'16. do'32 do'8 }
>> r8 |
R2*3 |
r4 r8 \twoVoices#'(corno1 corno2 corni) <<
  { mi''8 |
    mi''8. mi''16 mi''8. mi''16 |
    mi''2\fermata | }
  { mi'8 |
    mi'8. mi'16 mi'8. mi'16 |
    mi'2\fermata | }
>>
