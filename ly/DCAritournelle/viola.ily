\clef "alto" <mi' dod''>16-\sug\ff q q q q4:16 |
q2:16 |
q2:16 |
<mi' la'>16 <mi' dod'> q q q8 la'16 la' |
sold'8:16[ fad':16 mi':16 mi'':16] |
dod''16 dod'' si' si' la'8 la'16 la' |
sold'8:16[ fad':16 mi':16 re':16] |
dod':16[ si:16 la:16 sold:16] |
fad8 la'16. la'32 la'8 la' |
<sold' si'>4 r |
<mi' dod''>16 q q q q4:16 |
q2:16 |
q:16 |
q16 <mi' dod'> q q q8 la\noBeam |
do'4-\sug\sf( si8) la |
la( sold) sold mi'( |
red'-\sug\sf[ do' si la]) |
la4( sold8) mi |
<si sold'>2:16-\sug\ff |
<la la'>8 dod''[ la' la'] |
sold'2 |
fad'8 fad16 fad la la dod' dod' |
fad'8:16[ dod':16 la:16 fad:16] |
dod':16[ sold!:16 mid:16 dod:16] |
fad:16[ la:16 dod':16 fad':16] |
dod'4. dod'8 |
dod'8. dod'16 dod'8. sold'16 |
sold'2\fermata |
