\clef "bass" la,16\ff la la la la4:16 |
la,16 la la la la4:16 |
la,16 la la la la4:16 |
la,16 la la la
\tag #'basso <>^"Violoncelli" _"Bassi"
\twoVoices #'(toto fagotti basso) <<
  { la8 la16 la |
    sold8:16[ fad:16 mi:16 mi':16] |
    dod'16 dod' si si la la la la |
    sold8:16[ fad:16 mi:16 re:16] |
    dod:16[ si,:16 la,:16 sold,:16] | }
  { la8 la |
    sold[ fad mi mi'] |
    dod'[ si la la] |
    sold[ fad mi re] |
    dod[ si, la, sold,] | }
>>
fad,8 fad16. fad32 fad8 fad | 
mi4 r |
la,16 la la la la4:16 |
la,16 la la la la4:16 |
la,16 la la la la4:16 |
la,16 la la la la8 <<
  \tag #'fagotti {
    la8\noBeam |
    do'4-\sug\sf( si8) la |
    la[( sold) sold mi']( |
    red'[-\sug\sf do' si la]) |
    la4 sold8 r |
    
  }
  \tag #'basso {
    r8 |
    red2\sf |
    mi\p |
    fa\sf |
    mi |
  }
>>
r8 mi[\ff sold mi] |
la[ dod' la fad] |
mid2 |
\twoVoices #'(toto fagotti basso) <<
  { fad8 fad16 fad la la dod' dod' |
    fad'8:16[ dod':16 la:16 fad:16] |
    dod':16[ sold!:16 mid:16 dod:16] |
    fad:16[ dod:16 la,:16 fad,:16] | }
  { fad8[ fad la dod'] |
    fad'[ dod' la fad] |
    dod'[ sold! mid dod] |
    fad[ dod la, fad,] | }
>>
dod4. dod8 |
dod8. dod16 dod8. dod16 |
dod2\fermata |
