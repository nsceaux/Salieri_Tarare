\clef "treble" \tag #'oboi <>^"unis"
la''4.-\sug\ff \grace si''8 la''16 sold''32 la'' |
mi''4. \grace fad''8 mi''16 red''32 mi'' |
dod''4. \grace re''8 dod''16 si'32 dod'' |
la'4. la'8 |
sold'[( fad' mi') mi''-!] |
dod''[( si' la') la''-!] |
sold''[-! fad''-! mi''-! re''-!] |
dod''[ si' la' sold'] |
fad'8. red''16 red''8.\trill dod''32 red'' |
mi''8 mi''16. mi''32 mi''8 mi'' |
la''4. \grace si''8 la''16 sold''32 la'' |
mi''4. \grace fad''8 mi''16 red''32 mi'' |
dod''4. \grace re''8 dod''16 si'32 dod'' |
la'4. r8 |
R2*3 |
r8 <>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''16 mi'' mi''8 mi'' |
    mi''2 |
    mi''8 dod''16. dod''32 dod''8 dod'' | }
  { mi'16 mi' mi'8 mi' |
    mi'2 |
    mi'8 dod''16. dod''32 dod''8 dod'' | }
>>
\tag #'oboi <>^"unis" dod''2~ |
dod''8 la'16. la'32 la'8 dod'' |
fad''[ dod'' la' fad'] |
dod''[ sold'! mid' dod'] |
fad'[ la' dod'' fad''] |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''4. mid''8 |
    mid''8. sold''16 sold''8. dod'''16 |
    dod'''2\fermata | }
  { dod''4. dod''8 |
    dod''8. mid''16 mid''8. mid''16 |
    mid''2\fermata | }
>>
