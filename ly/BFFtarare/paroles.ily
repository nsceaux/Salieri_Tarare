- mir !

Sei -- gneur, si j’ai sau -- vé ta vi -- e,
si tu dai -- gnes t’en sou -- ve -- nir,
lais -- se- moi ven -- ger As -- ta -- si -- e
du traî -- tre qui l’o -- sa __ ra -- vir.
Per -- mets que, dé -- ploy -- ant ses ai -- les,
un lé -- ger vais -- seau de trans -- port
me mè -- ne vers ces in -- fi -- dè -- les,
cher -- cher As -- ta -- si -- e, As -- ta -- sie, ou la mort,
As -- ta -- si -- e, ou la mort.
