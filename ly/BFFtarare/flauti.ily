\clef "treble" R2. |
\twoVoices#'(flauto1 flauto2 flauti) <<
  { la''2. |
    si'' |
    la''~ |
    la''2~ la''8. sol''16 |
    fad''4 }
  { fad''2. |
    sol'' |
    fad''2 re''8 red'' |
    mi''2. |
    la'4 }
>> r4 r |
R2.*3 |
<>-\sug\f \twoVoices#'(flauto1 flauto2 flauti) <<
  { la''2 sold''4 | la'' }
  { dod''2 si'4 | dod'' }
>> r4 r2 |
<>-\sug\fp \twoVoices#'(flauto1 flauto2 flauti) <<
  { dod'''1 | dod'''2 }
  { lad''1 | lad''2 }
>> r4 |
R2.*2 |
r8 \twoVoices#'(flauto1 flauto2 flauti) <<
  { mi''4 mi''8 fad''8. red''16 |
    mi''2 mi''8.-\sug\f mi''16 |
    sol''2 dod''4 |
    re''2 la''8. fad''16 |
    si''2 dod'''4 |
    re''' }
  { si'4 si'8 do''4 |
    si'2. |
    si'2 sol'4 |
    fad'2 fad''4 |
    sol''8 re'' si' sol'' mi''8. mi''16 |
    re''4 }
  { s4.-\sug\p s4-\sug\sf | s2. | s-\sug\f | s-\sug\p-\sug\cresc | s-\sug\f }
>> r4 r |
