\clef "treble" re''8\f fad'([ la' re'' fad'' la'']) |
<<
  \tag #'violino1 {
    re'''8\p la'' r la'' r la'' |
    r si'' r si'' r si'' |
    r la'' r la'' la'' la'' |
    la''2~ la''8. sol''16 |
    r8 fad'' r fad''
  }
  \tag #'violino2 {
    fad''8\p fad'' r fad' r fad' |
    r sol' r sol' r sol' |
    r fad' r fad' re''( red'') |
    mi''8 <mi'' la'>4 q q8 |
    r la' r la'
  }
>> la'8.\f la'16 |
<<
  \tag #'violino1 {
    re''8. mi''16 fad''4 re''8.\p si'16 |
    sold'8. <sold' si>16
  }
  \tag #'violino2 {
    la'8. la'16 la'4 fad'8.\p re'16 |
    si8. <si sold'>16
  }
>> <si sold'>8.\f <mi' si'>16 q8. q16 |
<mi' dod''>8 la32( si dod' si) la8. <<
  \tag #'violino1 {
    dod''16 re''8. fad''16 |
    mi''32\f
  }
  \tag #'violino2 {
    la'16 la'8. la'16 |
    dod''32-\sug\f
  }
>> <dod'' la''>32 q q q q q q q4:32 <si' sold''>4:32 |
<<
  \tag #'violino1 {
    <dod'' la''>32\fp dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod'''4:32 dod'''2:32 |
    dod''':32\fp dod''':32 |
    dod''' r4 |
    <re''' re''>16.\fp re'32 re'16. re'32 re'16. re'32 re'16. re'32 re'16. re'32 re'16. re'32 |
    re'16.\f re'32 re'16. re'32 re'16. re'32 re'16. re'32 re'16. re'32 re'16. re'32 |
    mi'8 mi''[\p mi'' mi''] fad''8.(\sf red''16) |
    mi''8-! mi'( sol'\sf si') mi''8.\f mi''16 |
    sol''2\f <la sol'>8. q16 |
    fad'16\p re'8\cresc fad' la' re'' fad'' la''16 |
    <si' si''>16\f q8 q q q16 <mi' dod''>8. q16 |
    <la fad' re''>4
  }
  \tag #'violino2 {
    <dod'' la''>32-\sug\fp mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi''4:32 mi''2:32 |
    lad'':32-\sug\fp lad'':32 |
    lad''2 r4 |
    si'16.-\sug\fp si32 si16. si32 si16. si32 si16. si32 do'16. do'32 do'16. do'32 |
    si16.-\sug\f si32 si16. si32 si16. si32 si16. si32 si16. si32 si16. si32 |
    si8 si'[-\sug\p si' si'] do''4( |
    si'8) si'4 si' si'8 |
    si'2\f <mi' dod''>8. q16 |
    <fad' re''>16-\sug\p <la fad'>8-\sug\cresc q q q q q16 |
    sol'16-\sug\f <mi' re''>8 q q q16 <mi' dod''>8. q16 |
    <fad' re''>4
  }
>> r4 r |
