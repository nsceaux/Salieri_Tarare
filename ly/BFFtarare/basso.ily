\clef "bass" re4\f r r |
re8\p r re r re r |
re r re r re r |
re r re r fad r |
dod r dod r dod r |
re4 re r |
fad8.\f mi16 re4 re\p |
mi8. mi16 mi8.\f sold16 sold8. sold16 |
la4~ la8. la16 fad8. re16 |
mi16\f mi mi mi mi4:16 mi:16 |
la,32\fp la la la la la la la la4:32 la2:32 |
fad:32\fp fad:32 |
fad2 r4 |
si,8\fp si, si, si,\cresc fad fad |
sol2.:8\f |
sol4 \clef "tenor" sol'-\sug\p( la')\sf |
sol'2 \clef "bass" sol8.\f sol16 |
mi2\f la8. la16 |
re8\p re\cresc re re re re |
sol\f sol sol sol la8. la16 |
re4 r r |
