\clef "alto" fad'8-\sug\f fad([ la re' fad' la']) |
<la fad'>8-\sug\p q4 q q8 |
<si sol'> q4 q q8 |
r <la fad'>4 q q8 |
<la mi'>8 q4 q q8 |
<la fad'>8 q4 q8 r4 |
fad'8.-\sug\f mi'16 re'2 |
mi'8.-\sug\p mi'16 mi'8.-\sug\f sold16 sold8. sold16 |
la8 la32( si dod' si) la8. mi'16 re'8. re'16 |
mi'2.:32-\sug\f |
la32-\sug\fp la' la' la' la' la' la' la' la'4:32 la'2:32 |
fad':32-\sug\fp fad':32 |
fad'2 r4 |
fad'16.-\sug\fp fad32 fad16. fad32 fad16. fad32 fad16. fad32 la16. la32 la16. la32 |
sol16.-\sug\f sol32 sol16. sol32 sol16. sol32 sol16. sol32 sol16. sol32 sol16. sol32 |
sol4 sol'-\sug\p la'-\sug\sf |
sol'2 sol'4-\sug\f |
mi'2-\sug\f mi'8. mi'16 |
re'16-\sug\p re'8-\sug\cresc re' re' re' re' re'16 |
re'-\sug\f sol'8 sol' sol' sol'16 la'8. la16 |
re'4 r r |
