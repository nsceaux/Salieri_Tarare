\piecePartSpecs
#`((violino1 #:indent 0)
   (violino2 #:indent 0)
   (viola #:indent 0)
   (basso #:indent 0)
   (flauti #:score-template "score-flauti")
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#}))
