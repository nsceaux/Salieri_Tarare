\key re \major
\tempo "Andante" \midiTempo#80
\time 3/4 s2.*10
\time 4/4 s1*2
\time 3/4 s2.
\tempo "Andante" s2.*8 \bar "|."
