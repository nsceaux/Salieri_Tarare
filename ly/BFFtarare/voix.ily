\clef "bass" << re2 { s4. \once\override TextScript.self-alignment-X = #RIGHT s8^"Parlé" } >>
\ffclef "tenor/G_8" <>^\markup\character-text Tarare ardemment
r8 la |
re'2 r8 re' |
si4. re'8 \grace mi' re' dod'16[ si] |
\grace si8 la4 la re'8 red' |
mi'4. \grace re'8 dod'16 si la8. sol16 |
fad4 r la8. la16 |
re'8. mi'16 fad'4 re'8. si16 |
sold4 sold r8 si |
dod'4. dod'8 re'8. fad'16 |
mi'2~ mi'8. sold16 |
la4 r8 la dod'8. dod'16 dod' dod' re' mi' |
lad8 lad r fad16 sold lad8. si16 dod'8 dod'16 re' |
mi'2 r8 fad |
re' re'4 mi'16 re' do'8. do'16 |
si4 si r8 si |
mi'2 fad'8. red'16 |
mi'4 mi' mi'8. mi'16 |
sol'2 dod'8. dod'16 |
re'2 la'8 fad' |
mi'4 mi' la'8. la'16 |
re'2 r4 |
