\clef "alto" re'8.-\sug\ff la32 si64 dod' re'8. re'64 mi' fad' sol' |
la'4~ la'16 la la la |
<la fad'>4_\markup\tiny { \dynamic f \italic\bold sempre } r |
<la fad'>8 r <la mi'> r |
re'[ r16 re'] la8[ r16 la] |
re'4 r |
si r |
<mi' si'>8 r q r |
la'8[ r16 re'] la8[ r16 re'] |
la4 r |
<fad' dod''>8 r r4 |
si8 r lad r |
si8[ r16 si'] si8[ r16 si'] |
si8[ r16 si] sol!8[ r16 sol] |
la4 r |
fad'8 r dod' r |
re'8[ r16 re'] la8[ r16 la] |
re'8[ r16 re'] la'8[ r16 la] |
re'8. re'16 re'8 re' |
re'2 |
