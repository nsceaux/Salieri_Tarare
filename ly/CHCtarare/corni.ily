\clef "treble" \transposition re
<>-\sug\ff \twoVoices#'(corno1 corno2 corni) <<
  { do'4 do'8. mi'16 | sol'4 }
  { do'4 do'8. mi'16 | sol'4 }
>> r4 |
r16 <>-\sug\f \twoVoices#'(corno1 corno2 corni) <<
  { sol'32 sol' sol'16 sol' sol'4~ | sol'2~ | sol'8 }
  { sol'32 sol' sol'16 sol' sol'4~ | sol'2~ | sol'8 }
  { s8. s4 s2-\sug\p }
>> r8 r4 |
R2 |
r16 <>\f \twoVoices#'(corno1 corno2 corni) <<
  { re''32 re'' re''16 re'' re''4~ | re''2~ | re''8 }
  { re''32 re'' re''16 re'' re''4~ | re''2~ | re''8 }
  { s8. s4 s2\p }
>> r8 r4 |
R2 |
r16 <>\f \twoVoices#'(corno1 corno2 corni) <<
  { mi''32 mi'' mi''16 mi'' mi''4~ | mi''2~ | mi''8 }
  { mi'32 mi' mi'16 mi' mi'4~ | mi'2~ | mi'8 }
  { s8. s4 s2\p }
>> r8 r4 |
R2 |
r16 <>\f \twoVoices#'(corno1 corno2 corni) <<
  { sol''32 sol'' sol''16 sol'' sol''4~ | sol''2~ | sol''8 }
  { sol'32 sol' sol'16 sol' sol'4~ | sol'2~ | sol'8 }
  { s8. s4 s2\p }
>> r8 r4 |
r8 r16 <>\f \twoVoices#'(corno1 corno2 corni) <<
  { mi''16 re''8. re''16 |
    mi''8. do'16 do'8 do' |
    do'2 | }
  { do''16 sol'8. sol'16 |
    do''8. do'16 do'8 do' |
    do'2 | }
>>
