\clef "bass" re4-\sug\ff re8. re16 |
la,4 r |
R2*2 |
r8. <>-\sug\f re16 la,8. la,16 |
re4 r |
R2*2 |
r8. <>-\sug\f re16 la,8. la,16 |
la,4 r |
R2*2 |
r8. <>-\sug\ff re16 re8. re16 |
re4 r |
R2*2 |
r8. <>-\sug\ff re16 la,8. la,16 |
re8. re16 la,8. la,16 |
re8. re16 re8 re |
re2 |
