\clef "treble" \transposition re
<>-\sug\ff \twoVoices#'(tromba1 tromba2 trombe) <<
  { do'4 do'8. mi'16 | sol'4 }
  { do'4 do'8. mi'16 | sol'4 }
>> r4 |
R2*2 |
r8. <>-\sug\f \twoVoices#'(tromba1 tromba2 trombe) <<
  { mi''16 re''8. re''16 | mi''4 }
  { do''16 sol'8. sol'16 | do''4 }
>> r4 |
R2*2 |
r8. <>-\sug\f \twoVoices#'(tromba1 tromba2 trombe) <<
  { mi''16 re''8. mi''16 | re''4 }
  { do''16 sol'8. do''16 | sol'4 }
>> r4 |
R2*2 |
r8. <>-\sug\ff \twoVoices#'(tromba1 tromba2 trombe) <<
  { mi''16 do''8. mi''16 | do''4 }
  { do''16 do''8. do''16 | mi'4 }
>> r4 |
R2*2 |
r8. <>-\sug\ff \twoVoices#'(tromba1 tromba2 trombe) <<
  { mi''16 re''8. re''16 |
    mi''8. mi''16 re''8. re''16 |
    do''8. do'16 do'8 do' |
    do'2 | }
  { do''16 sol'8. sol'16 |
    do''8. do''16 sol'8. sol'16 |
    mi'8. do'16 do'8 do' |
    do'2 | }
>>
