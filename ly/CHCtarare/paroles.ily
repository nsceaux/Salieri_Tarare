\tag #'(tarare basse) {
  Qui veut la gloi -- re,
  à la vic -- toi -- re
  vole a -- vec moi.
}
\tag #'(vhaute-contre vtaille vbasse basse) {
  C’est moi, c’est moi.
}
\tag #'(tarare basse) {
  Su -- jets, es -- cla -- ves,
  que les plus bra -- ves
  don -- nent leur foi.
}
\tag #'(vhaute-contre vtaille vbasse basse) {
  C’est moi, c’est moi.
}
\tag #'(tarare basse) {
  Ni paix, ni trê -- ve,
  l’hor -- reur du glai -- ve
  fe -- ra la loi.
}
\tag #'(vhaute-contre vtaille vbasse basse) {
  C’est moi, c’est moi.
}
\tag #'(tarare basse) {
  Qui veut la gloi -- re,
  à la vic -- toi -- re
  vole a -- vec moi.
}
\tag #'(vhaute-contre vtaille vbasse basse) {
  C’est moi, c’est moi, c’est moi, c’est moi. __
}
