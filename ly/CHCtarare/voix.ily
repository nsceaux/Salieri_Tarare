<<
  \tag #'(vhaute-contre basse) {
    <<
      \tag #'basse { s2*4 s8. \ffclef "alto/G_8" }
      \tag #'vhaute-contre { \clef "alto/G_8" R2*4 | r8 r16 }
    >> fad'16 mi'8. la'16 |
    <<
      \tag #'basse { fad'8. s16 s4 | s2*2 | s8. \ffclef "alto/G_8" }
      \tag #'vhaute-contre { fad'4 r4 | R2*2 | r8 r16 }
    >> fad'16 mi'8. fad'16 |
    <<
      \tag #'basse { mi'8. s16 s4 | s2*2 | s8. \ffclef "alto/G_8" }
      \tag #'vhaute-contre { mi'4 r | R2*2 | r8 r16 }
    >> fad'16 re'8. si'16 |
    fad'4 <<
      \tag #'basse { s4 | s2*2 | s8. \ffclef "alto/G_8" }
      \tag #'vhaute-contre { r4 | R2*2 | r8 r16 }
    >> fad'16 mi'8. la'16 |
    fad'8. fad'16 mi'8. la'16 |
    la'2~ |
    la'4 r |
  }
  \tag #'vtaille {
    \clef "tenor/G_8" R2*4 |
    r8 r16 re' dod'8. dod'16 |
    re'4 r |
    R2*2 |
    r8 r16 re' dod'8. re'16 |
    dod'4 r |
    R2*2 |
    r8 r16 re' si8. fad'16 |
    re'4 r |
    R2*2 |
    r8 r16 re' dod'8. dod'16 |
    re'8. re'16 dod'8. dod'16 |
    fad'2~ |
    fad'4 r |
  }
  \tag #'vbasse {
    \clef "bass" R2*4 |
    r8 r16 re' la8. la16 |
    re'4 r |
    R2*2 |
    r8 r16 re' la8. re'16 |
    la4 r |
    R2*2 |
    r8 r16 re' si8. re'16 |
    si4 r |
    R2*2 |
    r8 r16 re' la8. la16 |
    re'8. re'16 la8. la16 |
    re'2~ |
    re'4 r |
  }
  \tag #'(tarare basse) {
    \clef "tenor/G_8" <>^\markup\italic { au peuple avec feu } _"chanté"
    R2 |
    r4 r16 la la la |
    re'8 la r16 la la la |
    re'8 re' mi' mi'16 mi' |
    <<
      \tag #'basse { fad'8. s16 s4 | s8. \ffclef "tenor/G_8" }
      \tag #'tarare { fad'4 r | r8 r16 }
    >> la16 re'8. fad'16 |
    sold8 sold r16 si si si |
    mi'8 mi' mi' mi'16 mi' |
    <<
      \tag #'basse { dod'8. s16 s4 | s8. \ffclef "tenor/G_8" }
      \tag #'tarare { dod'4 r | r8 r16 }
    >> dod'16 mi'8. dod'16 |
    fad'8 fad r16 fad' fad' fad' |
    re'8 re'16 re' dod'8 fad' |
    <<
      \tag #'basse { si8. s16 s4 | s \ffclef "tenor/G_8" }
      \tag #'tarare { si4 r | r }
    >> r16 sol'16 mi' re' |
    dod'8 dod' r16 la la la |
    re'8 re' mi' mi'16 mi' |
    <<
      \tag #'basse { fad'8. }
      \tag #'tarare { fad'4 r | R2*3 | }
    >>
  }
>>