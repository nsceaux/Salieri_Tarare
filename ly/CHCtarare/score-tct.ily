\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Trompettes \small en Ré }
    } <<
      \keepWithTag #'() \global
      \keepWithTag #'trombe \includeNotes "trombe"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Cors \small en Ré }
    } <<
      \keepWithTag #'() \global
      \keepWithTag #'corni \includeNotes "corni"
    >>
    \new Staff \with { instrumentName = "Timbales" } <<
      \global \includeNotes "timpani"
    >>
  >>
  \layout { indent = \largeindent }
}
