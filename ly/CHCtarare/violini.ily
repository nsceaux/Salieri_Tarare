\clef "treble" re'8.\ff la32 si64 dod' re'8. re'64 mi' fad' sol' |
la'4~ la'16 la' la' la' |
\tuplet 6/4 { re''16\f la' la' la' la' la' } \tuplet 6/4 { la'4.:16 } |
\tuplet 6/4 { re''16\fp la' la' la' la' la' } \tuplet 6/4 { mi''\fp la' la' la' la' la' } |
<<
  \tag #'violino1 {
    fad''8[\f r16 <fad'' la'>] <la' mi''>8[ r16 <la'' la'>] |
    <fad'' la'>16*2/3\fp la' la' la' la' la'
  }
  \tag #'violino2 {
    re''8-\sug\f[ r16 re''] dod''8[ r16 dod''] |
    re''16*2/3-\sug\fp la' la' la' la' la'
  }
>> la'4.*2/3:16 |
sold'16*2/3\fp mi'' mi' mi' mi' mi' mi' mi' mi' mi' mi' mi' |
mi''\fp mi' mi' mi' mi' mi' mi''\fp mi' mi' mi' mi' mi' |
<<
  \tag #'violino1 {
    dod''8[\f r16 <la' fad''>] <mi'' la'>8[ r16 <la' fad''>] |
    mi''16*2/3\fp la'' la'' la'' la'' la''
  }
  \tag #'violino2 {
    dod''8-\sug\f[ r16 re''] dod''8[ r16 re''] |
    dod''16*2/3-\sug\fp la'' la'' la'' la'' la''
  }
>> la''16*2/3 dod'' dod'' dod'' dod'' dod'' |
fad''\fp fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' fad' |
re''\fp fad' fad' fad' fad' fad' dod'' fad' fad' fad' fad' fad' |
<<
  \tag #'violino1 {
    re''8[\ff r16 fad''] re''8[ r16 si''] |
    fad''8[ r16 fad''] sol''16. sol''32 mi''16. re''32 |
  }
  \tag #'violino2 {
    si'8-\sug\ff[ r16 re''] si'8[ r16 <re'' fad''>] |
    q8[ r16 re''] si'16. sol''32 mi''16. re''32 |
  }
>>
dod''16*2/3\fp la' la' la' la' la' la' la' la' la' la' la' |
re''\fp la' la' la' la' la' mi''\fp la' la' la' la' la' |
fad''8\ff[ r16 <re'' fad''>] <mi'' dod''>8[ r16 <dod'' la''>] |
<re'' fad''>8[ r16 q] <dod'' mi''>8[ r16 <mi'' dod'''>] |
<re''' fad''>8. re'16 re'8 re' |
re'2 |
