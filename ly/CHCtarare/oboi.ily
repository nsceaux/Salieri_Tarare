\clef "treble"
<>-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { re' re'8. fad'16 | la'4 }
  { re' re'8. fad'16 | la'4 }
>> r4 |
R2*2 |
r8. <>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { fad''16 mi''8. la''16 | fad''4 }
  { re''16 dod''8. dod''16 | re''4 }
>> r4 |
R2*2 |
r8. <>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { fad''16 mi''8. fad''16 | mi''4 }
  { re''16 dod''8. re''16 | dod''4 }
>> r4 |
R2*2 |
r8. <>-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { fad''16 re''8. si''16 | fad''4 }
  { re''16 si'8. re''16 | re''4 }
>> r4 |
R2*2 |
r8. <>-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { fad''16 mi''8. la''16 |
    fad''8. fad''16 mi''8. dod'''16 |
    re'''8. re'16 re'8 re' |
    re'2 | }
  { re''16 dod''8. dod''16 |
    re''8. re''16 dod''8. mi''16 |
    fad''8. re'16 re'8 re' |
    re'2 | }
>>
