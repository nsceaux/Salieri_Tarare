\clef "bass" re8.\ff la,32 si,64 dod re8. re64 mi fad sol |
la4 r |
re_\markup { \dynamic f \italic\bold sempre } r4 |
re8 r dod r |
re8[ r16 re] la,8[ r16 la,] |
re4 r |
mi r |
sold8 r sold r |
la[ r16 re] la8[ r16 re] |
la,4 r |
lad, r |
si,8 r lad, r |
si,[ r16 si] si,8[ r16 si] |
si,8[ r16 si] sol!8[ r16 sol] |
la4 r |
fad8 r dod r |
re8[ r16 re] la8[ r16 la,] |
re8[ r16 re] la8[ r16 la,] |
re4 re8 re |
re4 r |
