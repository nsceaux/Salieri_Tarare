\piecePartSpecs
#`((violino1)
   (violino2 #:system-count 2)
   (viola #:system-count 2)
   (basso #:system-count 2)
   (clarinetti #:score "score-clarinetti")
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #})
   (silence #:system-count 2))
