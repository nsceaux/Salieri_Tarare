\clef "treble"
<<
  \tag #'violino1 {
    mi''\fp mi''8. mi''16 |
    do''4. do''8 dod''( dod'' dod'' dod'') |
    re''( mi'' fa'' re'') si'4. do''8 |
    re''8.( mi''16 fa''8. sol''16) mi''4. do''8 |
    fad''8 fad''4 fad''8 sol''( la''! fa''! re'') |
    do''( si'4 do''8) re''( fa'' la' si') |
    do''4 la'8.(\mf sol'16) sol'4( fa' |
    mi'4.) mi'8 re'4. mi'16 re' |
    do'8. sol16 do'8. sol16 do'2\fermata |
  }
  \tag #'violino2 {
    si'2-\sug\fp |
    mi'4. mi'8 sib'8( sib' sib' sib') |
    la'( sol' fa' la') re'4. sol'8 |
    sol'2~ sol'4. mi'8 |
    do''8( mib'' re'' do'') si' la'!4 la'8 |
    sol'2 la8( la') fa'8. fa'16 |
    mi'4 fa'8.(-\sug\mf mi'16) mi'4 la8( si) |
    do'4. do'8 do'4 si |
    do' sol sol2\fermata |
  }
>>
