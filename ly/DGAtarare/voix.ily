\clef "tenor/G_8" mi'4 mi'8. mi'16 |
do'4. do'8 dod' dod' dod' dod' |
re' mi' fa' re' si4. do'8 |
re'8. mi'16 fa'8 sol' mi'4 r8 do' |
fad'8 fad' fad' fad' sol' la'! sol'16[ fa'?] mi'[ re'] |
si4. do'8 re' fa' la si |
do'4 \tag #'recit <>^\markup\italic {
  (Il remet son masque et suit de loin l’empereur.)
} r4 r2 |
R1*2 |
