\score {
  \new GrandStaff <<
    \new Staff <<
      \global \keepWithTag #'violino1 \includeNotes "violini"
    >>
    \new Staff <<
      \global \keepWithTag #'violino2 \includeNotes "violini"
    >>
  >>
  \layout { }
}
