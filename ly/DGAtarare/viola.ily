\clef "alto" mi'2-\sug\fp |
la'4. la'8 mi'( mi' mi' mi') |
re'( dod' re' re') sol'4. sol'8 |
sol'4 re' sol4. do'8 |
do'( mib' re' do') si la!4 fa'8 |
re'4. do'8 fa4. sol8 |
do2~ do |
do8 mi'( do' la) fa fad sol sol |
mi4 mi mi2\fermata |
