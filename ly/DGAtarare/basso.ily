\clef "bass" sold4\fp sold8. sold16 |
la4. la8 sol! sol sol sol |
fa( mi re fa) sol4. mi8 |
si,2( do4) r8 do' |
la2 sol8 fa!4 fa8~ |
fa4. mi8 fa4. sol8 |
do1~ |
do8 mi( do la, fa, fad, sol, sol,) |
do,1\fermata |
