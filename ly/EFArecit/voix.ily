<<
  \tag #'(recit basse) {
    \clef "soprano/treble" <>^\markup\character-text Spinette à part, voilée
    r2 la'4 la'8 la' |
    fad'4 r \tag #'recit <>^\markup\italic { elle l’examine } r2 |
    R1 |
    r4 r8 la'16 la' re''4 la'16 si' do'' re'' |
    si'4 \tag #'recit <>^\markup\italic { Tarare se met à genoux à six pas d’elle. } r4 r2 |
    R1 |
    r8 si' si' do'' re'' re'' r4R1*2 |
    r4 r8 <>^\markup\italic Fièrement la'16 la' la'4 la'8 re'' |
    re'' la'16 la' la'8 sib' do'' do'' do'' re'' |
    sib'4 \tag #'recit <>^\markup\italic Ton de dignité r4 r r8 fa' |
    sib'2 r |
    do''4 do''8 do'' do''4. fa''8 |
    re''4 sib' r r8 sib' |
    mib''4 mib''8 re'' do''4 do''8 sib' |
    la'2 r8 la' la' sib' |
    do''4. do''8 do''4. fa''8 |
    re''4 re'' r mi''8 mi'' |
    fa''4 re''8 re'' do''4 do'' |
    fa'4
    \ffclef "tenor/G_8"
    \tag #'recit <>^\markup\character-text Tarare à part, se relevant. Ton concentré
    \tag #'basse <>^\markup\character Tarare
    r8 fa do' do'16 do' do'8 sib!16 do' |
    la8 la r do'16 mib'! re'8. la16 do'8 do'16 re' |
    sib!4 r8 re' sib sib16 sib sib8 sib16 sol |
    dod'4 r8 mi'16 mi' dod'4 dod'16 dod' dod' re' |

    la8 la
    \ffclef "soprano/treble" <>^\markup\character-text Spinette à part avec gaité
    r8 la'16 la' re''4 re''16 re'' re'' fa'' |
    re''4 re''8 mi''!16 fa'' mi''8. si'!16 re''8 do'' |
    la'8 la' <>^\markup\italic { elle se dévoile, Tarare la regarde } r4 r r8 do'' |
    fa''2 r8 do'' do''8. do''16 |
    la'2 r4 do''8 fa'' |
    mi''!2 sol''4. sib'8 |
    la'2 r4 la'8. sib'16 |
    do''4. mib''8 re''4 re''8 re'' |
    sib'4 sib' r8 sol' sol'8. la'16 |
    sib'4 sib'8. sib'16 sib'4 do''8 re'' |
    do''2 r8 do'' do''8. do''16 |
    la'8. do''16 do''8. do''16 do''4. fa''8 |
    re''2 r4 re''8. mib''16 |
    fa''2 lab'4. sib'8 |
    sol'2 r4 mib''8 re'' |
    do''2 mib''4. sol''8 |
    sib'2.( do''4) |
    sib'2 r4 r8 fa'16 fa' |
    sib'8 sib'
    \ffclef "tenor/G_8" <>^\markup\character Tarare
    r8 sib16 sib re'4 do'8 re' |
    sib sib r16 sib sib do' lab8 lab lab sib |
    sol4
    \ffclef "soprano/treble" <>^\markup\character-text Spinette sévèrement
    r8 sib' mib''4 sib'8 do'' |
    reb''4 reb''8 mib'' do''4 r16 mib'' mib'' mib'' |
    do''4. do''16 mib'' lab'8 lab'
    \ffclef "tenor/G_8" <>^\markup\character-text Tarare à ses pieds
    r8 lab |
    do' do' r do' reb'4. reb'8 |
    reb'4 sol8 lab sib4 sib8 do' |
    lab8 lab
    \ffclef "soprano/treble" <>^\markup\character-text Spinette d’un ton plus doux
    r8 do''16 re'' si'!4 r8 sol'16 la' |
    si'8. si'16 si' si' si' do'' re''4
    \ffclef "tenor/G_8" <>^\markup\character-text Tarare timidement
    re'4 |
    re'8 re' re' mib' fa'4 si8 do' |
    re'4 re'8 mib' do'4\fermata
  }
  \tag #'recit2 {
    \clef "tenor/G_8" R1*40 |
    r2 r4 <>^\markup\character-text Tarare s’écrie do'8. do'16 |
    re'4 re' r2 |
  }
>>