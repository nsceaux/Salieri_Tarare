\clef "bass" R1 |
r2 re4\p fad |
mi dod la, dod |
re r r2 |
r2 sol4 si |
la fad re fad |
sol r r2 |
sol4 sib sol sib |
sol sib sol sib |
fad r r2 |
R1 |
sol4. sol8\f la4 la |
sib4.\p sib,8 sib,4 sib, |
mib4 mib8. mib16 fa4 fa, |
sib,2 r |
sol4-\sug\p sol8. fa16 mib4 mib8. mi16 |
fa4 fa8.-\sug\f fa16 fa4 r |
la,4\p r la, r |
sib, r r sib |
la sib do' do |
fa r r2 |
r fad4 r |
sol4 r r2 |
sol4 r r2 |
fa4 r r2 |
r sold4 r |
r la\f la sol\p |
fa r fa r |
fa r fa r |
do r do r |
fa, r fa, r |
fa r fad r |
sol r sol\mf r |
sol\mf r sol\mf r |
la\mf r la\mf r |
fa\mf r la,\mf r |
sib,\mf r sib,\mf r |
re\p r re r |
mib mib mib mib |
mib mib mib mib |
fa2\fp fa8*2/3 fa fa fa,[-\sug\f fa, fa,] |
sib,2 r |
R1*2 |
r8. mib16\p mib4 r2 |
r lab,4 r |
R1 |
r2 mi!\p~ |
mi mi |
fa4 r sol r |
R1 |
r2 si,!~ |
si, do4
