\key do \major
\tempo "Allegro Agitato" \midiTempo#100
\time 4/4 s1*11 \bar "||"
\time 2/2 \key sib \major \tempo "Andante Maestoso" s1*15 \bar "||"
\key do \major \time 2/2 s1*26 s2. \bar "|."
