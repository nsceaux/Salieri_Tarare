\clef "treble" R1 |
r2 re'8\p re' r re' |
\grace mi' re' dod' r mi' sol' sol' r <<
  \tag #'violino1 { sol'8 | fad'4 }
  \tag #'violino2 { mi'8 | re'4 }
>> r4 r2 |
r2 sol'8 sol' r sol' |
\grace la' sol' fad' r la' do'' do'' r <<
  \tag #'violino1 { do''8 | si'4 }
  \tag #'violino2 { la'8 | sol'4 }
>> r4 r2 |
sib'8 re'' r re'' \grace fa'' mib''[ re''] \grace fa'' mib''[ re''] |
re'' re' r re' \grace fa' mib'[ re'] \grace fa' mib'[ re'] |
re'4 r r2 |
R1 |
<<
  \tag #'violino1 {
    sib'4 r8 sol'\f fa'!4 fa' |
    sib'4.\p sib'8 sib'4 sib' |
    do''4 do''8. do''16 do''4. fa''8 |
    re''4 sib' fa'8(\f sib' re'' fa'') |
    mib''4\p mib''8. re''16 do''4 do''8. sib'16 |
    la'4
  }
  \tag #'violino2 {
    re'4 r8 sib\f do'4 do' |
    re'4.\p re'8 re'4 sib' |
    sib' sib'8. sib'16 la'4. la'8 |
    sib'8 fa' re'4 re'8(-\sug\f fa' sib' re'') |
    sib'2-\sug\p sol' |
    do'4
  }
>> <fa' la>8.\f q16 q4 r |
fa'8*2/3-\p-. la'-. sib'-. do''-. sib'-. la'-. fa'' do'' sib' la' sol' fa' |
\omit TupletBracket
<<
  \tag #'violino1 {
    \tuplet 3/2 { r8 re'' mi'' } fa''8*2/3[ mi'' re''] fa''[ mi'' re''] mi'' fa'' sol'' |
    fa''[ la'' fa''] re'' fa'' re'' do''[ la' fa'] mi' sol' mi' |
    fa'4
  }
  \tag #'violino2 {
    \tuplet 3/2 { r8 re' mi' } fa'8*2/3[ mi' re'] fa'[ mi' re'] mi' fa' sol' |
    fa'[ la' fa'] re' fa' re' do'[ fa' la] sol sib sol |
    la4
  }
>> r4 r2 |
r2 <<
  \tag #'violino1 {
    re''4 r |
    re'' r r2 |
    dod''4 r r2 |
    re''4 r r2 |
    r mi''!4 r |
  }
  \tag #'violino2 {
    la'4 r |
    sib'!4 r r2 |
    mi'4 r r2 |
    la'4 r r2 |
    r si'!4 r |
  }
>>
r4 <>\f <<
  \tag #'violino1 {
    do''8. do''16 do''4 \grace do''8 sib'8\p la'16 sib' |
  }
  \tag #'violino2 {
    mi'!8. mi'16 mi'4 mi'\p |
  }
>>
la'8*2/3 la' sib' do'' sib' la' fa' la' sib' do'' sib' la' |
fa' la' sib' do'' sib' la' fa' la' sib' do'' sib' la' |
<<
  \tag #'violino1 {
    \tuplet 3/2 { r8 sol' la' } sib'8*2/3[ la' sol'] sib'[ la' sol'] sib' la' sol' |
    la'

  }
  \tag #'violino2 {
    \tuplet 3/2 { r8 mi'! fa' } sol'8*2/3[ fa' mi'] sol'[ fa' mi'] sol' fa' mi' |
    fa'
  }
>> do'' sib' la' sib' do'' fa'4 r |
la'8*2/3-! do''( re'' mib'' re'' do'') la''-! la'( sib' do'' sib' la') |
sib' sib' do'' re'' do'' sib' <<
  \tag #'violino1 {
    re'8*2/3_\markup\dynamic mfp sol' la' sib' la' sol' |
    re'_\markup\dynamic mfp sol' la' sib' la' sol' re'_\markup\dynamic mfp sol' la' sib' la' sol' |
    do''_\markup\dynamic\tiny mfp fa' sol' la' sol' fa' do'_\markup\dynamic mfp fa' sol' la' sol' fa' |
    fa''_\markup\dynamic mfp la' sib' do'' sib' la' fa'_\markup\dynamic mfp la' sib' do'' sib' la' |
    fa'_\markup\dynamic mfp sib' do'' re'' do'' sib' fa'_\markup\dynamic mfp sib' do'' re'' do'' sib' |
    fa'\p sib' do'' re'' do'' sib' fa'' fa' sol' lab' sol' fa' |
  }
  \tag #'violino2 {
    <re' sib>4-\sug\mf r4 |
    <sib re'>-\sug\mf r q-\sug\mf r |
    <fa' do'>-\sug\mf r q-\sug\mf r |
    <do' la'>-\sug\mf r <fa' do'>-\sug\mf r |
    <re' fa'>-\sug\mf r q-\sug\mf r |
    <sib fa'>1-\sug\p |
  }
>>
sol'8*2/3 sol' la' sib' la' sol' mib' <<
  \tag #'violino1 {
    sol''8*2/3 fa'' mib'' fa'' re'' |
    do'' do'' re'' mib'' re'' do'' sol' do'' re'' mib'' re'' do'' |
  }
  \tag #'violino2 {
    sol'8*2/3 sol' sol' sol' sol' |
    sol'2 sol'8*2/3 sol' fa' mib' fa' sol' |
  }
>>
fa'8*2/3\fp[ <re'' sib''> q] q[ q q] q[ q q] <do'' la''>4\f |
<re'' sib''>4 r r2 |
R1*2 |
r8. <<
  \tag #'violino1 { sol'16\p sol'4 r2 | r lab'4 r | }
  \tag #'violino2 { sib16-\sug\p sib4 r2 | r do'4 r | }
>>
R1 |
r2 <<
  \tag #'violino1 { reb''2\p~ | reb'' reb'' | do''4 }
  \tag #'violino2 { sib'2\p~ | sib' sib' | lab'4 }
>> r4 <re' si'!>4 r |
R1 |
r2 <<
  \tag #'violino1 { re''2~ | re'' do''4 }
  \tag #'violino2 { fa'2~ | fa' mib'4 }
>>
