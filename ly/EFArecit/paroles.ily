\tag #'(recit basse) {
  Comme il est laid !…
  Ce -- pen -- dant il n’est point mal fait.
  
  Il se pros -- ter -- ne ! il n’a point l’air fa -- rou -- che
  des au -- tres mons -- tres de ces lieux.
  Mu -- et, vo -- tre res -- pect me tou -- che ;
  je lis votre a -- mour dans vos yeux :
  un tendre a -- veu de vo -- tre bou -- che,
  ne pour -- rait me l’ex -- pri -- mer mieux.
  
  Grand dieux ! ce n’est point As -- ta -- si -- e,
  et mon cœur al -- lait s’ex -- ha -- ler !
  De m’être abs -- te -- nu de par -- ler,
  ô Bra -- ma ! je te re -- mer -- ci -- e.
  
  On croi -- rait qu’il se par -- le bas.
  Chaque a -- ni -- mal a son lan -- ga -- ge.
  
  De loin, je le veux bien, con -- tem -- plez mes ap -- pas.
  Je vou -- drais pou -- voir da -- van -- ta -- ge ;
  mais un mo -- narque, un ca -- life, un sul -- tan,
  le plus par -- fait, com -- me le plus puis -- sant,
  ne peut rien sur mon cœur, il est tout à Ta -- ra -- re.
}
\tag #'recit2 {
  À Ta -- ra -- re !…
}
\tag #'(recit basse) {
  Il me par -- le !
  
  Ô trans -- port qui m’é -- ga -- re !
  É -- ton -- ne -- ment trop in -- dis -- cret !
  
  Un mot a tra -- hi ton se -- cret !
  Tu n’es pas muet ? té -- mé -- rai -- re !
    
  Ma -- da -- me, hé -- las ! cal -- mez u -- ne jus -- te co -- lè -- re !
  
  Im -- pru -- dent ! quel es -- poir a pu te faire o -- ser…
  
  Ah ! c’est en m’ac -- cu -- sant, que je dois m’ex -- cu -- ser.
}
