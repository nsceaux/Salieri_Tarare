\clef "alto" R1 |
r2 fad4-\sug\p la |
sol mi dod la |
la r r2 |
r2 si4 re' |
do' la fad re' |
re' r r2 |
sol4 sib sol sib |
sol sib sol sib la r4 r2 |
R1 |
sol4 r8 sol-\sug\f la4 la |
sib4.-\sug\p fa'8 fa'4 fa' |
sol' sol'8. sol'16 fa'2 |
fa'4 r r2 |
sol'4-\sug\p sol'8. fa'16 mib'4 mib'8. mi'16 |
fa'4 do'8.-\sug\f do'16 do'4 r |
la-\sug\p r la r |
sib r r sib' |
la' sib' do'' do' |
fa' r r2 | \allowPageTurn
r fad'4 r |
sol'4 r r2 |
sol'4 r r2 |
fa'4 r r2 |
r sold'4 r |
r4 la8.-\sug\f la16 la4 sol-\sug\p |
fa' r fa' r |
fa' r fa' r |
do' r do' r |
fa r fa r |
fa' r fad' r |
sol' r sol'\mf r |
sol'\mf r sol'\mf r |
la'\mf r la'\mf r |
fa'\mf r la\mf r |
sib\mf r sib\mf r |
re'\p r re' r |
mib' mib' mib' mib' |
mib' mib' mib' mib' |
fa'2\fp fa'8*2/3 fa' fa' fa-\sug\f[ fa fa] |
sib2 r |
R1*2 | \allowPageTurn
r8. mib'16-\sug\p mib'4 r2 |
r lab4 r |
R1 |
r2 sol'-\sug\p~ |
sol' sol' |
do'4 r sol r |
R1 |
r2 sol'~ |
sol' sol'4
