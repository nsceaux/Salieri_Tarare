\clef "treble"
<<
  \tag #'violino1 {
    sol'8\mf |
    do''8. mi''16 re''8 do''4 mi''8 |
    sol''4 fa''8 mi''4 do'''8 |
    mi''4 la''8 sol''4 do''8 |
    re''4 mi''16 fa'' mi''4 r8 |
    \grace { fa''16[ mi'' re''] } mi''4 r8 \grace { la''16[ sol'' fa''] } sol''4 r8 |
    \grace { re''16[ do'' si'] } do''4 r8 \grace { fa''16[ mi'' re''] } mi''4 r8 |
    r4 do''8 re''4 mi''16 do'' |
    si'4. r8 r re''\mf |
    re''-! mi''-! fad''-! sol''-! la''-! si''-! |
    do'''\noBeam do'''16\f si'' la'' sol'' fad''4 re''8\mf |
    re'' fad'' sol'' la'' si'' do''' |
    si''\noBeam re'''16-\sug\f do''' si'' la'' sol''4 \grace { mi''16[ fad''] } sol''8\sf |
    fad''4\p( mi''8 re''4) \grace { sol''16[ la''] } si''8\sf |
    la''4(\p sol''8 fad''4) re'''8\f |
    re'''8( do'''16 si'' la'' sol'' sol''4 la''8) |
    la''4.( si''4) \grace { mi''16[ fad''] } sol''8\sf |
    fad''\p~ fad''16. sol''32 mi''16. fad''32 re''4 \grace { sol''16[ la''] } si''8\sf |
    la''8~ la''16. si''32 sol''16. la''32 fad''4 re'''8\f |
    re''' do'''16 si'' la'' sol'' sol''4 la''8 |
    sol''8. re''16 si'8 sol'4. |
  }
  \tag #'violino2 {
    r8 |
    mi'8.-\sug\mf sol'16 fa'8 mi'4 sol'8 |
    re'( sol') sol' sol'4 do''8 |
    do''4 do''8 do''4 sol'8 |
    sol' sol' sol' sol'4 r8 |
    \grace { la'16[ sol' fa'] } sol'4 r8 \grace { do''16[ si' la'] } si'4 r8 |
    \grace { fa'16[ mi' re'] } mi'4 r8 \grace { la'16[ sol' fa'] } sol'4 r8 |
    r4 mi'8 la'4 la'8 |
    re'4. r8 r si-\sug\mf |
    si-! do'-! la-! si-! la-! sol-! |
    la\noBeam la16-\sug\f si do' dod' re'4. |
    r8 re'-\sug\mf mi' fad' sol' la' |
    sol'\noBeam si'16-\sug\f la' sol' fad' sol'4 \grace { do'16[ re'] } mi'8-\sug\sf |
    re'4-\sug\p( do'8 si4) \grace { mi'16[ fad'] } sol'8-\sug\sf |
    fad'4-\sug\p( mi'8 re'4) re''8-\sug\f( |
    sol'4) do''8( si'4) fad''8 |
    fad''4.( sol''4) \grace { do'16[ re'] } mi'8-\sug\sf |
    re'4-\sug\p( do'8) si4 \grace { mi'16[ fad'] } sol'8-\sug\sf |
    fad'4( mi'8) re'4 re''8-\sug\f |
    sol'4 do''8 si' re'' fad' |
    sol'4. r4 r8 |
  }
>>
R2. |
<<
  \tag #'violino1 {
    \grace { sol''16[ fa'' mi''] } fa''4 r8 \grace { mi'''16[ re''' do'''] } re'''4 r8 |
  }
  \tag #'violino2 {
    \grace { si'16[ la' sol'] } la'4 r8 \grace { sol''16[ fa'' mi''] } fa''4 r8 |
  }
>>
R2. |
<<
  \tag #'violino1 {
    \grace { fa''16[ mi'' re''] } mi''4 r8 \grace { re'''16[ do''' si''] } do'''4 r8 |
  }
  \tag #'violino2 {
    \grace { la'16[ sol' fa'] } sol'4 r8 \grace { fa''16[ mi'' re''] } mi''4 r8 |
  }
>>
\grace { si''16[ la'' sol''] } la''4 r8 \grace { sol''16[ fa'' mi''] } fa''4 re''8 |
si'4. r8 r <<
  \tag #'violino1 {
    re''8\p |
    mi''8. fa''16 sol''8 sol'' fa'' mi'' |
    re''4( sol''8) re''4 re''8 |
    mi''8. fa''16 sol''8 sol'' fa'' mi'' |
    re''4. r8 r \grace { la'16[ si'] } do''8\sf |
    si'4(\p la'8 sol'4) \grace { fa''16[ sol''] } la''8\sf |
    sol''4(\p fa''8 mi''4) do'''8\f |
    do''' si''16 la'' sol'' fa'' mi''4 re''8 |
    re''4.( mi''4) \grace { la'16[ si'] } do''8\sf |
    si'8 si'16. do''32 la'16. si'32 sol'4 \grace { fa''16[ sol''] } la''8\sf |
    sol''\p~ sol''16. la''32 fa''16. sol''32 mi''4 do'''8\f |
    do'''8 si''16 la'' sol'' fa'' mi''4 re''8 |
    \grace { re''16[ do'' si'] } do''4\p r8 \grace { sol''16[ fad'' mi''] } fad''4 r8 |
    \grace { la''16[ sol'' fad''] } sol''4 r8 \grace { fa''16[ mi'' re''] } mi''4 r8 |
    \grace { sol''16[ fa'' mi''] } fa''4 r8 \grace { mi''16[ re'' do''] } re''4 r8 |
    \grace { re''16[ do'' si'] } do''4 r8 \grace { sol''16[ fad'' mi''] } fad''4 r8 |
    \grace { la''16[ sol'' fad''] } sol''4 r8 \grace { fa''16[ mi'' re''] } mi''4 r8 |
    \grace { sol''16[ fa'' mi''] } fa''4 r8 \grace { mi''16[ re'' do''] } re''4 r8 |
    do''4( mi''8 do''4 mi''8) |
    do''2.\fermata |
  }
  \tag #'violino2 {
    si'8-\sug\p |
    do''8. re''16 mi''8 mi'' re'' do'' |
    si'4 si'8 si'4 si'8 |
    do''8. re''16 mi''8 mi'' re'' do'' |
    si'4. r8 r \grace { la16[ si] } do'8-\sug\sf |
    si4(-\sug\p la8 sol4) \grace { fa'16[ sol'] } la'8-\sug\sf |
    sol'4-\sug\p( fa'8 mi'4) mi''8-\sug\f |
    la'' sol''16 fa'' mi'' re'' do''4 si'8 |
    si'4.( do''4) \grace { la16[ si] } do'8 |
    si4 la8 sol4 \grace { fa'16[ sol'] } la'8-\sug\sf |
    sol'4-\sug\p fa'8 mi'4 mi''8-\sug\f |
    la'' sol''16 fa'' mi'' re'' do''4 si'8 |
    \grace { re''16[ do'' si'] } do''4-\sug\p r8 \grace { si'16[ la' sol'] } la'4 r8 |
    \grace { do''16[ si' la'] } si'4 r8 \grace { la'16[ sol' fa'] } sol'4 r8 |
    \grace { si'16[ la' sol'] } la'4 r8 \grace { do''16[ si' la'] } si'4 r8 |
    \grace { re''16[ do'' si'] } do''4 r8 \grace { si'16[ la' sol'] } la'4 r8 |
    \grace { do''16[ si' la'] } si'4 r8 \grace { la'16[ sol' fa'] } sol'4 r8 |
    \grace { si'16[ la' sol'] } la'4 r8 \grace { do''16[ si' la'] } si'4 r8 |
    mi'4( sol'8 mi'4 sol'8) |
    mi'2.\fermata |
  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>
<<
  \tag #'violino1 {

  }
  \tag #'violino2 {

  }
>>