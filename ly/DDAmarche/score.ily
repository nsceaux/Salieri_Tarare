\score {
  <<
    \new GrandStaff \with { \oboiInstr } <<
      \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
      \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Bassons et Basses }
        shortInstrumentName = \markup\center-column { Bn. B. }
      } <<
        \global \keepWithTag #'basso \includeNotes "basso"
        \origLayout {
          s8 s2.*7\break s2.*8\break s2.*7\pageBreak
          s2.*8\break s2.*8\break \grace s8.
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
