\clef "alto" r8 |
do'4-\sug\mf do'8 do'4 do'8 |
si4 si8 do'4 mi'8 |
do'4 fa'8 mi'4 mi'8 |
si sol si do'4 sol8-\sug\ff |
do'8. si16 la8 sol fa mi |
la8. sol16 fa8 mi re do |
mi4 la8 fad4\trill mi16 fad |
sol8. re'16 si8 sol4 r8 |
R2. |
r8 la16-\sug\f si do' dod' re'4. |
R2. |
r8 sol'16-\sug\f re' si re' sol4 \grace { mi'16[ fad'] } sol'8-\sug\sf |
\once\slurDashed fad'4(-\sug\p mi'8 re'4) \grace { sol'16[ la'] } si'8-\sug\sf |
\once\slurDashed la'4(-\sug\p sol'8 fad'4) si8-\sug\f |
mi'4. re'4 re'8 |
re'4.~ re'4 \grace { mi'16[ fad'] } sol'8-\sug\sf |
fad'4-\sug\p( mi'8) re'4 \grace { sol'16[ la'] } si'8-\sug\sf |
la'4( sol'8) fad'4 si8-\sug\f |
mi'4. re'4 re'8 |
re'4. r8 r sol_\markup\whiteout { \dynamic f \italic\bold sempre } |
sol8. la16 sib8 la dod' dod' |
re' fa'16 mi' re' do'! si8 re'16 do' si la |
\footnoteHere #'(0 . 0) \markup {
  Source : \score {
    { \tinyQuote \clef "alto" \time 6/8 sol8. la16 sol8 sol la si }
    \layout { \quoteLayout }
  }
}
sol8. si16 la8 sol la si |
do' mi'16 re' do' si la8 do'16 si la sol |
fa8 la'16 sol' fa' mi' re' mi' re' do' si la |
<< \rp#2 { sol8 sol' sol' } { s4. s\p } >> |
\ru#6 { sol8 sol' sol' } |
sol8 sol' sol' sol4 \grace { fa16[ sol] } la8-\sug\sf |
sol4(-\sug\p fa8 mi4) \grace { re'16[ mi'] } fa'8-\sug\sf |
mi'4(-\sug\p re'8 do'4) do8-\sug\f |
fa4. sol4 sol8 |
sol4.~ sol4 \grace { fa16[ sol] } la8 |
sol4 fa8 mi4 \grace { re'16[ mi'] } fa'8-\sug\sf |
mi'4-\sug\p re'8 do'4 do8-\sug\f |
fa4. sol |
la4-\sug\p r8 re4 r8 |
sol4 r8 do4 r8 |
fa4 r8 sol4 r8 |
la4 r8 re4 r8 |
sol4 r8 do4 r8 |
fa4 r8 sol4 r8 |
do8 do' do' do do' do' |
do2.\fermata |
