\clef "treble" sol'8\mf |
do''8. mi''16 re''8 do''4 mi''8 |
sol''4 fa''8 mi''4 do'''8 |
mi''4 la''8 sol''4 do''8 |
re''4 mi''16 fa'' mi''4 r8 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { \grace { fa''16[ mi'' re''] } mi''4 s8 \grace { la''16[ sol'' fa''] } sol''4 s8 |
    \grace { re''16[ do'' si'] } do''4 s8 \grace { fa''16[ mi'' re''] } mi''4 s8 |
    s4 do''8 re''4 mi''16 do'' |
    si'4. }
  { \grace { la'16[ sol' fad'] } sol'4 s8 \grace { do''16[ si' la'] } si'4 s8 |
    \grace { fa'16[ mi' re'] } mi'4 s8 \grace { la'16[ sol' fa'] } sol'4 s8 |
    s4 mi'8 la'4 la'8 |
    re'4. }
  { s4 r8 s4 r8 |
    s4 r8 s4 r8 |
    r4 }
>>
<<
  \tag #'(oboe1 oboi) {
    r8 r <>^"solo" re''-\sug\mf |
    re''-! mi''-! fad''-! sol''-! la''-! si''-! |
    do'''\noBeam do'''16-\sug\f si'' la'' sol'' fad''4 re''8-\sug\mf |
    re''-! fad''-! sol''-! la''-! si''-! do'''-! |
    si''\noBeam re'''16\f do''' si'' la'' sol''4 \grace { mi''16[ fad''] } sol''8-\sug\sf |
    \slurDashed fad''4(-\sug\p mi''8 re''4) \grace { sol''16[ la''] } si''8-\sug\sf |
    la''4(-\sug\p sol''8 fad''4)
  }
  \tag #'oboe2 { r4 r8 | R2.*5 | r4 r8 r4 }
>>
<>\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { re'''8 |
    \slurDashed re'''8( do'''16 si'' la'' sol'' sol''4 la''8) |
    la''4.( si''4) \grace { mi''16[ fad''!] } sol''8-\sug\sf |
    fad''~ fad''16. sol''32 mi''16. fad''32 re''4 \grace { sol''16[ la''] } si''8-\sug\sf |
    la''8~ la''16. si''32 sol''16. la''32 fad''4 re'''8 |
    re''' do'''16 si'' la'' sol'' sol''4 la''8 |
    sol''8. re''16 si'8 sol'4 }
  { \slurDashed re''8( |
    sol'4) do''8( si'4) fad''8 |
    fad''4.( sol''4) r8 |
    R2. |
    r4 r8 r4 re''8 |
    sol'4 do''8 si' re'' fad' |
    sol'4. r4 }
  { s8 | s2.*3 | s4. s4 <>-\sug\f }
>> r8 |
R2. |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { \grace { sol''16[ fa'' mi''] } fa''4 s8 \grace { mi'''16[ re''' do'''] } re'''4 s8 | }
  { \grace { si'16[ la' sol'] } la'4 s8 \grace { sol''16[ fa'' mi''] } fa''4 s8 | }
  { s4 r8 s4 r8 | }
>>
R2. |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { \grace { fa''16[ mi'' re''] } mi''4 s8 \grace { re'''16[ do''' si''] } do'''4 s8 |
    \grace { si''16[ la'' sol''] } la''4 s8 \grace { sol''16[ fa'' mi''] } fa''4 re''8 |
    si'4. }
  { \grace { la'16[ sol' fa'] } sol'4 s8 \grace { fa''16[ mi'' re''] } mi''4 s8 |
    \grace { si''16[ la'' sol''] } la''4 s8 \grace { sol''16[ fa'' mi''] } fa''4 re''8 |
    si'4. }
  { s4 r8 s4 r8 | s4 r8 }
>> r8 r <>-\sug\p \twoVoices#'(oboe1 oboe2 oboi) <<
  { re''8 |
    mi''8. fa''16 sol''8 sol'' fa'' mi'' |
    re''4 sol''8 re''4 re''8 |
    mi''8. fa''16 sol''8 sol'' fa'' mi'' |
    re''4. }
  { si'8 |
    do''8. re''16 mi''8 mi'' re'' do'' |
    si'4 si'8 si'4 si'8 |
    do''8. re''16 mi''8 mi'' re'' do'' |
    si'4. }
>>
<<
  \tag #'(oboe1 oboi) {
    r8 r <>^"solo" \grace { la'16[ si'] } do''8-\sug\sf |
    \slurDashed si'4(-\sug\p la'8 sol'4) \grace { fa''16[ sol''] } la''8-\sug\sf |
    sol''4(-\sug\p fa''8 mi''4)
  }
  \tag #'oboe2 { r4 r8 | R2. | r4 r8 r4 }
>> <>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { do'''8 |
    do''' si''16 la'' sol'' fa'' mi''4 re''8 |
    \slurDashed re''4.( mi''4) \grace { la'16[ si'] } do''8 |
    si'8 si'16. do''32 la'16. si'32 sol'4 \grace { fa''16[ sol''] } la''8-\sug\sf |
    sol''-\sug\p~ sol''16. la''32 fa''16. sol''32 mi''4 do'''8 |
    do'''8 si''16 la'' sol'' fa'' mi''4 re''8 |
    \grace { re''16[ do'' si'] } do''4 s8 \grace { sol''16[ fad'' mi''] } fad''4 s8 |
    \grace { la''16[ sol'' fad''] } sol''4 s8 \grace { fa''16[ mi'' re''] } mi''4 s8 |
    \grace { sol''16[ fa'' mi''] } fa''4 s8 \grace { mi''16[ re'' do''] } re''4 s8 |
    \grace { re''16[ do'' si'] } do''4 s8 \grace { sol''16[ fad'' mi''] } fad''4 s8 |
    \grace { la''16[ sol'' fad''] } sol''4 s8 \grace { fa''16[ mi'' re''] } mi''4 s8 |
    \grace { sol''16[ fa'' mi''] } fa''4 s8 \grace { mi''16[ re'' do''] } re''4 s8 |
    do''4( mi''8 do''4 mi''8) |
    do''2.\fermata | }
  { mi''8 |
    la'' sol''16 fa'' mi'' re'' do''4 si'8 |
    \slurDashed si'4.( do''4) r8 |
    R2. |
    r4 r8 r4 mi''8 |
    la'' sol''16 fa'' mi'' re'' do''4 si'8 |
    \grace { re''16[ do'' si'] } do''4 s8 \grace { si'16[ la' sol'] } la'4 s8 |
    \grace { do''16[ si' la'] } si'4 s8 \grace { la'16[ sol' fa'] } sol'4 s8 |
    \grace { si'16[ la' sol'] } la'4 s8 \grace { do''16[ si' la'] } si'4 s8 |
    \grace { re''16[ do'' si'] } do''4 s8 \grace { si'16[ la' sol'] } la'4 s8 |
    \grace { do''16[ si' la'] } si'4 s8 \grace { la'16[ sol' fa'] } sol'4 s8 |
    \grace { si'16[ la' sol'] } la'4 s8 \grace { do''16[ si' la'] } si'4 s8 |
    mi'4( sol'8 mi'4 sol'8) |
    mi'2.\fermata | }
  { s8 | s2.*3 | s4. s4 s8-\sug\f | s2. |
    s4-\sug\p r8 s4 r8 | s4 r8 s4 r8 | s4 r8 s4 r8 |
    s4 r8 s4 r8 | s4 r8 s4 r8 | s4 r8 s4 r8 | }
>>
