\clef "bass" r8 |
R2.*3 |
r4 r8 r r sol\ff |
do'8. si16 la8 sol fa mi |
la8. sol16 fa8 mi re do |
mi4 la8 fad4\trill mi16 fad |
sol8. re16 si,8 sol,4 r8 |
R2. |
r8 la16\f si do' dod' re'4. |
R2. |
r8 sol16\f re si, re sol,4 r8 |
R2. |
r4 r8 r r si,-\sug\f |
mi4. re4 re8 |
re8 fad re sol4 r8 |
R2. |
r4 r8 r r si,-\sug\f |
mi4. re4 re8 |
sol,4. r8 r sol_\markup { \dynamic f \italic\bold sempre } |
sol8. la16 sib8 la dod' dod' |
re' fa'16 mi' re' do'! si8 re'16 do' si la |
sol8. si16 la8 sol la si |
do' mi'16 re' do' si la8 do'16 si la sol |
fa8 la16 sol fa mi re mi re do si, la, |
<< \rp#2 { sol,8 sol sol } { s4. s\p } >> |
\ru#6 { sol,8 sol sol } |
sol,8 sol sol sol, r r |
R2. |
r4 r8 r r do\f |
fa4. sol4 sol,8 |
sol, si, sol, do4 r8 |
R2. |
r4 r8 r r do |
fa4. sol4 sol,8 |
la,\p la la re re' re' |
sol, sol sol do do' do' |
fa, fa fa sol, sol sol |
la, la la re re' re' |
sol, sol sol do do' do' |
fa, fa fa sol, sol sol |
do do' do' do do' do' |
do2.\fermata |
