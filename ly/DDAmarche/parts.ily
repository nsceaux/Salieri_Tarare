\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso #:tag-notes basso)
   (oboi #:score-template "score-oboi")
   (fagotti #:notes "basso" #:tag-notes fagotti)
   (silence #:on-the-fly-markup , #{\markup\tacet #}))
