\clef "alto" sol16-\sug\p sol sol sol sol4:16 sol4 r |
fa2:16 fa4 r |
fa2:16 fa4 r |
sol2:16 sol4 r |
sol2:16 sol4 r |
fad2:16 fad4 r |
fad r la r |
re'2:8 re':8 |
re'2:8 re':8 |
sol'4 r4 r16 re'' do'' sib' la' sol' fa' mib' |
re' do' re' mib' fa' mib' re' do' sib do' re' mib' fa' mib' re' do' |
sib do' re' mib' fa' mib' re' do' sib do' re' mib' fa' mib' re' do' |
sib2:8\cresc sib:8 |
sib4 r r2 |
mib'4\p r r2 |
re'4 r r2 |
r r4 sol' |
