\tag #'recit {
  Crains de pay -- er de ta cou -- ron -- ne,
  un at -- ten -- tat sur sa per -- son -- ne.
  Ses sol -- dats se -- raient les plus forts.
  Si, sur un pré -- tex -- te fri -- vo -- le,
  tu les pri -- ves de leur i -- do -- le,
  cet -- te mi -- lice, en sa fu -- reur,
  peut, ou -- bli -- ant ton rang et ta nais -- san -- ce…
}
J’ai tout pré -- vu ; Ta -- ra -- re, dans l’er -- reur,
court à sa perte en cher -- chant la ven -- gean -- ce.
