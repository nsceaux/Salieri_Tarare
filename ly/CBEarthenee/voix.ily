\clef "bass"
<<
  \tag #'basse { R1*13 | r2 }
  \tag #'recit {
    <>^\markup\character Arthénée sol4 sol8 sol sol sol sol sol |
    fa4 fa r8 fa fa fa |
    sib4. sib8 sib4. sib8 |
    sol4 mib r sol8 sol |
    do'4. do'8 do'4 la8 sol |
    fad2 r |
    re8 re re mi fad4 fad8 sol |
    la4 la r la8 la |
    re'4 re'8 re' re'4. re'8 |
    sib4 sib r8 sib sib sib |
    sib fa fa fa re4 r |
    fa r fa8 fa fa sol |
    lab4. lab8 lab4. lab8 |
    re'4 re'
  }
>>
\ffclef "bass" <>^\markup\character-text Atar l’interrompt
r8 lab lab sib |
sol4 r8 sol sol sol sol la |
si4 re'8 re'16 re' si8 si r sol16 la |
si4 si8 do' sol sol r4 |
