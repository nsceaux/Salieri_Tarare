\clef "bass" mib16\p mib mib mib mib4:16 mib4 r |
re2:16 re4 r |
re2:16 re4 r |
mib2:16 mib4 r |
mib2:16 mib4 r |
re2:16 re4 r |
re4 r re r |
re2:8 re:8 |
re2:8 re:8 |
sol4 r4 r16 re' do' sib la sol fa mib |
re do re mib fa mib re do sib, do re mib fa mib re do |
sib, do re mib fa mib re do sib, do re mib fa mib re do |
sib,2:8\cresc sib,:8 |
sib,4 r r2 |
mib4\p r r2 |
re4 r r2 |
r r4 sol |
