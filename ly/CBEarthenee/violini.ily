\clef "treble"
<<
  \tag #'violino1 {
    mib'16\p re' mib' fa' sol' mib' re' mib' sib8 sib[-! mib'-! sol'-!] |
    sib'16 la' sib' do'' re'' sib' fa' re' sib8 sib[ re' fa'] |
    sib'16 la' sib' do'' re'' sib' fa' re' sib8 re'[ fa' sib'] |
    sol'16 mib' sol' sib' mib'' sib' sol' sib' mib'8 mib''-! sol''-! mib''-! |
    do''16 sol' la' si' do'' si' do'' re'' mib''8 do'' la' sol' |
    fad'16 mi' fad' sol' la' sol' fad' mi' re'8 re'' la' fad' |
  }
  \tag #'violino2 {
    sib16-\sug\p sib sib sib sib4:16 sib4 r |
    sib2:16 sib4 r |
    sib2:16 sib4 r |
    sib2:16 sib4 r |
    do'2:16 do'4 r |
    la2:16 la4 r |
  }
>>
re'8 re'16 la re' la mi' la fad'8 fad'16 re' fad' re' sol' mi' |
<<
  \tag #'violino1 {
    \rt#4 { la'16 fad' } \rt#4 { la'16 fad' } |
    \rt#4 { la'16 fad' } \rt#4 { la'16 fad' } |
  }
  \tag #'violino2 {
    \rt#4 { fad'16 re' } \rt#4 { fad'16 re' } |
    \rt#4 { fad'16 re' } \rt#4 { fad'16 re' } |
  }
>>
sib'16 la' sib' do'' re'' do'' sib' la' sib' re'' do'' sib' la' sol' fa' mib' |
re' do' re' mib' fa' mib' re' do' sib do' re' mib' fa' mib' re' do' |
sib do' re' mib' fa' mib' re' do' sib do' re' mib' fa' mib' re' do' |
<<
  \tag #'violino1 {
    sib16\cresc re' fa' re' fa' re' fa' re' \rt#4 { lab' fa' } |
    <fa' re''>4\!
  }
  \tag #'violino2 {
    sib8-\sug\cresc re'16 sib re' sib re' sib \rt#4 { fa' re' } |
    lab'4\!
  }
>> r4 r2 |
<<
  \tag #'violino1 { <sol' mib''>4\p }
  \tag #'violino2 { <sol' sib>4-\sug\p }
>> r4 r2 |
<<
  \tag #'violino1 { si'4 }
  \tag #'violino2 { fa' }
>> r4 r2 |
r2 r4 <sol re' si'>4 |
