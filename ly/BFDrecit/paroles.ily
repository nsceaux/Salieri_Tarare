Quoi ! sol -- dat ! pleu -- rer u -- ne fem -- me !
Ton Roi ne te re -- con -- naît pas.
Si tu perds l’ob -- jet de ta flam -- me,
tout un sé -- rail t’ou -- vre ses bras.
Pour u -- ne beau -- té, quel -- ques char -- mes,
on peut re -- trou -- ver mille at -- traits,
mais l’hon -- neur qu’on perd dans les lar -- mes,
on ne le re -- trou -- ve ja -- mais !

Sei -- gneur !
