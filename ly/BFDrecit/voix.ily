\clef "bass" <>^\markup\character-text Atar Fièrement ^\markup\italic parlé
do'4. sol8 mi4 r8 sol |
sib4 do'8 sol la la r4 |
r r8 do' la4 la8 la |
la4 sib8 do' fa4 r |
r2 r4 r8 fad16 sol |
la4. la8 la4 sol8 la |
fad fad r4 la la8 la |
re'4 do'8 do'16 re' si4 r8 sol |
sol4 sol8 la si4 r8 si16 si |
si8 fad r fad fad4 fad8 sol |
la4 la8. si16 sol4 r |
si4. si8 sol8. sol16 sol8 la16 si |
mi8 mi r4 sol8 sol fad mi |
lad4 lad8 si fad?4 r |
\ffclef "tenor/G_8" <>^\markup\character-text Tarare suppliant
r4 r8 fad' \grace mi' re'2 |
