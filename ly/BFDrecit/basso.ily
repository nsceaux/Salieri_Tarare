\clef "bass" R1 |
r2 r8 r16 fa\f fa8. fa16 |
fa4 r r2 |
r r4 r8. fa16 |
fa4 mi re r |
R1*2 |
fad4 r sol r |
R1 |
red4 r r2 |
r2 mi4 r |
R1*2 |
mi4 r r fad |
si,2 r |
