\clef "treble" R1 |
r2 r8 r16 <<
  \tag #'violino1 { la'16\f la'8. la'16 | la'4 }
  \tag #'violino2 { do'16-\sug\f do'8. do'16 | do'4 }
>> r4 r2 |
r r4 r8. <<
  \tag #'violino1 {
    do''16 |
    do''8. do''16 dod''8. dod''16 re''4
  }
  \tag #'violino2 {
    la'16 |
    <<
      { la'8. la'16 la'8. la'16 la'4 } \\
      { la'8. la'16 sol'8. sol'16 fad'4 }
    >>
  }
>> r4 |
R1*2 |
<<
  \tag #'violino1 { re''4 r si' r | }
  \tag #'violino2 { <re' la'>4 r <re' si'> r | }
>>
R1 |
<si' fad''>4 r r2 |
r2 sol''4 r4 |
R1*2 |
<dod'' lad''>4 r r <fad' dod'' lad''> |
<fad' re'' si''>2 r |
