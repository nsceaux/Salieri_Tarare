\clef "alto" R1 |
r2 r8 r16 fa'-\sug\f fa'8. fa'16 |
fa'4 r r2 |
r r4 r8. fa'16 |
fa'8. fa'16 mi'8. mi'16 re'4 r |
R1*2 |
fad4 r sol r |
R1 |
red'4 r r2 |
r2 mi'4 r |
R1*2 |
mi'4 r r fad' |
si2 r |
