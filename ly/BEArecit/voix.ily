\clef "bass" <>^\markup\character Urson
r4 r8 sol sib4 r16 sib sib sib |
sol4. sol8 sol sol lab sib |
sib mib
\ffclef "bass" <>^\markup\character Atar
r8 sol16 sol sol4 sol8 sol |
do'4. do'8 do' sol sol la |
fad fad
\ffclef "bass" <>^\markup\character Urson
r8 re' sib sib r16 sol sol la |
sib4 sib8 sib sib4 sib8 sib |
la4 r8 la16 si dod'4 r8 dod'16 mi' |
dod'4 la8 si dod'4 dod'8 re' |
la4 r
\ffclef "bass" <>^\markup\character Atar
r8 la la re' |
do' do' r do'16 sol la8 la
\ffclef "bass" <>^\markup\character Urson
r8 do' |
la4. fa8 la la sib do' |
fa4 r
\ffclef "bass" <>^\markup\character Atar
r4 r8 la |
do'4 r8 do' la la r do'16 re' |
sib4 r^\markup\italic (Urson sort) r2 |
r4^\markup\italic (à Altamort) r8 sol sib4 la8 sol |
dod'4 r8 dod'16 re' la8 la r4 |
R1 |
