\clef "bass" mib2 r |
R1*2 |
mib1 |
re2 sol |
sol1 |
dod |
dod |
r16 <>^\markup\italic Violoncelli solo la,-.\p si,!-. dod-. re-. mi-. fa-. mi-. re4 r |
r8. mi16 mi4 r8. fa16 fa4~ |
fa1 |
r16 do-.\p ^\markup\italic Violoncelli re-. mi-. fa-. sol-. la-. sol-. fa4 r |
R1 |
sib,4\p r sib fad |
sol r r2 |
r8. sol16\p sol4 r la\p |
re2 r |
