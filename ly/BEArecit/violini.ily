\clef "treble" <sol mib'>2 r |
R1*2 |
<<
  \tag #'violino1 {
    do''1 |
    la'!2 sib'~ |
    sib'1 |
    la' |
    la' |
  }
  \tag #'violino2 {
    sol'1 |
    fad'2 sol'~ |
    sol'1 |
    sol' |
    sol' |
  }
>>
r16 la-.\p si!-. dod'-. re'-. mi'-. fa'-. mi'-. re'4 r |
<<
  \tag #'violino1 {
    r8. do''16 do''4 r8. do''16 do''4~ |
    do''1 |
  }
  \tag #'violino2 {
    r8. sol'16 sol'4 r8. la'16 la'4~ |
    la'1 |
  }
>>
r16 do'-.\p re'-. mi'-. fa'-. sol'-. la'-. sol'-. fa'4 r |
R1 |
<<
  \tag #'violino1 { fa'4\p }
  \tag #'violino2 { re'4-\sug\p }
>> r8 sib'16 do'' re'' re' re' re' do' re' do' re' |
sib4 r r2 |
r8. <<
  \tag #'violino1 {
    dod'''16\p \appoggiatura { re'''16[ dod''' si''] } dod'''4
  }
  \tag #'violino2 {
    mi''16 \appoggiatura { fa''[ mi'' re''] } mi''4
  }
>> r4 <dod'' mi' la>4\p |
<la fa' re''>2 r |
