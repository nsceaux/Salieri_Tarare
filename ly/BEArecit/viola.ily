\clef "alto" mib'2 r |
R1*2 |
do'1 |
re'2 re'~ |
re'1 |
mi'1 |
mi' |
r16 la-.\p si!-. dod'-. re'-. mi'-. fa'-. mi'-. re'4 r |
r8. mi'16 mi'4 r8. fa'16 fa'4~ |
fa'1 |
r16 do'-.\p re'-. mi'-. fa'-. sol'-. la'-. sol'-. fa'4 r |
R1 |
sib4 r sib' fad' |
sol' r r2 |
r8. sol'16\p sol'4 r la' |
re'2 r |
