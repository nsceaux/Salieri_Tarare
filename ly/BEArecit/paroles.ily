Sei -- gneur, c’est ce guer -- rier, du peu -- ple la mer -- veil -- le…

Gar -- de- toi que son nom of -- fen -- se mon o -- reil -- le !

Il pleu -- re ; au -- tour de lui tout le peuple em -- pres -- sé
dit tout haut, qu’en ses vœux il doit être ex -- au -- cé.

Tu dis qu’il pleu -- re, qu’il sou -- pi -- re ?

Ses traits en sont presque ef -- fa -- cés.

Ur -- son, qu’il en -- tre ; c’est as -- sez.

Il est mal -- heu -- reux… Je res -- pi -- re.
