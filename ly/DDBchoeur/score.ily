\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new GrandStaff \with { \oboiInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          { \noHaraKiri s2 s1*26 \revertNoHaraKiri }
          \global \keepWithTag #'oboe1 \includeNotes "oboi"
        >>
        \new Staff <<
          { \noHaraKiri s2 s1*26 \revertNoHaraKiri }
          \global \keepWithTag #'oboe2 \includeNotes "oboi"
        >>
      >>
      \new GrandStaff \with { \trombeInstr } <<
        \new Staff <<
          { \noHaraKiri s2 s1*26 \revertNoHaraKiri }
          \global \keepWithTag #'tromba1 \includeNotes "trombe"
        >>
        \new Staff <<
          { \noHaraKiri s2 s1*26 \revertNoHaraKiri }
          \global \keepWithTag #'tromba1 \includeNotes "trombe"
        >>
      >>
      \new Staff \with { \fagottiInstr } <<
        { \noHaraKiri s2 s1*26 \revertNoHaraKiri }
        \global \includeNotes "fagotti"
      >>
      \new Staff \with { \timpaniInstr \consists "Mark_engraver" } <<
        { \noHaraKiri s2 s1*26 \revertNoHaraKiri }
        \global \includeNotes "timpani"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr \consists "Mark_engraver" } <<
        \global \includeNotes "viola"
      >>
    >>
    \new ChoirStaff \with {
      instrumentName = \markup\center-column\smallCaps { Chœur d'Européans }
      shortInstrumentName = \markup\smallCaps Ch.
      \haraKiri
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'bergere1 \includeNotes "voix"
      >> \keepWithTag #'bergere1 \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'bergere2 \includeNotes "voix"
      >> \keepWithTag #'bergere2 \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'berger \includeNotes "voix"
      >> \keepWithTag #'berger \includeLyrics "paroles"
    >>
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
      \consists "Mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s2 s1*4\pageBreak
        s1*5 s2 \bar "" \pageBreak
        \grace s8 s2 s1*4\pageBreak
        s1*5 s2 \bar "" \pageBreak
        \grace s8 s2 s1*5\pageBreak
        s1*6\break s1*6\pageBreak
        s1*6\break s1*4\break
      }
    >>
  >>
  \layout {
    \context {
      \Score
      \remove "Metronome_mark_engraver"
      \remove "Mark_engraver"
    }
  }
  \midi { }
}
