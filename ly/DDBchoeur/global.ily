\tag #'all \key do \major \midiTempo#120
\tempo\markup\vcenter { \musicglyph#"scripts.segno" Allegretto }
\time 2/2 \partial 2 s2 s1*26 s4 \endMark "Fin." s4 s8. \bar "|."
s16 s4 s1*24 s4. \tempo\markup\musicglyph#"scripts.segno" s8 \bar "|." \dacapoMark
