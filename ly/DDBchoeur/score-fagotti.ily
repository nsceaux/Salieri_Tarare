\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      { s2 s1*26 s2 s8.\break }
      \global \keepWithTag #'basse \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'basse \includeLyrics "paroles" }
    \new Staff <<
      \global \keepWithTag #'fagotti \includeNotes "fagotti"
    >>
    \new Staff <<
      { \startHaraKiri s2 s1*27 \stopHaraKiri }
      \global \includeNotes "basso"
    >>
  >>
  \layout { }
}
