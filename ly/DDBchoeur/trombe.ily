\clef "treble"
\twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''8 s re'' s |
    do''4 s do'' re''8. mi''16 |
    re''2 re''4 re'' |
    mi''4. sol''8 fa''8. mi''16 re''8. do''16 |
    sol'4 }
  { do''8 s sol' s |
    mi'4 s sol' sol'8. sol'16 |
    sol'2 sol'4 sol' |
    do''4. mi''8 do''4 re''8. do''16 |
    sol'4 }
  { s8\p r s r | s4 r s2\f | }
>> r4 r2 |
<>-\sug\p \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2 re'' | mi''4 }
  { mi'2 sol' | do''4 }
>> r4 r2 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2 re'' | mi''4 }
  { mi'2 sol' | do''4 }
>> r4 r2 |
R1 |
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8. re''16 re''4 }
  { sol'8. sol'16 sol'4 }
>> r4 |
R1 |
r2 r4 r8 <>-\sug\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'8 |
    do''1 |
    do''~ |
    do''8*2/3[ do'' do''] do''[ do'' do''] \tuplet 6/4 do''2.:8 |
    \tuplet 6/4 do'':8 \tuplet 6/4 do'':8 |
    do''2
  }
  { sol'8 |
    do'1 |
    do'~ |
    do'8*2/3[ do'' do''] do''[ do'' do''] \tuplet 6/4 do''2.:8 |
    \tuplet 6/4 do'':8 \tuplet 6/4 do'':8 |
    do''2    
  }
>> r2 |
R1*2 |
r4 <>\p \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8. re''16 re''4 re''8. re''16 | }
  { sol'8. sol'16 sol'4 sol'8. sol'16 | }
  { s2 s4-\sug\ff }
>>
do''1 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2. re''4 | do''2 re'' | mi'' re'' | mi''4 }
  { do''2. sol'4 | mi'2 sol' | do'' sol' | do''4 }
>> r4 r2 |
r2 r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'4 | do'' sol' do'4*3/4 s16 }
  { sol'4 | do'' sol' do'4*3/4 s16 }
>> r4 |
R1*24 |
r2
