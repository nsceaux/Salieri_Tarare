\clef "bass" do'8-\sug\p r sol r |
mi4 r mi\f re8. do16 |
si,2 si,4 sol, |
do4. do8 la8. sol16 fa8. fad16 |
sol4 sol, r2 |
do4\p r sol, r |
do, r r2 |
do4 r sol, r |
do2 r |
r4 do' do do' |
r sol sol, sol |
r do do fa |
sol sol sol sol,8.\ff sol,16 |
do8*2/3[ do do] do[ do do] \tuplet 6/4 do2.:8 |
\tuplet 6/4 do:8 \tuplet 6/4 do:8 |
do4. do'8 la4 do' |
la do' la mi |
fa2. re8.\p re16 |
sol2 r |
r4 do\p do, do |
r sol sol, sol8.\f sol16 |
do8*3/2 do'8*1/2 do'8. do'16 do'4 fa8. fa16 |
\tuplet 6/4 sol2.:8 \tuplet 6/4 sol,:8 |
\tuplet 6/4 do:8 \tuplet 6/4 do:8 |
\tuplet 6/4 do:8 \tuplet 6/4 do:8 |
do4. do8 mi4.\trill re16 do |
fa8. fa16 sol8. la16 sol4 sol, |
do sol, do,4*3/4 s16 r4 |
r8. do'16\p sol8. mi16 do4 r |
r8. sol16 re8. si,16 sol,4 sol |
re2 re |
sol4 r <>^\markup\italic "Violoncelli" sol4 sol8. sol16 |
re'4 r la la8. la,16 |
re4 r sol sol8. sol16 |
re'4 r la la8. la16 |
re4 r <>^\markup\italic "Tutti" re\p mi |
fad-! mi-! fad-! mi-! |
re2 do4\mf do |
si,4 si, do do |
re8 re re re re2:8 |
sol,4 r r2 |
r r4 r8 la,\mf |
re2 r |
R1 |
r2 re4\p^\markup\italic "Tutti" mi8. mi16 |
fad4 mi fad mi |
re re do!\mf do |
si,\p si, do do |
re2:8 re:8 |
sol:8\f do4 do' |
si8 si, si, si, do2:8 |
re:8 re:8 |
sol2\fermata
