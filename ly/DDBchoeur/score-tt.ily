\score {
  <<
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      { s2 s1*26 s2 s8.\break }
      \global \keepWithTag #'tt \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'tt \includeLyrics "paroles" }
    \new StaffGroup \with { \haraKiri } <<
      \new GrandStaff \with {
        instrumentName = \markup\center-column { Trompettes \small in Ut }
      } <<
        \new Staff << \global \keepWithTag #'tromba1 \includeNotes "trombe" >>
        \new Staff << \global \keepWithTag #'tromba1 \includeNotes "trombe" >>
      >>
      \new Staff \with {
        instrumentName = "Timbales"
      } << \global \includeNotes "timpani" >>
    >>
  >>
  \layout { indent = \largeindent }
}
