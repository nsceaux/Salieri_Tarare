\clef "treble"
<<
  \tag #'oboe1 {
    mi''4-\sug\p re''8. mi''16 |
    do''4 r do''-\sug\f re''8. mi''16 |
    fa''2 re''4 re'' |
    mi''4. sol''8 fa''8. mi''16 re''8. do''16 |
    si'4 r <>^"solo" sol''8*2/3[ la'' sol''] fa''[ mi'' re''] |
    do''4-\sug\p \grace re''8 do''8*2/3 si' do'' re''4 \grace mi''8 re''8*2/3 do'' re'' |
    mi''4. sol'8 sol''8*2/3[ la'' sol''] fa''[ mi'' re''] |
    do''4 \grace re''8 do''8*2/3 si' do'' re''4 \grace mi''8 re''8*2/3 do'' re'' |
    mi''4 r r2 |
    R1*3 |
    r2 r4 r8 sol'8-\sug\ff |
    do''2 fa''4 re'' |
    sol'' mi'' fa'' re'' |
    mi''4. mi''8 fa''4 mi'' |
    fa'' mi'' fa'' sol'' |
    la''2 r |
    R1*2 |
    r2 r4 re''8.-\sug\ff re''16 |
    mi''4. fa''8 sol''8. mi''16 la''8. fa''16 |
    mi''4 do'''2 si''4 |
    do'''4. sol''8 si''4.\trill la''16 sol'' |
    do'''4 sol''8. sol''16 si''4.\trill la''16 sol'' |
    do'''4.
  }
  \tag #'oboe2 {
    do''4-\sug\p si'8. si'16 |
    do''4 r sol'-\sug\f fa'8. mi'16 |
    re'2 sol'4 sol' |
    sol'4. sol'8 do''4 re''8. do''16 |
    si'4 r r2 |
    R1*7 |
    r2 r4 r8 sol'-\sug\ff |
    mi'2 la'4 sol' |
    sol' sol' la' si' |
    do''4. sol'8 fa'4 sol' |
    fa' sol' fa' do'' |
    do''2 r |
    R1*2 |
    r2 r4 si'8.-\sug\ff si'16 |
    do''4. re''8 mi''8. do''16 do''8. do''16 |
    do''4 mi''2 re''4 |
    mi''2 fa'' |
    mi'' fa'' |
    mi''4. 
  }
>> do'8 mi'4.\trill re'16 do' |
fa'8. fa'16 sol'8. la'16 sol'4 sol' |
do''4 sol' do'4*3/4 s16 r4 |
<<
  \tag #'oboe1 {
    R1*7 |
    r8 <>^"solo" fad''16-\sug\mf sol'' la''-! la''-! la''-! la''-! do''!4 r |
    R1*8 |
    r8 <>^"solo" fad''16-\sug\mf sol'' la'' la'' la'' la'' do''4 r |
    R1*7 |
    r2
  }
  \tag #'oboe2 { R1*24 r2 }
>>
