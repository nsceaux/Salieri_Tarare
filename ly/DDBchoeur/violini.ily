\clef "treble" <>
<<
  \tag #'violino1 {
    mi''4\p re''8. mi''16 |
    do''4 r do''\f re''8. mi''16 |
    fa''2 <re'' sol'>4 q |
    <sol mi' do'' mi''>4. sol''8 fa''8. mi''16 re''8. do''16 |
    si'4 r sol''8*2/3[ la'' sol''] fa''[ mi'' re''] |
    do''4\p \grace re''8 do''8*2/3 si' do'' re''4 \grace mi''8 re''8*2/3 do'' re'' |
    mi''4. sol'8 sol''8*2/3[ la'' sol''] fa''[ mi'' re''] |
    do''4 \grace re''8 do''8*2/3 si' do'' re''4 \grace mi''8 re''8*2/3 do'' re'' |
    mi''8. sol'16 la'8. si'16 do''8. re''16 mi''8. fa''16 |
    sol''4. fa''8 mi''8. fa''16 sol''8. la''16 |
    \grace sol''8 fa'' mi''16 fa'' re''8 re'' \grace sol''8 fa'' mi''16 fa'' re''8 re'' |
    mi''4. fa''8 sol''8. mi''16 la''8. fa''16 |
    mi''8. do''16 mi''8. mi''16 mi''8. fa''16 re''8. mi''16 |
    do''4.\ff do''8 \grace sol'' fa''8*2/3[ mi'' fa''] re''[ mi'' fa''] |
    sol''8. fa''16 mi''8. mi''16 \grace sol''8 fa''8*2/3[ mi'' fa''] re''[ mi'' fa''] |
    mi''4. mi''8 <fa' do'' fa''>4 <sol' do'' mi''> |
    <fa' do'' fa''> <sol' do'' mi''> <fa' do'' fa''> <mi' do'' sol''> |
    <fa' do'' la''>2. fa''8.\p mi''16 |
    re''2. mi''8. fa''16 |
    sol''4. fa''8 mi''8. fa''16 sol''8. la''16 |
    \grace sol''8 fa'' mi''16 fa'' re''8 re'' \grace sol''8 fa'' mi''16 fa'' re''8.\ff re''16 |
    mi''8. do''16 mi''8. fa''16 sol''8. mi''16 la''8. fa''16 |
    mi''8*2/3[
  }
  \tag #'violino2 {
    do''4-\sug\p si'8. si'16 |
    do''4 r sol'-\sug\f fa'8. mi'16 |
    re'2 <sol sol'>4 <sol re' si'>4 |
    <sol mi' do''>4. mi'8 do''4 la'8. la'16 |
    re'4 r r2 |
    mi'8*2/3[-\sug\p sol' fa'] mi'[ re' do'] si[ re' do'] si[ la si] |
    do'4 r r2 |
    mi'8*2/3[ sol' fa'] mi'[ re' do'] si[ re' do'] si[ la si] |
    do'8. mi'16 fa'8. sol'16 la'8. si'16 do''8. re''16 |
    mi''4. re''8 do''8. re''16 mi''8. fa''16 |
    \grace mi''8 re'' do''16 re'' si'8 si' \grace mi''8 re'' do''16 re'' si'8 si' |
    do''8. sol'16 do''8. re''16 mi''8. do''16 do''8. do''16 |
    do''8. sol'16 do''8. do''16 do''8. re''16 si'8. si'16 |
    do''8*2/3[-\sug\ff <mi' sol> q] q[ q q] la'[ sol' la'] si'[ do'' re''] |
    mi''8. re''16 do''8. do''16 la'8*2/3[ sol' la'] si'[ do'' re''] |
    do''[ <do'' do'''> q] q[ q q] \tuplet 6/4 q2.:8 |
    \tuplet 6/4 q:8 \tuplet 6/4 q:8 |
    q2. re''8.-\sug\p do''16 |
    si'2. do''8. re''16 |
    mi''4. re''8 do''8. re''16 mi''8. fa''16 |
    \grace mi''8 re'' do''16 re'' si'8 si' \grace mi''8 re'' do''16 re'' si'8.-\sug\ff si'16 |
    do''8. sol'16 do''8. re''16 mi''8. do''16 do''8. do''16 |
    do''8*2/3[
  }
>> <mi'' do'''>8*2/3 q] q[ q q] q[ q q] <re'' si''>[ q q] |
<<
  \tag #'violino1 {
    <do'' do'''>4. sol''8 si''4.\trill la''16 sol'' |
    do'''4 sol''8. sol''16 si''4.\trill la''16 sol'' |
    do'''4.
  }
  \tag #'violino2 {
    <sol' mi'' do'''>8*2/3[ <sol' mi''> q] q[ q q] \tuplet 6/4 <sol' fa''>2.:8 |
    \tuplet 6/4 <sol' mi''>:8 \tuplet 6/4 <sol' fa''>:8 |
    <mi' sol>4.
  }
>> do'8 mi'4.\trill re'16 do' |
fa'8. fa'16 sol'8. la'16 sol'4 sol |
do'4 sol do'8. <<
  \tag #'violino1 {
    do''16\p do''8. re''16 |
    mi''2 mi''8. mi''16 sol''8. mi''16 |
    re''2~ re''8. re''16 re''8. sol''16 |
    fad''8.( sol''16) mi''8.( fad''16) re''8.( mi''16) do''8.( re''16) |
    si'4 r re''16 do'' si'8 si'8. si'16 |
    la'4 r sol''16 fad'' mi''8 mi''8. mi''16 |
    fad''4 r re''16 do'' si'8 si'8. si'16 |
    la'4 r sol''16 fad'' mi''8 mi''8. mi''16 |
    fad''8\mf fad''16([ sol''] la'') la'' la'' la'' do''!4\p do''8. do''16 |
    do''4 do'' do'' do'' |
    do''8 fad'' fad''4 fad''8\mf( sol''16 fad'' mi''8 re'') |
    re''8( sol'') sol'' sol'' fad''( mi'' re'' do'') |
    si'2. \grace si'8 la' sol'16 la' |
    sol'4 r re'''16 do''' si''8 si''8. si''16 |
    la''4 r sol''16 fad'' mi''8 mi''8. mi''16 |
    fad''4 r re'''16 do''' si''8 si''8. si''16 |
    la''4 r sol''16 fad'' mi''8 mi''8. mi''16 |
    fad''8 fad''16\mf[ sol''] la'' la'' la'' la'' do''4\p do''8. do''16 |
    do''4 do'' do'' do'' |
    do''8( fad'') fad''4 fad''8\mf( sol''16 fad'' mi''8 re'') |
    re''8(\p sol'') sol'' sol'' fad''( mi'' re'' do'') |
    si'2. \grace si'8 la' sol'16 la' |
    si'8\f re''16 mi'' re''8 sol'' fad''( sol''16 fad'' mi''8 re'') |
    sol'' sol''([ si'' sol''] fad'' mi'' re'' do'') |
    si'2. \grace si'8 la' sol'16 la' |
    sol'2\fermata
  }
  \tag #'violino2 {
    mi'16-\sug\p mi'8. sol'16 |
    do''2 do''8. do''16 mi''8. do''16 |
    si'2~ si'8. si'16 si'8. si'16 |
    la'4( sol' fad' la') |
    sol'4 r si'16 la' sol'8 sol'8. sol'16 |
    fad'4 r mi''16 re'' dod''8 dod''8. dod''16 |
    re''4 r si'16 la' sol'8 sol'8. sol'16 |
    fad'4 r mi''16 re'' dod''8 dod''8. dod''16 |
    re''4 r fad'-\sug\p sol'8. sol'16 |
    la'4-! sol'-! la'-! sol'-! |
    fad'8 la' la'4 la'8-\sug\mf( si'16 la' sol'8 fad') |
    sol'4 sol'8 sol' sol'( do'' si' la') |
    sol'2. \grace sol'8 fad' mi'16 fad' |
    sol'4 r si'16 la' sol'8 sol'8. sol'16 |
    fad'4 r mi'16 re' dod'8 dod'8. dod'16 |
    re'4 r si'16 la' sol'8 sol'8. sol'16 |
    fad'4 r mi'16 re' dod'8 dod'8. dod'16 |
    re'4 r fad'-\sug\p sol'8. sol'16 |
    la'4 sol' la' sol' |
    fad'8 la' la'4 la'8-\sug\mf( si'16 la' sol'8 fad') |
    sol'2:8-\sug\p sol'8( do'' si' la') |
    sol'2. \grace sol'8 fad' mi'16 fad' |
    sol'8-\sug\f si'16 do'' si'8 si' la'4( sol'8 fad') |
    sol'8 re'' re'' re'' re''( do'' si' la') |
    sol'2. \grace sol'8 fad' mi'16 fad' |
    sol'2\fermata
  }
>>
