<<
  \tag #'(vdessus basse) {
    \clef "soprano/treble" mi''4^\p ^"chanté" re''8. mi''16 |
    do''4 r do''^\f re''8. mi''16 |
    fa''2 re''4 re'' |
    mi''4. sol''8 fa''8.[ mi''16] re''8. do''16 |
    si'4 sol'8 r r2 |
    R1 |
    r4 r8 <<
      { \voiceOne sol'8 sol'4 sol' |
        do''4. do''8 re''4. re''8 |
        mi''2. mi''8. fa''16 |
        sol''4. fa''8 mi''8.[ fa''16] sol''8. la''16 |
        fa''4 re'' \oneVoice r4 \voiceOne re''8. re''16 |
        mi''4. fa''8 sol''8.[ mi''16] la''8. fa''16 |
        mi''2.( re''4) |
        do''4. \oneVoice
      }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sol'8 sol'4 sol' |
        mi'4. do''8 si'4. si'8 |
        do''2. do''8. re''16 |
        mi''4. re''8 do''8.[ re''16] mi''8. fa''16 |
        re''4 si' s4 si'8. si'16 |
        do''4. re''8 mi''8.[ do''16] do''8. do''16 |
        do''2.( si'4) |
        do''4.
      }
    >> do''8 fa''4 re'' |
    sol'' mi'' fa'' re'' |
    mi''4. mi''8 fa''4 mi'' |
    fa'' mi'' fa'' sol'' |
    la''2. <<
      { \voiceOne fa''8.^\p mi''16 |
        re''2. mi''8. fa''16 |
        sol''4. fa''8 mi''8.[ fa''16] sol''8. la''16 |
        fa''4 re'' \oneVoice r4 \voiceOne re''8.^\ff re''16 |
        mi''4. fa''8 sol''8.[ mi''16] la''8. fa''16 |
        mi''2.( re''4) |
        do''4 \oneVoice
      }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo re''8. do''16 |
        si'2. do''8. re''16 |
        mi''4. re''8 do''8.[ re''16] mi''8. fa''16 |
        re''4 si' s si'8. si'16 |
        do''4. re''8 mi''8.[ do''16] do''8. do''16 |
        do''2.( si'4) |
        do''4
      }
    >> r4 r2 |
    R1*3
  }
  \tag #'vhaute-contre {
    \clef "alto/G_8" sol'4^\sug\p sol'8. sol'16 |
    sol'4 r sol'^\sug\f fa'8. mi'16 |
    re'2 sol'4 sol' |
    sol'4. mi'8 do'4 la'8. la'16 |
    re'4 si8 r r2 |
    R1 |
    r4 r8 sol sol4 sol |
    do'4. do'8 re'4. re'8 |
    mi'2. mi'8. fa'16 |
    sol'4. fa'8 mi'8.[ fa'16] sol'8. la'16 |
    fa'4 re' r4 re'8. re'16 |
    mi'4. fa'8 sol'8.[ mi'16] la'8. fa'16 |
    mi'2.( re'4) |
    do'4. do'8 la'4 sol' |
    sol' sol' la' sol' |
    sol'4. sol'8 fa'4 sol' |
    fa' sol' fa' do' |
    fa'2 r |
    r r4 mi'8. fa'16 |
    sol'4. fa'8 mi'8.[ fa'16] sol'8. la'16 |
    fa'4 re' r4 re'8.^\sug\ff re'16 |
    mi'4. fa'8 sol'8.[ mi'16] la'8. fa'16 |
    mi'2.( re'4) |
    do'4 r r2 |
    R1*3
  }
  \tag #'vtaille {
    \clef "tenor/G_8" do'4^\sug\p si8. si16 |
    do'4 r sol'^\sug\f si8. do'16 |
    sol2 sol4 si |
    do'4. do'8 do'4 la8. re'16 |
    re'4 re'8 r r2 |
    R1 |
    r4 r8 sol sol4 sol |
    mi4. do'8 si4. si8 |
    do'2. do'8. re'16 |
    mi'4. re'8 do'8.[ re'16] mi'8. fa'16 |
    re'4 si r4 si8. si16 |
    do'4. re'8 mi'8.[ do'16] do'8. do'16 |
    do'2.( si4) |
    do'4. do'8 fa'4 fa' |
    mi' do' fa' fa' |
    mi'4. do'8 do'4 do' |
    do' do' do' do' |
    do'2 r |
    r r4 do'8. re'16 |
    mi'4. re'8 do'8.[ re'16] mi'8. fa'16 |
    re'4 si r4 si8.^\ff si16 |
    do'4. re'8 mi'8.[ do'16] do'8. do'16 |
    do'2.( si4) |
    do'4 r r2 |
    R1*3
  }
  \tag #'vbasse {
    \clef "bass" do'4^\sug\p sol8. sol16 |
    mi4 r mi'^\f re'8. do'16 |
    si2 si4 sol |
    do'4. do'8 la8.[ sol16] fa8. fad16 |
    sol4 sol8 r r2 |
    R1 |
    r4 r8 sol sol4 sol |
    do'4. do'8 sol4. sol8 |
    do2. do'8. do'16 |
    do'4. do'8 do'4 do'8. do'16 |
    sol4 sol r2 |
    R1 |
    r2 r4 r8 sol |
    do'4. do'8 do'4 do' |
    do' do' do' do' |
    do'4. do'8 la4 do' |
    la do' la mi |
    fa2 r |
    r r4 do'8. do'16 |
    do'4. do'8 do'4 do'8 do' |
    sol4 sol r4 sol8.^\sug\ff sol16 |
    do'4. do'8 do'4 fa8. fa16 |
    sol1 |
    do4 r r2 |
    R1*3
  }
  \tag #'bergere1 \clef "soprano/treble"
  \tag #'bergere2 \clef "soprano/treble"
  \tag #'berger \clef "bass"
  \tag #'(bergere1 bergere2 berger tt) { r2 R1*26 }
>>
<<
  \tag #'(vdessus vhaute-contre vtaille vbasse) { R1*25 r2 }
  \tag #'(bergere1 tt basse) {
    r2 r8*3/2 <>^\markup\character Spinette do''8*1/2 do''8*3/2 re''8*1/2 |
    mi''2 mi''4 sol''8 mi'' |
    re''2. re''8 sol'' |
    fad''4 mi'' re'' do''8 do'' |
    si'4 si' r r8 si' |
    la'2 r2 |
    R1 |
    r2 mi''4 mi''8. mi''16 |
    fad''2 do''!4 do''8. do''16 |
    do''4 do'' do'' do'' |
    do''8[ fad''] fad''4 fad'' mi''8 re'' |
    sol''4 sol'' fad''8[ mi''] re''[ do''] |
    si'2.( la'4) |
    sol'2 r4 r8 si' |
    la'2 r |
    R1 |
    r2 mi''4 mi''8. mi''16 |
    fad''2 do''!4 do''8. do''16 |
    do''4 do'' do'' do'' |
    do''8[ fad''] fad''4 fad'' mi''8 re'' |
    sol''4 sol'' fad''8[ mi''] re''[ do''] |
    si'2.( la'4) |
    si'2 fad''4 mi''8 re'' |
    sol''4 sol'' fad''8[ mi''] re''[ do''] |
    si'2. la'4 |
    sol'2\fermata
  }
  \tag #'bergere2 {
    r2 r8*3/2 <>^\markup\character La Bergère sensible mi'8*1/2 mi'8*3/2 sol'8*1/2 |
    do''2 do''4 mi''8 do'' |
    si'2. si'8 si' |
    la'4 sol' fad' la'8 la' |
    sol'4 sol' r r8 sol' |
    fad'2 r2 |
    R1 |
    r2 dod''4 dod''8. dod''16 |
    re''2 fad'4 sol'8. sol'16 |
    la'4 sol' la' sol' |
    fad'8[ la'] la'4 la' sol'8 fad' |
    sol'4 sol' sol'8[ do''] si'[ la'] |
    sol'2.( fad'4) |
    sol'2 r4 r8 sol' |
    fad'2 r |
    R1 |
    r2 dod''4 dod''8. dod''16 |
    re''2 fad'4 sol'8. sol'16 |
    la'4 sol' la' sol' |
    fad'8[ la'] la'4 la' sol'8 fad' |
    sol'4 sol' sol'8[ do''] si'[ la'] |
    sol'2.( fad'4) |
    sol'2 la'4 sol'8 fad' |
    sol'4 sol' sol'8[ do''] si'8[ la'] |
    sol'2. fad'4 |
    sol'2\fermata
  }
  \tag #'berger {
    R1*5 |
    r2 r4 r8 la, |
    re2 r |
    R1*7 |
    r2 r4 r8 la, |
    re2 r |
    R1*9 |
    r2
  }
>>