\clef "alto" sol'4\p sol'8. sol'16 |
sol'4 r sol'\f si8. do'16 |
sol2 si4 sol |
do'4. do'8 la8. sol16 fa8. fad16 |
sol4 r r2 |
mi'8*2/3[\p sol' fa'] mi'[ re' do'] si[ re' do'] si[ la si] |
do'4 r r2 |
mi'8*2/3[ sol' fa'] mi'[ re' do'] si[ re' do'] si[ la si] |
do'4 r r2 |
r4 do' do do' |
r sol sol sol |
do'8. do'16 do'8. do'16 do'4 fa |
sol sol sol sol |
do8*2/3[-\sug\ff do' do'] do'[ do' do'] \tuplet 6/4 do'2.:8 |
\tuplet 6/4 do'2.:8 \tuplet 6/4 do'2.:8 |
\tuplet 6/4 do'2.:8 \tuplet 6/4 do'2.:8 |
\tuplet 6/4 do'2.:8 \tuplet 6/4 do'2.:8 |
do'2. re8.\p re16 |
sol2 r |
r4 do' do do' |
r sol sol sol8.\ff sol16 |
do'8. do16 do'8. do'16 do'8. do'16 fa8. fa16 |
\tuplet 6/4 sol2.:8 \tuplet 6/4 sol2.:8 |
\tuplet 6/4 <mi' do''>:8 \tuplet 6/4 <fa' re''>:8 |
\tuplet 6/4 <mi' do''>:8 \tuplet 6/4 <fa' re''>:8 |
<mi' do''>4. do'8 mi'4.\trill re'16 do' |
fa'8. fa'16 sol'8. la'16 sol'4 sol |
do' sol do4*3/4 s16 r4 |
r8. do'16-\sug\p sol8. mi16 do4 r |
r8. sol'16 re'8. si16 sol4 sol |
re'2 re |
sol4 r sol sol8. sol16 |
re'4 r la la8. la16 |
re4 r sol sol8. sol16 |
re'4 r la la8. la16 |
re'4 r re'-\sug\p mi'8. mi'16 |
fad'4 mi' fad' mi' |
re'2 do'4-\sug\mf do' |
si4 si do' do' |
re'8 re' re' re' re'2:8 |
sol4 r sol sol8. sol16 |
re'4 r la la8. la16 |
re4 r r sol |
re' r r la |
re r re-\sug\p mi8. mi16 |
fad'4 mi' fad' mi' |
re' re' do'!\mf do' |
si\p si do' do' |
re'2:8 re':8 |
sol':8\f do'4 do'' |
si'8 si si si do'2:8 |
re':8 re':8 |
sol'2\fermata
