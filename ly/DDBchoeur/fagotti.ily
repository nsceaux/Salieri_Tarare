\clef "bass" do'8-\sug\p r sol r |
mi4 r mi\f re8. do16 |
si,2 si,4 sol, |
do4. do8 la8. sol16 fa8. fad16 |
sol4 sol, r2 |
R1*7 |
r2 r4 r8 sol,-\sug\ff |
do1~ |
do |
do4. do'8 la4 do' |
la do' la mi |
fa2 r |
R1*2 | \allowPageTurn
r2 r4 sol,-\sug\ff |
do4. do8 do4 fa |
sol1 |
do'~ |
do' |
do'4. do8 mi4.\trill re16 do |
fa8. fa16 sol8. la16 sol4 sol, |
do sol, do,4*3/4 s16 \allowPageTurn r4 |
R1*24 |
r2
