\score {
  <<
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      { s2 s1*26 s2 s8.\break }
      \global \keepWithTag #'tt \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'tt \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiri } <<
      \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
      \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
    >>
  >>
  \layout { }
}
