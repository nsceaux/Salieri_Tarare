\clef "bass" r2 |
r do4-\sug\f do8. do16 |
sol,2 sol,4 sol, |
do4. do8 do4 do8. do16 |
sol,4 sol, r2 |
R1*7 |
r2 r4 r8 sol,-\sug\ff |
do2 r |
R1 |
r8 do16 do do8 do do4 do |
do do do do |
do2 r |
R1*2 |
r2 r4 sol,8.-\sug\ff sol,16 |
do8. do16 do8. do16 do4 do8. do16 |
sol,4 sol,8. sol,16 sol,4 sol, |
do4. do8 sol,4. sol,8 |
do4. do8 sol,4. sol,8 |
do4 r r2 |
r2 r4 sol, |
do sol, do4*3/4 s16 r4 |
R1*24 |
r2
