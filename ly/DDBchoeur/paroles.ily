\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Peu -- ple lé -- ger mais gé -- né -- reux,
  nous blâ -- mons les mœurs de l’A -- si -- e :
  ja -- mais, dans nos cli -- mats heu -- reux,
  \tag #'(vdessus vhaute-contre vtaille basse) {
    la beau -- té ne tremble as -- ser -- vi -- e,
  }
  la beau -- té ne tremble as -- ser -- vi -- e.
  Ja -- mais, \tag #'vbasse { ja -- mais, } dans nos cli -- mats heu -- reux,
  ja -- mais, dans nos cli -- mats heu -- reux,
  \tag #'(vdessus basse) { la beau -- té }
  la beau -- té ne tremble as -- ser -- vi -- e,
  la beau -- té ne tremble as -- ser -- vi -- e.
}

\tag #'(bergere1 bergere2 tt basse) {
  Chez nos ma -- ris, pres -- qu’à leurs yeux,
  un ga -- lant en fait son a -- mi -- e ;
  la prend,
}
\tag #'(berger) {
  la rend,
}
\tag #'(bergere1 bergere2 tt basse) {
  rit a -- vec eux,
  et porte ail -- leurs sa douce en -- vi -- e,
  et porte ail -- leurs sa douce en -- vi -- e.
  La prend,
}
\tag #'(berger) {
  la rend,
}
\tag #'(bergere1 bergere2 tt basse) {
  rit a -- vec eux,
  et porte ail -- leurs sa douce en -- vi -- e,
  et porte ail -- leurs sa douce en -- vi -- e,
  et porte ail -- leurs sa douce en -- vi -- _ e.
}
