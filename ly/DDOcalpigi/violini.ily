\clef "treble" r4^"Pizzi." sol'8 <sol mi' do''>4 <mi' do''>8 |
q4 q8 <sol' re''>4 sol''8 |
mi'' do'' sol' <sol mi' do''>4 <mi' do''>8 |
q4 q8 <re'' sol'>4 sol''8 |
mi''4 r8 <<
  \tag #'violino1 {
    sol'4 sol'8 |
    la'4 la'8 la'4 la'8 |
    sol'4 sol'8
  }
  \tag #'violino2 {
    mi'4 mi'8 |
    fa'4 fa'8 fa'4 fa'8 |
    mi'4 mi'8
  }
>> fa'8 la' fa' |
mi' sol' mi' re' fa' re' |
mi'4 sol'8 <<
  \tag #'violino1 {
    do''4 do''8 |
    do''4 do''8 re''4 sol''8 |
    mi'' do'' sol' do''4 do''8 |
    do''4 do''8 re''4 sol''8 |
    mi'' do'' mi'' re''4 mi''8 |
    re''4 mi''8 re''4 sol''8 |
    re''4 r8 re''4 re''8 |
    re'' mi'' do'' si' do'' la' |
    si' sol' si' re''4 re''8 |
    re'' mi'' do'' si' do'' la' |
  }
  \tag #'violino2 {
    <sol mi'>4 q8 |
    q4 q8 <sol re'>4 q8 |
    <sol mi'>4 q8 q4 q8 |
    q4 q8 <sol re'>4 q8 |
    <sol mi'>4 sol'8 sol'4 sol'8 |
    sol'4 sol'8 sol'4 si'8 |
    sol'4 r8 sol'4 sol'8 |
    sol'4 la'8 sol' la' fad' |
    sol' si re' sol'4 sol'8 |
    sol'4 la'8 sol' la' fad' |
  }
>>
sol'4. <re' si' sol''>4\f^"arco" <si' sol''>8 |
<sol mi' do'' mi''>8 re''16 do'' si' la'32 sol' <fad' la>4 q8 |
<si sol'>4 r8 <>^"pizzicato" <<
  \tag #'violino1 {
    dod''4 dod''8 |
    re''4 re''8 mi''4 mi''8 |
    fa'' mi'' re'' do''4 si'8 |
    do''4 do''8 re''4 re''8 |
    mi'' do'' sol'' fad''4 sol''8 |
    fa''!4 sol''8 mi''4 sol''8 |
    re''4.
  }
  \tag #'violino2 {
    sol'4 sol'8 |
    fa'4 fa'8 mi'4 mi'8 |
    re' mi' fa' sol'4 sol'8 |
    sol'4 sol'8 sol'4 sol'8 |
    sol' mi' mi'' do''4. |
    re'' do''4 mi''8 |
    si'4.
  }
>> do''4 do''8 |
do''8. si'16 la'8 sol'4 sol'8 |
sol'4. <<
  \tag #'violino1 {
    mi''4 mi''8 |
    mi''8. fa''16 sol''8 do''4 si'8 |
    do''4. \grace do''8 do'''4\f^"arco" do'''8 |
    do'''8. si''16 la''8 sol''4 sol''8 |
    sol''4. <sol mi' do'' mi''>4 <do'' mi''>8 |
    mi''8. fa''16 sol''8 <do'' mi'>4 <re' si'>8 |
    <do'' mi'>4.
  }
  \tag #'violino2 {
    sol'4 sol'8 |
    sol'8. fa'16 mi'8 mi'4 re'8 |
    mi'4. do''4\f^"arco" do''8 |
    do''8. si'16 la'8 sol'4 sol'8 |
    sol'4. <do'' mi' sol>4 sol'8 |
    sol'8. fa'16 mi'8 <mi' sol>4 <sol re'>8 |
    <sol mi'>4.
  }
>> \grace re'''8 do'''\ff si'' do''' |
\grace re''' do''' si'' do''' \grace re''' do''' si'' do''' |
\grace re''' do''' si'' do''' la'' sol''16 fa'' mi'' re'' |
sol''8 fa''16 mi'' re'' do'' fa''8 mi''16 re'' do'' si' |
do''4. <>^"pizzicato" <<
  \tag #'violino1 {
    sol'4 sol'8 |
    la'4 la'8 la'4 la'8 |
    sol'4 sol'8
  }
  \tag #'violino2 {
    mi'4 mi'8 |
    fa'4 fa'8 fa'4 fa'8 |
    mi'4 mi'8
  }
>> fa'8 la' fa' |
mi' sol' mi' re' fa' re' |
do'4 <<
  \tag #'violino1 {
    sol'8 do''4 do''8 |
    do''4 do''8 re''4 sol''8 |
    mi'' do'' sol' do''4 do''8 |
    do''4 do''8 re''4 sol''8 |
    mi'' do'' mi'' re''4 mi''8 |
    re''4 mi''8 re''4 sol''8 |
    re''4. re''4 re''8 |
    re'' mi'' do'' si' do'' la' |
    si'4. re''4 re''8 |
    re'' mi'' do'' si' do'' la' |
  }
  \tag #'violino2 {
    <mi' sol>8 q4 q8 |
    q4 q8 <sol re'>4 q8 |
    <mi' sol>4 q8 q4 q8 |
    q4 q8 <sol re'>4 q8 |
    <sol mi'>4 sol'8 sol'4 sol'8 |
    sol'4 sol'8 sol'4 si'8 |
    sol'4. sol'4 sol'8 |
    sol'4 la'8 sol' la' fad' |
    sol'4. sol'4 sol'8 |
    sol'4 la'8 sol' la' fad' |
  }
>>
sol'4. <re' si' sol''>4\f^"arco" <si' sol''>8 |
<mi'' do'' mi' sol> re''16 do'' si' la'32 sol' <fad' la>4 q8 |
<si sol'>4 r8 <>^"pizzicato" <<
  \tag #'violino1 {
    dod''4 dod''8 |
    re''4 re''8 mi''4 mi''8 |
    fa'' mi'' re'' do''4 si'8 |
    do''4 do''8 re''4 re''8 |
    mi'' do'' sol'' fad''4 sol''8 |
    fa''!4 sol''8 mi''4 sol''8 |
    re''4.
  }
  \tag #'violino2 {
    sol'4 sol'8 |
    fa'4 fa'8 mi'4 mi'8 |
    re' mi' fa' sol'4 sol'8 |
    sol'4 sol'8 sol'4 sol'8 |
    sol' mi' mi'' do''4. |
    re'' do''4 mi''8 |
    si'4.
  }
>> do''4 do''8 |
do'' si' la' sol' la' fad' |
sol'4. <<
  \tag #'violino1 {
    mi''4 mi''8 |
    mi'' fa'' sol'' do'' re'' si' |
    do''4. \grace do''8 do'''4.\f ^"arco"~ |
    do'''8 si'' la'' sol'' la'' fad'' |
    sol''4. <mi'' do'' mi' sol>~ |
    mi''8 fa'' sol'' do'' re'' si' |
    do''4.
  }
  \tag #'violino2 {
    sol'4 sol'8 |
    sol' fa' mi' mi' fa' re' |
    mi'4. do''-\sug\f ^"arco"~ |
    do''8 si' la' sol' la' fad' |
    sol'4. <sol' sol>~ |
    sol'8 fa' mi' mi' fa' re' |
    mi'4.
  }
>> \grace re'''8 do''' si'' do''' |
\grace re''' do''' si'' do''' \grace re''' do''' si'' do''' |
\grace re''' do''' si'' do''' la'' sol''16 fa'' mi'' re'' |
sol''8 fa''16 mi'' re'' do'' fa''8 mi''16 re'' do'' si' |
do''4 r8 <>^"pizzicato" <<
  \tag #'violino1 {
    sol'4 sol'8 |
    la'4 la'8 la'4 la'8 |
    sol'4 sol'8 fa' la' fa' |
    mi' sol' mi' re' fa' re' |
    mi'4 sol'8 do''4 do''8 |
    do''4 do''8 re''4 sol''8 |
    mi'' do'' sol' do''4 do''8 |
    do''4 do''8 re''4 sol''8 |
    mi'' do'' mi'' re''4 mi''8 |
    re''4 mi''8 re''4 sol''8 |
    re''4. re''4 re''8 |
    re'' mi'' do'' si'4 la'8 |
    si'4. re''4 re''8 |
    re'' mi'' do'' si'4 la'8 |
    sol'4.
  }
  \tag #'violino2 {
    mi'4 mi'8 |
    fa'4 fa'8 fa'4 fa'8 |
    mi'4 mi'8 fa' la' fa' |
    mi' sol' mi' re' fa' re' |
    do'4 <mi' sol>8 q4 q8 |
    <mi' sol>4 q8 <sol re'>4 q8 |
    <mi' sol>4 q8 q4 q8 |
    q4 q8 <sol re'>4 q8 |
    <mi' sol>4 sol'8 sol'4 sol'8 |
    sol'4 sol'8 sol'4 si'8 |
    sol'4. sol'4 sol'8 |
    sol'4 la'8 sol'4 fad'8 |
    sol'4. sol'4 sol'8 |
    sol'4 la'8 sol'4 fad'8 |
    sol'4.
  }
>> <re' si' sol''>4\f ^"arco" <si' sol''>8 |
<mi'' do'' mi' sol>8 re''16 do'' si' la'32 sol' <fad' la>4 q8 |
<si sol'>4. <>^"pizzicato" <<
  \tag #'violino1 {
    dod''4 dod''8 |
    re''4 re''8 mi''4 mi''8 |
    fa'' mi'' re'' do''4 si'8 |
    do''4 do''8 re''4 re''8 |
    mi'' do'' sol'' fad''4 sol''8 |
    fa''!4 sol''8 mi''4 sol''8 |
    re''4.
  }
  \tag #'violino2 {
    sol'4 sol'8 |
    fa'4 fa'8 mi'4 mi'8 |
    re' mi' fa' sol'4 sol'8 |
    sol'4 sol'8 sol'4 sol'8 |
    sol' mi' mi'' do''4. |
    re'' do''4 mi''8 |
    si'4.
  }
>> do''4 do''8 |
do'' si' la' sol' la' fad' |
sol'4. <<
  \tag #'violino1 {
    mi''4 mi''8 |
    mi'' fa'' sol'' do'' re'' si' |
    do''4. \grace do''8 do'''4.\f ^"arco"~ |
    do'''8 si'' la'' sol'' la'' fad'' |
    sol''4. <mi'' do'' mi' sol>~ |
    mi''8 fa'' sol'' do'' re'' si' |
    do''4.
  }
  \tag #'violino2 {
    sol'4 sol'8 |
    sol' fa' mi' mi' fa' re' |
    mi'4. do''-\sug\f ^"arco"~ |
    do''8 si' la' sol' la' fad' |
    sol'4. <sol' sol>~ |
    sol'8 fa' mi' mi' fa' re' |
    mi'4.
  }
>> \grace re'''8 do''' si'' do''' |
\grace re''' do''' si'' do''' \grace re''' do''' si'' do''' |
\grace re''' do''' si'' do''' la'' sol''16 fa'' mi'' re'' |
sol''8 fa''16 mi'' re'' do'' fa''8 mi''16 re'' do'' si' |
do''4. <>^\markup\whiteout "[pizzicato]" <<
  \tag #'violino1 {
    sol'4 sol'8 |
    la'4 la'8 la'4 la'8 |
    sol'4 sol'8
  }
  \tag #'violino2 {
    mi'4 mi'8 |
    fa'4 fa'8 fa'4 fa'8 |
    mi'4 mi'8
  }
>> fa'8 la' fa' |
mi' sol' mi' re' fa' re' |
<<
  \tag #'violino1 {
    mi'4 sol'8 do''4 do''8 |
    do''4 do''8 re''4 sol''8 |
    mi'' do'' sol' do''4 do''8 |
    do''4 do''8 re''4 sol''8 |
    mi'' do'' mi'' re''4 mi''8 |
    re''4 mi''8 re''4 sol''8 |
    re''4 r8 re''4 re''8 |
    re'' mi'' do'' si' do'' la' |
    si'4. re''4 re''8 |
    re'' mi'' do'' si' do'' la' |
  }
  \tag #'violino2 {
    do'4 <sol mi'>8 q4 q8 |
    q4 q8 <sol re'>4 q8 |
    <sol mi'>4 q8 q4 q8 |
    q4 q8 <sol re'>4 q8 |
    <sol mi'>4 sol'8 sol'4 sol'8 |
    sol'4 sol'8 sol'4 si'8 |
    sol'4 r8 sol'4 sol'8 |
    sol'4 la'8 sol' la' fad' |
    sol'4. sol'4 sol'8 |
    sol'4 la'8 sol' la' fad' |
  }
>>
<<
  \tag #'violino1 {
    r8 re'''16\f dod''' re''' dod''' re''' dod''' \rt#4 { re'''16 dod''' } |
    re'''4
  }
  \tag #'violino2 {
    r8 si''16 lad'' si'' lad'' si'' lad'' \rt#4 { si'' lad'' } |
    si''4
  }
>> r4 r2 |
R1 |
<<
  \tag #'violino1 {
    mi''4 r do'' r |
    re''4
  }
  \tag #'violino2 {
    \allowPageTurn si'4 r mi' r |
    sol'4
  }
>> r4 r2 |
<<
  \tag #'violino1 { mi''4 }
  \tag #'violino2 { sol'4 }
>> r8 <>^"pizzicato" do''4 do''8 |
do'' si' la' sol' la' fad' |
sol'4. <<
  \tag #'violino1 {
    mi''4 mi''8 |
    mi'' fa'' sol'' do'' re'' si' |
    do''4 r8 \grace do'' do'''4.-\sug\f ^"tutti arco" ~ |
    do'''8 si'' la'' sol'' la'' fad'' |
    sol''4. <mi'' do'' mi' sol>4. |
    mi''8 fa'' sol'' do'' re'' si' |
    do''4.
  }
  \tag #'violino2 {
    sol'4 sol'8 |
    sol' fa' mi' mi' fa' re' |
    mi'4. do''-\sug\f ^"arco" ~ |
    do''8 si' la' sol' la' fad' |
    sol'4. <sol' sol> |
    sol'8 fa' mi' mi' fa' re' |
    mi'4.
  }
>> \grace re'''8 do''' si'' do''' |
\grace re''' do''' si'' do''' \grace re''' do''' si'' do''' |
\grace re''' do''' si'' do''' la'' sol''16 fa'' mi'' re'' |
sol''8 fa''16 mi'' re'' do'' fa''8 mi''16 re'' do'' si' |
do''4. <>^"pizzicato" <<
  \tag #'violino1 {
    sol'4 sol'8 |
    la'4 la'8 la'4 la'8 |
    sol'4 sol'8
  }
  \tag #'violino2 {
    mi'4 mi'8 |
    fa'4 fa'8 fa'4 fa'8 |
    mi'4 mi'8
  }
>> fa'8 la' fa' |
mi' sol' mi' re' fa' re' |
<<
  \tag #'violino1 {
    mi'4 sol'8 do''4 do''8 |
    do''4 do''8 re''4 sol''8 |
    mi'' do'' sol' do''4 do''8 |
    do''4 do''8 re''4 sol''8 |
    mi'' do'' mi'' re''4 mi''8 |
    re''4 mi''8 re''4 mi''8 |
    re''4. re''4 re''8 |
    re'' mi'' do'' si' do'' la' |
    si'4. re''4 re''8 |
    re'' mi'' do'' si' do'' la' |
    sol'4.
  }
  \tag #'violino2 {
    do'4 <sol mi'>8 q4 q8 |
    q4 q8 <sol re'>4 q8 |
    <sol mi'>4 q8 q4 q8 |
    q4 q8 <sol re'>4 q8 |
    <sol mi'>4 sol'8 sol'4 sol'8 |
    sol'4 sol'8 sol'4 si'8 |
    sol'4. sol'4 sol'8 |
    sol'4 la'8 sol' la' fad' |
    sol'4. sol'4 sol'8 |
    sol'4 la'8 sol' la' fad' |
    sol'4.
  }
>> <re' si' sol''>4\f ^"arco" <si' sol''>8 |
<mi'' do'' mi' sol>8 re''16 do'' si' la'32 sol' <fad' la>4 q8 |
<si sol'>4. <>^"pizzicato" <<
  \tag #'violino1 {
    dod''4 dod''8 |
    re''4 re''8 mi''4 mi''8 |
    fa'' mi'' re'' do''4 si'8 |
    do''4
  }
  \tag #'violino2 {
    sol'4 sol'8 |
    fa'4 fa'8 mi'4 mi'8 |
    re' mi' fa' sol'4 sol'8 |
    sol'4
  }
>> do''8 re''4 mi''8 |
fa''4. r4
