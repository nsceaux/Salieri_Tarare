\clef "tenor" R2.*14 |
r4 r8 <>^"Solo" si4.~ |
si8 do' la sol la fad |
sol4. si~ |
si8 do' la sol la fad |
sol4. \clef "bass" <>^"[tutti]" sol4\f sol8 |
do4 do8 re4 re8 |
sol,4 r8 r4 r8 |
R2.*7 |
\clef "tenor" r4 r8 do'4 do'8 |
do' re' mi' mi'4 re'8 |
do'4. \clef "bass" do4\f do8 |
do8. si,16 la,8 sol,4 sol,8 |
sol,4. do4 do8 |
do4 do8 sol4 sol,8 |
do4 do8\ff do4 do8 |
do4 do8 do4 do8 |
do4 do8 \clef "tenor" fa la fa' |
mi' sol' mi' re' fa' re' |
mi'4. r4 r8 |
R2.*9 |
r4 r8 si4.~ |
si8 do' la sol la fad |
sol4. si~ |
si8 do' la sol la fad |
sol4. \clef "bass" sol,4\f sol,8 |
do4 do8 re4 re8 |
sol,4 r8 r4 r8 |
R2.*7 |
\clef "tenor" r4 r8 do'4.~ |
do'8 re' mi' mi' fa' re' |
do'4. \clef "bass" do'4\f do'8 |
do' si la sol la fad |
sol4. do4 do8 |
do4 do8 sol4 sol,8 |
do4 do8 do4 do8 |
do4 do8 do4 do8 |
do4 do8 \clef "tenor" fa la fa' |
mi' sol' mi' re' fa' re' |
mi'4. r4 r8 |
R2.*9 |
r4 r8 <>^"Solo" \grace { sol16 la } si4 si8 |
si8. do'16 la8 sol4 fad8 |
sol4. \grace { sol16 la } si4 si8 |
si8. do'16 la8 sol4 fad8 |
sol4. \clef "bass" sol4\f sol8 |
do4 do8 re4 re8 |
sol,4 r8 r4 r8 |
R2.*7 |
\clef "tenor" r4 r8 do'4.~ |
do'8 re' mi' mi' fa' re' |
do'4. \clef "bass" do'4\f do'8 |
do' si la sol la fad |
sol4. do4 do8 |
do4 do8 sol4 sol,8 |
do4 do8 do4 do8 |
do4 do8 do4 do8 |
do4 do8 \clef "tenor" fa la fa' |
mi' sol' mi' re' fa' re' |
mi'4. r4 r8 |
R2.*9 |
r4 r8 <>^"Solo" si4.~ |
si8 do' la sol la fad |
sol4. si~ |
si8 do' la sol la fad |
sol4 r r2 |
R1*4 |
R2.*2 |
r4 r8 do'4.~ |
do'8 re' mi' mi' fa' re' |
do'4. \clef "bass" do'4-\sug\f do'8 |
do' si la sol la fad |
sol4. do4 do8 |
do4 do8 sol4 sol,8 |
do4 do8 do4 do8 |
do4 do8 do4 do8 |
do4 do8 \clef "tenor" fa la fa' |
mi' sol' mi' re' fa' re' |
mi'4. r4 r8 |
R2.*9 |
r4 r8 si4.~ |
si8 do' la sol la fad |
sol4. si~ |
si8 do' la sol la fad |
sol4. \clef "bass" sol4\f sol8 |
do4 do8 re4 re8 |
sol,4. r4 r8 |
R2.*3 |
r4 r8 r4
