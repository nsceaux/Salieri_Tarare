\clef "bass" r4_"Pizzicato" r8 do4 do8 |
do4 do8 si,4 si,8 |
do4 do8 do4 do8 |
do4 do8 si,4 si,8 |
do4. r4 r8 |
R2. |
r4 r8 fa4. |
sol sol, |
do4 do8 do4 do8 |
do4 do8 si,4 si,8 |
do4 do8 do4 do8 |
do4 do8 si,4 si,8 |
do4 do'8 si4 do'8 |
si4 do'8 si4 sol8 |
si4 r8 si,4 si,8 |
si, do la, re4 re8 |
sol,4 sol,8 si,4 si,8 |
si,8 do la, re4 re8 |
sol,4. <>^"arco" sol4\f sol8 |
do4 do8 re4 re8 |
sol,4 r8 <>^"pizzicato" la4 la8 |
la4 la8 la4 la8 |
re4 re8 mi4 fa8 |
mi4 mi8 si,4 si,8 |
do4 do'8 la4. |
si4. do'4 do8 |
sol4. do'4 do'8 |
do'8. si16 la8 sol4 sol8 |
sol4. do4 do8 |
do4 do8 sol4 sol,8 |
do4. <>^"arco" do4\f do8 |
do8. si,16 la,8 sol,4 sol,8 |
sol,4. do4 do8 |
do4 do8 sol4 sol,8 |
do4 do8\ff do4 do8 |
do4 do8 do4 do8 |
do4 do8 fa4 r8 |
sol4 r8 sol,4 r8 |
do4. r4 r8 |
R2. |
r4 r8 <>^"pizzicato" fa4 r8 |
sol4 r8 sol,4 r8 |
do4 do8 do4 do8 |
do4 do8 si,4 si,8 |
do4 do8 do4 do8 |
do4 do8 si,4 si,8 |
do4 do'8 si4 do'8 |
si4 do'8 si4 sol8 |
si4. si,4 si,8 |
si, do la, re4 re8 |
sol4. si,4 si,8 |
si, do la, re4 re8 |
sol,4. <>^"arco" sol,4\f sol,8 |
do4 do8 re4 re8 |
sol,4 r8 <>^"pizzicato" la4 la8 |
la4 la8 la4 la8 |
re4 re8 mi4 fa8 |
mi4 mi8 si,4 si,8 |
do4. la |
si do'4 do8 |
sol4. do'4 do'8 |
do' si la sol la fad |
sol4. do4 do8 |
do4 do8 sol4 sol,8 |
do4. <>^"arco" do'4\f do'8 |
do' si la sol la fad |
sol4. do4 do8 |
do4 do8 sol4 sol,8 |
do4 do8 do4 do8 |
do4 do8 do4 do8 |
do4 do8 fa4. |
sol sol, |
do r4 r8 |
R2. |
r4 r8 <>^"pizzicato" fa4. |
sol sol, |
do4 do8 do4 do8 |
do4 do8 si,4 si,8 |
do4 do8 do4 do8 |
do4 do8 si,4 si,8 |
do4 do'8 si4 do'8 |
si4 do'8 si4 sol8 |
si4. si,4 si,8 |
si, do la, re4 re8 |
sol4. si,4 si,8 |
si,8. do16 la,8 re4 re8 |
sol,4. <>^"arco" sol4\f sol8 |
do4 do8 re4 re8 |
sol,4 r8 <>^"pizzicato" la4 la8 |
la4 la8 la4 la8 |
re4 re8 mi4 fa8 |
mi4 mi8 si,4 si,8 |
do4. la |
si do'4 do8 |
sol4. do'4 do'8 |
do' si la sol la fad |
sol4. do4 do8 |
do4 do8 sol4 sol,8 |
do4. <>^"arco" do'4\f do'8 |
do' si la sol la fad |
sol4. do4 do8 |
do4 do8 sol4 sol,8 |
do4 do8 do4 do8 |
do4 do8 do4 do8 |
do4 do8 fa4. |
sol sol, |
do r4 r8 |
R2. |
r4 r8 <>^"[pizzicato]" fa4 r8 |
sol4 r8 sol,4 r8 |
do4 r8 do4 do8 |
do4 do8 si,4 si,8 |
do4 do8 do4 do8 |
do4 do8 si,4 si,8 |
do4 do'8 si4 do'8 |
si4 do'8 si4 sol8 |
si4. si,4 si,8 |
si, do la, re4 re8 |
sol,4. si,4 si,8 |
si, do la, re4 re8 |
sol,4 r r2 |
R1*2 |
sold4 r la r |
si4 r r2 |
do'4 r8 <>^"pizzicato" do'4 do'8 |
do' si la sol la fad |
sol4. do4 do8 |
do4 do8 sol4 sol,8 |
do4. <>^"arco" do'4-\sug\f do'8 |
do' si la sol la fad |
sol4. do4 do8 |
do4 do8 sol4 sol,8 |
do4 do8 do4 do8 |
do4 do8 do4 do8 |
do4 do8 fa4. |
sol sol, |
do4. r4 r8 |
R2. |
r4 r8 <>^"[pizzicato]" fa4. |
sol sol, |
do do4 do8 |
do4 do8 si,4 si,8 |
do4 do8 do4 do8 |
do4 do8 si,4 si,8 |
do4 do'8 si4 do'8 |
si4 do'8 si4 sol8 |
si4. si,4 si,8 |
si, do la, re4 re8 |
sol4. si,4 si,8 |
si, do la, re4 re8 |
sol,4. <>^"arco" sol4\f sol8 |
do4 do8 re4 re8 |
sol,4. <>^"pizzicato" la4 la8 |
la4 la8 la4 la8 |
re4 re8 mi4 fa8 |
mi4 do8 re4 mi8 |
fa4. r4
