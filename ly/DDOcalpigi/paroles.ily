\tag #'(calpigi basse) {
  Je suis né na -- tif de Fer -- ra -- re ;
  là, par les soins d’un père a -- va -- re,
  mon chant s’é -- tant fort em -- bel -- li ;
  ahi ! __ po -- ve -- ro Cal -- pi -- gi !
  ahi ! __ po -- ve -- ro Cal -- pi -- gi !
  Je pas -- sai du con -- ser -- va -- toi -- re,
  pre -- mier chan -- teur à l’o -- ra -- toi -- re
  du sou -- ve -- rain di Na -- po -- li :
  ah ! bra -- vo, ca -- ro Cal -- pi -- gi !
  ah ! bra -- vo, ca -- ro Cal -- pi -- gi !
}
\tag #'(vdessus vbasse basse) {
  Ah ! bra -- vo, ca -- ro Cal -- pi -- gi !
  ah ! bra -- vo, ca -- ro Cal -- pi -- gi !
}
\tag #'(calpigi basse) {
  La plus cé -- lè -- bre can -- ta -- tri -- ce,
  de moi fit bien -- tôt par ca -- pri -- ce,
  un si -- mu -- la -- cre de ma -- ri.
  Ahi ! __ po -- ve -- ro Cal -- pi -- gi !
  Ahi ! __ po -- ve -- ro Cal -- pi -- gi !
  Mes fu -- reurs, ni mes ja -- lou -- si -- es,
  n’ar -- rê -- tant point ses fan -- tai -- si -- es,
  j’é -- tais chez moi comme un zé -- ro :
  ahi ! __ Cal -- pi -- gi po -- ve -- ro !
  ahi ! __ Cal -- pi -- gi po -- ve -- ro !
}
\tag #'(vdessus vbasse basse) {
  Ahi ! __ Cal -- pi -- gi po -- ve -- ro !
  ahi ! __ Cal -- pi -- gi po -- ve -- ro !
}
\tag #'(calpigi basse) {
  Je ré -- so -- lus, pour m’en dé -- fai -- re,
  de la vendre à cer -- tain cor -- sai -- re,
  ex -- près pas -- sé de Tri -- po -- li :
  ah ! bra -- vo, ca -- ro Cal -- pi -- gi !
  ah ! bra -- vo, ca -- ro Cal -- pi -- gi !
  Le jour ve -- nu, mon traî -- tre d’hom -- me,
  au lieu de me comp -- ter la som -- me,
  m’en -- chaîne au pied de leur châ -- lit,
  ahi ! __ po -- ve -- ro Cal -- pi -- gi !
  ahi ! __ po -- ve -- ro Cal -- pi -- gi !
}
\tag #'(vdessus vbasse basse) {
  ahi ! __ po -- ve -- ro Cal -- pi -- gi !
  ahi ! __ po -- ve -- ro Cal -- pi -- gi !
}
\tag #'(calpigi basse) {
  Le for -- ban en fit sa maî -- tres -- se ;
  de moi, l’ar -- gus de sa sa -- ges -- se ;
  et j’é -- tais là tout comme i -- ci :
  Ahi ! __ po -- ve -- ro Cal -- pi -- gi !
  Ahi ! __ po -- ve -- ro Cal -- pi -- gi !
  
  Qu’a -- vez- vous à ri -- re, Spi -- net -- te ?
  
  Vous voy -- ez ma faus -- se co -- quet -- te.
  
  Dit- il vrai ?
  
  Si -- gnor, è ve -- ro.
  
  Ahi ! __ Cal -- pi -- gi po -- ve -- ro !
  Ahi ! __ Cal -- pi -- gi po -- ve -- ro !
}
\tag #'(vdessus vbasse basse) {
  Ahi ! __ Cal -- pi -- gi po -- ve -- ro !
  ahi ! __ Cal -- pi -- gi po -- ve -- ro !
}
\tag #'(calpigi basse) {
  C’est Ta -- ra -- re !
  
  Bien -- tôt à tra -- vers la Ly -- bi -- e,
  l’E -- gyp -- te, l’Isthme et l’A -- ra -- bi -- e,
  il al -- lait nous vendre au So -- phi :
  ahi ! __ po -- ve -- ro Cal -- pi -- gi !
  ahi ! __ po -- ve -- ro Cal -- pi -- gi !
  Nous som -- mes pris, dit le bar -- ba -- re.
  qui nous pre -- nait ? Ce fut Ta -- ra -- re…
}
