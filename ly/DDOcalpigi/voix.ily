<<
  \tag #'(calpigi basse) {
    \clef "alto/G_8" R2.*8 |
    r4 sol8 do'4 do'8 |
    do'4 do'8 re'4 sol'8 |
    mi' do' sol do'4 do'8 |
    do'4 do'8 re'4 sol'8 |
    mi' do' mi' re'4 mi'8 |
    re'4 mi'8 re'4 sol'8 |
    re'4. re'~ |
    re'8 mi' do' si do' la |
    si4. re'~ |
    re'8 mi' do' si do' la |
    sol4. r4 r8 |
    R2. |
    r4 sol'8 dod'4 dod'8 |
    re'4 re'8 mi'4 mi'8 |
    fa' mi' re' do'4 si8 |
    do'4 do'8 re'4 re'8 |
    mi' do' sol' fad'4 sol'8 |
    fa'!4 sol'8 mi'4 sol'8 |
    re'4. do'4 do'8 |
    do'8. si16 la8 sol4 sol8 |
    sol4. mi'4 mi'8 |
    mi'8. fa'16 sol'8 do'4 si8 |
    do'4.
  }
  \tag #'vdessus \clef "soprano/treble"
  \tag #'vbasse \clef "bass"
  \tag #'(vdessus vbasse) { R2.*30 | r4 r8 }
>>
<<
  \tag #'calpigi { r4 r8 R2.*11 }
  \tag #'(vdessus basse) {
    \tag #'basse \ffclef "soprano/treble"
    do''4 do''8 |
    do''8. si'16 la'8 sol'4 sol'8 |
    sol'4. mi''4 mi''8 |
    mi''8. fa''16 sol''8 do''4 si'8 |
    do''4. r4 r8 |
    R2.*7 |
  }
  \tag #'vbasse {
    do'4 do'8 |
    do'8. si16 la8 sol4 sol8 |
    sol4. mi'4 mi'8 |
    mi'8. fa'16 sol'8 do'4 si8 |
    do'4. r4 r8 |
    R2.*7 |
  }
>>
<<
  \tag #'(calpigi basse) {
    \tag #'basse \ffclef "alto/G_8"
    r4 sol8 do'4 do'8 |
    do'4 do'8 re'4 sol'8 |
    mi' do' sol do'4 do'8 |
    do'4 do'8 re'4 sol'8 |
    mi' do' mi' re'4 mi'8 |
    re'4 mi'8 re'4 sol'8 |
    re'4. re'~ |
    re'8 mi' do' si do' la |
    si4. re'~ |
    re'8 mi' do' si do' la |
    sol4. r4 r8 |
    R2. |
    r8 r sol' dod'4 dod'8 |
    re'4 re'8 mi'4 mi'8 |
    fa' mi' re' do'4 si8 |
    do'4 do'8 re'4 re'8 |
    mi' do' sol' fad'4 sol'8 |
    fa'!4 sol'8 mi'4 sol'8 |
    re'4. do'~ |
    do'8 si la sol la fad |
    sol4. mi'~ |
    mi'8 fa' sol' do' re' si |
    do'4.
  }
  \tag #'(vdessus vbasse) { R2.*22 | r4 r8 }
>>
<<
  \tag #'calpigi { r4 r8 | R2.*11 }
  \tag #'(vdessus basse) {
    \tag #'basse \ffclef "soprano/treble"
    do''4.~ |
    do''8 si' la' sol' la' fad' |
    sol'4. mi''~ |
    mi''8 fa'' sol'' do'' re'' si' |
    do''4. r4 r8 |
    R2.*7 |
  }
  \tag #'vbasse {
    do'4.~ |
    do'8 si la sol la fad |
    sol4. mi'~ |
    mi'8 fa' sol' do' re' si |
    do'4. r4 r8 |
    R2.*7 |
  }
>>
<<
  \tag #'(calpigi basse) {
    \tag #'basse \ffclef "alto/G_8"
    r4 sol8 do'4 do'8 |
    do'4 do'8 re'4 sol'8 |
    mi' do' sol do'4 do'8 |
    do'4 do'8 re'4 sol'8 |
    mi' do' mi' re'4 mi'8 |
    re'4 mi'8 re'4 sol'8 |
    re'4. re'4 re'8 |
    re' mi' do' si4 la8 |
    si4. re'4 re'8 |
    re' mi' do' si4 la8 |
    sol4. r4 r8 |
    R2. |
    r8 r sol' dod'4 dod'8 |
    re'4 re'8 mi'4 mi'8 |
    fa' mi' re' do'!4 si8 |
    do'4 do'8 re'4 re'8 |
    mi' do' sol' fad'4 sol'8 |
    fa'!4 sol'8 mi'4 sol'8 |
    re'4. do'~ |
    do'8 si la sol la fad |
    sol4. mi'~ |
    mi'8 fa' sol' do' re' si |
    do'4.
  }
  \tag #'(vdessus vbasse) { R2.*22 r4 r8 }
>>
<<
  \tag #'calpigi { r4 r8 R2.*11 }
  \tag #'(vdessus basse) {
    \tag #'basse \ffclef "soprano/treble"
    do''4.~ |
    do''8 si' la' sol' la' fad' |
    sol'4. mi''~ |
    mi''8 fa'' sol'' do'' re'' si' |
    do''4. r4 r8 |
    R2.*7 |
  }
  \tag #'vbasse {
    do'4.~ |
    do'8 si la sol la fad |
    sol4. mi'~ |
    mi'8 fa' sol' do' re' si |
    do'4. r4 r8 |
    R2.*7 |
  }
>>
<<
  \tag #'(calpigi basse) {
    \tag #'basse \ffclef "alto/G_8"
    r4 sol8 do'4 do'8 |
    do'4 do'8 re'4 sol'8 |
    mi' do' sol do'4 do'8 |
    do'4 do'8 re'4 sol'8 |
    mi'8 do' mi' re'4 mi'8 |
    re'4 mi'8 re'4 sol'8 |
    re'4. re'~ |
    re'8 mi' do' si do' la |
    si4. re'~ |
    re'8 mi' do' si do' la |
    \tag #'didas <>^\markup\italic\column {
      Spinette en cet endroit fait un grand éclat de rire.
    }
    sol4 r r2 |
    \set Staff.shortInstrumentName = ""
    \ffclef "bass" r4 r8 <>^\markup\character Atar
    si si4 si8 do' |
    re'4 re'8 re' si si
    \ffclef "alto/G_8" <>^\markup\character Calpigi
    r8 si16 si |
    mi'8. si16 re'8 re'16 mi' do'8 do'
    \ffclef "bass" <>^\markup\character Atar
    r8 mi'16 do' |
    re'4
    \ffclef "soprano/treble" <>^\markup\character Spinette
    r8 sol'8 re''4 r8 re''16 sol'' |
    mi''4 r8
    \ffclef "alto/G_8" <>^\markup\character Calpigi
    do'4.~ |
    do'8 si la sol la fad |
    sol4. mi'~ |
    mi'8 fa' sol' do' re' si |
    do'4.
  }
  \tag #'(vdessus vbasse) { R2.*10 R1*5 R2.*4 | r4 r8 }
>>
<<
  \tag #'calpigi { r4 r8 \set Staff.shortInstrumentName = \markup\character Cal. R2.*10 }
  \tag #'(vdessus basse) {
    \tag #'basse \ffclef "soprano/treble"
    do''4.~ |
    do''8 si' la' sol' la' fad' |
    sol'4. mi''~ |
    mi''8 fa'' sol'' do'' re'' si' |
    do''4. r4 r8 |
    R2.*6 |
  }
  \tag #'vbasse {
    do'4.~ |
    do'8 si la sol la fad |
    sol4. mi'~ |
    mi'8 fa' sol' do' re' si |
    do'4. r4 r8 |
    R2.*6 |
  }
>>
<<
  \tag #'(calpigi basse) {
    \tag #'basse \ffclef "alto/G_8"
    r4 r8 <>^\markup\character-text Calpigi à part sol'4 sol'8 |
    mi' do' sol^\markup\italic haut et vite do'4 do'8 |
    do'4 do'8 re'4 sol'8 |
    mi' do' sol do'4 do'8 |
    do'4 do'8 re'4 sol'8 |
    mi' do' mi' re'4 mi'8 |
    re'4 mi'8 re'4 mi'8 |
    re'4 r8 re'4.~ |
    re'8 mi' do' si do' la |
    si4. re'~ |
    re'8 mi' do' si do' la |
    sol4. r4 r8 |
    R2. |
    r4 sol'8 dod'4 dod'8 |
    re'4 re'8 mi'4 mi'8 |
    fa' mi' re' do'!4 si8 |
    do'4 do'8 re'4 mi'8 |
    fa'4.\fermata fa'4
  }
  \tag #'(vdessus vbasse) { R2.*17 r4 r8 r4 }
>>
