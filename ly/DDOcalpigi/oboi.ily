\clef "treble" R2.*18 |
r4 r8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 re''8 |
    mi''4 mi''8 fad''4 fad''8 |
    sol''4. }
  { si'4 si'8 |
    do''4 do''8 la'4 la'8 |
    si'4. }
>> r4 r8 |
R2.*9 |
r4 r8 \tag #'oboi <>^"unis" do'''4-\sug\f do'''8 |
do'''8. si''16 la''8 sol''4 sol''8 |
sol''4. \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4 mi''8 |
    mi''8. fa''16 sol''8 do''4 si'8 |
    do''4. mi''4 mi''8 |
    fa''4 fa''8 fa''4 fa''8 |
    mi''4 mi''8 la'' sol''16 fa'' mi'' re'' |
    sol''8 fa''16 mi'' re'' do'' fa''8 mi''16 re'' do'' si' |
    do''4 }
  { sol'4 sol'8 |
    sol'8. fa'16 mi'8 mi'4 re'8 |
    mi'4. sol'4 sol'8 |
    la'4 la'8 la'4 la'8 |
    sol'4 sol'8 fa' la' fa' |
    mi' sol' mi' re' fa' re' |
    mi'4 }
  { s4. | s2. | s4. s-\sug\ff | }
>> r8 r4 r8 |
R2.*13 |
r4 r8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 re''8 |
    mi''4 mi''8 fad''4 fad''8 |
    sol''4. }
  { si'4 si'8 |
    do''4 do''8 la'4 la'8 |
    si'4. }
>> r4 r8 | \allowPageTurn
R2.*9 |
r4 r8 \tag #'oboi <>^"unis" do'''4.-\sug\f~ |
do'''8 si'' la'' sol'' la'' fad'' |
sol''4. \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4.~ |
    mi''8 fa'' sol'' do'' re'' si' |
    do''4. mi''4 mi''8 |
    fa''4 fa''8 fa''4 fa''8 |
    mi''4 mi''8 la'' sol''16 fa'' mi'' re'' |
    sol''8 fa''16 mi'' re'' do'' fa''8 mi''16 re'' do'' si' |
    do''4 }
  { sol'4.~ |
    sol'8 fa' mi' mi' fa' re' |
    mi'4. sol'4 sol'8 |
    la'4 la'8 la'4 la'8 |
    sol'4 sol'8 fa' la' fa' |
    mi' sol' mi' re' fa' re' |
    mi'4 }
>> r8 r4 r8 | \allowPageTurn
R2.*13 | \allowPageTurn
r4 r8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 re''8 |
    mi''4 mi''8 fad''4 fad''8 |
    sol''4. }
  { si'4 si'8 |
    do''4 do''8 la'4 la'8 |
    si'4. }
>> r4 r8 |
R2.*9 | \allowPageTurn
r4 r8 \tag #'oboi <>^"unis" do'''4.-\sug\f~ |
do'''8 si'' la'' sol'' la'' fad'' |
sol''4. \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4.~ |
    mi''8 fa'' sol'' do'' re'' si' |
    do''4. mi''4 mi''8 |
    fa''4 fa''8 fa''4 fa''8 |
    mi''4 mi''8 la'' sol''16 fa'' mi'' re'' |
    sol''8 fa''16 mi'' re'' do'' fa''8 mi''16 re'' do'' si' |
    do''4 }
  { sol'4.~ |
    sol'8 fa' mi' mi' fa' re' |
    mi'4. sol'4 sol'8 |
    la'4 la'8 la'4 la'8 |
    sol'4 sol'8 fa' la' fa' |
    mi' sol' mi' re' fa' re' |
    mi'4 }
>> r8 r4 r8 | \allowPageTurn
R2.*13 |
R1*5 |
R2.*4 |
r4 r8 \tag #'oboi <>^"[unis]" do'''4.-\sug\f ~ |
do'''8 si'' la'' sol'' la'' fad'' |
sol''4. \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4.~ |
    mi''8 fa'' sol'' do'' re'' si' |
    do''4. mi''4 mi''8 |
    fa''4 fa''8 fa''4 fa''8 |
    mi''4 mi''8 la'' sol''16 fa'' mi'' re'' |
    sol''8 fa''16 mi'' re'' do'' fa''8 mi''16 re'' do'' si' |
    do''4 }
  { sol'4.~ |
    sol'8 fa' mi' mi' fa' re' |
    mi'4. sol'4 sol'8 |
    la'4 la'8 la'4 la'8 |
    sol'4 sol'8 fa' la' fa' |
    mi' sol' mi' re' fa' re' |
    mi'4 }
>> r8 r4 r8 |
R2.*13 |
r4 r8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 re''8 |
    mi''4 mi''8 fad''4 fad''8 |
    sol''4. }
  { si'4 si'8 do''4 do''8 la'4 la'8 |
    si'4. }
>> r4 r8 |
R2.*3 |
r4 r8 r4
