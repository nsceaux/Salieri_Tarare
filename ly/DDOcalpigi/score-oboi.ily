\score {
  <<
    \new Staff \with { \tinyStaff \haraKiri } \withLyrics <<
      \global \keepWithTag #'calpigi \includeNotes "voix"
      { s2.*120 s1*6 s2.*5 \set Staff.shortInstrumentName = "" }
    >> { \set fontSize = -2 \keepWithTag #'calpigi \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
      \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
    >>
  >>
  \layout { }
}
