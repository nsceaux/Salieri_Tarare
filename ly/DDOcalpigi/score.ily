\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \flautiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'flauti \includeNotes "flauti"
      >>
      \new Staff \with {
        instrumentName = "[Hautbois]"
        shortInstrumentName = "[Htb.]"
      } << \global \keepWithTag #'oboi \includeNotes "oboi" >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      shortInstrumentName = \markup\character Ch.
      \haraKiriFirst
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\character Calpigi
      shortInstrumentName = \markup\character Cal.
    } \withLyrics <<
      \global \keepWithTag #'(calpigi didas) \includeNotes "voix"
    >> \keepWithTag #'calpigi \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \keepWithTag #'basso \includeNotes "basso"
      \origLayout {
        s2.*7\break s2.*5 s4. \bar "" \pageBreak
        s4. s2.*6\break s2.*6\pageBreak
        s2.*7\break s2.*7\pageBreak
        \grace s8 s2.*6 s4. \bar "" \break s4. s2.*6\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*7\break s2.*6\pageBreak
        s2.*6\break s2.*6\break s2.*5\pageBreak
        s2.*7\break s2.*7\pageBreak
        \grace s8 s2.*6\break s2.*6\break s1*4\pageBreak
        s1 s2.*5\break s2.*7\pageBreak
        s2.*5\break s2.*5\pageBreak
        s2.*6\break
      }
      \modVersion { s2.*35\break \grace s8 }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
