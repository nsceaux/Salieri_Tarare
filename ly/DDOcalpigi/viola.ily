\clef "alto" r4^"Pizzi." r8 <sol mi'>4 q8 |
q4 q8 <sol re'>4 q8 |
<sol mi'>4 q8 q4 q8 |
q4 q8 <sol re'>4 q8 |
<sol mi'>4 r8 do'4 do'8 |
do'4 do'8 do'4 do'8 |
do'4 do'8 fa4. |
sol sol |
do'4 do'8 do'4 do'8 |
do'4 do'8 si4 si8 |
do'4 do'8 do'4 do'8 |
do'4 do'8 si4 si8 |
do'4 do''8 si4 do'8 |
si4 do'8 si4 sol8 |
si4. si4 si8 |
si do' la re'4 re'8 |
sol4 sol8 si4 si8 |
si do' la re'4 re'8 |
sol4. <>^"arco" sol'4\f sol'8 |
do'4 do'8 re'4 re'8 |
sol4 r8 <>^"pizzicato" mi'4 mi'8 |
re'4 re'8 dod'4 dod'8 |
re'4 re'8 mi'4 re'8 |
do'4 do'8 si4 si8 |
do'4. la |
si4. do'4 do8 |
sol4. do'4 do'8 do'8. si16 la8 sol4 sol8 |
sol4. sol'4 sol'8 |
sol'8. fa'16 mi'8 mi'4 re'8 |
mi'4. <>^"arco" do''4\f do''8 |
do''8. si'16 la'8 sol'4 sol'8 |
sol'4. <sol mi' do''>4 sol'8 |
sol'8. fa'16 mi'8 mi'4 re'8 |
do'4 <sol mi'>8-\sug\ff q4 q8 |
<la fa'>4 q8 q4 q8 |
<sol mi'>4 q8 fa' la' fa' |
mi' sol' mi' re' fa' re' |
do'4. <>^"pizzicato" do'4 do'8 |
do'4 do'8 do'4 do'8 |
do'4 do'8 fa4 r8 |
sol4 r8 sol4 r8 |
do4 do'8 do'4 do'8 |
do'4 do'8 si4 si8 |
do'4 do'8 do'4 do'8 |
do'4 do'8 si4 si8 |
do'4 do'8 si4 do'8 |
si4 sol8 si4. |
si4. si4 si8 |
si do' la re'4 re'8 |
sol'4. si4 si8 |
si do' la re'4 re'8 |
sol4. <>^"arco" sol4-\sug\f sol8 |
do'4 do'8 re'4 re'8 |
sol4 r8 <>^"pizzicato" mi'4 mi'8 |
re'4 re'8 dod'4 dod'8 |
re'4 re'8 mi'4 re'8 |
do'4 do'8 sol4 sol'8 |
do'4. la |
si do'4 do8 |
sol4. do'4 do'8 |
do' si la sol la fad |
sol4. do'4 do'8 |
do'4 do8 sol4 sol8 |
do'4. <>^"arco" do''4\f do''8 |
do'' si' la' sol' la' fad' |
sol'4. do'4 do'8 |
do'4 do'8 sol'4 sol8 |
do'4. <sol mi'>4 q8 |
<la fa'>4 q8 q4 q8 |
<sol mi'>4 q8 fa' la' fa' |
mi' sol' mi' re' fa' re' |
mi'4 r8 <>^"pizzicato" do'4 do'8 |
do'4 do'8 do'4 do'8 |
do'4 do8 fa4. |
sol sol |
do4 do'8 do'4 do'8 |
do'4 do'8 si4 si8 |
do'4 do'8 do'4 do'8 |
do'4 do'8 si4 si8 |
do'4 do'8 si4 do'8 |
si4 do'8 si4 sol8 |
si4. si4 si8 |
si8 do' la re'4 re'8 |
sol'4. si4 si8 |
si do' la re'4 re'8 |
sol4. <>^"arco" sol'4-\sug\f sol'8 |
do'4 do'8 re'4 re'8 |
sol4 r8 <>^"pizzicato" mi'4 mi'8 |
re'4 re'8 dod'4 dod'8 |
re'4. mi'4 re'8 |
do'4 do'8 sol4 sol8 |
do'4. la |
si do'4 do8 |
sol4. do'4 do'8 |
do' si la sol la fad |
sol4. do'4 do'8 |
do'4 do8 sol4 sol8 |
do'4. <>^"arco" do''4-\sug\f do''8 |
do'' si' la' sol' la' fad' |
sol'4. do'4 do'8 |
do'4 do'8 sol'4 sol8 |
do'4. <sol mi'>4 q8 |
<la fa'>4 q8 q4 q8 |
<sol mi'>4 q8 fa' la' fa' |
mi' sol' mi' re' fa' re' |
do'4. <>^\markup\whiteout "[pizzicato]" do'4 do'8 |
do'4 do'8 do'4 do'8 |
do'4 do'8 fa4. |
sol sol |
do4 r8 do'4 do'8 |
do'4 do'8 si4 si8 |
do'4 do'8 do'4 do'8 |
do'4 do'8 si4 si8 |
do'4 do'8 si4 sol8 |
si4 sol8 si4. |
si4 r8 si4 si8 |
si do' la re'4 re'8 |
sol4. si4 si8 |
si do' la re'4 re'8 |
r8 re''16\f dod'' re'' dod'' re'' dod'' \rt#4 { re'' dod'' } |
re''4 r r2 |
R1 |
sold'4 r la' r |
si' r r2 |
do''4 r8 <>^"pizzicato" do'4 do'8 |
do' si la sol la fad |
sol4. do'4 do'8 |
do'4 do'8 sol'4 sol8 |
do'4. <>^"arco" do''4\f do''8 |
do'' si' la' sol' la' fad' |
sol'4. do'4 do'8 |
do'4 do'8 sol'4 sol8 |
do'4 <sol mi'>8 q4 q8 |
<la fa'>4 q8 q4 q8 |
<sol mi'>4 q8 fa' la' fa' |
mi' sol' mi' re' fa' re' |
do'4 r8 <>^"pizzicato" do'4 do'8 |
do'4 do'8 do'4 do'8 |
do'4 do'8 fa4. |
sol sol |
do4 r8 do'4 do'8 |
do'4 do'8 si4 si8 |
do'4 do'8 do'4 do'8 |
do'4 do'8 si4 si8 |
do'4 do'8 si4 do'8 |
si4 do'8 si4 sol8 |
si4. si4 si8 |
si do' la re'4 re'8 |
sol4. si4 si8 |
si do' la re'4 re'8 |
sol4. <>^"arco" sol'4-\sug\f sol'8 |
do'4 do'8 re'4 re'8 |
sol4. <>^"pizzicato" mi'4 mi'8 |
re'4 re'8 dod'4 dod'8 |
re'4 re'8 mi'4 re'8 |
do'4 do'8 re'4 mi'8 |
fa'4. r4
