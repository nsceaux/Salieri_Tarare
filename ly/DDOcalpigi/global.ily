\tag #'all \key do \major
\tempo "Allegretto" \midiTempo#120
\time 6/8 s2.*120
\time 4/4 \tempo "Allegro" \midiTempo#100 s1*5
\time 6/8 \midiTempo#120 s2.*32 s4. s4 \bar "|."
