\clef "treble" R2.*4 |
<>^"[Solo]" do''8 re''16 mi'' fa''32 sol'' la'' si'' do'''8 si'' do''' |
\grace re''' do''' si'' do''' \grace re''' do''' si'' do''' |
\grace { do'''16 re''' } mi'''4 do'''8 la'' sol''16 fa'' mi'' re'' |
sol''8 fa''16 mi'' re'' do'' fa''8 mi''16 re'' do'' si' |
do''4. r4 r8 |
R2.*5 |
r4 r8 re'''4.~ |
re'''8 mi''' do''' si'' do''' la'' |
si''4. re'''~ |
re'''8 mi''' do''' si'' do''' la'' |
sol''4. sol'''4-\sug\f sol'''8 |
mi'''8 re'''16 do''' si'' la''32 sol'' fad''4 fad''8 |
sol''4. r4 r8 |
R2.*5 |
r4 r8 do'''4 do'''8 |
do'''8. si''16 la''8 sol''4 sol''8 |
sol''4. mi'''4 mi'''8 |
mi'''8. fa'''16 sol'''8 do'''4 si''8 |
do'''4. <>^"unis" do'''4-\sug\f do'''8 |
do'''8. si''16 la''8 sol''4 sol''8 |
sol''4. mi'''4 mi'''8 |
mi'''8. fa'''16 sol'''8 do'''4 si''8 |
do'''4. <>-\sug\ff \twoVoices #'(flauto1 flauto2 flauti) <<
  { mi''4 mi''8 |
    fa''4 fa''8 fa''4 fa''8 |
    mi''4 mi''8 }
  { sol'4 sol'8 |
    la'4 la'8 la'4 la'8 |
    sol'4 sol'8 }
>> r4 r8 |
R2. |
<>^"Solo" do''8 re''16 mi'' fa''32 sol'' la'' si'' do'''8 si'' do''' |
\grace re'''8 do''' si'' do''' \grace re''' do''' si'' do''' |
\grace { do'''16 re''' } mi'''4 do'''8 la'' sol''16 fa'' mi'' re'' |
sol''8 fa''16 mi'' re'' do'' fa''8 mi''16 re'' do'' si' |
do''4. r4 r8 |
R2.*5 |
r4 r8 <>_"solo" re'''4.~ |
re'''8 mi''' do''' si'' do''' la'' |
si''4. re'''~ |
re'''8 mi''' do''' si'' do''' la'' |
sol''4. sol'''4-\sug\f sol'''8 |
mi''' re'''16 do''' si'' la''32 sol'' fad''4 fad''8 |
sol''4. r4 r8 |
R2.*5 |
r4 r8 do'''4.~ |
do'''8 si'' la'' sol'' la'' fad'' |
sol''4. mi'''~ |
mi'''8 fa''' sol''' do''' re''' si'' |
do'''4 r8 <>^"unis" do'''4.-\sug\f~ |
do'''8 si'' la'' sol'' la'' fad'' |
sol''4. mi'''~ |
mi'''8 fa''' sol''' do''' re''' si'' |
do'''4. \twoVoices #'(flauto1 flauto2 flauti) <<
  { mi''4 mi''8 |
    fa''4 fa''8 fa''4 fa''8 |
    mi''4 mi''8 }
  { sol'4 sol'8 |
    la'4 la'8 la'4 la'8 |
    sol'4 sol'8 }
>> r4 r8 |
R2. |
<>^"Solo" do''8 re''16 mi'' fa''32 sol'' la'' si'' do'''8 si'' do''' |
\grace re'''8 do''' si'' do''' \grace re''' do''' si'' do''' |
\grace { do'''16 re''' } mi'''4 do'''8 la'' sol''16 fa'' mi'' re'' |
sol''8 fa''16 mi'' re'' do'' fa''8 mi''16 re'' do'' si' |
do''4. r4 r8 |
R2.*5 | \allowPageTurn
r4 r8 \grace { si''16 do''' } re'''4 re'''8 |
re'''8. mi'''16 do'''8 si''4 la''8 |
si''4. \grace { si''16 do''' } re'''4 re'''8 |
re'''8. mi'''16 do'''8 si''4 la''8 |
sol''4. sol'''4-\sug\f sol'''8 |
mi'''8 re'''16 do''' si'' la''32 sol'' fad''4 fad''8 |
sol''4. r4 r8 |
R2.*5 |
r4 r8 do'''4.~ |
do'''8 si'' la'' sol'' la'' fad'' |
sol''4. mi'''~ |
mi'''8 fa''' sol''' do''' re''' si'' |
do'''4 r8 <>^"unis" do'''4.-\sug\f~ |
do'''8 si'' la'' sol'' la'' fad'' |
sol''4. mi'''~ |
mi'''8 fa''' sol''' do''' re''' si'' |
do'''4. \twoVoices #'(flauto1 flauto2 flauti) <<
  { mi''4 mi''8 |
    fa''4 fa''8 fa''4 fa''8 |
    mi''4. }
  { sol'4 sol'8 |
    la'4 la'8 la'4 la'8 |
    sol'4. }
>> r4 r8 |
R2. |
<>^"Solo" do''8 re''16 mi'' fa''32 sol'' la'' si'' do'''8 si'' do''' |
\grace re''' do''' si'' do''' \grace re''' do''' si'' do''' |
\grace { do'''16 re''' } mi'''4 do'''8 la'' sol''16 fa'' mi'' re'' |
sol''8 fa''16 mi'' re'' do'' fa''8 mi''16 re'' do'' si' |
do''4. r4 r8 |
R2.*5 |
r4 r8 re'''4.~ |
re'''8 mi''' do''' si'' do''' la'' |
si''4. re'''~ |
re'''8 mi''' do''' si'' do''' la'' |
sol''4 r r2 |
R1*4 |
r4 r8 do'''4.~ |
do'''8 si'' la'' sol'' la'' fad'' |
sol''4. mi'''~ |
mi'''8 fa''' sol''' do''' re''' si'' |
do'''4 r8 <>^"[unis]" do'''4.-\sug\f ~ do'''8 si'' la'' sol'' la'' fad'' |
sol''4. mi'''~ |
mi'''8 fa''' sol''' do''' re''' si'' |
do'''4. \twoVoices #'(flauto1 flauto2 flauti) <<
  { mi''4 mi''8 |
    fa''4 fa''8 fa''4 fa''8 |
    mi''4 mi''8 }
  { sol'4 sol'8 |
    la'4 la'8 la'4 la'8 |
    sol'4 sol'8 }
>> r4 r8 |
R2. |
<>^"Solo" do''8 re''16 mi'' fa''32 sol'' la'' si'' \grace re'''8 do''' si'' do''' |
\grace re''' do''' si'' do''' \grace re''' do''' si'' do''' |
\grace { do'''16 re''' } mi'''4 do'''8 la'' sol''16 fa'' mi'' re'' |
sol''8 fa''16 mi'' re'' do'' fa''8 mi''16 re'' do'' si' |
do''4. r4 r8 |
R2.*5 |
r4 r8 re'''4.~ |
re'''8 mi''' do''' si'' do''' la'' |
si''4. re'''~ |
re'''8 mi''' do''' si'' do''' la'' |
sol''4. sol'''4-\sug\f sol'''8 |
mi''' re'''16 do''' si'' la''32 sol'' fad''4 fad''8 |
sol''4. r4 r8 |
R2.*3 |
r4 r8 r4
