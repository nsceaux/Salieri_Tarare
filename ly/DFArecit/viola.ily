\clef "alto" mi'4 r r2 |
r4 la2:16\ff si4:16 |
dod':16 la:16 si:16 dod':16 |
re':16 si:16 dod':16 re':16 |
mi':16 dod':16 re':16 mi':16 |
re' r r r8. re'16-\sug\ff |
re'2. r4 |
R1 |
r4 re' r2 |
r8 r16 sol-\sug\ff sol2 r4 |
R1 |
r4 <sol mi'>-\sug\ff <do sol mi'> r |
R1*2 | \allowPageTurn
r2 r4 sol16-\sug\p sol sol sol |
sol4 r r la:16 |
la4 r r2 |
r r4 mi':16 |
mi'4 r r2 |
R1 |
r8 r16 mi' mi'4 r8 r16 mi' mi'4 |
r8 r16 la la4 r2 |
R1 |
r4 la16-\sug\ff( re' fa' la') la'4 r |
sol' r r2 |
R1 | \allowPageTurn
do'4 r r2 |
r4 do'-\sug\f do' do' |
do'1~ |
do' |
r4 r8 r16 fa'-\sug\f fa'4 fa' |
fa'1-\sug\fp |
r4 r8 r16 fa'-\sug\f fa'4 fa |
fa1-\sug\p~ |
fa |
sol |
la |
sib |
la |
re'( |
fa) |
mi |
fa4-\sug\f r8 fa16 sol la8 la16 sib do'8 re'16 mib' |
fa'4 r r2 | \allowPageTurn
re'2:16-\sug\fp re':16 |
re':16 re':16 |
re':16 re':16 |
re'16-\sug\fp re'' re'' re'' re''4:16 re''2:16 |
re'':16 re'':16 |
mib'':16-\sug\fp mib'':16 |
mib'':16 mib'':16 |
lab':16\fp lab':16 |
lab':16 lab'4 r |
r2 r8 r16 mi-\sug\f mi4 |
r2 r8 r16 fa fa4\fermata |
sol16-\sug\fp re'' re'' re'' re''4:16 re''2:16 |
re'':16 re'':16 |
re'':16-\sug\f re'':16 |
re''4 r r2 |
R1 | \allowPageTurn
r16 do'\f re' mi' fa' sol' la' si' do''4 r |
R1*2 |
r8 r16 si\f si4 r2 |
R1 |
la4 r r2 |
R1*2 | \allowPageTurn
<sol mi'>1 |
r2 r4 sol-\sug\f |
fa r r fa:16-\sug\p |
mi:16 fa:16 sol:16 mi:16 |
fa4 r r la:16 |
sol:16 la:16 sib:16 sol:16 |
la r r2 |
r2\fermata r8. la16 la8. la16 |
re'16-\sug\fp fa fa fa fa4:16 fa2:16 |
fa:16 fa:16 |
fa:16 fa:16 |
sol:16-\sug\fp sol:16 |
sol:16 sol:16 |
sol:16 sol:16 |
sol:16 sol:16 |
fa16 fa' fa' fa' fa'4:16 fa'2:16 |
solb':16 solb':16 |
fa':16-\sug\f fa':16 |
fa':16-\sug\p fa':16 |
mib':16-\sug\f mib':16 |
mib':16 mib':16 |
<< { mib':16 mib':16 | mib'4 } \\ { do'2:16-\sug\f do':16 | do'4 } >> r4\fermata r2 |
fa1-\sug\pp~ |
fa |
mib4 r\fermata sol8-\sug\ff la!16 si! do' re' mi'! fad' |
sol'2:16-\sug\fp sol':16 |
sol':16 sol':16 |
sol':16-\sug\f sol':16 |
do'4 r\fermata r2 |
r r8 r16 do'-\sug\p do'8. do'16 |
mi'4 r r2 |
fa'4 r r2 |
R1 | \allowPageTurn
mi'4 r\fermata re'2-\sug\fp~ |
re'1~ |
re'2 re'4 r |
re'1-\sug\fp |
sol4 r\fermata |
r16 sol' sol' sol' r sol' sol' sol' |
r fad' fad' fad' r fad' fad' fad' |
r fad' fad' fad' r fad' fad' fad' |
r16 sol' sol' sol' r sol' sol' sol' |
r16 sol' sol' sol' r sol' sol' sol' |
r re' re' re' r re' re' re' |
r re' re' re' r re' re' re' |
re'4 r r <sol re' si'>-\sug\f |
R1 | \allowPageTurn
do'8-\sug\p -.( do'-. do'-. do'-.) do'2:8^\dotFour |
do':8 do':8 |
do':8 do':8 |
do':8 do':8 |
do'2 r |
sib8*2/3-\sug\p re' fa' sib re' fa' la do' fa' la do' fa' |
\ru#4 { sib re' fa' } |
\ru#4 { re' fa' sib' } |
\ru#4 { mib' sol' sib' } |
mib' sol' sib' mib' sol' sib' mib' sol' do'' mib' sol' do'' |
\ru#4 { re' la' do'' } |
\ru#4 { re' fad' la' } |
\ru#4 { sol' sib' re'' } |
\ru#2 { mib' sol' do'' } \ru#2 { fa' la' do'' } |
sib4 r r sib16 sib sib sib |
sib4 r r2 |
r4 re'16 re' re' re' mib'4 r |
R1 | \allowPageTurn
r4 mib'16 mib' mib' mib' mib'4 r\fermata |
sol2\pp( lab8) r r4 |
r4 do' reb'8 r\fermata r4 |
re'!1_\markup\dynamic mfp ~ |
re'4 re' mib'8 r r4 |
mi'!1_\markup\dynamic mfp ~ |
mi'2~ mi'4 do'-\sug\mf |
fa'4 r fad'2_\markup\tiny\dynamic mfp |
sol'8 r r4 r2 |
sol'2_\markup\tiny\dynamic mfp fa'8 r la4\f |
sib4 r\fermata |
sib\sf sib\sf |
mib'\sf sol'-\sug\sf |
lab'-\sug\sf sib'-\sug\f |
mib' r r2 | \allowPageTurn
r lab'4 r |
mi'! mi' |
fa' r |
fa' reb' |
mib'32-\sug\ff <mib' mib''> q q q q q q q4:32 |
q2:32 |
lab'4 r r2 |
R1 |
r2 r8 do''-\sug\f lab' fa' |
re'4 r r2 |
R1*2 | \allowPageTurn
r2 mib'4 r |
r2 mib'4 r |
R1 |
r4 sib si2 |
R1*2 | \allowPageTurn
sol8-\sug\p mi' sol mi' sol mi' sol mi' |
fa' la la' fa' do' la fa la' |
<re' fa'>1 |
mi'16 sol' fa' sol' mi' sol' fa' sol' mi'4 r |
sol8 sol' sol sol' sol sol' sol sol' |
sol sol' sol sol' sol sol' sol sol' |
sol sol' sol sol' sol sol' sol sol' |
sol2 r\fermata |
R1 | \allowPageTurn
sol8 mi' sol mi' sol mi' sol mi' |
fa' la la' fa' do' la la' fa' |
<re' fa'>1 |
mi'16( sol' fa' sol' mi' sol' fa' sol') mi'8 sol do' mi' |
re'4 re'16 sol sol sol re' sol sol sol re' sol sol sol |
mi' sol sol sol mi' sol sol sol mi'8 mi'([ re' do']) |
do'16( si la si) do' si la si do' si la si do' la do' la |
si re' do' re' si re' do' re' si8 sol'[-! si'-! sol'-!] |
<la fad' re''>4-\sug\f <la fad'>-\sug\p q <la la'> |
<la fad'>2-\sug\fp~ q8 <fad' la'> q q |
<la fad' re''>4-\sug\f <la fad'>-\sug\p q <la la'> |
<la fad'>2-\sug\fp~ q8 si'8 si' si' |
la'4-\sug\fp la re'-\sug\fp re' |
sol8-\sug\f sol' sol sol' sol sol'[ si' sol'] |
<la fad' re''>4-\sug\fp <la fad'> q <la la'> |
re'8\fp re'' re' re'' re' re'' re' re'' |
re'\fp re'' re' re'' re' re'' re' re'' |
re'2\f fad'4\p fad' |
sol'\f sol' fad'\p fad' |
sol'\f sol' fad'\p fad' |
sol'\f sol' fad'\p fad' |
sol'\f sol' si si |
do' do' si si |
do' do' si\p si |
do' do' si\cresc si |
do' do' si si |
do' do' si si |
do'8 do' mi' sol' do''4 do' |
sol8\fp sol' sol sol' sol sol' sol sol' |
sol8\fp sol' sol sol' sol sol' sol sol' |
sol8\fp sol' sol sol' sol sol' sol sol' |
sol8\fp sol' sol sol' sol sol' sol sol' |
re'4\fp re' sol'\fp sol' |
do'8\f do'' do' do'' do' do' do' do' |
sol8\fp sol' sol sol' sol sol' sol sol' |
sol8\fp sol' sol sol' sol sol' sol sol' |
sol8\fp sol' sol sol' sol sol' sol sol' |
sol2\f mi'4\fp mi'8 mi' |
fa'8 r fa' r sol' r sol' r |
la'4 la'8 la' si'4\cresc si'8 si' |
do''4 do''8 do'' re''4 re''8 re'' |
mi''2 do'8*2/3\ff do' do' do' do' do' |
fa'2.*2/3:8 sol':8 |
do'4 r mi'\fp mi'8 mi' |
fa' r fa' r sol' r sol' r |
la'4 la'8 la' si'4 si'8 si' |
do''4 do''8 do'' re''4 re''8 re'' |
mi''2 do'8*2/3\ff do' do' do' do' do' |
fa'2.*2/3:8 sol':8 |
do'':8 do':8 |
fa':8 sol':8 |
do'8 do'' do' do'' do' do'' do' do'' |
do' do'' do' do'' do' do'' do' do'' |
do' do'' do' do'' do' do'' do' do'' |
do' do'' do' do'' do' do'' do' do'' |
do' sol' mi' do' si4\mf si |
do' do' si\p si |
do' do' si si |
do' do' si_\markup\italic [en diminuant] si |
do' do'' sol' mi' |
do'2\fermata
