\drummode {
  R1*106 R2*8 R1*30 R2*4 R1*2 R2*5 R1*29
  <>-\sug\f \ru#10 { hh2 r }
  R1*10
  \ru#10 { hh2 r }
  R1*13
  \ru#4 { hh2 r }
  R1*5
  r2
}