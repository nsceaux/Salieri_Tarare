\score {
  \new ChoirStaff <<
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
      \haraKiriFirst
    } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new Staff \with {
      \haraKiriFirst
      \consists "Metronome_mark_engraver"
    } <<
      \global \keepWithTag #'fagotti \includeNotes "fagotti"
      \notemode {
        \quoteBasso "DFAbasso"
        s1*27
        <>_\markup\tiny Basso \cue "DFAbasso" { \mmRestDown s1*6 }
        s1*11
        \new CueVoice {
          \mmRestUp re2:16\fp re:16 |
          re:16 re:16 |
          re:16 re:16 |
          \mmRestDown sol,4\fp r r2 |
          fa4 r r2 |
          mib4 r r2 |
          \mmRestCenter s1 |
          \mmRestDown lab4 r r2 |
          \mmRestCenter s1 |
          \mmRestDown r2 r8 r16 mi!\f^"tutti" mi4 |
          r2 r8 r16 fa fa4\fermata |
          sol,16\fp sol sol sol sol4:16 sol2:16 |
          sol2:16 sol2:16 |
          sol8\ff sol, si, re sol re si, re |
          sol,4 r r2 |
        }
        s1
        \cue "DFAbasso" { \mmRestDown s1 \mmRestCenter }
        s1*2
        \cue "DFAbasso" { \mmRestDown s1 \mmRestCenter }
        s1
        \cue "DFAbasso" { \mmRestDown s1 \mmRestCenter }
        s1*2
        \cue "DFAbasso" { \mmRestDown s1*7 \mmRestCenter }
        s1*16
        \cue "DFAbasso" { \mmRestUp s1*3 \mmRestDown s1*7 \mmRestCenter }
        s1
        \cue "DFAbasso" { \mmRestDown s1*4 s2*8 s1 \mmRestCenter }
        s1
        \cue "DFAbasso" { \mmRestDown s1*17 \mmRestCenter }
        s1
        \cue "DFAbasso" { \mmRestDown s1*10 s2*4 s1*2 s2*3 \mmRestCenter }
        s2*2 s1*2
        \cue "DFAbasso" { \mmRestDown s1*2 \mmRestCenter }
        s1*2
        \cue "DFAbasso" { \mmRestDown s1*2 \mmRestCenter }
        s1
        \cue "DFAbasso" { \mmRestDown s1 \mmRestCenter }
        s1
        \cue "DFAbasso" { \mmRestDown s1*17 s2 \mmRestCenter }
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
}
