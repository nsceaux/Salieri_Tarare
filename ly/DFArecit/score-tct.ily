\score {
  <<
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
      \haraKiriFirst
    } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes en Ut }
        shortInstrumentName = "Tr."
      } << \global \keepWithTag #'trombe \includeNotes "trombe" >>
      \new Staff \with {
        \consists "Metronome_mark_engraver"
        instrumentName = \markup\center-column { Cors en Sol }
        shortInstrumentName = \markup Cor.
      } << \global \keepWithTag #'corni \includeNotes "corni" >>
      \new DrumStaff \with {
        \override StaffSymbol.line-count = #1
        instrumentName = \markup Tambour
        shortInstrumentName = \markup Tamb.
        drumStyleTable = #percussion-style
      } << \global \includeNotes "tambour" >>
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 10\mm
    \context { \Score \remove "Metronome_mark_engraver" }
  }
}
