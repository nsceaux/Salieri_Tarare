\clef "bass" mi4 r r2 |
r4 la,2:16\ff si,4:16 |
dod:16 la,:16 si,:16 dod:16 |
re:16 si,:16 dod:16 re:16 |
mi:16 dod:16 re:16 mi:16 |
re4 r r r8. re16\ff |
re2. r4 |
R1 |
r4 fad r2 |
r8 r16 sol,\ff^"tutti" sol,2 r4 |
R1 |
r4 do do r |
r <>^"Violoncelli" mi16\p mi mi mi mi4 r |
r2 r4 fa16 fa fa fa |
fa4 r r mi16 mi mi mi |
mi4 r r re:16 |
re4 r r2 |
r r4 mi:16 |
mi r r2 |
R1 |
r8 r16 mi mi4 r8 r16 mi mi4 |
r8 r16 la, la,4 r2 |
R1 |
r4 fa\ff^"tutti" fa4 r |
sol r r2 |
R1 | \allowPageTurn
do4^"Violoncelli" r r2 |
r4 <>^"tutti" do-\sug\f do do |
do1~ |
do |
r4 r8 r16 fa\f fa4 fa |
fa1\fp |
r4 r8 r16 fa\f fa4 fa, |
<<
  { <>^"Viol" sib,1-\sug\p |
    sib, |
    mib |
    fa |
    sib, |
    fa1 |
    sib, |
    sib, | } \\
  { sib,4_\markup\center-align "Bassi [pizzicato]" sol,\rest r2 |
    sib,4 sol,\rest r2 |
    mib4 sol,\rest r2 |
    fa4 sol,\rest r2 |
    sib,4 sol,\rest r2 |
    fa,4 sol,\rest r2 |
    sib,4 sol,\rest r2 |
    sib,4 sol,\rest r2 | }
>>
do1 |
fa4-\sug\f r8 <>^"arco" fa16 sol la8 la16 sib do'8 re'16 mib' |
fa'4 r r2 |
re2:16\fp re:16 |
re:16 re:16 |
re:16 re:16 |
<<
  { <>^\markup\right-align\translate#'(-1 . 0) Viol. sol16-\sug\fp sol' sol' sol' sol'4:16 sol'2:16 |
    \clef "tenor" fa'2:16 fa':16 |
    \clef "bass" mib':16-\sug\fp mib':16 |
    mib':16 mib':16 | } \\
  { sol,4 r r2 |
    fa4 r r2 |
    mib4 r r2 |
    R1 | }
>>
lab4 r r2 |
R1 | \allowPageTurn
r2 r8 r16 mi!\f^"tutti" mi4 |
r2 r8 r16 fa fa4\fermata |
sol,16\fp sol sol sol sol4:16 sol2:16 |
sol2:16 sol2:16 |
sol8\ff sol, si, re sol re si, re |
sol,4 r r2 |
R1 |
r16 do-\sug\f re mi fa sol la si do'4 r |
R1*2 | \allowPageTurn
r8 r16 si,\f si,4 r2 |
R1 |
la,4 r r2 |
R1*2 | \allowPageTurn
sib,1 |
r2 r4 do\f |
fa, r r fa:16\p |
mi:16 fa:16 sol:16 mi:16 |
fa4 r r la:16 |
sol:16 la:16 sib:16 sol:16 |
la4 r r2 |
r2\fermata r |
re8\fp re re re re2:8 |
re:8 re:8 |
re:8 re:8 |
sol,:8\fp sol,:8 |
sol,:8 sol,:8 |
do:8 do:8 |
do:8 do:8 |
fa:8 fa:8 |
mib:8 mib:8 |
reb8\f mib16 fa solb lab sib do' reb'2:8 |
reb'4-\sug\p r r2 |
mib8\f fa16 sol lab sib do' re' mib'2:8 |
mib'2. mib4 |
lab,2:8\f lab,:8 |
lab,4 r\fermata r2 |
sib,1\pp~ |
sib, |
mib4 r\fermata sol,8\ff la,!16 si,! do re mi fad |
sol2:16\fp sol:16 |
sol:16 sol:16 |
sol:16\f sol:16 |
do4 r\fermata r2 |
r r8 r16 do\p do8. do16 |
mi4 r r2 |
fa4 r r2 |
R1 | \allowPageTurn
mi4 r\fermata re2\fp~ |
re1~ |
re2 re4 r |
re1\fp |
sol4 r\fermata |
sol8 r sol r |
fad r fad r |
fad r fad r |
mi r mi r |
mi r mi r |
fad r fad r |
fad r fad r |
sol4 r r sol,\f |
R1 | \allowPageTurn
do8-\sug\p -.( do-. do-. do-.) do2:8_\dotFour |
do2:8 fa:8 |
fa:8 fa:8 |
fa:8 fa:8 |
fa r |
sib,4\p sib, la, la, |
sib, sib, sib, sib, |
re re re re |
mib mib mib mib |
mib mib mib mib |
re re re re |
re re re re |
sol sol sol sol |
mib4. mib8 fa4. fa,8 |
sib,4 r r sib,16 sib, sib, sib, |
re4 r r2 |
r4 re16 re re re mib4 r |
R1 | \allowPageTurn
r4 mib16 mib mib mib mib4 r\fermata |
sol,2\pp( lab,8) r r4 |
r4 do reb8 r\fermata r4 |
re!1_\markup\dynamic mfp ~ |
re4 re mib8 r r4 |
mi!1_\markup\dynamic mfp ~ |
mi2~ mi4 do\mf |
fa4 r fad2_\markup\dynamic mfp |
sol4 r r2 |
sol2_\markup\dynamic mfp fa8 r la,4\f |
sib, r\fermata |
sib,4\sf sib,\sf |
mib\sf sol\sf |
lab\sf sib\f |
mib r r2 |
r lab4 r |
mi! mi |
fa r |
fa reb |
mib16\ff mib mib mib mib mib mib mib |
mib mib mib mib mib mib mib mib |
lab,4 r4 r2 |
R1 |
r2 r8 do'\f lab fa |
re4 r r2 |
R1*2 | \allowPageTurn
r2 mib4 r |
r2 lab,?4 r |
R1 |
r4 sib, si,2 |
R1 | \allowPageTurn
do8 do do do do2:8 |
do:8\p do:8 |
do:8 do:8 do:8 do:8 |
do:8 do:8 |
sol,8 sol sol, sol sol, sol sol, sol |
sol, sol sol, sol sol, sol sol, sol |
sol, sol sol, sol sol, sol sol, sol |
sol,2 r\fermata |
do8 do do do do2:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
si,:8 si,:8 |
do:8 do:8 |
re:8 re:8 |
sol2 r8 sol-! si-! sol-! |
re\f re'-\sug\p re re' \rt#2 { re re' } |
<>\fp \rt#2 { re re' } \rt#2 { re re' } |
<>\fp \rt#2 { re re' } \rt#2 { re re' } |
re8\fp re' re re' re si,[ si, si,] |
la,4\fp la, re\fp re |
sol,8-\sug\f sol sol, sol sol, sol[ si sol] |
re\fp re' re re' re re' re re' |
re\fp re' re re' re re' re re' |
re\fp re' re re' re re' re re' |
re2\f fad4\p fad |
sol\f sol fad\p fad |
sol\f sol fad\p fad |
sol\f sol fad\p fad |
sol\f sol si, si, |
do do si, si, |
do do si,\p si, |
do do si,\cresc si, |
do do si, si, |
do do si, si, |
do8 do mi sol do'4 do |
sol,8\fp sol sol, sol sol, sol sol, sol |
sol,8\fp sol sol, sol sol, sol sol, sol |
sol,8\fp sol sol, sol sol, sol sol, sol |
sol,8\fp sol sol, sol sol, sol sol, sol |
re4\fp re sol\fp sol |
do8\f do' do do' do do do do |
sol,8\fp sol sol, sol sol, sol sol, sol |
sol,8\fp sol sol, sol sol, sol sol, sol |
sol,8\fp sol sol, sol sol, sol sol, sol |
sol,2\f mi4\fp mi8 mi |
fa8 r fa r sol r sol r |
la4 la8 la si4\cresc si8 si |
do'4 do'8 do' re'4 re'8 re' |
mi'2 do8*2/3\ff do do do do do |
fa2.*2/3:8 sol:8 |
do4 r mi\fp mi8 mi |
fa r fa r sol r sol r |
la4 la8 la si4 si8 si |
do'4 do'8 do' re'4 re'8 re' |
mi'2 do8*2/3\ff do do do do do |
fa2.*2/3:8 sol:8 |
do':8 do:8 |
fa:8 sol:8 |
do8 do' do do' do do' do do' |
do do' do do' do do' do do' |
do do' do do' do do' do do' |
do do' do do' do do' do do' |
do sol mi do si,4\mf si, |
do do si,\p si, |
do do si, si, |
do do si,_\markup\italic [en diminuant] si, |
do do' sol mi |
do2\fermata
