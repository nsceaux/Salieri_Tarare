\clef "treble" R1*106 R2*8 R1*30 R2*4 R1*2 R2*5 R1*28 |
r2 r8 \twoVoices#'(flauto1 flauto2 flauti) <<
  { sol''8-! si''-! sol''-! |
    re'''4 re''' re''' fad''' |
    re'''2~ re'''8 la'' la'' la'' |
    re'''4 re''' re''' fad''' |
    re'''2~ re'''8 re''' re''' re''' |
    do'''4 do'''8. do'''16 la''4 la''8. la''16 |
    si''16( re''' si'' re''' si'' re''' si'' re''') si''8 sol''[ si'' sol''] |
    re'''4 re''' re''' fad''' |
    re'''2~ re'''8 la'' la'' la'' |
    re'''4 re''' re''' fad''' |
    re'''2 }
  { sol''8-! si''-! sol''-! |
    fad''4 fad'' fad'' la'' |
    fad''2~ fad''8 fad'' fad'' fad'' |
    fad''4 fad'' fad'' la'' |
    fad''2~ fad''8 re'' re'' re'' |
    mi''4 mi''8. mi''16 fad''4 fad''8. fad''16 |
    sol''( si'' sol'' si'' sol'' si'' sol'' si'') si''8 sol''[ si'' sol''] |
    fad''4 fad'' fad'' la'' |
    fad''2~ fad''8 fad'' fad'' fad'' |
    fad''4 fad'' fad'' la'' |
    fad''2 }
  { s4. | s4-\sug\f s2.-\sug\p | s1-\sug\fp | s4-\sug\f s2.-\sug\p | s1-\sug\fp |
    s2-\sug\fp s-\sug\fp | s1-\sug\f | s-\sug\fp | s-\sug\fp | s-\sug\fp | s2-\sug\f }
>>
<<
  \tag #'(flauto1 flauti) {
    <>^"Flûte seule" re'''8*2/3-\sug\p mi''' re''' do''' si'' la'' |
    sol''-\sug\f sol'' sol'' sol''4 re'''8*2/3\p mi''' re''' do''' si'' la'' |
    sol''\f sol'' sol'' sol''4 re'''8*2/3\p mi''' re''' do''' si'' la'' |
    sol''\f sol'' sol'' sol''4 re'''8*2/3\p mi''' re''' do''' si'' la'' |
    sol'' sol'' sol'' sol''4 sol''8*2/3\f la'' sol'' fa''! mi'' re'' |
    do'' do'' do'' do''4 sol''8*2/3 la'' sol'' fa'' mi'' re'' |
    do'' do'' do'' do''4 sol''8*2/3\p la'' sol'' fa'' mi'' re'' |
    do'' do'' do'' mi'' mi'' mi'' sol''\cresc la'' sol'' fa'' mi'' re'' |
    do'' do'' do'' mi'' mi'' mi'' sol'' la'' sol'' fa'' mi'' re'' |
    do'' do'' do'' do''4 sol''8*2/3 la'' sol'' fa'' mi'' re'' |
    do''2\!
  }
  \tag #'flauto2 { r2 | R1*9 | r2 }
>> r8 \twoVoices#'(flauto1 flauto2 flauti) <<
  { <>^"tutti" do''8 mi'' do'' |
    sol''4 sol'' sol'' si'' |
    sol''4 sol''16 la'' si'' do''' re'''8 re''[ re'' re''] |
    sol''4 sol'' sol'' si'' |
    sol'' sol''16 la'' si'' do''' re'''8 sol''[ sol'' sol''] |
    fa''!4 fa''8. fa''16 re''4 re''8. re''16 |
    mi''16( sol'' mi'' sol'' mi'' sol'' mi'' sol'') mi''8 do'' mi'' do'' |
    sol''4 sol'' sol'' si'' |
    sol'' sol''16 la'' si'' do''' re'''8 re''[ re'' re''] |
    sol''4 sol'' sol'' si'' |
    sol''2 }
  { do''8 mi'' do'' |
    si'4 si' si' re'' |
    \sug si'4 sol''16 la'' si'' do''' re'''8 si'[ si' si'] |
    si'4 si' si' re'' |
    si' sol''16 la'' si'' do''' re'''8 si'[ si' si'] |
    la'4 la'8. la'16 si'4 si'8. si'16 |
    do''16( mi'' do'' mi'' do'' mi'' do'' mi'') do''8 do'' mi'' do'' |
    si'4 si' si' re'' |
    si' sol''16 la'' si'' do''' re'''8 si'[ si' si'] |
    si'4 si' si' re'' |
    si'2 }
  { s4. | s4-\sug\f s2.-\sug\p | s2-\sug\f s8 s4.-\sug\p |
    s4-\sug\f s2.-\sug\p | s2-\sug\f s8 s4.-\sug\p |
    s2-\sug\fp s2-\sug\fp | s1-\sug\f |
    s4-\sug\f s2.-\sug\p | s1 | s-\sug\fp | s2-\sug\f }
>> r2 |
R1 |
\twoVoices#'(flauto1 flauto2 flauti) <<
  { do''4 do''8 do'' re''4 re''8 re'' |
    mi''4 mi''8 mi'' fa''4 fa''8 fa'' |
    sol''2 do''' |
    do''' si'' |
    do'''4 }
  { do'4 do'8 do' re'4 re'8 re' |
    mi'4 mi'8 mi' fa'4 fa'8 fa' |
    sol'2 do'' |
    re'' re'' |
    do''4 }
  { s2 s-\sug\cresc | s1 | s2 s-\sug\ff }
>> r4 r2 |
R1 |
\twoVoices#'(flauto1 flauto2 flauti) <<
  { do''4 do''8 do'' re''4 re''8 re'' |
    mi''4 mi''8 mi'' fa''4 fa''8 fa'' |
    sol''2 do''' |
    do''' si'' |
    do'''1 |
    do'''2 si'' |
    do'''2. sol''8*2/3 la'' si'' |
    do'''4 do''' do''' mi''' |
    do'''2. sol''8*2/3 la'' si'' |
    do'''4 do''' do''' mi''' |
    do'''2 sol''8*2/3 la'' sol'' fa'' mi'' re'' |
    do'' do'' do'' do''4 sol''8*2/3 la'' sol'' fa'' mi'' re'' |
    do'' do'' do'' do''4 }
  { do'4 do'8 do' re'4 re'8 re' |
    mi'4 mi'8 mi' fa'4 fa'8 fa' |
    sol'2 mi'' |
    re'' re'' |
    mi''1 |
    re'' |
    mi''2. mi''8*2/3 fa'' re'' |
    mi''4 mi'' mi'' sol'' |
    mi''2. mi''8*2/3 fa'' re'' |
    mi''4 mi'' mi'' sol'' |
    mi''2 re''4 re' |
    mi'8*2/3 mi' mi' mi'4 re' re' |
    mi'8*2/3 mi' mi' mi'4 }
  { s1*2 | s2 s-\sug\ff | s1*7 | s2 s-\sug\mf | s s-\sug\p }
>> r2 |
R1*2 |
r2
