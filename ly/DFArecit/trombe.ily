\clef "treble" R1*106 R2*8 R1*30 R2*4 R1*2 R2*5 R1*28 R1*34 |
r2 <>-\sug\ff \twoVoices#'(tromba1 tromba2 trombe) <<
  { mi''2 | re''1 | do''2 }
  { do''2 | do'' sol' | mi' }
>> r2 |
R1*3 |
r2 <>-\sug\ff \twoVoices#'(tromba1 tromba2 trombe) <<
  { mi''2 |
    re''1 |
    mi'' |
    re'' |
    do''2. sol'4 |
    do'' do'' do'' mi'' |
    do''2. sol'4 |
    do'' do'' do'' mi'' |
    do''2 re''4 re'' |
    mi''8*2/3 mi'' mi'' mi''4 }
  { do''2 |
    do'' sol' |
    do''1 |
    do''2 sol' mi'2. sol'4 |
    mi' mi' mi' sol' |
    mi'2. sol'4 |
    mi' mi' mi' sol' |
    mi'2 sol'4 sol' |
    do''8*2/3 do'' do'' do''4 }
  { s2 | s1*7 | s2 s-\sug\mf }
>> r2 |
R1*3 |
r2
