\tag #'(recit basse) {
  On vient ; c’est le sul -- tan.

  Quel in -- so -- lent i -- ci ?…

  Un in -- so -- lent !… C’est Cal -- pi -- gi !

  D’où vient cet -- te voix dé -- plo -- ra -- ble ?

  Sei -- gneur, c’est… c’est ce mi -- sé -- ra -- ble.
  Croy -- ant en -- ten -- dre quel -- que bruit,
  nous fai -- sions la ron -- de de nuit.
  D’u -- ne sou -- dai -- ne fré -- né -- si -- e
  cet -- te brute à l’ins -- tant sai -- si -- e…
  peut- ê -- tre a- t-il per -- du l’es -- prit !
  Mais il pleu -- re, il cri -- e, il s’a -- gi -- te,
  par -- le, par -- le, par -- le si vi -- te,
  qu’on n’en -- tend rien de ce qu’il dit.

  Il par -- le, ce mu -- et ?

  Que dis- je !
  par -- ler se -- rait un beau pro -- di -- ge !
  d’af -- freux sons i -- nar -- ti -- cu -- lés…

  O bi -- zar -- re sort de ton maî -- tre !
  Tu mau -- dis quel -- que -- fois ton ê -- tre…
  Je ve -- nais, les sens a -- gi -- tés,
  l’ho -- no -- rer de quel -- ques bon -- tés,
  sou -- pi -- rer d’a -- mour au -- près d’el -- le.
  À peine é -- tais-je à ses cô -- tés,
  el -- le s’é -- chap -- pe, la re -- bel -- le !
  Je l’ar -- rête et sai -- sis sa main :
  tu n’as vu chez nul -- le mor -- tel -- le
  l’ex -- em -- ple d’un pa -- reil dé -- dain !
  « Fa -- rouche A -- tar ! quelle est donc ton en -- vi -- e ?
  A -- vant de me ra -- vir l’hon -- neur,
  il fau -- dra m’ar -- ra -- cher la vi -- e… »
  Ses yeux pé -- til -- laient de fu -- reur.
  Fa -- rouche A -- tar !… son hon -- neur !… la sau -- va -- ge,
  ap -- pe -- lant la mort à grands cris… __
  A -- tar, en -- fin, a con -- nu le mé -- pris.

  Vingt fois j’ai vou -- lu, dans ma ra -- ge,
  é -- par -- gner moi- même à son bras…
  Al -- lons, Cal -- pi -- gi, suis mes pas.

  Sei -- gneur, pre -- nez vo -- tre si -- mar -- re.

  Rat -- tache a -- vant, mon bro -- de -- quin,
  sur le corps de cet a -- fri -- cain…

  Je sens que la fu -- reur m’é -- ga -- re !…

  Mal -- heu -- reux nègre, ab -- ject et nu,
  au lieu d’un rep -- tile in -- con -- nu,
  que du né -- ant rien ne sé -- pa -- re,
  que n’es- tu l’o -- di -- eux Ta -- ra -- re !
  Que n’es- tu l’o -- di -- eux Ta -- ra -- re !
  A -- vec quel plai -- sir, de ce flanc,
  ma main é -- pui -- se -- rait le sang !…
  Si l’in -- so -- lent pou -- vait ja -- mais con -- naî -- tre
  quels dé -- dains il vaut à son maî -- tre !…
  Et c’est pour cet in -- digne ob -- jet ;
  c’est pour lui seul qu’el -- le me bra -- ve !…
  Cal -- pi -- gi, je forme un pro -- jet :
  cou -- pons la tête à cet es -- cla -- ve ;
  dé -- fi -- gu -- re- la tout- à- fait ;
  por -- te- la de ma part toi- mê -- me.
  Dis- lui qu’en mes trans -- ports ja -- loux,
  sur -- pre -- nant i -- ci son é -- poux…


  De cet hor -- ri -- ble stra -- ta -- gè -- me,
  ah ! mon maî -- tre, qu’es -- pé -- rez- vous ?
  Quand el -- le pour -- rait s’y mé -- pren -- dre,
  en de -- vien -- drait- el -- le plus ten -- dre ?
  En l’in -- qui -- é -- tant sur ses jours,
  vous la ra -- mè -- ne -- rez tou -- jours.

  La ra -- me -- ner !… j’a -- dopte une autre i -- dé -- e.
  El -- le me croit l’âme en -- chan -- té -- e :
  mon -- trons- lui bien le peu de cas
  que je fais de ses vains ap -- pas.
  Cette or -- gueil -- leuse a dé -- dai -- gné son maî -- tre !
  ô le plus char -- mant des pro -- jet !
  Je pu -- nis l’au -- da -- ce d’un traî -- tre
  qui m’en -- le -- va le cœur de mes su -- jets ;
  et j’a -- vi -- lis la su -- perbe à ja -- mais.
  Cal -- pi -- gi ?…

  Quoi ! Sei -- gneur !

  Ju -- re- moi sur ton â -- me,
  d’o -- bé -- ir.

  Oui, sei -- gneur.

  Point de zèle in -- dis -- cret ;
  tout à l’heure,

  À l’ins -- tant.

  Prends- moi ce vil mu -- et ;
  con -- duis- le chez elle en se -- cret ;
  ap -- prends- lui que ma ten -- dre flam -- me
  la donne à ce mons -- tre pour fem -- me.
  Dis- lui bien que j’ai fait ser -- ment
  qu’el -- le n’au -- ra ja -- mais d’autre é -- poux, d’autre a -- mant.
  Je veux que l’hy -- men s’ac -- com -- plis -- se ;
  et si l’or -- gueil -- leu -- se pré -- tend
  s’y dé -- ro -- ber, promp -- te jus -- ti -- ce,
  qu’à son lit à l’ins -- tant con -- duit,
  a -- vec elle il pas -- se la nuit ;
  et qu’à tous les yeux ex -- po -- sé -- e,
  de -- main, de mon sé -- rail el -- le soit la ri -- sé -- e !
  À pré -- sent, Cal -- pi -- gi, de moi je suis con -- tent.
  Toi, par tes si -- gnes, fais que cet -- te brute ap -- pren -- ne
  le sort for -- tu -- né qui l’at -- tend.

  Ah ! sei -- gneur, ce n’est pas la pei -- ne ;
  s’il ne par -- le pas, il en -- tend.

  Ac -- com -- pa -- gne ton maître à la gar -- de pro -- chai -- ne.

  Quel heu -- reux dé -- noue -- ment !

  Mais quelle hor -- ri -- ble scè -- ne !

  Ah ! res -- pi -- rons.

  Je pense au plai -- sir que j’au -- rai,
  su -- per -- be ! quand je te ver -- rai
  au sort d’un vieux nè -- gre li -- é -- e,
  et par cent cris hu -- mi -- li -- é -- e !

  Sa -- lu -- ons tous la fière Ir -- za,
  qui, re -- gret -- tant u -- ne ca -- ba -- ne,
  aux vœux d’un roi se re -- fu -- sa :
  de ce vieux nègre elle est sul -- ta -- ne.
  Sa -- lu -- ons tous la fière Ir -- za.
  Hein ? Cal -- pi -- gi ?

  Ah ! quel plai -- sir mon maître au -- ra !

  Hein ! Cal -- pi -- gi ?

  Ah ! quel plai -- sir mon maître au -- ra,
  quand le sé -- rail re -- ten -- ti -- ra…
}
\tag #'(recit recit2 basse) {
  Sa -- lu -- ons tous la fière Ir -- za,
  qui, re -- gret -- tant u -- ne ca -- ba -- ne,
  aux vœux d’un roi se re -- fu -- sa :
  de ce vieux nègre elle est sul -- ta -- ne.
  Sa -- lu -- ons tous la fière Ir -- za.
  Ah ! quel plai -- sir \tag #'(recit basse) mon \tag #'recit2 ton maître au -- ra,
  quel plai -- sir, quel plai -- sir, quel plai -- sir, quel plai -- sir,
  ah ! quel plai -- sir \tag #'(recit basse) mon \tag #'recit2 ton maître au -- ra,
  ah ! quel plai -- sir \tag #'(recit basse) mon \tag #'recit2 ton maître au -- ra,
  quel plai -- sir, quel plai -- sir, quel plai -- sir, quel plai -- sir,
  ah ! quel plai -- sir \tag #'(recit basse) mon \tag #'recit2 ton maître au -- ra,
  ah ! quel plai -- sir \tag #'(recit basse) mon \tag #'recit2 ton maître au -- ra.
}
