\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with { \flautiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'flauti \includeNotes "flauti"
        { s1*106 s2*8 s1*30 s2*4 s1*2 s2*5 s1*29 \noHaraKiri }
      >>
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
        { s1*106 s2*8 s1*30 s2*4 s1*2 s2*5 s1*29 \noHaraKiri }
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small en Sol }
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag#'() \global \keepWithTag #'corni \includeNotes "corni"
        { s1*106 s2*8 s1*30 s2*4 s1*2 s2*5 s1*29 \noHaraKiri
          s1*30 \revertNoHaraKiri }
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \small en Ut }
        shortInstrumentName = "Tr."
      } << \keepWithTag#'() \global \keepWithTag #'trombe \includeNotes "trombe" >>
      \new DrumStaff \with {
        \override StaffSymbol.line-count = #1
        instrumentName = \markup Tambour
        shortInstrumentName = \markup Tamb.
        drumStyleTable = #percussion-style
      } <<
        \global \includeNotes "tambour"
        { s1*106 s2*8 s1*30 s2*4 s1*2 s2*5 s1*29 \noHaraKiri }
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'recit \includeNotes "voix"
      >> \keepWithTag #'recit \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'recit2 \includeNotes "voix"
      >> \keepWithTag #'recit2 \includeLyrics "paroles"
    >>
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*7\pageBreak
        s1*4\break s1*4\pageBreak
        s1*3\break s1*4\break s1*3\pageBreak
        s1*4\break s1*4\break s1*5\pageBreak
        s1*4 s2 \bar "" \break s2 s1*3\break s1*3\pageBreak
        s1*3\break s1*4\break s1*4\pageBreak
        s1*4\break s1*3\break s1*5\pageBreak
        s1*5\break s1*6\break s1*5\pageBreak
        s1*4\break s1*4\break s1*4\pageBreak
        s1*3\break s1*2\break s1 s2*3\pageBreak
        s2*5\break s1*4\break s1*3\pageBreak
        s1*4\break s1*4 s2 \bar "" \break s2 s1*3\pageBreak
        s1*3\break s1*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1*2 s2 \bar "" \break s2 s2*4\break s1*2 s2*3\pageBreak
        s2*2 s1*3\break s1*4\break s1*4\pageBreak
        s1*5\break s1*6\break s1*3 s2 \bar "" \pageBreak
        s2 s1*2\break s1*4\pageBreak
        s1*4\break s1*5\pageBreak
        s1*5\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
        s1*3 s2 \bar "" \break s2 s1*4\pageBreak
        s1*4\break s1*6\pageBreak
      }
      \modVersion {
        s1*190 s2\pageBreak <>^"toto"
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
