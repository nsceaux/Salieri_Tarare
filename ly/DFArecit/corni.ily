\clef "treble" \transposition sol
R1 |
r4 <>^"unis" re''2-\sug\ff re''4 |
re''1~ |
re''~ |
re''~ |
re''4 r r r8. sol'16\ff |
sol'2. r4 |
R1*2 |
r8 r16 <>-\sug\ff \twoVoices#'(corno1 corno2 corni) <<
  { sol'16 sol'2 }
  { mi'16 mi'2 }
>> r4 |
R1 |
r4 <>-\sug\ff \twoVoices#'(corno1 corno2 corni) <<
  { do''4 do'' }
  { do'' do'' }
>> r |
R1*11 |
r4 <>-\sug\ff \twoVoices#'(corno1 corno2 corni) <<
  { re''4 sol'' }
  { sol' re'' }
>> r4 |
\twoVoices#'(corno1 corno2 corni) <<
  { do''4 }
  { do' }
>> r4 r2 |
R1*81 R2*8 R1*30 R2*4 R1*2 R2*5 R1*28 |
r2 r8 \twoVoices#'(corno1 corno2 corni) <<
  { do''8-! mi''-! do''-! |
    sol''4 sol'' sol'' sol'' |
    re''2~ re''8 re'' re'' re'' |
    sol''4 sol'' sol'' sol'' |
    re''2~ re''8 sol'' sol'' sol'' |
    fa''4 fa''8. fa''16 re''4 re''8. re''16 |
    mi''2 mi''8 do'' mi'' do'' |
    sol''4 sol'' sol'' sol'' |
    re''2 re''8 re'' re'' re'' |
    sol''4 sol'' sol'' sol'' |
    re''2 }
  { do''8-! mi''-! do''-! |
    sol'4 sol' sol' sol' |
    sol'2~ sol'8 sol' sol' sol' |
    sol'4 sol' sol' sol' |
    sol'2~ sol'8 mi'' mi'' mi'' |
    re''4 re''8. re''16 sol'4 sol'8. sol'16 |
    do''2 do''8 do'' mi'' do'' |
    sol'4 sol' sol' sol' |
    sol'2 sol'8 sol' sol' sol' |
    sol'4 sol' sol' sol' |
    sol'2 }
  { s4. | s4-\sug\f s2.-\sug\p | s1-\sug\fp | s4-\sug\f s2.-\sug\p | s1-\sug\fp |
    s2-\sug\fp s-\sug\fp | s1\f | s-\sug\fp | s-\sug\fp | s-\sug\fp | s2-\sug\f }
>> r2 |
R1*3 |
r2 <>^"unis" do''2~ |
do''1~ |
do''~ |
<< do''~ { s2 s-\sug\cresc } >> |
do''1~ |
do''~ |
do''2\! r |
\twoVoices#'(corno1 corno2 corni) <<
  { mi''4 mi'' mi'' sol'' |
    mi''2~ mi''8 do'' do'' do'' |
    mi''4 mi'' mi'' sol'' |
    mi''2~ mi''8 do'' do'' do'' |
    re''4 re''8. re''16 mi''4 mi''8. mi''16 |
    fa''2 }
  { do''4 do'' do'' mi'' |
    do''2~ do''8 mi' mi' mi' |
    do''4 do'' do'' mi'' |
    do''2~ do''8 do'' do'' do'' |
    re''4 re''8. re''16 mi''4 mi''8. mi''16 |
    fa''2 }
  { s4-\sug\f s2.-\sug\p | s2-\sug\f s8 s4.-\sug\p |
    s4-\sug\f s2.-\sug\p | s2-\sug\f s8 s4.-\sug\p |
    s2-\sug\fp s2-\sug\fp | s2-\sug\f }
>> r2 |
\twoVoices#'(corno1 corno2 corni) <<
  { mi''4 mi'' mi'' sol'' |
    mi''2~ mi''8 do''[ do'' do''] |
    mi''4 mi'' mi'' sol'' |
    mi''2 }
  { do''4 do'' do'' mi'' |
    do''2~ do''8 mi'[ mi' mi'] |
    do''4 do'' do'' mi'' |
    do''2 }
  { s4-\sug\f s2.-\sug\p | s1 | s-\sug\fp | s2-\sug\f }
>> r2 |
R1*22 |
r2
