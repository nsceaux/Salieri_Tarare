<<
  \tag #'(recit basse) {
    \clef "alto/G_8" r4\fermata <>^\markup\character-text Calpigi crie, effrayé
    r8 si8 mi'4 r16 mi' re' mi' |
    dod'4 \tag #'recit <>^\markup\italic { (Tarare tombe la face contre terre.) } r4 r2 |
    R1*4 |
    \ffclef "bass" <>^\markup\character-text Atar d’un ton terrible
    r2 r4 la fad8 fad fad sol la4
    \ffclef "alto/G_8" <>^\markup\character-text Calpigi troublé
    r16 la la la |
    re'4 r r8 re' do' re' |
    si4 r
    \ffclef "bass" <>^\markup\character Atar
    r4 r8 sol |
    si4 si8 do' re'4 si8 sol |
    do' do' r4
    \ffclef "alto/G_8" <>^\markup\character Calpigi
    r4 r8 sol |
    do'4 r do' r |
    do'8 do' sib do' la la r4 |
    r16 la la la la la la si dod'4 r |
    mi'16 mi' mi' mi' dod'8 dod'16 la re'4 r |
    r16 la la la la la la la re'8 re' r re'16 re' |
    re'8 si16 si si8 si sold sold r4 |
    r4 r8 si mi' mi' r si |
    sold sold la si mi4 r8 sold16 la |
    si8 si r si mi' mi' r mi'16 mi' dod'8 dod' r4 la16 la dod'! dod' mi'8 fa'16 sol' |
    dod'8 dod' r16 la la si dod'8. dod'16 dod'8 re' |
    la4 r4
    \ffclef "bass" <>^\markup\character-text Atar d’un ton terrible
    r8 la re'16 re' re' la |
    si!4
    \ffclef "alto/G_8" <>^\markup\character-text Calpigi plus troublé
    r8 fa' si! si r sol |
    si4. si8 re' re' re' mi' |
    do' do' r sol16 sol do'8 do'16 do' do'8 re' |
    mi'4 r r2 |
    R1 |
    \ffclef "bass" \tag #'recit <>^\markup\character-text Atar
    lui prend le bras. Tarare est sans mouvement, prosterné.
    sol8. sol16 sol8 sol do'4 sib8 do' |
    la la r4 r2 |
    r4 fa8 sol la la16 la sib8 do' |
    fa fa r4 r2 |
    r2 r4 fa8. fa16 |
    sib4. fa8 fa4 fa8. sib16 |
    sol4 r r sol8. sib16 |
    la4. sib8 do'4 la8 fa |
    sib4 r sib8 sib sib re' |
    do'2 fa4. mib8 |
    re4 re r r8 fa |
    sib4. do'8 re' sib sol fa |
    mi4 sol8 sol16 sol do'8 sol sol sib |
    la fa r4 r2 |
    r4 r8 fa16 sol la8 la16 la do'8 la |
    fad4 r r fad8. sol16 |
    la4. la8 la4 la8 re' |
    re'8 la r la fad fad la16 sib do'16. re'32 |
    sib4 r r8 sib sib do' |
    re'4 re'8 re' re'4 do'8 sib |
    mib'8 mib' r sib sib16 sib sib sib sib8. sib16 |
    mib'4 r8 sib16 do' reb'8 reb'16 reb' reb'8 mib' |
    do' do' r4 r r8 mib |
    lab8 lab16 lab do'8 sib16 do' lab4 r |
    r8 lab lab sib do'4 r |
    r r8 do'16 sol lab4 r |
    r r8 re'16 re' si8 si r4 |
    sol8 la si do' re'4 re'8 mib'! |
    fa'1~ |
    fa'4 r\fermata r8 sol sol sol |
    re4 re8 mi fa4 re8 mi |
    \tag #'recit <>^\markup\italic { Il tire son poignard. }
    do4 r r r8 sol |
    do'4. do'16 do' mi'4 re'8 mi' |
    do' do' do'16 do' do' do' mi'4 do'8 la |
    sold4 r r r8 sold |
    si4 si8 si sold4 r8 sold16. la32 |
    mi4
    \ffclef "alto/G_8" <>^\markup\character-text Calpigi lui présente sa simare
    r8 la do'4 r8 mi' |
    do' do' re' mi' mi' la r4 |
    \ffclef "bass" <>^\markup\character Atar
    r8 mi mi mi la la la la |
    sol4 r8 sol16 mi mi4. mi8 |
    \tag #'recit <>^\markup\italic { Il met son pied sur le corps de Tarare. }
    mi4 mi8 fa do4 r |
    R1*4 |
    r4 r8 fa la16 la la la sib8 do' |
    fa8 fa r4\fermata \tag #'recit <>^\markup\italic Il regarde Tarare r8 la la8. la16 |
    re'4. la8 la4. fa8 |
    re2 r4 r8 fa |
    la4 la8 la la4 la8 la |
    sib2 r8 sib sib do' |
    re'2 sib4 sib8 sol |
    mi4 mi r sol8 sol |
    do'4 do'8 do' do'4. sol8 |
    lab4 lab r lab8 sib |
    do'4 do'8 do' do'4. lab8 |
    reb'4 reb' r lab? |
    reb' reb'8 reb' reb'4 reb'8 reb' |
    sol2. sib4 |
    mib'8 sib sib sol mib4 mib' |
    do'2 r |
    r\fermata r8 lab lab lab |
    sib,4. sib,8 re re re mib |
    fa fa r fa16 fa sib8. fa16 lab8 lab16 sib |
    sol8 sol r4 r2 |
    r4 r8 sol re'4. re'8 |
    re' re' re' re' si!4 re'8 re'16 mib' |
    fa'!2 re'4 re'8 mib' |
    do'8 do' r4\fermata r4 r8 sol16 sol |
    do'8. sol16 mi!8 mi16 sol do4 r |
    r r16 sol sol sol do'8 do' do' do' |
    la8 la r16 do' do' do' la8 la sib do' |
    fa4 r8 la16 la la8 la16 la la8 si! |
    dod' dod' r\fermata la re'4. la8 |
    la la la la fad4 r |
    \tag #'recit <>^\markup\italic { il tire le sabre de Calpigi } fad16 fad fad sol la8 la16 si! do'
    \ffclef "alto/G_8"
    \tag #'recit <>^\markup\character-text Calpigi l’arrête et l’éloigne de son ami
    \tag #'basse <>^\markup\character Calpigi
    do'16 do' do' do' do' do' do' |
    fad'8 fad' r fad'16 sol' la'8 do'16 do' re'8. la16 |
    si4 r8 si |
    re' re'16 re' re'8 re'16 si |
    la8 la la16 la la la |
    red'4 fad'8. si16 |
    mi'8 mi' r4 |
    mi'16 mi' mi' mi' mi'8 sol'16 mi' |
    re'!4 r8. re'16 |
    re' la la si do'8 re' |
    si
    \ffclef "bass" <>^\markup\character Atar
    si8 re' fa' si4 r |
    r r8 sol sol sol fa sol |
    mi mi r4 sol sol8 sol |
    do'4 do'8 sib16 do' la8 la r4 |
    r8 fa fa sol la la la sol |
    la4 r8 do'16 do' la8 la16 la sib8 do' |
    fa2 r4 r16 fa sol la |
    sib4. fa8 fa8. fa16 fa8. fa16 |
    re4 re r2 |
    sib8. sib16 sib8. sib16 sib4 lab8. sib16 |
    sol4 r r sol8. sol16 |
    sol4. sol8 do'4 do'8 do' |
    fad4 fad r8 fad fad la |
    re'4. la8 la sib do' re' |
    sib2 r8 sib do' re' |
    mib4 mib8 mib fa4 fa8 fa |
    sib,4 fa8 fa sib4
    \ffclef "alto/G_8" <>^\markup\character-text Calpigi troublé
    re'8 re' |
    sib4
    \ffclef "bass" <>^\markup\character Atar
    r8 sib16 sib sib8 fa16 fa fa fa fa sol |
    lab4
    \ffclef "alto/G_8" <>^\markup\character-text Calpigi plus troublé
    sib8. sib16 sol4 r |
    \ffclef "bass" <>^\markup\character Atar
    r8 sib16 sib sib8 sib16 sib sol4 r8 sol16 sib |
    mib4
    \ffclef "alto/G_8" <>^\markup\character-text Calpigi égaré
    mib'8 mib' sib4
    \ffclef "bass" <>^\markup\character Atar
    r8 mib |
    mib mib mib mib lab,4 r8\fermata mib |
    mib mib16 mib lab8 lab16 lab reb4 r8\fermata fa16 fa |
    sib4 fa16 fa fa fa re8 re r re |
    fa fa16 sol lab8 lab16 sib sol8 sol r sol16 sol |
    do'4 sol16 sol sol sol mi4 r |
    do'8 do'16 do' do'8 re' sib sol16 la! sib8 sol16 la |
    fa4 r8 la re' re'16 re' re'8 re'16 re' |
    sib8 sib r sib sib4 sib8 sib |
    mib' mib'16 mib' mib' sib sib do' la4 <>^"Mesuré" fa'8 fa'16 fa' |
    re'8 re' r sib16 sib |
    sib8 sib16 sib sib8. sib16 |
    sol4 sib8 sib |
    do'8 do' re' re'16 re' |
    mib'4 r sol8 sol sol lab |
    sib4 do'8 reb' do' do' r do' |
    do'8. sib16 lab8. sol16 |
    lab4 lab8. lab16 |
    reb'4 mib'8. fa'16 |
    mib'2~ |
    mib' |
    lab4 r r r8 lab16 lab |
    lab4 lab8 sib do'4 mib' |
    do'8 do' reb' mib' lab4 r |
    fa4 fa8 fa sib sib r4 |
    sib4. sib8 re' re' do' re' |
    sib sib r sib sib4 fa8 sol |
    lab4 lab8 sib sol4
    \ffclef "alto/G_8" <>^\markup\character-text Calpigi tranquilisé
    sib8. sib16 |
    mib'4 mib'16 mib' re' mib' do'8 do' r4 |
    do'8 do' sib lab re'!4 re'8 mib'? |
    sib4 r
    \ffclef "bass" <>^\markup\character Atar
    r4 r8 sol16 sol |
    sol4 sol8 la si! si16 re' si8 si16 do' |
    sol8 sol \tag #'recit <>^\markup\italic { Il se retourne pour sortir } r4 r2 |
    R1 |
    \ffclef "alto/G_8" \tag #'recit <>^\markup\character-text Calpigi \vcenter\column {
      \line { en se baissant pour ramasser la simare }
      \line { de l’Empereur, dit tout bas à Tarare. }
    }
    r4 la8. la16 la4 la8 do' |
    si4 r r2 |
    \tag #'recit <>^\markup\italic { il suit Atar. }
    R1 |
    \ffclef "tenor/G_8" <>^\markup\character-text Tarare se relève à genoux.
    re'4 re'8 re' re'4. si8 |
    sol4 sol \tag #'recit <>^\markup\italic {
      (Il ôte son masque, qui tombe à terre loin de lui.)
    }
    r2 |
    R1 |
    r4 re' r re'8 sol' |
    mi'2
    \ffclef "bass" \tag #'recit <>^\markup\character-text Atar \vcenter\column {
      \line { revient à l’appartement d’Astasie d’un }
      \line { air menaçant et dit avec une joie féroce }
    }
    r4 r8 do |
    sol4 sol8 sol sol4 sol8 do' |
    la2 r4 r8 la |
    si4 si re'8 re' si sol |
    do'2 r4 r8 do' |
    sol4 sol8 sol sol4 sol8 sol |
    mi4.( sol8) mi do' re' mi' |
    fad2 re'4 re'8 re' |
    si4 si \tag #'recit <>^\markup\italic {
      il imite le chant trivial des esclaves
    } r8 sol si sol |
    re'4 re' re' fad' |
    re'2 r8 la la la |
    re'4 re' re' fad' |
    re'2 re'8 re' re' re' |
    do'4 do' la la |
    si2 r8 sol si sol |
    re'4 re' re' fad' |
    \tag #'recit <>^\markup\italic\override#'(line-width . 70) \justify {
      (Il va, il vient. Calpigi, sous prétexte de lui donner sa simare, se met
      toujours entre lui et Tarare pour qu’il ne le voie pas sans masque.)
    }
    re'2 re'8 la la la |
    re'4 re' re' fad' |
    re'2 r |
    sol r4 re'8 re' |
    si4 r
    \ffclef "alto/G_8" <>^\markup\character-text Calpigi effrayé, feint de la joie
    re'4 re'8 re' |
    sol'4 re' re' re' |
    sol'2 r |
    \ffclef "bass" <>^\markup\character Atar
    do'2 r4 sol8 sol |
    do'2
    \ffclef "alto/G_8" <>^\markup\character Calpigi
    sol'4 sol8 sol |
    do'4 mi' sol' sol |
    do'2 sol'4 sol8 sol |
    do'4 mi' sol' sol |
    do'2 r8 mi' mi' do' |
    sol'4 sol' sol' si' |
    sol'2 r8 re' re' re' |
    sol'4 sol' sol' si' |
    sol'2 sol'8 sol' sol' sol' |
    fa'4 fa' re' re' |
    mi'2 r8 do' mi' do' |
    sol'4 sol' sol' si' |
    sol'2 sol'8 re' re' re' |
    sol'4 sol' sol' si' |
    sol'2 sol'4 sol'8 sol' |
    la4 la si si |
    do' do'8 do' re'4 re'8 re' |
    mi'4 mi'8 mi' fa'4 fa'8 fa' |
    sol'2 mi'4 mi'8 mi' |
    re'4 re' sol' sol' |
    do'2 sol'4 sol'8 sol' |
    la4 la si si |
    do'4 do'8 do' re'4 re'8 re' |
    mi'4 mi'8 mi' fa'4 fa'8 fa' |
    sol'2 mi'4 mi'8 mi' |
    re'4 re' sol' sol' |
    mi'2 mi'4 mi'8 mi' |
    re'4 re' sol' sol' |
    \tag #'recit <>^\markup\italic (ils sortent) do'2 r |
    R1*8 |
    r2
  }
  \tag #'recit2 {
    \clef "bass" R2*389 |
    r2 r8 mi8 sol do' |
    si4 si si re' |
    si2 r8 sol sol sol |
    si4 si si re' |
    si2 si8 sol sol sol |
    la4 la si si |
    do'2 r8 mi sol do' |
    si4 si si re' |
    si2 si8 sol sol sol |
    si4 si si re' |
    si2 mi4 mi8 mi |
    fa4 fa sol sol |
    la la8 la si4 si8 si |
    do'4 do'8 do' re'4 re'8 re' |
    mi'2 do'4 do'8 do' |
    fa4 fa sol sol |
    do2 mi4 mi8 mi |
    fa4 fa sol sol |
    la4 la8 la si4 si8 si |
    do'4 do'8 do' re'4 re'8 re' |
    mi'2 do'4 do'8 do' |
    fa4 fa sol sol |
    do'2 do'4 do'8 do' |
    fa4 fa sol sol |
    do2 r |
    R1*8 |
    r2
  }
>>
