\clef "bass" R1*5 |
r2 r4 r8. re16 |
re2. r4 |
R1*2 |
r8 r16 sol,-\sug\ff sol,2 r4 |
R1 |
r4 do-\sug\ff do r |
R1*11 |
r4 fa-\sug\ff fa r |
sol r r2 |
R1*8 |
r8 sib,-\sug\p re fa sib fa re fa |
r sib, re fa sib fa re fa |
r sib, mib sol sib sol mib sol |
r do fa la do' la fa do |
r re fa sib re' sib fa re' |
r do' mib' do' la fa do' do |
r re sib, re fa sib fa re |
r fa sib do' re' sib sol fa |
mi1 |
fa4-\sug\f r8 fa16 sol la8 la16 sib do'8 re'16 mib' |
fa'4 r r2 |
R1*32 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { la1 | la | la | sib~ |
    sib | sib~ | sib | lab |
    do' | reb'~ | reb'4 }
  { fa1 | fa | fa | sol~ |
    sol | sol~ | sol | fa |
    solb | fa~ | fa4 }
  { s1*3-\sug\fp | s1*6-\sug\fp | s1-\sug\f }
>> r4 r2 |
<>-\sug\f \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sib1~ | sib4 }
  { sol1~ | sol4 }
>> r4 r2 |
lab1-\sug\f |
lab4 r\fermata r2 |
R1*15 R2*8 R1*30 R2*4 R1*2 R2*3
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { mib'2~ | mib' | lab4 }
  { mib2~ | mib | lab,4 }
>> r4 r2 |
R1*27 |
r2 r8 sol-! si-! sol-! |
re-\sug\fp re' re re' \rt#2 { re re' } |
<>-\sug\fp \rt#2 { re re' } \rt#2 { re re' } |
<>-\sug\fp \rt#2 { re re' } \rt#2 { re re' } |
re8-\sug\fp re' re re' re si,[ si, si,] |
la,4-\sug\fp la, re-\sug\fp re |
sol,8-\sug\f sol sol, sol sol, sol[ si sol] |
re-\sug\fp re' re re' re re' re re' |
re-\sug-\sug\fp re' re re' re re' re re' |
re-\sug\fp re' re re' re re' re re' |
re2-\sug\f fad4-\sug\p fad |
sol-\sug\f sol fad-\sug\p fad |
sol-\sug\f sol fad-\sug\p fad |
sol-\sug\f sol fad-\sug\p fad |
sol-\sug\f sol si, si, |
do do si, si, |
do do si,-\sug\p si, |
do do si,-\sug\cresc si, |
do do si, si, |
do do si, si, |
do8 do mi sol do'4 do |
sol,8-\sug\fp sol sol, sol sol, sol sol, sol |
sol,8-\sug\fp sol sol, sol sol, sol sol, sol |
sol,8-\sug\fp sol sol, sol sol, sol sol, sol |
sol,8-\sug\fp sol sol, sol sol, sol sol, sol |
re4-\sug\fp re sol-\sug\fp sol |
do8-\sug\f do' do do' do do do do |
sol,8-\sug\fp sol sol, sol sol, sol sol, sol |
sol,8-\sug\fp sol sol, sol sol, sol sol, sol |
sol,8-\sug\fp sol sol, sol sol, sol sol, sol |
sol,2-\sug\f r |
R1 |
la4 la8 la si4-\sug\cresc si8 si |
do'4 do'8 do' re'4 re'8 re' |
mi'2 do2-\sug\ff |
fa2 sol |
do4 r r2 |
R1 |
la4 la8 la si4 si8 si |
do'4 do'8 do' re'4 re'8 re' |
mi'2 do-\sug\ff |
fa2 sol |
do' do |
fa sol |
do8 do' do do' do do' do do' |
do do' do do' do do' do do' |
do do' do do' do do' do do' |
do do' do do' do do' do do' |
do sol mi do si,4-\sug\mf si, |
do do si,-\sug\p si, |
do do r2 |
R1*2 |
r2
