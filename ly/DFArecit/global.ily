\tag #'all \key do \major
\tempo "Allegro assai" \midiTempo#100
\time 4/4 s1*27
\tempo "Andante" s1*6 \bar "||"
\tempo "Andante" \time 4/4 \tag #'all \key sib \major s1*9
\tempo "Allegro assai" s1*18
\tempo "Allegro" \tag #'all \key do \major s1*10 s2
\tempo "Allegro" s2 s1*4 s2
\tempo "Allegro Moderato" s2 s1*17 s2
\tempo "Allegro assai" s2 s1*4 s2.
\tempo "Allegro" s4 s1*7
\time 2/4 s2
\tempo "Andante agitato" s2*7
\time 4/4 s1*7
\tempo "Allegretto" s1*9 s2.
\tempo "Presto" s4 s1*7
\tempo "mesuré Andante" s1*6
\time 2/4 s2*4
\time 4/4 s1*2
\time 2/4 s2*5
\time 4/4 s1*2
\tempo "Andante" s1*9
\tempo "Allegretto" \time 4/4 s1*70 s2 \bar "|."
