\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix" #:indent 0)
   (basso #:score-template "score-part-voix")
   (oboi #:score "score-oboi")
   (flauti #:score "score-flauti")
   (fagotti #:score "score-fagotti")
   (corni #:score "score-tct")
   (trombe #:score "score-tct")
   (timpani #:score "score-tct")
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #}))
