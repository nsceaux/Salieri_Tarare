\clef "treble"
<<
  \tag #'violino1 { si''4 }
  \tag #'violino2 { sold'' }
>> r4 r2 |
r4 <<
  \tag #'violino1 {
    dod'2:16\ff re'4:16 |
    mi':16 dod':16 re':16 mi':16 |
    fad':16 re':16 mi':16 fad':16 |
    sol':16 mi':16 fad':16 sol':16 |
    fad'4
  }
  \tag #'violino2 {
    la2:16-\sug\ff la4:16 |
    la2:16 la:16 |
    la2:16 la:16 |
    la2:16 la:16 |
    la4
  }
>> r4 r r8. re'16-\sug\ff |
re'2. r4 |
R1 |
r4 <<
  \tag #'violino1 { re'' }
  \tag #'violino2 { la' }
>> r2 |
r8 r16 <re' si>-\sug\ff q2 r4 |
R1 |
r4 <sol mi'>4\ff <sol sol'>4 r |
r4 <<
  \tag #'violino1 { do''16\p do'' do'' do'' do''4 }
  \tag #'violino2 { sol'16-\sug\p sol' sol' sol' sol'4 }
>> r4 |
r2 r4 <<
  \tag #'violino1 { la'16 la' la' la' | la'4 }
  \tag #'violino2 { do'16 do' do' do' | do'4 }
>> r4 r <<
  \tag #'violino1 { dod''16 dod'' dod'' dod'' | dod''4 }
  \tag #'violino2 { sol'16 sol' sol' sol' | sol'4 }
>> r4 r <<
  \tag #'violino1 { re''4:16 | re''4 }
  \tag #'violino2 { fa'4:16 | fa' }
>> r4 r2 |
r2 r4 <<
  \tag #'violino1 { si'4:16 | si'4 }
  \tag #'violino2 { <si! sold'>4:16 | q4 }
>> r4 r2 |
R1 |
<<
  \tag #'violino1 {
    r8 r16 si'16 si'4 r8 r16 mi'' mi''4 |
    r8 r16 dod'' dod''4
  }
  \tag #'violino2 {
    r8 r16 sold'16 sold'4 r8 r16 <sold' si'> q4 |
    r8 r16 <la' mi'> q4
  }
>> r2 |
R1 |
r4 re'16(\ff fa' la' re'') re''4 r |
<si' re'>4 r r2 |
R1 | \allowPageTurn
<<
  \tag #'violino1 { <mi' do''>4 }
  \tag #'violino2 { <mi' sol>4 }
>> r4 r2 |
r4 <sol mi'>\f q q |
<<
  \tag #'violino1 { <sol mi'>1~ | q | }
  \tag #'violino2 { <sol mi'>1 | do'1 | }
>>
r4 r8 r16 <<
  \tag #'violino1 {
    do'16\f do'4 do' |
    do'1\fp |
    r4 r8 r16 do''\f do''4 mib' |
  }
  \tag #'violino2 {
    la16-\sug\f la4 la |
    la1-\sug\fp |
    r4 r8 r16 la'-\sug\f la'4 la |
  }
>>
%%
<<
  \tag #'violino1 {
    <>_"Pizzicato" r8 sib-! re'-! fa'-! sib'-! fa'-! re'-! fa'-! |
    r8 sib re' fa' sib' fa' re' fa' |
    r sib mib' sol' sib' sol' mib' sol' |
    r do' fa' la' do'' la' fa' do' |
    r re' fa' sib' re'' sib' fa' re'' |
    r do'' mib'' do'' la' fa' do'' mib' |
    r re' sib re' fa' sib' fa' re' |
    r fa' sib' do'' re'' sib' sol' fa' |
    <mi' sol>4 r r2 |
    <fa' la>4\f <>^"arco"
  }
  \tag #'violino2 {
    <sib re'>1\p~ |
    q |
    <sol mib'>~ |
    <do' mib'> |
    <sib re'> |
    do'2.( fa'4) |
    fa'1( |
    re') |
    sol |
    la4-\sug\f
  }
>> r8 fa'16 sol' la'8 la'16 sib' do''8 re''16 mib'' |
fa''4 r r2 |
<<
  \tag #'violino1 {
    fad''16\fp la' la' la' la'4:16 la'2:16 |
    la':16 la':16 |
    la':16 la':16 |
    sib'16\fp sib'' sib'' sib'' sib''4:16 sib''2:16 |
    sib'':16 sib'':16 |
    sib'':16\fp sib'':16 |
    sib'':16 sib'':16 |
    do''':16\fp do''':16 |
    do''':16 do'''4
  }
  \tag #'violino2 {
    la'16-\sug\fp fad' fad' fad' fad'4:16 fad'2:16 |
    fad':16 fad':16 |
    fad':16 fad':16 |
    sol'16-\sug\fp sol'' sol'' sol'' sol''4:16 sol''2:16 |
    lab'':16 lab'':16 |
    sol'':16-\sug\fp sol'':16 |
    sol'':16 sol'':16 |
    lab''16-\sug\fp mib'' mib'' mib'' mib''4:16 mib''2:16 |
    mib'':16 mib''4
  }
>> r4 |
r2 r8 r16 <<
  \tag #'violino1 { sol'16-\sug\f sol'4 | }
  \tag #'violino2 { do'16-\sug\f do'4 | }
>>
r2 r8 r16 <<
  \tag #'violino1 {
    do'16 do'4\fermata |
    sol16\fp re''' re''' re''' re'''4:16 re'''2:16 |
    re''':16 re''':16 |
    fa''':16\f fa''':16 |
    fa'''4
  }
  \tag #'violino2 {
    lab16 lab4\fermata |
    sol16-\sug\fp si'' si'' si'' si''4:16 si''2:16 |
    si'':16 si'':16 |
    si'':16-\sug\f si'':16 |
    si''4
  }
>> r4 r2 | \allowPageTurn
R1 |
%%
r16 do'-\sug\f re' mi' fa' sol' la' si' do''4 r |
R1*2 | \allowPageTurn
r8 r16 <<
  \tag #'violino1 { sold''16-\sug\f sold''4 }
  \tag #'violino2 { re''16-\sug\f re''4 }
>> r2 |
R1 |
<<
  \tag #'violino1 { la'4 }
  \tag #'violino2 { <mi' do'>4 }
>> r4 r2 |
R1*2 |
<<
  \tag #'violino1 { sol'1 | }
  \tag #'violino2 { <mi' do'> | }
>>
r2 r4 <<
  \tag #'violino1 {
    mi'4\f |
    fa' r r do':16\p |
    do'2:16 do':16 |
    do'4 r r do':16 |
    mi'!:16 fa':16 sol':16 mi':16 |
    fa'4
  }
  \tag #'violino2 {
    sib4-\sug\f |
    la r r la:16-\sug\p |
    sol:16 la:16 sib:16 sol:16 |
    la4 r r do':16 |
    do'2:16 do':16 |
    do'4
  }
>> r4 r2 |
r2\fermata r8. la'16 la'8. la'16 |
<<
  \tag #'violino1 {
    re''4.-\sug\fp la'8 la'4. fa'8 |
    re'8 re'16 dod' re' dod' re' dod' re'8 la fa' sol' |
    la' la'4 la' la' la'8 |
    sib'\fp re'16 mi'32 fad' sol'4. \grace do''8 sib'16 la' sib'8 do'' |
    re''2 sib'4. sol'8 |
    mi'8 sol16\sf la32 si! do'4. do'16 mi' sol'8 sol' |
    do''4. do'''8 do'''4. sol'8 |
    lab'2:16 lab':16 |
    lab':16 lab':16 |
    lab':16\f lab':16 |
    lab':16\p lab':16 |
    <sol' sib>:16\f q:16 |
    q:16 q:16 |
  }
  \tag #'violino2 {
    re''16-\sug\fp <re' la> q q q4:16 q2:16 |
    q:16 q:16 |
    q:16 q:16 |
    <sib re'>:16-\sug\fp q:16 |
    q:16 q:16 |
    <sib mi'>:16 q:16 |
    q:16 q:16 |
    <lab fa'>16 do' do' do' do'4:16 do'2:16 |
    do'':16 do'':16 |
    reb'':16-\sug\f reb'':16 |
    reb'':16-\sug\p reb'':16 |
    sib':16-\sug\f sib':16 |
    sib':16 sib':16 |
  }
>>
lab'16\f lab'' sol'' fa'' mib''? reb'' do'' sib' lab' sol' lab' sol' lab' sol' lab' sol' |
lab'4 r4\fermata r2 |
<<
  \tag #'violino1 {
    re'1\pp~ |
    re' |
    mib'4
  }
  \tag #'violino2 {
    lab1-\sug\pp~ |
    lab |
    sol4
  }
>> r4\fermata sol8-\sug\ff la!16 si! do' re' mi'! fad' |
<<
  \tag #'violino1 {
    sol'16-\sug\fp re'' re'' re'' re''4:16 re''2:16 |
    re'':16 re'':16 |
    re'':16-\sug\f re'':16 |
    <mib' sol>4
  }
  \tag #'violino2 {
    sol'16-\sug\fp si' si' si' si'4:16 si'2:16 |
    si':16 si':16 |
    si':16-\sug\f si':16 |
    do''4
  }
>> r4\fermata r2 |
r2 r8 r16 <<
  \tag #'violino1 { mi'!16\p mi'8. mi'16 | sol'4 }
  \tag #'violino2 { sol16-\sug\p sol8. sol16 | do'4 }
>> r4 r2 |
<<
  \tag #'violino1 { la'4 }
  \tag #'violino2 { do'4 }
>> r4 r2 | \allowPageTurn
R1 |
<<
  \tag #'violino1 {
    dod''4 r\fermata re''2\fp~ |
    re''1~ |
    re''2 do''4 r |
    <la' fad''>1\fp |
    <si'! sol''>4
  }
  \tag #'violino2 {
    sol'4 r\fermata <fad' la'>2-\sug\fp~ |
    q1~ |
    q2 q4 r |
    <re' do''>1-\sug\fp |
    <re' si'>4
  }
>> r4\fermata |
%%
<<
  \tag #'violino1 {
    r16 re'' re'' re'' r re'' re'' re'' |
    r red'' red'' red'' r red'' red'' red'' |
    r red'' red'' red'' r red'' red'' red'' |
    r mi'' mi'' mi'' r mi'' mi'' mi'' |
    r mi'' mi'' mi'' r mi'' mi'' mi'' |
    r re''! re'' re'' r re'' re'' re'' |
    r re'' re'' re'' r re'' re'' re'' |
  }
  \tag #'violino2 {
    r16 si' si' si' r si' si' si' |
    r la' la' la' r la' la' la' |
    r la' la' la' r la' la' la' |
    r si' si' si' r si' si' si' |
    r si' si' si' r si' si' si' |
    r la' la' la' r la' la' la' |
    r la' la' la' r la' la' la' |
  }
>>
<re' si'>4 r r <re' si' sol''>4\f |
R1 | \allowPageTurn
<sol mi'>8(-.\p q-. q-. q-.) q2:8^\dotFour |
q:8 <la fa'>:8 |
q:8 q:8 |
q:8 q:8 |
<<
  \tag #'violino1 {
    fa'4~ fa'16 sol'32\f la' sib' do'' re'' mi'' fa''8. fa''16 sol''8. la''16 |
    sib''4-\sug\p~ sib''8. fa''16 fa''8. fa''16 fa''8. fa''16 |
    re''4. fa'16( sib' re''8) sib'16( re'' fa''8) re''16( fa'' |
    sib''8.) sib'16 sib''8. sib''16 sib''4 lab''8. sib''16 |
    sol''8 mib''16( re'' mib'' re'' mib'' re'') mib''8. sib'16 sol''8. sol''16 |
    sol''4. sol''8 do'''4. sib''16*2/3 la'' sol'' |
    fad''4. la'16( re'' fad''8) re''16( fad'' la''8) fad''16( la'' |
    re'''4.) la''8 la''8. sib''16 do'''8. re'''16 |
    sib''4. re''16( sol'' sib''8.) sib''16 do'''8. re'''16 |
    mib''4. \grace fa''8 mib''16 re''32 mib'' fa''4. \grace sol''8 fa''16 mi''32 fa'' |
    sib'4
  }
  \tag #'violino2 {
    <fa' la>2 r |
    sib8*2/3\p re' fa' sib re' fa' la do' fa' la do' fa' |
    sib re' fa' sib re' fa' sib re' fa' sib re' fa' |
    re' fa' sib' re' fa' sib' re' fa' sib' re' fa' sib' |
    mib' sol' sib' mib' sol' sib' mib' sol' sib' mib' sol' sib' |
    mib' sol' sib' mib' sol' sib' mib' sol' do'' mib' sol' do'' |
    \ru#4 { re' la' do'' } |
    \ru#4 { re' fad' la' } |
    \ru#4 { sol' sib' re'' } |
    mib' sol' do'' mib' sol' do'' fa' la' do'' fa' la' do'' |
    sib4
  }
>> r4 r <<
  \tag #'violino1 { re''16 re'' re'' re'' | sib'4 }
  \tag #'violino2 { fa'16 fa' fa' fa' | fa'4 }
>> r4 r2 |
r4 <<
  \tag #'violino1 { sib'16 sib' sib' sib' sol'4 }
  \tag #'violino2 { <fa' sib>16 q q q sib4 }
>> r4 |
R1 | \allowPageTurn
r4 <<
  \tag #'violino1 { mib''16 mib'' mib'' mib'' sib'4 }
  \tag #'violino2 { sol'16 sol' sol' sol' sol'4 }
>> r4\fermata |
<<
  \tag #'violino1 { mib'2\pp~ mib'8 }
  \tag #'violino2 { sib2-\sug\pp( do'8) }
>> r8 r4 |
r4 <<
  \tag #'violino1 { lab'4 lab'8 }
  \tag #'violino2 { mib'4 fa'8 }
>> r8\fermata r4 |
<<
  \tag #'violino1 {
    sib'1_\markup\dynamic mfp ~ |
    sib'4 lab' sol'8
  }
  \tag #'violino2 {
    fa'1_\markup\tiny\dynamic mfp ~ |
    fa'4 <fa' sib>4 <sib sol'>8
  }
>> r8 r4 |
<<
  \tag #'violino1 {
    do''1_\markup\dynamic mfp ~ |
    do''2~ do''4 mi''\mf |
    fa''4 r re''2_\markup\dynamic mfp |
    re''8
  }
  \tag #'violino2 {
    sol'1_\markup\tiny\dynamic mfp ~ |
    sol'2~ sol'4 <sol' sib'>-\sug\mf |
    la'4 r la'2_\markup\tiny\dynamic mfp |
    sib'8
  }
>> r8 r4 r2 |
<<
  \tag #'violino1 { mib''2_\markup\dynamic mfp do''8 r fa''4\f | }
  \tag #'violino2 { sib'2_\markup\tiny\dynamic mfp la'8 r <fa' do''>4-\sug\f | }
>>
re''4 r\fermata |
\ru#2 { re'16(\sf fa' sib' fa') } |
mib'\sf( sib mib' sol') sib'(\sf sol' mib' sib') |
<<
  \tag #'violino1 {
    do''16(\sf lab' do'' lab') fa'(\cresc re'' fa'' re'') |
    mib''4\!
  }
  \tag #'violino2 {
    do''8-\sug\sf lab4 lab8-\sug\f |
    sol4
  }
>> r4 r2 |
r <mib' do''>4 <<
  \tag #'violino1 {
    r8 do''' |
    do'''8. sib''16 lab''8. sol''16 |
    lab''4 lab''8. lab''16 |
    reb'''4 mib'''8. fa'''16 |
    mib'''2:32\ff |
    mib''':32 |
    lab''4
  }
  \tag #'violino2 {
    r4 |
    sol'8. do'16 do'8. do'16 |
    do'4 r |
    lab'' lab'' |
    <do'' lab''>2:32-\sug\ff |
    <sib' sol''>:32 |
    <do'' lab''>4
  }
>> r4 r2 |
R1 |
r2 r8 do''\f lab' fa' |
re'4 r r2 |
R1*2 | \allowPageTurn
r2 <sib sol'>4 r |
r2 <lab' do'>4 r |
R1 |
r4 <<
  \tag #'violino1 { lab'4 sol'2 | }
  \tag #'violino2 { re'4 re'2 | }
>>
R1*2 | \allowPageTurn
mi'16(\p sol' fa' sol' mi' sol' fa' sol' mi' sol' fa' sol' mi' sol' fa' sol') |
la'( do'' si' do'' re'' do'' si' do'') la'4 r |
si'16( do'' si' do'' re'' mi'' re'' mi'' fa'' mi'' fa'' mi'' fa'' re'' do'' si') |
do'' mi'' re'' mi'' do'' mi'' re'' mi'' do''4 r8 do'' |
<<
  \tag #'violino1 {
    sol''4 sol'' sol'' si'' |
    sol''2. re''8*2/3 mi'' fad'' |
    sol''4 sol'' sol'' si'' |
    sol''2
  }
  \tag #'violino2 {
    si'4 si' si' re'' |
    si'2. si'8*2/3 do'' la' |
    si'4 si' si' re'' |
    si'2
  }
>> r2\fermata |
R1 | \allowPageTurn
mi'16( sol' fa' sol' mi' sol' fa' sol' mi' sol' fa' sol' mi' sol' fa' sol') |
la'( do'' si' do'' re'' do'' si' do'') la'4 r |
si'16( do'' si' do'' re'' mi'' re'' mi'' fa'' mi'' fa'' mi'' fa'' re'' do'' si') |
do''( mi'' re'' mi'' do'' mi'' re'' mi'') do''8-! sol''-! mi''-! do''-! |
<<
  \tag #'violino1 {
    <sol' sol>2~ q |
    q~ q8 do''([ re'' mi'']) |
    <fad' la>1 |
    <sol sol'>2~ q8 sol''[-! si''-! sol''-!] |
    re'''4\f re'''\p re''' fad''' |
    re'''2_\markup\concat\dynamic { f \tiny p } ~ re'''8 la'' la'' la'' |
    re'''4\f re'''\p re''' fad''' |
    re'''2\fp~ re'''8 re''' re''' re''' |
    do'''4\fp do'''8. do'''16 la''4\fp la''8. la''16 |
    si''16(\f re''' si'' re''' si'' re''' si'' re''') si''8 sol''[ si'' sol''] |
    re'''4-\sug\fp re''' re''' fad''' |
    re'''2\fp~ re'''8 la'' la'' la'' |
    re'''4_\markup\concat\dynamic { f \tiny p } re''' re''' fad''' |
    re'''2\f re''8*2/3-!\p mi''-! re''-! do''-! si'-! la'-! |
    sol'\f sol' sol' sol'4 re''8*2/3\p mi'' re'' do'' si' la' |
    sol'\f sol' sol' sol'4 re''8*2/3\p mi'' re'' do'' si' la' |
    sol'\f sol' sol' sol'4 re''8*2/3\p mi'' re'' do'' si' la' |
    sol' sol' sol' sol'4 sol''8*2/3\f la'' sol'' fa''! mi'' re'' |
    do'' do'' do'' do''4 sol''8*2/3 la'' sol'' fa'' mi'' re'' |
    do'' do'' do'' do''4 sol''8*2/3\p la'' sol'' fa'' mi'' re'' |
    do'' do'' do'' mi'' mi'' mi'' sol''\cresc la'' sol'' fa'' mi'' re'' |
    do'' do'' do'' mi'' mi'' mi'' sol'' la'' sol'' fa'' mi'' re'' |
    do'' do'' do'' do''4 sol''8*2/3 la'' sol'' fa'' mi'' re'' |
    do''2 r8
  }
  \tag #'violino2 {
    sol'4 re'16 sol sol sol re' sol sol sol re' sol sol sol |
    mi' sol sol sol mi' sol sol sol mi'8 mi'([ re' do']) |
    do'16( si la si) do' si la si do' si la si do' la do' la |
    si re' do' re' si re' do' re' si8 sol'[-! si'-! sol'-!] |
    <la fad' re''>4-\sug\f <fad' re''>-\sug\p q <la' fad''> |
    <la fad' re''>2-\sug\fp~ <fad' re''>8 <fad' la'> q q |
    <la fad' re''>4-\sug\f <fad' re''>-\sug\p q <la' fad''> |
    <la fad' re''>2-\sug\fp~ <fad' re''>8 re'' re'' re'' |
    mi''4-\sug\fp mi''8. mi''16 fad''4-\sug\fp fad''8. fad''16 |
    sol''16-\sug\f( si'' sol'' si'' sol'' si'' sol'' si'') sol''8 sol'[ si' sol'] |
    <la fad' re''>4-\sug\fp <fad' re''> q <la' fad''> |
    <la fad' re''>2-\sug\fp~ <fad' re''>8 <fad' la'> q q |
    <la fad' re''>4-\sug\fp <fad' re''> q <la' fad''> |
    <la fad' re''>2-\sug\f <la re'>4-\sug\p q |
    <si re'>8*2/3-\sug\f q q q4 <la re'>-\sug\p q |
    <si re'>8*2/3-\sug\f q q q4 <la re'>-\sug\p q |
    <si re'>8*2/3-\sug\f q q q4 <la re'>-\sug\p q |
    <si re'>8*2/3-\sug\f q q q4 <sol re'> q |
    <sol mi'>8*2/3 q q q4 <sol re'> q |
    <sol mi'>8*2/3 q q q4 <sol re'>8*2/3-\sug\p q q q q q |
    \ru#2 \tuplet 3/2 <sol mi'>4.:8 <>-\sug\cresc \ru#2 \tuplet 3/2 <sol re'>4.:8 |
    <sol mi'>2.*2/3:8 <sol re'>:8 |
    <sol mi'>8*2/3 q q q q q <sol re'> q q q q q |
    <sol mi'>8 do' mi' sol' do''
  }
>> do'' mi'' do'' |
<sol re' si' sol''>4\f <si' sol''>\p q <re'' si''> |
<sol re' si' sol''>2\f~ <si' sol''>8\p <<
  \tag #'violino1 { re''8 re'' re'' | }
  \tag #'violino2 { <si' re'>8 q q | }
>>
<sol re' si' sol''>4\f <si' sol''>\p q <re'' si''> |
<sol re' si' sol''>2\f~ <si' sol''>8 <<
  \tag #'violino1 {
    sol''8[\p sol'' sol''] |
    fa''4\fp fa''8. fa''16 re''4\fp re''8. re''16 |
    mi''16(\f sol'' mi'' sol'' mi'' sol'' mi'' sol'') mi''8 do'' mi'' do'' |
  }
  \tag #'violino2 {
    sol'8[-\sug\p sol' sol'] |
    la'4-\sug\fp la'8. la'16 si'4-\sug\fp si'8. si'16 |
    do''16( mi'' do'' mi'' do'' mi'' do'' mi'') do''8 mi' sol' do'' |
  }
>>
<sol re' si' sol''>4\f <si' sol''>\p q <re'' si''> |
<sol re' si' sol''>2~ <si' sol''>8 <<
  \tag #'violino1 { <re'' re'>8[ q q] | }
  \tag #'violino2 { <re' si'>[ q q] | }
>>
<sol re' si' sol''>4\fp <si' sol''> q <re'' si''> |
<sol re' si' sol''>2\f \grace la''16 sol''32\fp fad'' sol''8. sol''8 sol'' |
la'8 r la' r si' r si' r |
do''8*2/3 do' do' do' do' do' re''\cresc re' re' re' re' re' |
mi'' mi' mi' mi' mi' mi' fa'' fa' fa' fa' fa' fa' |
sol''2 <mi'' do'''>8*2/3\ff q q q q q |
<re'' do'''>2.*2/3:8 <re'' si''>:8 |
do'''4 r \grace la''8 sol''32\fp fad'' sol''8. sol''8 sol'' |
la'8 r la' r si' r si' r |
do''8*2/3 do' do' do' do' do' re'' re' re' re' re' re' |
mi'' mi' mi' mi' mi' mi' fa'' fa' fa' fa' fa' fa' |
sol''2 <mi'' do'''>8*2/3\ff q q q q q |
<re'' do'''>2.*2/3:8 <re'' si''>:8 |
<mi'' do'''>:8 q:8 |
<re'' do'''>:8 <re'' si''>:8 |
<do'' do'''>2. <<
  \tag #'violino1 {
    sol''8*2/3 la'' si'' |
    do'''4 do''' do''' mi''' |
    do'''2. sol''8*2/3 la'' si'' |
    do'''4 do''' do''' mi''' |
    <do''' mi''>2 sol''8*2/3\mf la'' sol'' fa'' mi'' re'' |
    do'' do'' do'' do''4 sol''8*2/3\p la'' sol'' fa'' mi'' re'' |
    do'' do'' do'' do''4 sol''8*2/3 la'' sol'' fa'' mi'' re'' |
    do'' do'' do'' do''4 sol''8*2/3_\markup\italic en diminuant la'' sol'' fa'' mi'' re'' |
    do''4 <sol mi' do''> q q |
    q2\fermata
  }
  \tag #'violino2 {
    mi''8*2/3 fa'' re'' |
    mi''4 <mi'' sol'> q sol'' |
    <mi'' do'' mi' sol>2. mi''8*2/3 fa'' re'' |
    mi''4 <mi'' sol'> q sol'' |
    <mi'' do'' mi' sol>2 <sol re'>4-\sug\mf q |
    <sol mi'>8*2/3 q q q4 <sol re'>4-\sug\p q |
    <sol mi'>8*2/3 q q q4 <sol re'>4 q |
    <sol mi'>8*2/3 q q q4 <sol re'>4_\markup\italic [en diminuant] q |
    mi' <mi' sol> q q |
    q2\fermata
  }
>>
