\clef "bass" si,2 r |
R1 |
sol4 r r2 |
fad1 |
r4 sol,16(\f la, si, do) re8 mi-! fad-! sol-! |
sol,4 r r sol,16( la, si, do) |
re8 mi-! fad-! sol-! sol,4 r |
r sol16( la si do') re'8 si sol fa! |
mi4 r r2 |
R1*2 |
fa4 r r2 |
\clef "tenor" r4 fa'2\sf fa'4\p |
mi'1 |
\clef "bass" r4 re\ff re r |
R1 |
r2 mi4 r |
R1 |
r2 la,16(\ff si, do re mi fad sold la) |
si!2. sold4 |
la4 r r2 |
sol1-\sug\p~ |
sol2 fa4 r |
R1*2 |
fa1\fp |
sib,4 r r2 |
R1*2 | \allowPageTurn
r4 sib,2.\ff~ |
sib,4 r r2 |
mib4-\sug\ff r r2 |
r8 mib-! re-! mib-! sol-! re-! mib-! sol-! |
la,!4 r r2 |
R1 |
sib,4 r r2 |
mi!4-\sug\mf r r2 |
fa4 r r2 |
fa2~ fa |
mi\p~ mi |
mi1 |
la4 sol,!\fz sol,2~ |
sol,1~ |
sol,2 fa,4 r |
r2 fad2\fp~ |
fad sol4 r |
R1 |
r8. sol16 sol4 r2 |
do4\f r r2 |
do4\p do do do |
do do do do |
do do do do |
do r r2 |
r fa8 r re4\fp~ |
re1~ |
re |
re4 r r2 |
do4 r r2 |
R1 | \allowPageTurn
la,4\p la, la, la, |
re re re re |
mi mi mi mi |
sold, sold, sold, sold, |
la, la, la, la, |
la, la, la, la |
sold sold sold sold |
fad fad red red |
mi mi mi mi |
mid mid\mf\cresc fad fad |
re8 re'16 dod' si8 si16 la sold8 la16 si dod'8. dod16 |
fad4. fad16\f sold la8 la16 si dod'8. dod16 |
fad4 fad\p fad fad |
mid mid mid mid |
fad r\fermata r2 |
fad4 fad fad re |
mi2. si8( sold) |
mi4( sold si re') |
\slurDashed dod'( mi' dod' la) |
fad( sold la4. re8) |
mi2( mi,) | \slurSolid
la, r |
R1 |
r2 mi2\fp~ |
mi1 |
fa4 r r2 |
r re2\fp~ |
re1 |
sol4 r r2 |
R1*2 |
sol1\fp~ |
sol~ |
sol2 fa4 r |
