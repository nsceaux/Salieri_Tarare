\clef "bass" <>^\markup\character-text Atar en entrant violemment _"parlé"
r4 r8 fad8*1/2 fad si4 si8 red' |
si4 \ffclef "alto/G_8" <>^\markup\character Calpigi
si8 dod' red'4 red'8 mi' |
si si r mi' mi' si r16 si si do' |
la4 re'8 la do'4 do'8 re' |
si si r4 r2 |
\ffclef "bass" <>^\markup\character Atar
r4 r8 sol si si r4 |
r2 r8 si re' si |
sol sol r4 r2 |
r4 r8 sol sol sol sol sol |
mi4 r8 mi mi mi mi fa |
sol4 r8 sol16 la sib4 sib8 do' |
la4
\ffclef "alto/G_8" <>^\markup\character Calpigi
r8 do'16 do' do'4 sib8 do' |
la4 r r2 |
r4 r8 sol dod'4 mi'8 dod'16 la |
re'8 re' r4
\ffclef "bass" <>^\markup\character Atar
r4 la |
fa r16 fa fa sol la4 r8 la16 la |
re'4 la8 si sold sold r4 |
sold8 sold16 sold sold8 la si si16 si si8 mi'16 si |
do'4 r r2 |
R1 |
\ffclef "alto/G_8" <>^\markup\character Calpigi
r4 r8 la16 si do'8 do'16 do' do'8 re' |
mi' mi' r4 mi'8 mi' fa' sol' |
sib4 sib8 do' la4 r8 do' |
do' do' do' do' fa'4 do'8. do'16 |
la4 sib8 do' fa8 fa r4 |
\ffclef "bass" <>^\markup\character Atar
do'4. do'8 la8. la16 do'8 do'16 re' |
sib4 r16 fa fa fa sib8 sib sib la |
sib4 r8^\markup\italic animé sib16 sib sib8. fa16 fa8 fa16 fa |
re4 r8^\markup\italic plus animé  fa lab4 lab8 lab16 lab |
re'4 r r2 |
r4 r8^\markup\italic en colère lab re' re' re' mib' |
sib sib r4 r2 |
R1 |
r4 r8^\markup\italic plus simple do' do' do' la r16 do' |
fa4 r la8 la16 la do'8 fa |
sib4 r8 sib sib sib sib re' |
do'4 r8 do' do' do' do' sol |
la4
\ffclef "alto/G_8" <>^\markup\character-text Calpigi avec sensibilité
r16 do' do' do' la4 r8 la |
<>^\markup a tempo re'4. mi'8 fa' re' si la |
sold4 r8 <>^\markup senza rigore si mi'4 r16 si si si |
sold4 r8 si16 do' re'8 re' r re'16 mi' |
do'8 do'
\ffclef "bass" <>^\markup\character Atar
dod'4~ dod' mi8 mi |
la la la dod' la4 r8 la16 si |
dod'4 dod'16 dod' dod' re' la8 la r fa |
la la16 la la8 la16 la re'4 r8 la16 la |
fad4 la16 si! do' re' si8 si r16 sol sol la |
si8. si16 do'8 re' sol4
\ffclef "alto/G_8" <>^\markup\character Calpigi
r8 si16 do' |
re'4 r sol' r8 re' |
mi' mi' r4
\ffclef "bass" <>^\markup\character Atar
r4 r8 sol |
do'2 r8 sol sol do' |
la4 la r8 la la la |
si4 si8 do' re'4 fa8 sol |
mi mi r16 do' do' re' sib8 sib r sol16 la |
sib4 sib8 do' la r la4~ |
la la8 la re'4 la8 la |
la4 sol8 la fa4 r16 la la si |
sold4 r8 mi si si si do' |
la8 la
\ffclef "alto/G_8" <>^\markup\character Calpigi
r16 do' re' mi' la4
\ffclef "bass" <>^\markup\character Atar
r8 mi |
la2 r |
%%%
<>^\markup\italic à demi voix la4. mi8 la4 la8 la |
fad4 fad r re'8 si |
sold2 mi8 fad sold la |
si4. si8 si4 mi' |
dod'2 r4 mi |
la mi8 mi la4 la8 dod' |
si4 si r8 si si dod' |
red4. fad8 si4. la8 |
sold4 sold r8 sold sold sold |
dod'4 dod' r dod' |
re' si8 si sold4 dod'8 dod' |
fad2 r |
r r8 fad la fad |
dod'4. dod'8 dod' dod' dod' dod' |
la4 fad r8\fermata la si dod' |
re'4. re'8 re' dod' si la |
sold4 mi r si8 sold |
mi4( sold) si re' |
dod'( mi') dod' la |
fad( sold) la4. re8 |
mi1 |
la,2
\ffclef "alto/G_8" <>^\markup\character Calpigi
r8 la la si |
do'!4. mi'8 do'4. do'8 |
la4
\ffclef "bass" <>^\markup\character Atar
la8 si do'!4. sol!16 sol |
mi4 sol8 la sib4 sib8 do' |
la4 r8 la la4 la8 sib |
do'4 la8 la fad fad r la |
re'4 la8 sib do'4 la8 sib |
sol4
\ffclef "alto/G_8" <>^\markup\character Calpigi
re'2 re'8 re' |
re' re' do' re' sib sib r sol |
sib16 sib sib sib do'8 re' sol4
\ffclef "bass" <>^\markup\character Atar
r8 re' |
sib4. sib8 sib sib sib sib |
la la r la16 si dod'?4. mi'8 |
dod'4 dod'8 re' la la r4 |
