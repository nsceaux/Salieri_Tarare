\key do \major \midiTempo#100
\time 4/4 s1*31
\tempo "Allegro assai" s1*7
\tempo "Andante" s1*10 s4.
\tempo "Andante" s8*5 s1*10 \bar "|."
\tempo "Allegretto" \key la \major \time 4/4 s1*22
\key la \minor s1*12 \bar "|."
