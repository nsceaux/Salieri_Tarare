\clef "alto" si2 r |
R1 |
mi'4 r r2 |
fad'1 |
r4 sol16(\f la si do') re'8 mi'-! fad'-! sol'-! |
sol4 r r sol16( la si do') |
re'8 mi'-! fad'-! sol'-! sol4 r |
r sol'16( la' si' do'') re''8 si' sol' fa'! |
mi'4 r r2 |
R1*2 |
fa4 r r2 |
r4 fa'2-\sug\sf fa'4-\sug\p |
mi'1 |
r4 re'\ff re' r |
R1 |
r2 mi'4 r |
R1 |
r2 la16(-\sug\ff si do' re' mi' fad' sold' la') |
<re' si>1 |
do'4 r r2 |
sol'1-\sug\p~ |
sol'2 do'4 r |
R1*2 |
mib1-\sug\fp |
re4 r r2 |
R1*2 |
r4 <sib lab'>2.-\sug\ff~ |
q4 r r2 |
mib'16\ff fa' sol' lab' sib' lab' sol' fa' mib'8 sib sol sib |
mib\noBeam mib'-! re'-! mib'-! sol'-! re'-! mib'-! sol'-! |
la!4 r4 r2 |
R1 |
sib4 r r2 |
mi'!4-\sug\mf r r2 |
fa'4 r r2 |
la2. la4 |
si2-\sug\p si~ |
si si |
la4 mi-\sug\fz mi2~ |
mi1~ |
mi2 re4 r |
r2 fad-\sug\fp~ |
fad sol4 r |
R1 | \allowPageTurn
r8. sol'16 sol'4 r2 |
mi'!4-\sug\f r r2 |
do'4\p do' do' do' |
do' do' do' do' |
do' do' do' do' |
do' r r2 |
r2 do'8 r re'4-\sug\fp~ |
re'1~ |
re' |
si!4 r r2 |
la4 r r2 |
R1 | \allowPageTurn
%%%
la4-\sug\p la la la |
re re re re |
mi mi mi mi |
mi mi mi mi |
mi mi mi mi |
la la la la |
sold sold sold sold |
fad fad red red |
mi mi mi mi |
mid mid-\sug\mf -\sug\cresc fad fad |
re'8 re''16 dod'' si'8 si'16 la' sold'8 la'16 si' dod''8. dod'16 |
fad'4. fad'16\f sold' la'8 la'16 si' dod''8. dod'16 |
fad'4 fad-\sug\p fad fad |
mid4 mid mid mid |
fad r\fermata r2 |
fad4 fad fad re |
mi2. si'8( sold') |
mi'4( sold' si' re'') |
dod''( mi'' dod'' la') |
\slurDashed fad'( sold' la'4. re'8) |
mi'2( mi) | \slurSolid
la r |
R1 |
r2 do'!-\sug\fp~ |
do'1 |
fa4 r r2 |
r la-\sug\fp~ |
la1 |
sol4 r r2 |
R1*2 |
sol'1-\sug\fp |
la~ |
la2 la4 r |
