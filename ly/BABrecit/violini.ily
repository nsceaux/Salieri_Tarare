\clef "treble"
<<
  \tag #'violino1 { <si' si''>2 }
  \tag #'violino2 { <fad' red''>2 }
>> r2 |
R1 |
<<
  \tag #'violino1 { mi''4 }
  \tag #'violino2 { si'4 }
>> r4 r2 |
<<
  \tag #'violino1 { re''1 | }
  \tag #'violino2 { la'1 | }
>>
r4 sol16(\f la si do') re'8 mi'-! fad'-! sol'-! |
sol4 r r sol16( la si do') |
re'8 mi'-! fad'-! sol'-! sol4 r |
r sol'16( la' si' do'') re''8 si' sol' fa'! |
mi'4 r r2 |
R1*2 |
<do' la'>4 r r2 |
r4 <<
  \tag #'violino1 {
    fa''2\sf( mi''8\p re'') |
    dod''1 |
    r4 <re' la' fa''>4\ff <re' la' la''> r |
  }
  \tag #'violino2 {
    << { la'2 la'4 | } \\ { fa'2-\sug\sf fa'4-\sug\p | } >>
    <sib' sol'>1 |
    r4 <re' la' fa''>\ff q r |
  }
>>
R1 |
r2 <si! sold' mi''>4 r |
R1 |
r2 la'16(\ff si' do'' re'' mi'' fad'' sold'' la'') |
sold''8 fa''!-! re''-! si'-! sold'-! fa'-! re'-! si-! |
do'4 r r2 |
<<
  \tag #'violino1 { mi''1\p~ | mi''2 fa''4 }
  \tag #'violino2 { sib'1-\sug\p~ | sib'2 la'4 }
>> r4 |
R1*2 |
<<
  \tag #'violino1 { <do' mib'>1\fp | <sib re'>4 }
  \tag #'violino2 { la1-\sug\fp | sib4 }
>> r4 r2 |
R1*2 |
r4 <<
  \tag #'violino1 { <re' sib' lab''>2\ff lab''16( sol'' fa'' mib'') | re''4 }
  \tag #'violino2 { <sib fa' re''>2.-\sug\ff~ | <fa' re''>4 }
>> r4 r2 |
mib''16\ff fa'' sol'' lab'' sib'' lab'' sol'' fa'' mib''8:16 sib':16 sol':16 sib':16 |
mib' mib'-! re'-! mib'-! sol'-! re'-! mib'-! sol'-! |
la!4 r r2 |
R1 |
sib4 r r2 |
<<
  \tag #'violino1 { <mi' do''>4\mf }
  \tag #'violino2 { <sol sol'>4-\sug\mf }
>> r4 r2 |
<do' la'>4 r r r8 la' |
<<
  \tag #'violino1 {
    re''8( la' re'' mi'' fa'' re'' si' la') |
    sold'2\p sold'~ |
    sold' sold' |
    la'4 dod'\fz dod'2~ |
    dod'1~ |
    dod'2( re'4)
  }
  \tag #'violino2 {
    la2. re'4 |
    re'2-\sug\p re'~ |
    re' re' |
    do'4 la\fz la2~ |
    la1~ |
    la2~ la4
  }
>> r4 |
r2 <<
  \tag #'violino1 { re'2\fp~ | re'2 re'4 }
  \tag #'violino2 { la2-\sug\fp~ | la si4 }
>> r4 |
R1 |
r8. <<
  \tag #'violino1 { <si' sol''>16 q4 }
  \tag #'violino2 { fa''16 fa''4 }
>> r2 |
<sol mi' do'' mi''>4.\f do'8 mi'4 sol' |
do''8.\p sol16 mi'8. sol16 mi'8. sol16 mi'8. sol16 |
fa'8. la16 fa'8. la16 fa'8. la16 fa'8. la16 |
<re' fa'>8. sol16 <re' fa'>8. sol16 <re' fa'>8. sol16 <re' fa'>8. sol16 |
<do' mi'>4 r r2 |
r2 <la fa'>8 r <<
  \tag #'violino1 { la'4\fp~ | la'1~ | la' | <sold' si!>4 }
  \tag #'violino2 { fa'4-\sug\fp~ | fa'1~ | fa' | mi'!4 }
>> r4 r2 |
<<
  \tag #'violino1 { <la la'>4 }
  \tag #'violino2 { <mi' la>4 }
>> r4 r2 |
R1 |
%%%
dod'8*2/3\p re' mi' dod' re' mi' dod' re' mi' dod' re' mi' |
re' mi' fad' re' mi' fad' re' mi' fad' re' mi' fad' |
si dod' re' si dod' re' si dod' re' si dod' re' |
si dod' re' si dod' re' si dod' re' si dod' re' |
dod' re' mi' dod' re' mi' dod' re' mi' dod' re' mi' |
dod' re' mi' dod' re' mi' dod' re' mi' dod' re' mi' |
si red' mi' si red' mi' si red' mi' si red' mi' |
la red' fad' la red' fad' si red' fad' si red' fad' |
si mi' sold' si mi' sold' si mi' sold' si mi' sold' |
dod' mid' sold' dod'\mf\cresc mid' sold' dod' fad' la' fad' la' dod'' |
re''8 re''16 dod'' si'8 si'16 la' sold'8 sold'16 la'32 si' dod''8. dod'16 |
fad'4. fad'16\f sold' la'8 la'16 si' dod''8. mid''16 |
fad''4 <<
  \tag #'violino1 {
    fad'8*2/3\p sold' la' fad' sold' la' fad' sold' la' |
    sold' la' si' sold' la' si' sold' la' si' sold' la' si' |
    la'4
  }
  \tag #'violino2 {
    la8*2/3-\sug\p si dod' la si dod' la si dod' |
    dod'2:8 dod':8 |
    dod'4
  }
>> r4\fermata r2 |
re'8*2/3 fad' la' re' fad' la' re' fad' la' re' fad' la' |
si mi' sold' si mi' sold' sold4 si'8( sold') |
mi'4( sold' si' re'') |
dod''( mi'' dod'' la') |
fad'( sold' la'4. re'8) |
<<
  \tag #'violino1 { mi'2( sold') | la' }
  \tag #'violino2 { mi'( si) | dod' }
>> r2 |
R1
r2 <<
  \tag #'violino1 { do''!\fp~ | do''1 | do'4 }
  \tag #'violino2 { sol'!2-\sug\fp~ | sol'1 | la'4 }
>> r4 r2 |
r2 <<
  \tag #'violino1 { <fad' la>2\fp~ | q1 | <sol sol'>4 }
  \tag #'violino2 { do'2-\sug\fp~ | do'1 | sib4 }
>> r4 r2 |
R1*2 |
<<
  \tag #'violino1 { sib'1\fp | dod'~ | dod'2 re'4 }
  \tag #'violino2 { re'1-\sug\fp | mi'!~ | mi'2 re'4 }
>> r4 |
