Lais -- se- moi, Cal -- pi -- gi !

La fu -- reur vous é -- ga -- re.
Mon maî -- tre ! ô roi d’Or -- mus ! grâ -- ce, grâce à Ta -- ra -- re !

Ta -- ra -- re ! en -- cor Ta -- ra -- re ! Un nom ab -- ject et bas,
pour ton or -- gane im -- pur, a donc bien des ap -- pas !

%Quand sa trou -- pe nous prit, au fond d’un an -- tre som -- bre,
%je dé -- fen -- dais mes jours con -- tre ces in -- hu -- mains,
%bles -- sé, prêt à pé -- rir, ac -- ca -- blé par le nom -- bre ;
%cet hom -- me gé -- né -- reux m’ar -- ra -- cha de leurs mains.
Je lui dois d’être à vous, sei -- gneur, fai -- tes- lui grâ -- ce.

Qui, moi, je souf -- fri -- rais qu’un sol -- dat eût l’au -- da -- ce
d’ê -- tre tou -- jours heu -- reux, quand son roi ne l’est pas !

À tra -- vers le tor -- rent d’Ar -- sa -- ce,
il vous a sau -- vé du tré -- pas ;
et vous l’a -- vez nom -- mé chef de vo -- tre mi -- li -- ce.

Ah ! com -- bien je l’ai re -- gret -- té !
Son or -- gueil -- leuse hu -- mi -- li -- té,
le res -- pect d’un peuple hé -- bé -- té,
son air, jus -- qu’à son nom… Cet homme est mon sup -- pli -- ce.
Où trou -- ve- t-il, dis- moi, cet -- te fé -- li -- ci -- té ?
Est- ce dans le tra -- vail, ou dans la pau -- vre -- té ?

Dans son de -- voir. Il sert a -- vec sim -- pli -- ci -- té
le ciel, les mal -- heu -- reux, la pa -- tri -- e et son maî -- tre.

Lui ? c’est un hum -- ble fas -- tu -- eux,
dont l’or -- gueil est de le pa -- raî -- tre :
l’hon -- neur d’ê -- tre cru ver -- tu -- eux
lui tient lieu du bon -- heur de l’ê -- tre :
il n’a ja -- mais trom -- pé mes yeux.

Vous trom -- per, lui, Ta -- ra -- re !

I -- ci la loi des Bra -- mes,
per -- met à tous un grand nom -- bre de fem -- mes ;
il n’en a qu’u -- ne, et s’en croit plus heu -- reux,
mais nous l’au -- rons cet ob -- jet de ses vœux ;
en la per -- dant il gé -- mi -- ra peut- ê -- tre.

Il en mour -- ra !

Tant mieux. Oui, le fils du grand- Prê -- tre,
Al -- ta -- mort a re -- çu mon or -- dre cet -- te nuit.
Il vole à la rive op -- po -- sé -- e,
a -- vec sa trou -- pe dé -- gui -- sé -- e :
en son ab -- sen -- ce il va dé -- vas -- ter son ré -- duit.
Il ra -- vi -- ra sur- tout son As -- ta -- si -- e,
il ra -- vi -- ra sur- tout son As -- ta -- si -- e,
ce mi -- ra -- cle, dit- on, des beau -- tés de l’A -- si -- e.

Eh ! quel est donc son crime, hé -- las !

D’être heu -- reux, Cal -- pi -- gi, quand son roi ne l’est pas,
de fai -- re par -- tout ses con -- quê -- tes
des cœurs que j’a -- vais au -- tre -- fois…

Ah ! pour tour -- ner tou -- tes les tê -- tes,
il faut si peu de chose aux rois !

D’a -- voir, par un ma -- nège ha -- bi -- le,
en -- traî -- né le peuple im -- bé -- ci -- le.
