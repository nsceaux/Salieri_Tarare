\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*2\break s1*4 s2 \bar "" \pageBreak
        s2 s1*4\break s1*5\break s1*4\pageBreak
        s1*4\break s1*4\break s1*5\pageBreak
        s1*4 s2 \bar "" \break s2 s1*4\break s1*3\pageBreak
        s1*4\break s1*4\break s1*4\pageBreak
        s1*4 s2 \bar "" \break s2 s1*5\break s1*5\pageBreak
        s1*6\break s1*5\break s1*3\pageBreak
        s1*3 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
