\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with {
        instrumentName = "[Hautbois]"
        shortInstrumentName = "[Htb.]"
      } <<
        \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
        \new Staff \with { \haraKiriFirst } << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small in La }
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      \new Staff \with { \bassoInstr } <<
        \global \includeNotes "basso"
        \origLayout {
          s2.*7\break s2.*7\break s2.*8\pageBreak
          s2.*5\break s2.*5\pageBreak
          s2.*5\break s2.*7\pageBreak
          s2.*8\break s2.*6\pageBreak
          s2.*5\break
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
