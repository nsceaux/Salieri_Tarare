\clef "treble" R2.*22 |
<<
  \tag #'(oboe1 oboi) {
    <>^"Solo" \slurDashed dod''2(-\sug\p si'8.) dod''16 |
    la'4( mi' la') |
    dod''2( si'8.) dod''16 |
    la'2. |
    <>^"Solo" si'2( dod''8) re'' | \slurSolid
    mi''4. la''8( mi'' dod'') |
    la'2 si'8. mi''16 |
    re''4.( si'8 dod''4) |
    \once\slurDashed dod''2( si'8.) dod''16 |
    la'4( mi' la') |
    dod''2( si'8.) dod''16 |
    la'2. |
    si'2(\f dod''8 re'') |
    mi''4. la''8([ \grace { la''16[ sold'' fad''] } sold''8 la'']) |
    la'2 \grace dod''8 si'4 |
    la' r r |
  }
  \tag #'oboe2 { R2.*16 }
>>
R2.*13 |
r4 r r8 <>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''8 |
    fad''( sol'' la'' si'' dod''' re''') |
    \tuplet 3/2 { mi''( sol'' si'') } re''4. mi''8 |
    re''4 }
  { mi''8 |
    re''( mi'' fad'' sol'' mi'' fad'') |
    si'8. sol'16 fad'4. dod''8 |
    re''4 }
>> r4 r |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { la'4\p~ la'8 dod'' mi'' la'' |
    la''8.( fad''16 la''8. fad''16 la''8. fad''16) |
    mi''4~ mi''16 fad'' sol'' fad'' sol'' mi'' re'' dod'' |
    re''4. mi''16 fad'' la'4 |
    la'\p~ la'8 dod''( mi'' la'') |
    la''8.( fad''16 la''8. fad''16 la''8. fad''16) |
    mi''4~ mi''16 fad'' sol'' fad'' sol'' mi'' re'' dod'' |
    re''2 }
  { R2.*7 | r4 r }
>> \twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''8( la''16 si'') |
    la''2 dod''8 re''16 mi'' |
    re''2 sol''8\sf la''16 si'' |
    la''2 dod''8 re''16 mi'' |
    re''4 re'' re'' |
    re''2.\fermata | }
  { si'8( la'16 sol') |
    fad'8( re' fad' la') la'4~ |
    la' fad' r |
    r8 la'4 la' la'8 |
    la'4 fad' fad' |
    fad'2.\fermata | }
  { s4\sf | }
>>
