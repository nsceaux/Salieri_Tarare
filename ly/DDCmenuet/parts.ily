\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso #:tag-notes basso)
   (oboi #:score-template "score-oboi")
   (fagotti #:tag-notes fagotti #:system-count 7)
   (silence #:on-the-fly-markup , #{\markup\tacet #})
   (corni #:instrument , #{ \markup\center-column { Cors \small en La } #}
          #:tag-global ()
          #:music , #{
\quoteViolinoI "DDCviolin"
\cueTransp "DDCviolin" do' {
  <>_"Vln" \mmRestUp s2.*3 \mmRestDown s2.*2 \mmRestUp s2.*4 \mmRestDown s2.
  \mmRestUp s2. \mmRestDown s2.*2 \mmRestUp s2.*5 \mmRestDown s2.*4
}
s2.*16
\cueTransp "DDCviolin" do' {
  \mmRestUp s2.*3 \mmRestDown s2.*2 \mmRestUp s2.*6 \mmRestDown s2.*2
  \mmRestUp s2. \mmRestDown s2. \mmRestUp s2. \mmRestCenter
}
s2.*9
\cueTransp "DDCviolin" do' {
  \mmRestUp s2.*5
}
#}))
