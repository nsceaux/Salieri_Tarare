\clef "bass" r4 re-\sug\p( dod) |
si,-! mi( re) |
dod8 re( dod re si, sol,) |
fad,( sol, fad, mi,) re,4 |
r re re, |
r re re, |
r si, sol, |
la, r r |
r re-\sug\p( dod) |
si,-! mi( re) |
dod8 re( dod re si, sol,) |
fad,( sol, fad, mi,) re,4 |
r re re, |
r re re, |
r4 la, la, |
r la, re, |
r re re, |
r dod dod, |
r si, sold, |
r la,-\sug\cresc dod |
re2\f si,4 |
mi2. |
r4 <>_"pizzicato" la, mi |
la r r |
r la, mi |
la r r |
r mi re |
dod la, r |
r mi mi |
r mi la, |
r la, mi |
la r r |
r la, mi |
la r r |
r mi\f re |
dod la, r |
r mi mi |
la, r r |
r <>^"arco" re-\sug\p( dod) |
si, mi re |
\slurDashed dod8 re( dod re si, sol,) |
fad,( sol, fad, mi,) re,4 | \slurSolid
r re re, |
r re re, |
r si, sol, |
la, r r |
r re-\sug\p( dod) |
si, mi( re) |
dod8 re( dod re si, sol,) |
\once\slurDashed fad,( sol, fad, mi,) re,4 |
r re re, |
la,2.\f |
re |
sol4 la la, |
re4 re4\p re, |
r4 la( sol |
fad re) r |
r la, la, |
r4 re( fad) |
r la(\p sol) |
fad re r |
r la, la, |
re2.~ |
re~ |
re~ |
re~ |
re~ |
re2.\fermata |
