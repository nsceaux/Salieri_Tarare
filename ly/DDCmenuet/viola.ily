\clef "alto" r4 re'-\sug\p( dod') |
si-! mi( re) |
dod8-! re'( dod' re' si sol) |
fad( sol fad mi) <<
  { re8 re' |
    \slurDashed dod'( si) la4. si8 |
    la( sol) fad4. \slurSolid } \\
  { re8 fad |
    \slurDashed la(\sf sol) fad4. sol8 |
    fad(\sf mi) re4. \slurSolid }
>> sol8 |
fad( mi re mi fad sol) |
mi r la-\sug\f( fad sol mi) |
re r re'4-\sug\p( dod') |
si8 r mi4( re) |
dod8\noBeam re'( dod' re' si sol) |
fad( sol fad mi) <<
  { re8 re' |
    dod' si la4. si8 |
    la sol fad4. sol8 |
    la4 } \\
  { re8 fad |
    la-\sug\sf sol fad4. sol8 |
    fad-\sug\sf mi re4. mi8 |
    fad4 }
>> fad4 sol |
sol2( fad4) |
fad2( sold8) la |
\once\slurDashed mi2( la8) dod' |
re4. sold8( si re) |
dod(-\sug\cresc mi la dod') mi'([ dod']) |
fad'-\sug\f( re' dod' si re' si) |
la( sold si sold) mi4 |
<>-\sug\p \ru#2 { dod'16( mi' la' mi') } re'( mi' sold' mi') |
\ru#3 { dod'16( mi' la' mi') } |
\ru#2 { dod'16 mi' la' mi' } re' mi' sold' mi' |
\ru#3 { dod'16 mi' la' mi' } |
\ru#3 { si mi' sold' mi' } |
la mi' la' mi' \ru#2 { dod' mi' la' mi' } |
\ru#2 { dod'16 mi' la' mi' } si mi' sold' mi' |
\ru#2 { si mi' sold' mi' } dod' mi' la' mi' |
\ru#2 { dod'16 mi' la' mi' } si mi' sold' mi' |
\ru#3 { dod'16 mi' la' mi' } |
\ru#2 { dod'16 mi' la' mi' } si mi' sold' mi' |
\ru#3 { dod'16 mi' la' mi' } |
\ru#3 { si mi' sold' mi' } |
la mi' la' mi' \ru#2 { dod' mi' la' mi' } |
\ru#2 { dod'16 mi' la' mi' } si mi' sold' mi' |
dod' mi' la' sold' la' si' sol' la' fad' sol' mi' fad' |
r4 re'-\sug\p( dod') |
si mi( re) |
dod8\noBeam re'( dod' re' si sol) |
fad( sol fad mi) <<
  { re8 re' |
    \slurDashed dod'( si) la4. si8 |
    la( sol) fad4. sol8 | \slurSolid } \\
  { re8 fad |
    \slurDashed la(-\sug\sf sol) fad4. sol8 |
    fad(-\sug\sf mi) re4. sol8 | \slurSolid }
>>
fad( mi re mi fad sol) |
mi8 r la-\sug\f( fad sol mi) |
re r re'4-\sug\p( dod') |
si8 r mi4( re) |
dod8\noBeam re'( dod' re' si sol) |
fad( sol fad mi) <<
  { re8 re' |
    \slurDashed dod'( si) la4. fad'8 |
    mi'( re' dod' si la sol) |
    fad( sol la si dod' re') | \slurSolid } \\
  { re8 fad |
    \slurDashed la(-\sug\sf sol) fad4. re'8\f |
    dod'( si la sol fad mi) |
    re( mi fad sol mi fad) | \slurSolid }
>>
si8. sol16 fad4. sol8 |
fad8 <<
  { fad'4 fad' fad'8 | mi'2. | fad' | } \\
  { la4-\sug\p la la8 | dod'2. | re' | }
>>
r4 <<
  { mi'4 mi' |
    \oneVoice r4 \voiceOne fad' fad' |
    mi'2. | fad' |
    \oneVoice r4 \voiceOne mi'4 mi' |
    fad'2 sol'4 |
    fad'2( mi'4) |
    fad'2 sol'4 |
    fad'2 mi'4 |
    fad'2. |
    fad'2.\fermata | } \\
  { dod'4 dod' | 
    s4 re'4 re' |
    dod'2. |
    re' |
    s4 dod'\p dod' |
    la2 si4\sf |
    la2. |
    la2 si4 |
    la2. |
    la |
    la\fermata | }
>>