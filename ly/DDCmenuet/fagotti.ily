\clef "bass" R2.*3 |
r4 r r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { re'8 |
    \slurDashed dod'( si) la4. si8 |
    la8( sol) fad4. sol8 | \slurSolid }
  { fad8 |
    \slurDashed la( sol) fad4. sol8 |
    fad( mi) re4. sol8 | \slurSolid }
  { s8 | s2.\sf | s\sf }
>> fad8( mi re mi fad sol) |
mi\noBeam la,-\sug\f la( fad sol mi) |
re4 r r |
R2.*2 |
r4 r r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { re'8 |
    dod' si la4. si8 |
    la sol fad4. sol8 |
    la4 }
  { fad8 |
    la sol fad4. sol8 |
    fad mi re4. mi8 |
    fad4 }
  { s8 | s2.\sf | s\sf }
>> fad4 sol |
sol2( fad4) |
<<
  \tag #'(fagotto1 fagotti) {
    <>^"Solo" fad2( sold8) la |
    \once\slurDashed mi2( la8) dod' |
    re4. sold8( si re) |
    dod(\cresc mi la dod') mi'([ dod']) |
    fad'-\sug\f( re' dod' si re' si) |
    la( sold si sold) mi4 |
    mi'2.-\sug\p~ |
    mi'~ |
    mi'~ |
    mi' |
    R2.*3 |
    r4 mi' mi' |
    mi'2.~ |
    mi'~ |
    mi' |
    mi' |
    R2.*2 |
    r4 mi'-\sug\f mi' |
    mi' r r |
    R2.*3 |
  }
  \tag #'fagotto2 { R2.*25 }
>>
r4 r r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { re'8 |
    \slurDashed dod'( si) la4. si8 |
    la( sol) fad4. sol8 | \slurSolid }
  { fad8 |
    \slurDashed la( sol) fad4. sol8 |
    fad( mi) re4. sol8 | \slurSolid }
  { s8 | s2.\sf | s\sf }
>>
\once\slurDashed fad8( mi re mi fad sol) |
mi\noBeam la,-\sug\f la( fad sol mi) |
re4 r r |
R2.*2 |
\clef "tenor" r4 r r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { re'8 |
    \slurDashed dod'( si) la4. fad'8 |
    mi'( re' dod' si la sol) |
    fad( sol la si dod' re') | \slurSolid }
  { fad8 |
    \slurDashed la( sol) fad4. re'8 |
    dod'( si la sol fad mi) |
    re( mi fad sol mi fad) | \slurSolid }
  { s8 | s2\sf s8 s8\f }
>>
\clef "bass" sol4 la la, |
re r r |
\clef "tenor" <>-\sug\p \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { mi'2. |
    fad' |
    s4 mi' mi' |
    s4 fad' fad' |
    mi'2. |
    fad' |
    s4 mi' mi' | }
  { dod'2. |
    re' |
    s4 dod' dod' |
    s re' re' |
    dod'2. |
    re' |
    s4 dod' dod' | }
  { s2.*2 | r4 s2 | r4 s2 | s2.*2 | r4 s2\p | }
>>
re'2.~ |
re'2 mi'8( fad'16 sol' |
fad'4 re' si8 sol) |
fad( la re' fad') mi'8[ fad'16 sol'] |
fad'4 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { fad'4 fad' | fad'2.\fermata | }
  { la4 la | la2.\fermata | }
>>
