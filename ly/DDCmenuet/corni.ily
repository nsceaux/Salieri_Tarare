\clef "treble" \transposition la
R2.*22 |
<>-\sug\p \twoVoices#'(corno1 corno2 corni) <<
  { \slurDashed mi''2( re''8.) mi''16 |
    do''4( sol' do'') |
    mi''2( re''8.) mi''16 | \slurSolid
    do''4. sol'8 sol' sol' |
    sol'2. |
    sol' |
    do''2 re''4 |
    re''2 mi''8 }
  { do''2 sol'4 |
    mi'2 mi'4 |
    do''2 sol'4 |
    mi'4. sol'8 sol' sol' |
    sol'2. |
    sol' |
    mi'2 sol'4 |
    sol'2 do''8 }
>> r8 |
\twoVoices#'(corno1 corno2 corni) <<
  { \slurDashed mi''2( re''8.) mi''16 |
    do''4( sol' do'') |
    mi''2( re''8.) mi''16 | \slurSolid
    do''4. sol'8 sol' sol' |
    sol'2. |
    sol' |
    do''2 do''4 |
    do''2. | }
  { do''2 sol'4 |
    mi'2 mi'4 |
    do''2 sol'4 |
    mi'4. sol'8 sol' sol' |
    sol'2. |
    sol' |
    mi'2 sol'4 |
    do'2. | }
  { s2.*4 <>-\sug\f }
>>
R2.*16 |
r4 <>\p \twoVoices#'(corno1 corno2 corni) <<
  { do''4 do'' | do''2. | do'' | do'' |
    do''~ | do''~ | do''~ | do'' | do''2 }
  { do''4 do'' | do''2. | do'' | do'' |
    do''~ | do''~ | do''~ | do'' | do''2 }
>> r4 |
R2.*5 |
