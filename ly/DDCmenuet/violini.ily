\clef "treble" re'\p re'4. \grace fad'8 mi'16 re' |
re'8(\sf sol') sol'4. fad'8 |
fad'( mi' mi' fad' sol' la'16 si') |
si'8( la') la'4. <<
  \tag #'violino1 {
    re''8 |
    dod''8(\sf si') la'4. si'8 |
    la'8(\sf sol') fad'4. sol'8 |
    fad'( mi' re' mi' fad' sol') |
    mi'\noBeam
  }
  \tag #'violino2 {
    fad'8 |
    la'-\sug\sf( sol') fad'4. sol'8 |
    fad'-\sug\sf( mi') re'4. si8 |
    la( dod') re'4 si |
    dod'8
  }
>> la8\f la'( fad' sol' mi') |
re'4 re'4.(\p \grace fad'8 mi'16 re') |
re'8(\sf sol') sol'4. fad'8 |
fad'( mi' mi'8. fad'16 sol'8 la'16 si') |
si'8( la') la'4. <<
  \tag #'violino1 {
    re''8 |
    dod''(\sf si') la'4. si'8 |
    la'8(\sf sol') fad'4. sol'8 |
    la'( fad' re') fad' mi' dod' |
    dod'2( re'4) |
  }
  \tag #'violino2 {
    fad'8 |
    la'(\sf sol') fad'4. sol'8 |
    fad'8(\sf mi') re'4. mi'8 |
    fad' re' la la la la |
    la2 la4 |
  }
>>
fad'2( sold'8) la' |
mi'2( la'8) dod'' |
re'4. sold'8( si' re') |
dod'(\cresc mi' la' dod'') mi''([ dod'']) |
fad''8(\f re'' dod'' si' re'' si') |
la'( sold' si' sold') mi'4 |
<<
  \tag #'violino1 {
    dod''2\p( si'8.) dod''16 |
    la'4( mi' la') |
    dod''2( si'8.) dod''16 |
    la'2. |
    si'2( dod''8) re'' |
    mi''4. \once\slurDashed la''8( mi'' dod'') |
    la'2 si'8. mi''16 |
    re''4.( si'8 dod''4) |
    dod''2( si'8.) dod''16 |
    la'4( mi' la') |
    dod''2( si'8.) dod''16 |
    la'2. |
    si'2(\f dod''8 re'') |
    mi''4. la''8([ \grace { la''16[ sold'' fad''] } sold''8 la'']) |
    la'2 \grace dod''8 si'4 |
    la'8 \grace si'
  }
  \tag #'violino2 {
    <>-\sug\p \ru#2 { dod'16( mi' la' mi') } re'( mi' sold' mi') |
    \ru#3 { dod'16( mi' la' mi') } |
    \ru#2 { dod'16 mi' la' mi' } re' mi' sold' mi' |
    \ru#3 { dod'16 mi' la' mi' } |
    \ru#3 { si mi' sold' mi' } |
    la mi' la' mi' \ru#2 { dod' mi' la' mi' } |
    \ru#2 { dod'16 mi' la' mi' } si mi' sold' mi' |
    \ru#2 { si mi' sold' mi' } dod' mi' la' mi' |
    \ru#2 { dod'16 mi' la' mi' } si mi' sold' mi' |
    \ru#3 { dod'16 mi' la' mi' } |
    \ru#2 { dod'16 mi' la' mi' } si mi' sold' mi' |
    \ru#3 { dod'16 mi' la' mi' } |
    \ru#3 { si mi' sold' mi' } |
    la mi' la' mi' \ru#2 { dod' mi' la' mi' } |
    \ru#2 { dod'16 mi' la' mi' } si mi' sold' mi' |
    dod' mi'
  }
>> la'16( sold' la' si' sol' la' fad' sol' mi' fad') |
re'4\p re'4. \grace fad'8 mi'16 re' |
re'8(\sf sol') sol'4. fad'8 |
fad'([ mi'] mi'[ fad'] sol'[ la'16 si']) |
si'8([ la']) la'4. <<
  \tag #'violino1 {
    re''8 |
    dod''(\sf si') la'4. si'8 |
    la'8(\sf sol') fad'4. sol'8 |
    fad'8( mi' re' mi' fad' sol') |
    mi'\noBeam
  }
  \tag #'violino2 {
    fad'8 |
    la'-\sug\sf( sol') fad'4. sol'8 |
    fad'-\sug\sf( mi') re'4. si8 |
    la( dod') re'4 si |
    dod'8\noBeam
  }
>> la8\f la'( fad' sol' mi') |
re'4 re'4.\p \grace fad'8 mi'16 re' |
re'8(\sf sol') sol'4. fad'8 |
fad'( mi' mi' fad' sol' la'16 si') |
si'8( la') la'4. <<
  \tag #'violino1 {
    re''8 |
    dod''8(\sf si') la'4. fad''8\f |
    mi''( re'' dod'' si' la' sol') |
    fad'8( sol' la' si' dod'' re'') |
  }
  \tag #'violino2 {
    fad'8 |
    la'-\sug\sf( sol') fad'4. re''8-\sug\f |
    dod''( si' la' sol' fad' mi') |
    re'( mi' fad' sol' mi' fad') |
  }
>>
\tuplet 3/2 { mi'8( sol' si') } re'4. <<
  \tag #'violino1 { \grace fad'8 mi' | }
  \tag #'violino2 { dod'8 | }
>>
re'8 fad'16\p re' \ru#2 { la( re' fad' re') } |
\ru#3 { la dod' mi' dod' } |
\ru#3 { la re' fad' re' } |
\ru#3 { la mi' sol' mi' } |
\ru#3 { la re' fad' re' } |
\ru#3 { la dod' mi' dod' } |
\ru#3 { la re' fad' re' } |
\ru#3 { la mi' sol' mi' } |
re'8 fad'16 re' la re' fad' re' si\sf re' sol' re' |
la re' fad' re' la re' fad' re' la' la la la |
la re' fad' re' la re' fad' re' si\sf re' sol' re' |
la re' fad' re' la re' fad' la' la' la la la |
\ru#3 { la re' fad' re' } |
la2.\fermata |
