\clef "alto/G_8" R1*6 |
<>^\markup\character-text Calpigi sans voir Atar r4 <>_\markup\right-align parlé r8 la16 la la4 la8 la |
re'4 r16 re' do' la sib4 r8 sib |
re'4 r8 fa' fa' re' re' re' |
sib8. sib16 re'8 sib mib'8 mib' r4 |
\ffclef "bass" <>^\markup\general-align #X #0.8 \override#'(baseline-skip . 2)
\right-column {
  \smallCaps Atar \italic\small\line { lui frappe sur l’épaule }
}
sib4
\ffclef "alto/G_8" <>^\markup\character-text Calpigi troublé
r8 sib sol4 r16 sib sib do' |
reb'4
\ffclef "bass" <>^\markup\character Atar
r8 sol sib sib mib' sib |
do' do'
\ffclef "alto/G_8" <>^\markup\character Calpigi
r8 sol16 sol do'8 do' do' mi' |
do'4 r8 sol do'4 r16 do' sib do' |
lab4
\ffclef "bass" <>^\markup\character-text Atar brusquement
fa8 fa16 sol lab8 lab sib do' |
fa fa
\ffclef "alto/G_8" <>^\markup\character Calpigi
r16 do' do' fa' fa'8 do' do' re' |
si!4
\ffclef "bass" <>^\markup\character-text Atar plus brusquement
r8 re' re' si! si si |
sol4 r8 sol si si r16 la si do' |
sol4 r
\ffclef "alto/G_8" <>^\markup\character-text Calpigi à part avec douleur
sol8. sol16 do'8 do'16 mi' |
do'4 r r8 do'16 do' si8 si16 mi' |
mi'8 si r si si si si do' |
re'4. re'8 re' re' re' do' |
la la r4
\ffclef "bass" <>^\markup\character-text Atar l’examinant
r4 r8 la16 la |
la4 si8 do' do'4 sol8 sol |
sib4 do'8 sol la4
\ffclef "alto/G_8" <>^\markup\character-text Calpigi affecte un air gai
r8 do' | \noBreak
la4 r16 la la sib do'8 do'16 re' mib'8 mib'16 fa' |
re'8 re' r16 re' do' sib mi'!8. mi'16 mi'8 fa' |
do' do' r4
\ffclef "bass" <>^\markup\character-text Atar en colère
r4 la8. la16 |
dod'4 r8 mi'16 mi' dod'4 r8 la16 si |
dod'4 dod'8 re' la4
\ffclef "alto/G_8" <>^\markup\character-text Calpigi à part en s’en allant
r16 la re' fad' |
re'8 re' r4 r2 |
<< R1*2 { s1 <>^\markup\right-align\italic { les Bostangis se retirent } } >>
