Les jar -- dins é -- clai -- rés ! des bos -- tan -- gis ! pour -- quoi ?
Quel autre ose au sé -- rail don -- ner des or -- dres ?…

Moi.

Sei -- gneur… puis- je sa -- voir ?…

Ma fête à ce que j’ai -- me ?

Est fi -- xé -- e à de -- main ; sei -- gneur, c’est vo -- tre loi.

Moi, je la veux à l’ins -- tant mê -- me.

Tous mes ac -- teurs sont dis -- per -- sés.

Du bruit au -- tour d’Ir -- za ; qu’on dan -- se, et c’est as -- sez.

Ô l’af -- freux con -- tre- temps ! De cet or -- dre bi -- zar -- re,
il n’est au -- cun moy -- en de pré -- ve -- nir Ta -- ra -- re !

Quel est donc ce mur -- mure in -- qui -- et et pro -- fond ?

Je dis… qu’il sem -- ble voir ces spec -- ta -- cles de Fran -- ce,
où tout va bien, pour -- vu qu’on dan -- se.

Vil chré -- tien ! o -- bé -- is ; ou ta tête en ré -- pond.

Ty -- ran fé -- ro -- ce !
