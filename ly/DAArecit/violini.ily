\clef "treble" r8 sib-!\ff re'-! fa'-! sib'16 la' sib' la' sib'8 re'' |
fa'4. sib'8 re'16 mib' fa' mib' re'8 fa' |
sib4. fad'8 sol' fad' sol' la' |
sib'16 do'' sib' la' sol'8 mib'' re'' mib'' re'' dod'' |
re''16 dod'' re'' dod'' re''8 si'! do''?16 si' do'' si' do''8 la' |
sib'16 la' sib' la' sib'8 re'' do'' sib' la' sol' |
<<
  \tag #'violino1 { re''4 }
  \tag #'violino2 { <re' la'>4 }
>> r4 r2 |
r2 <re' sib'>4 r |
<<
  \tag #'violino1 { <re'' re'>4 }
  \tag #'violino2 { lab' }
>> r4 r2 |
r2 <<
  \tag #'violino1 { <mib' mib''>4 }
  \tag #'violino2 { <sol' sib'>4 }
>> r4 |
R1*2 |
<<
  \tag #'violino1 { do''4 }
  \tag #'violino2 { <sol' sol>4 }
>> r4 r2 |
R1 |
<<
  \tag #'violino1 { do''4 }
  \tag #'violino2 { lab' }
>> r4 r2 |
R1 |
<<
  \tag #'violino1 { <re' si'!>4 }
  \tag #'violino2 { sol'4 }
>> r4 r2 |
R1 |
do''8 do'16([ si] do' si do' si) do'4 r |
r8 do'16( re' si do' la si) sold4 r |
R1*2 | \allowPageTurn
r8 la16( sold la sold la sold) la4 r |
r2 <<
  \tag #'violino1 { do''2\fp~ | do'' la'4 }
  \tag #'violino2 { sol'2-\sug\fp~ | sol' do'4 }
>> r4 |
R1 |
<<
  \tag #'violino1 { sib'4 r mi''! r | }
  \tag #'violino2 { re'4 r sol' r | }
>>
r8 mi'32\ff fa' sol' la' sib'8. sib'16 <la' mi'>4 r |
R1 |
r2 <re' la' fad''>4 r |
r16 re' mi' fad' sol' la' si' dod'' re''8 <<
  \tag #'violino1 {
    <la' fad''>8 <la' mi''>8 q |
    <la' fad''>8 re'''[ dod''' dod'''] re'''8.
  }
  \tag #'violino2 {
    <fad' re''>8 <mi' dod''> q |
    <la fad' re''> <re' la' fad''>[ <la' dod'' mi''> q] <re' la' fad''>8.
  }
>> re'16 re'8 re' |
re'2 r |
