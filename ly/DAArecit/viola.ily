\clef "alto" r8 sib\ff-! re'-! fa'-! sib'4 sib'8 re'' |
fa'4. sib'8 re'4 re'8 fa' |
sib4. re'8 re'2:8 |
re'4 sib8 sib' la' sib' la' sib' |
la'4. re'8 sol'4. do'8 |
fa'4. re''8 do'' sib' la' sol' |
fad'4 r r2 |
r sol'4 r |
fa'! r r2 |
r2 mib'4 r |
R1*2 |
mi'!4 r r2 |
R1 |
fa'4 r r2 |
R1 |
fa'4 r r2 |
R1 |
sol8 la sol fa mi4 r |
r8 do' si la sold4 r |
R1*2 | \allowPageTurn
r8 la16( sold la sold la sold) la4 r |
r2 do'-\sug\fp~ |
do'~ do'4 r |
R1 |
sib4 r sib r |
r mi'-\sug\ff <mi' la> r |
R1 |
r2 re'4 r |
r16 re' mi' fad' sol' la' si' dod'' re''8 re'' la' la' |
re' re' la la re'4 re'8 re' |
re'2 r |
