\clef "bass" r8 sib,\ff re fa sib4 sib8 re' |
fa4. sib8 re4 re8 fa |
sib,4. la,8 sib, la, sib, fad, |
sol,4. sol8 fad sol fad sol |
fad4. fa8 mi!4. mib8 |
re4. re'8 do' sib la sol |
fad4 r r2 |
r sol4 r |
fa! r r2 |
r2 mib4 r |
R1*2 |
mi!4 r r2 |
R1 |
fa4 r r2 |
R1 |
fa4 r r2 |
R1 |
mi!8 fa mi re do4 r |
r8 do si, la, sold,4 r |
R1*2 | \allowPageTurn
r8 la,16( sold, la, sold, la, sold,) la,4 r |
r2 mi\fp~ |
mi fa4 r |
R1 |
sib,4 r sib, r |
r do\ff dod r |
R1 |
r2 re4 r |
r16 re mi fad sol la si dod' re'8 re' la la |
re re la, la, re4 re8 re |
re2 r |
