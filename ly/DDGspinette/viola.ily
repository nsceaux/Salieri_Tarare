\clef "alto" r8 |
la-\sug\ff dod' mi' dod' |
la re' fad' re' |
la re' fad' re' |
la dod' mi' dod' |
la dod' mi' dod' |
sold si mi' sold' |
dod'' la' fad'16 mi' red' fad' |
mi'4 r8 mi-\sug\f |
mi'8. fad'16 sol'8 sol' |
\grace la' sol'( fad') r4 |
re''8\f si' lad' fad' |
si'4 r |
mi' r |
la r |
fad' r |
mi' r |
la8\p dod' mi' dod' |
la re' fad' re' |
la re' fad' re' |
la mi'16 dod' la8 la-\sug\f |
dod'4-\sug\sf la8 dod' |
mi'4-\sug\sf dod'8 la' |
re' re' mi' mi |
la4 r\fermata |
la'4-\sug\p-! sold'-! |
fad'-! mi'-! |
re'-! r |
dod' si8 la |
mi'4 mi |
r sold' |
la' sold' |
fad' r |
re'8 re'([ dod' si]) |
mi'4 mi |
r8 la la' la |
dod''4 si' |
la' sold' |
fad'8 r r4 |
re'8 re'([ dod' si]) |
mi'4 mi |
la4. r4 dod8-\sug\p |
mi4 mi8 fad4 sold8 |
la4( si8) dod'4 dod8 |
mi4 mi8 fad4 sold8 |
la4( si8) dod' sold([-\sug\sf la]) |
mi2.~ |
mi4 mi'8 re'4 si8 |
dod'4. la4 la8 |
la re'([ dod'] si dod' re') |
mi'4 mi8 mi4 mi8 |
la4. r4 r8 |
la16-\sug\p mi' la mi' la mi' si mi' |
do'-\sug\fp mi' do' mi' do' mi' la mi' |
sold mi' sold mi' sold mi' la mi' |
si mi' si mi' sold mi' sold mi' |
la-\sug\f mi' do' mi' la mi' do' mi' |
sol! re' si re' sol re' si re' |
do'2:16 |
do'4:16 do'8 mi' |
fa'-\sug\fp fa'8. mi'16 re' do' |
si8 si si4\trill |
mi'8-\sug\fp mi'8. re'16 do' si |
la8 la la4\trill |
re'4-\sug\f sol'8 fa' |
mi'4 do'8 mi' |
sol' sol sol sol |
do'4 r |
do'4 r |
mi16-\sug\p do' sol do' mi do' sol do' |
fa do' la do' fa do' la do' |
sol do' sib do' sol do' sib do' |
la8 fa r4 |
R2*2 | \allowPageTurn
fa16-\sug\f la fa la fa re fa re |
mi8 mi' mi r |
la16-\sug\p mi' la mi' la mi' si mi' |
do'-\sug\sf mi' do' mi' do' mi' la mi' |
sold mi' sold mi' sold mi' la mi' |
si mi' si mi' sold mi' sold mi' |
la mi' do' mi' la mi' do' mi' |
la-\sug\f mi' dod' mi' la mi' dod' mi' |
la-\sug\p re' fa' la' la' re' fa' re' |
la-\sug\f la' si' la' si' la' si' la' |
si'8-\sug\p si'4 sold'16 mi' |
mi'8 do' re' si |
mi'4 mi |
do'4-\sug\f la |
re''8 re''4 mi'8 |
mi' la'4 fa'8 |
mi' mi' mi mi |
la4 r\fermata |
la'4-!\p sold'-! |
fad' mi' |
re' r |
dod' si8 la |
mi'4 mi |
r sold' |
la' sold' |
fad' r |
re'8 re'([ dod' si]) |
mi'4 mi |
r8 la la' la |
dod''4 si' |
la' sold' |
fad'8 r r4 |
re'8 re'([ dod' si]) |
mi'4 mi |
la4. r4\fermata dod8 |
mi4 mi8 fad4 sold8 |
la4( si8 dod'4) dod8 |
mi4 mi8 fad4 sold8 |
la4( si8) dod' sold-\sug\sf([ la]) |
mi2.~ |
mi4 mi'8( re'4 si8) |
dod'4. la4 la8 |
la re'([ dod'] si dod' re') |
mi'4 mi8 mi4 mi8 |
la4. r4 r8 |
la8-\sug\f dod' mi' dod' |
la re' fad' re' |
la re' fad' re' |
la dod' mi' dod' |
la dod' mi' dod' |
sold si mi' sold' |
dod'' la' fad'16 mi' red' fad' |
mi'4 r8 mi-\sug\f |
\grace la sol fad \grace la sol fad |
\grace la sol fad r4 |
re''8-\sug\f si' lad' fad' |
si'4 r |
si' r |
la' r |
fad' r8 la' |
sold'4 r |
la8\p dod' mi' dod' |
la re' fad' re' |
la re' fad' re' |
la\noBeam mi'16 dod' la8 la-\sug\f |
\grace { la16 si } dod'4-\sug\sf( la8) dod' |
\grace { dod'16 re' } mi'4-\sug\sf( dod'8) la' |
re'8 re' mi' mi |
la4 r |
la' sold' |
fad' mi' |
re' r |
dod'4 si8 la |
mi'4 mi |
mi' re'8 re' |
dod' dod' si si |
la[ r16 dod'] si8[ r16 la] |
fad8 re'([ dod' si]) |
mi'4 mi |
r8 la la la |
mi'4-\sug\f re'8 re' |
dod' dod' si si |
la[ r16 dod'] si8[ r16 la] |
fad8 re'([ dod' si]) |
mi'4 mi |
dod'16-\sug\ff mi' dod' mi' dod' mi' dod' mi' |
re' mi' re' mi' re' mi' re' mi' |
dod' mi' dod' mi' dod' mi' dod' mi' |
re' mi' re' mi' re' mi' re' mi' |
dod' mi' dod' mi' dod' mi' dod' mi' |
re' mi' re' mi' re' mi' re' mi' |
dod' mi' dod' mi' dod' mi' dod' mi' |
re' mi' re' mi' re' mi' re' mi' |
dod'8 la[ dod' mi'] |
la'[ la dod' mi'] |
la'4 la |
la r |

