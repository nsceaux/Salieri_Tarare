\clef "treble" r8 |
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''2 |
    fad'' |
    fad'' |
    mi'' |
    mi''4 mi''8 mi'' |
    mi''4 mi''8 mi'' |
    fad''16 sold'' la'' fad'' red''8 red'' |
    mi''4 }
  { dod''2 |
    re'' |
    re'' |
    dod'' |
    dod''4 dod''8 dod'' |
    si'4 si'8 si' |
    dod'' la' fad' fad' |
    mi'4 }
>> r8 r |
mi'8.-\sug\f fad'16 sol'8 sol' |
\grace la' sol'( fad') r4 |
r8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''8[ fad'' fad''] | fad''4 }
  { fad''8[ fad'' fad''] | fad''4 }
>> r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sold''4 }
  { si' }
>> r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''16 si' la' sold' la' si' dod'' re'' |
    dod'' si' la' sold' la' si' dod'' red'' |
    mi''4 }
  { dod''16 si' la' sold' la' si' dod'' re'' |
    dod'' si' la' sold' la' si' dod'' red'' |
    mi''4 }
>> r4 |
<>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''2 |
    fad''~ |
    fad'' |
    mi''4. la'8 | }
  { dod''2 |
    re''~ |
    re'' |
    dod''4. la'8 | }
  { s2*3 | s4. s8-\sug\f }
>>
\grace { la'16 si' } <>-\sug\sf \twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''4 la'8 dod'' | }
  { dod''4 la'8 dod'' | }
>>
\grace { dod''16 re'' } <>-\sug\sf \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4 dod''8 mi'' |
    fad''16 la'' sold'' fad'' mi'' re'' dod'' si' |
    la'4 }
  { mi''4 dod''8 la' |
    re''16 fad'' mi'' re'' dod'' si' la' sold' |
    la'4 }
>> r4\fermata |
R2*16 |
R2.*11 |
R2*4 | \allowPageTurn
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''8 do'' \grace re'' do'' si'16 do'' |
    re''8 re'' \grace mi'' re''8 do''16 re'' |
    mi''4 fa'' |
    sol''4. sol''8 |
    la'' la''8. sol''16 fa'' mi'' |
    re''8 re'' re''4\trill |
    sol''8 sol''8. fa''16 mi'' re'' |
    do''8 do'' do''4\trill | }
  { la'2 |
    si' |
    do''~ |
    do''4. do''8 |
    la''8 la''8. sol''16 fa'' mi'' |
    re''8 re'' re''4\trill |
    sol''8 sol''8. fa''16 mi'' re'' |
    do''8 do'' do''4\trill | }
  { s2-\sug\f | s2*3 | s2\fp | s  | s2\fp | s }
>>
r4 r8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8 |
    sol''2 |
    mi''4. re''8 |
    do''4 }
  { si'8 |
    do''2 |
    do''4. si'8 |
    do''4 }
>> r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 }
  { do''4 }
>> r4 |
<<
  \tag #'(oboe1 oboi) {
    <>^"Solo" do''4. do''8 |
    \grace { re''16 do'' si' } do''4. fa''8 |
    mi''16 fa'' sol'' la'' sib'' sol'' fa'' mi'' |
    fa''4 r |
    R2 |
  }
  \tag #'oboe2 { R2*5 }
>>
r4 r8 <>-\tag #'oboi ^"à deux" la'8-\sug\p |
re''16 mi'' fa'' mi'' re'' do'' si' la' |
sold'4 r |
R2*5 | \allowPageTurn
<>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 }
  { dod'' }
>>
R2 |
<>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 }
  { fad'' }
>>
R2*3 | \allowPageTurn
<>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'16 si' do'' re'' mi'' fad'' sold'' la'' |
    sold''8 sold''4 sold''8 |
    la''4 fa''16 re'' do'' si' |
    do''4 si'8 si' |
    la'4 }
  { la'16 si' do'' re'' mi'' fad'' sold'' la'' |
    re''8 re''4 si'8 |
    la'4 la'16 re'' do'' si' |
    la'4 sold'8 sold' |
    la'4 }
>> r4\fermata |
R2*16 |
R2.*11 | \allowPageTurn
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''2 |
    fad'' |
    fad'' |
    mi'' |
    mi''4 mi''8 mi'' |
    mi''4 mi''8 mi'' |
    fad''16 sold'' la'' fad'' red''8 red'' |
    mi''4 }
  { dod''2 |
    re'' |
    re'' |
    dod'' |
    dod''4 dod''8 dod'' |
    si'4 si'8 si' |
    dod'' la' fad' fad' |
    sold'4 }
>> r8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi'8 }
  { mi' }
>>
\grace la'8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'8[ fad'] }
  { sol'[ fad'] }
>>
\grace la'8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'8[ fad'] }
  { sol'[ fad'] }
>>
\grace la'8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'8[ fad'] }
  { sol'[ fad'] }
>>
r4 |
r8 fad''[-\sug\f fad'' fad''] |
fad''4 r |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sold''4 }
  { si' }
>> r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''16 re'' |
    dod'' si' la' sold' la' si' dod'' re'' |
    dod'' si' la' sold' la' si' dod'' red'' |
    mi''4 }
  { mi''16 re'' |
    dod'' si' la' sold' la' si' dod'' re'' |
    dod'' si' la' sold' la' si' dod'' red'' |
    mi''4 }
>> r4 |
<>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''2 |
    fad'' |
    fad'' |
    mi''4. la'8 | }
  { dod''2 |
    re'' |
    re'' |
    dod''4. la'8 | }
  { s2*3 | s4. s8-\sug\f }
>>
\grace { la'16 si' } <>-\sug\sf \twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''4 la'8 dod'' | }
  { dod''4 la'8 dod'' | }
>>
\grace { dod''16 re'' } <>-\sug\sf \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4 dod''8 mi'' |
    fad''16 la'' sold'' fad'' mi'' re'' dod'' si' |
    la'4 }
  { mi''4 dod''8 mi'' |
    re''16 fad'' mi'' re'' dod'' si' la' sold' |
    la'4 }
>> r4 |
R2*16 | \allowPageTurn
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 |
    sold'' |
    la'' |
    sold'' |
    la'' |
    sold'' |
    la'' |
    sold'' |
    la''8 la'[ dod'' mi''] |
    la'' la'[ dod'' mi''] |
    la''4 dod''' |
    la'' }
  { dod''2 |
    si' |
    dod'' |
    si' |
    dod'' |
    si' |
    dod'' |
    si' |
    la'8 la'[ dod'' mi''] |
    la'' la'[ dod'' mi''] |
    la''4 mi'' |
    la'' }
>> r4 |
