\clef "treble" \transposition la
r8 |
<>-\sug\ff \twoVoices #'(corno1 corno2 corni) <<
  { do''2 |
    do'' |
    do'' |
    do'' |
    mi''4 mi''8 mi'' |
    re''4 re''8 re'' |
    mi''4 re''8 re'' |
    sol'4 }
  { do'2 |
    do' |
    do' |
    do' |
    sol'4 sol'8 sol' |
    sol'4 sol'8 sol' |
    do''4 re''8 re'' |
    sol'4 }
>> r8 r |
R2*4 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 s |
    mi'' s |
    do'' s |
    sol' s |
    do''2~ |
    do''~ |
    do'' |
    do''4. do''8 |
    mi''4 do''8 mi'' |
    sol''4 mi''8 }
  { sol'4 s |
    do'' s |
    do'' s |
    sol' s |
    do'2~ |
    do'~ |
    do' |
    do'4. do''8 |
    mi'4 do'8 mi' |
    sol'4 mi'8 }
  { s4 r | s r | s r | s r | s2*3\p | s4. s8-\sug\f | s2-\sug\sf | s4-\sug\sf }
>> r8 |
r4 r8 \twoVoices #'(corno1 corno2 corni) <<
  { re''8 | do''4 }
  { sol'8 | mi'4 }
>> r4\fermata |
R2*16 |
R2.*11 |
R2*30 |
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { do''2 }
  { do'' }
>>
R2 |
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { do''2 | }
  { do'' | }
>>
R2*3 |
r8 <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { do''8 do'' do'' |
    re'' re''4 re''8 |
    do''4 do'' |
    do'' re''8 re'' |
    do''4 }
  { do''8 do'' do'' |
    re'' re''4 sol'8 |
    do''4 do'' |
    do'' sol'8 sol' |
    do''4 }
>> r4\fermata |
R2*16 |
R2.*11 |
<>\f \twoVoices #'(corno1 corno2 corni) <<
  { do''2 |
    do'' |
    do'' |
    do'' |
    mi''4 mi''8 mi'' |
    re''4 re''8 re'' |
    do''4 re''8 re'' |
    sol'4 }
  { do''2 |
    do'' |
    do'' |
    do'' |
    sol'4 sol'8 sol' |
    sol'4 sol'8 sol' |
    do''4 re''8 re'' |
    sol'4 }
>> r8 r |
R2*4 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 s |
    mi'' s |
    do'' s |
    sol' s |
    do''2 |
    do'' |
    do'' |
    do''4. do''8 |
    mi''4 do''8 mi'' |
    sol''4 mi''8 }
  { sol'4 s |
    do'' s |
    do'' s |
    sol' s |
    do'2 |
    do' |
    do' |
    do'4. do'8 |
    mi'4 do'8 mi' |
    sol'4 mi'8 }
  { s4-\sug\f r | s r | s r | s r | s2*3\p | s4. s8-\sug\f | s2-\sug\sf | s4-\sug\sf }
>> r8 |
r4 r8 \twoVoices #'(corno1 corno2 corni) <<
  { re''8 | do''4 }
  { sol'8 mi'4 }
>> r4 |
R2*16 |
<>-\sug\ff \twoVoices #'(corno1 corno2 corni) <<
  { mi''2 |
    re'' |
    mi'' |
    re'' |
    mi'' |
    re'' |
    mi'' |
    re'' |
    do''8[ do' mi' sol'] |
    do''[ do' mi' sol'] |
    do''4 mi'' |
    do'' }
  { sol'2 |
    sol' |
    sol' |
    sol' |
    sol' |
    sol' |
    sol' |
    sol' |
    do''8[ do' mi' sol'] |
    do''[ do' mi' sol'] |
    do''4 sol' |
    mi' }
>> r4 |
