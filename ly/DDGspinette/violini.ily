\clef "treble" mi''8\ff |
\grace si'' la'' sold''16 la'' \grace si''8 la'' sold''16 la'' |
fad''8 fad'' fad'' sold'' |
\grace si'' la'' sold''16 la'' \grace si''8 la'' sold''16 la'' |
mi''8 mi'' mi'' la'' |
\grace { la''16 si'' } dod'''4 la''8 la'' |
mi'''4 mi''8 mi'' |
fad''16 sold'' la'' fad'' red'' mi'' fad'' red'' |
mi''4 r8 mi'\f |
mi'8. fad'16 sol'8 sol' |
\grace la' sol'( fad') r8 fad''\f |
<<
  \tag #'violino1 {
    \grace dod'''8 si'' lad''16 si'' \grace re'''8 dod''' si''16 dod''' |
    re'''8 si''16 dod''' re''' dod''' si'' la'' |
    sold''8
  }
  \tag #'violino2 {
    re''8 si' lad' fad' |
    si'4 r |
    <mi' si' sold''>8\noBeam
  }
>> si''16 la'' sold'' fad'' mi'' re'' |
dod'' si' la' sold' la' si' dod'' re'' |
dod'' si' la' sold' la' si' dod'' red'' |
mi''4. mi''8 |
\grace si''8 la''\p sold''16 la'' \grace si''8 la'' sold''16 la'' |
fad''8 fad'' fad'' sold'' |
\grace si''8 la'' sold''16 la'' \grace si''8 la'' sold''16 la'' |
mi''8[ mi'' mi''] la'\f\noBeam |
\grace { la'16[ si'] } dod''4\sf( la'8) dod''-! |
\grace { dod''16 re'' } mi''4\sf dod''8 <<
  \tag #'violino1 {
    mi''8 |
    fad''16 la'' sold'' fad'' mi'' re'' dod'' si' |
  }
  \tag #'violino2 {
    la'8 |
    re''16 fad'' mi'' re'' dod'' si' la' sold' |
  }
>>
la'4 r\fermata |
<<
  \tag #'violino1 {
    dod'''4\p si''8-! si''-! |
    la''-! la''-! sold''-! sold''-! |
    fad''-! fad''-! la''-! la''-! |
    mi''-! mi''-! re'''-! dod'''-! |
    dod'''4 si'' |
    mi'''4 re'''8-! re'''-! |
    dod'''-! dod'''-! si''-! si''-! |
    la''8 dod'''([ si'' la'']) |
    fad'' si''([ dod''' re''']) |
    la''4.( si''8) |
    \grace si''8 dod'''2 |
    mi'''4 re'''8 re''' |
    dod''' dod''' si'' si'' |
    la'' dod'''([ si'' la'']) |
    fad'' si''([ dod''' re''']) |
    la''4.( si''8) |
    la''4.
  }
  \tag #'violino2 {
    R2*3 |
    r4 sold'8-\sug\p la' |
    la'4 sold' |
    R2*3 |
    r8 fad'([ mi' re']) |
    dod' mi'' dod'' sold' |
    \grace sold'8 la'2 |
    R2*3 |
    r8 fad'([ mi' re']) |
    dod'-! mi'-! dod'-! sold-! |
    la4.
  }
>> r4 dod'8-\sug\p |
mi'4 mi'8 fad'4 sold'8 |
la'4( si'8) dod''4 dod'8 |
mi'4 mi'8 fad'4 sold'8 |
la'4( si'8) dod'' sold'([\sf la']) |
<<
  \tag #'violino1 {
    mi'4 mi''8( re''4 si'8) |
    dod''4 dod''8( si'4 sold'8) |
    la'4
  }
  \tag #'violino2 {
    mi'4. r4 r8 |
    r4 mi'8 re'4 si8 |
    dod'4
  }
>> la'8( sol'4 mi'8) |
fad'8 <<
  \tag #'violino1 {
    si'([ dod''] \grace mi'' re'' dod'' si') |
    la'4.~ la'4 sold'8 |
    la'4. r4 r8 |
  }
  \tag #'violino2 {
    fad'8([ mi'] re' mi' fad') |
    dod'4 dod'8 dod'4 si8 |
    dod'4. r4 r8 |
  }
>>
<<
  \tag #'violino1 {
    do''4.\p re''8 |
    \grace { do''16 re'' } mi''4.\sf re''16 do'' |
    si'8 si' si' do'' |
    \grace mi'' re'' do''16 re'' si'4 |
    do''8\f do'' \grace re'' do'' si'16 do'' |
    re''8 re'' \grace mi'' re'' do''16 re'' |
    \grace fa''8 mi'' re''16 mi'' \grace sol''8 fa'' mi''16 fa'' |
    sol''4. sol''8 |
    la''[\fp la''8.] sol''16[ fa'' mi''] |
    re''8 re'' re''4\trill |
    sol''8\fp sol''8. fa''16 mi'' re'' |
    do''8 do'' do''4\trill |
  }
  \tag #'violino2 {
    la16-\sug\p mi' la mi' la mi' si mi' |
    do'\fp mi' do' mi' do' mi' la mi' |
    sold mi' sold mi' sold mi' la mi' |
    si mi' si mi' sold mi' sold mi' |
    la-\sug\f mi' do' mi' la mi' do' mi' |
    sol! re' si re' sol re' si re' |
    do' do'' do'' do'' do'' do'' do'' do'' |
    do''4:16 do''8 sol' |
    la'-\sug\fp la'8. sol'16 fa' mi' |
    re'8 re' re'4\trill |
    sol'8-\sug\fp sol'8. fa'16 mi' re' |
    do'8 do' do'4\trill |
  }
>>
fa''16\f mi'' re'' do'' si' sol'' la'' si'' |
do''' si'' la'' sol'' fa'' mi'' re'' do'' |
<<
  \tag #'violino1 {
    do''4. re''8 |
    do''4 r |
    do''4 r |
  }
  \tag #'violino2 {
    do''16 sol' mi' sol' mi' sol' si re' |
    do'8 mi'16 re' do' re' do' si |
    do'8 do''16 si' do'' sol' mi' sol' |
  }
>>
do''4.\p do''8 |
\grace { re''16 do'' si' } do''4. fa''8 |
mi''16 fa'' sol'' la'' sib'' sol'' fa'' mi'' |
<<
  \tag #'violino1 {
    fa''8 fa'' \grace sol'' fa'' mi''16 re'' |
    dod''4. \grace re'''8 dod'''16\f si''32 dod''' |
    re'''4. la'8\f |
  }
  \tag #'violino2 {
    fa''4 r |
    sol'16( la' sol' la' sol' la' sol' la') |
    fa'-\sug\f la' fa' la' fa' la' fa' la' |
  }
>>
re''16 mi'' fa'' mi'' re'' do'' si' la' |
sold' la' si' sold' <<
  \tag #'violino1 {
    mi'8 r |
    do''4.\p re''8 |
    \grace { do''16 re'' } mi''4.\f re''16 do'' |
    si'8 si' si' do'' |
    \grace mi'' re''8 do''16 re'' si'4 |
    do''8 do'' do'' si'16 do'' |
    dod''2\f |
    re''8\p re'' \grace mi'' re'' dod''16 re'' |
    red''2\f |
    mi''8\p mi''8.
  }
  \tag #'violino2 {
    mi'16 re' do' si |
    la\p mi' la mi' la mi' si mi' |
    do'\sf mi' do' mi' do' mi' la mi' |
    sold mi' sold mi' sold mi' la mi' |
    si mi' si mi' sold mi' sold mi' |
    la mi' do' mi' la mi' do' mi' |
    la-\sug\f mi' dod' mi' la mi' dod' mi' |
    la-\sug\p re' fa' la' la' re' fa' re' |
    la-\sug\f la' si' la' si' la' si' la' |
    sold'8-\sug\p sold'8.
  }
>> sold''16 si'' sold'' |
la''16 mi'' re'' mi'' fa'' re'' do'' si' |
do'' mi'' la' do'' si' re'' si' sold' |
la'\f si' do'' re'' mi'' fad'' sold'' la'' |
sold''8 sold''8. si''16 la'' sold'' |
la'' mi'' re'' mi'' fa'' re'' do'' si' |
do'' mi'' la' do'' si' re'' si' sold' |
la'4 r\fermata |
<<
  \tag #'violino1 {
    dod'''4\p si''8-! si''-! |
    la'' la'' sold'' sold'' |
    fad''8 fad'' la'' la'' |
    mi'' mi'' re''' dod''' |
    dod'''4 si'' |
    mi''' re'''8 re''' |
    dod''' dod''' si'' si'' |
    la'' dod'''([ si'' la'']) |
    fad'' si''([ dod''' re''']) |
    la''4. si''8 |
    dod'''2 mi'''4 re'''8 re''' |
    dod''' dod''' si'' si'' |
    la'' dod'''([ si'' la'']) |
    fad'' si''([ dod''' re''']) |
    la''4. si''8 |
    la''4.
  }
  \tag #'violino2 {
    R2*3 |
    r8 mi'-\sug\p sold' la' |
    la'4 sold' |
    R2*3 |
    r8 fad'([ mi' re']) |
    dod' mi'' dod'' sold' |
    la'2 |
    R2*3 |
    r8 fad'([ mi' re']) |
    dod'-! mi'-! dod'-! sold-! |
    la4.
  }
>> r4\fermata dod'8 |
mi'4 mi'8 fad'4 sold'8 |
la'4( si'8) dod''4 dod'8 |
mi'4 mi'8 fad'4 sold'8 |
la'4( si'8) dod''8 sold'([\sf la']) |
<<
  \tag #'violino1 {
    mi'4 mi''8( re''4 si'8) |
    dod''4 dod''8( si'4 sold'8) |
    la'4
  }
  \tag #'violino2 {
    mi'4. r4 r8 |
    r4 mi'8( re'4 si8) |
    dod'4
  }
>> la'8 sol'4 mi'8 |
fad' <<
  \tag #'violino1 {
    si'([ dod''] \grace mi'' re'' dod'' si') |
    la'4.~ la'4 sold'8 |
    la'4.
  }
  \tag #'violino2 {
    fad'8([ mi'] re' mi' fad') |
    dod'4 dod'8 dod'4 si8 |
    dod'4.
  }
>> r8 r mi'' |
\grace si'' la''\f sold''16 la'' \grace si''8 la'' sold''16 la'' |

fad''8 fad'' fad'' sold'' |
\grace si'' la'' sold''16 la'' \grace si''8 la'' sold''16 la'' |
mi''8 mi'' mi'' la'' |
\grace { la''16 si'' } dod'''4 la''8 la'' |
mi'''4 mi''8 mi'' |
fad''16 sold'' la'' fad'' red'' mi'' fad'' red'' |
mi''4 r8 mi'\f |
\grace la' sol'( fad') \grace la' sol'( fad') |
\grace la' sol'( fad') <<
  \tag #'violino1 {
    r8 fad'' |
    \grace dod''' si''\f lad''16 si'' \grace re'''8 dod''' si''16 dod''' |
    re'''8 si''16 dod''' re''' dod''' si'' la'' |
    sold''8
  }
  \tag #'violino2 {
    r4 |
    re''8-\sug\f si' lad' fad' |
    si'4 r |
    <mi' si' sold''>8\noBeam
  }
>> si''16 la'' sold'' fad'' mi'' re'' |
dod'' si' la' sold' la' si' dod'' re'' |
dod'' si' la' sold' la' si' dod'' red'' |
mi''4. mi''8 |
\grace si'' la''8\p sold''16 la'' \grace si''8 la'' sold''16 la'' |
fad''8 fad'' fad'' sold'' |
\grace si'' la'' sold''16 la'' \grace si''8 la'' sold''16 la'' |
mi''8[ mi'' mi''] la'\f |
\grace { la'16 si' } dod''4\sf( la'8) dod'' |
\grace { dod''16 re'' } mi''4(\sf dod''8) <<
  \tag #'violino1 {
    mi''8 |
    fad''16 la'' sold'' fad'' mi'' re'' dod'' si' |
  }
  \tag #'violino2 {
    la'8 |
    re''16 fad'' mi'' re'' dod'' si' la' sold' |
  }
>>
la'4 r |
<<
  \tag #'violino1 {
    dod'''4 si''8 si'' |
    la'' la'' sold'' sold'' |
    fad'' fad'' la'' la'' |
    mi'' mi'' re''' dod''' |
    dod'''4 si'' |
  }
  \tag #'violino2 {
    R2*3 |
    r8 mi' sold' la' |
    la'4 sold' |
  }
>>
mi''4 re''8 re'' |
dod'' dod'' si' si' |
la'[ r16 dod''] si'8[ r16 la'] |
fad'8 <<
  \tag #'violino1 {
    si'8([ dod'' re'']) |
    la'4. si'8 |
    \grace si'8 dod''2 |
  }
  \tag #'violino2 {
    fad'8([ mi' re']) |
    dod' mi' dod' sold |
    \grace sold8 la2 |
  }
>>
mi''4\f re''8 re'' |
dod'' dod'' si' si' |
la'[ r16 dod''] si'8[ r16 la'] |
fad'8 <<
  \tag #'violino1 {
    si'8([ dod'' re'']) |
    la'4. sold'8 |
  }
  \tag #'violino2 {
    fad'8([ mi' re']) |
    dod'4. si8 |
  }
>>
la'16\ff si' dod'' re'' mi'' fad'' sold'' la'' |
sold'' la'' si'' la'' sold'' mi'' fad'' sold'' |
la'' sold'' fad'' mi'' re'' dod'' si' la' |
sold' la' si' la' sold' mi' fad' sold' |
la' si' dod'' re'' mi'' fad'' sold'' la'' |
sold'' la'' si'' la'' sold'' mi'' fad'' sold'' |
la'' sold'' fad'' mi'' re'' dod'' si' la' |
sold' la' si' la' sold' mi' fad' sold' |
la'8[ la' dod'' mi''] |
la''[ la' dod'' mi''] |
la''4 <la' mi'' dod'''>4 |
q r |
