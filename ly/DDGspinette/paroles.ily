\tag #'(vdessus basse) {
  Dans nos ver -- gers dé -- li -- ci -- eux,
  le mal, le mieux,
  tout se ba -- lan -- ce ;
  et si nos jeu -- nes gens sont vieux,
  tous nos vieil -- lards sont dans l’en -- fan -- ce,
  et si nos jeu -- nes gens sont vieux,
  tous nos vieil -- lards sont dans l’en -- fan -- ce.
}
\tag #'(vbasse basse) {
  Chez nous point d’im -- pos -- tu -- re ;
  en -- fants de la na -- tu -- re,
  nos ten -- dres soins
  sont pour les foins,
  et notre a -- mour pour la pâ -- tu -- re.
}
\tag #'(vdessus basse) {
  Quand l’é -- poux de -- vient in -- do -- lent,
  contre un ga -- lant
  l’a -- mour l’é -- chan -- ge ;
  et de ses vo -- la -- ges dé -- sirs,
  par des plai -- sirs,
  l’hy -- men se ven -- ge,
  et de ses vo -- la -- ges dé -- sirs,
  par des plai -- sirs,
  l’hy -- men se ven -- ge.
}
\tag #'(vbasse basse) {
  Chez nous, ja -- mais lé -- gè -- re,
  l’ac -- ti -- ve mé -- na -- gè -- re,
  pour fa -- vo -- ri
  n’a qu’un ma -- ri ;
  mais de ses fils cha -- cun est pè -- re.
}
\tag #'(vdessus basse) {
  Chez nous, sans bruit
  on se dé -- truit ;
  on brigue, on nuit ;
  mais sans scan -- da -- le.
}
\tag #'(vbasse basse) {
  Ma foi, chez nous, tout ce qu’au -- trui
  te fait, fais- lui ;
  c’est la mo -- ra -- le.
  Ma foi, chez nous, tout ce qu’au -- trui
  te fait, fais- lui ;
  c’est la mo -- ra -- le.
}
