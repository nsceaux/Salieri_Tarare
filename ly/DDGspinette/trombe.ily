\clef "treble" r8 |
R2*7 |
r4 r8 r |
R2*32 |
R2.*11 |
R2*4 |
<>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2 | re'' | do''~ | do''4 }
  { mi'2 | sol' | do''~ | do''4 }
>> r4 |
<>-\sug\fp \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2 | re''8 }
  { do''2 | re''8 }
>> r8 r4 |
<>-\sug\fp \twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'2 | do''4 }
  { sol'2 | do''4 }
>> r4 |
r <>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''4 | mi''2~ | mi''4. re''8 | do''4 }
  { sol'4 | do''2~ | do''4. sol'8 | mi'4 }
>> r4 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 }
  { mi'4 }
>> r4 |
R2*6 |
<>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8 re''16 re'' re''8 re'' | mi''4 }
  { re''8 re''16 re'' re''8 re'' | mi''4 }
>> r4 |
R2*16 |
R2*16
R2.*11 R2*7 r4 r8 r8 R2*16 R2*28
