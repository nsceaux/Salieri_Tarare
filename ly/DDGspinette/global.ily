\tag #'all \key la \major
\tempo "Allegro" \midiTempo#120
\time 2/4 \partial 8 s8 s2*7 s4. \bar ":|.|:"
s8 s2*16 \bar "|."
\tempo "un poco piu Allegro" s2*16
\time 6/8 s2 \tempo "un poco Andante" s4 s2.*10 \bar "|."
\time 2/4 \tempo\markup { \concat { 1 \super o } Tempo }
\tag #'all \key do \major s2*15 \alternatives s2 s2 \bar ".|:" s2*24 \bar ":|."
\tag #'all \key la \major s2*16
\time 6/8 s2.*11 \bar "|.|:"
\time 2/4 \grace s8 s2*7 s4. \bar ":|.|:" s8 s2*16 \bar ":|."
s2*28 \bar "|."
