\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso #:tag-notes basso)
   (oboi #:score-template "score-oboi")
   (corni #:score "score-tc")
   (trombe #:score "score-tc")
   (fagotti #:notes "basso" #:tag-notes fagotti
            #:music , #{
s8 <>^"[Basses]" s2*40 s2.*11 <>^\markup\translate #'(-1 . 0) \right-align Bassons
s2*4 <>^"[Tutti]" s2*21
<>^"Bassons" s2*4 <>^"[Tutti]" s2*12
s2*16 s4. s4 s8^"[Basses]" s2.*10
<>^"Bassons avec les Basses" s2*24
s2*5 <>^"[Basses]" s2*11 <>^"[Bassons]" #})
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
