\clef "bass" r8 |
la,\ff dod mi dod |
la, re fad re |
la, re fad re |
la, dod mi dod |
la, dod mi dod |
sold, si, mi sold |
la la si si, |
mi4 r8 mi\f |
mi8. fad16 sol8 sol |
\grace la sol( fad) r4 |
R2*2 |
mi4 r |
la r |
fad r |
mi r |
la,8\p dod mi dod |
la, re fad re |
la, re fad re |
la, mi16[ dod] la,8 la,\f |
dod4-\sug\sf( la,8) dod |
mi4\sf( dod8) dod |
re8 re mi mi |
la,4 r\fermata |
R2*16 | \allowPageTurn
r4 r8 r4 dod8\p |
mi4 mi8 fad4 sold8 |
la4( si8) dod'4 dod8 |
mi4 mi8 fad4 sold8 |
la4( si8) dod' sold([\sf la]) |
mi2.~ |
mi |
la4 r8 dod4 dod8 |
re re([ dod] si, dod re) |
mi4 mi8 mi4 mi,8 |
la,4. r4 r8 |
<<
  \tag #'fagotti {
    <>^"Solo" do'4.\p re'8 |
    \grace { do'16 re' } mi'4.\sf re'16 do' |
    si8 si si do' |
    \grace mi' re' do'16 re' si4 |
  }
  \tag #'basso { R2*4 }
>>
la8\f la la la |
sol!2:8 |
do8 do re re |
mi4. mi8 |
fa\fp fa8. mi16 re do |
si,8 si, si,4\trill |
mi8\fp mi8. re16 do si, |
la,8 la, la,4\trill |
re\f sol8 fa |
mi4 do8 mi |
sol4 sol, |
do r |
do r |
<<
  \tag #'fagotti { R2*6 }
  \tag #'basso {
    <>^"Violoncelli" mi16\p do' sol do' mi do' sol do' |
    fa do' la do' fa do' la do' |
    sol do' sib do' sol do' sib do' |
    la4 fa8 r |
    R2*2 |
    <>^"Tutti"
  }
>>
fa16\f la fa la fa re fa re |
mi8 mi' mi r |
<<
  \tag #'fagotti {
    do'4. re'8 |
    \grace { do'16 re' } mi'4. re'16 do' |
    si8 si si do' |
    \grace mi' re' do'16 re' si4 |
  }
  \tag #'basso { R2*4 }
>>
la8\p la la la |
sol2\f |
fa8\p fa fa fa |
fad2\f |
mi8\p mi4 re8 |
do4 re |
mi mi, |
do4\f la, |
si,8 si4 re'8 |
do' do re re |
mi mi mi, mi, |
la,4 r\fermata |
R2*16 | \allowPageTurn
r4 r8 r4\fermata dod8-\sug\p |
mi4 mi8 fad4 sold8 |
la4( si8) dod'4 dod8 |
mi4 mi8 fad4 sold8 |
la4( si8) dod' sold([\sf la]) |
mi2.~ |
mi |
la,4 r8 dod4 dod8 |
re re([ dod] si, dod re) |
mi4 mi8 mi4 mi,8 |
la,4. r4 r8 |
la,8\f dod mi dod |
la, re fad re |
la, re fad re |
la, dod mi dod |
la, dod mi dod |
sold, si, mi sold |
la4 si8 si, |
mi4 r8 mi\f |
\grace la sol( fad) \grace la sol( fad) |
\grace la sol( fad) fad r |
R2*2 |
mi4\f r |
la r |
fad4. fad8 |
mi4 r |
la,8\p dod mi dod |
la, re fad re |
la, re fad re |
la, mi16 dod la,8 la,\f |
dod4\sf( la,8) dod |
mi4(\sf dod8) dod |
re re mi mi, |
la,4 r |
R2*5 | \allowPageTurn
dod'4 si |
la sold |
fad r |
re8 re([ dod si,]) |
mi4 mi, |
r8 la, la la, |
dod'4\f si |
la sold |
fad r |
re8 re([ dod si,]) |
mi4 mi, |
<<
  \tag #'fagotti {
    \clef "tenor" <>-\sug\ff <<
      { mi'2 | mi' | mi' | mi' | mi' | mi' | mi' | mi' | mi'8 } \\
      { dod'2 | re' | dod' | re' | dod' | re' | dod' | re' | dod'8 }
    >> \clef "bass"
  }
  \tag #'basso {
    la2:16\ff |
    la:16 |
    la:16 |
    la:16 |
    la:16 |
    la:16 |
    la:16 |
    la:16 |
    la8
  }
>> la,8 dod mi |
la la, dod mi |
la4 la, |
la, r |
