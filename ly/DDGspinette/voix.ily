<<
  \tag #'(vdessus basse) {
    \clef "soprano/treble" r8 |
    R2*7 |
    r4 r8 r |
    R2*16 |
    dod''4 si'8 si' |
    la' la' sold' sold' |
    fad' fad' la' la' |
    mi' mi' re'' dod'' |
    dod''4 si' |
    mi'' re''8 re'' |
    dod'' dod'' si' si' |
    la' dod'' si' la' |
    fad' si' dod'' re'' |
    la'4.( si'8) |
    dod''4 r |
    mi'' re''8 re'' |
    dod'' dod'' si' si' |
    la' dod'' si' la' |
    fad' si' dod'' re'' |
    la'4.\melisma si'8\melismaEnd |
    la'4.
  }
  \tag #'vbasse { \clef "bass" r8 | R2*40 | r4 r8 }
>>
<<
  \tag #'vdessus { r4 r8 | R2.*10 }
  \tag #'(vbasse basse) {
    \tag #'basse \ffclef "bass"
    r4 dod8 |
    mi4 mi8 fad4 sold8 |
    la4( si8) dod'4 dod8 |
    mi4 mi8 fad4 sold8 |
    la4( si8) dod'4 r8 |
    r r mi' re'4 si8 |
    dod'4 dod'8 si4 sold8 |
    la4 la8 sol4 mi8 |
    fad si[ dod'] \grace mi' re'[ dod'] si |
    la4.\melisma la4 si8\melismaEnd |
    la4. r4 r8 |
  }
>>
R2*41 |
<<
  \tag #'(vdessus basse) {
    \tag #'basse \clef "soprano/treble"
    dod''4 si'8 si' |
    la' la' sold' sold' |
    fad' fad' la' la' |
    mi' mi' re'' dod'' |
    dod''4 si' |
    mi'' re''8 re'' |
    dod'' dod'' si' si' |
    la' dod'' si' la' |
    fad' si' dod'' re'' |
    la'4.( si'8) |
    dod''4 r |
    mi'' re''8 re'' |
    dod'' dod'' si' si' |
    la' dod'' si' la' |
    fad' si' dod'' re'' |
    la'4.( si'8) |
    la'4.
  }
  \tag #'vbasse { R2*16 r4 r8 }
>>
<<
  \tag #'vdessus { r4 r8 | R2.*10 }
  \tag #'(vbasse basse) {
    \tag #'basse \ffclef "bass"
    r4\fermata dod8 |
    mi4 mi8 fad4 sold8 |
    la4( si8) dod'4 dod8 |
    mi4 mi8 fad4 sold8 |
    la4( si8) dod'4 r8 |
    r4 mi'8 re'4 si8 |
    dod'4 dod'8 si4 sold8 |
    la4 la8 sol4 mi8 |
    fad si[ dod'] re'[ dod'] si |
    la4.~ la4\melisma si8\melismaEnd |
    la4. r4 r8 |
  }
>>
R2*7 |
r4 r8 r |
R2*16 |
<<
  \tag #'(vdessus basse) {
    \tag #'basse \clef "soprano/treble"
    dod''4 si'8 si' |
    la' la' sold' sold' |
    fad' fad' la' la' |
    mi' mi' re'' dod'' |
    dod''4 si' |
  }
  \tag #'vbasse { R2*5 }
>>
<<
  \tag #'vdessus { R2*12 }
  \tag #'(vbasse basse) {
    \tag #'basse \ffclef "bass"
    mi'4 re'8 re' |
    dod' dod' si si |
    la8. dod'16 si8 r16 la |
    fad8 si dod' re' |
    la4.\melisma si8\melismaEnd |
    dod'4 r |
    <>^\markup\character Les Paysans mi' re'8 re' |
    dod' dod' si si |
    la8. dod'16 si8 r16 la |
    fad8 si dod' re' |
    la4.\melisma si8\melismaEnd |
    la4 r4 |
  }
>>
R2*11 |
