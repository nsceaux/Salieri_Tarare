\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with { \oboiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small en La }
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag#'() \global \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \small en Ut }
        shortInstrumentName = "Tr."
      } <<
        \keepWithTag#'() \global \keepWithTag #'trombe \includeNotes "trombe"
      >>
      \new Staff \with { \fagottiInstr \haraKiriFirst } <<
        { \startHaraKiri s8 s2*40 s2.*11
          \stopHaraKiri s2*17 s2*24
          \startHaraKiri s2*16 s2.*11
          \stopHaraKiri s2*24
          \startHaraKiri s2*16 \stopHaraKiri
        }
        \global \keepWithTag #'fagotti \includeNotes "basso"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with { \haraKiri } <<
      \new Staff \with {
        instrumentName = \markup\center-column {
          \smallCaps Spinette en Bergère de cour
        }
      } \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\center-column { Paysan grossier }
      } \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \keepWithTag #'basso \includeNotes "basso"
      \origLayout {
        s8 s2*7\pageBreak
        s2*8\break s2*9\break s2*7\pageBreak
        s2*6\break s2*3 s2.*2\break s2.*6\pageBreak
        s2.*3 s2*4\break s2*9\break s2*8\pageBreak
        s2*8\break s2*7\break s2*7\pageBreak
        s2*7\break s2*7\break s2.*7\pageBreak
        s2.*4 s2*3\break s2*9\break s2*10\pageBreak
        s2*6\break s2*6\break \grace s8 s2*6\pageBreak
        s2*5\break
      }
      \modVersion {
        s8 s2*24\break
        s2*16 s2.*11\break
        s2*17 s2*24\break
        s2*16 s2.*11\break
        \grace s8 s2*24\break
        s2*16\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
