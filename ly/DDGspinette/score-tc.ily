\score {
  \new StaffGroup \with { \haraKiri } <<
    \new Staff \with {
      instrumentName = \markup\center-column { Trompettes \small en Ut }
      shortInstrumentName = "Tr."
    } <<
      { s8 s2*40\break s2.*11 }
      \keepWithTag#'() \global \keepWithTag #'trombe \includeNotes "trombe"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Cors \small en La }
      shortInstrumentName = "Cor."
    } <<
      \keepWithTag#'() \global \keepWithTag #'corni \includeNotes "corni"
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 10\mm
    system-count = 9
  }
}
