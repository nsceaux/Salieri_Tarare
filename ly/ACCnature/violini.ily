\clef "treble" <<
  \tag #'violino1 {
    sol'4 \grace { lab'16[ sol' fa'] } sol'8. lab'16 sib'8. sib'16 do''8. re''16 |
    mib''2. mib''4 |
    do'' do''8. do'''16 \grace re'''8 do''' sib'' \grace do''' sib''8. lab''16 |
    sol''8*2/3[-! sol'' lab''] sib''[\f sol''-! mib'']-! sol''[-! mib''-! sib']-! mib''[-! sib'-! sol']-! |
    <mib' sol>1\p~ |
    mib'8. mib'16 sol'8. sib'16 reb''8. sol''16 sib''8. reb''16 |
    do''8*2/3[ lab''(\f sib''] do'''[) lab''-! mib'']-!
  }
  \tag #'violino2 {
    \ru#2 { sib8*2/3[ mib' sol'] } \ru#2 { sib[ re' fa'] } |
    \ru#2 { sol[ do' mib'] } \ru#2 { sol[ sib mib'] } |
    lab[ do' mib'] do'[ mib' lab'] re'[ fa' sib'] re'[ fa' sib'] |
    << \ru#12 { sib[ mib' sol'] } { s4 s2.-\sug\f <>-\sug\p } >> |
    lab8*2/3[ lab''(-\sug\f sib''] do'''[) lab''-! mib'']-!
  }
>> lab''[-! mib''-! do'']-! lab'[ mib' do'] |
lab4. <<
  \tag #'violino1 {
    do''8\p do''4. re''8 |
    si'2.
  }
  \tag #'violino2 {
    mib'8-\sug\p mib'4. lab'8 |
    sol'2.
  }
>> r4 |
r2 r4 <si' re' sol>\f |
