\clef "alto" \ru#2 { sib8*2/3[ mib' sol'] } \ru#2 { sib[ re' fa'] } |
\ru#2 { sol[ do' mib'] } \ru#2 { sol[ sib mib'] } |
lab[ do' mib'] do'[ mib' lab'] re'[ fa' sib'] re'[ fa' sib'] |
<< \ru#12 { sib[ mib' sol'] } { s4 s2.-\sug\f <>-\sug\p } >> |
lab8*2/3[ lab' sib'] do''[ lab' mib'] lab'[ mib' do'] lab[ mib do] |
lab4. lab8 lab4. fa'8 |
re'2. r4 |
r2 r4 sol'-\sug\f |
