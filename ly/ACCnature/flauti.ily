\clef "treble" r4 \grace { lab''16 sol'' fa'' } sol''8. lab''16 sib''8. sib''16 do'''8. re'''16 |
mib'''2. mib'''4 |
do'''4. do'''8 \grace re'''8 do''' sib'' \grace do''' sib''8. lab''16 |
sol''4 sib''8*2/3-! sol''-! mib''-! sol''[ mib'' sib'] mib''[ sib' sol'] |
mib'2 r |
R1 |
r8*2/3 lab''[ sib''] do'''[ lab'' mib''] lab'' mib'' do'' lab'8. lab'16 |
lab'2 r |
R1*2 |
