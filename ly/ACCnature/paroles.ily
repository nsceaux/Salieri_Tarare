vous.
Bril -- lant So -- leil, en vain la Na -- ture est fé -- con -- de ;
sans un ray -- on de vo -- tre feu sa -- cré,
mon œuvre est mor -- te, et son but é -- ga -- ré.
