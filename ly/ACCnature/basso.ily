\clef "bass" mib2( re) |
do sol, |
lab, sib, |
mib4 mib\f mib mib |
mib\p mib mib mib |
mib mib mib mib |
lab2.\f \tuplet 3/2 { lab8 mib do } |
lab,4. lab8\p lab4. fa8 |
fa2. r4 |
r2 r4 sol\f |
