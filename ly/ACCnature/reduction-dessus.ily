\clef "treble" 
sol'4 \grace { lab''16[ sol'' fa''] } sol''8. lab''16 sib''8. sib''16 do'''8. re'''16 |
mib'''2. mib'''4 |
do''' do'''8. do'''16 \grace re'''8 do''' sib'' \grace do''' sib''8. lab''16 |
sol''8*2/3[-! sol'' lab''] sib''[\f sol''-! mib'']-! sol''[-! mib''-! sib']-! mib''[-! sib'-! sol']-! |
mib'1\p~ |
mib'8. mib'16 sol'8. sib'16 reb''8. sol''16 sib''8. reb''16 |
do''8*2/3[ lab''(\f sib''] do'''[) lab''-! mib'']-!
lab''[-! mib''-! do'']-! lab'[ mib' do'] |
lab4. <mib' do''>8\p q4. <lab' re''>8 |
<sol' si'>2. r4 |
r2 r4 <si' re' sol'>\f |
