\clef "soprano/treble"
sol'4 r r8 sib' do''8. re''16 |
mib''2. mib''4 |
do''4 do''8. do''16 do''8[ sib'] sib' lab' |
sol'2 sol'4 r |
sib'2 sib'4. do''8 |
reb''4. reb''8 reb''8. reb''16 reb''8. mib''16 |
do''4 r r2 |
r4 r8 do'' do''4. re''8 |
si'2 si'4 sol'8 la' |
si'4 si'8 do'' sol'4 r |
