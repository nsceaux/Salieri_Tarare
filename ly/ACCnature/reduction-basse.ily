\clef "bass" \ru#2 { sib,8*2/3[ mib sol] } \ru#2 { sib,[ re fa] } |
\ru#2 { sol,[ do mib] } \ru#2 { sol,[ sib, mib] } |
lab,[ do mib] do[ mib lab] re[ fa sib] re[ fa sib] |
\ru#12 { sib,[ mib sol] } |
lab2. \tuplet 3/2 { lab8 mib do } |
lab,4. lab8 lab4. fa8 |
<fa re'>2. r4 |
r2 r4 sol |

