\clef "bass" <<
  { sol2( fa) |
    sol2. mib'4~ |
    mib' mib' re' re' |
    mib'1 |
    sib~ |
    sib |
    do'2~ do' |
    do'4 } \\
  { mib2( re) |
    do2. sol4 |
    lab lab sib fa |
    sol1 |
    sol~ |
    sol |
    lab2~ lab |
    lab4 }
>> r4 r2 |
R1*2 |
