\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (flauti #:score-template "score-flauti")
   (oboi #:score-template "score-oboi")
   (fagotti #:tag-notes fagotti)
   (timpani #:score "score-tct")
   (trombe #:score "score-tct")
   (corni #:score "score-tct")
   (basso #:system-count 6 #:music , #{ s1*49\noBreak #})
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #}))
