<<
  \tag #'(vdessus basse) {
    \clef "soprano/treble" r2^\markup\center-align Chanté r4 fad'8^\p sol' |
    la'4 la'8 la' si'4 dod'' |
    re'' la' r la'8 la' |
    si'4 dod''8 dod'' re''4 mi'' |
    fad''4 re'' r re''8 fad'' |
    la''4 la''8 la'' la''4. la''8 |
    la''1~ |
    la''~ |
    la''~ |
    la'' |
    la''2 r4 re'' |
    si''1~ |
    si'' |
    la''2 r4 mi''8 mi'' |
    fad''4 fad''8 fad'' sol''4 sol'' |
    fad''2 r4 fad'' |
    sold'2 si'4. si'8 |
    mi''2 mi''4 mi'' |
    dod''2 r4 dod''8^\p re'' |
    mi''4 dod''8 dod'' re''4 mi'' |
    fad'' re'' mi'' fad'' |
    sol'' mi''8 mi'' fad''4 sol'' |
    fad'' fad'' r re''8 fad'' |
    la''4 la''8 la'' la''4. la''8 |
    la''1~ |
    la''~ |
    la''~ |
    la'' |
    la''2 r4 re'' |
    si''1~ |
    si''2.( la''4) |
    sold''2\fermata r4 mi'8^\p fad' |
    sold'[ la'] si' dod'' re''4 re'' |
    dod''2 r4 la'^\f |
    re''2 fad''4. fad''8 |
    la''2 la''4 la'' |
    re''2 r4 r8 re'' |
    si'2 r4 r8 sol'' |
    mi''2 r4 r8 la' |
    re''2 fad''4. fad''8 |
    la''2 la''4 la'' |
    re''2 fad''4. fad''8 |
    la''2 la'4 la' |
    re''2 re''4 fad'' |
    la''1 |
    la''2. la''4 |
    re''2 r |
  }
  \tag #'vhaute-contre {
    \clef "alto/G_8" r2 r4 re'8 mi' |
    fad'4 fad'8 fad' sol'4 mi' |
    fad' fad' r fad'8 fad' |
    sol'4 la'8 la' si'4 dod' |
    re'4 re' r re'8 fad' |
    la'4 la'8 la' la'4. la'8 |
    la'1~ |
    la'~ |
    la'~ |
    la' |
    la'2 r4 re' |
    si'1~ |
    si' |
    la'2 r4 la'8 la' |
    la'4 la'8 la' la'4 la' |
    la'2 r4 fad' |
    sold2 si4. si8 |
    mi'2 sold'4. sold'8 |
    la'2 r4 dod'8 re' |
    mi'4 dod'8 dod' re'4 mi' |
    fad' re' mi' fad' |
    sol' mi'8 mi' fad'4 sol' |
    fad' fad' r re'8 fad' |
    la'4 la'8 la' la'4. la'8 |
    la'1~ |
    la'~ |
    la'~ |
    la' |
    la'2 r4 re' |
    si'1~ |
    si' |
    si'2\fermata r4 mi'8 red' |
    mi'[ fad'] sold' la' si'4 sold' |
    la'2 r4 la |
    re'2 fad'4. fad'8 |
    la'2 la'4 la' |
    re'2 r4 r8 fad' |
    sol'2 r4 r8 sol' |
    la'2 r4 r8 la |
    re'2 fad'4. fad'8 |
    la'2 la'4 la' |
    re'2 fad'4. fad'8 |
    la'2 la'4 la' |
    re'2 fad'4. fad'8 |
    la'1 |
    la'2. la'4 |
    re'2 r |
  }
  \tag #'vtaille {
    \clef "tenor/G_8" r2 r4 fad8^\p sol |
    la4 la8 la si4 dod' |
    re' la r la8 la |
    si4 dod'8 dod' re'4 mi' |
    fad'4 re' r2 |
    R1 |
    r2 r4 re' |
    fad'2 fad'4. mi'8 |
    re'2 re'4 mi' |
    fad' fad'8 fad' fad'4 fad'8 mi' |
    re'2 re'4 re' |
    sol'1~ |
    sol' |
    fad'2 r4 la8 la |
    re'4 re'8 re' mi'4 mi' |
    re'2 r4 fad' |
    sold2 si4. si8 |
    mi'2 mi'4. mi'8 |
    la2 r4 la8^\p si |
    dod'4 la8 la si4 dod' |
    re' si dod' re' |
    mi' dod'8 dod' re'4 mi' |
    re' re' r2 |
    R1 |
    r2 r4 re' |
    fad'2 fad'4. mi'8 |
    re'2 re'4 mi' |
    fad' fad'8 fad' fad'4 fad'8 mi' |
    re'2 re'4 re' |
    sol'1~ |
    sol'2.\melisma fa'4\melismaEnd |
    mi'2\fermata r4 mi8^\p fad |
    sold[ la] si dod' re'4 re' |
    dod'2 r4 la^\f |
    re'2 fad'4. fad'8 |
    la2 la4 la |
    re'2 r4 r8 re' |
    re'2 r4 r8 mi' |
    dod'2 r4 r8 la |
    re'2 fad'4. fad'8 |
    la2 la4 la |
    re'2 fad'4. fad'8 |
    la2 la4 la |
    re'2 fad'4 fad' |
    la1 |
    la2. la4 |
    re'2 r |
  }
  \tag #'vbasse {
    \clef "bass" r2 r4 re8 mi |
    fad4 fad8 fad sol4 mi |
    fad fad r fad8 fad |
    sol4 la8 la si4 dod' |
    re'4 re r2 |
    R1 |
    r2 r4 re |
    re'2 re'4. la8 |
    fad2 fad4 la |
    re'4 re'8 re' re'4 re'8 la |
    fad2 fad4 re' |
    re'1~ |
    re' |
    re'2 r4 dod'8 dod' |
    re'4 re'8 re' la4 la |
    re'2 r4 fad' |
    sold2 si4. si8 |
    mi'2 mi4. mi8 |
    la2 r4 la8 la |
    la4 la8 la la4 la |
    la la la la |
    la la8 la la4 la |
    re re r2 |
    R1 |
    r2 r4 la |
    re'2 re'4. la8 |
    fad2 fad4 la |
    re'4 re'8 re' re'4 re'8 la |
    fad2 fad4 re' |
    re'1~ |
    re' |
    mi'2\fermata r4 mi8 red |
    mi[ fad] sold la si4 sold |
    la2 r4 la |
    re'2 fad'4. fad'8 |
    la2 la4 la |
    re'2 r4 r8 re |
    sol2 r4 r8 mi |
    la2 r4 r8 la |
    re'2 fad'4. fad'8 |
    la2 la4 la |
    re'2 fad'4. fad'8 |
    la2 la4 la |
    re'2 fad'4 fad' |
    la1 |
    la2. la4 |
    re'2 r |
  }
>>
R1*8 |
