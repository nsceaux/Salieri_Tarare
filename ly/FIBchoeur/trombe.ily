\clef "treble" \transposition re
R1*6 |
r2 r4 <>-\sug\f \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''4 |
    mi''2 mi''4. re''8 |
    do''2 do''4 re'' |
    mi''2 mi''4 re'' |
    do''2 do''4. do''8 | }
  { do'4 |
    do''2 do''4. sol'8 |
    mi'2 mi'4 sol' |
    do''2 do''4 sol' |
    mi'2 mi'4. do''8 | }
>>
do''1~ |
do'' |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { do''4 sol'8 sol' sol'4 sol' |
    mi''2 re'' |
    mi''1 | }
  { mi'4 sol'8 sol' sol'4 sol' |
    do''2 sol' |
    sol'1 | }
>>
re''1~ |
re'' |
R1*6 | \allowPageTurn
r2 r4 <>-\sug\f \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''4 |
    mi''2 mi''4. re''8 |
    do''2 do''4 re'' |
    mi''2 mi''4 re'' |
    do''2 do''4 }
  { do'4 |
    do''2 do''4. sol'8 |
    mi'2 mi'4 sol' |
    do''2 do''4 sol' |
    mi'2 mi'4 }
>> r |
do''1\ff |
do'' |
re''2.\fermata r4 |
R1*2 | \allowPageTurn
<>-\sug\ff \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''2 mi'' | sol'4 re''8 re'' re''4 re'' | do''2 }
  { do'2 mi' | sol4 sol'8 sol' sol'4 sol' | mi'2 }
>> r2 |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { do''2 }
  { do' }
>> r4 r8 \twoVoices#'(tromba1 tromba2 trombe) <<
  { re''8 | sol'2 }
  { re''8 | sol'2 }
>> r4 r8 \twoVoices#'(tromba1 tromba2 trombe) <<
  { sol'8 |
    do''2 mi'' |
    sol'4 re''8 re'' re''4 re'' |
    do''2 mi'' |
    sol'4 re''8 re'' re''4 re'' |
    do''2 mi'' |
    sol'4 mi''8 mi'' mi''4 mi'' |
    re''4 re''8 re'' re''4 re'' |
    do''2 do'' |
    do''2 do''^\markup\tiny\center-align { [Source : \concat { \italic ré  ] } } |
    do''4 }
  { sol'8 |
    do'2 mi' |
    sol4 sol'8 sol' sol'4 sol' |
    do'2 mi' |
    sol4 sol'8 sol' sol'4 sol' |
    do'2 mi' |
    sol4 do''8 do'' do''4 do'' |
    sol' sol'8 sol' sol'4 sol' |
    mi'2 mi' |
    mi' mi' |
    mi'4 }
  { s8 | s1*9 | s4-\sug\p }
>> r4 r2
R1*6 |
