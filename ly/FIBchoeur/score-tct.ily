\score {
  <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'basse \includeLyrics "paroles" }
    \new StaffGroup <<
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \small in D }
        shortInstrumentName = \markup Tr.
      } << \keepWithTag #'() \global \keepWithTag #'trombe \includeNotes "trombe" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small in D }
        shortInstrumentName = \markup Cor.
      } << \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni" >>
      \new Staff \with { \timpaniInstr } <<
        \keepWithTag #'() \global \includeNotes "timpani"
        { s1*24\pageBreak s1*24\break }
      >>
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 10\mm
  }
}
