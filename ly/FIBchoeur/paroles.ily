Quel plai -- sir de nos cœurs s’em -- pa -- re,
quel plai -- sir de nos cœurs s’em -- pa -- re !
\tag #'(vdessus vhaute-contre basse) {
  Vi -- ve no -- tre grand roi Ta -- ra -- re !
}
\tag #'(vtaille vbasse) {
  Ta -- ra -- re, Ta -- ra -- re,
  la belle As -- ta -- sie et Ta -- ra -- re !
}
Ta -- ra -- re !
Nous a -- vons le meil -- leur des rois :
ju -- rons de mou -- rir sous ses lois.

Quel plai -- sir de nos cœurs s’em -- pa -- re,
quel plai -- sir de nos cœurs s’em -- pa -- re !
\tag #'(vdessus vhaute-contre basse) {
  Vi -- ve no -- tre grand roi Ta -- ra -- re !
}
\tag #'(vtaille vbasse) {
  Ta -- ra -- re, Ta -- ra -- re,
  la belle As -- ta -- sie et Ta -- ra -- re !
}
Ta -- ra -- re !
Nous a -- vons le meil -- leur des rois :
ju -- rons de mou -- rir sous ses lois.
Ju -- rons, ju -- rons, ju -- rons de mou -- rir sous ses lois,
de mou -- rir sous ses lois,
de mou -- rir sous ses lois.
