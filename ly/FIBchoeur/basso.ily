\clef "bass" re1\fp~ |
re~ |
re8 re re re re2:8 |
re:8\mf re:8 |
re2 r |
R1*2 |
re2\f r |
re r |
re r |
re4 re re re |
sol, si re' si |
sol re si, sol, |
re2 r4 dod |
re r dod r |
re r re r |
mi r mi r |
mi r mi r |
la,8 la\p la la la2:8 |
la:8 la:8 |
la:8\cresc la:8 |
la8 la, la, la,\! la,2:8 |
re2 r |
R1*2 |
re2\f r |
re r |
re r |
re4 re re re |
sol sol,\ff si, re |
sol re'8 si sol4 fa |
mi2.\fermata r4 |
mi2:8\p mi:8 |
la:8 la:8 |
re\ff fad |
la2 la, |
re r4 r8 re |
sol2 r4 r8 mi |
la2 r4 r8 la |
re2 fad |
la la, |
re fad la la, |
re fad |
la4 la,8 la, la4 la,8 la, |
la4 la,8 la, la4 la,8 la, |
re2 re |
re re |
<<
  { <>^"Violoncelli" re2 re'8*2/3([ la fad] re fad la) |
    dod[ mi la] dod'[ la mi] dod2 |
    do8*2/3[ mi la] do'[ mi' do'] la2 |
    la8*2/3[ fad red] si,[ red fad] si4. la8 |
    sol8*2/3[ si, mi] sol[ si sol] mi4 mi8*2/3[ sol mi] | } \\
  { re1_\markup { \dynamic p Bassi } |
    dod |
    do |
    si,~ |
    si,~ |
    si,~ |
    si, |
  }
>>