\clef "treble" \transposition re
<>^"unis" do'1-\sug\fp~ |
do'~ |
do'~ |
do' |
do'2 r |
R1 |
r2 r4 <>-\sug\f \twoVoices#'(corno1 corno2 corni) <<
  { do''4 |
    mi''2 mi''4. re''8 |
    do''2 do''4 re'' |
    mi''2 mi''4 re'' |
    do''2 do''4. do''8 | }
  { do'4 |
    do''2 do''4. sol'8 |
    mi'2 mi'4 sol' |
    do''2 do''4 sol' |
    mi'2 mi'4. do''8 | }
>>
do''1~ |
do'' |
\twoVoices#'(corno1 corno2 corni) <<
  { do''4 sol'8 sol' sol'4 sol' |
    mi''2 re'' |
    mi''1 | }
  { mi'4 sol'8 sol' sol'4 sol' |
    do''2 sol' |
    sol'1 | }
>>
re''1~ |
re'' |
\twoVoices#'(corno1 corno2 corni) <<
  { sol'1~ |
    sol'~ |
    sol'~ |
    sol'1*1/2 s2 |
    do'2 }
  { sol1~ |
    sol~ |
    sol~ |
    sol1*1/2 s2 |
    do'2 }
  { s1*2\p | s1*2-\sug\cresc | s2\! }
>> r2 |
R1 |
r2 r4 <>-\sug\f \twoVoices#'(corno1 corno2 corni) <<
  { do''4 |
    mi''2 mi''4. re''8 |
    do''2 do''4 re'' |
    mi''2 mi''4 re'' |
    do''2 do''4 }
  { do'4 |
    do''2 do''4. sol'8 |
    mi'2 mi'4 sol' |
    do''2 do''4 sol' |
    mi'2 mi'4 }
>> r |
do''1\ff |
do'' |
re''2.\fermata r4 |
R1*2 |
<>-\sug\ff \twoVoices#'(corno1 corno2 corni) <<
  { do''2 mi'' | sol'4 re''8 re'' re''4 re'' | do''2 }
  { do'2 mi' | sol4 sol'8 sol' sol'4 sol' | mi'2 }
>> r2 |
\twoVoices#'(corno1 corno2 corni) <<
  { do''2 }
  { do' }
>> r4 r8 \twoVoices#'(corno1 corno2 corni) <<
  { re''8 | sol'2 }
  { re''8 | sol'2 }
>> r4 r8 \twoVoices#'(corno1 corno2 corni) <<
  { sol'8 |
    do''2 mi'' |
    sol'4 re''8 re'' re''4 re'' |
    do''2 mi'' |
    sol'4 re''8 re'' re''4 re'' |
    do''2 mi'' |
    sol'4 mi''8 mi'' mi''4 mi'' |
    re''4 re''8 re'' re''4 re'' |
    do''2 do'' |
    do''2 do''^\markup\tiny\center-align { [Source : \concat { \italic ré  ] } } | }
  { sol'8 |
    do'2 mi' |
    sol4 sol'8 sol' sol'4 sol' |
    do'2 mi' |
    sol4 sol'8 sol' sol'4 sol' |
    do'2 mi' |
    sol4 do''8 do'' do''4 do'' |
    sol' sol'8 sol' sol'4 sol' |
    mi'2 mi' |
    mi' mi' | }
>>
<>-\sug\p \twoVoices#'(corno1 corno2 corni) <<
  { do''1 | re''1 | re'' | mi'' | }
  { mi'1 | sol'1 | sol' | mi' | }
>>
R1*3 |
