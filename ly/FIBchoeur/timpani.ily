\clef "bass" R1*7 |
re4-\sug\f re:16 re4 la, |
re8 la, re la, re4 la, |
re re re la, |
re8 la, re la, re2:8 |
re2:16 re:16\< |
re2:16 re:16 |
re2\! r4 la, |
re r la, r |
re r re r |
re2:16 re:16 |
re:16 re:16 |
la,2 r |
R1*6 |
re4-\sug\f re:16 re4 la, |
re8 la, re la, re4 la, |
re re re la, |
re8 la, re la, re2:8 |
re:16-\sug\ff re:16 |
re4 r r2 |
r2\fermata r |
R1*2 |
re2-\sug\ff re |
la,4 la,8 la, la,4 la, |
re2 r |
r8*2/3 re\ff re re re re re4 r |
r8*2/3 la,\ff la, la, la, la, la,4. la,8 |
re2 re |
la,4 la,8. la,16 \tuplet 6/4 la,2.:8 |
re2 re |
la,4 la,8 la, \tuplet 6/4 { la,[ la, la, la, la, la,] } |
re2 re |
la,4 la,8 la, \ru#3 \tuplet 6/4 la,2.:8 |
re2 re |
re2 re |
re4-\sug\p r r2 |
R1*6 |
