\clef "bass" re1-\sug\fp~ |
re~ |
re~ |
re |
re2 r |
R1 |
\clef "tenor" r2 r4 <>-\sug\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la4 |
    fad'2 fad'4. mi'8 |
    re'2 re'4. mi'8 |
    fad'4. fad'8 fad'4. mi'8 |
    re'4 }
  { la4 |
    re'2 re'4. la8 |
    fad2 fad4. la8 |
    re'4. re'8 re'4. la8 |
    fad4 }
>> \clef "bass" re4 re re |
sol, si re' si |
sol re si, sol, |
re2 r4 dod |
re r dod r |
re r re r |
mi r mi r |
mi r mi r |
la,2 r |
\override Staff.Slur.dash-definition = #'((0 1 0.4 0.75))
\clef "tenor" r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { dod'4( re' mi') |
    fad' re'( mi' fad') |
    sol' mi'( fad' sol') |
    fad'4 }
  { la( si dod') |
    re' si( dod' re') |
    mi' dod'( re' mi') |
    re' }
  { s2.-\sug\p | s4 s2.\mf }
>> r4 r2 |
\revert Staff.Slur.dash-definition
R1 |
r2 r4 <>-\sug\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la4 |
    fad'2 fad'4. mi'8 |
    re'2 re'4. mi'8 |
    fad'4. fad'8 fad'4. mi'8 |
    re'1 | }
  { la4 |
    re'2 re'4. la8 |
    fad2 fad4. la8 |
    re'4. re'8 re'4. la8 |
    fad1 | }
>> \clef "bass"
sol4 sol,-\sug\ff si, re |
sol re'8 si sol4 fa |
mi2.\fermata \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mi8 fad | sold la si dod' re'4. re'8 | dod'2 }
  { mi8-\sug\p red | mi fad sold la si4. si8 | la2 }
>> r4 la-\sug\ff |
re2 fad |
la2 la, |
re r4 r8 re |
sol2 r4 r8 mi |
la2 r4 r8 la |
re2 fad |
la la, |
re fad la la, |
re fad |
la4 la,8 la, la4 la,8 la, |
la4 la,8 la, la4 la,8 la, |
re2 re |
re re |
re1-\sug\p |
dod |
do |
si, |
si, |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fad2 sol | la sol | }
  { red2 mi | fad mi | }
>>
