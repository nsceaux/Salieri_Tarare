\clef "treble" <la' fad'' re'''>2\f r4 <<
  \tag #'violino1 {
    fad'8\p sol' |
    la' r la'4( si' dod'') |
    re'' la' r la' |
    si'\mf( dod'' re'' mi'') |
    fad''4 re'' r re''8\ff fad'' |
    <la'' la'>4
  }
  \tag #'violino2 {
    re'8-\sug\p mi' |
    fad' r fad'4( sol' mi') |
    fad' fad' r fad' |
    sol'-\sug\mf( la' si' dod'') |
    re'' fad' r re'8-\sug\ff fad' |
    la'4
  }
>> <la' la''>8 q q4 q |
q2:16 q:16 |
q:16 q:16 |
q:16 q:16 |
q:16 q:16 |
q8 <<
  \tag #'violino1 {
    re''4 fad'' la'' re'''8 |
    <si'' re''>8 q4 q q q8~ |
    q q4 q q q8 |
    \omit TupletBracket \tuplet 3/2 { <la'' re''>8 la'8[ la'] }
  }
  \tag #'violino2 {
    << { la'4 la' la' } \\ { fad'4 fad' fad' } >> <la' fad''>8 |
    <si' sol''>8 q4 q q q8~ |
    q q4 q q q8 |
    \omit TupletBracket \tuplet 3/2 { <fad'' la'>8 la'[ la'] }
  }
>> la'8*2/3 la' la' la'2.*2/3:8 |
<<
  \tag #'violino1 {
    fad''8*2/3 la' la' la' la' la' sol'' la' la' la' la' la' |
    fad'' la' la' la' la' la' la' fad'' fad'' fad'' fad'' fad'' |
    sold'2 si'4. si'8 |
    <mi'' mi'>2 <mi' si' sold''> |
    <mi' dod'' la''>2. dod''8\p re'' |
    mi''4 dod''( re'' mi'') |
    fad''8-! fad'' re''\mf re'' mi'' mi'' fad'' fad'' |
    sol'' sol'' mi'' mi'' fad'' fad'' sol'' sol'' |
    fad''4 re'' r re''8\ff fad'' |
    <la'' la'>4
  }
  \tag #'violino2 {
    re''8*2/3 la' la' la' la' la' mi'' la' la' la' la' la' |
    re'' la' la' la' la' la' la' re'' re'' re'' re'' re'' |
    \tuplet 6/4 { mi'8[ <sold' si> q q q q] } \tuplet 6/4 q2.:8 |
    <sold' si'>2.*2/3:8 q:8 |
    la'2. la'8-\sug\p si' |
    dod''4 la'( si' dod'') |
    re''8 re'' si'-\sug\mf si' dod'' dod'' re'' re'' |
    mi'' mi'' dod'' dod'' re'' re'' mi'' mi'' |
    re''4 fad' r re'8-\sug\ff fad' |
    la'4
  }
>> <la' la''>8 q q4 q |
q2:16 q:16 |
q:16 q:16 |
q:16 q:16 |
q:16 q:16 |
q8 <<
  \tag #'violino1 {
    re''4 fad'' la'' re'''8 |
    <si'' re''>8\ff q4 q q q8~ |
    q
  }
  \tag #'violino2 {
    << { la'4 la' la' } \\ { fad'4 fad' fad' } >> <la' fad''>8 |
    <si' sol''>8-\sug\ff q4 q q q8~ |
    q
  }
>> <si' si''>4 q q <si' la''>8 |
<si' sold''>2.\fermata <<
  \tag #'violino1 {
    mi'8\p fad' |
    sold'( la' si' dod'') re''4. re''8 |
    dod''2
  }
  \tag #'violino2 {
    mi'8-\sug\p red' |
    mi'( fad' sold' la') si'4. sold'8 |
    la'2
  }
>> r4 la'\ff |
re''8*2/3 re' re' re' re' re' fad'' fad' fad' fad' fad' fad' |
la'' <la'' la'>[ q q q q] q <mi'' dod'''>[ q] q[ q q] |
re''' <re'' re'>[ q] q[ q q] q4 r8 re'' |
si'8*2/3 sol sol sol sol sol sol4. sol''8 |
mi''8*2/3 la la la la la la4. la'8 |
re''8*2/3 re' re' re'4.*2/3:8 fad''8*2/3 fad' fad' fad'4.*2/3:8 |
<la' la''>2.*2/3:8 <<
  \tag #'violino1 { <mi'' dod'''>2.*2/3:8 | <re''' re''>8*2/3 }
  \tag #'violino2 { <la' la''>2.*2/3:8 | <fad'' la'>8*2/3 }
>> re'8*2/3 re' re' re' re' fad'' fad' fad' fad' fad' fad' |
<la' la''>2.*2/3:8 <<
  \tag #'violino1 { <mi'' dod'''>2.*2/3:8 | <re'' re'''>8*2/3 }
  \tag #'violino2 { <la' la''>2.*2/3:8 | <fad'' la'>8*2/3 }
>> re' re' re'4.*2/3:8 fad''8*2/3 fad' fad' fad'4.*2/3:8 |
la''8*2/3 <fad'' re'''> q q4.*2/3:8 q2.*2/3:8 |
<mi'' dod'''>:8 q:8 |
re''8*2/3 fad'' la'' re''' la'' fad'' re'' fad'' la'' re''' la'' fad'' |
re'' fad'' la'' re''' la'' fad'' re'' fad'' la'' re''' la'' fad'' |
<<
  \tag #'violino1 {
    re''(\mf fad'' la'' re''' la'' fad'' re'' la' fad' re' fad' la') |
    dod'( mi' la' dod'' mi'' la'') dod'''( la'' mi'' dod'' la' mi') |
    do'( mi' la' do'' mi'' la'') do'''( la'' mi'' do'' mi'' la') |
    la'( fad' red' si red' fad') si'( red'' mi'' fad'' red'' la') |
    sol'( si mi' sol' si' mi'') sol''( si'' sol'' mi'' si' sol') |
    si( red' fad' si' fad' red') si( mi' sol' si' sol' mi') |
    si( fad' la' si' la' fad') si( mi' sol' si' sol' mi') |
  }
  \tag #'violino2 {
    re''1-\sug\p |
    la'~ |
    la' |
    <fad' la'> |
    sol' |
    fad'2 sol' |
    la' sol' |
  }
>>
