\clef "treble" R1*4 |
r2 r4 \tag #'flauti <>^"unis" re''8-\sug\ff fad'' |
la''4 la''8 la'' la''4 la'' |
la''1~ |
la''~ |
la''~ |
la'' |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { la''1 | si''~ | si'' | la''2 }
  { fad''1 | sol''~ | sol'' | fad''2 }
>> r4 \tag #'flauti <>^"unis" mi'''4 |
fad'''8*2/3 la'' la'' la'' la'' la'' mi''' la'' la'' la'' la'' la'' |
fad''' la'' la'' la'' la'' la'' la'' fad''' fad''' fad''' fad''' fad''' |
sold''2 si'' |
mi''' sold'' |
la'' r |
R1*3 |
r2 r4 \tag #'flauti <>^"unis" re''8-\sug\ff fad'' |
la''4 la''8 la'' la''4 la'' |
la''1~ |
la''~ |
la''~ |
la'' |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { la''1 | si'' | si''2. la''4 | sold''2.\fermata }
  { fad''1 | sol'' | re'' | mi''2.\fermata }
  { s1 | s-\sug\ff }
>> r4
R1 |
r2 r4 \tag #'flauti <>^"unis" la'-\sug\ff |
re''2 fad'' |
la''2 \twoVoices #'(flauto1 flauto2 flauti) <<
  { dod'''2 | re'''8*2/3 re'' re'' }
  { mi''2 | fad''8*2/3 re'' re'' }
>> re'' re'' re'' re''4 r |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { si''2 }
  { re'' }
>> r4 r8 \twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''8 | mi''2 }
  { si'8 | dod''2 }
>> r4 r8 la' |
re''2 fad'' |
la'' \twoVoices #'(flauto1 flauto2 flauti) <<
  { dod''' |
    re''' fad'' |
    la'' dod''' |
    re''' fad'' | }
  { mi'' |
    re'' fad'' |
    la'' mi'' |
    fad'' fad'' | }
>>
la'8*2/3 re'' fad'' la'' fad'' re'' re'''2.*2/3:8 |
dod''':8 dod''':8 |
re''8*2/3 fad'' la'' re''' la'' fad'' re'' fad'' la'' re''' la'' fad'' |
re'' fad'' la'' re''' la'' fad'' re'' fad'' la'' re''' la'' fad'' |
<<
  \tag #'(flauti flauto1) {
    <>^"solo" \once\slurDashed re''(-\sug\mf fad'' la'' re''' la'' fad'' re'' la' fad' re' fad' la') |
    r mi'( la' dod'' mi'' la'') dod'''( la'' mi'' dod'' la' mi') |
    r mi'( la' do'' mi'' la'') do'''( la'' mi'' do'' mi'' la') |
  }
  \tag #'flauto2 R1*3
>>
\twoVoices #'(flauto1 flauto2 flauti) <<
  { fad''1 |
    sol'' |
    fad''2 sol'' |
    la'' sol'' | }
  { red''1 |
    mi'' |
    red''2 mi'' |
    fad'' mi'' | }
>>
