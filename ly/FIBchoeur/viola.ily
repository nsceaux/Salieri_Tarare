\clef "alto" re'1-\sug\fp~ |
re'~ |
re'8 re' re' re' re'2:8 |
re':8-\sug\mf re':8 |
re'2 r4 re'8-\sug\ff fad' |
la'4 la'8 la' la'4 la' |
la'2 r4 la |
<<
  { fad'2 fad'4. mi'8 |
    re'2 re'4 mi' |
    fad'4 fad'8 fad' fad'4. mi'8 | } \\
  { re'2 re'4. la8 |
    fad2 fad4 la |
    re'4 re'8 re' re'4. la8 | }
>>
re'4 re' re' re' |
sol si' re'' si' |
sol' re' si sol |
re'2 r4 dod' |
re' r dod' r |
re' r re' r |
\ru#2 \tuplet 6/4 { mi'2.:8 } |
\ru#2 mi'2.*2/3:8 |
la2. <<
  { dod'8 re' |
    mi'4 dod'( re' mi') |
    fad'8 fad' re' re' mi' mi' fad' fad' |
    sol' sol' mi' mi' fad' fad' sol' sol' |
    fad'4 re' } \\
  { la8-\sug\p si |
    dod'4 la( si dod') |
    re'8 re' si-\sug\mf si dod' dod' re' re' |
    mi' mi' dod' dod' re' re' mi' mi' |
    re'4 fad4 }
>> r4 re'8-\sug\ff fad' |
la'4 la'8 la' la'4 la' |
la'2 r4 la |
<<
  { fad'2 fad'4. mi'8 |
    re'2 re'4 mi' |
    fad'4 fad'8 fad' fad'4. mi'8 | } \\
  { re'2 re'4. la8 |
    fad2 fad4 la |
    re'4 re'8 re' re'4. la8 | }
>>
<re' fad>8 q4 q q q8 |
sol4 sol\ff si re' |
sol' re''8 si' sol'4 fa' |
mi'2.\fermata <<
  { mi8 fad | sold la si dod' re'4. re'8 | dod'2 } \\
  { mi8-\sug\p red | mi fad sold la si4. si8 | la2 }
>> r4 la\ff |
re'8*2/3 re re re re re fad' fad fad fad fad fad |
la' la la la la la la la' la' la' la' la' |
re'' re' re' re' re' re' re'4 r |
<re' si'>8*2/3 sol sol sol sol sol sol4. si'8 |
dod''8*2/3 la la la la la la4. la8 |
re'8*2/3 re re re4.*2/3:8 fad'8*2/3 fad fad fad4.*2/3:8 |
la'8*2/3 la la la4.*2/3:8 la8*2/3 la' la' la'4.*2/3:8 |
re'8*2/3 re re re re re fad' fad fad fad fad fad |
la' la la la la la la la' la' la' la' la' |
re'8*2/3 re re re4.*2/3:8 fad'8*2/3 fad fad fad4.*2/3:8 |
la'2.*2/3:8 la':8 |
la':8 la':8 |
<re'' fad'>2 <la fad' re''> |
q q |
<fad' la'>1-\sug\p |
<mi' la'>~ |
q |
<red' fad'> |
mi' |
red'2 mi' |
fad' mi' |
