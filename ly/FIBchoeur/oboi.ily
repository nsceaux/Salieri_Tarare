\clef "treble" R1 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'4( si' dod'') | re''4 }
  { fad'( sol' mi') | fad' }
  { s2.\p }
>> r4 r2 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''4( re'' mi'') | fad'' }
  { la'4 ( si' dod'') | re'' }
  { s2.\mf }
>> r4 r2 |
R1 |
r2 r4 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 |
    fad''2 fad''4. mi''8 |
    re''2 re''4 mi'' |
    fad''2 fad''4. mi''8 |
    re''8 re''4 fad'' la'' la''8 |
    si''1~ |
    si'' |
    la''2 }
  { re'4 |
    re''2 re''4. la'8 |
    fad'2 fad'4 la' |
    re''2 re''4. la'8 |
    fad'8 fad'4 la' fad'' fad''8 |
    sol''1~ |
    sol'' |
    fad''2 }
>> r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4 |
    fad''2 sol'' |
    fad''1 |
    sold''1~ |
    sold'' |
    la''2 }
  { la'4 |
    re''2 mi'' |
    re''1 |
    si'~ |
    si' |
    dod''2 }
>> r2 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''( re'' mi'') |
    fad'' re''( mi'' fad'') |
    sol'' mi''( fad'' sol'') |
    fad'' }
  { la'4( si' dod'') |
    re'' si'( dod'' re'') |
    mi'' dod''( re'' mi'') |
    re'' }
  { s2.\p | s4 s2.\mf }
>> r4 r2 |
R1 |
r2 r4 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 |
    fad''2 fad''4. mi''8 |
    re''2 re''4 mi'' |
    fad''2 fad''4. mi''8 |
    re''8 re''4 fad'' la'' la''8 |
    si''1 |
    si''2. la''4 |
    sold''2.\fermata }
  { re'4 |
    re''2 re''4. la'8 |
    fad'2 fad'4 la' |
    re''2 re''4. la'8 |
    fad'8 fad'4 la' fad'' fad''8 |
    sol''1 |
    sol''4 re''2 re''4 |
    si'2.\fermata }
  { s4 | s1*4 | s1-\sug\ff }
>> r4 |
R1 |
r2 r4 \tag #'oboi <>^"unis" la'-\sug\ff |
re''2 fad'' |
la''2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { dod'''2 | re'''8*2/3 re' re' }
  { mi''2 | fad''8*2/3 re' re'  }
>> re' re' re' re'4 r |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { si''8*2/3 sol' sol' }
  { re'' sol' sol' }
>> sol' sol' sol' sol'4. \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''8 | mi''8*2/3 la' la' }
  { si'8 | dod''8*2/3 la' la' }
>> la' la' la' la'4. la'8 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 fad'' |
    la'' dod''' |
    re'''2 fad'' |
    la'' dod''' |
    re''' fad'' |
    la''1 |
    dod''' |
    re'''2 fad'' |
    fad'' fad'' |
    fad''1 |
    mi'' |
    mi'' |
    red'' |
    mi'' | }
  { re'2 fad' |
    la' mi'' |
    fad'' fad' |
    la' mi'' |
    fad'' fad' |
    fad''1 |
    mi'' |
    fad''2 la' |
    la'2 la' |
    la'1 |
    la' |
    la' |
    fad' |
    sol' | }
  { s1*9 | s1-\sug\p }
>>
\tag #'oboi <>^"unis" r8*2/3 si' si' si' si' si' si'2 |
si'1 |
