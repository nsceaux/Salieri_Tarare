\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \flautiInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          { s1*21
            \footnoteHere#'(0 . 0) \markup\justify {
              Source : page 445 manquante, reconstitution à partir des mesures
              5 à 10, et de la partition réduite
              \italic { Paris : T. Michaelis, [ca 1884] }
              (Bibliothèque de la Marie de Paris, cote TM921 Michaelis)
            }
            \override TextSpanner.style = #'dashed-line
            \override TextSpanner.bound-details.left.text =
            \markup { \draw-line #'(0 . -1) }
            \override TextSpanner.bound-details.right.text =
            \markup { \draw-line #'(0 . -1) }
            s2\startTextSpan s2 s1*5 s2.. s8\stopTextSpan
          }
          \global \keepWithTag #'flauto1 \includeNotes "flauti"
        >>
        \new Staff << \global \keepWithTag #'flauto2 \includeNotes "flauti" >>
      >>
      \new GrandStaff \with { \oboiInstr } <<
        \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \small en D }
        shortInstrumentName = \markup Tr.
      } <<
        \keepWithTag #'() \global \keepWithTag #'trombe \includeNotes "trombe"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small in D }
        shortInstrumentName = \markup Cor.
      } <<
        \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with { \timpaniInstr } <<
        \keepWithTag #'() \global \includeNotes "timpani"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      instrumentName = \markup\center-column\smallCaps { Chœur Général }
      shortInstrumentName = \markup\character Ch.
      \haraKiri
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \keepWithTag #'basso \includeNotes "basso"
      \origLayout {
        s1*4\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*5 s2 \bar "" \pageBreak
        s2 s1*6\pageBreak
        s1*7\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*4\break s1*4\pageBreak
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
