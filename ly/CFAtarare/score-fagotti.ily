\score {
  \new Staff <<
    \global \includeNotes "fagotti"
    { \quoteBasso "CFAbasso"
      s4
      <>^\markup\tiny "B." \cue "CFAbasso" {
        \mmRestUp s1*6 \mmRestDown s1*3 \mmRestUp s1 \mmRestCenter
      }
      s1*11
      \cue "CFAbasso" { \mmRestDown s1*8 \mmRestCenter }
    }
  >>
  \layout { }
}
