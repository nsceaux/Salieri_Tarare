\clef "bass" r4 |
do\f r do r |
do r do r |
re r re r |
re r re r |
sol, r sol, r |
do r r8 do\f mi sol |
do'2 r8 mi sol do' |
mi'2 r8 do'-\sug\f re' mi' |
fa' mi' re' do' si la sol fad |
sol4 sol, sol, r |
do2:8\ff do:8 |
do:8 do:8 |
do:8\p do:8 |
do:8 do:8 |
do:8 mi:8 |
re:8 re:8\< <>\! |
re:8 re:8\p |
mib:8 mib:8 |
mib:8\cresc mib:8 |
mib:8\f mib:8 |
re4 re re r |
sol\p r re r |
sol, r re r |
sol r re r |
sol, r re r |
sol2:8 sol:8 |
sol2:8\f sol:8 |
sold:8 sold:8 |
la2 r |
R1^\fermataMarkup |
fa4.\fermata r8 re2\fp |
sol si,\fp |
do4 do' la fa |
sol2:8 sol,:8 |
do4.\f do16 re mi4. do16 re32 mi |
fa4. fa16 mi re4. re16 do |
si,4. si8\p si4 si |
do'( sib lab sol) |
lab2:8 lab:8\p |
lab8\f lab, do mib lab mib do lab, |
fa2:8 fa:8 |
sol r4\fermata r8 mi!\f |
fa2 r |
r4.\fermata r8 re2\fp |
sol si,\fp |
do8 do' do' do' la la fa fa |
sol2:8 sol,:8 |
do8 do do'\cresc do' sib2:8 |
la:8 sol8 sol fad fad |
sol2:8\fp sol:8 |
sol,:8\f sol,:8 |
do:8 mi:8 |
fa:8 fa:8 |
sol:8 si,:8 |
do4 fa sol sol, |
do do do r |

