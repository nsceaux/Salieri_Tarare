\clef "treble" r4 |
<>\f \ru#11 {
  \twoVoices#'(tromba1 tromba2 trombe) <<
    { sol'4 }
    { sol' }
  >> r4
}
\twoVoices#'(tromba1 tromba2 trombe) <<
  { r2 | r2 r8 mi' sol' do'' | mi''2 r | R1*2 |
    do''1 | re'' | do''2 }
  { r8 do'-\sug\f mi' sol' | do''2 r | R1*3 |
    mi'1 | sol' | mi'2 }
  { s2 s1*4 | s1\ff | s | s2-\sug\p }
>> r2 |
R1 |
r2 \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''2 | re''~ re''~ | re'' }
  { do''2 | re''~ re''~ | re'' }
  { s2\p | s2 s\< <>\! }
>> r2 |
r4 <>-\sug\p \twoVoices#'(tromba1 tromba2 trombe) <<
  { sol'2 sol'4 | }
  { sol'2 sol'4 | }
>>
sol'1-\sug\cresc~ |
sol'-\sug\f |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { re''4 re'' re'' }
  { re''4 re'' re'' }
>> r4 |
R1*8 |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1*3 |
r2 <>-\sug\f \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''4 s | do'' s re'' s | re'' s2. | do''1~ | do''2 }
  { do''4 s | do'' s re'' s | sol' s2. | do'1~ | do'2 }
  { s4 r | s r s r | s r r2 | }
>> r2 |
do''1-\sug\f |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { do''4 re''2 re''4 | sol'2 }
  { do''4 re''2 re''4 | sol'2 }
>> r2\fermata |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1*2 |
<>\p \twoVoices#'(tromba1 tromba2 trombe) <<
  { mi''2 re'' | }
  { do''2 sol' | }
>>
do''1\p\cresc |
do'' |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { mi''1 |
    re'' |
    do''2 do'' |
    do''2. re''4 |
    re'' re''2 re''4 |
    mi'' do'' mi'' re'' |
    do'' do' do' }
  { do''1 |
    sol' |
    do''2 do'' |
    do''2. re''4 |
    re'' sol'2 sol'4 |
    do'' do'' do'' sol' |
    mi' do' do' }
  { s1-\sug\fp | s1-\sug\f }
>> r4 |
