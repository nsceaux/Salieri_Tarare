\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        \consists "Metronome_mark_engraver"
        \oboiInstr
      } << \global \keepWithTag #'oboi \includeNotes "oboi" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \smaller en Ut }
        shortInstrumentName = "Tr."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'trombe \includeNotes "trombe"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \smaller en Sol }
        shortInstrumentName = "Cor."
        \haraKiri
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
        { s4 s1*21 <>^"Cors en Sol" }
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { 
          \consists "Metronome_mark_engraver"
        } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Tarare
      shortInstrumentName = \markup\character Ta.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s4 s1*5\break s1*6\pageBreak
        s1*5\break s1*7\pageBreak
        s1*7\break s1*7\pageBreak
        s1*6\break s1*6\pageBreak
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" } 
  }
  \midi { }
}
