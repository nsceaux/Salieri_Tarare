\clef "bass" r4 |
R1*10 |
do1-\sug\ff~ |
do~ |
do-\sug\p~ |
do |
do2 mi |
re r |
R1 |
mib'1-\sug\p~ |
mib'-\sug\cresc~ |
mib'-\sug\f |
re'4 re re r |
R1*8 |
R1^\fermataMarkup |
r2\fermata \clef "tenor" la4-\sug\fp fa'8( mi') |
re'4 sol'2 sol'4~ |
sol'( do'2 fa'4) |
mi'2 si |
\clef "bass" do4.-\sug\f do16 re mi4. do16 re32 mi |
fa4. fa16 mi re4. re16 do |
si,4 r r2 |
do'4( sib lab? sol) |
lab1 |
lab-\sug\f |
fa' |
re'2 r\fermata |
R1^\fermataMarkup |
r2\fermata \clef "tenor" la4-\sug\fp fa'8( mi') |
re'4 sol'2 sol'4~ |
sol' do'2 fa'4 |
\clef "bass" mi'2 si |
do'2*1/2 s4-\sug\cresc sib2 <>\! |
la( sol4 fad) |
sol1-\sug\fp |
sol,-\sug\f |
do2 mi |
fa fa, |
sol, si, |
do4 fa sol sol, |
do do do r |
