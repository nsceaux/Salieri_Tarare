\clef "tenor/G_8" sol4^"chanté" |
do'2 mi'4 mi'8 sol' |
do'2 r8 do' mi' do' |
si2~ si8 do' re' mi' |
fa'2~ fa'8 la' fa' re' |
si4 fa' sol4. fa8 |
mi4 mi r8 mi sol do' |
mi'2 r8 sol do' mi' |
sol'2~ sol'8 mi' fa' sol' |
la'[ sol'] fa'[ mi'] re'[ do'] si[ la] |
sol4 sol r2 |
R1*2 |
do'2 mi'4. sol'8 |
fa'4. fa'8 fa'4. fa'8 |
mi'2. sol'4 |
fad'2 fad'4. la'8 |
re2 re'4. do'8 |
sib4 sib r r8 sib |
mib'2 sol'4. sib'8 |
mib2 sol'4. sol'8 |
fad'4 re' r2 |
R1 |
re'2 r |
r re'4 re'8 re' |
re'2 re'4 re' |
si4. sol8 si4 re' |
fa'!2. re'4 |
si4. si8 mi'4. re'8 |
do'4 do' r^"Récit" r8 la |
la4. la8 la la si dod' |
re'4.\fermata re'8^"a Tempo" fa'4 re'8 do' |
si4. sol8 re'4 mi'8 fa' |
mi'2 fa'4 la' |
sol'1 |
do'2 r4 r8 sol' |
la2 r4 r8 fa' |
sol4. re'8 re'4 re' |
mib'1~ |
mib'4. mib'8 mib'4 mib' |
lab'2. lab'4 |
lab'4. fa'8 re'4. do'8 |
si4 si r^"Récit" r8 sol |
la4. la8 si! dod' re' mi' |
fa'4.\fermata fa'8^"a Tempo" fa'4 re'8 do' |
si4. re'8 re'4 mi'8 fa' |
mi'2 fa'4 la' |
sol'1 |
mi'4. do'8 do'4 re'8 mi' |
fa'2 sol'4 la' |
sol'1~ |
sol' |
do'4 r r2 |
R1*4 |
