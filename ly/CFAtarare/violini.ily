\clef "treble" r4 |
<<
  \tag #'violino1 {
    do''16\fp do'' do'' do'' do''4:16 do''2:16\fp |
    do'':16\fp do'':16\fp |
    si':16\fp si':16\fp |
    si':16\fp si':16\fp |
    si':16\fp si':16\fp |
    do'':16\fp do'':16 |
    do'':16\fp do'':16 |
    do'':16\fp do''8
  }
  \tag #'violino2 {
    <sol mi'>16-\sug\fp q q q q4:16 q2:16-\sug\fp |
    q:16-\sug\fp q:16-\sug\fp |
    <sol fa'>:16-\sug\fp q:16-\sug\fp |
    q:16-\sug\fp q:16-\sug\fp |
    q:16-\sug\fp q:16-\sug\fp |
    <sol mi'>:16-\sug\fp q:16 |
    q:16-\sug\fp q:16 |
    q:16-\sug\fp q8
  }
>> mi''8[\f fa'' sol''] |
la'' sol'' fa'' mi'' re'' do'' si' la' |
sol'4 sol16 sol sol sol sol4 r |
do''8\ff( re''16 mi'' fa'' sol'' la'' si'' do''' si'' la'' sol'' fa'' mi'' re'' do'') |
si'( la' sol' la' si' do'' re'' mi'' fa'' mi'' re'' do'' si' la' sol' fa') |
<<
  \tag #'violino1 {
    mi'(\p do'' re'' mi'' fa'' sol'' la'' si'' do''' si'' la'' sol'' fa'' mi'' re'' do'') |
    si'( la' sol' la' si' do'' re'' mi'' fa'' mi'' re'' do'' si' la' sol' fa') |
    mi'8 sol''4 sol'' sol'' sol''8 |
    fad''2 fad''4.(\< la''8\!) |
    re'2 re''4.\p do''8 |
    sib' sib'4 sib' sib' sib'8 |
    mib''2\cresc sol''4. sib''8 |
    mib'2\f sol''4. sol''8 |
    fad''4 <re''' re'' re'> re' r |
    r8 sol'\p sol' sol' r la' la' la' |
    r si' si' si' r la' la' la' |
    r8 sol' sol' sol' r la' la' la' |
    r si' si' si' r la' la' la' |
    sol'2:16 sol':16 |
    <re' fa'!>2:16\f q:16 |
    << { mi'2:16 mi':16 } \\ { re':16 re':16 } >> |
    <do' mi'>2
  }
  \tag #'violino2 {
    <sol mi'>8-\sug\p q4 q q q8 |
    <sol fa'>8 q4 q8 <sol re'>8 q4 q8 |
    <sol mi'>8 q4 q do'' do''8 |
    <la' do''>2:16 q:16\< <>\! |
    q2:16 <fad' la'>:16-\sug\p |
    <sol' sib>16 <sol' sib'> q q q4:16 q2:16 |
    q:16\cresc q:16 |
    q:16\f dod'':16 |
    re''4 <re' la' fad''> re' r |
    r8 si\p si si r re' re' re' |
    r sol' sol' sol' r re' re' re' |
    r si si si r re' re' re' |
    r sol' sol' sol' r re' re' re' |
    <si re'>2:16 q:16 |
    q:16-\sug\f q:16 |
    q:16 q:16 |
    do'2
  }
>> r2 |
R1^\fermataMarkup |
<<
  \tag #'violino1 {
    re''4.\fermata re''8 fa''4(-\sug\fp re''8 do'') |
    si'4. sol'8 re''4(\fp mi''8 fa'') |
    mi''2( fa''4 la'') |
    sol''4. fa''32 mi'' re'' do'' si'2:8 |
    do'':16\f sol'':16\fp |
    sol'':16\fp fa'':16\fp |
    fa'':16\fp fa'':16 |
    mib''2:16 mib'':16 |
    mib'':16 mib''\p:16 |
    <do'' lab''>8\f q4 q q q8 |
    q4 fa'':16 re'':16 do'':16 |
    si'!2 r4\fermata r8 sol'\f |
    la'2
  }
  \tag #'violino2 {
    la'4.\fermata r8 la'4(-\sug\fp fa'8 mi') |
    re'4 sol'2 sol'4 |
    sol'2:16 do'':16 |
    <mi' do''>:16 <re' si'>:16 |
    <mi' do''>:16-\sug\f sol':16-\sug\fp |
    la':16-\sug\fp la':16-\sug\fp |
    sol':16-\sug\fp sol':16 |
    <mib' do''>:16 q:16 |
    q:16 q:16-\sug\p |
    q:16-\sug\f q:16 |
    <fa' do''>4 lab'2:16 lab'4:16 |
    sol'2 r4\fermata r8 do'-\sug\f |
    do'2
  }
>> r2\fermata |
<<
  \tag #'violino1 {
    r4\fermata r8 fa'' fa''4\fp( re''8 do'') |
    si'4. sol'8 re''4\fp mi''8 fa'' |
    mi''2:16 fa''4:16 la'':16 |
    sol''2:16 sol'':16 |
    mi''4. do''8\cresc do''4 re''8 mi'' |
    fa''2( sol''4 la'')\! |
    sol''16\fp <mi'' do'''>[ q q] q4:16 q2:16 |
    <re'' si''>2:16\f q:16 |
  }
  \tag #'violino2 {
    r2\fermata la'4-\sug\fp( fa'8 mi') |
    re'4( sol'2) sol'4 |
    sol'2:16 do'':16 |
    <mi' do''>:16 <re' si'>:16 |
    << { s4 s-\sug\cresc } <mi' do''>2:16 >> q:16\! |
    <fa' do''>4 fa''( mi'' red'') |
    <do'' mi''>2:16-\sug\fp q:16 |
    <si' sol''>:16-\sug\f q:16 |
  }
>>
<mi'' do'''>4 r8 mi''16 fa'' sol''( fa'' mi'' re'' do'' si' la' sol') |
la'8 si'16( do'' re'' mi'' fa'' sol'') la''( sol'' fa'' mi'' re'' do'' si' la') |
si'( re'' mi'' fa'' sol'' la'' si'' do''' re''' do''' si'' la'' sol'' fa'' mi'' re'') |
mi''4 <fa' do'' la''> <mi' do'' sol''> <sol re' si'> |
<sol mi' do''> do'16 si do' si do'4 r |
