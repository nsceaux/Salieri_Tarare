\clef "treble" \transposition sol
r4 |
R1*21 |
\twoVoices#'(corno1 corno2 corni) <<
  { s8 do'' do'' do'' s re'' re'' re'' |
    s mi'' mi'' mi'' s re'' re'' re'' |
    s do'' do'' do'' s re'' re'' re'' |
    s mi'' mi'' mi'' s re'' re'' re'' |
    do''1~ | do'' | mi'' | re''2 }
  { s8 mi' mi' mi' s sol' sol' sol' |
    s do'' do'' do'' s sol' sol' sol' |
    s mi' mi' mi' s sol' sol' sol' |
    s do'' do'' do'' s sol' sol' sol' |
    do'1~ | do' | mi' | re''2 }
  { r8\p s4. r8 s4. |
    r8 s4. r8 s4. |
    r8 s4. r8 s4. |
    r8 s4. r8 s4. |
    s1 | s1-\sug\f }
>> r2 |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1*10 |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1*12 |
