\tag #'all \key do \major
\tempo "Allegro alla breve" \midiTempo#120
\time 2/2 \partial 4 s4 s1*30 s4. \tempo "a Tempo" s8 s2
s1*12 s4. \tempo "a Tempo" s8 s2 s1*12 \bar "|."
