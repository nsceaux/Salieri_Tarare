J’i -- rai :
oui, j’o -- se -- rai :
pour la re -- voir je fran -- chi -- rai
cet -- te bar -- rière im -- pé -- né -- tra -- ble,
je fran -- chi -- rai
cet -- te bar -- rière,
cet -- te bar -- rière im -- pé -- né -- tra -- ble.
De ton re -- paire, af -- freux vau -- tour !
j’i -- rai l’ar -- ra -- cher morte ou vi -- ve,
j’i -- rai l’ar -- ra -- cher morte ou vi -- ve ;
et si je suc -- combe au re -- tour,
ne me plains pas, Ti -- ran, quoi -- qu’il m’ar -- ri -- ve :
ce -- lui qui te sau -- va le jour
a bien mé -- ri -- té, a bien mé -- ri -- té qu’on l’en pri -- ve !
Ti -- ran, Ti -- ran, ne me plains pas, __ ne me plains pas,
Ti -- ran, quoi -- qu’il m’ar -- ri -- ve :
ce -- lui qui te sau -- va le jour
a bien mé -- ri -- té, a bien mé -- ri -- té qu’on l’en pri -- ve,
a bien mé -- ri -- té qu’on l’en pri -- ve !
