\clef "treble" r4 |
<>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { do''4 s do'' s |
    do'' s do'' s |
    si' s si' s |
    si' s si' s |
    si' s si' s |
    do'' s4 r8 mi'-\sug\f sol' do'' |
    mi''2 r8 sol' do'' mi'' |
    sol''2 r | }
  { mi'4 s mi' s |
    mi' s mi' s |
    fa' s fa' s |
    fa' s fa' s |
    fa' s fa' s |
    mi' s r2 |
    R1*2 | }
  { s4 r s r |
    s r s r |
    s r s r |
    s r s r |
    s r s r |
    s r }
>>
R1*2 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { do''1 |
    si' |
    do'' |
    si' |
    do''2 sol'' |
    fad''2~ fad''~ |
    fad'' }
  { mi'1 |
    re' |
    mi' |
    re' |
    mi'2 sol' |
    la'~ la'~ |
    la' }
  { s1-\sug\ff s s-\sug\p | s1*2 | s2 s\< <>\! }
>> r2 |
r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { sib'2 sib'4 |
    mib''2 sol'' |
    sib'' sol'' |
    fad''4 re'' re' }
  { sol'2 sol'4 |
    sol'2 mib''! |
    sol'' dod'' |
    re''4 re'' re' }
  { s2.-\sug\p | s1-\sug\cresc | s1-\sug\f }
>> r4 |
R1*8 |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1*3 |
r2 <>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''4 s | sol'' s fa'' s | fa'' }
  { sol'4 s | la' s la' s | sol' }
  { s4 r | s r s r | }
>> r4 r2 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mib''1~ |
    mib'' |
    lab''~ |
    lab''4 fa'' re''4. do''8 |
    si'2 }
  { do''1~ |
    do'' |
    do''~ |
    do''4 lab'2 lab'4 |
    re'!2 }
  { s1*2 | <>-\sug\f }
>> r2\fermata |
R1^\fermataMarkup |
R1^\fermataMarkup |
R1*2 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''2 re'' | }
  { do''2 si' | }
>>
do''1\p\cresc |
<>\! \twoVoices#'(oboe1 oboe2 oboi) <<
  { do''4 fa'' sol'' la'' |
    sol''4 do'''2 do'''4 |
    si''1 |
    do'''4 }
  { do''4 fa'' mi'' red'' |
    mi''4 mi''2 mi''4 |
    re''1 |
    do''4 }
  { s1 | s-\sug\fp | s-\sug\f }
>> r8 mi''16 fa'' sol''( fa'' mi'' re'' do'' si' la' sol') |
la'8 si'16( do'' re'' mi'' fa'' sol'') la''( sol'' fa'' mi'' re'' do'' si' la') |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { si'4 sol''2 sol''4 |
    mi'' la'' sol'' si' |
    do'' do' do' }
  { si'4 si'2 re''4 |
    do'' do'' mi'' re'' |
    do'' do' do' }
>> r4 |
