\score {
  <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { Trompettes \smaller en Ut }
    } <<
      \new Staff <<
        \keepWithTag #'() \global
        \keepWithTag #'tromba1 \includeNotes "trombe"
        { s4 s1*34 <>^"Trompettes" }
      >>
      \new Staff <<
        \keepWithTag #'() \global
        \keepWithTag #'tromba2 \includeNotes "trombe"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Cors \smaller en Sol }
      \haraKiri
    } <<
      \keepWithTag #'() \global
      \keepWithTag #'corni \includeNotes "corni"
      { s4 s1*21 <>^"Cors en Sol" }
    >>
  >>
  \layout { indent = \largeindent }
}
