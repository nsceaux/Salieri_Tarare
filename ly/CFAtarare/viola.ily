\clef "alto" r4 |
do'16-\sug\fp do' do' do' do'4:16 do'2:16-\sug\fp |
do':16-\sug\fp do':16-\sug\fp |
re':16-\sug\fp re':16-\sug\fp |
re':16-\sug\fp re':16-\sug\fp |
re':16-\sug\fp re':16-\sug\fp |
do':16-\sug\fp do':16 |
do':16-\sug\fp do':16 |
do':16-\sug\fp do'8 do'[-\sug\f re' mi'] |
fa' mi' re' do' si la sol fad |
sol4 sol16 sol sol sol sol4 r |
<sol mi'>8\ff q4 q q q8 |
<sol fa'>8 q4 q8 <sol re'>8 q4 q8 |
\once\slurDashed do'(-\sug\p re'16 mi' fa' sol' la' si' do'' si' la' sol' fa' mi' re' do') |
si( la sol la si do' re' mi' fa' mi' re' do' si la sol fa) |
mi8 do' do' do' mi'2:8 |
re':8 re':8\< <>\! |
re':8 re':8-\sug\p |
mib':8 mib':8 |
mib':8-\sug\cresc mib':8 |
mib':8-\sug\f mib':8 |
re'16 <re' re''> q q q4:16\f q2:16 |
q:16\p q:16 |
q:16 q:16 |
q:16 q:16 |
q:16 q:16 |
q8 sol[ sol sol] sol2:8 |
sol:8-\sug\f sol:8 |
sold:8 sold:8 |
la2 r |
R1^\fermataMarkup |
fa'4.\fermata r8 re'2-\sug\fp |
sol' si-\sug\fp |
do'4 do' la fa |
sol2:8 sol:8 |
do'4.-\sug\f do'16 re' mi'4. do'16 re'32 mi' |
fa'4. fa'16 mi' re'4. re'16 do' |
si-\sug\fp re'' re'' re'' re''4:16 re''2:16 |
do''4( sib' lab' sol') |
lab'2:8 lab':8-\sug\p |
lab':8-\sug\f lab':8 |
fa':8 fa':8 |
re'2 r4\fermata r8 mi!-\sug\f |
fa2 r\fermata |
r4.\fermata r8 re'2-\sug\fp |
sol' si-\sug\fp |
do'2:8 la8 la fa fa |
sol2:8 sol':8 |
sol'8 sol'4 sol'-\sug\cresc sol' sol'8 <>\! |
do'4 do''2 do''4 |
do''8-\sug\fp mi'' sol' do'' mi' sol' do' mi' |
sol2:8-\sug\f sol:8 |
do':8 do':8 |
do':8 la4 re' |
re'8 \once\slurDashed mi'16( fa' sol' la' si' do'' re'' do'' si' la' sol' fa' mi' re') |
mi'4 fa' sol' sol |
do' do' do' r |
