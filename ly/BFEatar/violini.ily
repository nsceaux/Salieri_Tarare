\clef "treble" r16 la''\f si'' dod''' |
re'''4 re''8. mi''16 fad''4.\trill mi''16 re'' |
la''2 r\fermata |
la16\p la' sold' fad' mi' re' dod' si la si dod' re' mi' fad' sold' la' |
sol' fad' mi' fad' sol' fad' mi' fad' sol' fad' mi' fad' sol' fad' mi' fad' |
sol' mi' fad' sol' la' si' dod'' re'' mi'' re'' dod'' si' la' sol' fad' mi' |
fad' mi' re' dod' re' mi' fad' sol' fad' mi' re' dod' re' mi' fad' sol' |
<fad' la'>2:8\fp q:8 |
<sol' si'>:8\fp q:8 |
<<
  \tag #'violino1 {
    <mi' dod''>2:8\fp q:8 |
    <re' re''>4
  }
  \tag #'violino2 <<
    { la'2:8 la':8 | la'4 } \\
    { sol'2:8-\sug\fp sol':8 | fad'4 }
  >>
>> fad''8.\ff fad''16 fad''4 <la' la''>4 |
<re' re''>4 q8. q16 q4 fad'' |
la'4 la'8. la'16 la'4 <re' re''> |
re'4. mi'8 fad'4. sold'8 |
la'2.*1/3 s2\fermata r4 |
la16 si dod' re' mi' fad' sold' la' la4 r |
mi'16 fad' sold' la' si' dod'' red'' mi'' mi'4 r |
mi'16 fad' sold' la' si' dod'' red'' mi'' mi'4 r |
la'16 si' dod'' re'' mi'' fad'' sold'' la'' la' si' dod'' re'' mi'' fad'' sold'' la'' |
la'4 <<
  \tag #'violino1 {
    la''4:16 si'':16 dod''':16 |
    re'''4 re'''8 dod''' si''4 si''8 la'' |
    sold''4 mi'''2:16 mi'''4:16 |
    mi''4 r r2 |
    r4 mi''(\p mi'' mi'') |
    fad''2 fad''4( si'') |
    la''( sold'' fad'' mi'') |
    la''4 mi''8. fad''16 mi''4 la'' |
    si'' mi''8. fad''16 mi''4 re''' |
    r dod''' r la'' |
    r fad'' r sold'' |
    la''2
  }
  \tag #'violino2 {
    dod''4:16 re'':16 mi'':16 |
    la'4 la'8 la' fad''4 fad'8 fad' |
    si4 <dod'' la''>4:16 <si' sold''>:16 <dod'' la''>:16 |
    <si' sold''> mi'8. mi'16 mi'4 re' |
    dod' mi'(\p mi' mi') |
    fad'2 fad'4( si') |
    la'( sold' fad' mi') |
    la'4 mi'8. fad'16 mi'4 la' |
    si' mi'8. fad'16 mi'4 <mi' si'> |
    r <mi' dod''> r <la' mi''> |
    r re'' r si' |
    dod''2
  }
>> r8\fermata la''\f si'' dod''' |
re'''2 re'4.\p re'8 |
do'2.\trill si8 do' |
si2\fermata~ si8 <<
  \tag #'violino1 { si''8\f[ dod''' red'''] | mi'''2 }
  \tag #'violino2 { si'8-\sug\f[ dod'' red''] | mi''2 }
>> mi'4.\p mi'8 |
re'2.\trill dod'8 re' |
dod'1\fermata |
<<
  \tag #'violino1 {
    dod''2:16\fp dod'':16 |
    dod'':16 dod'':16 |
    si':16 si':16 |
    la'8 r re''2:16\fp re''4:16 |
    re''2:16 re'':16 |
    re''2:16 re'':16 |
    re''4:16\f si':16 re'':16 sol'':16 |
    si''4 <re' si' si''>4\p q q |
    <re' re'' re'''>2.\ff si''4 |
    re'''4.\fp re'''8 dod'''4. si''8 |
    lad''4 fad''16\ff fad'' fad'' fad'' fad''4 r |
    r4 <si'' si'>:16 q4 r |
    r <si' sol''>:16 q r |
    r <mi'' dod'''>:16 q r4 |
  }
  \tag #'violino2 {
    mid'2:16-\sug\fp mid':16 |
    mid':16 mid':16 |
    sold':16 sold':16 |
    fad'8 r <fad' la'>2:16-\sug\fp q4:16 |
    q2:16 q:16 |
    <re' do''>2:16 q:16 |
    <re' si'>2:16-\sug\f q:16 |
    q:16 q:16\p |
    q:16-\sug\ff q:16 |
    q:16-\sug\fp q:16 |
    lad'4 <<
      { dod''16 dod'' dod'' dod'' dod''4 } \\
      { lad'16-\sug\ff lad' lad' lad' lad'4 }
    >> r4 |
    r4 <re'' fad''>:16 q4 r |
    r4 <si' sol''>:16 q4 r |
    r4 <mi'' dod'''>:16 q4 r |
  }
>>
r4 <fad'' re'''>2:16\p q4:16 |
<<
  \tag #'violino1 {
    <fa'' re'''>8 r re'''4\cresc re''' re''' |
    fa'''2 re'''4. re'''8\! |
    re'''2 re'''4. re'''8 |
    dod'''2 r | \allowPageTurn
  }
  \tag #'violino2 {
    <fa'' re'''>16-\sug\cresc <<
      { fa''16[ fa'' fa''] fa''4:16 fa''2:16 |
        fa'':16 fa'':16 |
        fa''16 fa'' fa'' fa'' fa''4:16 } \\
      { re''16[ re'' re''] re''4:16 re''2:16 |
        re'':16 re'':16 |
        re''16 re'' re'' re'' re''4:16 }
    >> sold''16 sold'' sold'' sold'' sold''4:16 |
    la''2 r | \allowPageTurn
  }
>>
r4 fad'\p fad' fad' |
sol'2 mi'4. re'8 |
dod'2 la'4. sol'8 |
fad'4 fad'8 sol' fad'4 la' |
<<
  \tag #'violino1 {
    re''1 |
    re'' |
    re''2 dod'' |
    si'1\fermata |
    r4 re''8 dod'' re''4 fad'' |
    r mi'' r mi''\cresc |
    r mi'' r dod''\! |
  }
  \tag #'violino2 {
    re''2 re' |
    mi' mid' |
    fad' mi'! |
    re'1\fermata |
    <re' la'>2. re''4 |
    r re'' r re''-\sug\cresc |
    r dod'' r dod''\! |
  }
>>
