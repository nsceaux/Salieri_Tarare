\clef "bass" r16 la\f si dod' |
re'4 re8. mi16 fad4.\trill mi16 re |
la2 r\fermata |
\twoVoices #'(fagotto1 fagotto2 fagotti fagotti-part) <<
  { mi1~ | mi~ | mi | fad |
    la | si | dod' | re'4 }
  { dod1~ | dod~ | dod | re |
    fad | sol | mi1 | fad4 }
  { s1*4-\sug\p s1\fp s\fp s-\sug\fp }
>> fad8\ff fad fad4 la |
re4 re8 re re4 fad |
la,4 la,8 la, la,4 re' |
re4. mi8 fad4. sold8 |
la2.*1/3 s2\fermata r4 |
r2 la\f |
r mi' |
r mi' |
r4 la2 la4 |
la,4 la sold sol |
fad fad8 mi re4 re8 red |
mi2:8 mi:8 |
mi4 mi8. mi16 mi4 re! |
dod2 r |
<<
  \tag #'fagotti {
    R1*6 |
    r2 r\fermata |
    R1*2 |
    r2\fermata r |
    R1*2 |
    R1\fermataMarkup |
  }
  \tag #'fagotti-part { R1*13 }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti fagotti-part) <<
  { sold1~ | sold | si | la~ |
    la | do' | si1~ | si4 re'2 re'4 |
    re'1 | re' | }
  { mid1~ | mid | sold | fad~ |
    fad | la | sol1~ | sol4 si2 si4 |
    si1 | si | }
  { s1-\sug\fp | s1*5 | s1-\sug\f | s4 s2.-\sug\p |
    s1-\sug\ff | s1-\sug\fp }
>>
r4 <>-\sug\ff \twoVoices #'(fagotto1 fagotto2 fagotti fagotti-part) <<
  { dod'2 }
  { lad }
>> r4 |
r4 \twoVoices #'(fagotto1 fagotto2 fagotti fagotti-part) <<
  { re'2 }
  { si }
>> r4 |
r \twoVoices #'(fagotto1 fagotto2 fagotti fagotti-part) <<
  { mi'2 }
  { sol }
>> r4 |
r \twoVoices #'(fagotto1 fagotto2 fagotti fagotti-part) <<
  { mi'2 }
  { dod' }
>> r4 |
r <>-\sug\p \twoVoices #'(fagotto1 fagotto2 fagotti fagotti-part) <<
  { fad'2 fad'4 | fa'1 | re' | fa'2 re' | dod'! }
  { la2 la4 | la1 | sib | re'2 sib | la }
  { s2. | s1-\sug\cresc | s\! }
>> r2 |
R1*4 |
\clef "tenor" r2 <>^"solo" re' |
mi' mid' |
fad' mi'! |
re'1\fermata |
R1*3 |
