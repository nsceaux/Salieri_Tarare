<<
  \tag #'(atar basse) {
    \clef "bass" <>^\markup { Chanté \italic { Ton le plus brillant } }
    r16 la si dod' |
    re'4 re8. mi16 fad4. mi16 re |
    la8 la r4 r2\fermata |
    la2 la4. si8 |
    sol2. mi4 |
    sol sol sol la |
    fad2 r |
    la2. fad8 re |
    si2 sol4. sol8 |
    dod'2. si8 la |
    re'4 re' r2 |
    r4 re' re' fad' |
    la2. re'4 |
    re4. mi8 fad4. sold8 |
    la2*1/2 s4\fermata r4 mi |
    la2 r4 r8 mi |
    si2 r4 r8 si |
    mi'4 mi si mi' |
    dod' dod' r2 |
    r4 la si dod' |
    re' re'8 dod' si4 si8 la |
    sold2 r4 r8 mi' |
    mi2 r |
    r4 mi mi mi |
    fad2 fad4 si |
    sold2 fad4 mi |
    la la r2 |
    r4 si si re' |
    dod'2 la4. la8 |
    re2 mi4. mi8 |
    la2 r8\fermata la si dod' |
    re'2 re4. re8 |
    do2 do4.\fermata do8 |
    si,4*1/2 s8\fermata si,4 r8 si dod' red' |
    mi'2 mi4. mi8 |
    re2 re4. re8 |
    dod4 dod\fermata r2 |
    dod'2. sold8 mid |
    dod!2. dod4 |
    si si si si |
    la re'2 la8 fad |
    re2 re4. re8 |
    do'2 do'4. do'8 |
    si4 si r2 |
    r4 si si si |
    re'2. si4 |
    re'4. re'8 dod'4. si8 |
    fad2 r4 fad |
    si2. re'4 |
    sol2. sol4 |
    mi'4. dod'8 la4. sol8 |
    fad4 fad r2 |
    r4 re' re' re' |
    fa'2 re'4. re'8 |
    re'2 re'4. re'8 |
    la2 r\fermata |
    r4 fad fad fad |
    sol2 mi4. re8 |
    dod2 la4. sol8 |
    fad4 fad r2 |
    r4 re' re' fad |
    sol2 sold4. sold8 |
    la2 lad4. lad8 |
    si2 r\fermata |
    r4 re' re' fad |
    sol2 mi4. mi8 |
    la2 la4. la8 |
  }
  \tag #'tarare {
    \clef "tenor/G_8" r4 |
    R1*32 |
    r4*1/2 s8\fermata <>^\markup\character-text Tarare suppliant r8 fad' red'2 |
    R1*33 |
  }
>>
