\clef "bass" r4 |
R1*47 |
<>^\markup\whiteout { Timballes in Ré } r4 re:16-\sug\ff re r |
r4 re:16 re r |
r4 la,:16 la, r |
r4 re2:16-\sug\p re4:16 |
re2:16-\sug\cresc re:16 |
re2:16 re:16 |
re2:16 re:16 |
la,2\! r |
R1*7 |
R1^\fermataMarkup |
R1*3 |
