\clef "treble" \transposition re
r4 |
R1 |
r2 r\fermata |
R1*4 |
r4 <>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { do'4 do''2~ | do''4 do' do''2 | }
  { do'4 do''2~ | do''4 do' do''2 | }
>>
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { do'4 do''2~ |
    do''4 mi''8. mi''16 mi''4 sol'' |
    do'' do''8. do''16 do''4 mi'' |
    sol'4 sol'8. sol'16 sol'4 do'' | }
  { do'4 do''2~ |
    do''4 mi'8. mi'16 mi'4 sol' |
    do'4 do''8. do''16 do''4 mi'' |
    sol'4 sol'8. sol'16 sol'4 do'' | }
  { s2. | s4 s2.-\sug\ff }
>>
do'1 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { sol2.*1/3 }
  { sol }
>> s2\fermata r4 |
r2 <>\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { re'' }
  { sol' }
>>
r2 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re'' }
  { re'' }
>>
r2 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re'' }
  { re'' }
>>
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''2 re''4 | re''4 }
  { sol'2 sol'4 | sol' }
>> r4 r2 |
R1 |
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''2 re''4 | re'' }
  { re''2 re''4 | re'' }
>> r4 r2 |
R1*7 |
r2 r\fermata |
R1*2 |
r2\fermata r |
R1*2 |
R1\fermataMarkup |
R1*3 |
r4 <>\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2 sol'8 mi' | }
  { do''2 sol'8 mi' | }
>>
do'1 |
do'~ |
do' |
R1*3 |
r4 <>-\sug\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2 }
  { mi' }
>> r4 |
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2 }
  { do'' }
>> r4
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''2 }
  { re'' }
>> r4 |
r \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''2 }
  { sol' }
>> r4 |
r <>-\sug\p \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2 mi''4 | do''1~ | do'' | do'' | sol'2 }
  { do''2 do''4 |
    \tag #'trombe \hideNotes do''1~ | do'' |
    do'' \tag #'trombe \unHideNotes | sol'2 }
  { s2. | s1-\sug\cresc | s\! }
>> r2 |
R1*7 |
R1^\fermataMarkup |
R1*3 |
