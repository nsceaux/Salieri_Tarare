\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        \consists "Metronome_mark_engraver"
        \oboiInstr
      } << \global \keepWithTag #'oboi \includeNotes "oboi" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes en Ré }
        shortInstrumentName = "Tr."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'trombe \includeNotes "trombe"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with { \timpaniInstr \haraKiriFirst } <<
        \keepWithTag #'() \global \includeNotes "timpani"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Tarare
      shortInstrumentName = \markup\character Ta.
      \haraKiriFirst
    } \withLyrics <<
      \global \keepWithTag #'tarare \includeNotes "voix"
    >> \keepWithTag #'tarare \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\character Atar
      shortInstrumentName = \markup\character At.
    } \withLyrics <<
      \global \keepWithTag #'atar \includeNotes "voix"
    >> \keepWithTag #'atar \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { Violoncelli et Basso }
      shortInstrumentName = "B."
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s4 s1*3\break s1*5\pageBreak
        s1*6\break s1*4\pageBreak
        s1*6\break s1*6\pageBreak
        s1*6\break s1*6\pageBreak
        s1*5\break s1*6\pageBreak
        s1*6\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
