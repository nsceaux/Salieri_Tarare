\clef "bass" r16 la\f si dod' |
re'4 re8. mi16 fad4.\trill mi16 re |
la2 r\fermata |
la,2:8\p la,:8 |
la,:8 la,:8 |
la,:8 la,:8 |
re:8 re:8 |
r4 re\f re'2~ |
re'4 re re'2~ |
re'4 re re'2~ |
re'4 fad8\ff fad fad4 la |
re re8 re re4 fad |
la, la,8 la, la,4 re' |
re4. mi8 fad4. sold8 |
la2*1/2 s2\fermata r4 |
<<
  { la,16 si, dod re mi fad sold la la,4 } \\
  { la,8 dod mi la la,4 }
>> r4 |
<<
  { mi16 fad sold la si dod' red' mi' mi4 } \\
  { mi8 sold si mi' mi4 }
>> r4 |
<<
  { mi16 fad sold la si dod' red' mi' mi4 } \\
  { mi8 sold si mi' mi4 }
>> r4 |
<<
  { la,16 si, dod re mi fad sold la la, si, dod re mi fad sold la | } \\
  { la,8 dod mi la la, dod mi la | }
>>
la,4 la sold sol |
fad fad8 mi re4 re8 red |
mi2:8 mi:8 |
mi4 mi8. mi16 mi4 re! |
dod1\p |
re |
re,2 re |
dod r4 dod' |
sold2 r4 sold |
la r dod r |
re r mi r |
la,2 r8\fermata la\f si dod' |
re'2 re4.\p re8 |
do2.\trill si,8 do |
si,2\fermata~ si,8 si[\f dod' red'] |
mi'2 mi4.\p mi8 |
re2.\trill dod8 re |
dod1\fermata |
dod2:8\fp dod:8 |
dod:8 dod:8 |
mid:8 mid:8 |
fad8 r re\f re re2:8\p |
re:8 re:8 |
fad:8 fad:8 |
sol8\f sol sol sol sol sol sol sol |
sol2:8 sol:8\p |
fad:8\fp fad:8 |
mid:8\fp mid:8 |
fad4 fad\ff fad r |
r si, si r |
r mi mi' r |
r la, la r |
r re\p fad la |
re'2:8\cresc re':8 |
sib:8 sib:8 |
sib:8 sib:8 |
la2\! r\fermata |
re1\p |
mi2 sol |
la dod |
re r |
r fad |
sol sold |
la lad |
si1\fermata |
fad |
sol4 r mi\cresc r |
la r la,\! r |
