\clef "alto" r16 la'-\sug\f si' dod'' |
re''4 re'8. mi'16 fad'4.\trill mi'16 re' |
la'2 r\fermata |
dod2:8-\sug\p dod:8 |
dod:8 dod:8 |
dod:8 dod:8 |
re:8 re:8 |
r4 re-\sug\f re'2~ |
re'4 re re'2~ |
re'4 re re'2~ |
re'4 fad'8.-\sug\ff fad'16 fad'4 la' |
re'4 re'8. re'16 re'4 fad' |
la4 la8. la16 la4 re' |
re4. mi8 fad4. sold8 |
la2.*1/3 s2\fermata r4 |
la16 si dod' re' mi' fad' sold' la' la4 r |
mi16 fad sold la si dod' red' mi' mi4 r |
mi16 fad sold la si dod' red' mi' mi4 r |
la16 si dod' re' mi' fad' sold' la' la si dod' re' mi' fad' sold' la' |
la4 la':16 sold':16 sol':16 |
fad'4 fad'8 mi' re'4 re'8 red' |
mi'4 <mi' mi''>2:16 q4:16 |
mi'4 mi'8. mi'16 mi'4 re' |
dod'1-\sug\p |
re' |
si2 sold |
la2. mi'4 |
mi'2. sold4 |
la r dod' r |
re' r mi' r |
la2 r8\fermata la'8\f si' dod'' |
re''2 re'4.-\sug\p re'8 |
do'2.\trill si8 do' |
si2\fermata~ si8 si'-\sug\f[ dod'' red''] |
mi''2 mi'4.-\sug\p mi'8 |
re'2.\trill dod'8 re' |
dod'1\fermata |
sold'2:16-\sug\fp sold':16 |
sold':16 sold':16 |
<dod' sold'>:16 q:16 |
<dod' la'>8 r <la fad'>2:16-\sug\fp q4:16 |
q2:16 q:16 |
<re' la'>:16 q:16 |
<re' si'>:16-\sug\f q:16 |
q:16 q:16-\sug\p |
q:16-\sug\ff q:16 |
q:16-\sug\fp q:16 |
fad'4 <fad' dod''>16-\sug\ff q q q q4 r |
r4 <fad' re''>4:16 q4 r |
r <sol' mi''>:16 q4 r |
r <sol' mi''>:16 q4 r |
r4 <fad' re''>2:16-\sug\p q4:16 |
<fa' re''>2:16-\sug\cresc q:16 |
q:16 q:16 |
q:16 q:16 |
<mi' dod''>2 r |
r4 fad\p fad fad |
sol2 mi4. re8 |
dod2 la4. sol8 |
fad4 fad8 sol fad4 la |
re'2 fad |
sol sold |
la lad |
si1\fermata |
fad1 |
sol4 r mi-\sug\cresc r |
la r la'\! r |
