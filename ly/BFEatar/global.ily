\tag #'all \key re \major
\tempo "Maëstoso" \midiTempo#100
\time 4/4 \partial 4 s4 s1*2
\tempo "Allegro assai" \midiTempo#120 s1*64
