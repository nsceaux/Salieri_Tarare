\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso)
   (fagotti ;#:system-count 6
            #:tag-notes fagotti-part
            #:music , #{
\quoteBasso "BFEbasso"
s4 s1*23 <>^\markup\tiny "Basso"
\cue "BFEbasso" { \mmRestDown s1*13 \mmRestCenter }
#})
   (oboi #:score-template "score-oboi")
   (trombe #:score "score-tr-timb")
   (timpani #:score "score-tr-timb")
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #}))
