\clef "treble" r4 |
r <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re'8. mi'16 fad'4.\trill mi'16 re' | la'2 }
  { re'8. mi'16 fad'4.\trill mi'16 re' | la'2 }
>> r2\fermata |
R1*4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''1 |
    si'' |
    dod''' |
    re'''4 fad''8. fad''16 fad''4 la'' |
    re''4 re''8. re''16 re''4 fad'' |
    la'4 la'8. la'16 la'4 re'' |
    re'4. mi'8 fad'4. sold'8 |
    la'2.*1/3 }
  { fad''1 |
    sol'' |
    mi'' |
    fad''4 fad''8. fad''16 fad''4 la'' |
    re''4 re''8. re''16 re''4 fad'' |
    la'4 la'8. la'16 la'4 re'' |
    re'4. mi'8 fad'4. sold'8 |
    la'2.*1/3 }
  { s1\fp | s\fp | s-\sug\fp | s4 s2.-\sug\ff }
>> s2\fermata r4 |
r2 <>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 }
  { dod'' }
>>
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sold''2 }
  { si' }
>>
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sold''2 }
  { si' }
>>
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 la''4 |
    la''4 la'' si'' dod''' |
    re'''4. dod'''8 si''4. la''8 |
    sold''4 la'' sold'' la'' |
    sold'' }
  { dod''2 dod''4 |
    dod''4 la' si' dod'' |
    re''4. dod''8 si'4. la'8 |
    sold'4 dod'' si' dod'' |
    si' }
>> r4 r2 |
R1*7 |
r2 r\fermata |
R1*2 |
r2\fermata r |
R1*2 |
R1^\fermataMarkup |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''1~ |
    dod''~ |
    dod''~ |
    dod''4 re''2. |
    re''1~ |
    re''~ |
    re''4 re''2 si'4( |
    re''4) si''2 si''4 |
    si''1 |
    re'''2 dod'''4. si''8 | }
  { \tag #'oboi \hideNotes dod''1~ |
    dod''~ |
    dod''~ | \tag #'oboi \unHideNotes
    dod''4 re''2. |
    \tag #'oboi \hideNotes re''1~ |
    re''~ | \tag #'oboi \unHideNotes
    re''4 si'2 si'4~ |
    si'4 re''2 re''4 |
    re''1 |
    re'' |
  }
  { s1\fp | s1*2 | s4 s2.\f | s1*2 | s1-\sug\f | s4 s2.-\sug\p |
    s1-\sug\ff | s1-\sug\fp }
>>
r4 <>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { lad''2 }
  { dod'' }
>> r4 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { si''2 }
  { re'' }
>> r4 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2 }
  { si' }
>> r4 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { dod'''2 }
  { mi'' }
>> r4 |
r <>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { re'''2 re'''4 | re'''1~ | re''' | re'''1 | dod'''2 }
  { fad''2 fad''4 | fa''1~ | fa'' | fa''2. re''4 | mi''2 }
  { s2. | s1-\sug\cresc | s\! }
>> r2 |
R1*7 |
R1^\fermataMarkup |
R1*3 |
