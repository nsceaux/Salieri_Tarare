\tag #'(atar basse) {
  Qu’as- tu donc fait de ton mâ -- le cou -- ra -- ge ?
  Toi qu’on voy -- ait ru -- gir dans les com -- bats,
  toi qui for -- ças un tor -- rent à la na -- ge,
  en trans -- por -- tant ton maî -- tre dans tes bras !
  Le fer, le feu, le sang et le car -- na -- ge
  n’ont ja -- mais pu t’ar -- ra -- cher un sou -- pir,
  ja -- mais ; et l’a -- ban -- don d’une es -- cla -- ve vo -- la -- ge
  a -- bat ton âme et la force à gé -- mir !
}
\tag #'tarare {
  Sei -- gneur !
}
\tag #'(atar basse) {
  Qu’as- tu donc fait de ton mâ -- le cou -- ra -- ge ?
  Qu’as- tu donc fait de ton mâ -- le cou -- ra -- ge ?
  Toi qu’on voy -- ait ru -- gir dans les com -- bats,
  toi qui for -- ças un tor -- rent à la na -- ge,
  en trans -- por -- tant ton maî -- tre dans tes bras !
  Le fer, le feu, le sang et le car -- na -- ge
  n’ont ja -- mais pu t’ar -- ra -- cher un sou -- pir ;
  et l’a -- ban -- don d’une es -- cla -- ve vo -- la -- ge
  a -- bat ton âme et la force à gé -- mir,
  a -- bat ton âme et la force à gé -
}
