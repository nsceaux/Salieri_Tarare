\score {
  <<
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag#'basse \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag#'basse \includeLyrics "paroles" }
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff <<
        { s4 s1*6 <>^"Trompettes en Ré" }
        \keepWithTag #'() \global
        \keepWithTag #'trombe \includeNotes "trombe"
      >>
      \new Staff <<
        \keepWithTag #'() \global \includeNotes "timpani"
        { s4 s1*22\break s1*33 \break }
      >>
    >>
  >>
  \layout { }
}
