\score {
  \new StaffGroup <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { Cors et Trompettes \small en Ré }
    } <<
      \new Staff <<
        \keepWithTag #'() \global
        \keepWithTag #'corno1 \includeNotes "corni"
      >>
      \new Staff <<
        \keepWithTag #'() \global
        \keepWithTag #'corno2 \includeNotes "corni"
      >>
    >>
    \new Staff \with { instrumentName = "Timbales" } <<
      \global \includeNotes "timpani"
    >>
    \new DrumStaff \with {
      \override StaffSymbol.line-count = #1
      instrumentName = \markup\center-column { Grand Tambour }
      drumStyleTable = #percussion-style
    } << \global \includeNotes "tambour" >>
  >>
  \layout { indent = \largeindent }
}
