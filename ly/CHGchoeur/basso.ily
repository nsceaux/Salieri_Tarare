\clef "bass" re4.\ff re8 fad4 la |
re1 |
re4 re8. re16 re4 dod |
si, fad, si,2 |
sol4 mi dod la, |
re la, re fad |
la fad dod re |
la,2 la\ff |
sold2:8 sold:8 |
la:8 la:8 |
sold:8 sold:8 |
la4 la,8 si, dod re mi fad |
sold la si dod' re' mi' fad' sold |
la2 re4. re8 |
mi4 mi8. mi16 mi4 mi |
red1 |
re! |
dod2:8 re:8 |
mi:8 mi:8 |
la,4 la,8. la,16 la,2 |
la,2 r8 la,\ff dod la, |
mi4 mi8. mi16 mi4 mi |
dod2 r8 si,16\ff dod \grace mi16 re8 dod16 si, |
fad4 fad8. fad16 fad4 fad |
re2 r8 si,\f re si, |
sol4. fad8 mi re dod si, |
lad,4.\f fad,8 re4.\f dod16 si, |
fad4.\f sold16 lad si8 fad re si, |
fad4 fad8. fad16 fad2 |
re4\p fad re fad |
sol re sol2 |
mi4 la, mi la, |
mi si mi2 |
R1 |
re4\f re8. dod16 re4 re' |
dod' si la fad |
si8 si, si, si, si,2:8 |
mi2 r |
<>^\markup\center-align "Violoncelli" dod' re' |
la,2.\ff ^"Tutti" la4 |
si,2 r |
la,2.\ff la4 |
re2 r8 la, dod la, |
re4 re8. re16 re4 r |
