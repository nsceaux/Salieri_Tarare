Bra -- ma ! si la ver -- tu t’es chè -- re,
si la voix du peu -- ple, la voix du peuple est ta voix,
par des suc -- cès sou -- tiens le choix
que le peuple en -- tier vient de fai -- re,
que le peuple en -- tier vient de fai -- re ! - re !
Que sur ses pas
tous nos sol -- dats
mar -- chent d’une au -- da -- ce plus fiè -- re !
Que l’en -- ne -- mi, triste, a -- bat -- tu,
que l’en -- ne -- mi, triste, a -- bat -- tu,
par son as -- pect dé -- jà vain -- cu,
par son as -- pect \tag #'(vhaute-contre basse) { dé -- jà } vain -- cu,
sous nos coups mor -- de la pous -- siè -- re,
mor -- de la pous -- siè -- re ! - re !
