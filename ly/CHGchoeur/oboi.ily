\clef "treble"
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { re'4. re'8 fad'4 la' |
    re''1 |
    fad''4 fad''8. fad''16 fad''4 lad'' |
    si'' \grace re'''16 dod'''8 si''16 dod''' re'''2 | }
  { re'4. re'8 fad'4 la' |
    fad'1 |
    la'4 la'8. la'16 la'4 mi'' |
    re'' lad' si'2 | }
>>
\tag #'oboi <>^"a 2" sol''4 mi'' dod'' la' |
re'' la' re'' fad'' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 sol''4 fad'' | mi''2 }
  { la'2 mi''4 re'' | dod''2 }
>> r2 |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''8 mi'' mi'' re'' mi'' si' mi'' | dod''1 | re'' | dod''4 }
  { si'8 si' si' si'4 si' | la'1 | si' | la'4 }
>> r4 r2 |
r4 \tag #'oboi <>^"a 2" si'8 dod'' re'' mi'' fad'' sold'' |
la''2 fad''4. fad''8 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4 dod''8. dod''16 si'4 si' |
    fad'' fad''8. fad''16 \slurDashed fad''4( si'') |
    la''8( sold'' fad'' mi'') mi''4 mi'' | \slurSolid
    la''2 fad'' |
    mi''2 sold'' |
    la'' }
  { mi''4 la'8. la'16 sold'4 sold' |
    si'1~ |
    si'2 sold' |
    mi'' re'' |
    dod'' si' |
    la' }
>> r |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 }
  { la'2 }
>> r8 \tag #'oboi <>^"a 2" la'16-\sug\ff si' \grace re''16 dod''8 si'16 la' |
mi''4 mi''8. mi''16 mi''4 mi'' |
dod''2 r8 si'16\ff dod'' \grace mi'' re''8 dod''16 si' |
fad''4 fad''8. fad''16 fad''4 fad'' |
re''2 r8 si'16-\sug\f dod'' \grace mi'' re''8 dod''16 si' |
sol''4 sol''8. fad''16 mi''8 re'' dod'' si' |
lad'4.\trill sold'16 fad' re''4.\trill\f dod''16 si' |
fad''4.\f sold''16 lad'' si''8 fad'' re'' si' |
fad''4 fad'8. fad'16 fad'2 |
re''4\p re''8. re''16 re''4 re'' |
re''4. re''8 re''2 |
mi''4 mi''8. mi''16 mi''4 mi'' |
mi''4. red''8 mi''2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''4-\sug\f la''8. sold''16 la''4 dod''' |
    re''' la'' fad'' re'' |
    la''4 la''8. sold''16 la''4 lad'' |
    si''8 }
  { mi''2.-\sug\f mi''4 |
    fad'' mi'' re'' fad' |
    mi'4 \grace mi''8 re'' dod''16 re'' dod''4 mi'' |
    re''8 }
>> si'16 si' si'8 si' si'2:8 |
mi''4 mi''16 fad'' sol'' la'' si'' la'' sol'' fad'' mi'' re'' dod'' si' |
la'2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''\f~ |
    fad''8. mi''16 fad''8. sol''16 la''2 |
    si' fad''2~ |
    fad''8. mi''16 fad''8. sol''16 la''2 |
    re'' }
  { re''2~ |
    re''8. re''16 re''8. re''16 dod''2 |
    si' re''~ |
    re''8. re''16 re''8. re''16 dod''2 |
    re'' }
>> r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 }
  { re'' }
>> r |
