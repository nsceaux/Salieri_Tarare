\clef "bass" re2-\sug\ff r |
re r |
re4 re8. re16 re4 la, |
re la, re2 |
la,4 la,8. la,16 la,4 la, |
re la,8. la,16 re4 re |
la,4 la,8. la,16 la,4 re |
la,2 r |
r8 re re re re2:8 |
la, r |
r8 re re re re2:8 |
la,2 r |
R1*5 |
la,8 la,16 la, la,8 la, re re16 re re8 re |
la,2:16 re:16 |
la,2 r |
la, r8 la,16-\sug\ff la, la,8 la, |
re2 r |
r r8 re16-\sug\ff re re8 re |
re2 r |
r2 r8 re16-\sug\f re re8 re |
re2 re |
R1*8 |
re4-\sug\f re8. la,16 re4 re |
la,4 la,8. la,16 la,4 la, |
re8 re16 re re8 re re2:8 |
re r |
R1 |
la,4-\sug\ff la,8. la,16 la,4 la, |
re2 r |
la,4-\sug\ff la,8. la,16 la,4 la, |
re2 r8 la,16 la, la,8 la, |
re2 r |
