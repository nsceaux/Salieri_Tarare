\tag #'all \key re \major
\tempo "Marche, sans lenteur" \midiTempo#120
\time 2/2 s1*19 \alternatives s1 s1
\bar ".|:" s1*21 \alternatives s1 s1 \bar "|."
