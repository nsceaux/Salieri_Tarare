\clef "treble" \transposition re
<>-\sug\ff \twoVoices #'(corno1 corno2 corni) <<
  { do'4. do'8 mi'4 sol' |
    do''1 |
    mi''4 mi''8. mi''16 mi''4 re'' |
    do'' mi'' do''2 | }
  { do'4. do'8 mi'4 sol' |
    mi'1 |
    do''4 mi'8. mi'16 mi'4 mi' |
    mi' mi' mi'2 | }
>>
R1 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { sol' do'' mi'' | sol'2 re''4 mi'' | re''2 }
  { sol'4 do'' mi'' | sol'2 sol'4 do'' | sol'2 }
>> r2 |
r8 \twoVoices #'(corno1 corno2 corni) <<
  { re''8 re'' re'' re''2:8 | }
  { re''8 re'' re'' re''2:8 | }
>>
re''1 |
re'' |
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 }
  { re'' }
>> r4 r2 |
R1*2 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''8. re''16 re''4 re'' | mi''2 }
  { re''8. re''16 re''4 re'' | mi''2 }
>> r2 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''8. re''16 re''4 re'' | re''2 mi'' | }
  { re''8. re''16 re''4 re'' | sol'2 do'' | }
>>
re''1 |
sol'2 r |
sol' r |
R1*2 |
<>-\sug\ff \twoVoices #'(corno1 corno2 corni) <<
  { mi''4 mi''8. mi''16 mi''4 mi'' | do''2 }
  { mi'4 mi'8. mi'16 mi'4 mi' | do'2 }
>> r2 |
R1 |
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { mi''1 | mi'' | mi''4 mi'8. mi'16 mi'2 | }
  { mi'1 | mi' | mi'4 mi'8. mi'16 mi'2 | }
>>
R1*4 |
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { re''4 re''8. re''16 re''4 re'' |
    mi'' re'' do'' mi'' |
    re''4 re''8. re''16 re''4 re'' |
    do'' do'' do'' }
  { re''4 sol'8. sol'16 sol'4 sol' |
    do'' sol' mi' do'' |
    sol'4 sol'8. sol'16 sol'4 mi' |
    mi' mi' mi' }
>> r4 |
R1*2 |
<>\ff \twoVoices #'(corno1 corno2 corni) <<
  { sol'1 | do'2 }
  { sol1 | do'2 }
>> r2 |
<>\ff \twoVoices #'(corno1 corno2 corni) <<
  { sol'1 | do''2 }
  { sol1 | do'2 }
>> r2 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 }
  { do'2 }
>> r |
