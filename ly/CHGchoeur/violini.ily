\clef "treble" re'4.\ff re'8 fad'4 la' |
<la fad' re''>4. mi''16*2/3 re'' dod'' re''4 re' |
<<
  \tag #'violino1 {
    fad''4 fad''8. fad''16 fad''4 lad'' |
    si'' \grace re'''16 dod'''8 si''16 dod''' re'''2 |
  }
  \tag #'violino2 {
    la'4 la'8. la'16 la'4 mi'' |
    re'' lad' si'2 |
  }
>>
sol''4 mi''8. mi''16 dod''4 la' |
re'' la'8. la'16 re''4 fad'' |
<<
  \tag #'violino1 { la''2 sol''4 fad'' | mi''2 }
  \tag #'violino2 { la'2 mi''4 re'' | dod''2 }
>> r8 la'16\f si' \grace re''8 dod'' si'16 la' |
mi''8 mi' mi' mi' re' mi' si mi' |
dod' la dod' mi' la' la'16 si' \grace re'' dod''8 si'16 la' |
mi''8 mi' mi' mi' re' mi' si mi' |
dod'4 la8 si dod' re' mi' fad' |
sold' la' si' dod'' re'' mi'' fad'' sold'' |
la''2 <re' la' fad''>4. <<
  \tag #'violino1 {
    fad''8 |
    mi''4 mi''8. mi''16 mi''4 mi'' |
    fad''4 fad''8. fad''16 fad''4( si'') |
    la''8( sold'' fad'' mi'') mi''4 mi'' |
    la''8 dod'''16 si'' la''8 la'' fad'' fad''16 fad'' la''8 fad'' |
    mi''16 la' dod'' mi'' la'' mi'' dod'' la' mi'4 <mi' si' sold''> |
    <mi' dod'' la''>4
  }
  \tag #'violino2 {
    la'8 |
    <<
      { dod''4 dod''8. dod''16 si'4 si' | si'1 | si' | } \\
      { la'4 la'8. la'16 sold'4 sold' | fad'1 | mi'2 sold' | }
    >>
    la'8 <la' mi''> q q <la' fad''>2:8 |
    << { dod''2:8 si':8 } \\ { la':8 sold':8 } >> |
    la'4
  }
>> la8. la16 la2 |
<mi' dod'' la''>2 r8 la'16\ff si' \grace re''16 dod''8 si'16 la' |
mi''4 mi''8. mi''16 mi''4 mi'' |
dod''2 r8 si'16\ff dod'' \grace mi'' re''8 dod''16 si' |
fad''4 fad''8. fad''16 fad''4 fad'' |
re''2 r8 si'16-\sug\f dod'' \grace mi'' re''8 dod''16 si' |
sol''4 sol''8. fad''16 mi''8 re'' dod'' si' |
lad'4.\trill sold'16 fad' re''4.\trill\f dod''16 si' |
fad''4.\f sold''16 lad'' si''8 fad'' re'' si' |
fad''4 fad'8. fad'16 fad'2 |
<<
  \tag #'violino1 {
    re''4\p re''8. re''16 re''4 re'' |
    re''4. re''8 re''2 |
    mi''4 mi''8. mi''16 mi''4 mi'' |
    mi''4. red''8 mi''2 |
    la''4\f la''8. sold''16 la''4 <la' mi'' dod'''> |
    <la' fad'' re'''> <la' la''> <la' fad''> <re' re''> |
    <la' la''>4 la''8. sold''16 la''4 <fad' dod'' lad''> |
    <si'' si'>8
  }
  \tag #'violino2 {
    fad'4-\sug\p la'8 la' fad'4 la' |
    si'4. la'8 si'2 |
    si'4 do''8. do''16 si'4 do'' |
    si'4. si'8 si'2 |
    <dod' mi'>2.-\sug\f <la sol'!>4 |
    <la fad'> <la mi'> re' <la' fad''> |
    <la' mi''>4 \grace mi''8 re'' dod''16 re'' dod''4 <dod'' mi''> |
    re''8
  }
>> si'16 si' si'8 si' si'2:8 |
mi''4 mi''16 fad'' sol'' la'' si'' la'' sol'' fad'' mi'' re'' dod'' si' |
la'2 <<
  \tag #'violino1 {
    <re'' fad''>2\f~ |
    fad''8. mi''16 fad''8. sol''16 la''2 |
    si2 <re'' fad''>2~ |
    fad''8. mi''16 fad''8. sol''16 la''2 |
    re'' r8 la'16 si' \grace re'' dod''8 si'16 la' |
  }
  \tag #'violino2 {
    <re' re''>2~ |
    re''8. re''16 re''8. re''16 dod''2 |
    si <re' re''>2~ |
    re''8. re''16 re''8. re''16 dod''2 |
    re'' r |
  }
>>
re''4 re'8. re'16 re'2 |
