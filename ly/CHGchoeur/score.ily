\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with {
        instrumentName = \markup\center-column { Hautbois Flûtes Clarinettes }
        shortInstrumentName = \markup\center-column { Htb. Fl. Cl. }
      } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'oboe1 \includeNotes "oboi"
        >>
        \new Staff <<
          \global \keepWithTag #'oboe2 \includeNotes "oboi"
        >>
      >>
      \new GrandStaff \with {
        instrumentName = \markup\center-column { Cors et Trompettes \small en Re }
        shortInstrumentName = \markup\center-column { Cor. Tr. }
      } <<
        \new Staff <<
          \keepWithTag #'() \global
          \keepWithTag #'corno1 \includeNotes "corni"
        >>
        \new Staff <<
          \keepWithTag #'() \global
          \keepWithTag #'corno2 \includeNotes "corni"
        >>
      >>
      \new Staff \with { \timpaniInstr } << \global \includeNotes "timpani" >>
      \new DrumStaff \with {
        \override StaffSymbol.line-count = #1
        instrumentName = \markup\center-column { Grand Tambour }
        shortInstrumentName = "Tamb."
        drumStyleTable = #percussion-style
      } << \global \includeNotes "tambour" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { 
          \consists "Metronome_mark_engraver"
        } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      instrumentName = \markup\character Chœur
      shortInstrumentName = \markup\character Ch.
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
        >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
        >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Bassons Basses }
      shortInstrumentName = \markup\center-column { Bn. B. }
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*6\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*5\pageBreak
        s1*6\pageBreak
        s1*4\pageBreak
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
