\clef "alto" re'4.-\sug\ff re'8 fad'4 la' |
<fad' la'>1 |
re'4 re'8. re'16 fad'4 fad' |
fad' fad' fad'2 |
sol'4 mi' dod' la |
re' la re' fad' |
la'4 la'8 la' la'4 la' |
la2 r |
r8 <si mi'> q q q2:8 |
<dod' mi'>2:8 q:8 |
<si mi'>:8 q:8 |
<dod' mi'>4 la8 si dod' re' mi' fad' |
sold8 la si dod' re' mi' fad' sold' |
la'2 re'4. re'8 |
mi'4 mi'8. mi'16 mi'4 mi |
red'1 |
re'! |
dod'2:8 re':8 |
mi':8 mi':8 |
la4 la8. la16 la2 |
la2 r8 la16\ff si \grace re' dod'8 si16 la |
mi'4 mi'8. mi'16 mi'4 mi' |
dod'2 r8 si16-\sug\ff dod' \grace mi' re'8 dod'16 si |
fad'4 fad'8. fad'16 fad'4 fad' |
re'2 r8 si16\f dod' \grace mi' re'8 dod'16 si |
sol'4 sol'8. fad'16 mi'8 re' dod' si |
lad4.\f fad8 re'4.\f dod'16 si |
fad'4.\f sold'16 lad' si'8 fad' re' si |
fad'4 fad'8. fad'16 fad'2 |
re'4\p fad'8 fad' re'4 fad' |
sol'4. fad'8 sol'2 |
sol'4 la'8. la'16 sol'4 la' |
sol'4. fad'8 sol'2 |
<dod' mi'>2.-\sug\f <mi' la>4 |
re' re'8. dod'16 re'4 re' |
dod'4 si la fad' |
fad'8 si16 si si8 si si2:8 |
mi'4 mi'16 fad' sol' la' si' la' sol' fad' mi' re' dod' si |
la2 r |
la2.-\sug\ff la'4 |
si2 r |
la2.-\sug\ff la'4 |
re'2 r8 la16 si \grace re' dod'8 si16 la |
re'4 re'8. re'16 re'2 |
