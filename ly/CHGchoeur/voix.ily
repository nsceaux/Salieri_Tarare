<<
  \tag #'(vhaute-contre basse) {
    \clef "alto/G_8" r2^"Chanté" r4 la |
    re'1 fad'4 fad'8 fad' fad'4 mi' |
    re'( dod') re'2 |
    sol'4 mi' dod' la |
    re' la8 la re'4 fad' |
    la'2 sol'4 fad' |
    mi'2 r |
    R1 |
    mi'2 mi'4 mi' |
    mi' mi' mi' mi' |
    mi'2 r |
    r4 si8 dod' re'[ mi'] fad'[ sold'] |
    la'2 fad'4. fad'8 |
    mi'1 |
    fad'2 r |
    r4 mi'8 mi' mi'4 mi' |
    la'2 fad'4. fad'8 |
    mi'1 |
    la2 r |
    la r |
    r mi'4 mi'8 mi' |
    dod'2 r |
    r fad'4 fad'8 fad' |
    re'2 r |
    sol'4 sol'8 fad' mi' re' dod' si |
    fad'1~ |
    fad'~ |
    fad'4 fad r2 |
    re'4^\p re'8 re' re'4 re' |
    re'4. re'8 re'2 |
    mi'4 mi'8 mi' mi'4 mi' |
    mi'4. red'8 mi'2 |
    la'4 la'8 sold' la'4 sol' |
    fad' mi' re'2 |
    la'4 la'8 sold' la'4 lad' |
    si' si si si8 si |
    mi'1 |
    r2 fad'~ |
    fad'8. mi'16 fad'8. sol'16 la'2 |
    si fad'~ |
    fad'8. mi'16 fad'8. sol'16 la'2 |
    re' r |
    re' r |
  }
  \tag #'vtaille {
    \clef "tenor/G_8" r2 r4 la |
    re'1 |
    la4 la8 la la4 lad |
    si( lad) si2 |
    sol'4 mi' dod' la |
    re' la8 la re'4 fad' |
    la2 mi'4 re' |
    la2 r |
    R1 |
    dod'2 dod'4 dod' |
    re' re' re' re' |
    dod'2 r |
    r4 si8 dod' re'[ mi'] fad'[ sold'] |
    la'2 re'4. re'8 |
    dod'2( si) |
    si r |
    r4 si8 si si4 si |
    mi'2 re'4. re'8 |
    dod'2( si) |
    la r |
    la r |
    r mi'4 mi'8 mi' |
    dod'2 r |
    r fad'4 fad'8 fad' |
    re'2 r |
    sol'4 sol'8 fad' mi' re' dod' si |
    fad'1~ |
    fad'~ |
    fad'4 fad r2 |
    fad4^\p la8 la fad4 la |
    si4. la8 si2 |
    si4 do'8 do' si4 do' |
    si4. si8 si2 |
    R1 |
    re'4 re'8 la re'4 re' |
    mi'4. mi'8 mi' mi' mi' mi' |
    re'4 si si si8 si |
    mi'1 |
    r2 re'~ |
    re'8. re'16 re'8. re'16 dod'2 |
    si re'~ |
    re'8. re'16 re'8. re'16 dod'2 |
    re'2 r |
    re' r |
  }
  \tag #'vbasse {
    \clef "bass" r2 r4 la |
    re'1 |
    re'4 re'8 re' re'4 \sug dod'^\markup\center-align\tiny { [source : ré] } |
    si( fad) si2 |
    sol4 mi dod la, |
    re la,8 la, re4 fad |
    la2 dod'4 re' |
    la2 r |
    R1 |
    la2 la4 la |
    si si si si |
    la2 r |
    r4 si8 dod' re'[ mi'] fad'[ sold] |
    la2 la4. la8 |
    la2( sold) |
    fad2 r |
    r4 sold8 sold sold4 sold |
    la2 la4. la8 |
    la2( sold) |
    la2 r |
    la r8 la dod' la |
    mi2 r |
    r r8 si re' si |
    fad2 r |
    R1 |
    sol4 sol8 fad mi re dod si, |
    fad1~ |
    fad~ |
    fad4 fad r2 |
    re4^\p fad8 fad re4 fad |
    sol4. fad8 sol2 |
    sol4 la8 la sol4 la |
    sol4. fad8 sol2 |
    R1 |
    re'4 re'8 dod' re'4 re' |
    dod'4 si la8 la fad fad |
    si4 si si si8 si |
    mi'1 |
    R1 |
    la8. la16 la8. la16 la2 |
    si r |
    la8. la16 la8. la16 la2 |
    re r |
    re r |
  }
>>