\key fa \major
\tempo "Allegro Maëstoso ma con Moto" \midiTempo#120
\time 4/4 s1*43 \bar "||"
\key fa \minor \tempo "Piu Allegro" s1*14 \bar "||"
\key fa \major s1*43 s4 \tempo "Piu Allegro" s2. s1*3 \bar "|."
