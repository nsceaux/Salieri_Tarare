\clef "treble" <>
<<
  \tag #'violino1 {
    <fa' do'' fa''>4\f do''4. re''8-! mi''-! fa''-! |
    mi'' re'' re''2 \sugRythme { si'8. si'16 } re''8 fa'' |
    mi''4 mi''16 fa'' sol'' la'' sib''8 sol'' mi'' sib' |
    la'8
  }
  \tag #'violino2 {
    <la fa' do''>4-\sug\f la'4. sib'8-! sol'-! la'-! |
    do'' sib' sib'2 sib'4 |
    <sol' sib'>4 sib'16 la' sol' fa' mi'8 sol' sib' mi' |
    fa'8
  }
>> <la fa'>16 q q8 q q4 r |
<<
  \tag #'violino1 {
    <fa' do'' fa''>4\p do''4. re''8-! mi''-! fa''-! |
    mi'' re'' re''2 re''8. fa''16 |
    mi''4.( sol''8) do''4.( sib'8) |
    la'4
  }
  \tag #'violino2 {
    la'4-\sug\p la'4. sib'8-! sol'-! la'-! |
    do'' sib' sib'2 sib'4 |
    sib'16( do'' sib' la' sol' la' sol' fa') mi'8-! sol'-! do''-! mi'-! |
    fa'4
  }
>> fa''16\f la'' sol'' fa'' \grace fa''8 mi'' re''16 do'' \grace do''8 sib' la'16 sol' |
fa'8 <fa' do'' fa''>[ q q] q4 <<
  \tag #'violino1 {
    la'8.\p sib'16 |
    do''4~ do''16 sib' la' sib' do''8 fa'' mi'' re'' |
    do''8 si' si'4
  }
  \tag #'violino2 {
    fa'8.\p sol'16 |
    la'4~ la'16 sol' fa' sol' la'8 la'4 la'8 |
    re'8 re' re' re'
  }
>> si'8.\mf la'16 si'8. do''16 |
re''4\fp~ re''16 do'' si' la' sol' la' si' do'' re'' mi'' fa'' sol'' |
mi''4 <<
  \tag #'violino1 {
    <do''' mi'' sol'>\f <mi' do'' sol''> sol''8.\p mi''16 |
    re''2:16\fp re'':16\fp |
    re''2 \grace la''8 sol''16 fad'' sol'' la'' sol''8 re'' |
    mi''8. do''16 mi''8. sol''16 fa''8. mi''16 re''8. do''16 |
  }
  \tag #'violino2 {
    <mi' do'' sol''>4-\sug\f <sol mi' do'' mi''> sol'-\sug\p |
    sol'2:16-\sug\fp la':16-\sug\fp |
    si'2 \grace do''8 si'16 la' si' do'' si'8 si' |
    do''8. sol'16 do''8. mi''16 re''8. do''16 si'8. do''16 |
  }
>>
si'16 sol' la' si' do''\f re'' mi'' fad'' sol'' la'' si'' la'' sol'' la'' si'' la'' |
sol''2 r |
r4 <<
  \tag #'violino1 {
    mi''2\sf( fa''8 re'') |
    r do''-! do''( mi'') r re''-! re''( fa'') |
    mi''4 r r2 |
    R1 | \allowPageTurn
    r4 mi''2\sf fa''8 re'' |
    r do''-! do''( mi'') r re''-! re''( si') |
    do''8 do'[\f do' do'] \grace { re'16 do' si? } do'4 mi'8.\p do'16 |
    sol'4 sol'2 sol'4~ |
    sol' sol'2 sol'4 |
    sol'
  }
  \tag #'violino2 {
    sol'2(-\sug\sf la'8 fa') |
    r8 mi'-! mi'( do') r si-! si( re') |
    do'4 r r8 sol'-! fa'-! mi'-! |
    re'2 r |
    r4 sol'2-\sug\sf la'8 fa' |
    r mi'-! mi'( sol') r fa'-! fa'( re') |
    mi' do'-\sug\f[ do' do'] do'4 r |
    re'8-\sug\p re' mi' mi' fa' fa' mi' mi' |
    re' re' fa' fa' mi' mi' re' re' |
    mi'4
  }
>> r4 do'16 re' mi' fa' sol' la' si' do'' |
<<
  \tag #'violino1 {
    si'8\p \grace do''8 si'16 la' si'8. do''16 re''8 sol' re'' fa'' |
    mi''4
  }
  \tag #'violino2 {
    fa'8-\sug\p fa' fa' mi' re' sol' sol' sol' |
    sol'4
  }
>> <sol' mi'' do'''>8.\f <mi'' do'''>16 q4 r |
<<
  \tag #'violino1 {
    do''4\p do''2 do''4~ |
    do'' do''2 do''4 |
    do''
  }
  \tag #'violino2 {
    sol'8-\sug\p sol' la' la' sib' sib' la' la' |
    sol' sol' sib' sib' la' la' sol' sol' |
    la'4
  }
>> r4 fa'16\fp sol' la' sib' do'' re'' mi'' fa'' |
<<
  \tag #'violino1 {
    mi''8\p \grace fa'' mi''16 re'' mi''8 fa'' sol'' mi'' do'' sib' |
    la'16\f sib' do'' re'' mi'' fa'' sol'' la'' sol''8 do'' do'' do'' |
    do''4
  }
  \tag #'violino2 {
    sib'8-\sug\p sib' sib' la' sol' do' do' do' |
    do'8-\sug\f do' do' do' do''16 re'' mi'' fa'' sol'' la'' sib'' do''' |
    la''4
  }
>> <fa' la>8. q16 q4 q |
q2 <<
  \tag #'violino1 {
    do''4.\p do''8 |
    fa''2~ fa''8( mi'' fa'' re'') |
    do''4. la'8 sol'4. do''8 |
    la'4 r r2 |
    r do''4. do''8 |
    fa''2~ fa''8( mi'' fa'' re'') |
    do''4.( la'8 sol'4. do''8) |
    fa'8 lab'[ lab' lab'] lab'2:8 |
    lab':8 lab':8 |
    sol'8 do'' do'' do'' do''2:8 |
    si':8 si':8 |
    do'':8 do'':8 |
    do'':8\f do'':8 |
    sib'2:8\p sib':8 |
    do'':8 do'':8 |
    reb'':8\fp reb'':8 |
    reb'':16\fp reb'':16\fp |
    reb''4:16\fp do'':16 do'':16 sib':16 |
    lab'2:16\fp lab':16\fp |
    lab'8\f lab'' fa'' reb'' lab' fa' reb' fa' |
    si2:16 si:16 |
    do'2 do''8\p do'' do'' do'' |
    do''4( fa'' mi'' re'') |
    do''4. do''8\f fa''4. \grace sol''8 fa''16 mi''32 fa'' |
    sol''4 do'4. do''8 do'' do'' |
    do''4( fa'' mi'' re'') |
    do''4. do''8\f fa''4. \grace sol''8 fa''16 mi''32 fa'' |
    sol''4:16\fp mi'':16 do'':16 sib':16 |
  }
  \tag #'violino2 {
    r2 |
    r4_"Pizzicato" <fa' do''> r <fa' re''> |
    r <fa' la'> r <mi' do''> |
    r <fa' la'> r2 |
    R1 |
    r4 <fa' do''> r <fa' re''> |
    r <fa' la'> r <mi' do''> |
    r8 fa'_"con l’arco" fa' fa' fa'2:8 |
    fa':8 fa':8 |
    sol':8 sol':8 |
    fa':8 fa':8 |
    mi'8 do' do' do' do'2:8 |
    do'8-\sug\f mib'! mib' mib' mib'2:8 |
    mib':8-\sug\p mib':8 |
    mib':8 mib':8 |
    fa'16-\sug\fp lab' lab' lab' lab'4:16 lab'2:16 |
    lab':16-\sug\fp lab':16-\sug\fp |
    solb':16-\sug\fp solb':16 |
    solb':16-\sug\fp solb':16-\sug\fp |
    <fa' lab>:16-\sug\f q:16 |
    q:16 q:16 |
    <sol mi'>2 r |
    fa'1-\sug\p fa'16-\sug\f do' do' do' do'4:16 do'2:16 |
    do'2:16 do':16 |
    fa'1 |
    fa'16\f do' do' do' do'4:16 do'2:16 |
    do':16-\sug\fp sol':16 |
  }
>>
la'8\f si'16 dod'' re'' mi'' fad'' sold'' la''8 <mi'' la'> q q |
<<
  \tag #'violino1 {
    <la' fa''>4:16-\sug\ff <la' mi''>:16 <la' fa''>:16 <la' mi''>:16 |
    <la' fa''>4 r r8 do'''\p do''' do''' |
    re'''4-! do'''-! re'''-! do'''-! |
    re'''4 r sib8\fp mi''[ mi'' mi''] |
    fa''4:16-\sug\cresc mi'':16 fa'':16 sol'':16 |
  }
  \tag #'violino2 {
    re''4:16-\sug\ff dod'':16 re'':16 dod'':16 |
    re''4 r r8 fa''-\sug\p fa'' fa'' |
    fa''4-! fa''-! fa''-! fa''-! |
    fa''4 r sib8-\sug\fp sol'[ sol' sol'] |
    fa'16-\sug\cresc do'' do'' do'' do''4:16 do''2:16 |
  }
>>
la''16\fp la'' sol'' sol'' fa'' fa'' mi'' mi'' re''8:16 do'':16 sib':16 la':16 |
sol':16 la':16 sib':16 la':16 sol':16 fa':16 mi':16 re':16 |
do'2 <sol' mi''>4\f q8. q16 |
<<
  \tag #'violino1 {
    fa''4.\fp fa''8 re'''4.\fp re'''8 |
    do'''\f do'' do'' do'' do'' la' sol' mi' |
    fa'2 do''8\p do'' do'' do'' |
    do''4( fa'' mi'' re'') |
    do''4. do''8\f fa''4. \grace sol''8 fa''16 mi''32 fa'' |
    sol''4\fp do'4. do''8[ do'' do''] |
    do''4( fa'' mi'' re'') |
    do''4. do''8\f fa''4. \grace sol''8 fa''16 mi''32 fa'' |
    sol''4:16-\sug\fp mi'':16 do'':16 sib':16 |
  }
  \tag #'violino2 {
    fa''2:16-\sug\fp fa'':16-\sug\fp |
    fa''8:16-\sug\f la'':16 fa'':16 do'':16 do'':16 fa'':16 mi'':16 sol'':16 |
    fa''2 r |
    fa'1-\sug\p |
    fa'16 do'\f do' do' do'4:16 do'2:16 |
    do':16-\sug\fp do':16 |
    fa'1 |
    fa'16 do'\f do' do' do'4:16 do'2:16 |
    do':16-\sug\fp sol':16 |
  }
>>
la'8\f si'16 dod'' re'' mi'' fad'' sold'' la''8 <mi'' la'> q q |
<<
  \tag #'violino1 {
    <la' fa''>4:16\ff <la' mi''>:16 <la' fa''>4:16 <la' mi''>:16  |
    <la' fa''>4 r r8 do'''\p do''' do''' |
    re'''4 do''' re''' do''' |
    re''' r sib8\fp mi''[ mi'' mi''] |
    fa''4:16 mi'':16\cresc fa'':16 sol'':16 |    
  }
  \tag #'violino2 {
    re''4:16-\sug\ff dod'':16 re'':16 dod'':16 |
    re''4 r r8 fa''\p fa'' fa'' |
    fa''4 fa'' fa'' fa'' |
    fa'' r sib8-\sug\fp sol'[ sol' sol'] |
    fa'16 do''[ do'' do''] do''4:16 do''2:16 |
  }
>>
la''16\fp la'' sol'' sol'' fa'' fa'' mi'' mi'' re''8:16 do'':16 sib':16 la':16 |
sol':16 la':16 sib':16 la':16 sol':16 fa':16 mi':16 re':16 |
do'2 <sol' mi''>4 q8. q16 |
<<
  \tag #'violino1 {
    <la' fa''>4.\fp fa''8 re'''4.\fp re'''8 |
    do'''8\fp do'' do'' do'' do'' la' sol' mi' |
    fa'\f <la' fa''> q q <do'' sol''>2:8 |
    la'':16 la'':16 |
    re''':16 re'''4 <mi'' sib' sol'> |
  }
  \tag #'violino2 {
    fa''2:16-\sug\fp fa'':16-\sug\fp |
    fa''8:16-\sug\fp la'':16 fa'':16 do'':16 do'':16 fa'':16 mi'':16 sol'':16 |
    fa''\f do'' do'' do'' do''2:8 |
    do'':16 fa'':16 |
    fa'':16 fa''4 <mi'' sib' sol'> |
  }
>>
fa''4 <la' la''>~ la''16 sol'' fa'' mi'' re'' do'' sib' la' |
sib'4 <sib' sib''>4~ sib''16 la'' sol'' fa'' mi'' re'' do'' sib' |
do''4 <do'' do'''>4~ do'''16 sib'' la'' sol'' fad'' sol'' la'' fad'' |
sol''4 <<
  \tag #'violino1 { re'''4 do''' }
  \tag #'violino2 { <sib'' sib'>4 <la' la''> }
>> <mi'' sib' sol'> |
<fa' la' fa''>4 r8 <<
  \tag #'violino1 {
    fa'8 fa'4 \grace fa'8 mib' re'16 mib' |
    re'4 r r2 |
    re'4\p
  }
  \tag #'violino2 {
    la8 la4 la |
    la r r2 |
    sib4-\sug\p
  }
>> r4 r2 |
R1 |

