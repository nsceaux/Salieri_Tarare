<<
  \tag #'(spinette recit) {
    \clef "soprano/treble" <>^"Chanté"
    R1*3 |
    r2 r4 r8 do'' |
    fa''4 do''8. do''16 do''8[ re''] mi'' fa'' |
    mi''[ re''] re''2 re''8 fa'' |
    mi''4.( sol''8) do''4. sib'8 |
    la'4 la' r2 |
    r r4 la'8. sib'16 |
    do''4. do''8 fa''4 mi''8 re'' |
    do''8[ si'] si'4 si'8. la'16 si'8. do''16 |
    re''2 re''4. sol''8 |
    mi''2. sol''8. mi''16 |
    re''4 re''8. re''16 re''4. do''8 |
    si'2 r4 re''8. re''16 |
    mi''4. sol''8 fa''8.[ mi''16] re''8. do''16 |
    si'4 sol' r2 |
    r re''4. re''8 |
    mi''2. fa''8[ re''] |
    do''4.( mi''8) re''4. fa''8 |
    mi''4 r r2 |
    r2 re''4. re''8 |
    mi''2. fa''8[ re''] |
    do''4.( mi''8) re''4. si'8 |
    do''2 <<
      \tag #'recit { s2 s1*2 s2 \ffclef "soprano/treble" <>^\markup\character Spinette }
      \tag #'spinette { r2 | R1*2 | r2 }
    >> r4 sol'8 do'' |
    si'4. do''8 re''4 re''8 fa'' |
    mi''4 do'' <<
      \tag #'recit { s2 s1*2 s2 \ffclef "soprano/treble" <>^\markup\character Spinette }
      \tag #'spinette { r2 | R1*2 | r2 }
    >> r4 do''8 fa'' |
    mi''4 fa''8 la'' sol''4. sib'8 |
    do''4 fa''8 la'' sol''4. sib'8 |
    la'2 r |
    r do''4. do''8 |
    fa''2~ fa''8[ mi''] fa''[ re''] |
    do''4.( la'8) sol'4 do'' |
    la'2
    <<
      \tag #'recit { s2 s \ffclef "soprano/treble" <>^\markup\character Spinette }
      \tag #'spinette { r2 r }
    >> do''4. do''8 |
    fa''2~ fa''8[ mi''] fa''[ re''] |
    do''4.( la'8) sol'4 do'' |
    fa'2 r |
    <<
      \tag #'recit {
        s1*13 s2
        \ffclef "soprano/treble" <>^\markup\character Spinette
      }
      \tag #'spinette { R1*13 r2 }
    >> do''8 do'' do'' do'' |
    do''4.( fa''8) mi''4 re'' |
    <<
      \tag #'recit {
        do''4. s8 s2 | s2 s8
        \ffclef "soprano/treble" <>^\markup\character Spinette
      }
      \tag #'spinette {
        do''2 r2 |
        r2 r8
      }
    >> do'' do'' do'' |
    do''4 fa'' mi'' re'' |
    <<
      \tag #'recit {
        do''4. s8 s2 s1*3 s2 
        \ffclef "soprano/treble" <>^\markup\character Spinette 
      }
      \tag #'spinette { do''2 r | R1*3 | r2 }
    >> r8 do'' do'' do'' |
    re''4 do'' re'' do'' |
    re''2 <<
      \tag #'recit { s2 s1*3 s2 \ffclef "soprano/treble" <>^\markup\character Spinette }
      \tag #'spinette { r2 R1*3 r2 }
    >> sol'4 sol'8 sol' |
    la'2 sib' |
    la'2. sol'4 |
    fa'2 do''8 do'' do'' do'' |
    do''4( fa'') mi'' re'' |
    <<
      \tag #'recit {
        do''4. s8 s2 | s2 s8
        \ffclef "soprano/treble" <>^\markup\character Spinette
      }
      \tag #'spinette {
        do''2 r2 |
        r2 r8
      }
    >> do''8 do'' do'' |
    do''4 fa'' mi'' re'' |
    <<
      \tag #'recit {
        do''4. s8 s2 s1*3 s2
        \ffclef "soprano/treble" <>^\markup\character Spinette
      }
      \tag #'spinette { do''2 r2 R1*3 r2 }
    >> r8 do'' do'' do'' |
    re''4 do'' re'' do'' |
    re''2 <<
      \tag #'recit { s2 s1*3 s2 \ffclef "soprano/treble" <>^\markup\character Spinette }
      \tag #'spinette { r2 R1*3 r2 }
    >> sol'4 sol'8 sol' |
    la'2 sib' |
    la'2. sol'4 |
    la'2 do''4 do''8 do'' |
    fa''2 la' |
    sib'2. sol'4 |
    fa'4 r4 r2 |
    R1*4 |
  }
  %%%
  \tag #'(tarare recit) {
    <<
      \tag #'recit { s1*24 s2 r4 \ffclef "tenor/G_8" <>^\markup\character Tarare }
      \tag #'tarare { \clef "tenor/G_8" R1*24 r2 r4 }
    >> mi8. do16 |
    sol2. sol4 |
    sol2 sol4. sol8 |
    sol2 <<
      \tag #'recit { s2 s1 s2 r4 \ffclef "tenor/G_8" <>^\markup\character Tarare }
      \tag #'tarare { r2 R1 r2 r4 }
    >> sol4 |
    do'4. do'8 do'4. do'8 |
    do'2. do'4 |
    la4 la <<
      \tag #'recit { s2 s1*6 s2 \ffclef "tenor/G_8" <>^\markup\character Tarare }
      \tag #'tarare { r2 R1*6 r2 }
    >> la4. sib8 |
    do'2 <<
      \tag #'recit { s2 s1*3 \ffclef "tenor/G_8" <>^\markup\character Tarare }
      \tag #'tarare { r2 R1*3 }
    >>
    r2 lab4. fa8 |
    do'2 do'4. do'8 |
    fa'2. si4 |
    do'4 do' r2 |
    r do'8 sib do' reb'! |
    mib'2 mib4. reb'8 |
    do'2 r4 r8 lab |
    reb'1 |
    reb'4 reb'8 reb' fa'4. reb'8 |
    reb'4 do' r8 sib sib sib |
    lab4. do'8 mib'4. solb'8 |
    fa'4 fa' r8 fa' fa' fa' |
    si4. si8 si4. si8 |
    do'2 <<
      \tag #'recit { s2 s1 s4. \ffclef "tenor/G_8" <>^\markup\character Tarare }
      \tag #'tarare { r2 R1 r4 r8 }
    >> do'8 fa'2 |
    sol'4 do'8 do'  <<
      \tag #'recit { do'8 s4. s1 s4. \ffclef "tenor/G_8" <>^\markup\character Tarare }
      \tag #'tarare {
        do'4. sib8 |
        la4 la r2 |
        r4 r8
      }
    >> do'8 fa'4 fa' |
    sol'4. mi'8 do'4 sib |
    la la r8 mi' mi' mi' |
    fa'4 mi' fa' mi' |
    fa'2 <<
      \tag #'recit { s2 s1 s2 \ffclef "tenor/G_8" <>^\markup\character Tarare }
      \tag #'tarare { r2 R1 r2 }
    >> r8 mi' mi' mi' |
    fa'4 mi' fa' sol' |
    la'8[ sol'] fa' mi' re'[ do'] sib[ la] |
    sol[ la] sib[ la] sol[ fa] mi[ re] |
    do2 <<
      \tag #'recit { s2 s1*4 s4. \ffclef "tenor/G_8" <>^\markup\character Tarare }
      \tag #'tarare {
        mi'4 mi'8 mi' |
        fa'2 re' |
        do'2. mi4 |
        fa2 r |
        R1 |
        r4 r8
      }
    >> do'8 fa'2 |
    sol'4 do'8 do' <<
      \tag #'recit { do'8 s4. | s1 s4. \ffclef "tenor/G_8" <>^\markup\character Tarare }
      \tag #'tarare {
        do'4. sib8 |
        la4 la r2 |
        r4 r8
      }
    >> do'8 fa'4 fa' |
    sol'4. mi'8 do'4 sib |
    la4 la mi' mi'8 mi' |
    fa'4 mi' fa' mi' |
    fa'2 <<
      \tag #'recit { s2 s1 s2 \ffclef "tenor/G_8" <>^\markup\character Tarare }
      \tag #'tarare { r2 R1 r2 }
    >> r8 mi' mi' mi' |
    fa'4 mi' fa' sol' |
    la'8[ sol'] fa' mi' re'[ do'] sib[ la] |
    sol[ la] sib[ la] sol[ fa] mi[ re] |
    do2 <<
      \tag #'recit { s2 s1*10 }
      \tag #'tarare {
        mi'4 mi'8 mi' |
        fa'2 re' |
        do'2. mi'4 |
        fa'2 sol'4 sol'8 sol' |
        la'2 fa' |
        re'2. mi4 |
        fa4 r r2 |
        R1*4 |
      }
    >>
  }
  \tag #'basse { \clef "soprano/treble" R1*101 }
>>
<<
  \tag #'(spinette basse recit) {
    <>^"Récit" r4 re'' r8 re'' re'' do''16 re'' |
    sib'4
    \tag #'spinette { r4^\markup\italic { (elle sort.) } r2 R1 }
  }
  \tag #'(tarare basse recit) {
    <<
      \tag #'(basse recit) { s1 s4 \ffclef "tenor/G_8" <>^\markup\character Tarare }
      \tag #'tarare { R1 r4 }
    >> r8 sol16 la sib4 do'8 re' |
    sol sol r4 r2 |
  }
>>
