\clef "treble"
\twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''-\sug\f do''4. re''8-! mi''-! fa''-! |
    mi'' re'' re''2 re''8. fa''16 |
    mi''1 |
    fa''8 do''16 do'' do''8 do'' do''4 }
  { la'4-\sug\f la'4. sib'8-! sol'-! la'-! |
    do'' sib' sib'2 sib'4 |
    sib'1 |
    la'8 la'16 la' la'8 la' la'4 }
>> r4 |
R1*3 |
r4 fa''16-\sug\f la'' sol'' fa'' \grace fa''8 mi'' re''16 do'' \grace do''8 sib' la'16 sol' |
fa'8 \twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''8 fa'' fa'' fa''4 }
  { la'8 la' la' la'4 }
>> r4 |
R1*4 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { re''2 re'' |
    re'' sol''4. re''8 |
    mi''4. sol''8 fa''8. mi''16 re''8. do''16 |
    si'4 sol''8.-\sug\f sol''16 sol''4 sol'' |
    sol''2 }
  { sol'2 la' |
    si' si'4. si'8 |
    do''4. mi''8 re''8. do''16 si'8. do''16 |
    si'4 si'8.-\sug\f si'16 si'4 si' |
    si'2 }
  { s2\fp s\fp }
>> r2 |
<<
  \tag #'(oboe1 oboi) {
    R1*2 |
    r4 <>^"solo" do''16 re'' mi'' fa'' sol''8 sol'' sol'' sol'' |
    \grace { la''16 sol'' fad'' } sol''2 r |
    R1*12 |    
  }
  \tag #'oboe2 { R1*16 }
>>
r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { la''4-\sug\f sol''2 |
    la''4 fa'8. fa'16 fa'4 fa' |
    fa'2 }
  { do''4-\sug\f do''2 |
    do''4 fa'8. fa'16 fa'4 fa' |
    fa'2 }
>> r2 |
R1*2 |
r2 <>-\sug\p \twoVoices#'(oboe1 oboe2 oboi) <<
  { la'4. sib'8 | do''2 }
  { fa'4. sol'8 | la'2 }
>> r2 |
R1*2 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { lab'1~ | lab' | do'' | si' |
    do''~ | do'' | sib'-\sug\p | do'' |
    reb''!-\sug\fp | reb''-\sug\fp | reb''4-\sug\fp do''2 sib'4 | lab'1-\sug\fp |
    lab'-\sug\f | si' | do''2 }
  { fa'1~ | fa' | sol' | fa' |
    mi'4 r r2 | r2 mib'!~ | mib'1-\sug\p | mib' |
    fa'-\sug\fp | fa'-\sug\fp | solb'2-\sug\fp solb' | solb'1-\sug\fp |
    fa'1-\sug\f | fa' | mi'2 }
>> r2 |
R1 |
r2 \twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''2 | sol''1\fp | la''4 }
  { la'2 | do''1-\sug\fp | do''4 }
>> r4 r2 |
r2 \twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''2-\sug\f |
    sol''1\fp |
    la''2\f~ la''8 mi'' mi'' mi'' |
    fa''4-\sug\ff mi'' fa'' mi'' |
    fa'' }
  { la'2-\sug\f |
    do''2.-\sug\fp sib'4 |
    la'2-\sug\f~ la'8 la' la' la' |
    re''4-\sug\ff dod'' re'' dod'' |
    re'' }
>> r4 r2 |
R1*2 |
r4 do''2 do''4 |
do''4 r r2 |
R1 |
r2 \twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''4-\sug\f mi''8. mi''16 |
    fa''2-\sug\fp re'''-\sug\fp |
    la''2.-\sug\f sol''4 |
    fa''2 }
  { sol'4-\sug\f sol'8. sol'16 |
    la'2-\sug\fp sib'8-\sug\fp re'' fa'' sib'' |
    fa''2.-\sug\f mi''4 |
    fa''2 }
>> r2 |
R1 |
r2 <>\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''2 | sol''1-\sug\fp | do''4 }
  { la'2 | do''1-\sug\fp | la'4 }
>> r4 r2 |
r2 \twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''2-\sug\f |
    sol''1-\sug\fp |
    la''2-\sug\f~ la''8 mi''[ mi'' mi''] |
    fa''4 mi'' fa'' mi'' |
    fa'' }
  { la'2-\sug\f |
    do''2.-\sug\fp sib'4 |
    la'2-\sug\f~ la'8 la'[ la' la'] |
    re''4 dod'' re'' dod'' |
    re'' }
>> r4 r2 |
R1 |
r2 r8 do''-\sug\p do'' do'' |
<< do''1 { s4 s2.-\sug\cresc } >> |
do''4-\sug\fp r r2 |
R1 |
r2 \twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''2 |
    fa''-\sug\fp re'''-\sug\fp |
    la''2.-\sug\fp sol''4 |
    fa''2\f sol'' |
    la''1 |
    re'''2. sib'4 |
    fa'' la''2 la''4 |
    sib''1 |
    do'''2. fad''4 |
    sol'' sib'' la'' mi'' |
    fa''4 }
  { sol'2 |
    la'-\sug\fp sib'-\sug\fp |
    r4 fa''2 mi''4 |
    fa''2\f do'' |
    do'' fa'' |
    fa''2. mi''4 |
    la' fa''2 fa''4 |
    mi''1 |
    mib'' |
    re''4 re'' do'' sib' |
    la' }
>> r4 r2 |
R1*3 |
