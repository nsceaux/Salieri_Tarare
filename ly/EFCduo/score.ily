\score {
  <<
    \new GrandStaff \with { \oboiInstr } <<
      \new Staff \with { \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'oboe1 \includeNotes "oboi"
      >>
      \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff <<
      \new Staff \with {
        instrumentName = \markup\character Spinette
        shortInstrumentName = \markup\character Spi.
      } \withLyrics <<
        \global \keepWithTag #'spinette \includeNotes "voix"
      >> \keepWithTag #'spinette \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\character Tarare
        shortInstrumentName = \markup\character Ta.
      } \withLyrics <<
        \global \keepWithTag #'tarare \includeNotes "voix"
      >> \keepWithTag #'tarare \includeLyrics "paroles"
    >>
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*5\break s1*5\pageBreak
        s1*5\break s1*6\pageBreak
        \grace s8. s1*7\break s1*6\pageBreak
        s1*7\break s1*8\pageBreak
        s1*6\break s1*5\pageBreak
        s1*5\break s1*5\pageBreak
        s1*5\break s1*5\pageBreak
        s1*6\break s1*6\pageBreak
        s1*7\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
