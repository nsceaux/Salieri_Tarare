\clef "alto" fa'4-\sug\f fa'2 fa'4 |
r8 fa' fa' fa' fa' fa' re' sib |
do'2:8 do':8 |
do'8 fa' do' la fa4 r |
fa2:8-\sug\p fa:8 |
sib:8 sib:8 |
do':8 do:8 |
fa4 do'8-\sug\f do'' sib' sol' mi' mi' |
fa' fa fa fa fa4 r |
fa2:8-\sug\p fa:8 |
sol sol8.-\sug\mf fad16 sol8. la16 |
si8-\sug\fp sol si re' sol' re' re' re' |
do'2:8 do'':8\fp |
si':8\fp fad':8\fp |
sol':8\fp sol':8\fp |
sol':8 sol':8 |
sol'8 sol sol-\sug\f sol <sol re' si'>4 q |
q2 r |
do'2-\sug\p( fa) |
sol4 r sol r |
do' r r8 mi'-! re'-! do'-! |
si2 r |
do' fa |
sol4 r sol r |
do' r r2 |
si8-\sug\p si do' do' re' re' do' do' |
si si re' re' do' do' si si |
do'2:8 mi':8-\sug\fp |
re'8-\sug\p re' re' do' si2:8 |
do'4 do'-\sug\f do r |
mi'8-\sug\p mi' fa' fa' sol' sol' fa' fa' |
mi' mi' sol' sol' fa' fa' mi' mi' |
fa'2:8 la':8-\sug\fp |
sol'8-\sug\p sol' sol' fa' mi' mi' mi' mi' |
fa'2:8-\sug\f sol':8 |
do'4 do'8. do'16 do'4 do' |
do'2 r |
la4_"Pizzicato" r sib r |
do' r do r |
fa r r2 |
R1 |
la4 r sib r |
do' r do r |
r8 do'_"con l’arco" do' do' do'2:8 |
do':8 do':8 |
do':8 do':8 |
fa':8 fa':8 |
sol'4 r r2 |
lab1-\sug\f |
sol-\sug\p lab |
lab16-\sug\fp fa' fa' fa' fa'4:16 fa'2:16 |
fa':16-\sug\fp fa':16-\sug\fp |
mib':16-\sug\fp mib':16 |
mib':16-\sug\fp mib':16-\sug\fp |
reb':16-\sug\f reb':16 |
reb'16 reb reb reb reb4:16 reb2:16 |
do r |
la8\p fa re' fa do' fa sib fa |
la2:16\f la:16 |
sol:16 sol:16 |
fa8 fa re' fa do' fa sib fa |
la16-\sug\f la la la la4:16 la2:16 |
sol:16-\sug\fp sol:16 |
la8-\sug\f <la mi'> q q q dod' dod' dod' |
re'4:16-\sug\ff la:16 re':16 la:16 |
re' r r8 la'-\sug\p la' la' |
sib'4-! la'-! sib'-! la'-! |
sib' r sib8-\sug\fp do''[ do'' do''] |
do''-\sug\cresc do' do' do' do'2:8 |
fa'2-\sug\f r |
sib-\sug\p r |
do' do'4-\sug\f do'8. do'16 |
do'16-\sug\fp do'' do'' do'' do''4:16 re''16-\sug\fp sib' sib' sib' sib'4:16 |
la'8-\sug\f fa' do' la la do' do do' |
fa2 r |
la8\p fa re' fa do' fa sib fa |
la2:16\f la:16 |
sol:16-\sug\fp sol:16 |
la8 fa re' fa do' fa sib fa |
la2:16-\sug\f la:16 |
sol:16-\sug\fp sol:16 |
la8-\sug\f <la mi'> q q q dod'[ dod' dod'] |
re'4:16-\sug\ff la:16 re':16 la:16 |
re'4 r r8 la'-\sug\p la' la' |
sib'4 la' sib' la' |
sib' r sib8-\sug\fp do''[ do'' do''] |
do'' do' do'\cresc do' do'2:8 |
fa'-\sug\fp r |
sib r |
do' do'4 do'8. do'16 |
do'2:16-\sug\fp sib8-\sug\fp sib' sib' sib' |
la'8-\sug\fp fa' do' la la do' do do |
fa2:8\f mi:8 |
fa:8 re':8 |
sib:8 sib4 do' |
fa'8 <fa' la>4 q q q8 |
<sib mi'>8 q4 q q q8 |
<do' mib'>8 q4 q q q8 |
re'4 sol do' do |
fa r8 fa' fa'4 \grace fa'8 mib' re'16 mib' |
re'4 r r2 |
sol'4\p r r2 |
R1 |
