\markup\bold TACET
\score {
  \new ChoirStaff <<
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
    } \withLyrics <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit { \set fontSize = #-2 \includeLyrics "paroles" }
  >>
  \layout {
    system-count = #(or (*system-count*) #f)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
    ragged-last = ##f
    \context {
      \Score
      \remove "Metronome_mark_engraver"
    }
  }
}
