\tag #'(spinette recit) {
  A -- mi, ton cou -- ra -- ge m’é -- clai -- re,
  ton cou -- ra -- ge m’é -- clai -- re.
  Si Ta -- rare ai -- mait à me plai -- re,
  il eût tout bra -- vé com -- me toi.
  J’ou -- blie -- rai qu’il ob -- tint ma foi :
  c’en est fait, mon cœur te pré -- fè -- re ;
  tu se -- ras Ta -- ra -- re pour moi.
  tu se -- ras Ta -- ra -- re pour moi.
}
\tag #'(tarare recit) {
  Quoi ! Ta -- rare ob -- tint vo -- tre foi !
}
\tag #'(spinette recit) {
  C’en est fait, mon cœur te pré -- fè -- re.
}
\tag #'(tarare recit) {
  C’est moi que vo -- tre cœur pré -- fè -- re ?
}
\tag #'(spinette recit) {
  J’ou -- blie -- rai qu’il ob -- tint ma foi, qu’il ob -- tint ma foi :
  tu se -- ras Ta -- ra -- re pour moi.
}
\tag #'(tarare recit) {
  Je se -- rai…
}
\tag #'(spinette recit) {
  Tu se -- ras Ta -- ra -- re pour moi.
}
\tag #'(tarare recit) {
  Est-ce un songe ! ô Bra -- ma, veil -- lé- je ?
  Tout ce que j’en -- tends me con -- fond.
  A -- tar, toi que la haine as -- siè -- ge,
  m’as- tu con -- duit de piège en piè -- ge
  dans un a -- bîme aus -- si pro -- fond !
}
\tag #'(spinette recit) {
  Ce n’est point un piè -- ge ; non, non :
}
\tag #'(tarare recit) {
  A -- tar, toi que la haine \tag #'tarare { as -- siè -- ge, }
}
\tag #'(spinette recit) {
  de son par -- don je te ré -- ponds.
}
\tag #'(tarare recit) {
  m’as- tu con -- duit de piège en piè -- ge
  dans un a -- bîme aus -- si pro -- fond !
}
\tag #'(spinette recit) {
  de son par -- don je te ré -- ponds.
}
\tag #'(tarare recit) {
  m’as- tu con -- duit de piège en piè -- ge
  dans un a -- bîme aus -- si pro -- fond,
  \tag #'tarare {
    dans un a -- bîme aus -- si pro -- fond !
  }
}
\tag #'(spinette recit) {
  de son par -- don je te ré -- ponds.
  Ce n’est point un piè -- ge ; non, non :
}
\tag #'(tarare recit) {
  A -- tar, toi que la haine \tag #'tarare { as -- siè -- ge, }
}
\tag #'(spinette recit) {
  de son par -- don je te ré -- ponds.
}
\tag #'(tarare recit) {
  m’as- tu con -- duit de piège en piè -- ge
  dans un a -- bîme aus -- si pro -- fond !
}
\tag #'(spinette recit) {
  de son par -- don je te ré -- ponds.
}
\tag #'(tarare recit) {
  m’as- tu con -- duit de piège en piè -- ge
  dans un a -- bîme aus -- si pro -- fond,
  \tag #'tarare {
    dans un a -- bîme aus -- si pro -- fond,
    dans un a -- bîme aus -- si pro -- fond !
  }
}
\tag #'(spinette recit) {
  de son par -- don je te ré -- ponds,
  de son par -- don je te ré -- ponds.
}
%%
\tag #'(spinette recit basse) {
  Ciel ! on vient l’ar -- rê -- ter !
}
\tag #'(tarare recit basse) {
  Tout es -- poir m’a -- ban -- don -- ne.
}
