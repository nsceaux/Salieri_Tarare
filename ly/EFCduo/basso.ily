\clef "bass" fa8\f fa fa fa fa2:8 |
sib,:8 sib,:8 |
do:8 do:8 |
fa8 la do' la fa4 r |
fa2:8\p fa:8 |
sib,:8 sib,:8 |
do:8 do:8 |
fa4 la\f sol do |
fa8 fa fa fa fa4 r |
fa2:8\p fa:8 |
sol2 sol8.\mf fa16 sol8. la16 |
si8\fp si, si, si, si,2:8 |
do:8 do':8\fp |
si:8\fp fad:8\fp |
sol:8\fp sol:8\fp |
sol:8 sol:8 |
sol8 sol, sol,\f sol, sol, sol, sol, sol, |
sol,2 r |
do'2(\p fa) |
sol4 r sol, r |
do r r do |
sol4 r r2 |
do2( fa) |
sol4 r sol, r |
do r r2 |
sol,4-\sug\p r r2 |
sol,4 r r2 |
do2:8 do:8\fp |
re8\p re re do si,2:8 |
do4 do8.\f do16 do4 r |
do\p r r2 |
do4 r r2 |
fa2:8 fa:8\fp |
sol8\p sol sol fa mi mi mi mi |
fa2:8\f mi:8 |
fa4 fa8. fa16 fa4 fa |
fa2 r |
la,4_"pizzicato" r sib, r |
do r do r |
fa r r2 |
R1 |
la,4 r sib, r |
do r do r |
fa4 r r2 |
r2 <>_"con l’arco" fa2 |
mib1( |
reb |
do) |
lab,\f |
sol,\p |
lab, |
reb8\fp reb reb reb reb2:8 |
reb:8\fp reb:8\fp |
mib:8\fp reb:8 |
do:8\fp do:8\fp |
reb:8\f reb:8 |
reb:8 reb:8 |
do2 r |
la8\p fa re' fa do' fa sib fa |
la\f fa fa fa fa2:8 |
mi:8 mi:8 |
fa8 fa re' fa do' fa sib fa |
la fa\f fa fa fa2:8 |
mi:8\fp mi:8 |
dod:8\f dod:8 |
re8\ff re la, la, re re la, la, |
re4 r r2 |
R1 |
r2 sib,8\fp sib[ sib sib] |
la\cresc la sib sib la la mi mi |
fa2\f r |
sib,\p r |
do do8\f do do do |
fa2:8\fp sib,:8\fp |
do:8\f do:8 |
fa2 r |
la8\p fa re' fa do' fa sib fa |
la\f fa fa fa fa2:8 |
mi:8\fp mi:8 |
fa8 fa re' fa do' fa sib fa |
la-\sug\f fa fa fa fa2:8 |
mi:8\fp mi:8 |
dod:8\f dod:8 |
re8\ff re la, la, re re la, la, |
re4 r r2 |
R1 |
r2 sib,8-\sug\fp sib[ sib sib] |
la4 sib\cresc la mi |
fa2\fp r2 |
sib, r |
do do8\fp do do do |
fa2:8\fp sib,:8\fp |
do:8\fp do:8 |
fa:8\f mi:8 |
fa:8 re:8 |
sib,:8 sib,4 do |
fa2:8 fa:8 |
sol:8 sol:8 |
la:8 la:8 |
sib4 sol do' do |
fa4 r8 fa fa4 fa |
fad4 r r2 |
sol4\p r4 r2 |
R1 |
