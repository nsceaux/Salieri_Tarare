Sur un choix im -- por -- tant le ciel est con -- sul -- té.
Vous, pré -- pa -- rez l’au -- tel ; vous, les fleurs les plus pu -- res ;
vous, choi -- sis -- sez par -- mi les en -- fants des au -- gu -- res,
ce -- lui pour qui Bra -- ma s’est plus ma -- ni -- fes -- té,
en le dou -- ant d’un cœur plein de sim -- pli -- ci -- té.

C’est le jeune E -- la -- mir. Il vient à vous.

Mon pè -- re !

Ap -- pro -- chez- vous, mon fils ; un grand jour vous é -- clai -- re.
Croy -- ez- vous que Bra -- ma vous par -- le par ma voix,
et qu’il parle à moi seul ?

Mon pè -- re, oui, je le crois.

Le Ciel choi -- sit par vous un ven -- geur à l’em -- pi -- re :
ne di -- tes rien, mon fils, que ce qu’il vous ins -- pi -- re.

Ah ! s’il vous ins -- pi -- rait de nom -- mer Al -- ta -- mort !
L’é -- tat se -- rait vain -- queur, il vous de -- vrait son sort !

Je l’en sup -- plie -- rai tant, mon pè -- re,
qu’il me l’ins -- pi -- re -- ra, j’es -- pè -- re.

Moi je l’es -- père aus -- si : priez- le a -- vec trans -- port.
