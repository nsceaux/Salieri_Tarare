\key do \major
\tempo "Andante Sostenuto" \midiTempo#60
\time 4/4 s1*6 \midiTempo#100 s1*18 s4
\tempo "plus lent" s2. s1*8
\tempo "Allegro" s1*2 s2
\tempo "Primo tempo" s2 s1*10 \bar "|."
