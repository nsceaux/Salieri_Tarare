\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with {
        \consists "Metronome_mark_engraver"
        \oboiInstr
      } << \global \keepWithTag #'oboi \includeNotes "oboi" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \smaller en Mi♭ }
        shortInstrumentName = "Cor."
        \haraKiri
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { 
          \consists "Metronome_mark_engraver"
        } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { Bassons Basses }
      shortInstrumentName = \markup\center-column { Bn. B. }
      \consists "Metronome_mark_engraver"
    } <<
      \global \keepWithTag #'basso \includeNotes "basso"
      \origLayout {
        s1*4\pageBreak
        s1*5\break s1*4\break s1*4\pageBreak
        s1*4\break s1*4\break s1*4\pageBreak
        s1*5\break s1*4\break s1*4\pageBreak
      }
      \modVersion { s1*6 s2 \bar "" \break \set Staff.shortInstrumentName = "B." }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" } 
  }
  \midi { }
}
