\clef "bass" R1*6 |
r2 <>^\markup\character-text Arthénée ton dogmatique ^"parlé"
r4 fa8 fa |
fa4 fa8 fa sib4. sib8 |
re'4. re'8 do'4 re' |
sib4 r r2 |
<>^\markup\italic { à un prêtre } sol8 sol16 sol sol8 la si! r re'^\markup\italic { à un autre } r16 re' |
si4 si8 do' do' sol r4 |
r^\markup\italic { à un troisième } sol8 sol16 sol do'8 do' do' sol16 la |
sib4 sib8 do' la la r fa^\markup\italic ton dévot |
fa fa fa sol la4 r8 la |
do' do' do' re' sib4 r8 sib^\markup\italic ton caffard |
sib sib lab sib sol4 r |
sol8 sol16 sol la!8 sib fa4 r |
\ffclef "bass" <>^\markup\character Un Prêtre
r4 r8 fa16 fa sib4 sib8 re' |
sib4 r16 sib lab sib sol4 r |
\ffclef "soprano/treble" <>^\markup\character-text Elamir accourant
r2 r4 r8 sib' |
mib'' mib''
\ffclef "bass" <>^\markup\character-text Arthénée tendrement
r16 sib sib sib sol4. sib8 |
mib4 r r2 |
r8 sol16 sol do'2 sib8 do' |
lab lab r4^\markup\italic (il s’assied) r2 |
r4 r8 lab16 lab lab4 lab8 lab |
reb'4 r8 lab dob' dob' reb' lab |
sib4 r8 reb'16 reb' sib4 lab8 solb |
si!4
\ffclef "soprano/treble" <>^\markup\character-text Elamir pénétré
r8 fad' si'!4 si'8 r |
red''4 r8 red''16 red'' si'4 r |
\ffclef "bass" <>^\markup\character-text Arthénée Fièrement
r2 r4 fad |
si4. si8 si4 si |
fad4 fad8 sold la4 la8 si |
sold4 sold8 r r\fermata si si si |
re'4. si8 sold4 r8 si |
sold sold sold la la mi r4 |
r4 <>^\markup\italic avec séduction sol4. sol8 sol fad16 sol |
mi4 mi8 fad sol4 sol8 la |
fad4 r8 la do'8. do'16 do'8 la |
fad4 r8 la fad fad fad sol |
re4 r
\ffclef "soprano/treble" <>^\markup\character-text Elamir ardemment
r8 sol'16 sol' sol'8 sol'16 la' |
si'4. si'8 sol' sol' r re'' |
re'' re'' do'' re'' si'4 r8 do'' |
do'' sol'
\ffclef "bass" <>^\markup\character Arthénée
r4 sol8 sol16 sol fa8 sol |
mi4 r8 sol sol sol do'8. do'16 |
la4 r r2*3/4 s8^\markup\italic\right-align\line { L’enfant se prosterne. } |
