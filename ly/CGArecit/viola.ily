\clef "alto" sib8.-\sug\ff sib'16 sib'8. sol16 lab8 sol fa4 |
fa'8. re'16 re'8. re'16 mib'8 fa' sib4 |
r16. lab32 do'16. mib'32 lab'4 r16. sib32 re'16. fa'32 sib'8 si' |
do''4 r r8 fa'16. fa'32 fa'8 fa' |
sib'8 sib16. sib32 sib8 sib mib'\noBeam do'16. do'32 do'8 do' |
sib4. r16 sib sib4 sib |
sib2 r |
R1*2 |
r8 fa16.-\sug\f fa32 fa8 fa' re'2 |
r2 r8 r16 sol sol4 |
r2 r8 mib'16. mib'32 mib'8 si! |
do'4 r r2 |
r fa'4 r |
R1 |
r2 re'4 r |
r2 mib'4 r |
r2 r4 fa' |
sib r r2 |
r2 r4 mib'-\sug\p |
fa'( re' mib') r |
R1 |
r4 mib( sol8) sol( lab sib) |
sol1 |
fa4 r lab8-\sug\f lab4 lab8 |
\tieDashed lab1-\sug\p~ |
lab2~ lab | \tieSolid
sib~ sib |
si!1 |
si2~ si4 r8. si16\f |
si8. si16 si8 si si4 r |
R1*2 |
r4 mi'-\sug\f mi r |
R1 |
r2 r8 dod'16. dod'32 dod'8 dod' |
mi'1~ |
mi' |
re'~ |
re' |
r4 re'-\sug\f sol2~ |
sol1 |
sol |
do'~ |
do'4 r r2 |
do'4 r r2 |
