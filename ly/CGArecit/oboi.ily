\clef "treble"
<>-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { mib'8. mib''16 mib''8. mib''16 re''8 do'' sib'4 |
    re''8. re''16 re''8. re''16 mib''8 re'' mib''4 | }
  { mib'8. sol'16 sol'8. sol'16 lab'8 sol' fa'4 |
    fa'8. fa'16 fa'8. fa'16 sol'8 fa' mib'4 | }
>>
do''~ do''16. do''32 fa''16. mib''32 re''4~ re''16. re''32 sol''16. fa''32 |
mib''8. sol''32 fa'' mib''16-! re''-! do''-! sib'?-!
\twoVoices#'(oboe1 oboe2 oboi) <<
  { la'!8 mib''16. mib''32 mib''8 mib'' |
    re''8\noBeam lab''16. lab''32 lab''8 lab'' sol''\noBeam la''16. la''32 la''8 la'' |
    sib''4. s16 sib' sib'4 sib' |
    sib'2 }
  { la'!8 la'16. la'32 la'8 la' |
    sib'8\noBeam re''16. re''32 re''8 re'' mib''?\noBeam do''16. do''32 do''8 do'' |
    sib'4. s16 re' re'4 re' |
    re'2 }
  { s2 s1 s4. r16 }
>> r2 |
R1*39 |
