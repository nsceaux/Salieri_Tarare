\clef "bass" mib8.\ff mib16 mib8. mib16 fa8 mib re4 |
sib,8. sib,16 sib,8. sib,16 mib8 lab sol4 |
r16. lab,32 do16. mib32 lab4 r16. sib,32 re16. fa32 sib8 si |
do'4 r r8 fa16. fa32 fa8 fa |
sib8 sib,16. sib,32 sib,8 sib, mib\noBeam do16. do32 do8 do |
sib,4. r16 sib, sib,4 sib, |
sib,2
<<
  \tag #'fagotti { r2 | R1*39 | }
  \tag #'basso {
    r2 |
    R1*2 |
    r8 sib,16.\f sib,32 sib,8 sib, si,2 |
    r2 r8 r16 sol, sol,4 |
    r2 r8 mib16. mib32 mib8 si,! |
    do4 r r2 |
    r2 fa4 r |
    R1 |
    r2 re4 r |
    r2 mib4 r |
    r2 r4 fa |
    sib, r r2 |
    r r4 mib\p |
    fa( re mib) r |
    R1 |
    r4 mib sol8( mib fa sol) |
    mi!1 |
    fa2 fa4\f mib |
    reb1\p~ |
    reb2~ reb |
    solb~ solb |
    red1~ |
    red2~ red4 r8. si,16\f |
    si,8. si,16 si,8 si, si,4 r |
    R1*2 |
    r4 mi-\sug\f mi r |
    R1 |
    r2 r8 la,16. la,32 la,8 la, |
    dod1~ |
    dod |
    do!~ |
    do |
    r4 re\f sol, r |
    R1*2 |
    mi1~ |
    mi4 r r2 |
    fa4 r r2 |
  }
>>
