\clef "treble" \transposition mib
<>-\sug\ff \twoVoices #'(corno1 corno2 corni) <<
  { mi''8. mi''16 mi''8. mi''16 fa''8 mi'' re''4 |
    re''8. re''16 re''8. re''16 mi''8 re'' do''4 | }
  { do''8. do''16 do''8. do''16 re''8 do'' re''4 |
    sol'8. sol'16 sol'8. sol'16 do''8 re'' do''4 | }
>>
R1 |
r2 r8 \twoVoices #'(corno1 corno2 corni) <<
  { re''16. re''32 re''8 re'' |
    sol'8\noBeam re''16. re''32 re''8 re'' mi''\noBeam do''16. do''32 do''8 do'' |
    sol'4. s16 sol' sol'4 sol' |
    sol'2 }
  { re''16. re''32 re''8 re'' |
    sol'8\noBeam sol'16. sol'32 sol'8 sol' do''\noBeam do''16. do''32 do''8 do'' |
    sol'4. s16 sol sol4 sol |
    sol2 }
  { s4. | s1 | s4. r16 }
>> r2 |
R1*39 |
