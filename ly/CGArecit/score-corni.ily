\score {
  <<
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
      \haraKiriFirst
    } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new GrandStaff \with {
      instrumentName = \markup\center-column { Cors \smaller en Mi♭ }
      \haraKiri
    } <<
      \new Staff <<
        \keepWithTag #'() \global
        \keepWithTag #'corno1 \includeNotes "corni"
      >>
      \new Staff <<
        \keepWithTag #'() \global
        \keepWithTag #'corno2 \includeNotes "corni"
      >>
    >>
  >>
  \layout { indent = \largeindent }
}
