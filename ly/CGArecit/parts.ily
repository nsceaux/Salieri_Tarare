\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix" #:tag-notes basso)
   (fagotti #:score-template "score-part-voix"
            #:notes "basso" #:tag-notes basso)
   (oboi #:score "score-oboi")
   (corni #:score "score-corni")
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#}))
