\clef "treble" <sol mib'>8.\ff <sol' mib''>16 q8. mib'16 re'8 \grace re'16 do' sib?32 do' sib!4 |
<re' sib'>8. <sib' sib''>16 q8. lab''16 sol''16. sib''32 \grace mib''16 re''16 do''32 re'' mib''4 |
do''~ do''16. do''32 fa''16. mib''32 re''4~ re''16. re''32 sol''16. fa''32 |
mib''8. sol''32 fa'' mib''16-! re''-! do''-! sib'?-! la'!8 <<
  \tag #'violino1 {
    <fa' mib''!>16. q32 q8 q |
    <fa' re''>8\noBeam <sib' lab''>16. q32 q8 q <sib' sol''>8\noBeam la''16. la''32 la''8 la'' |
    sib''4. r16 <sib' re'>16 q4 q |
    q2
  }
  \tag #'violino2 {
    la'16. la'32 la'8 la' |
    sib'8\noBeam <fa' re''>16. q32 q8 q <sol' mib''>8\noBeam <mib' mib''>16. q32 q8 q |
    <re'' re'>4. r16 <fa' sib>16 q4 q |
    q2
  }
>> r2 |
R1*2 |
r8 <<
  \tag #'violino1 { sib'16.\f sib'32 sib'8 lab' sol'2 | }
  \tag #'violino2 { re'16.-\sug\f re'32 re'8 re' re'2 | }
>>
r2 r8 r16 <<
  \tag #'violino1 { sol''16 sol''4 | }
  \tag #'violino2 { <re' si'!>16 q4 | }
>>
r2 r8 <<
  \tag #'violino1 { do''16. do''32 do''8 re'' | mib''4 }
  \tag #'violino2 { sol'16. sol'32 sol'8 sol' | sol'4 }
>> r4 r2 |
r2 <fa' do'' la''>4 r |
R1 |
r2 <re' sib' sib''>4 r |
r2 <mib' sib' sol''>4 r |
r2 r4 <fa' do'' la''>4 |
<fa' re'' sib''>4 r r2 |
r2 r4 sol'8.(\p mib'16) |
<<
  \tag #'violino1 { re'8( do'' sib' lab'?) sol'4 }
  \tag #'violino2 { re'4( fa' sib) }
>> r4 |
R1 |
r4 <<
  \tag #'violino1 { sib'8. mib''16 mib''4. reb''8 | do''1 | }
  \tag #'violino2 { mib'4~ mib'8 mib'4 mib'8 | sol'1 | }
>>
r4 r8. <<
  \tag #'violino1 {
    lab'16_\markup\center-align\italic\bold\line { et très \dynamic f } lab'8 do''4 do''8 |
    reb''1\p~ |
    reb''2~ reb''~ |
    reb''~ reb'' |
    si'!1 |
    si'2~ si'4 r8. fad'16\f |
    fad'8. si'16 si'16. red''32 red''16. fad''32 fad''4
  }
  \tag #'violino2 {
    lab'16-\sug\f lab'8 lab' \grace lab' solb' fa'16 solb' |
    fa'1-\sug\p~ |
    fa'2~ fa' |
    solb'~ solb' |
    fad'1 |
    fad'2~ fad'4 r8. red'16-\sug\f |
    red'8. red'16 red'16. si'32 si'16. red''32 red''4
  }
>> r4 |
R1*2 |
r4 <mi' si' sold''>\f <<
  \tag #'violino1 { <mi' si' si''>4 }
  \tag #'violino2 { <mi' si' sold''>4 }
>> r4 |
R1 |
r2 r8 <<
  \tag #'violino1 {
    la'16. la'32 la'8 la' |
    la'1~ |
    la' |
    la'~ |
    la' |
    r4 <re' la' fad''>\f sol''2~ |
    sol''1 |
    sol'' |
    sol''~ |
    sol''4 r r2 |
    la''4 r r2 |
  }
  \tag #'violino2 {
    mi'16. mi'32 mi'8 mi' |
    sol'!1~ |
    sol' |
    fad'~ |
    fad' |
    r4 <re' la'>-\sug\f <re' si'>2~ |
    q1 |
    q |
    do''1~ |
    do''4 r r2 |
    do''4 r r2 |
  }
>>
