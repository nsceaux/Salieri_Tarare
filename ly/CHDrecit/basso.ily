\clef "bass" R1*2 |
dod1\p |
r4 si,16\p lad, si, lad, \rt#4 { si, lad, } |
si,2 r |
R1 |
r2 r8. si,16\f si,8. si,16 |
mid1 |
fad4 fad fad r |
r2 mi4 r |
r2 re4 r |
