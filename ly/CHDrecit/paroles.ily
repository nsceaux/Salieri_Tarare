Je ne puis sou -- te -- nir la cla -- meur im -- por -- tu -- ne ;
d’un peuple en -- tier sourd à ma voix.

Ce choix est une in -- ju -- re à tous tes chefs com -- mu -- ne ;
il at -- ta -- que nos pre -- miers droits.
l’ar -- ro -- gant sol -- dat de for -- tu -- ne
doit- il aux grands dic -- ter des lois ?
