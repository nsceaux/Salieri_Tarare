\clef "alto" R1*2 |
dod'1\p |
r4 si16\p lad si lad \rt#4 { si lad } |
si2 r |
R1 |
r2 r8. si16\f si8. si16 |
mid'1 |
fad'4 fad' fad' r |
r2 mi'4 r |
r2 re'4 r |
