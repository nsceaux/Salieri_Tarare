\clef "bass" <>^\markup\character-text Atar (à part) _"Parlé"
r4 r8 la16 la la4 la8 la |
fad4 fad8 fad fad4 fad8 sol |
mi mi r16 mi mi mi lad4 lad8 lad16 si |
fad4 r r2 |
\ffclef "bass" <>^\markup\character-text Altamort l’arrête
r2 r4 r8 fad |
fad fad fad fad si si r si |
re' re' dod' re' si si r4 |
r r8 sold16 sold dod'8 dod' si si16 dod' |
la4 r r r8 fad16 sold |
la8 la la la16 si dod'8 dod' r16 mi' mi' mi' |
dod'8 dod' mi' la re'4 r |
