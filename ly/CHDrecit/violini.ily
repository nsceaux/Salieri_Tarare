\clef "treble" R1*2 |
<<
  \tag #'violino1 { lad'1\p | }
  \tag #'violino2 { mi'1-\sug\p | }
>>
r4 si16\p lad si lad \rt#4 { si lad } |
si2 r |
R1 |
r2 r8. <<
  \tag #'violino1 { re'''16\f re'''8. re'''16 | dod'''4 }
  \tag #'violino2 { fad''16-\sug\f fad''8. fad''16 | sold''4 }
>> r4 r2 |
r16 la' dod'' fad'' la'' fad'' dod'' fad'' la'4 r |
r2 <<
  \tag #'violino1 { dod'''4 }
  \tag #'violino2 { sol''4 }
>> r4 |
r2 <<
  \tag #'violino1 { re'''4 }
  \tag #'violino2 { <la' fad''>4 }
>> r4 |
