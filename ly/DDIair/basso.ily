\clef "bass" 
<<
  \tag #'fagotti { r4 | R2*7 | r4 }
  \tag #'basso {
    r8 sol,\p |
    re-! sol-! r sol, |
    re sol fad( sol |
    mi re) dod4( |
    do!) si,8-! sol,-! |
    re sol r sol, |
    re sol fad( fa |
    mi do re) re, |
    sol, r
  }
>> sol,16*2/3\ff[ si, re] sol[ re si,] |
sol,[ si, re] sol[ re si,] sol,[ si, re] sol[ si sol] |
fad[ re' la] fad[ la fad] re8-\sug\p r |
<<
  \tag #'fagotti {
    R2 |
    r4 re'\ff~ |
    re' dod'~ |
    dod'
  }
  \tag #'basso {
    mi8 r dod r |
    re r re16*2/3-\sug\ff re re re re re |
    \ru#3 \tuplet 6/4 { re4.:16 }
  }
>> re8~ re32 mi64 fad sol la si dod' |
re'16*2/3 re re re re re mi16. mi'32 mi'16. mi'32 |
mi'4 fad16*2/3 mi re fad mi re |
sol fad mi sol fad mi fad mi re fad mi re |
sol fad mi sol fad mi r8 fad |
r sol la la, |
re4 r |
<<
  \tag #'fagotti { R2*7 | r4 }
  \tag #'basso {
    R2*3 |
    r4\fermata r8 sol,-\sug\p |
    re-! sol-! r8 sol, |
    re sol fad( sol |
    mi re) dod4( |
    do!)
  }
>> si,8\f sol, |
re-! sol-! r8 sol, |
re sol fad fa |
mi do re re, |
sol,4 <<
  \tag #'fagotti {
    r4 |
    R2 |
    r4 <>^"Solo" \clef "tenor" \slurDashed fa'8(-\sug\mf sol'16. fa'32 |
    \grace fa'8 mib' re' \grace re' do' sib16 la |
    sib4) r4 |
    R2 |
    r4 fa'8(-\sug\mf sol'16. fa'32 |
    \grace fa'8 mib' re' \grace re' do' sib16 la) | \slurSolid
    sib4 \clef "bass"
  }
  \tag #'basso {
    sol4-\sug\p( |
    fad8 fa mib4 |
    re) re'(\mf |
    la8 sib mib fa) |
    sib,4 r8 sol(-\sug\p |
    fad fa mib4 |
    re) \once\slurDashed re'(\mf |
    do'8 sib mib fa) |
    sib,4
  }
>> mib4-\sug\ff |
mib mib |
mib mib |
mib mib |
mib sol8 sol16. sol32 |
sol8 re16 mib fa8 sol16 fa |
mib4 sol8 sol16. sol32 |
sol8 re16 mib fa8 sol16 fa |
mib4 r |
<<
  \tag #'fagotti {
    R2*3 |
    \clef "tenor" r4 \slurDashed sol'8.( re'32 sib) |
    la8.( do'32 mib') \slurSolid sol8 fad |
    sol4 \clef "bass"
  }
  \tag #'basso {
    R2 |
    r4 sol(\p |
    fad8 fa mib4 |
    re) r8 sib, |
    r do re re |
    sol,4
  }
>> mib4\ff |
mib mib |
mib mib |
mib mib |
mib sol8 sol16. sol32 |
sol8 re16 mib fa8 sol16 fa |
mib4 sol8 sol16. sol32 |
sol8 re16 mib fa8 sol16 fa |
mib8 r r4 |
<<
  \tag #'fagotti { R2*10 }
  \tag #'basso {
    R2 |
    r4 sol-\sug\p( |
    fad8 fa mib4) |
    re r8 sib, |
    r do r re |
    r mib r mib |
    re-! mib-! re-! mib-! |
    re4 r |
    R2 |
    r4 \grace mi8 re32 dod re16 r8 |
  }
>>
%% Majeur
<<
  \tag #'fagotti { R2*4 | r4 }
  \tag #'basso {
    <>^"Tutti" re,4 r8 sol, |
    re8 sol r sol, |
    re sol fad( sol |
    mi re dod4) |
    do4
  }
>> si,8\f sol, |
re sol, do do |
si, la, la\ff la |
<<
  \tag #'fagotti {
    <<
      { la2 | si | la | sol4 } \\
      { fad2 | sol | fad | sol4 }
    >> r4 |
    R2 |
    \clef "tenor" r4 mi'8\p fad'16( sol') |
    sol'8 fad'16 mi' re'8 re'16. re'32 |
    re'4 mi'8 fad'16 sol' |
    sol'16( re' si sol) sol8 \grace si la |
    sol( la) si16.( re'32 fad16. la32) |
    sol8 la si16. re'32 fad16. la32 |
    sol2:8 |
    sol4 r r |
    R2.*2 |
  }
  \tag #'basso {
    re2:8 |
    re:8 |
    re:8 |
    mi8 r do r |
    re r re r |
    sol,4-\sug\p( do) |
    si,( fad,) |
    sol,4( do) |
    si,8 sol, re re |
    sol( re sol, re) |
    sol re sol, re |
    sol sol, sol, sol, |
    sol,4 r r |
    fa8.\f mi16 re4 re |
    mi2.\fermata |
  }
>>
