\clef "treble" re'''8.(-\sug\p mi'''16) |
\grace sol''8 fad''( sol'') re'''8.( mi'''16) |
\grace sol''8 fad''( sol'') la''( si'') |
\grace re''' do'''( si'') la'' la''16. la''32 |
la''8. si''32 do''' re'''16 \tuplet 3/2 { mi'''32 re''' dod''' } re'''16. mi'''32 |
\once\slurDashed fad''8( sol'') re'''16 \tuplet 3/2 { mi'''32 re''' dod''' } re'''16. mi'''32 |
fad''8( sol'') la''( si'' |
do'''8 re'''16. mi'''32 si''8.) la''16 |
sol''4 <>-\sug\ff \twoVoices#'(flauto1 flauto2 flauti) <<
  { sol''8. si''16 |
    si''8. re'''16 re'''8. si''16 |
    la''4 }
  { si'8. sol''16 |
    sol''8. si''16 si''8. sol''16 |
    re''4 }
>>
<<
  \tag #'(flauto1 flauti) {
    <>^"Solo" la''8.-\sug\p si''16 |
    \tuplet 3/2 { la''16( fad'' sol'') } sol''8 sol''8. la''16 |
    \tuplet 3/2 { sol''16( mi'' fad'') } fad''8 r4 |
    R2*3
  }
  \tag #'flauto2 { r4 | R2*5 | }
>>
r4 r16*2/3 \twoVoices#'(flauto1 flauto2 flauti) <<
  { la''16*2/3 la'' la'' la'' la'' |
    la''16. si''32 la''8~ la''16*2/3 la'' la'' la'' la'' la'' |
    la''16. si''32 la''16. la''32 la''4 |
    si''8. sol''16 fad''8 mi'' |
    re''4 }
  { la'16*2/3 la' la' la' la' |
    dod''4 re''16*2/3 la' la' la' la' la' |
    dod''4 re'' |
    re''8. mi''16 re''8 dod'' |
    re''4 }
>> r4 |
<<
  \tag #'(flauto1 flauti) {
    R2*3 |
    r16\fermata <>^"Solo" sol''32([ fad''] sol'' la'' si'' do''') \slurDashed re'''8.( mi'''16 |
    \grace sol''8 fad'' sol'' re'''8. mi'''16) |
    \grace sol''8 fad'' sol'' la''( si'' |
    \grace re'''8 do''' si'') \slurSolid la'' la''16. la''32 |
    la''8. si''32 do''' re'''16\f \tuplet 3/2 { mi'''32 re''' dod''' } re'''16. mi'''32 |
    \grace sol''8 fad'' sol'' re'''16 \tuplet 3/2 { mi'''32 re''' dod''' } re'''16. mi'''32 |
    \grace sol''8 fad'' sol'' la'' si'' |
    do'''8 re'''16. mi'''32 si''8 la'' |
    sol''4 r4 |
    R2*7 |
    r4
  }
  \tag #'flauto2 { R2*19 r4 }
>> \allowPageTurn r4 |
R2*3 |
r4 <>^"tutti" sib''8-\sug\ff sib''16. sib''32 |
sib''8 fa''16 sol'' \grace sib''8 lab''16 sol''32 lab'' sib''16 lab'' |
sol''4 sib''8 sib''16. sib''32 |
sib''8 fa''16 sol'' \grace sib''8 lab''16 sol''32 lab'' sib''16 lab'' |
sol''8 r r4 |
R2*9 |
r4 <>^"tutti" sib''8-\sug\ff sib''16. sib''32 |
sib''8 fa''16 sol'' \grace sib''8 lab''16 sol''32 lab'' sib''16 lab'' |
sol''8 r sib''8 sib''16. sib''32 |
sib''8 fa''16 sol'' \grace sib''8 lab''16 sol''32 lab'' sib''16 lab'' |
sol''8 r r4 |
R2*7 |
re''16 mi''32 fad'' sol'' la'' si'' dod''' re'''16 re''' re''' re''' |
re'''2~ |
re''' |
re'''4 re'''8. mi'''16 |
\grace sol''8 fad'' sol'' re'''8. mi'''16 |
\grace sol''8 fad'' sol'' la'' si'' |
\grace re''' do''' si'' la'' la''16. la''32 |
la''8. si''32 do''' re'''16\f mi'''32*2/3 re''' dod''' re'''16. mi'''32 |
fad''8 sol''8 mi'''16 fa'''32*2/3 mi''' re''' mi'''16. fa'''32 |
sold''8 la'' \twoVoices#'(flauto1 flauto2 flauti) <<
  { do'''8 do'''16. do'''32 |
    fad''2 sol'' |
    fad'' |
    sol''8 s do''' s |
    si'' s la'' s |
    sol''4 }
  { mi''8 mi''16. mi''32 |
    la'2 |
    si' |
    la' |
    si'8 s sol'' s |
    sol'' s fad'' s |
    sol''4 }
  { s4 s2*3 | s8 r s r | s r s r | }
>> r4 |
R2*3 |
\twoVoices#'(flauto1 flauto2 flauti) <<
  { sol''8 la'' si'' la'' |
    sol'' la'' si'' la'' |
    sol''2:8 |
    sol''4 }
  { si'8 re'' sol'' re'' |
    si' re'' sol'' re'' |
    si'2:8 |
    si'4 }
>> r4 r |
R2.*2 |

