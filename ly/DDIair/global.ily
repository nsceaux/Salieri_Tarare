\tag #'all \key sol \major
\tempo "un poco Andante" \midiTempo#60
\time 2/4 \partial 4 s4 s2*19 s4 \bar "|." s4 s2*11 \bar "||"
\tag #'all \key sol \minor s2*8 s4
\bar ".|:" s4 s2*32 \bar "||"
\tag #'all \key sol \major s2*19
\time 3/4 s4 \tempo "Adagio" s2 s2.*2 \bar "|."
