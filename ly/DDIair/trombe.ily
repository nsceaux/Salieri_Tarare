\clef "treble" \transposition mib
r4 |
R2*39 |
r4 <>-\sug\ff \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''8 do''16. do''32 |
    do''8 do''16. do''32 do''8 do''16. do''32 |
    do''4 do''8 do''16. do''32 |
    do''8 do''16. do''32 do''8 do''16. do''32 |
    do''4 sol''8 sol''16. sol''32 |
    sol''2~ |
    sol''~ |
    sol''~ |
    sol''4 }
  { do'8 do'16. do'32 |
    do'8 do'16. do'32 do'8 do'16. do'32 |
    do'4 do'8 do'16. do'32 |
    do'8 do'16. do'32 do'8 do'16. do'32 |
    do'4 sol'8 sol'16. sol'32 |
    sol'2~ |
    sol'~ |
    sol' |
    do''4 }
>> r4 |
R2*5 |
r4 <>-\sug\ff \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''8 do''16. do''32 |
    do''8 do''16. do''32 do''8 do''16. do''32 |
    do''4 do''8 do''16. do''32 |
    do''8 do''16. do''32 do''8 do''16. do''32 |
    do''4 sol''8 sol''16. sol''32 |
    sol''2~ |
    sol''~ |
    sol''~ |
    sol''8 }
  { do'8 do'16. do'32 |
    do'8 do'16. do'32 do'8 do'16. do'32 |
    do'4 do'8 do'16. do'32 |
    do'8 do'16. do'32 do'8 do'16. do'32 |
    do'4 sol'8 sol'16. sol'32 |
    sol'2~ |
    sol'~ |
    sol'~ |
    sol'8 }
>> r8 r4 |
R2*10 R2*19 R2.*3
