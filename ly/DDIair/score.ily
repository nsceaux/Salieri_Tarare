\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \flautiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'flauti \includeNotes "flauti"
      >>
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small in G }
        shortInstrumentName = "Cor."
        \haraKiri
      } <<
        \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \small in Mi♭ }
        shortInstrumentName = "Tr."
        \haraKiri
      } <<
        \keepWithTag #'() \global \keepWithTag #'trombe \includeNotes "trombe"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "basso"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      shortInstrumentName = "As."
      \haraKiriFirst
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr \consists "Metronome_mark_engraver" } <<
      \global \keepWithTag #'basso \includeNotes "basso"
      \origLayout {
        s4 s2*4\break s2*5\pageBreak
        s2*5\break s2*5\pageBreak
        s2*6\break \grace s8 s2*5\pageBreak
        s2*8\break \grace s8 s2*6\pageBreak
        s2*7\break s2*7\pageBreak
        s2*8\break s2*7\pageBreak
        \grace s8 s2*5\break \grace s8 s2*8\pageBreak
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
