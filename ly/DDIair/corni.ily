\clef "treble" \transposition sol
r4 |
R2*7 |
r4 <>-\sug\ff \twoVoices#'(corno1 corno2 corni) <<
  { do''8. mi''16 |
    mi''8. sol''16 sol''8. mi''16 |
    re''4 }
  { mi'8. do''16 |
    do''8. mi''16 mi''8. do''16 |
    sol'4 }
>> r4 |
R2 |
r4 <>-\sug\ff \twoVoices#'(corno1 corno2 corni) <<
  { sol'4~ |
    sol'2~ |
    sol' |
    sol'4 do''16. do''32 do''16. do''32 |
    do''4 }
  { sol'4~ |
    sol'2~ |
    sol' |
    sol'4 do''16. do''32 do''16. do''32 |
    do''4 }
>> r16*2/3 \twoVoices#'(corno1 corno2 corni) <<
  { re''16*2/3 re'' re'' re'' re'' | re''2~ | re''4 s8 re'' |
    s mi'' re'' re'' | sol'4 }
  { re''16*2/3 re'' re'' re'' re'' | re''2~ | re''4 s8 sol' |
    s do'' re'' re'' | sol'4 }
  { s16*10/3 | s2 | s4 r8 s | r }
>> r4 |
R2*7 |
r4 <>-\sug\f \twoVoices#'(corno1 corno2 corni) <<
  { do''8. mi''16 |
    re''8 do'' do''8. mi''16 |
    re''8 do'' re'' do'' |
    do''4 mi''8 re'' |
    do''4 }
  { mi'8. do''16 |
    sol'8 mi' mi'8. do''16 |
    sol'8 mi' sol' do'' |
    do''4 do''8 sol' |
    do''4 }
>> r4 |
R2*7 |
r4 r |
R2*32 |
%% Majeur
R2*4 |
r4 <>-\sug\f \twoVoices#'(corno1 corno2 corni) <<
  { mi''8. mi''16 | re''8 do'' }
  { do''8. do''16 | sol'8 mi' }
>> r4 |
r \twoVoices#'(corno1 corno2 corni) <<
  { re''8 re''16. re''32 |
    re''16. re''32 re''16. re''32 re''16. re''32 re''16. re''32 |
    mi''16. mi''32 mi''16. mi''32 mi''16. mi''32 mi''16. mi''32 |
    re''16. re''32 re''16. re''32 re''16. re''32 re''16. re''32 |
    do''8 s do'' s |
    mi'' s re'' s |
    do''2 |
    do''4 re'' |
    do''2 |
    mi''4 re'' |
    do''8 re'' mi'' re'' |
    do'' re'' mi'' re'' |
    do''2:8 |
    do''4 }
  { re''8 re''16. re''32 |
    sol'16. sol'32 sol'16. sol'32 sol'16. sol'32 sol'16. sol'32 |
    do''16. do''32 do''16. do''32 do''16. do''32 do''16. do''32 |
    sol'16. sol'32 sol'16. sol'32 sol'16. sol'32 sol'16. sol'32 |
    mi'8 s do'' s |
    do'' s sol' s |
    do''2 |
    do''4 sol' |
    do''2 |
    do''4 sol' |
    mi'8 sol' do'' sol' |
    mi' sol' do'' sol' |
    mi'2:8 |
    mi'4 }
  { s4 | s2*3 | s8 r s r | s r s r | s-\sug\p }
>> r4 r |
R2.*2 |
