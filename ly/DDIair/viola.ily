\clef "alto" r8 sol\p |
re'-! sol'-! r sol |
re' sol' fad'( sol' |
mi' re') dod'4( |
do'!4) si8-! sol-! |
re' sol' r sol |
re' sol' fad'( fa' |
mi' do' re') re |
sol r sol16*2/3-\sug\ff[ si re'] sol'[ re' si] |
sol[ si re'] sol'[ re' si] sol[ si re'] sol'[ si' sol'] |
fad'[ re'' la'] fad'[ la' fad'] re'8-\sug\p r |
mi' r dod' r |
re' r re'16*2/3-\sug\ff re' re' re' re' re' |
\tuplet 6/4 re'4.:16 \tuplet 6/4 dod':16 |
\tuplet 6/4 dod':16 re8~ re32 mi64 fad sol la si dod' |
re'16*2/3 re re re re re mi16. dod''32 dod''16. dod''32 |
dod''4 fad16*2/3[ mi re] fad[ mi re] |
sol[ fad mi] sol[ fad mi] fad[ mi re] fad[ mi re] |
sol fad mi sol fad mi \tuplet 6/4 fad4.:16 |
\tuplet 6/4 sol:16 \tuplet 6/4 la:16 |
re4 re'8-\sug\p re'16. re'32 |
do'8 r sold sold |
la4 do'8 do'16. do'32 |
si8 r fad r |
sol4\fermata r8 sol |
re'-! sol'-! r sol |
re' sol' \slurDashed fad'( sol' |
mi' re') dod'4( |
do'!) \slurSolid si16*2/3-\sug\f[ re' sol'] si[ re' sol'] |
la re' do' si la sol si[ re' sol'] si[ re' sol'] |
la re' do' si la sol re'[ fad' la'] re'[ sol' si'] |
mi'8 do' re' re |
sol4 \once\slurDashed sol-\sug\p( |
fad8 fa mib4 |
re) re'-\sug\mf( |
la8 sib mib fa) |
sib4 r8 \once\slurDashed sol(-\sug\p |
fad fa mib4 |
re) \once\slurDashed re'-\sug\mf( |
do'8 sib mib fa) |
sib4 <>-\sug\ff \ru#8 { sol16*2/3 sib mib' sol' mib' sib } sol8 sol'16. sol'32 |
sol'8 re'16 mib' \grace sol'8 fa'16 mib'32 fa' sol'16 fa' |
mib'4 sol'8 sol'16. sol'32 |
sol'8 re'16 mib' fa' mib'32 fa' sol'16 fa' |
mib'4 do'-\sug\p |
sib8 r8 la!4 |
sib8 r sol'4( |
fad'8 fa' mib'4 |
re'4) r8 sib |
r do' re'4 |
sib <>-\sug\f \ru#8 { sol16*2/3 sib mib' sol' mib' sib } sol8 sol'16. sol'32 |
sol'8 re'16 mib' fa'16 mib'32 fa' sol'16 fa' |
mib'8 r sol'8 sol'16. sol'32 |
sol'8 re'16 mib' fa'8 sol'16 fa' |
mib'8 r do'4-\sug\p |
sib8 r8 la!4 |
sib8 r sol'4( |
fad'8 fa' mib'4) |
re'4 r8 <re' sol>8 |
r mib' r fa' |
r sol' r sol' |
fad' mib!-! re-! mib-! |
re4 r |
R2 |
r4 \grace mi8 re32 dod re16 r8 |
r4 r8 sol |
re'8 sol' r sol |
re' sol' fad' sol' |
mi' re' dod'4 |
do'!4 si16*2/3-\sug\f[ re' sol'] si[ re' sol'] |
la re' do' si la sol sol[ do' mi'] la[ do' mi'] |
re' mi' re' do' si la la'8 la'16. la'32 |
la'16. <fad' la'>32 q16. q32 q16. <la fad'>32 q16. q32 |
<si sol'>16. q32 q16. q32 q16. <sol' si'>32 q16. q32 |
<re' la'>16. q32 q16. q32 q16. <la fad'>32 q16. q32 |
<si sol'>8 r <do sol mi' do''>8 r |
<sol re' si'> r <la fad'> r |
<sol sol'>4-\sug\p( <sol mi'>) |
<< { re'2 | re'4 mi' | re' re'8 fad' | } \\
  { sol4 la | si sol | sol si8 la | } >>
sol'8 re' sol re' |
sol' re' sol re' |
sol2:8 |
sol4 r r |
la'8.-\sug\f sol'16 fa'8. fa'16 fa'8. fa'16 |
si2.\fermata |

