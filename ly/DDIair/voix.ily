\clef "soprano/treble" r4 |
R2*84 |
r4 mi''8 mi''16. mi''32 |
re''8. re''16 re''8. do''16 |
si'8. si'16 mi''8 mi''16 mi'' |
re''4 re''8. fad'16 |
sol'4 r |
R2*2 R2.*3 |
