\clef "treble"
<<
  \tag #'violino1 {
    re''8.\p( mi''16) |
    \grace sol'8 fad'( sol') re''8.( mi''16) |
    \grace sol'8 fad'( sol') la'( si') |
    \grace re'' do''( si') la' la'16. la'32 |
    la'8. si'32 do'' re''16 \tuplet 3/2 { mi''32 re'' dod'' } re''16. mi''32 |
    fad'8( sol') re''16 \tuplet 3/2 { mi''32 re'' dod'' } re''16. mi''32 |
    fad'8( sol') la'( si' |
    do'' re''16. mi''32 si'8.) la'16 |
    sol'8 r <re' si' sol''>16\ff \tuplet 3/2 { fad''32[ sol'' la''] } \tuplet 3/2 { sol''16-![ re''-! si''-!] } |
    \override Beam.auto-knee-gap = #2
    si''8 sol16. re'''32 re'''8. si''32 sol'' |
    \revert Beam.auto-knee-gap
    la''16. fad''32 re''8 la''8.(\p si''16 |
    la''16*2/3 fad'' sol'') sol''8 sol''8.( la''16 |
    sol''16*2/3 mi'' fad'') fad''8
  }
  \tag #'violino2 {
    si16*2/3-![-\sug\p re'-! sol'-!] si-![ re'-! sol'-!] |
    la[ re' do'] si[ la sol] si[ re' sol'] si[ re' sol'] |
    la re' do' si la sol re'[ fad' la'] re'[ sol' si'] |
    mi'[ sol' do''] re'[ sol' si'] mi'[ sol' la'] mi'[ sol' la'] |
    re'[ fad' la'] re'[ fad' la'] re'[ sol' si'] si[ re' sol'] |
    la[ re' do'] si[ la sol] si[ re' sol'] si[ re' sol'] |
    la[ re' do'] si[ la sol] re'[ fad' la'] re'[ sol' si'] |
    mi'[ sol' do''] mi'[ sol' do''] re'[ sol' si'] re'[ fad' la'] |
    sol'8 r sol16*2/3-\sug\ff[ si re'] sol'[ re' si] |
    sol[ si re'] sol'[ re' si] sol[ si re'] sol'[ si' sol'] |
    fad'[ re'' la'] fad'[ la' fad'] re'-\sug\p[ fad' la'] re'[ fad' la'] |
    si[ mi' sol'] si[ mi' sol'] la[ mi' sol'] la[ mi' sol'] |
    la[ re' fad'] la[ re' fad']
  }
>> re'8\ff~ re'32 mi'64 fad' sol' la' si' dod'' |
re''16.[ re'32] \grace mi'8 re'16 dod'32 re' <<
  \tag #'violino1 {
    mi'16. mi''32 mi''16. mi''32 |
    mi''4
  }
  \tag #'violino2 {
    dod'16. la'32 la'16. la'32 |
    la'4
  }
>> fad'8~ fad'32 sol'64 la' si' do'' re'' mi'' |
fad''16.[ fad'32] \grace sol'8 fad'16 mi'32 fad' sol'16. sol''32 sol''16. sol''32 |
sol''4 <<
  \tag #'violino1 {
    r16*2/3 <la' la''> q q q q |
    la''16. si''32 la''8~ la''16*2/3 la'' la'' la'' la'' la'' |
    la''16. si''32 la''16. la''32 re'''8. la''32 fad'' |
    si''8. sol''32 mi'' re''8 mi'' |
    re''4
  }
  \tag #'violino2 {
    la'16*2/3[ sol' fad'] la'[ sol' fad'] |
    mi'[ re' dod'] mi'[ re' dod'] la'[ sol' fad'] la'[ sol' fad'] |
    mi'[ re' dod'] mi'[ re' dod'] re' <re' la'> q q q q |
    <re' si'> <sol' si'> q q q q << { la' la' la' dod'' dod'' dod'' } \\ { fad' fad' fad' mi' mi' mi' } >> |
    <fad' re''>4
  }
>>
<<
  \tag #'violino1 {
    fa''8\p fa''16. fa''32 |
    mi''4 \grace mi''8 re''16. do''32 re''16. mi''32 |
    do''4 mi''8 mi''16. mi''32 |
    re''16*2/3 mi'' fad'' sol'' la'' si'' la'' sol'' fad'' mi'' re'' do'' |
    si'16\fermata sol'32([ fad'] sol'[ la' si' do'']) re''8.( mi''16 |
    \grace sol'8 fad' sol' re''8. mi''16) |
    \grace sol'8 fad' sol' la'( si' |
    \grace re'' do'' si') la' la'16. la'32 |
    la'8. si'32 do'' re''16\f \tuplet 3/2 { mi''32 re'' dod'' } re''16. mi''32 |
    \grace sol'8 fad' sol' re''16 \tuplet 3/2 { mi''32 re'' dod'' } re''16. mi''32 |
    \grace sol'8 fad' sol' la' si' |
    do''8 re''16. mi''32 si'8 la' |
  }
  \tag #'violino2 {
    sold'8-\sug\p sold'16. sold'32 |
    la'4 <mi' si'>8 q |
    <mi' do''>4 fad'8 fad'16. fad'32 |
    sol'8 r <la' re'> r |
    <re' si'>8\fermata r si16*2/3 re' sol' si re' sol' |
    la re' do' si la sol si re' sol' si re' sol' |
    la re' do' si la sol re' fad' la' re' sol' si' |
    mi' sol' do'' re' sol' si' mi' sol' la' mi' sol' la' |
    re' fad' la' re' fad' la' re'-\sug\f[ sol' si'] si[ re' sol'] |
    la re' do' si la sol si[ re' sol'] si[ re' sol'] |
    la re' do' si la sol re' fad' la' re' sol' si' |
    mi'[ sol' do''] mi'[ sol' do''] re'[ sol' si'] re'[ fad' la'] |
  }
>>
%% Mineur
<<
  \tag #'violino1 {
    sol'4 re''8\p( \grace fa'' mib''16. re''32 |
    re''8 re'' sol''16 la''32 sib'' la''16 sol'' |
    sol''8 fad'') fa''8(\mf sol''16. fa''32 |
    \grace fa''8 mib'' re'' \grace re'' do'' sib'16 la' |
    \grace la'8 sib' fa') re''8\p( \grace fa'' mib''16. re''32 |
    re''8 re'' sol''16 la''32 sib'' la''16 sol'' |
    sol''8 fad'') fa''(\mf sol''16. fa''32 |
    \grace fa''8 mib'' re'' \grace re'' do'' sib'16 la') |
    sib'4
  }
  \tag #'violino2 {
    sol'4 sib'-\sug\p( |
    la' sol' |
    la') sib'-\sug\mf( |
    do''8 sib' sol' mib' |
    re'4) sib'-\sug\p( |
    la' sol' |
    la') sib'-\sug\mf( |
    la'8 sib' sol' mib') |
    re'4
  }
>>
%% Reprise
<<
  \tag #'violino1 {
    <sol mib' mib''>8\ff <sol' mib''>16. q32 |
    q8 <sol' sib'>16. q32 q8 <sib sol'>16. q32 |
    q8 <mib' sol> <mib' sib' sol''>8 <sib' sol''>16. q32 |
    q8 <mib'' sol'>16. q32 q8 <sol' sib'>16. q32 |
    q8 <sol' sib> <sib' sib''>8 q16. q32 |
    q8 fa''16[ sol''] \grace sib''8 lab''16 sol''32 lab'' sib''16 lab'' |
    sol''4 <sib'' sib'>8 q16. q32 |
    q8 fa''16[ sol''] \grace sib''8 lab''16 sol''32 lab'' sib''16 lab'' |
    sol''8 r mib'16*2/3\p mib'' re'' do'' sib' la'! |
    sib'8 r do'''16*2/3-. sib''-. la''!-. sol''-. fa''-. mib''-. |
    re''8 r re''( \grace fa'' mib''16. re''32 |
    re''8 re'' sol''16 la''32 sib'' la''16 sol'' |
    sol''8 fad'') sol''8.( re''32 sib') |
    la'8.( do''32 mib'') sol'8 fad' |
    sol'4 <sib sol' mib''>8\f <sol' mib''>16. q32 |
    q8 <sol' sib'>16. q32 q8 <sol' sib>16. q32 |
    q8 <mib' sol> <mib' sib' sol''>8 <sib' sol''>16. q32 |
    q8 <sol' mib''>16. q32 q8 <sol' sib'>16. q32 |
    q8 <sol' sib> <sib'' sib'> q16. q32 |
    <sib' sib''>8 fa''16[ sol''] \grace sib''8 lab''16 sol''32 lab'' sib''16 lab'' |
    sol''4 <sib'' sib'>8 q16. q32 |
    q8 fa''16[ sol''] \grace sib''8 lab''16 sol''32 lab'' sib''16 lab'' |
    sol''8 r mib'16*2/3\p mib''-. re''-. do''-. sib'-. la'!-. |
    sib'8 r8 do'''16*2/3-. sib''-. la''!-. sol''-. fa''-. mib''-. |
    re''8 r re''( \grace fa'' mib''16. re''32 |
    re''8 re'' sol''16 la''32 sib'' la''16 sol'') |
    sol''8( fad'')
  }
  \tag #'violino2 {
    <>\ff \ru#8 { sol16*2/3 sib mib' sol' mib' sib } sol <sib' sib''> q q q q |
    \ru#6 \tuplet 6/4 q4.:16 |
    q8 r do'4-\sug\p |
    sib8 r la!4 |
    sib8 r sib'4( |
    la'! sol' |
    la') r8 <re' sol> |
    r8 <do' mib'> sib la |
    sol4 <>-\sug\f \ru#8 { sol16*2/3 sib mib' sol' mib' sib } sol <sib' sib''> q q q q |
    \ru#6 \tuplet 6/4 <sib'' sib'>4.:16 |
    q8 r do'4\p |
    sib8 r la!4 |
    sib8 r sib'4( la' sol') |
    la'
  }
>> sol''8.( re''32 sib') |
la'8 r16 la'( si'8) r16 si'( |
do''8) r16 do''( dod''8) r16 dod'' |
re''8-! <<
  \tag #'violino1 { dod''-! re''-! dod''-! | re''4 }
  \tag #'violino2 { sol'8-! fad'-! sol'-! | fad'4 }
>> r4 |
<<
  \tag #'violino1 {
    r4 \grace mi''8 re''32 dod'' re''16 r8 |
    R2 |
  }
  \tag #'violino2 {
    R2 |
    \grace mi'8 re'32 dod' re'16 r8 r4 |
  }
>>
%% Majeur
<<
  \tag #'violino1 {
    r4 re''8.( mi''16) |
    \grace sol'8 fad' sol' re''8. mi''16 |
    \grace sol'8 fad' sol' la' si' |
    \grace re'' do'' si' la' la'16. la'32 |
    la'8. si'32 do'' re''16\f \tuplet 3/2 { mi''32 re'' dod'' } re''16. mi''32 |
    fad'8 sol' mi''16 \tuplet 3/2 { fa''32 mi'' re'' } mi''16. fa''32 |
    \grace la'8 sold' la' <la' do'''>8 q16. q32 |
    q16. la''32 la''16. fad''32 fad''16. re''32 re''16. do''32 |
    si'16. re''32 re''16. sol''32 sol''16. si''32 si''16. re'''32 |
    do'''16. la''32 la''16. fad''32 fad''16. re''32 re''16. do''32 |
    si'8
  }
  \tag #'violino2 {
    r4 si16*2/3[ re' sol'] si[ re' sol'] |
    la re' do' si la sol si re' sol' si re' sol' |
    la re' do' si la sol re' fad' la' re' sol' si' |
    mi' sol' do'' re' sol' si' mi' sol' la' mi' sol' la' |
    re' fad' la' re' fad' la' re'\f[ sol' si'] si[ re' sol'] |
    la re' do' si la sol sol[ do' mi'] la[ do' mi'] |
    re' mi' re' do' si la <<
      { mi''8 mi''16. mi''32 |
        fad''16. fad''32 fad''16. fad''32 fad''16. la'32 la'16. la'32 |
        si'16. si'32 si'16. si'32 si'16. sol''32 sol''16. sol''32 |
        fad''16. fad''32 fad''16. fad''32 fad''16. la'32 la'16. la'32 |
        si'8 } \\
      { do''8 do''16. do''32 |
        la'16. la'32 la'16. la'32 la'16. fad'32 fad'16. fad'32 |
        sol'16. sol'32 sol'16. sol'32 sol'16. si'32 si'16. si'32 |
        la'16. la'32 la'16. la'32 la'16. fad'32 fad'16. fad'32 |
        sol'8 }
    >>
  }
>> r8 <sol mi' do'' mi''>8 r |
<si sol' re''> r <la fad'> r |
<<
  \tag #'violino1 {
    <si sol'>8 r mi''8\p fad''16( sol'') |
    \grace la''8 sol'' fad''16 mi'' re''8 re''16. re''32 |
    re''4 mi''8( fad''16 sol'') |
    sol'' re'' si' sol' sol'8 \grace si' la' |
    sol'( la') si'16.( re''32 fad'16. la'32) |
    sol'8 la' si'16. re''32 fad'16. la'32 |
    sol'2 |
    sol'4..
  }
  \tag #'violino2 {
    si16*2/3-\sug\p([ re' sol'] si[ re' sol']) sol([ do' mi'] sol[ do' mi']) |
    sol([ si re'] sol[ si re']) la([ do' re'] la[ do' re']) |
    sol[ si re'] sol[ si re'] sol[ do' mi'] sol[ do' mi'] |
    sol[ si re'] sol[ si re'] si[ re' sol'] la[ re' fad'] |
    si[ re' sol'] fad'[ re' fad'] sol'[ re' si] la[ re' do'] |
    si[ re' sol'] fad'[ re' fad'] sol'[ re' si] la[ re' do'] |
    si8 <si re'> q q |
    q4..
  }
>> sol'16\f si'8. do''16 |
re''8. mi''16 fa''8. re''16 si'8. la'16 |
la'2 sold'4 |
