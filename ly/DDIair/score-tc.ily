\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Trompettes \small en Mi♭ }
      shortInstrumentName = "Tr."
      \haraKiri
    } <<
      \keepWithTag #'() \global \keepWithTag #'trombe \includeNotes "trombe"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Cors \small in G }
      shortInstrumentName = "Cor."
      \haraKiri
    } <<
      \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni"
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 8\mm
  }
}
