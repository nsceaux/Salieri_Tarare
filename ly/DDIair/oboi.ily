\clef "treble" r4 |
R2*7 |
r4 <>-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''8. si''16 |
    si''8. re'''16 re'''8. si''16 |
    la''4 }
  { si'8. sol''16 |
    sol''8. si''16 si''8. sol''16 |
    re''4 }
>> r4 |
R2 |
r4 <>-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { re''4~ |
    re'' mi''~ |
    mi'' \once\tieDashed fad''~ |
    fad'' sol''~ |
    sol'' }
  { re''4~ |
    re'' dod''~ |
    dod'' \once\tieDashed re''~ |
    re'' dod''~ |
    dod'' }
>> r16*2/3 \twoVoices#'(oboe1 oboe2 oboi) <<
  { la''16*2/3 la'' la'' la'' la'' |
    la''16. si''32 la''8~ la''16*2/3 la'' la'' la'' la'' la'' |
    la''16. si''32 la''16. la''32 la''4 |
    si''8. sol''16 fad''8 mi'' |
    re''4 }
  { la'16*2/3 la' la' la' la' |
    dod''4 re''16*2/3 la' la' la' la' la' |
    dod''4 re'' |
    re''8. mi''16 re''8 dod'' |
    re''4 }
>> r4 |
R2*7 |
r4 <>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { re''4~ |
    re''2~ |
    re''4 la'8 si' |
    do''8 re''16. mi''32 si'8 la' |
    sol'4 }
  { re''4~ |
    re''2~ |
    re''4 la'8 si' |
    do''8 re''16. mi''32 sol'8 fad' |
    sol'4 }
>>
<<
  \tag #'(oboe1 oboi) {
    <>^"Solo" \once\slurDashed re''8(-\sug\p \grace fa'' mib''16. re''32 |
    re''8 re'' sol''16 la''32 sib'' la''16 sol'' |
    sol''8 fad'') r4 |
    R2 |
    r4 \once\phrasingSlurDashed re''8\(-\sug\p \grace fa'' mib''16. re''32 |
    re''8 re'' sol''16 la''32 sib'' la''16 sol'' |
    sol''8( fad'')\) r4 |
    R2 |
    r4
  }
  \tag #'oboe2 { r4 | R2*7 | r4 }
>> \allowPageTurn
<>-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { mib''8 mib''16. mib''32 |
    mib''8 sib'16. sib'32 sib'8 sol'16. sol'32 |
    sol'8 mib' sol'' sol''16. sol''32 |
    sol''8 mib''16. mib''32 mib''8 sib'16. sib'32 |
    sib'8 sol' sib'' sib''16. sib''32 |
    sib''2~ |
    sib''~ |
    sib''~ |
    sib''8 }
  { sol'8 sol'16. sol'32 |
    sol'8 sol'16. sol'32 sol'8 mib'16. mib'32 |
    mib'8 mib' sib' sib'16. sib'32 |
    sib'8 sol'16. sol'32 sol'8 sol'16. sol'32 |
    sol'8 mib' sib' sib'16. sib'32 |
    sib'2~ |
    sib'~ |
    sib'~ |
    sib'8 }
>> r8 r4 |
<<
  \tag #'(oboe1 oboi) {
    R2 |
    r4^"Solo" \once\phrasingSlurDashed re''8\(-\sug\p \grace fa'' mib''16. re''32 |
    re''8 re'' sol''16 la''32 sib'' la''16 sol'' |
    sol''8( fad'')\) r4 |
    R2 |
  }
  \tag #'oboe2 { R2*5 | }
>>
r4 <>-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { mib''8 mib''16. mib''32 |
    mib''8 sib'16. sib'32 sib'8 sol'16. sol'32 |
    sol'8 mib' sol'' sol''16. sol''32 |
    sol''8 mib''16. mib''32 mib''8 sib'16. sib'32 |
    sib'8 sol' sib''8 sib''16. sib''32 |
    sib''2~ |
    sib''~ |
    sib''~ |
    sib''8 }
  { sol'8 sol'16. sol'32 |
    sol'8 sol'16. sol'32 sol'8 mib'16. mib'32 |
    mib'8 mib' sib' sib'16. sib'32 |
    sib'8 sol'16. sol'32 sol'8 sol'16. sol'32 |
    sol'8 mib' sib' sib'16. sib'32 |
    sib'2~ |
    sib'~ |
    sib'~ |
    sib'8 }
>> r8 r4 |
<<
  \tag #'(oboe1 oboi) {
    R2 |
    r4^"[Solo]" \once\phrasingSlurDashed re''8\(-\sug\p \grace fa'' mib''16. re''32 |
    re''8 re'' sol''16 la''32 sib'' la''16 sol''\) |
    sol''8( fad'') r4 |
    R2*6 |
  }
  \tag #'oboe2 { R2*10 }
>>
R2*4 |
r4 <>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { re''8. sol''16 |
    fad''8 sol'' mi''8. la''16 |
    sold''8 la'' mi'' mi''16. mi''32 |
    fad''2 |
    sol'' |
    fad'' |
    sol''8 s mi'' s |
    re'' s fad'' s |
    sol''4 }
  { sol'8. re''16 |
    la'8 si' do''8. mi''16 |
    re''8 do'' do'' do''16. do''32 |
    la'2 |
    si' |
    la' |
    si'8 s do'' s |
    si' s la' s |
    si'4 }
  { s4 s2*5 | s8 r s r | s r s r | }
>> r4 |
R2*3 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''8 la'' si'' la'' |
    sol'' la'' si'' la'' |
    sol''2:8 |
    sol''4 }
  { si'8 re'' sol'' re'' |
    si' re'' sol'' re'' |
    si'2:8 |
    si'4 }
>> r4 r |
R2.*2 |
