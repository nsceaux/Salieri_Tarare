\clef "bass" la,4 la\p la la |
sold sold sold sold |
la la,\f la, la, |
re re8 re re4 red |
mi r r2 |
mi8\ff mi mi mi mi mi mi mi |
mi2:8 mi:8 |
la8 la, la, la, la,2:8 |
re4 re8 re re4 re8 re |
mi2:8 mi:8 |
la,4 r r2 |
