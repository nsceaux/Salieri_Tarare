\clef "treble" <mi' dod'' la''>8 <dod'' mi'>4\p q q q8 |
<<
  \tag #'violino1 {
    mi''8 mi''4 mi'' mi'' mi''8 |
    dod'' dod''4\f mi'' la'' la''8 |
  }
  \tag #'violino2 {
    si'8 si'4 si' si' si'8 |
    mi' <mi' dod''>4-\sug\f q q q8 |
  }
>>
<re' la' fad''>4 <<
  \tag #'violino1 { fad''8 fad'' fad''4 sold''8 la'' | sold''4 }
  \tag #'violino2 { la'8 la' la'4 fad' | si }
>> r4 r2 |
r8 <<
  { si'4 si' si' si'8 | si' } \\
  { sold'4\ff sold' sold' sold'8 | sold' }
>>
<<
  \tag #'violino1 {
    <sold' mi''>4 q q q8 |
    mi'' dod''4 mi'' la'' la''8 |
  }
  \tag #'violino2 {
    <<
      { si'4 si' si' si'8 |
        dod'' dod''4 dod'' dod'' dod''8 | } \\
      { sold'4 sold' sold' sold'8 |
        la' mi'4 mi' mi' mi'8 | }
    >>
  }
>>
<re' la' fad''>4 <<
  \tag #'violino1 {
    fad''8 fad'' fad''4 sold''8 la'' |
    mi''16
  }
  \tag #'violino2 {
    la'8 la' la'4 sold'8 fad' |
    mi'16
  }
>> <dod'' la''>16 q q q4:16 <si' sold''>2:16 |
<la' la''>4 r r2 |
