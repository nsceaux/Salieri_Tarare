\clef "alto" la4 la'-\sug\p la' la' |
sold' sold' sold' sold' |
la' la\f la la |
re' re'8 re' re'4 red' |
mi' r r2 |
mi'8\ff mi' mi' mi' mi' mi' mi' mi' |
mi'2:8 mi':8 |
la'8 la la la la2:8 |
re'4 re'8 re' re'4 re'8 re' |
mi'2:8 mi':8 |
la4 r r2 |
