<<
  \tag #'(recit basse) {
    \clef "alto/G_8" <>^\markup\character Calpigi
    r4 dod'8 dod' dod'4 dod'8 dod' |
    mi'4 mi'8 mi' mi'4 mi'8 mi' |
    dod'4 la r mi' |
    fad' fad'8 fad' fad'4 sold'8 la' |
    sold'4 mi'8
    \ffclef "tenor/G_8" <>^\markup\character Tarare
    si8 mi'8. si16 si si si si |
    sold4
  }
  \tag #'vdessus { \clef "soprano/treble" R1*5 r4 }
  \tag #'vhaute-contre { \clef "alto/G_8" R1*5 r4 }
  \tag #'vtaille { \clef "tenor/G_8" R1*5 r4 }
  \tag #'vbasse { \clef "bass" R1*5 r4 }
>>
<<
  \tag #'recit { r4 r2 | R1*5 }
  \tag #'(vdessus basse) {
    \tag #'basse \ffclef "soprano/treble" <>^\markup\character Chœur
    si'8 si' si'4 si'8 si' |
    mi''4 mi''8 mi'' mi''4 mi''8 mi'' |
    dod''4 dod'' r dod'' |
    fad''4 fad''8 fad'' fad''4 sold''8 fad'' |
    mi''1 |
    la'4 r r2 |
  }
  \tag #'vhaute-contre {
    si8 si si4 si8 si |
    sold'4 sold'8 sold' sold'4 sold'8 sold' |
    la'4 la' r la' |
    la' la'8 la' la'4 sold'8 fad'? |
    mi'1 |
    la4 r r2 |
  }
  \tag #'vtaille {
    sold8 sold sold4 sold8 sold |
    si4 si8 si si4 si8 si |
    dod'?4 dod' r mi' |
    fad'? re'8 re' re'4 re'8 re' |
    mi'1 |
    la4 r r2 |
  }
  \tag #'vbasse {
    mi'8 mi' mi'4 mi'8 mi' |
    mi4 mi8 mi mi4 mi8 mi |
    la4 la r la |
    re' re'8 re' re'4 re'8 re' |
    mi'1 |
    la4 r r2 |
  }
>>