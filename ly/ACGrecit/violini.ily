\clef "treble" r4 r2 |
R1*2 |
r2 <la fad'>4 r |
R1*2 |
<si sol'>4 r r2 |
<<
  \tag #'violino1 {
    si'4 r r2 |
    do''4 r r2 |
    R1 |
    la'4 r r2 |
  }
  \tag #'violino2 {
    re'4 r r2 |
    sol'4 r r2 |
    R1 |
    do'4 r r2 |
  }
>>
fa'4\f la'8. la'16 <<
  \tag #'violino1 {
    re''4~ re''16. re''32 \grace mi''8 re''16 dod''32 re'' |
    mi''1~ |
    mi'' |
    do''4 r r2 |
    R1*2 |
    r8. la'16\p la'8. la'16 la'8. la'16 la'8. la'16 |
    dod''8. dod''16 dod''8. dod''16 dod''8. dod''16 dod''8. dod''16 |
    re''8. re''16 re''8. re''16 re''8. re''16 fa''8. fa''16 |
    mi''8. do''16 do''8. do''16 si'8. si'16 si'8. si'16 |
    la'4. la'8\f sol'!4 fa' |
    do''1\p~ |
    do''~ |
    do''~ |
    do''2~ do''4 r8 la'\f |
    la'4 dod'' re''2~ |
    << { s2. <>\p } re''1~ >> |
    re'' |
    do'' |
    si'4 r
  }
  \tag #'violino2 {
    re''8. la'16 la'8. la'16 |
    sold'1~ |
    sold' |
    mi'4 r r2 |
    R1*2 |
    r8. mi'16-\sug\p mi'8. mi'16 mi'8. mi'16 mi'8. mi'16 |
    la'8. la'16 la'8. la'16 la'8. la'16 la'8. la'16 |
    la'8. la'16 la'8. la'16 la'8. la'16 la'8. re''16 |
    do''8. la'16 la'8. la'16 sold'8. sold'16 sold'8. sold'16 |
    la'4. la'8\f sol'!4 fa' |
    sol'1-\sug\p~ |
    sol'( |
    la')~ |
    la'2~ la'4 r8 do'\f |
    do'4 sol' fad'2~ |
    << { s2. <>\p } fad'1~ >> |
    fad'~ |
    fad' |
    sol'4 r
  }
>>
