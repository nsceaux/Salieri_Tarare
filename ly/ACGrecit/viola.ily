\clef "alto" r4 r2 |
R1*2 |
r2 re'4 r |
R1*2 |
sol'4 r r2 |
fa'!4 r r2 |
mi'4 r r2 |
R1 |
fa'4 r r2 |
fa4-\sug\f la8. la16 re'8. la16 la8. la16 |
si1~ |
si |
la4 r r2 |
R1*2 |
r8. do'16-\sug\p do'8. do'16 do'8. do'16 do'8. do'16 |
mi'8. mi'16 mi'8. mi'16 mi'8. mi'16 mi'8. mi'16 |
re'8. re'16 re'8. re'16 re'8. re'16 re'8. re'16 |
mi'4 mi' mi mi |
la4. la8-\sug\f sol!4 fa |
mi1-\sug\p~ |
mi( |
fa)~ |
fa2~ fa4 r |
la-\sug\f la la2~ |
<< { s2. <>\p } la1~ >> |
la~ |
la |
sol4 r
