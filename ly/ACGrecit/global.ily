\key do \major \midiTempo#100
\time 4/4 \partial 2. s2. s1*10
\tempo "Andante Maestoso" s1*6
\tempo "Andante con Moto" s1*13 s2 \bar "|."
