\tag #'(recit0 basse) {
  Que sont ces deux su -- per -- bes om -- bres,
  qui sem -- blent me -- na -- cer, ta -- ci -- tur -- nes et som -- bres ?
  
  Rien : mais di -- tes un mot ; as -- si -- gnant leur é -- tat,
  je fais un roi de l’u -- ne et de l’autre un sol -- dat.
  
  Per -- met -- tez ; ce grand choix les tou -- che -- ra peut- ê -- tre.
  
  J’en dou -- te.
  
  Un de vous deux est roi : le -- quel veut l’ê -- tre ?
  
  Roi ?
}
\tag #'(recit1 basse) {
  Roi ?
}
Je ne m’y sens au -- cun em -- pres -- se -- ment.
\tag #'(recit0 basse) {
  En -- fants, il vous man -- que de naî -- tre,
  pour pen -- ser bien dif -- fé -- rem -- ment.
  
  Mon œil, en -- tr’eux, cherche un roi pré -- fé -- ra -- ble ;
  mais que je crains mon ju -- ge -- ment !
  Na -- tu -- re, l’er -- reur d’un mo -- ment
  peut rendre un siè -- cle mi -- sé -- ra -- ble.
}
