\clef "bass" r4 r2 |
R1*2 |
r2 re4 r |
R1*2 |
sol4 r r2 |
fa!4 r r2 |
mi4 r r2 |
R1 |
fa4 r r2 |
fa2 r8. fa16\f fa8. fa16 |
mi1~ |
mi |
la,4 r r2 |
R1*2 |
la,4\p la la la |
sol! sol sol sol |
fa fa fa re |
mi mi mi mi, |
la,4. la8\f sol!4 fa |
mi1\p~ |
mi( |
fa)~ |
fa2~ fa4 r |
fa\f mi re2~ |
<< { s2. <>\p } re1~ >> |
re1~ |
re |
sol4 r
