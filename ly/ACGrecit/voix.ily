<<
  \tag #'(recit0 basse) {
    \clef "bass" <>^\markup\character-text Le Génie parlé
    r8 mi la4. la8 |
    la la la si sol sol r sol |
    sol sol fad sol mi4 mi8 mi |
    la4 la8 mi fad fad r4 |
    \ffclef "soprano/treble" <>^\markup\character La Nature
    re''4 r8 la' la'4 sol'8 la' |
    fad'4 r8 la'16 si' do''4 do''8 re'' |
    si'4 r8 sol' sol' sol' sol' la' |
    si' si' r re''16 re'' si'4 si'8 do'' |
    sol'4
    \ffclef "bass" <>^\markup\character Le Génie
    r8 sol16 sol mi4 r8 sol16 sol |
    do'4. sol8 sol la sib do' |
    la la
    \ffclef "soprano/treble" <>^\markup\character La Nature
    r8 do'' fa'4 fa'8 r |
    R1 |
    \ffclef "bass" <>^\markup\character-text Le Génie très imposant
    r2 r8 si si si |
    mi'4. si8 sold4 r16 si mi' si |
    do'8 do' r4
    \ffclef "bass" <>^\markup\character L'Ombre d'Arar
    la2 <<
      \tag #'recit0 { r
        2 r8 <>^\markup\italic Ensemble et sans couleur do' do' do' |
        la4. la8 la la sold la |
        mi4
      }
      \tag #'basse { s1*2 s4 }
    >> r4 r
    \ffclef "soprano/treble" <>^\markup\character La Nature
    la' |
    dod''4 dod''8. dod''16 mi''4 dod''8. la'16 |
    re''4 re'' r re''8. fa''16 |
    mi''2 mi''8. mi''16 mi''8. mi''16 |
    la'2 r |
    \ffclef "bass" <>^\markup\character-text Le Génie examine les deux ombres
    r4 r8 sol do'4 r8 do' |
    sol4 sol8 la sib4 sib8 do' |
    la la do'2 do'8 do' |
    la la sib do' fa4 r |
    r2 r4 <>^\markup\italic { ton réfléchi } r8 la |
    re'2 re'4 r8 la |
    la4 sol8 la fad4 r8 la |
    do'4. mi'!8 do' do' do' si! |
    sol sol r4
  }
  \tag #'(recit1 basse) {
    <<
      \tag #'recit1 { \clef "tenor/G_8" r4 r2 | R1*14 | }
      \tag #'basse { s2. s1*14 \ffclef "tenor/G_8" }
    >> <>^\markup\character L'Ombre de Tarare
    do'2 r8 mi' mi' mi' |
    do'4. do'8 do' do' si do' |
    la4
    \tag #'recit1 { r4 r2 | R1*12 | r2 }
  }
>>
