\clef "treble" r4 r2 |
R1*2 |
r2 fad'4 r |
R1*2 |
sol'4 r r2 |
si'4 r r2 |
<sol' do''>4 r r2 |
R1 |
la'4 r r2 |
fa'4\f la'8. la'16 re''4~ re''16. re''32 \grace mi''8 re''16 dod''32 re'' |
<sold' mi''>1~ |
q |
<mi' do''>4 r r2 |
R1*2 |
r8. <do' mi' la'>16\p q8. q16 q8. q16 q8. q16 |
<mi' la' dod''>8. q16 q8. q16 q8. q16 q8. q16 |
<la' re''>8. q16 q8. q16 q8. q16 <la' fa''>8. <re'' fa''>16 |
<do'' mi''>8. <la' do''>16 q8. q16 <sold' si'>8. q16 q8. q16 |
la'4. la'8\f sol'!4 fa' |
<sol' do''>1\p~ |
q |
<la' do''>~ |
q2~ q4 r8 <do' la'>\f |
<do' la'>4 <sol' dod''> <fad' re''>2~ |
<< { s2. <>\p } q1~ >> |
q |
<fad' do''> |
<si' sol'>4 r
