\score {
  <<
    \new ChoirStaff <<
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'recit1 \includeNotes "voix"
      >> \keepWithTag #'recit1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'recit0 \includeNotes "voix"
      >> \keepWithTag #'recit0 \includeLyrics "paroles"
    >>
    \new PianoStaff <<
      \new Staff = "dessus" << \global \includeNotes "reduction-dessus" >>
      \new Staff = "basse" << \global \includeNotes "reduction-basse" >>
    >>
  >>
  \layout { }
  \midi { }
}
