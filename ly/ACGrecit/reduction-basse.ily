\clef "bass" r4 r2 |
R1*2 |
r2 <re la>4 r |
R1*2 |
<sol si>4 r r2 |
<fa! re'>4 r r2 |
mi4 r r2 |
R1 |
<fa do'>4 r r2 |
fa4 la8. la16 re'8. <la fa>16 q8. q16 |
<mi si>1~ |
q |
la,4 r r2 |
R1*2 |
la,4 la la la |
sol! sol sol sol |
fa fa fa re |
mi mi mi mi, |
la,4. la8 sol!4 fa |
mi1~ |
mi |
fa~ |
fa2~ fa4 r |
<fa la> <mi la> <re la>2~ |
q1~ |
q |
q |
sol4 r
