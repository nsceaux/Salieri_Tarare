\clef "tenor"
<>-\sug\f \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { mi'1~ | mi'~ | mi'4 }
  { sol1~ | sol~ | sol4 }
>> r4 r2 |
R1 |
<>-\sug\ff \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { mi'1~ | mi'~ | mi'4 }
  { do'1~ | do'~ | do'4 }
>> r4 r2 |
R1 |
\tag #'fagotti <>^"unis" re'2-\sug\ff~ re'8 fad la re' |
fad'2~ fad'8 la re' fad' |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { re'4. fad'8 fad'4. re'8 |
    re'4. la8 la4. fad8 | }
  { fad4. la8 la4. fad8 |
    fad4. fad8 fad4. re8 | }
>>
\tag #'fagotti <>^"unis" re4 re8. re16 re4 re |
re2 r |
\clef "bass" do'1-\sug\p |
sib |
la2 sol |
fad1 |
sol-\sug\f |
sol2.\fermata r4 |
do'2-\sug\ff do'4 do'8. do'16 |
do'1 |
do'4 do' do' do'8 do' |
do'4 dod'2\fermata la4 |
re'2 re'4. re'8 |
re'2~ re'8 re' re' re' |
re'4 re' re' re' |
si2.\fermata sol8. sol16 |
mi'4. mi'8 mi'4 fad' |
sol'1\p\< |
do'-\sug\ff |
do2 do |
do do8 re16 mi fa sol la si |
do'4. do8 do4 do |
do1 |
