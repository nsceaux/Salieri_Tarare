De ce grand bruit, de cet é -- clat,
ô ciel ! ap -- prends- nous le mys -- tè -- re !


Mor -- tel, qui que tu sois, prin -- ce, brame ou sol -- dat ;
hom -- me ! ta gran -- deur sur la ter -- re,
n’ap -- par -- tient point à ton é -- tat ;
elle est toute à ton ca -- rac -- tè -- re.
