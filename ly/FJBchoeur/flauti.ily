\clef "treble"
<>-\sug\f \twoVoices#'(flauto1 flauto2 flauti) <<
  { do'''1~ | do'''~ | do'''4 }
  { mi''1~ | mi''~ | mi''4 }
>> r4 r2 |
R1 |
<>-\sug\ff \twoVoices#'(flauto1 flauto2 flauti) <<
  { do'''1~ | do'''~ | do'''4 }
  { mi''1~ | mi''~ | mi''4 }
>> r4 r2 |
R1 |
<>-\sug\ff \twoVoices#'(flauto1 flauto2 flauti) <<
  { re'''1~ |
    re'''~ |
    re'''~ |
    re'''~ |
    re'''~ |
    re''' |
    mib''' |
    re''' |
    do'''2 sib'' |
    la''1 |
    si''! | }
  { fad''~ |
    fad''~ |
    fad''~ |
    fad''~ |
    fad''~ |
    fad'' |
    fad'' |
    sol'' |
    la''2 sol'' |
    fad''1 |
    sol'' | }
  { s1*5 | s-\sug\p | s1-\sug\f }
>>
r2\fermata r4 r |
R1*7 |
r2\fermata r4 \tag #'flauti <>^"unis" sol''8. sol''16 |
mi'''4. mi'''8 mi'''4 fad''' |
sol'''1-\sug\p -\sug\cresc |
sol''16-\sug\ff do''' si'' la'' sol'' do''' si'' la'' sol'' do''' si'' la'' sol'' do''' si'' la'' |
sol'' do''' si'' la'' sol'' fa'' mi'' re'' do''4 r |
r2 do''8 re''16 mi'' fa'' sol'' la'' si'' do'''4. do''8 do''4 do'' |
do''1 |
