\clef "treble" \transposition re
\tag #'corni <>^"[unis]" re''1-\sug\f ~ |
re''~ |
re''4 r r2 |
R1 |
<>-\sug\ff \twoVoices#'(corno1 corno2 corni) <<
  { re''1~ | re''~ | re''4 }
  { sol'1~ | sol'~ | sol'4 }
>> r4 r2 |
R1 |
\tag #'corni <>^"unis" do''4.-\sug\ff sol8 do' mi' sol' do'' |
mi''4. do'8 \twoVoices#'(corno1 corno2 corni) <<
  { mi'8 sol' do'' mi'' |
    sol''4. mi''8 mi''4. do''8 |
    do''4. sol'8 sol'4. mi'8 | }
  { mi'8 sol do' mi' |
    sol'4. sol'8 sol'4. mi'8 |
    mi'4. mi'8 mi'4. do'8 | }
>>
\tag #'corni <>^"unis" do'4 do'8. do'16 do'4 do' |
do'2 r |
R1*5 |
r2\fermata r4 r | \allowPageTurn
\transposition do
<>^"Cors en Ut" <>-\sug\ff \twoVoices#'(corno1 corno2 corni) <<
  { do''2 do''4 do''8. do''16 |
    do''1 |
    do''4 do'' do'' do''8 do'' |
    do''4 }
  { do'2 do'4 do'8. do'16 |
    do'1 |
    do'4 do' do' do'8 do' |
    do'4 }
>> r4\fermata r2 |
\tag #'corni <>^"unis" re''2 re''4. re''8 |
re''2~ re''8 re'' re'' re'' |
re''4 re'' re'' re'' |
sol'2.\fermata sol'8. sol'16 |
mi''4. mi''8 mi''4 \twoVoices#'(corno1 corno2 corni) <<
  { fad''4 |
    sol''1 |
    mi''1 |
    mi''2 mi'' |
    do'' do'' |
    do''4. do'8 do'4 do' | }
  { r4 |
    sol'1 |
    sol' |
    sol'2 sol' |
    mi' mi' |
    mi'4. do'8 do'4 do' | }
  { s4 | s1-\sug\p -\sug\cresc | s1-\sug\ff }
>>
do'1 |
