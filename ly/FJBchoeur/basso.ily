\clef "bass"
<<
  \ru#4 << { do2:16 do:16 | } \\ { do2:8 do:8 } >>
  { s1*2-\sug\f s\p }
>>
<<
  \ru#4 << { la,2:16 la,:16 | } \\ { la,:8 la,:8 } >>
  { s1*2\ff s\p }
>>
<<
  \ru#5 << { re2:16 re:16 | } \\ { re:8 re:8 } >>
  { s1\ff }
>>
re8\p r re r re r re r |
re4 re re re |
re re re re |
re re re re |
re re re re |
sol\f sol, sol, sol, |
sol,2.\fermata r4 |
r8 do(\ff sol, do) mi( sol do' sol) |
r do( la, do) fa( la do' la) |
r do( la, do) fa( do' la fa) |
mi4 dod2\fermata r4 |
r8 re( la, re) fad( la re' la) |
r8 re( si, re) sol( si re' si) |
r do'( la fad) re( fad la fad,) |
sol,2.\fermata sol,8. sol,16 |
mi4. mi8 mi4 fad8. fad16 |
sol2:32\p\cresc sol:32 |
do4\ff do do do |
do2 do |
do do8 re16 mi fa sol la si |
do'4. do8 do4 do |
do1 |
