<<
  \tag #'(vdessus basse) {
    \clef "soprano/treble" <>^"Chanté" R1*13 |
    r2 r8 re'' re'' re'' |
    mib''4. mib''8 mib''4. mib''8 |
    re''2. re''4 |
    fad''2 sol''4 sol'' |
    la''2 re''4. do''8 |
    si'!1 |
    si'2.\fermata \ffclef "soprano/treble" <>^\markup\character La Nature sol'4 |
    do''2 do''4 do''8 do'' |
    do''1 |
    do''4 do'' do'' do''8 do'' |
    do''4 dod''4*1/2 s8\fermata dod''8 r la' la' |
    re''2 re''4. re''8 |
    re''2 re''8 re'' re'' re'' |
    re''4 re'' re'' re'' |
    si'2.\fermata sol'8. sol'16 |
    mi''4. mi''8 mi''4 fad''8. fad''16 |
    sol''1 |
    do''4 r r2 |
    R1*4 |
  }
  \tag #'vhaute-contre {
    \clef "alto/G_8" R1*13 |
    r2 r8 fad' fad' fad' |
    la'4. la'8 la'4. la'8 |
    re'2. re'4 |
    re'2 re'4 re' |
    re'2 re'4. re'8 |
    sol'1 |
    sol'2.\fermata r4 |
    R1*15 |
  }
  \tag #'vtaille {
    \clef "tenor/G_8" R1*13 |
    r2 r8 la la la |
    fad'4. fad'8 fad'4. fad'8 |
    sol'2. re'4 |
    do'2 sib4 sib |
    la2 la4. re'8 |
    re'1 |
    re'2.\fermata r4 |
    R1*15 |
  }
  \tag #'vbasse {
    \clef "bass" R1*13 |
    r2 r8 re' re' re' |
    do'4. do'8 do'4. do'8 |
    sib2. sib4 |
    la2 sol4 sol |
    fad2 fad4. fad8 |
    sol1 |
    sol2.\fermata \ffclef "bass" <>^\markup\character Le Génie du Feu sol4 |
    do'2 do'4 do'8 do' |
    do'1 |
    do'4 do' do' do'8 do' |
    do'4 dod'4*1/2 s8^\markup\musicglyph#"scripts.ufermata" dod'8 r la la |
    re'2 re'4. re'8 |
    re'2 re'8 re' re' re' |
    re'4 re' re' re' |
    si2.\fermata sol8. sol16 |
    mi'4. mi'8 mi'4 fad8. fad16 |
    sol1 |
    do'4 r r2 |
    R1*4 |
  }
>>
