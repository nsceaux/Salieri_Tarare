\tag #'all \key do \major
\tempo "Allegro" \midiTempo#120
\time 4/4 \once\override Staff.TimeSignature.stencil = ##f s1*13 s4
\tempo "Andante con moto" s2. s1*6
\tempo "Lent et Majestueux" s1*10
\tempo "Allegro" s1*5 \bar "|."
