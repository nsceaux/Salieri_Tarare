\score {
  <<
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'vdessus \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'vdessus \includeLyrics "paroles" }
    \new StaffGroup <<
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \small en Ut }
        shortInstrumentName = \markup Tr.
      } << \keepWithTag #'() \global \keepWithTag #'trombe \includeNotes "trombe" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small en Ré }
        shortInstrumentName = \markup Cor.
      } << \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni" >>
      \new Staff \with { \timpaniInstr } <<
        \keepWithTag #'() \global \includeNotes "timpani"
      >>
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 10\mm
  }
}
