\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \flautiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'flauti \includeNotes "flauti"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Hautbois et Clarinettes }
        shortInstrumentName = \markup\center-column { Htb. Cl. }
      } << \global \keepWithTag #'oboi \includeNotes "oboi" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small en Ré }
        shortInstrumentName = \markup Cor.
      } << \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \small en Ut }
        shortInstrumentName = \markup Tr.
      } << \keepWithTag #'() \global \keepWithTag #'trombe \includeNotes "trombe" >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with { \timpaniInstr } <<
        \keepWithTag #'() \global \includeNotes "timpani"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      instrumentName = \markup\center-column\smallCaps { Chœur Général }
      \haraKiri
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \keepWithTag #'basso \includeNotes "basso"
      \origLayout {
        s1*5\pageBreak
        s1*8\pageBreak
        s1*6\pageBreak
        s1*6\pageBreak
        s1*5\break
      }
      \modVersion { s1*19 s2. \bar "" \break }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
