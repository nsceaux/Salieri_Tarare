\clef "alto" <sol mi' do''>16 <sol' mi''>[\f q q] q4:16 q2:16 |
q:16 q:16 |
q:16\p q:16 |
q:16 q:16 |
la16-\sug\ff la' la' la' la'4:16 la'2:16 |
la':16 la':16 |
la':16-\sug\p la':16 |
la':16 la':16 |
<fad' la'>:16-\sug\ff q:16 |
q:16 q:16 |
q:16 q:16 |
q:16 q:16 |
q:16 q:16 |
q4 re'-\sug\p re' re' |
do'1 |
sib |
la2 sol |
fad1 |
sol4 <<
  { re'4 re' re' re'2.\fermata } \\
  { si!4\f si si | si2. }
>> r4 |
r8 do'(\ff sol do') mi'( sol' do'' sol') |
r do'( la do') fa'( la' do'' la') |
r do'( la do') fa'( do'' la' fa') |
mi'4 dod'2\fermata r4 |
r8 re'( la re') fad'( la' re'' la') |
r8 re'( si re') sol'( si' re'' si') |
r do''( la' fad') re'( fad' la' fad) |
sol2.\fermata sol8. sol16 |
mi'4. mi'8 mi'4 fad'8. fad'16 |
sol'2:32\p\cresc sol':32 |
<sol mi'>8\ff q4 q q q8 |
q16 do''[ si' la'] sol' fa' mi' re' do'8 si16 la sol fa mi re |
do8 re16 mi fa sol la si do'8 re'16 mi' fa' sol' la' si' |
do''4. do8 do4 do |
do1 |
