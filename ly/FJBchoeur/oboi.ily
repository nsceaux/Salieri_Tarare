\clef "treble"
<>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { do'''1~ | do'''~ | do'''4. }
  { mi''1~ | mi''~ | mi''4. }
>> \tag #'oboi <>^"unis" do'8\ff do'4. mi'8 |
mi'4. sol'8 sol'4. do''8 |
mi''4-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { do'''2 do'''4~ | do'''1~ | do'''4. }
  { mi''2 mi''4~ | mi''1~ | mi''4. }
>> \tag #'oboi <>^"unis" do'8\ff do'4. mi'8 |
mi'4. do'8 do'4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''4 | }
  { do''4 }
>>
\tag #'oboi <>^"unis" re''4. la'8 re' fad' la' re'' |
fad''4. re'8 fad' la' re'' fad'' |
la''4. \twoVoices#'(oboe1 oboe2 oboi) <<
  { fad''8 fad''4. re''8 |
    re''4. la'8 la'4. fad'8 | }
  { la'8 la'4. fad'8 |
    fad'4. fad'8 fad'4. re'8 | }
>>
\tag #'oboi <>^"unis" re'4 re'8. re'16 re'4 re' |
re'4
\twoVoices#'(oboe1 oboe2 oboi) <<
  { re''2 re''4 |
    mib''1 |
    re'' |
    fad''2 sol'' |
    la'' re''4. do''8 |
    si'!1~ |
    si'2.\fermata }
  { fad'2 fad'4 |
    fad'1 |
    sol' |
    do''2 sib' |
    la'1 |
    sol'~ |
    sol'2.\fermata }
  { s2.-\sug\p | s1*4 | s1-\sug\f }
>> r4 |
\tag #'oboi <>^"unis" do''2-\sug\ff do''4 do''8. do''16 |
do''1 |
do''4 do'' do'' do''8 do'' |
do''4 dod''2\fermata la'4 |
re''2 re''4. re''8 |
re''2~ re''8 re'' re'' re'' |
re''4 re'' re'' re'' |
si'2.\fermata sol'8. sol'16 |
mi''4. mi''8 mi''4 fad'' |
sol''1\p\cresc |
<>-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''4 sol'' sol'' sol'' |
    sol''2 sol'' |
    sol'' sol'' |
    sol''4. }
  { mi''4 mi'' mi'' mi'' |
    mi''2 mi'' |
    mi'' mi'' |
    mi''4. }
>> do'8 do'4 do' |
do'1 |
