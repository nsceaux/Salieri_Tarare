\clef "treble" do''16 <mi'' do'''>[\f q q] q4:16 q2:16 |
q:16 q:16 |
<<
  \tag #'violino1 {
    q:16\p q:16 |
    q:16 q:16 |
  }
  \tag #'violino2 {
    <mi'' do'''>4. do'8\ff do'4. mi'8 |
    mi'4. sol'8 sol'4. do''8 |
  }
>> |
<mi'' do'''>2:16\ff q:16 |
q:16 q:16 |
<<
  \tag #'violino1 {
    q:16\p q:16 |
    q:16 q:16 |
    la''16\ff <fad'' re'''> q q q4:16 q2:16 |
    q:16 q:16 |
    q:16 q:16 |
    q:16 q:16 |
    q:16 q:16 |
    q8 re'' r re''\p r re'' r re'' |
    r mib'' r mib'' r mib'' r mib'' |
    r re'' r re'' r re'' r re'' |
    r fad'' r fad'' r sol'' r sol'' |
    r la'' r la'' r re'' r do'' |
    si'!\f
  }
  \tag #'violino2 {
    q4. do'8\ff do'4. mi'8 |
    mi'4. do'8 do'4 <do'' mi''> |
    <re' la' fad''>4.-\sug\ff la8 re' fad' la' re'' |
    fad''4. re'8 fad' la' re'' fad'' |
    la''4. <fad'' la'>8 q4. <fad' re''>8 |
    q4. <fad' la'>8 q4. fad'8 |
    <<
      { re'4 re'8. re'16 re'4 re' } \\
      { re'4 re'8. re'16 re'4 re' }
    >>
    re'8 fad' r <fad' la'>-\sug\p r q r q |
    r q r q r q r q |
    r <sol' sib'> r q r q r q |
    r <la' do''> r q r <sol' sib'> r q |
    r <fad' la'> r q r q r q |
    sol'8-\sug\f
  }
>> si''[ sol'' re''] si' sol' re' si |
sol2.\fermata r4 |
r8 <>\ff \grace { re'16 do' si } do'8( sol do' mi' sol' do'' sol') |
r8 \grace { re'16 do' si } do'8( la do' fa' la' do'' la') |
r8 \grace { re'16 do' si } do'8( la do' fa' do'' la' fa') |
mi'4 dod''2\fermata r4 |
r8 \grace { mi'16 re' do' } re'8( la re' fad' la' re'' la') |
r8 \grace { mi'16 re' dod' } re'8( si re' sol' si' re'' si') |
r8 \grace { re''16 do'' si' } do''8( la' fad' re' fad' la' la') |
si2.\fermata sol'8. sol'16 |
mi''4. mi''8 mi''4 fad''8. fad''16 |
sol''2:32\p\cresc sol'':32 |
sol''16\ff do''' si'' la'' sol'' do''' si'' la'' sol'' do''' si'' la'' sol'' do''' si'' la'' |
sol'' do''' si'' la'' sol'' fa'' mi'' re'' do''8 si'16 la' sol' fa' mi' re' |
do'8 re'16 mi' fa' sol' la' si' do''8 re''16 mi'' fa'' sol'' la'' si'' |
do'''4. do'8 do'4 do' |
do'1 |
