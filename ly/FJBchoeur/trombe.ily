\clef "treble"
<>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''1~ | mi''~ | mi''4. }
  { sol'1~ | sol'~ | sol'4. }
>> \tag #'trombe <>^"unis" do'8-\sug\ff do'4. mi'8 |
mi'4. sol'8 sol'4. do''8 |
<>-\sug\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''1~ | do''~ | do''4. }
  { mi'1~ | mi'~ | mi'4. }
>> \tag #'trombe <>^"unis" do'8-\sug\ff do'4. mi'8 |
mi'4. do'8 do'4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''4 | }
  { do''4 }
>>
\tag #'trombe <>^"unis" re''2 r4 r8 re'' |
re''2 r4 r8 re'' |
re''2 r4 r8 re'' |
re''2 r4 r8 re'' |
re''4 re''8. re''16 re''4 re'' |
re''2 r |
R1*5 |
r2\fermata r4 r |
<>-\sug\ff \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''2 do''4 do''8. do''16 |
    do''1 |
    do''4 do'' do'' do''8 do'' |
    do''4 }
  { do'2 do'4 do'8. do'16 |
    do'1 |
    do'4 do' do' do'8 do' |
    do'4 }
>> r4\fermata r2 |
\tag #'trombe <>^"unis" re''2 re''4. re''8 |
re''2~ re''8 re'' re'' re'' |
re''4 re'' re'' re'' |
sol'2.\fermata sol'8. sol'16 |
mi''4. mi''8 mi''4 \twoVoices#'(tromba1 tromba2 trombe) <<
  { fad''4 |
    sol''1 |
    mi''1 |
    mi''2 mi'' |
    do'' do'' |
    do''4. do'8 do'4 do' | }
  { r4 |
    sol'1 |
    sol' |
    sol'2 sol' |
    mi' mi' |
    mi'4. do'8 do'4 do' | }
  { s4 | s1-\sug\p -\sug\cresc | s1-\sug\ff }
>>
do'1 |
