\clef "bass" do2:16-\sug\f do:16 |
do:16 do:16 |
do4 r r2 |
R1 |
do2:16-\sug\ff do:16 |
do:16 do:16 |
do4 r r2 |
R1 |
do2-\sug\ff r |
do r |
do2:16 do:16 |
do:16 do:16 |
do:16 do:16 |
do4 r r2 |
R1*5 |
r2^\fermata r4 r |
R1*7 |
r2\fermata r4 r |
R1 |
sol,2:32-\sug\p -\sug\cresc sol,:32 |
do4-\sug\ff do do do |
do2 do |
do do |
do4. do8 do4 do |
do1 |
