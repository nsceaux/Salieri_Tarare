\clef "bass" do4\p r sol, r |
lab, r fa, r |
sol, r si, r |
do r re r |
mib r r2 |
lab4 r lab r |
lab r lab r |
fa r fa r |
sol r mib r |
lab r lab r |
fa r fa r |
sol r sol r |
sol r sol r |
sol r sol r |
sol r sol r |
do r r2 |
r fad4 r |
R1 |
sol4 r sol r |
fa r r2 |
sold4 r r2 |
la4 r r2 |
r r4
