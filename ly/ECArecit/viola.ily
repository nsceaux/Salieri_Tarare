\clef "alto" do'4\p r sol r |
lab r fa r |
sol r si r |
do' r re' r |
mib' r r2 |
r8 lab' lab' lab' r lab' lab' lab' |
r lab' lab' lab' r lab' lab' lab' |
r fa' fa' fa' r fa' fa' fa' |
r mib' mib' mib' r mib' mib' mib' |
r mib' mib' mib' r mib' mib' mib' |
r fa' fa' fa' r fa' fa' fa' |
r sol' sol' sol' r sol' sol' sol' |
r sol' sol' sol' r sol' sol' sol' |
r sol' sol' sol' r sol' sol' sol' |
r sol' sol' sol' r sol' sol' sol' |
sol'4 r r2 |
r2 fad'4 r |
R1 |
sol'4 r sol' r |
fa' r r2 |
sold'4 r r2 |
la'4 r r2 |
r r4
