\clef "soprano/treble" <>^\markup\character-text Astasie parlé
mib''2 mib''4. re''8 |
do''4 do'' fa'' fa''8 re'' |
si'4 si' r re''8 re'' |
mib''4. mib''8 fa''4 lab'8 lab' |
sol' sol'
\ffclef "soprano/treble" <>^\markup\character-text Spinette \normal-text Récit
r8 sol'16 lab' sib'8 sib'16 sib' mib''8 sib' |
do''4 r
\ffclef "soprano/treble" <>^\markup\character Astasie
mib''8. mib''16 mib''8. mib''16 |
do''2 r4 do''8 do'' |
reb''4 reb'' r8 reb''! reb'' reb'' |
sib'4 sib' mib''4. mib''8 |
do''4 r do''8 do'' do'' do'' |
fa''2 fa''4. re''8 |
si'!4 si' r re''8 re'' |
si'2 si'8 si' do'' re'' |
sol'2 r4 si'8 do'' |
re''4 re''8 re'' re''4. fa''8 |
mib''4 do''
\ffclef "soprano/treble" <>^\markup\character-text Spinette \normal-text Récit
r4 r8 sol' |
do'' do''16 do'' do''8 re''16 mib'' re''8 re'' r re'' |
re'' la' r la' do'' do'' do'' re'' |
sib'4
\ffclef "soprano/treble" <>^\markup\character Astasie
r16 re'' re'' mi'' dod''8 dod'' dod'' re'' |
la'4 r8 la'16 la' re''4 mi''8 fa'' |
mi'' mi'' r si' re'' re'' re'' mi'' |
do'' do'' r la'16 si' do''8 do''16 do'' re''8 mi'' |
la'4 r r
