Ô ma com -- pa -- gne ! ô mon a -- mi -- e !
sau -- ve- moi de cette in -- fa -- mi -- e.

Eh ! com -- ment vous prou -- ver ma foi ?

Prends mes di -- a -- mants, ma pa -- ru -- re :
je te les donne, ils sont à toi.

Ah ! dans cette hor -- rible a -- ven -- tu -- re,
sois Ir -- za, re -- pré -- sen -- te- moi ;
on ré -- prime un mu -- et sans pei -- ne.

Si c’est Cal -- pi -- gi qui l’a -- mè -- ne,
ma -- da -- me, il me re -- con -- naî -- tra.

Ce long man -- teau te cou -- vri -- ra.
Sou -- viens- toi de Ta -- ra -- re, et nom -- me- le sans ces -- se ;
son nom seul te ga -- ran -- ti -- ra.
