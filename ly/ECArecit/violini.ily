\clef "treble"
<<
  \tag #'violino1 {
    r8 mib''\p mib'' mib'' r mib'' mib'' re'' |
    r do'' do'' do'' r fa'' fa'' re'' |
    r si' si' si' r re'' re'' re'' |
    r mib'' mib'' mib'' r fa'' fa'' lab' |
    sol'4 r r2 |
    r8 do'' do'' do'' r do'' do'' do'' |
    r do'' do'' do'' r do'' do'' do'' |
    r reb'' reb'' reb'' r reb'' reb'' reb'' |
    r sib' sib' sib' r sib' sib' sib' |
    r do'' do'' do'' r do'' do'' do'' |
    r do'' do'' do'' r re''! re'' re'' |
    r si'! si' si' r si' si' si' |
    r8 si' si' si' r si' si' si' |
    r si' si' si' r si' si' si' |
    r si' si' si' r si' si' si' |
    do''4 r r2 |
    r re''4 r |
    R1 |
    re''4 r la'' r |
    la'' r r2 |
    mi''!4 r r2 |
    mi''4 r r2 |
    r r4
  }
  \tag #'violino2 {
    r8 sol'-\sug\p sol' sol' r do'' do'' si' |
    r mib' mib' mib' r lab' lab' lab' |
    r re' re' re' r sol' sol' sol' |
    r sol' lab' lab' r lab' lab' fa' |
    mib'4 r r2 |
    r8 mib' mib' mib' r mib' mib' mib' |
    r mib' mib' mib' r mib' mib' mib' |
    r lab' lab' lab' r lab' lab' lab' |
    r sol' sol' sol' r sol' sol' sol' |
    r lab' lab' lab' r lab' lab' lab' |
    r lab' lab' lab' r lab' lab' lab' |
    r re' re' re' r re' re' re' |
    r re' re' re' r re' re' re' |
    r re' re' re' r re' re' re' |
    r re' re' re' r re' re' re' |
    mib'4 r r2 |
    r la'4 r |
    R1 |
    sib'4 r dod'' r |
    re'' r r2 |
    si'!4 r r2 |
    do''4 r r2 |
    r r4
  }
>>
