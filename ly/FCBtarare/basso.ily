\clef "bass" r2 |
fa,1\fp |
sib,2 sib,\mf |
sib,2~ sib,4 r |
sib,\p r r2 |
do4 r r2 |
fa,4 r r2 |
fa4 r r2 |
sib,2 re |
mib fa |
sol do\mf |
re2.\p sib,4 |
do2 do, |
fa,4 fa,8.\f fa,16 fa,4 r |
fa\p r r2 |
fa4 r r2 |
do4 r r2 |
mi2(\mf fa4) r |
fa r do-\sug\p r |
fa2 r |
fa-\sug\fp fa, |
sib,\fermata r |
mib r |
fa4 fa fa fa, |
sib, r sib, r |
sib, r sib, r |
sib, r sib, r |
fad r fad\mf r |
sol r do2\f\fermata |
fa-\sug\p r\fermata |
re2:8 re:8 |
mi:8 re:8\p |
dod:8 la,8 la, la, si, |
dod2:8\cresc dod:8 |
re:8\f sib,:8 |
la,2 r |
la4\mf r sold r |
la r la r |
sol! r do r |
fa2:8\f fa:8 |
fa1~ |
fa4 r r\fermata fa\pp |
mi2:8 mi2:8 |
mib!:8 mib:8 |
re:8 re:8 |
re:8 re:8 |
do:8\cresc do:8 |
do8\ff do' mib' re' do' sib la sol |
fa2\fermata r |
mi4\p r mi r |
fa r sib,8\ff-! sib,-! do-! re-! |
mib2 fa4. fa,8 |
sib,2 \once\slurDashed sib8( la16 sol fa mib re do) |
sib,2:8 sib,4:8 do:8 |
re:8 mib:8 fa:8 sol:8 |
lab8 fa re fa lab fa re fa |
lab4 sib,8 sib, sib,2:8 |
si,4. sol,8 sol,4 sol, |
sol,2 r |
