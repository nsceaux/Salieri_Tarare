\score {
  <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \includeNotes "voix"
    >> { \set fontSize = -2 \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiriFirst \consists "Metronome_mark_engraver" } <<
      \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
      \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
}
