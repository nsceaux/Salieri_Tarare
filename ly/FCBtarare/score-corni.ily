\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \includeNotes "voix"
    >> { \set fontSize = -2 \includeLyrics "paroles" }
    \new Staff \with {
      instrumentName = \markup\center-column { Cors \small in F }
      \haraKiri \consists "Metronome_mark_engraver" 
    } <<
      \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni"
    >>
  >>
  \layout {
    indent = \largeindent
    \context { \Score \remove "Metronome_mark_engraver" }
  }
}
