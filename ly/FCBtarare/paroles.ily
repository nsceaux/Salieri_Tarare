Je ne puis mou -- rir qu’u -- ne fois.
Qu’en je m’en -- ga -- geai sous tes lois,
A -- tar, je te don -- nai ma vi -- e ;
elle est toute en -- tière à mon roi,
elle est toute en -- tière à mon roi ;
au lieu de la per -- dre pour toi,
c’est par toi qu’el -- le m’est ra -- vi -- e.
J’ai rem -- pli mon sort, suis ton choix ;
je ne puis mou -- rir, je ne puis mou -- rir qu’u -- ne fois.
Mais sou -- hai -- te qu’un jour ton peu -- ple te par -- don -- ne.

U -- ne me -- na -- ce ?

Il s’en é -- ton -- ne !
Roi fé -- ro -- ce ! as- tu donc comp -- té,
par -- mi les droits de ta cou -- ron -- ne,
ce -- lui du crime et de l’im -- pu -- ni -- té ?
Ta fu -- reur ne peut se con -- train -- dre,
et tu veux n’ê -- tre pas ha -- ï !
Trem -- ble d’or -- don -- ner…

Qu’ai-je à crain -- dre ?

De te voir tou -- jours o -- bé -- i ;
jus -- qu’à l’ins -- tant où l’ef -- fray -- an -- te som -- me
de tes for -- faits dé -- chaî -- nant leur cour -- roux…
Tu pou -- vais tout contre un seul hom -- me ;
tu ne pour -- ras rien con -- tre tous.

Qu’on l’en -- tou -- re !
