\clef "treble" r2 |
R1*29 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''1 |
    sol'' |
    sol'' |
    sol'' |
    fa''2 sol'' |
    la'' }
  { \tieDashed la'1~ |
    la'2~ la'8 sib' sib' sib' | \tieSolid
    la'2 dod''4. re''8^\markup\whiteout\override #'(baseline-skip . 0)
    \general-align #X #-0.1 \center-column\tiny {
      \line { [source : \concat { \italic do ♯ ] } }
      ↓
    } |
    mi''1 |
    re''2 re'' |
    dod''2 }
  { s1-\sug\f | s2 s8 s4.-\sug\p | s1 | s-\sug\cresc | s1-\sug\f }
>> r2 |
R1*3 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { r16 fa'-\sug\f sol' la' sib' do'' re'' mi'' fa'' la' sib' do'' re'' mi'' fa'' sol'' |
    la''4 fa'' do'' la' |
    fa' }
  { r2 r16 fa'-\sug\f sol' la' sib' do'' re'' mi'' |
    fa''4 do'' la' fa' |
    fa' }
>> r4 r2\fermata |
R1*5 |
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { do'''1 | do'''2\fermata }
  { mib''1 | mib''2\fermata }
>> r2 |
R1 |
r2\fermata \tag #'oboi <>^"unis" re''!8-!-\sug\ff re''-! mib''-! fa''-! |
sol''2 fa''4. fa''8 |
\once\slurDashed sib''8( la''16 sol'' fa'' mib'' re'' do'') sib'4 r |
R1*3 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { lab''2 lab''4 |
    sol''4. si''8 si''4. si''8 |
    si''2 }
  { re''2 re''4 |
    re''4. re''8 re''4. re''8 |
    re''2 }
>> r2 |
