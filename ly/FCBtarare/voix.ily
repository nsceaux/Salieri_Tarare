\clef "tenor/G_8" <>^\markup\character-text Tarare Fièrement ^"Chanté"
la8 sib do' re' |
mib'2 do'4. fa'8 |
sib4 r r2 |
r r4 sib8 do' |
re'4. mib'8 fa'4 re'8 sib |
la2 r4 r8 la |
do'2 r8 la do' la |
fa4. do'8 fa'4.( mib'8) |
re'2 r4 r8 sib |
sol4 sol8 sol la4 la8 la |
sib2 r4 r8 mi' |
fa'2 sib4 re' |
do'2 do'4 do' |
fa2 r4 r8 fa |
la4 la8 la la4 la8 sib |
do'2 r4 do'8 do' |
mi'4 mi'8 fa' sol'4. sib8 |
do'4.( sib8) la4 r |
r do'8 do' do'4. do'8 |
fa2 r |
r fa'4 fa' |
re'2\fermata mib'8 re' do' sib |
sol2 sol'8 re' mib' do' |
sib2. la8 do' |
sib2 r |
<>^\markup\italic ton sombre et fort
r4 fa8. fa16 sib4 sib8 do' |
re'4. re'8 re' re' do' sib |
la4 la
\ffclef "bass" <>^\markup\character Atar
r8 la re' la |
sib4 sib
\ffclef "tenor/G_8" <>^\markup\character Tarare
r8 sib sib do' |
la4 la r\fermata la8 la |
re'4. re'8 fa' fa' mi' re' |
dod'2 r8 sib sib sib |
la4. la8 dod'4. re'8 |
mi'4 mi' r8 mi' la' mi' |
fa'4. re'8 sol' sol' sol' re' |
mi'2 r4 mi'8. mi'16 |
mi'4. mi'8 mi'4 mi'8 mi' |
do'4 do' r do'8 re' |
mi'2 sol'8 mi' do' sib |
la2 r |
la'4. fa'8 do'4. do'8 |
do'4
\ffclef "bass" <>^\markup\character Atar
la8 sib do' do'
\ffclef "tenor/G_8" <>^\markup\character-text Tarare un peu plus serré
fa8 fa |
sol4. sol8 sol4 la8 sib |
la2 r8 fa sol la |
sib4. sib8 sib sib sib sib |
si4 si8 si si4. si8 |
do'2. do'8 re' |
mib'2 mib'4. mib'8 |
mib'2\fermata <>^\markup\italic sombre et concentré r8 do' do' do' |
reb'2 reb'4^\markup\italic sans rigueur do'8 sib |
la!4 la\fermata <>^\markup\italic Fortement re'8 re' mib' fa' |
sol'2 fa'4. fa'8 |
sib2
\ffclef "bass" <>^\markup\character-text Atar \normal-text Parlé
r4 fa'8 fa' |
re'4 sib r2 |
R1*5 |
