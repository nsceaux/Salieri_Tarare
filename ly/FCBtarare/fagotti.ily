\clef "bass" r2 |
R1*23 |
sib,1~ |
sib,~ |
sib, |
fad\cresc |
sol2 do4\! r |
r2 r\fermata |
re1-\sug\f |
mi2 re-\sug\p |
dod la,4. si,8 |
dod1-\sug\cresc |
re2-\sug\f sib, |
la,2 r |
la4-\sug\mf r sold r |
la r la r |
sol! r do r |
fa2:8-\sug\f fa:8 |
fa1~ |
fa4 r r\fermata fa-\sug\pp |
mi1 |
mib! |
re1 |
re |
do-\sug\cresc |
do8-\sug\ff do' mib' re' do' sib la sol |
fa2\fermata r |
R1 |
r2\fermata sib,8\ff-! sib,-! do-! re-! |
mib2 fa4. fa,8 |
sib,2 \once\slurDashed sib8( la16 sol fa mib re do) |
sib,2:8 sib,4:8 do:8 |
re:8 mib:8 fa:8 sol:8 |
lab8 fa re fa lab fa re fa |
lab4 sib,8 sib, sib,2:8 |
si,4. sol,8 sol,4 sol, |
sol,2 r |
