\clef "treble" \transposition fa
r2 |
R1*29 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''1~ |
    mi''2~ mi''8 fa'' fa'' fa'' |
    mi''1~ |
    mi'' |
    mi''2 re'' |
    mi'' }
  { do''1( |
    re''2)~ re''8 re'' re'' re'' |
    re''1~ |
    re'' |
    do''2 re'' |
    mi''_\markup\whiteout\center-align\tiny { [source : \concat { \italic do ] } } }
  { s1\f | s2 s8 s4.\p | s1 | s-\sug\cresc | s-\sug\f }
>> r2 |
R1*3 |
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { do''1~ | do''~ | do''4 }
  { do'1~ | do'~ | do'4 }
>> r4 r2\fermata |
R1*6 |
do''2\fermata r |
R1 |
r2\fermata r |
\tag#'corni <>^"unis" re''2-\sug\ff do''4. do''8 |
do''4 r r2 |
R1*3 |
r4 do''2 do''4 |
re''4. re''8 re''4. re''8 |
re''2 r |
