\clef "treble" r2 |
<<
  \tag #'violino1 {
    <do' mib'>1\fp |
    <re' sib>4. sol'8(\mf fa'4. sol'8) |
    fa'2~ fa'4 r |
    sib'8(\p sib' sib' sib' sib'2:8) |
    la'2:8( la':8) |
    la'2:8 la':8 |
    la'2:8 la':8 |
    sib':8 sib':8 |
    sol':8 la':8 |
    sib'4. sib'8\mf mi''4. mi''8 |
    fa''2(\p sib'4 re'') |
    do''1 |
    fa'4 <fa' la>8.\f q16 q4 r |
    la'2:8\p la'8 la' la' sib' |
    do''2:8 do'':8 |
    mi''4.( fa''8 sol''4. sib'8) |
    sib'2 la'8(\mf sol' la' sib') |
    do''4 la'8(\p do'' do'' sib' la' sol') |
    fa'4.( sol'8 la' sib' do'' re'') |
    mib''4.\fp re''8 do''( re'' mib'' fa'') |
    re''2\fermata r |
    sol' sol''8( re'' mib'' do'') |
    sib'2.:8 la'8 la' |
    sib'16 fa' fa' fa' fa'4:16 fa'2:16 |
    fa'2:16 fa':16 |
    fa'2:16 fa':16 |
    la':16 <la' re'>:16\cresc |
    <re' sib'>:16 <sib' mi''>\fp\fermata |
    <la' fa''>2\p r\fermata |
    re'''2 fa'''8. fa'''16 mi'''8. re'''16 |
    dod'''2 r8 sib''!\p sib'' sib'' |
    la''2:16 la'':16 |
    la'':16\cresc la'':16 |
    la''16\f re''' re''' re''' re'''4:16 re'''2:16 |
    dod'''2 r |
    r16 <mi' do'>\fp q q q4:16 r16 <<
      { mi'16 mi' mi' mi'4:16 } \\
      { re'16\fp re' re' re'4:16 }
    >> |
    r16 <do' mi'>\fp q q q4:16 r16 <do' mi'>\fp q q q4:16 |
    r16 <do' mi'>\fp q q q4:16 r16 <do' mi'>\fp q q q4:16 |
    r16
  }
  \tag #'violino2 {
    la1\fp |
    sib4. mib'8(-\sug\mf re'4. mib'8) |
    re'2~ re'4 r |
    re'8(-\sug\p re' re' re' re'2:8) |
    mib'2:8( mib':8) |
    mib'2:8 mib':8 |
    mib'2:8 mib':8 |
    re':8 fa':8 |
    mib'2:8 mib':8 |
    re'4. sib'8-\sug\mf sib'4. sib'8 |
    \once\slurDashed la'2(-\sug\p fa'4 sib') |
    la'2 sol' |
    fa'4 do'8.-\sug\f do'16 do'4 r |
    do'2:8-\sug\p do'8 do' do' sib |
    la la' la' la' la'2:8 |
    sib'8 sib' sib' la' sol'2:8 |
    sol'2 fa'8(\mf mi' fa' sol') |
    la'4 fa'8-\sug\p( la' la' sol' fa' mi') |
    fa'2~ fa'8( sol' la' sib') |
    do''4.-\sug\fp sib'8 la'( sib' do'' la') |
    sib'2\fermata r |
    sib2 mib'8( fa' sol' mib') |
    re'2.:8 do'8 do' |
    re'16 re' re' re' re'4:16 re'2:16 |
    re'2:16 re':16 |
    re':16 re':16 |
    re':16 <re' la'>:16-\sug\cresc |
    <re' sib'>:16 <sol' sib'>-\sug\fp\fermata |
    la'-\sug\p r\fermata |
    <la' fa''>2:16 q:16 |
    sol'16 sol'' sol'' sol'' sol''4:16 sol''2:16-\sug\p |
    sol'':16 sol'':16 |
    sol'':16-\sug\cresc sol'':16 |
    fa'':16-\sug\f sol''4:16 sol''8. re''16 |
    mi''2 r |
    r16 do'!-\sug\fp do' do' do'4:16 r16 si-\sug\fp si si si4:16 |
    r16 do'-\sug\fp do' do' do'4:16 r16 do'-\sug\fp do' do' do'4:16 |
    r16 sib!-\sug\fp sib sib sib4:16 r16 sib-\sug\fp sib sib sib4:16 |
    la16
  }
>> fa'\f sol' la' sib' do'' re'' mi'' fa'' <<
  \tag #'violino1 {
    la' sib' do'' re'' mi'' fa'' sol'' |
    la''4:16 fa'':16 do'':16 la':16 |
    fa'4
  }
  \tag #'violino2 {
    fa'16 sol' la' sib' do'' re'' mi'' |
    fa''4:16 do'':16 la':16 fa':16 |
    la4
  }
>> r4 r\fermata fa'\pp |
<<
  \tag #'violino1 {
    \rp#3 { sol'16( la' sib' la') } sol'( la' sib' sol') |
    \rp#3 { la'16( sib' do'' sib') } la'( sib' do'' la') |
    \rp#4 { sib'( do'' re'' do'') } |
    \rp#3 { si'16 do'' re'' do'' } si'16 do'' re'' si' |
    <>\mf \rp#4 { do''16 re'' mib'' re'' } |
    do''\ff mib''' mib''' mib''' mib'''4:16 mib'''2:16 |
    <mib''' mib''>2\fermata r |
    <>^\markup\italic con la voce reb''2:16\p reb'':16 |
    do''4 r\fermata re''!8-!\ff re''-! mib''-! fa''-! |
    <mib' sib' sol''>2 <la' fa''>4. fa''8 |
  }
  \tag #'violino2 {
    sib2:16 sib:16 |
    do':16 do':16 |
    fa':16 fa':16 |
    fa':16 fa':16 |
    <>-\sug\mf \rp#4 { mib'16 fa' sol' fa' } |
    mib'\ff <mib'' do'''> q q q4:16 q2:16 |
    q2\fermata r |
    sib'2:16-\sug\p sib':16 |
    la'!4 r\fermata re'8-!-\sug\ff re'-! mib'-! fa'-! |
    sol'2 la'4. la'8 |
  }
>>
sib'16( sib'' la'' sol'' fa'' mib'' re'' do'') sib'8( la'16 sol' fa' mib' re' do') |
sib2:16 sib4:16 do':16 |
re':16 mib':16 fa':16 sol':16 |
lab'16( sol' fa' mib') re'( mib' fa' sol') lab'( sol' fa' mib') re'( mib' fa' sol') |
lab'4 <<
  \tag #'violino1 {
    lab''2:16 lab''4:16 |
    sol''4. <re'' si''>8 q4. <re'' re'''>8 |
    q2 r |
  }
  \tag #'violino2 {
    <fa' re''>2:16 q4:16 |
    <sol' re''>4. <re'' sol''>8 q4. <re'' si''>8 |
    q2 r |
  }
>>
