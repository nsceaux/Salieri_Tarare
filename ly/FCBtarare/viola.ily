\clef "alto" r2 |
fa1\fp |
sib2 sib-\sug\mf |
sib~ sib4 r |
fa'8(\p fa' fa' fa' fa'2:8) |
fa':8( fa':8) |
fa':8 fa':8 |
fa':8 fa':8 |
fa':8 sib:8 |
sib8 sol mib mib' do'2:8 |
sol'4. sol'8-\sug\mf sol'4. sol'8 |
fa'1-\sug\p~ |
fa'2 mi' |
fa'4 fa8.-\sug\f fa16 fa4 r |
fa'2:8-\sug\p fa':8 |
fa':8 fa':8 |
sol'8 sol' sol' fa' mi'2:8 |
do' <<
  { \slurDashed la8( sol la sib) |
    do'4 la8( do' do' sib la sol) |
    fa4.( sol8 la sib do' re') |
    mib'4. re'8 do'( re' mib' fa') | \slurSolid
    re'2\fermata } \\
  { fa8-\sug\mf mi fa sol |
    la4 fa8\p la la sol fa mi |
    fa2 fa8 sol la sib |
    do'4.-\sug\fp sib8 la sib do' la |
    sib2\fermata }
>> r2 |
mib2 r |
fa'2. fa4 |
sib r sib r |
sib r sib r |
sib r sib r |
fad r fad-\sug\mf r |
sol r do'2-\sug\fp\fermata |
fa-\sug\p r2\fermata |
re''2:16 re'':16 |
mi''16 mi' mi' mi' mi'4:16 re'2:16-\sug\p |
mi'16 la la la la2.:16 |
la16-\sug\cresc mi'' mi'' mi'' mi''4:16 mi''2:16 |
re''2:16-\sug\f sib2:16 |
la2 r |
la4-\sug\mf r sold r |
la r la r |
sol! r r16 sol-\sug\fp sol sol sol4:16 |
fa2:8-\sug\f fa:8 |
fa1~ |
fa4 r r\fermata fa\pp |
mi2:16 mi:16 |
mib!:16 mib:16 |
re2:16 re:16 |
re':16 re':16 |
do':8\cresc do':8 |
do'8\ff do'' mib'' re'' do'' sib' la' sol' |
fa'2\fermata r |
mi'2:16-\sug\p mi'2:16 |
fa'4 r\fermata sib8\ff-! sib-! do'-! re'-! |
mib'2 fa'4. fa8 |
sib2 \once\slurDashed sib'8( la'16 sol' fa' mib' re' do') |
sib2:8 sib4:8 do':8 |
re':8 mib':8 fa':8 sol':8 |
lab'8 fa' re' fa' lab' fa' re' fa' |
lab'4 sib8 sib sib2:8 |
si4. sol8 sol4. sol8 |
sol2 r |
