\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (reduction)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet#})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#})
   (silence #:system-count 2))
