\clef "treble" <>_\markup\whiteout\italic sans sourdines R1^\fermataMarkup |
<re' si'>4 r r2 |
r <<
  \tag #'violino1 { 
    red''2 |
    red''1 |
    mi''4 r r2 |
    lad'2
  }
  \tag #'violino2 {
    la'2 |
    la'1 |
    sol'4 r4 r2 |
    fad'
  }
>> r4 <dod' lad'> |

