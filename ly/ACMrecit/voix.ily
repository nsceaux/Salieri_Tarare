\clef "soprano/treble" r2^\fermata_\markup\italic Parlé <>^\markup\italic au Génie
r4 re''8 re'' |
si'2 r |
si'8 si' si' do'' la'4. la'8 |
red''4. red''8 red'' red'' red'' si' |
mi'' mi'' r4 si'8 si'16 si' mi''8 dod'' |
lad' fad'16 sold' lad'8 lad'16 si' fad'4 r |
