\clef "treble" R1^\fermataMarkup |
<re' si'>4 r r2 |
r <la' red''>2 |
q1 |
<sol' si' mi''>4 r r2 |
<dod' fad' lad'>2 r4 <dod' lad'> |

