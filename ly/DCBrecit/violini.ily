\clef "treble" R1*2 |
<dod' la'>4 r <<
  \tag #'violino1 { dod''4 r | re'' }
  \tag #'violino2 { sol'4 r | <fad' la>4 }
>> r4 r2 |
r2 <<
  \tag #'violino1 { si'2~ | si'1~ | si'2 do''4 }
  \tag #'violino2 { fa'!2~ | fa'1~ | fa'2 mi'4 }
>> r4 |
R1 |
<<
  \tag #'violino1 { fa''4 r fa'' r | }
  \tag #'violino2 { <do'' fa'>4 r <re' si'!> r | }
>>
r2 r4 <<
  \tag #'violino1 { fa''4\f | mi'' }
  \tag #'violino2 { si'4-\sug\f | si' }
>> r4 r2 |
<<
  \tag #'violino1 { mi'4\p r red'' r | }
  \tag #'violino2 { do''4-\sug\p r fad' r | }
>>
r4 <si fad' red''?>4\f <sol' mi''>8 sol''16([ la''] si'' sol'' si'' sol'') |
mi''8 fad''16( sol''! la'' si'' dod''' red''' mi''' red''' mi''' red''' mi''' red''' mi''' red''' |
mi'''4) r8 r16 <si' sol''!> <<
  \tag #'violino1 { <si' sol''>4 <si' si''>4 }
  \tag #'violino2 { <mi' si' sol''>4 q | }
>>
mi'2 r\fermata |
