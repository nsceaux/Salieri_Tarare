\clef "bass" <>^\markup\character Atar
r4 r8 sold16 sold dod'4 r8 dod'16 dod' |
dod'4. sold8 si si dod' sold |
la la
\ffclef "alto/G_8" <>^\markup\character Calpigi
r16 la la si dod'8 dod' dod' re' |
la la r la re'8. re'16 re'8 mi' |
do'!8. do'16 do' do' do' do' si4 si |
re'8 re' mi' fa' si4 si8 do' |
re'4 si8 sol do' do' r16 do' do' re' |
mi'4. sol'8 mi' mi' re' do' |
fa'4 r16 do' fa' mi' fa'4 fa'8 re' |
si4 si8 do' sol sol r4 |
r4 <>^\markup\italic (à part) r8 si mi' mi'16 mi' mi'8 re'16 mi' |
do'4 r16 do' si la red'8. red'16 red'8 mi' |
si4 r r2 |
R1*2 |
R1^\fermataMarkup |
