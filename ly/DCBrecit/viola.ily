\clef "alto" R1*2 |
fad'4 r mi' r |
re' r r2 |
r re'~ |
re'1~ |
re'2 sol4 r |
R1 |
la4 r sol r |
r2 r4 sol-\sug\f |
sold r r2 |
la4-\sug\p r la r |
r si-\sug\f <sol' si'>2:16 |
q:16 q:16 |
q4 r8 r16 mi' mi'4 mi' |
mi'2 r\fermata |
