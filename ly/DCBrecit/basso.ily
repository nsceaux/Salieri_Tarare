\clef "bass" R1*2 |
fad4 r mi r |
re r r2 |
r re~ |
re1~ |
re2 do4 r |
R1 |
la,4 r sol,! r |
r2 r4 sol\f |
sold r r2 |
la4\p r la r |
r si\f mi16 mi mi mi mi mi mi mi |
mi2:16 mi:16 |
mi4. mi8 mi4 mi |
mi2 r\fermata |
