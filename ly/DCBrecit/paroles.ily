Cal -- pi -- gi, quel spec -- tacle of -- frai-je à ma sul -- ta -- ne ?

C’est u -- ne fête eu -- ro -- pé -- a -- ne.
Ain -- si, quand l’un des Rois de ces puis -- sants é -- tats,
or -- don -- ne qu’on a -- muse u -- ne Reine a -- do -- ré -- e,
des jeux bril -- lants, des mœurs de vos cli -- mats,
sa no -- ble fête à l’ins -- tant est pa -- ré -- e.

Ta -- ra -- re n’est point pré -- ve -- nu ;
s’il ar -- ri -- vait, il est per -- du.
