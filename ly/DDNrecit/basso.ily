\clef "bass" mi2 r |
r fa4 r |
R1*2 |
r2 r8 sib,-!\f re-! fa-! |
sib4 r4 r2 |
R1 |
r2 fad4 r |
r2 sol4 r |
R1 |
r8. dod16 dod4 r2 |
R1 |
r4 re re r |
R1*2 |
r8 sol\f si sol re sol si, re |
sol,4 sol,16 la, si, do re4 re |
sol, sol,16 la, si, do re4 re sol,4. sol,8 sol,4 sol, |
sol,2 r |
