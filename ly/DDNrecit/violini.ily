\clef "treble" <sol mi' do''>2 r |
r <fa' la' fa''>4 r |
R1*2 |
r2 r8 sib'\f-! re''-! fa''-! |
<re' sib' sib''>4 r r2 |
R1 |
r2 <<
  \tag #'violino1 { la''4 }
  \tag #'violino2 { re'' }
>> r4 |
r2 <<
  \tag #'violino1 { sib''4 }
  \tag #'violino2 { re''4 }
>> r4 |
R1 |
r8. <<
  \tag #'violino1 { <la' la''>16 q4 }
  \tag #'violino2 { <la' mi''>16 q4 }
>> r2 |
R1 |
r4 <re' la' fad''>4 re'4 r |
R1 |
r8 sol''-!\f si''-! sol''-! re'' sol'' si' re'' |
sol' sol' si' sol' re' sol' si re' |
sol4 sol16 la si do' re'4 <la fad'> |
<si sol'> sol'16 la' si' do'' re''4 <re' la' fad''> |
<re' si' sol''>4. <si' si''>8 <sol'' si' re'>4 q |
q2 r |
