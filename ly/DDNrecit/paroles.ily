Cal -- pi -- gi, ta fête est char -- man -- te !
J’aime un ta -- lent vain -- queur à qui tout o -- bé -- it :
ton es -- prit fer -- ti -- le m’en -- chan -- te.
Des mers d’Eu -- rope et con -- tre toute at -- ten -- te,
dis- nous quel heu -- reux sort en ce lieu t’a con -- duit ?
Mais pour a -- mu -- ser mon a -- man -- te,
a -- ni -- me ton ré -- cit d’u -- ne gaî -- té pi -- quan -- te.

J’y veux mê -- ler un nom qui nous ren -- dra la nuit.
