\clef "alto" mi'2 r |
r fa'4 r |
R1*2 |
r2 r8 sib-!\f re'-! fa'-! |
sib'4 r4 r2 |
R1 |
r2 fad'4 r |
r2 sol'4 r |
R1 |
r8. dod'16 dod'4 r2 |
R1 |
r4 re' re' r |
R1*2 |
r8 sol'\f si' sol' re' sol' si re' |
sol4 sol16 la si do' re'4 re' |
sol sol16 la si do' re'4 re' sol4. sol8 sol4 sol |
sol2 r |
