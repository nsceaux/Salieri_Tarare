\clef "bass" <>^\markup\character-text Atar avec joie ^"parlé"
r4 r8 sol16 sol do'4. sol8 |
sib4 sib8 do' la la r4 |
do'8 do'16 do' do'8 do' la la16 la fa8 sib16 do' |
fa4 r la8 la la sib |
do'4 do'8 re' sib sib r4 |
r8 fa fa fa sib4. sib8 |
re' re' do' re' sib sib r sib |
sib sib do' re' la4 la8 sib |
do'4 do'8 re' sib4 r |
sol8 sol sol la sib4 sib8 sib |
la la r la la la la si! |
dod'4 r dod'8 dod'16 dod' dod'8 re' |
la la r4
\ffclef "alto/G_8" <>^\markup\character-text Calpigi à part d’un ton sombre
r8 la la16 la la la |
re'4. la8 la si do' re' |
si4 r r2 |
<<
  R1*5 |
  \tag #'recit { s1*3 s2 <>^\markup\right-align\italic {
      Il prend une mandoline et chante sur le ton de Barcarole.
    }
  }
>>
