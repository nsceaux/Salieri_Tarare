\clef "bass" r4 |
fa8\p la fa la fa sib fa sib |
fa la fa la fa la fa la |
sol sib sol sib mi do' mi do' |
fa la do' la fa la do' la |
fa sol fa sol fa sol fa sol |
fa sol fa sol fa sol fa sol |
mi sol mi sol mi sol mi sol |
si, sol si, sol do mi re do |
fa fa re fa sol fa sol sol, |
la,2.\fermata r4 |
fad8 la fad la fad la fad la |
sol sib sol sib fad la fad la |
sol sib sol sib sol sib sol sib |
do sol do sol mi sol mi sol |
fa do' la fa mi sol mi sol |
fa la fa la la, fa la, fa |
sib, fa sib, fa do fa do mi |
fa la do' la fa\mf fa sol la |
sib sol la sib do' sib do' do |
fa\f la do' la sol4 do' |
fa8 la do' la sol4 do |
fa8 fa do la, fa,4 r |
