\clef "soprano/treble" <>4.^\markup\italic {
  pendant qu’on l’habille,
  avec hypocrisie
} ^"chanté"
do''8 do'' |
do''4. la'8 re''[ mi''] fa'' re'' |
do''4 la' r do''8 fa'' |
\grace fa'' mi''4. fa''8 sol''[ mi''] do'' sib' |
la'4 fa' r r8 do'' |
si'4. do''8 re'' mi'' fa'' re'' |
si'2. la'8 sol' |
do''4. do''8 si'[ do''] fa'' mi'' |
re''2 mi''8 sol'' fa'' mi'' |
mi''[ re''] fa'' re'' do''4\melisma si'\melismaEnd |
do''2.\fermata dod''4 |
re''4. mib''8 re'' mib'' re'' do'' |
\grace do''8 sib'4. mib''8 re'' mib'' re'' do'' |
\grace do'' sib'2 r4 sib'8 sib' |
mi''!4. fa''8 sol''[ mi''] do'' sib' |
la'4. la''8 sol''[ mi''] do'' sib' |
la'2 do''8 re'' mi'' fa'' |
re''[ fa''] re'' sib' la'4( sol') |
la'2 fa''8 la'' sol'' fa'' |
mi''[ re''] do'' sib' la'4\melisma sol'\melismaEnd |
fa' r r2 |
R1*2 |
