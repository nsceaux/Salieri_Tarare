\clef "treble" do''\p |
do''4. la'8 re'' mi'' fa'' re'' |
do''4 la' r do''8 fa'' |
fa''4( mi''8) fa'' sol'' mi'' do'' sib' |
la'4 fa' r do'' |
si'4. do''8 re'' mi'' fa'' re'' |
do''4 si'2 la'8 sol' |
do''4. do''8 si' do'' fa'' mi'' |
re''2 mi''8 sol'' fa'' mi'' |
mi'' re'' fa'' re'' do''4( si') |
do''2.\fermata dod''4 |
re''4. mib''8 re'' mib'' re'' do'' |
do''4 sib'8( mib'' re'' mib'' re'' do'') |
do''4( sib'!) r sib' |
mi''!4. fa''8 sol''( mi'' do'' sib') |
la'( do'' fa'' la'') sol''( mi'' do'' sib') |
la'2 do''8( re'' mi'' fa'') |
re''( fa'' re'' sib') la'4 sol' |
la'8( fa' la' do'') fa''(\mf la'' sol'' fa'') |
mi''( re'' do'' sib') <<
  \tag #'violino1 { la'4 sol' | }
  \tag #'violino2 { la'8 fa' \grace fa' mi' re'16 mi' | }
>> fa'8\f fa''-! la''-! fa''-! sib''16-! la''-! sol''-! fa''-! mi''-! re''-! do''-! sib'-! |
la'8 do'' la' fa' sib'16 la' sol' fa' mi' re' do' sib |
la8 <<
  \tag #'violino1 { <fa' do'' fa''>8 q q q4 }
  \tag #'violino2 { <do' la'>8 q q q4 }
>> r4 |
