\clef "alto" sold4 r |
r8 mi[ mi mi] |
mi4 r |
r8 fad[ fad fad] |
mi4 r r2 |
R1 |
la4 r r2 |
R1 |
re'4 r r2 |
sold4 r r2 |
la4 r |
r8 do'[ do' fa!] |
sol4. r8 |
sol r sol r |
sol4 r\fermata |
