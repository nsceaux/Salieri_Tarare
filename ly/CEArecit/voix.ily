\clef "alto/G_8" <>^\markup\character Calpigi
R2*4
<>^\markup\italic { Il se découvre. }
r4 r8 si mi' mi' r mi'16 si |
sold4
\ffclef "tenor/G_8" <>^\markup\character Tarare
r8 si16 dod' re'4
\ffclef "alto/G_8" <>^\markup\character Calpigi
r8 re'16 mi' |
dod'4 r8 la16 la la4 la8 si |
dod'4 r8 dod'16 mi' dod'4 dod'8 re' |
la la r la16 la re'4 mi'8 fa' |
mi'4 r8 si re' re' mi' si |
do'4 r |
R2*4 |
