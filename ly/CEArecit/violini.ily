\clef "treble"
<<
  \tag #'violino1 {
    r16 \grace fad' mi'32\p-. red'-. mi'16-. fad'16-. sold'16-. mi'-. sold'-. la'-. |
    si'-. si'-. mi''-. si'-. dod''-. la'-. mi''-. dod''-. |
    si' \grace fad' mi'32 red' mi'16 fad' sold' mi' si' sold' |
    la' fad' red' si la red' fad' la |
    sold4
  }
  \tag #'violino2 {
    si4 r |
    r8 sold[ la la] |
    sold4 r |
    r8 red'[ red' red'] |
    mi'4
  }
>> r4 r2 |
R1 |
<<
  \tag #'violino1 { dod''4 }
  \tag #'violino2 { mi'4 }
>> r4 r2 |
R1 |
<<
  \tag #'violino1 { re''4 }
  \tag #'violino2 { la' }
>> r4 r2 |
<<
  \tag #'violino1 { si'4 }
  \tag #'violino2 { mi' }
>> r4 r2 |
<<
  \tag #'violino1 {
    do''4. \grace { si'16[\p la' sol'] } la'16. si'32 |
    do''16-. la'-. mi'-. la'-. do'' la' re''16. do''32 |
    si'4. \grace { la'16[ sol' fa'] } sol'16. la'32 |
    si'8 \grace { do''16[ si' la'] } si'16. do''32 re''8 \grace { mi''16[ re'' do''] } re''16. mi''32 |
    fa''4
  }
  \tag #'violino2 {
    mi'4 r |
    r8 mi'[ mi' la'] |
    re'4 r |
    re'8 \grace { la'16[ sol' fa'] } sol'16. la'32 si'8 \grace { do'16[ si la] } si16. do'32 |
    re'4
  }
>> r4\fermata |
