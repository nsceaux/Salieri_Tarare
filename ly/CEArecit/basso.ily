\clef "bass" mi4\p r |
r8 mi[ mi mi] |
mi4 r |
r8 si,[ si, si,] |
mi4 r r2 |
R1 |
la,4 r r2 |
R1 |
fa!4 r r2 |
sold4 r r2 |
la4 r |
r8 la[ la fa!] |
sol4. r8 |
sol r sol r |
sol4 r\fermata |
