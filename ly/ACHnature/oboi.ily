\clef "treble"
r8. <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''16 re''8. re''16 | sol''4. re''8 re''4. re''8 | sib'2 }
  { re''16 re''8. re''16 | sib'2 la' | sib' }
>> r8. \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib'16 do''8. re''16 | mib''2. re''4 | do''2. }
  { sol'16 la'8. sib'16 | do''2. sib'4 | la'2. }
  { s16 s4 | s2 <>-\sug\p }
>> r8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { do'8 | fa'4. fa'8 la'4 do'' | fa''1 | re''2 }
  { do'8 | fa'4. fa'8 la'4 do'' | fa''2 fa'~ | fa' }
>> r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''8. fa''16 | sol''4 mib''8. mib''16 do''4 fa'' | }
  { sib'4~ | sib' do''8. do''16 la'2 | }
>>
