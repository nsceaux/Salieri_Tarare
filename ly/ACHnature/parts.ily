\piecePartSpecs
#`((reduction)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet#})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#})
   (oboi #:score "score-oboi")
   (fagotti #:clef "tenor")
   
   (violino1)
   (violino2)
   (viola)
   (basso)
   (silence #:ragged #f #:system-count 2))
