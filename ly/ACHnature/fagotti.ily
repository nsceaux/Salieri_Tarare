\clef "tenor" r8. re'16-\sug\f re'8. re'16 |
sol'4. re'8 re'4. re'8 |
sib2 r |
R1 |
r2 r4 r8 do-\sug\f |
fa4. fa8 la4 do' |
fa'1 |
fa'2 r4 \clef "bass" re'4 |
mib' do' fa' fa |
