\clef "alto" r2 |
sol'2\f fad' |
sol' r |
do'2 la4\p sib |
fa'4 fa'8. fa'16 fa'4 r |
fa'2\f r |
la2 la' |
sib'4 sib r re' |
mib' do' fa' fa |
