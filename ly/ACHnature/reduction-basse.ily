\clef "bass" r2 |
<sol sib re'>2 <fad la re'> |
<sol sib re'> r |
do2 la,4 sib, |
fa4 fa8. fa16 fa4 r8 la |
<fa la>2 r |
<la, do>2 la |
sib4 sib, r re |
mib do fa fa, |
