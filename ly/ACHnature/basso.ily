\clef "bass" r2 |
sol2\f fad |
sol r |
do2 la,4\p sib, |
fa4 fa8. fa16 fa4 r |
fa2\f r |
la,2 la |
sib4 sib, r re |
mib do fa fa, |
