\clef "treble" r8. re''16\f re''8. re''16 |
sol''4. re''8 re''4. re''8 |
sib'2 r8. <sol' sib'>16 <la' do''>8. <sib' re''>16 |
<do'' mib''>4 q8.\p q16 q4 <sib' re''>8. q16 |
<la' do''>4 q8. q16 q4. do'8\f |
fa'4. fa'8 la'4 do'' |
fa''2 <fa' do''>4. do''8 |
<fa' re''>4 q r <sib' fa''>8. q16 |
<sib' sol''>4 <do'' mib''>8. q16 <la' do''>4 <la' fa''> |
