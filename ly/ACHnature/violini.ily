\clef "treble"
<<
  \tag #'violino1 {
    r8. re''16\f re''8. re''16 |
    sol''4. re''8 re''4. re''8 |
    sib'2 r8. sib'16 do''8. re''16 |
    mib''4 mib'8.\p mib'16 mib'4 re'8. re'16 |
    do'4 do'8. do'16 do'4. do'8\f |
    fa'4. fa'8 la'4 do'' |
    fa''2 do'4. do'8 |
    re'4 re' r fa''8. fa''16 |
    sol''4 mib''8. mib''16 do''4 fa'' |
    
  }
  \tag #'violino2 {
    r8. re'16-\sug\f re'8. re'16 |
    <re' sib'>2 <la' re'> |
    <re' sib'>2 r8. sol'16 la'8. sib'16 |
    do''4 do'8.-\sug\p do'16 do'4 sib8. sib16 |
    la4 la8. la16 la4. la8-\sug\f |
    la4. fa'8 la'4 do'' |
    do'2 fa' |
    fa'4 fa' r sib'8. sib'16 |
    sib'4 do''8. do''16 la'4 la |
  }
>>
