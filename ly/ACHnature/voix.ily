\clef "soprano/treble" <>^\markup\italic chanté
^\markup\italic\line { aux deux ombres, très fier }
r8. re''16 re''8. re''16 |
sol''4. re''8 re''4. re''8 |
sib'2 r8 sib' do'' re'' |
mib''!4 mib''8. mib''16 mib''4 re''8. re''16 |
do''4 do'' r r8 do'' |
fa'2. r8 do'' |
fa''2 fa''4 fa''8. fa''16 |
re''4 re'' r fa''8. fa''16 |
sol''4 mib''8. mib''16 do''4. fa''8 |
