\clef "tenor" R1*30 |
< \tag#'tr1 reb' \tag#'tr2 sib \tag#'tr3 sol >1-\sug\fp~ |
q~ |
q |
fa2-\sug\f lab |
do' r |
R1 |
\clef "bass" r2 r4 r8 r16 < \tag#'(tr1 tr2) sol \tag#'tr3 sol,> |
q1\fp~ |
q |
q4 r r2 |
r r4 sol |
\clef "tenor" < \tag#'tr1 do' \tag#'tr2 sol \tag#'tr3 mi >2~ q4 r |
R1*5 R2.*4 R1*23 |
r2 r4\fermata
