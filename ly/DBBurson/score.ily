\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes et Cors \small in Mi♭ }
        shortInstrumentName = \markup\center-column { Tr. Cor. }
        \haraKiri
      } <<
        { \noHaraKiri s1*29 \revertNoHaraKiri }
        \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with { \tromboniInstr \haraKiri } <<
        \global \includeNotes "tromboni"
        { s1*29 \noHaraKiri }
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Timballes Cimballes }
        shortInstrumentName = \markup Tim.
        \haraKiri
      } << \global \includeNotes "timpani" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Urson
      shortInstrumentName = \markup\character Ur.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*6\break s1*4\pageBreak
        s1*6\break s1*3\pageBreak
        s1*4\break s1*4\pageBreak
        s1*5\break s1*5\pageBreak
        s1*5\break s1*4\pageBreak
        s1 s2.*3\break s2. s1*2\break s1*3\pageBreak
        s1*4\break s1*3\break s1*4\pageBreak
        s1*3\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
