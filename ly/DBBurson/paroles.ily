Ta -- ra -- re le pre -- mier ar -- rive au ren -- dez- vous :
Par quel -- ques pas -- ses dans la plai -- ne,
il met son che -- val en ha -- lei -- ne,
et vient con -- ver -- ser a -- vec nous.
Sa con -- te -- nance est noble et fiè -- re.
Un long nu -- a -- ge de pous -- siè -- re
s’a -- van -- ce du cô -- té du nord ;
on croit voir une ar -- mée en -- tiè -- re.
C’est l’im -- pé -- tu -- eux Al -- ta -- mort.
D’es -- cla -- ves ar -- més un grand nom -- bre,
au ga -- lop à pei -- ne le suit.
Son as -- pect est fa -- rouche et som -- bre,
com -- me les spec -- tres de la nuit.
D’un œil ar -- dent me -- su -- rant l’ad -- ver -- sai -- re ;
du vain -- cu dé -- ci -- dons le sort.
« Ma loi », dit Ta -- rare, « est la mort ».
L’un sur l’au -- tre à l’ins -- tant fond com -- me le ton -- ner -- re.
Al -- ta -- mort pa -- re le pre -- mier.
Un coup af -- freux de ci -- me -- ter -- re
fait vo -- ler au loin son ci -- mier.
L’a -- cier é -- tin -- cel -- le,
le casque est bri -- sé,
un noir sang ruis -- sel -- le.
Dieux ! je suis bles -- sé.
Plus fu -- ri -- eux que la tem -- pête,
à plomb sur la tê -- te,
le coup est ren -- du,
le bras ten -- du,
Ta -- ra -- re pa -- re…
et tient en l’air le tré -- pas sus -- pen -- du.

Je vois qu’Al -- ta -- mort est per -- du.

A -- veu -- glé par le sang, il s’a -- gite, il chan -- cel -- le.
Ta -- ra -- re, cour -- bé sur sa sel -- le,
pique en a -- vant. Son fier cour -- sier,
sen -- tant l’ai -- guil -- lon qui le per -- ce,
s’é -- lan -- ce, et du poi -- trail ren -- ver -- se
et le che -- val et le guer -- rier.
Ta -- rare à l’ins -- tant saute à ter -- re,
court à l’en -- ne -- mi ter -- ras -- sé.
Cha -- cun fré -- mi, le cœur gla -- cé
du ter -- ri -- ble droit de la guer -- re…
