\clef "alto" mib4-\sug\f sol sib sol |
mib sib8. sib16 sol8. sol16 mib8. mib16 |
lab8. lab16 fa8. fa16 re8. re16 sib8. sib16 |
mib4 r r2 |
R1 |
mib4 sol sib sol |
mib( sol la sib) |
do'2 r |
R1*2 |
fa'4. re8 re re re re |
re4.. sib16 sib4.. sib16 |
sib1~ |
sib |
r4 sib(-\sug\p do' dod') |
re'8 re' re'-\sug\cresc re' re'2:8 |
re'2-\sug\f <la! fad'>2 |
<fad' la'!>4 r r2 |
R1 | \allowPageTurn
<<
  { re'8 re'4 re'8 re' re'4 re'8 |
    re' re'4 re'8 re' re'4 re'8 |
    re'4 } \\
  { sib8\p sib4 sib8 la la4\cresc la8 |
    sib sib4 sib8 la la4 la8 |
    sib4\! }
>> r4 r2 |
<<
  { re'8 re'4 re'8 re' re'4 re'8 | re'4 } \\
  { sib8\p sib4\cresc sib8 lab! lab4 lab8 | lab4\! }
>> r4 r2 |
<sib sol'>8-\sug\ff q4 q q q8 |
q4 r r2 |
R1 | \allowPageTurn
<sib sol'>8-\sug\ff q4 q q q8 |
<<
  { \ru#4 { sib'16 lab' sol' lab' } | sib'4. } \\
  { \ru#4 { sol'16 fa' mib' fa' } | sol'4. }
>> mib8 mib4 mib |
mi1\fp |
R1*2 | \allowPageTurn
fa4..\f fa32 sol lab4.. lab32 sib |
do'2~ do'8 do'16 do' re' re' mi' mi' |
fa'4 r r2 |
r r4 r8 r16 sol |
sol1\fp~ |
sol |
sol4 r r2 |
r r4 sol' |
do'8 re'16 mi' fa' sol' la' si' do''4 r |
R1 |
r4 <do sol mi' do''>4 q r |
r2 r4 <do la fa' la'> |
<do la fa' do''>4 r r2 |
R1 |
sib8-\sug\f sib16 do' re' mib' fa'4 fa'16 sol'32 la'! |
sib'4. fa'-\sug\mf |
sib sol' |
do' la'! |
re'4 <mib' mib''>2.\sf~ |
q4 r r2 |
r4 <sib' sol'>2.-\sug\ff |
fa'4 r r2 |
re''16(-.\f re''-. re''-. re''-. re''2.:16)^\dotFour |
re''2:16 re'':16 |
re''2:16 re'':16 |
sol':16-\sug\p sol':16 |
sol':16 sol':16 |
sol':16 dod':16-\sug\fp |
dod':16 re':16-\sug\fp |
re':16 re':16 |
r16 mi-\sug\ff fad sold la si dod' red' mi' sold' fad' mi' red' dod' si la |
sold sold' fad' mi' red' dod' si la sold2~ |
sold la |
si4 r r si |
mi'4 r r2 |
sol'!4-\sug\f r do16 do' si do' sol do' mi sol |
do4 r r2 |
r4 mi:16-\sug\p fa:16 sol:16 |
la16 do' do' do' do'4:16 do'2:16 |
do':16 do':16 |
do':16 do':16 |
do'4 r r\fermata
