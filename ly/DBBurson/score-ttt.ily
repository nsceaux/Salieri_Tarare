\score {
  <<
    \new Staff \with { \haraKiri \tinyStaff } \withLyrics <<
      \global \includeNotes "voix"
    >> { \set fontSize = -2 \includeLyrics "paroles" }
    \new StaffGroup <<
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes et Cors \small en Mi♭ }
        shortInstrumentName = \markup\center-column { Tr. Cor. }
        \haraKiri
      } <<
        \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with { \tromboniInstr \haraKiri } <<
        \global \includeNotes "tromboni"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Timballes Cimballes }
        shortInstrumentName = \markup Tim.
        \haraKiri
      } << \global \includeNotes "timpani" >>
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 10\mm
  }
}
