\clef "bass" R1*41 |
<>^"Timballes" do2:16 do4 r |
R1 |
r4 <>^"Cimballes" do do r |
r2 r4 do |
do r r2 |
R1 \allowPageTurn R2.*4 R1*12 |
r2 <>^"Timballes" sold,:16-\sug\ff |
sold,:16 sold,4 r |
R1*9 |
r2 r4\fermata
