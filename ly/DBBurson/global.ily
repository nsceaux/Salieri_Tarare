\tag #'all \key mib \major \midiTempo#100
\time 4/4 s1*14
\tempo "piu Allegro" s1*19
\tempo "Sostenuto Allegro" s1*8 \bar "||"
\tag #'all \key do \major s1*6
\time 6/8 \tempo "Allegretto" \midiTempo#148 s2.*4
\time 4/4 \midiTempo#100 s1*2 \midiTempo#52 s1 \midiTempo#100 s1*9
\tempo "Allegro" s1*11 s2. \bar "|."
