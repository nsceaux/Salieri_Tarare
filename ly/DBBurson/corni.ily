\clef "treble" \transposition mib
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { do''2~ do''8 do'' do'' do'' | do''4 }
  { mi'2~ mi'8 mi' mi' mi' | mi'4 }
>> r4 r2 |
r2 r4 \twoVoices #'(corno1 corno2 corni) <<
  { sol'4 | do'' }
  { sol' | mi' }
>> r4 r2 |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''2~ do'':8 | do''4 mi''2 mi''4 | re''2 }
  { mi'2~ mi':8 | mi'4 do''2 do''4 | do''2 }
>> r2 |
R1*2 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol'2~ sol'8 sol' sol' sol' |
    sol'4.. re''16 re''4.. re''16 |
    re''2 }
  { sol'2~ sol'8 sol' sol' sol' |
    sol'4.. sol'16 sol'4.. sol'16 |
    sol'2 }
>> r2 |
R1*9 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol''2 fa'' | fa''4 }
  { mi''2 re'' | re''4 }
  { s2-\sug\p s-\sug\cresc | s4\! }
>> r4 r2 |
<>\ff \twoVoices #'(corno1 corno2 corni) <<
  { sol''8 sol''16 sol'' sol''8 sol''16 sol'' sol''8 sol''16 sol'' sol''8 sol''16 sol'' |
    sol''4 }
  { sol'8 sol'16 sol' sol'8 sol'16 sol' sol'8 sol'16 sol' sol'8 sol'16 sol' |
    sol'4 }
>> r4 r2 |
R1 |
<>\f \twoVoices #'(corno1 corno2 corni) <<
  { sol''8 sol''16 sol'' sol''8 sol''16 sol'' sol''8 sol''16 sol'' sol''8 sol''16 sol'' |
    sol''8 sol''16 sol'' sol''8 sol''16 sol'' sol''8 sol''16 sol'' sol''8 sol''16 sol'' |
    sol''4 }
  { sol'8 sol'16 sol' sol'8 sol'16 sol' sol'8 sol'16 sol' sol'8 sol'16 sol' |
    sol'8 sol'16 sol' sol'8 sol'16 sol' sol'8 sol'16 sol' sol'8 sol'16 sol' |
    sol'4 }
>> r4 r2 |
R1*17 R2.*4 R1*23 |
r2 r4^\fermata
