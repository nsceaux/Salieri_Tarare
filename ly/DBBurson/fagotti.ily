\clef "bass" mib4\f sol sib sol |
mib sib sol mib |
lab fa re sib, |
mib4 r r2 |
R1 |
mib4 sol sib sol |
mib mib' do' sib |
la2 r |
R1*2 | \allowPageTurn
sib,4 re fa re |
sib,4.. sib,16 sib,4.. sib,16 |
sib,1~ |
sib, |
r4 sib,(\p do dod) |
re1 |
<>-\sug\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'1~ | re'4 }
  { re1~ | re4 }
>> r4 r2 |
R1 |
sol2:8-\sug\p fad:8-\sug\cresc |
sol:8 fad:8 |
sol4\! r r2 |
sol'2-\sug\p fa'-\sug\cresc~ |
fa'4\! r r2 |
<>-\sug\ff \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib1 | }
  { sol | }
>>
mib4 r r2 |
R1 |
<>-\sug\ff \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib1 | sib | sib4 }
  { sol1 | sol | sol4 }
>> r4 r2 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { reb'1~ | reb'~ | reb' | }
  { mi1~ | mi~ | mi | }
  { s1-\sug\fp }
>>
fa2-\sug\f lab |
do' r8 do re mi |
fa4 r r2 |
r r4 r8 r16 sol, |
sol,1-\sug\fp~ |
sol,~ |
sol,4 r r2 |
r r4 sol |
do8 re16 mi fa sol la si do'4 r |
R1 |
r4 do' do r |
r2 r4 fa |
fa,1 |
fa,4 r r2 |
sib,4.\f fa |
sib re\mf |
mib mi |
fa fad |
sol4 \clef "tenor" mib'2.-\sug\sf~ |
mib'4 r r2 |
\clef "bass" r4 do2.\ff |
fa4 r r2 |
re'1-\sug\f~ |
re'~ |
re' |
R1*6 |
sold8-\sug\ff fad16 mi red dod si, la, sold,2~ |
sold, la, |
la4 r r si |
mi4 r r2 |
mi4\f r do8 do' sol mi |
do4 r r2 |
R1*4 |
r2 r4\fermata
