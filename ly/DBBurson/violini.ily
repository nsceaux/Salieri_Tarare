\clef "treble" <sol mib'>4.\f \grace fa'16 mib' re' <sol mib'>8 q q q |
q4 sib'8. sib'16 sol'8. sol'16 mib'8. mib'16 |
lab'8. lab'16 fa'8. fa'16 re'8. re'16 sib8. sib16 |
<mib' sol>4 r r2 |
R1 |
<sol mib'>4. \grace fa'16 mib' re' <mib' sol>2:8 |
q4 <<
  \tag #'violino1 { sol'8 sol'16 sol' sol'8 sol'16 sol' sol'8 sol'16 sol' | fa'2 }
  \tag #'violino2 { mib'8 mib'16 mib' mib'8 mib'16 mib' mib'8 mib'16 mib' | mib'2 }
>> r2 |
R1*2 |
<re' sib'>4. \grace do'8 sib16 lab sib8 sib sib sib |
sib4.. <<
  \tag #'violino1 {
    <sib fa'>16 q4.. <re' sib'>16 |
    sib'1~ |
    sib' |
  }
  \tag #'violino2 {
    <sib re'>16 q4.. <re' fa'>16 |
    q1~ |
    q |    
  }
>>
r4 sib:16\p do':16 dod':16 |
<< \ru#4 { re'16( mi' fad' mi') } { s4 s2.\cresc } >> |
re'8\f mi'16( fad' sol' la'? si' dod'') re''8( mi''16 fad'' sol'' la'' si'' dod''') |
re'''4 r r2 |
R1 |
sol'16(\p la' sol' la') sib'( do'' sib' do'') re''( mib'' re'' mib'') re''(\cresc do'' sib' la') |
sol' la' sol' la' sib' do'' sib' do'' re'' mib'' re'' mib'' re'' do'' sib' la' |
sol'4\! r r2 |
sol'16(\p la' sol' la' sib' do'' sib' do'') re''( mib''!\cresc fa'' sol'' lab'' sol'' fa'' mib'') |
re''4\! r r2 |
mib''16(\ff sib' mib'' sib' mib'' sib' mib'' sib') \rt#4 { sol''16( mib'') } |
sib''4 r r2 |
R1 |
<>\ff mib''16( sib' mib'' sib' mib'' sib' mib'' sib') sol''( mib'' sol'' mib'' sol'' mib'' sol'' mib'') |
<<
  \tag #'violino1 {
    sib''16( lab'' sol'' lab'') sib'' lab'' sol'' lab'' sib'' lab'' sol'' lab'' sib'' lab'' sol'' lab'' |
    sib''4.
  }
  \tag #'violino2 {
    sol''16( fa'' mib'' fa'') sol'' fa'' mib'' fa'' sol'' fa'' mib'' fa'' sol'' fa'' mib'' fa'' |
    sol''4.
  }
>> mib'8 mib'4 mib' |
<<
  \tag #'violino1 { reb''1\fp | }
  \tag #'violino2 { <sol' sib>1\fp | }
>>
R1*2 | \allowPageTurn
fa'4..\f fa'32 sol' lab'4.. lab'32 sib' |
do''2~ do''8 do''16 do'' re'' re'' mi'' mi'' |
fa''4 r r2 |
r r4 r8 r16 <re' si> |
q1\fp~ |
q |
q4 r r2 |
r r4 <sol re' si'> |
do'8 re'16 mi' fa' sol' la' si' do''4 r |
R1 |
r4 <sol mi' do'' mi''>4 <mi' do'' sol''> r |
r2 r4 <fa' do'' fa''> |
<fa' do'' la''>16 fa'' mi'' re'' do'' sib' la' sol' fa' la' sol' fa' mi' re' do' sib |
la4 r r2 |
sib'8\f sib'16 do'' re'' mib'' fa''4 fa''16 sol''32 la''! |
sib''8 sib' sib' sib''\mf sib' sib' |
sol'' sol' sol' do''' do'' do'' |
la''! la' la' re''' re'' re'' |
sib''4 <<
  \tag #'violino1 { mib'''2.\sf~ | mib'''4 }
  \tag #'violino2 { <sol'' sib'>2.-\sug\sf~ | q4 }
>> r4 r2 |
r4 <sib' mi''>2.\ff |
<la'! fa''>4 r r2 |
<<
  \tag #'violino1 {
    fad''16(\f-. fad''-. fad''-. fad''-. fad''2.:16^\dotFour) |
    fad''2:16 fad'':16 |
    fad''2:16 fad'':16 |
    re'':16\p re'':16 |
    re'':16 re'':16 |
    re'':16 mi''!:16-\sug\fp |
    mi''4 la'' re'''16\fp re'' re'' re'' re''4:16 |
    re''2:16 re'':16 |
  }
  \tag #'violino2 {
    <la'! do''>16(-.-\sug\f q-. q-. q-. q2.:16^\dotFour) |
    <la' do''>2:16 q:16 |
    q:16 q:16 |
    sib'2:16-\sug\p sib':16 |
    sib':16 sib':16 |
    sib':16 la':16-\sug\fp |
    la'4 <la' mi''> <la' fa''>16-\sug\fp <la' fa'> q q q4:16 |
    q2:16 q:16 |
  }
>>
r16 mi'!\ff fad' sold' la' si'! dod'' red'' mi'' sold'' fad'' mi'' red'' dod'' si' la' |
sold'8 fad'16 mi' red' dod' si la sold2~ |
sold la |
<fad' red''>4 r r <si fad' red''> |
<si sol' mi''>4 r r2 |
r16 do'''\f si'' do''' sol'' do''' mi'' sol'' do'' do'' si' do'' sol' do'' mi' sol' |
do'4 r r2 |
r4 do'2:16\p <<
  \tag #'violino1 {
    do'4:16 |
    fa'2:16 fa':16 |
    fa'2:16 fa':16 |
    fa'2:16 fa':16 |
    fa'4
  }
  \tag #'violino2 {
    sib4:16 |
    la2:16 la:16 |
    la2:16 la:16 |
    la2:16 la:16 |
    la4
  }
>> r4 r\fermata
