\clef "treble" \tag #'oboi <>^"a 2"
mib'4.-\sug\f \grace fa'16 mib' re' mib'8 mib' mib' mib' |
mib'4 sib' sol' mib' |
lab' fa' re' \twoVoices#'(oboe1 oboe2 oboi) <<
  { sib'4 | sol' }
  { re'4 | mib' }
>> r4 r2 |
R1 |
\tag #'oboi <>^"a 2" mib'4. \grace fa'16 mib' re' mib'2:8 |
mib'4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''2 sol''4 | fa''2 }
  { mib''2 mib''4 | mib''2 }
>> r2 |
R1*2 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { sib'2~ sib'8 sib' sib' sib' |
    sib'4.. fa''16 fa''4.. sib''16 |
    sib''2 }
  { re'2~ re'8 re' re' re' |
    re'4.. re''16 re''4.. re''16 |
    re''2 }
>> r2 |
R1*3 |
<>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { la'2 fad''~ | fad''4 }
  { fad'2 la'!~ | la'4 }
>> r4 r2 |
R1 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { re''1 | re'' | re''4 }
  { sib'2 la' | sib' la' | sib'4 }
  { s2\p s\cresc | s1 | s4\! }
>> r4 r2 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { re''1 | re''4 }
  { sib'2 lab'! | lab'4 }
  { s2\p s\cresc | s4\! }
>> r4 r2 |
<>-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { mib''2 sol'' | sib''4 }
  { sol'2 sib' | sib'4 }
>> r4 r2 |
R1 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mib''2 sol'' |
    sib''8 sib''16 sib'' sib''8 sib''16 sib'' sib''8 sib''16 sib'' sib''8 sib''16 sib'' |
    sib''4. mib'8 mib'4 mib' |
    reb''1 | }
  { sol'2 sib' |
    sib'8 sib'16 sib' sib'8 sib'16 sib' sib'8 sib'16 sib' sib'8 sib'16 sib' |
    sib'4. mib'8 mib'4 mib' |
    sol'1 | }
  { s1*3-\sug\ff s1-\sug\fp }
>>
R1*2 |
\tag #'oboi <>^"a 2" fa'2-\sug\f lab' |
do'' r8 do'' re'' mi'' |
fa''4 r r2 |
r r4 r8 r16 sol' |
sol'1\fp~ |
sol' |
sol'4 r r2 |
r r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { si'4 | do''2~ do''4 }
  { re'4 | mi'2~ mi'4 }
>> r4 |
R1 |
r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''4 sol'' }
  { sol' do'' }
>> r4 |
r2 r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''4 | la'' }
  { la' | do'' }
>> r4 r2 |
R1 |
\tag #'oboi <>^"a 2" sib'8-\sug\f sib'16 do'' re'' mib'' fa''4 fa''16 sol''32 la''! |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { sib''4 s8 sib''4 s8 |
    sol''4 s8 do'''4 s8 |
    la''!4 s8 re'''4 s8 |
    sib''4 sib''2.~ |
    sib''4 }
  { sib''4 s8 fa''4 s8 |
    sib'4 s8 sol''4 s8 |
    do''4 s8 la''4 s8 |
    re''4 sol''2.~ |
    sol''4 }
  { s4 r8 s4-\sug\mf r8 |
    s4 r8 s4 r8 |
    s4 r8 s4 r8 |
    s4 s2.-\sug\sf }
>> r4 r2 |
R1*2 | \allowPageTurn
\twoVoices#'(oboe1 oboe2 oboi) <<
  { fad''1~ | fad''~ | fad'' | sol''4 }
  { do''1~ | do''~ | do'' | sib'4 }
  { s1-\sug\f }
>> r4 r2 | \allowPageTurn
R1*15 |
r2 s4\fermata
