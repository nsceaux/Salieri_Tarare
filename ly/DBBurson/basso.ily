\clef "bass" mib4\f sol sib sol |
mib sib sol mib |
lab fa re sib, |
mib4 r r2 |
R1 |
mib4 sol sib sol |
mib mib' do' sib |
la2 r |
R1*2 |
sib,4 re fa re |
sib,4.. sib,16 sib,4.. sib,16 |
sib,1~ |
sib, |
r4 sib,(\p do dod) |
re8 re re\cresc re re2:8 |
re2:8\f re:8 |
re4 r r2 |
R1 |
sol2:8\p fad:8\cresc |
sol:8 fad:8 |
sol4\! r r2 |
sol2:8\p fa8\cresc fa fa re |
sib,4\! r r2 |
mib2:8\ff mib:8 |
mib4 r r2 |
R1 | \allowPageTurn
mib2:8\ff mib:8 |
mib:8 mib:8 |
mib4. mib8 mib4 mib |
mi1\fp |
R1*2 |
fa4..\f fa32 sol lab4.. lab32 sib |
do'2~ do'8 do re mi |
fa4 r r2 |
r r4 r8 r16 sol, |
sol,1\fp~ |
sol,~ |
sol,4 r r2 |
r r4 sol |
do8 re16 mi fa sol la si do'4 r |
R1 |
r4 do' do r |
r2 r4 fa |
fa,1 |
fa,4 r r2 |
sib,4.\f fa |
sib re\mf |
mib mi |
fa fad |
sol4 \clef "tenor" <>^"Violoncelli" mib'2.-\sug\sf~ |
mib'4 r r2 |
\clef "bass" <>^"Tutti" r4 do2.\ff |
fa4 r r2 |
re'2\ff r |
R1*2 |
sol2:16\p sol:16 |
sol:16 sol:16 |
sol:16 dod:16\fp |
dod:16 re:16\fp |
re:16 re:16 |
mi4 r r2 |
sold8-\sug\ff fad16 mi red dod si, la, sold,2~ |
sold, la, |
la4 r r si |
mi4 r r2 |
mi4\f r do8 do' sol mi |
do4 r r2 |
r4 do(\p re mi!) |
fa2:16 fa:16 |
fa:16 fa:16 |
fa:16 fa:16 |
fa4 r r\fermata
