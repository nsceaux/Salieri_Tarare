\clef "bass" R1*3 |
r4 r8 sib sib sib sib sib |
sol4. sol8 sol sol lab sib |
mib4 r r2 |
R1 |
r8 fa fa sol la la la sib |
do' do' r do' la la16 la la8 sib16 do' |
fa8 fa r fa la la16 sib do'8 do'16 re' |
sib4 r r2 |
R1 |
r2 r8 fa fa fa |
sib4. fa8 sib4 re' |
sib4 sib r2 |
R1*2 |
r8 la la la re' re' re' dod' |
re' re' r re' re' la la16 sib do'! re' |
sib4 r r2 |
R1 |
r4 re'8 re' sib4 sib16 sib do' re' |
sol4 sol r2 |
r4 sib16 sib sib do' re'4 re'8 mib' |
sib4 r r2 |
r4 r8 sol sol sol16 lab sib8 sib16 sib |
mib'8 mib' r mib'16 mib' mib'8. sib16 sol8 sol16 sib |
\sug mib4 r r2 |
R1*2 |
r2 r4 reb'8. reb'16 |
sib2 sib8 sib do' reb' |
mi mi sol sol16 lab sib8 sib sib do' |
lab4 r r2 |
R1 |
r8 fa fa sol lab4 lab8 lab |
lab4*2 lab8 lab sol sol |
r2 r4 sol8 sol |
re'2 re'8 re' do' re' |
si4 r8 re sol4 sol8 la |
si4 si8 do' sol4 r |
r2 r8 sol16 sol sol8 sol16 sol |
do'4 mi'!8 mi' mi' mi' re' mi' |
do' do' r4 r r8 sol16 sol |
do'4 do'16 do' sib do' la4 r |
r16*2 do' do' do' do'8 do' do' fa' |
fa' do' do' do' la8. la16 do'8 do'16 re' |
sib4. r4 r8 |
r8 r sib sib sib sib |
sol sol sol do' do' do' |
la!4 la8 re' re' re' |
sib sib mib'4. sib8 sib sib |
sol4 r16 mib mib fa sol8 sol sol lab |
sib8. sol16 mi'!4. fa'16 sol' do' do'32 do' do'16 sib32 do' |
la!4 r16 la16 la sib do'8. do'16 do'8. la16 |
fad'2 fad'8 re' re' re' |
re'8 la!16 la fad8 sol16 la re4
\ffclef "bass" <>^\markup\character Atar
r8 fad |
la!4 la8 sib do'4 do'8 re' |
sib4
\ffclef "bass" <>^\markup\character Urson
\sug r8 sol16 sol sol4 sol8 la |
sib4 re'8 re' sib4 sib8 re' |
sol8 sol r sib la! la16 la la8 la16 si |
dod'8 dod' mi'! dod'16 la re'4 r16 la la la |
re'4. re'8 re' si!16 si si8 si16 si |
sold4 sold8 si mi'2 |
mi'4 r r r8 mi' |
mi' mi' re' mi' do' do' r16 do' si la |
red'8. red'16 red'8 mi' si4 r |
r4 r8 si si si16 si mi'8 re'16 mi' |
do'8 do' r4 r2 |
do'8 do' do' do' do'4 do'8 do' |
sol4 r r2 |
r r8 fa fa sol |
la8. la16 la8 sib do'4 r |
do'8 do' do' do' la4 la8 do' |
fa fa r4 r\fermata
