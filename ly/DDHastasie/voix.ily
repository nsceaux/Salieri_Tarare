\clef "soprano/treble" r4 r8 la' dod''4. dod''16 dod'' |
dod''4 dod''8 re'' mi'' mi'' r mi'' |
sol''4 dod''8 re'' mi''4 mi''8 fad'' |
re'' re'' r4 r2 |
