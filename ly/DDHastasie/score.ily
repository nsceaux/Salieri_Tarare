\score {
  \new Staff \with {
    instrumentName = \markup\character Astasie
    shortInstrumentName = \markup\character As.
  } \withLyrics <<
    \global \includeNotes "voix"
  >> \includeLyrics "paroles"
  \layout { system-count = 1 }
  \midi { }
}
