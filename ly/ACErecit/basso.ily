\clef "bass" lab,2 r |
r4 lab( fa mib) |
re2 r |
r4 sib,8. do16 re4 sib, |
si,!4 r r2 |
do4 r r2 |
r r4 <>^"Violoncelle solo" fa |
mib1~ |
mib2 re4 r |
r2 <>^"tutti" dod\p~ |
dod( re4) r |
R1 |
re4\mf r r2 |
sol4( si! la fad) |
sol r8 sol, re4 fad |
sol r r2 |
R1 |
r8. do16 do4 r2 |
r8 r16 do\mf do4 r2 |
r8 r16 do\f do4~ do2\p |
r8 r16 fa\f fa8. fa16 fa2~ |
fa r |
fad1\p~ |
fad~ |
fad |
sol4 r re(\mf do) |
sib,(\f sol, la,) la, |
sib, r r2 |
R1 |
mib4 r r2 |
R1 |
<>^"Violoncelli" lab1~ |
lab2 mi!~ |
mi1 |
r8 r16 fa\f fa8. fa16 fa2~ |
fa\p fa~ |
fa mib |
fad r4 sol\p |
R1 |
r2 sol |
fa4 r r2 |
r sold |
la4 r r2 |
R1*2 |
