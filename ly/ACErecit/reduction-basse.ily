\clef "bass" lab,2 r |
r4 lab( fa mib) |
re2 r |
r4 <re sib,>8. <mib do>16 <fa re>4 <sib, re fa>~ |
<si,! fa re>4 r r2 |
do4 r r2 |
r r4 fa |
mib1~ |
mib2 re4 r |
r2 dod~ |
dod( re4) r |
R1 |
<re la>4 r r2 |
<< { si8 re'4 re' re' re'8 } \\ { sol4( si! la fad) } >> |
<sol re'>4. <si sol>8 << { la8 sol16 fad } \\ re4 >> fad8 la |
<sol si>4 r r2 |
R1 |
r8. do16 do4 r2 |
r8 r16 do do4 r2 |
r8 r16 do do4~ do2 |
r8 r16 fa fa8. fa16 fa2~ |
fa r |
<fad re'>1~ |
q~ |
q |
<sol re'>4 r re8 la do re |
<sib, re>4 <sol, sib,> <la, do>4 <fa la,> |
<sib, fa> r r2 |
R1 |
<mib sib>4 r r2 |
R1 |
lab1~ |
lab2 mi!~ |
mi1 |
r8 r16 <fa do'> q8. <fa lab>16 q2~ |
q fa~ |
fa mib |
fad r4 sol |
R1 |
r2 sol |
fa4 r r2 |
r sold |
la4 r r2 |
R1*2 |
