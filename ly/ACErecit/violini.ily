\clef "treble"
<<
  \tag #'violino1 {
    mib'2 r |
    r4 do''2 do''4 |
    sib'2 r |
    r4 sib'2 \grace sib'8 lab' sol'16 lab' |
    sol'4 r r2 |
    sol'4 r r2 |
    r r4 la' |
    do''1~ |
    do''2 sib'4 r |
    r2 la'\p~ |
    la'2~ la'4 r |
    R1 |
    r8 \acciaccatura sol'' fa''16(-\sug\mf mi'' fa''8 sol'' la'' sol'' fa'' mi'') |
    re''4. sol''8( sol'' fad'' la'' do''') |
    si''( re''' si'' sol'') \acciaccatura sol'' fad'' mi''16 re'' re''8 re'' |
    re''4 r r2 |
    R1 |
    r8. do'''16 do'''4 r2 |
    r8 r16 <mi'' sib'>\mf q4 r2 |
    r8 r16 <mi' sol>16\f q4~ q2\p |
    r8. <la' fa''>16\f q8. q16 q2~ |
    q r |
    re''1\p |
    do''~ |
    do'' |
    sib'8 sib'([\mf do'' re'']) re'' fad' \acciaccatura sol'8 fad' mi'16 fad' |
    sol'8(\f re'' mib'' sol'' fa'' do'' \acciaccatura re''8 do'' sib'16 do'') |
    re''4 r r2 |
    R1 |
    mib''4 r r2 |
    R1 |
    do''1~ |
    do''2 reb''~ |
    reb''1 |
    r8 r16 lab'\f lab'8. do''16 do''2~ |
    do''\p si'!~ |
    si' do'' |
    re'' r4 sib'\p |
    R1 |
    r2 dod'' |
    re''4 r r2 |
    r si'! |
    do''4 r r2 |
    R1*2 |
  }
  \tag #'violino2 {
    do'2 r |
    r4 lab'2 lab'4 |
    lab'2 r |
    r4 re'8. mib'16 fa'4 re'~ |
    re' r r2 |
    mib'4 r r2 |
    r r4 do' |
    la'1~ |
    la'2 sib'4 r |
    r2 mi'-\sug\p~ |
    mi'( fa'4) r |
    R1 | \allowPageTurn
    r8 \acciaccatura sol'8 fa'16(\mf mi' fa'8 sol' la' sol' fa' mi') |
    re'4. \once\slurDashed sol'8( sol' fad' la' do'') |
    si'( re'' si' sol') \acciaccatura sol'8 fad' mi'16 re' re'8 la' |
    si'4 r r2 |
    R1 |
    r8. <sol' mi''>16 q4 r2 |
    r8 r16 <sol' sib'>16-\sug\mf q4 r2 |
    r8 r16 sib\f sib4~ sib2\p |
    r8. <fa' do''>16-\sug\f q8. q16 q2~ |
    q r2 |
    la'1-\sug\p~ |
    la'~ |
    la' |
    re'8 sib([-\sug\mf do' re']) re' fad' \acciaccatura sol' fad' mi'16 fad' |
    \once\slurDashed sol'8(\f re' mib' sol' fa' do' \acciaccatura re' do' sib16 do') |
    re'4 r r2 |
    R1 |
    sol'4 r r2 |
    R1 |
    mib'1~ |
    mib'2 sol'~ |
    sol'1 |
    r8 r16 fa'-\sug\f fa'8. lab'16 lab'2~ |
    lab'-\sug\p \once\tieDashed sol'~ |
    sol' sol' |
    re' r4 re'-\sug\p |
    R1 |
    r2 mi' |
    re'4 r r2 |
    r mi' |
    mi'4 r r2 |
    R1*2 |
  }
>>
