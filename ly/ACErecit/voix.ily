\clef "bass" <>^\markup\character-text Le Génie à la Nature
r2_\markup\italic { parlé } r8 lab lab do' |
lab4 r r2 |
r4 r8 fa16 fa sib4 sib8 sib |
sib4 fa r2 |
<>^\markup\italic { à l’ombre } r16 sol sol la! si!4 r8 si16 si si8 si16 sol |
do'8 do' r4
\ffclef "soprano/treble" <>^\markup\character L'ombre
sol'4 sol'8 r |
\ffclef "bass" <>^\markup\character Le Génie
r8 do' do' sol la4 r |
\ffclef "soprano/treble" <>^\markup\character L'ombre
r2 r4 la'8 sib' |
do''4 la'8 fa' sib'4
\ffclef "bass" <>^\markup\character Le Génie
r8 fa16 fa |
sib4 sib8 sib la4 r8 mi |
sol sol sol la fa4
\ffclef "soprano/treble" <>^\markup\character-text La Nature souriant
r16 la' la' la' |
re''8 re'' re'' fa'' re'' re'' r4 |
R1*3 |
\ffclef "bass" <>^\markup\character-text Le Génie à l’Ombre de Spinette
r4 r8 sol re' re' do' re' |
si4 si8 do' re'4 si8 sol |
do'4 r4
\ffclef "soprano/treble" <>^\markup\character L'Ombre de Spinette
r4 r8 sol'16 la' |
sib'4 r r r8 sib'16 sib' |
mi''!4 r8\fermata mi''16 sol'' mi''4 mi''8 fa'' |
do'' do'' r4
\ffclef "bass" <>^\markup\character-text Le Génie à la Nature
do'2~ |
do'4. la8 fa fa
\ffclef "soprano/treble" <>^\markup\character La Nature
r8 fa'' |
re''4 r8 re''16 re'' la'4 la'8 sib' |
do''4. do''8 do'' do'' re'' mib'' |
re''4. la'8 do'' do'' do'' re'' |
sib'4 r r2 |
R1 |
\ffclef "bass" <>^\markup\character-text Le Génie à l’Ombre d’Astasie
r4 r8 sib re' re' do' re' |
sib4 sib8 sib sib4 lab8 sib |
sol sol r sol sol sol sol lab |
sib4 r8 sib sib sib mib' sib |
do'4 r
\ffclef "soprano/treble" <>^\markup\character L'Ombre d'Astasie
r4 r8 mib''16 mib'' |
do''8 do''16 do'' do''8 do'' reb'' reb'' r reb'' |
reb''4. sol'8 sol' lab' sib' do'' |
lab'4 r r2 |
\ffclef "soprano/treble" <>^\markup\character-text La Nature d’un ton imposant
do''4 do''8 re'' si'!4 r8 sol'16 la' |
si'4 si'8 do'' sol' sol' r do''16 mib'' |
re''8 re'' la'16 sib' do'' re'' sib'4 r |
\ffclef "soprano/treble"
<>^\markup\column {
  \smallCaps L'Ombre d'Astasie
  \italic étonnée, la main sur son cœur
}
r4 r8 sol' sib'4 sib'8 r |
\ffclef "soprano/treble" <>^\markup\character La Nature
re''8 re'' re'' mi'' dod''4 dod''8 dod''16 re'' |
la'8 la' r4
\ffclef "soprano/treble" <>^\markup\character-text L'Ombre d'Astasie troublée
r8 la' la' la' |
fa'4 r
\ffclef "soprano/treble" <>^\markup\character La Nature
si'8 si'16 do'' re''8 mi'' |
do''4
\ffclef "bass" <>^\markup\character Le Génie
r8 mi mi mi mi mi |
la4 la8 la do'4 si8 do' |
la la r4 r2 |
