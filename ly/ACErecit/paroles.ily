 Un mot en -- cor ; c’est une om -- bre fe -- mel -- le.

Ai -- mable en -- fant, vou -- lez- vous ê -- tre bel -- le ?

Bel -- le ?

Vous rou -- gis -- sez !

Suis- je donc sans ap -- pas ?

Son ins -- tinct la tra -- hit, mais ne la trom -- pe pas !

Il peut au moins la com -- pro -- met -- tre.

Et vous dont les re -- gards cau -- se -- ront cent dé -- bats ?

Je vou -- drais… je vou -- drais… je vou -- drais tout sou -- met -- tre.

Ô ! Na -- tu -- re !

J’ai tort ; de -- vant vous j’ai tra -- hi,
sur ses plus doux se -- crets, mon se -- xe fa -- vo -- ri.

Mais vous, jeu -- ne beau -- té, qui sem -- blez a -- ni -- mé -- e,
vou -- dri -- ez- vous à tous don -- ner aus -- si la loi ?

Que je sois seu -- le -- ment ai -- mé -- e…
il n’est que ce bon -- heur pour moi.

Tu le se -- ras, sous le nom d’As -- ta -- si -- e,
et Ta -- ra -- re ob -- tien -- dra ta foi.

Ta -- ra -- re !

Je te fais un sort di -- gne d’en -- vi -- e.

Je n’en sais rien.

Moi, je le sais pour toi.

Voy -- ez quel -- le rou -- geur à ce nom l’a sai -- si -- e !
