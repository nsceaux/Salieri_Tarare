\clef "alto" lab2 r |
r4 do'2 do'4 |
fa'2 r |
r4 sib8. do'16 re'4 fa'~ |
fa' r r2 |
mib'4 r r2 |
r r4 fa' |
fa'1~ |
fa'2 fa'4 r |
r2 la-\sug\p~ |
la2~ la4 r |
R1 |
la4\mf r r2 |
si8 re'4 re' re' re'8 |
re'4. si8 la sol16 fad fad8 la' |
si'4 r r2 |
R1 |
r8. do'16 do'4 r2 |
r8 r16 do'-\sug\mf do'4 r2 |
r8 r16 do'-\sug\f do'4~ do'2-\sug\p |
r8. fa16-\sug\f fa8. fa16 fa2~ |
fa r2 |
\tieDashed re'1-\sug\p~ |
re'~ |
re' \tieSolid |
re'4 r r8 la4-\sug\mf re'8 |
re'4\f sib do'8( la fa) fa |
fa4 r r2 |
R1 |
sib4 r r2 |
R1 |
lab~ |
lab2 sib'~ |
sib'1 |
r8 r16 do'-\sug\f do'8. lab16 \tieDashed lab2~ |
lab-\sug\p re'!~ |
re' \tieSolid do' |
la' r4 sol'-\sug\p |
R1 |
r2 la' |
la'4 r r2 |
r sold |
la4 r r2 |
R1*2 |
