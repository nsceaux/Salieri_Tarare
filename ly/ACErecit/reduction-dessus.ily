\clef "treble" <do' mib'>2 r |
r4 <lab' do''>2 q4 |
<fa' lab' sib'>2 r |
r4 sib'2 \grace sib'8 lab' sol'16 lab' |
sol'4 r r2 |
<mib' sol'>4 r r2 |
r r4 <do' fa' la'> |
<fa' la' do''>1~ |
q2 <fa' sib'>4 r |
r2 <mi' la'>\p~ |
q2~ q4 r |
R1 |
r8 \acciaccatura sol'' fa''16(-\sug\mf mi'' fa''8 sol'' la'' sol'' fa'' mi'') |
re''4. sol''8( sol'' fad'' la'' do''') |
si''( re''' si'' sol'') \acciaccatura sol'' fad'' mi''16 re'' re''8 re'' |
re''4 r r2 |
R1 |
r8. <mi'' sol'' do'''>16 q4 r2 |
r8 r16 <mi'' sib' sol'>\mf q4 r2 |
r8 r16 <mi' sib sol>16\f q4~ q2\p |
r8. <la' do'' fa''>16\f q8. q16 q2~ |
q r |
<la' re''>1\p~ |
<la' do''>~ |
q |
sib'8 sib'([\mf do'' re'']) re'' fad' \acciaccatura sol'8 fad' mi'16 fad' |
sol'8(\f re'' mib'' sol'' fa'' do'' \acciaccatura re''8 do'' sib'16 do'') |
re''4 r r2 |
R1 |
<sol' mib''>4 r r2 |
R1 |
<mib' do''>1~ |
q2 <sol' sib' reb''>~ |
q1 |
r8 r16 <fa' lab'>\f q8. <lab' do''>16 q2~ |
q\p <re' sol' si'!>~ |
q <sol' do''> |
<la' re''> r4 <re' sib'>\p |
R1 |
r2 <mi' la' dod''> |
<la' re''>4 r r2 |
r <mi' si'!> |
<mi' do''>4 r r2 |
R1*2 |
