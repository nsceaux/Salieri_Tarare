\clef "bass" r2 |
R1 |
r4 r8 fa\p fa4 fa |
fa2 r |
R1 |
r2 dod4 r |
R1 |
re4 r r2 |
re4 r r2 |
r4 mi la, r |
r2 r8 r16 sol\f sol4~ |
sol2 r4 la |
re8 mi16 fad sol la si dod' re'8 la fad la |
re4 r r2 |
R1 |
sol4 r r2 |
R1 |
mi4 r sold2 |
la4 r r2 |
mi4 r r2 |
R1 |
fa4 r r2 |
r fad4\ff r |
sol\p r r2 |
R1 |
fa2\fp mib4 r |
re r r sol |
