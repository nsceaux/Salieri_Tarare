\clef "treble" r2 |
R1 |
r4 r8 <<
  \tag #'violino1 { do'8\p do'4 do' | do'2 }
  \tag #'violino2 { la8-\sug\p la4 la | la2 }
>> r2 |
R1 |
r2 <<
  \tag #'violino1 { la'4 }
  \tag #'violino2 { mi' }
>> r4 |
R1 |
<<
  \tag #'violino1 { la'4 }
  \tag #'violino2 { fa' }
>> r4 r2 |
<<
  \tag #'violino1 { sold'4 }
  \tag #'violino2 { mi'! }
>> r4 r2 |
r4 <mi' si' sold''> <mi' dod'' la''> r |
r2 r8 r16 <<
  \tag #'violino1 { <la' dod'''>16\f q4~ | q2 }
  \tag #'violino2 { <dod'' mi''>16-\sug\f q4~ | q2 }
>> r4 <la' mi'' dod'''>4 |
re''8 mi''16 fad'' sol'' la'' si'' dod''' re'''8 la''16 la'' fad''8 la'' |
re''4 r r2 |
R1 | \tag #'violino2 \allowPageTurn
<re' si' si''>4 r r2 |
R1 |
<<
  \tag #'violino1 { <sol' mi'' do'''>4 r <mi'' mi'>2 | <mi' do''>4 }
  \tag #'violino2 { <mi' do'' sol''>4 r <mi' si'>2 | <mi' do''>4 }
>> r4 r2 |
<mi' do'' sol''>4 r r2 |
R1 |
<fa' do'' la''>4 r r2 |
r2 <la fad'>4\ff r |
<re' sib>4\p r r2 |
R1 |
<<
  \tag #'violino1 { re''2\fp mib''4 r | si'! }
  \tag #'violino2 { lab'2-\sug\fp sol'4 r | fa' }
>> r4 r <sol re' si'>4 |
