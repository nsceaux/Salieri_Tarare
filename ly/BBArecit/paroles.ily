Mais qu’an -- nonce Al -- ta -- mort, à mon im -- pa -- ti -- en -- ce ?

Mon maître est o -- bé -- i ; tout est fait, rien n’est su.

As -- ta -- si -- e ?

Est à toi, sans qu’on m’ait a -- per -- çu,
sans qu’elle ait de -- vi -- né qui la veut, qui l’en -- lè -- ve.

Au rang de mes vi -- zirs, Al -- ta -- mort, je t’é -- lè -- ve.

Pour la bien re -- ce -- voir sont- ils tous pré -- pa -- rés ?
Le sé -- rail est- il prêt, les jar -- dins dé -- co -- rés,
Cal -- pi -- gi ?

Tout, Sei -- gneur.

Qu’u -- ne su -- per -- be fê -- te,
de -- main, de ma gran -- deur en -- i -- vre ma con -- quê -- te.

De -- main ? Le terme est court.

Mal -- heu -- reux !

Vous l’au -- rez.

J’ai par -- lé : tu m’en -- tends ? S’il man -- que quel -- que cho -- se…

Man -- quer ! cha -- cun sait trop à quel mal il s’ex -- po -- se.
