\clef "bass" <>^\markup\character Atar
sol8. sol16 sol8 sol16 la |
do'4. sol8 sib sib do' sol |
la la r4 r2 |
\ffclef "bass" <>^\markup\character Altamort
r4 r8 fa la la sol la |
fa4 r8 fa16 sol la4 r8 la16 do' |
fa4
\ffclef "bass" <>^\markup\character Atar
r8 fa16 sol la8 la
\ffclef "bass" <>^\markup\character Altamort
r8 la16 la |
mi4 mi8 fa sol4 sol8 la |
fa4 r8 la re' la la si! |
sold4 r8 mi16 fad sold4 r8 sold16 la |
mi8 mi r4
\ffclef "bass" <>^\markup\character-text Atar gravement
r4 r8 la |
la8. la16 la8 si dod'4 r8 mi'16 mi' |
dod'4 dod'8 re' la la r4 |
R1 |
r2 r8 <>^\markup\italic vite à Calpigi la16 la la8 la16 la |
re'4 la8 si do'4 re'8 la |
si4 r8 re'16 re' re'4 do'8 re' |
si4 r8 si16 do' re'4 si8 sol |
do'4 r8 do'16 re' mi'4
\ffclef "alto/G_8" <>^\markup\character Calpigi mi'8 mi' |
do'4 r
\ffclef "bass" <>^\markup\character Atar
la8 la16 la si8 do' |
do' sol r sol do' do' do' re' |
sib4 r8 sol sib sib sib la |
fa fa
\ffclef "alto/G_8" <>^\markup\character Calpigi
r8 do' fa'4 r16 la sib do' |
fa4
\ffclef "bass" <>^\markup\character Atar
r8 do'16 mib' re'4
\ffclef "alto/G_8" <>^\markup\character Calpigi
r8 re'16 re' |
sib4
\ffclef "bass" <>^\markup\character Atar
r8 sol16 la sib4 r8 sib16 re' |
sol4 r8
\footnoteHere#'(0 . 0) \markup {
  Source : \score {
    \new Staff \withLyrics {
      \clef "bass" \tinyQuote
      sol4 r8 sib16*2 sib sib8 sib do' | \noBreak re' re'
    } \lyrics { -tends s’il man -- que quel -- que cho -- se, }
    \layout { \quoteLayout }
  }
}
sib sib sib8 sib do' |
re' re'
\ffclef "alto/G_8" <>^\markup\character Calpigi
r8 sib mib'4 r16 mib' fa' sol' |
si!8 sol16 la si8 si16 do' sol8 sol r4 |
