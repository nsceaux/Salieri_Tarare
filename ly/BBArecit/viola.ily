\clef "alto" r2 |
R1 |
r4 r8 fa-\sug\p fa4 fa |
fa2 r |
R1 |
r2 dod'4 r |
R1 |
re'4 r r2 |
si!4 r r2 |
r4 mi' la r |
r2 r8 r16 sol'\f sol'4~ |
sol'2 r4 la' |
re'8 mi'16 fad' sol' la' si' dod'' re''8 la' fad' la' |
re'4 r r2 |
R1 |
sol'4 r r2 |
R1 |
mi'4 r sold'2 |
la'4 r r2 |
mi'4 r r2 |
R1 |
fa'4 r r2 |
r fad'4-\sug\ff r |
sol'-\sug\p r r2 |
R1 |
fa'2-\sug\fp mib'4 r |
re'_\markup\tiny\center-align "[Source : do]" r r sol' |
