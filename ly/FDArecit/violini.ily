\clef "treble" R1*2 |
<mi' si'! sold''>4 r r2 |
R1 |
<do'' la''>4 r r2 |
<<
  \tag #'violino1 {
    sol''!4 r4 r2 |
    r2 mi''\fp~ |
    mi''1~ |
    mi''2 fa''~ |
    fa'' dod''~ |
    dod'' re''~ |
    re''1~ |
    re''4 r <re'' re'>\f r |
  }
  \tag #'violino2 {
    do''4 r r2 |
    r2 sib'-\sug\fp~ |
    sib'1~ |
    sib'2 \once\tieDashed la'~ |
    la' sol'~ |
    sol' fa'~ |
    fa'1~ |
    fa'4 r <re' la'>-\sug\f r |
  }
>>
r2 r8 r16 <re' sib'> q4 |
r2 r8 r16 <<
  \tag #'violino1 {
    <re'' re'>16\p q8 r\fermata |
    <mib' mib''>1\fp~ |
    q |
  }
  \tag #'violino2 {
    lab'16-\sug\p lab'8 r\fermata |
    <sol' sib>1-\sug\fp~ |
    q |
  }
>>
r4 lab16(\f sib do' sib) lab( sib do' reb' mib'? reb' do' sib) |
lab4 r r2 |
<<
  \tag #'violino1 {
    <mib' do''>4 r r2 |
    reb''4
  }
  \tag #'violino2 {
    <lab' lab>4 r r2 |
    q4
  }
>> reb'16( mib' fa' mib') reb'( mib' fa' solb') lab'?( solb' fa' mib') |
reb'4 r r2 |
R1 |
<<
  \tag #'violino1 { do''4 }
  \tag #'violino2 { sol'!4 }
>> r4 r2 |
<lab' do'>4\f r r2 |
r8 fa'16 fa' sol' sol' lab' lab' sib' sib' do'' do'' re'' re'' mi'' mi'' |
fa''4 r r2 |
<<
  \tag #'violino1 {
    r8 si'!16(\f do'' re'' mib'' fa'' sol'') lab''4
  }
  \tag #'violino2 {
    r8 si!16-\sug\f( do' re' mib' fa' sol') lab'4
  }
>> r4 |
r <sol' re'' si''> <sol' mib'' do'''>2 |
