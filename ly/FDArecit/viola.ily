\clef "alto" R1*2 |
mi'!4 r r2 |
R1 |
la'4 r r2 |
mi'4 r r2 |
r sol'-\sug\fp~ |
sol'1~ |
sol'2 do'~ |
do' mi'~ |
mi' la~ |
la1~ |
la4 r fad-\sug\f r |
r2 r8 r16 sol sol4 |
r2 r8 r16 si-\sug\p si8 r\fermata |
<sol mib'>1-\sug\fp~ |
q |
r4 \once\slurDashed lab16(\f sib do' sib) lab8 do' mib' do' |
lab4 r r2 |
solb'4 r r2 |
fa'4 \once\slurDashed reb'16( mib' fa' mib') reb'8 fa' lab' fa' |
reb'4 r r2 |
R1 | \allowPageTurn
mi!4 r r2 |
fa8\f fa16[ fa] sol sol lab lab sib sib do' do' re' re' mi' mi' |
fa'8 fa16 fa sol sol lab lab sib? sib do' do' re' re' mi' mi' |
fa'4 r r2 |
R1 |
r4 sol' do'2 |
