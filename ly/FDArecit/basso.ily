\clef "bass" R1*2 |
mi!4 r r2 |
R1 |
la4 r r2 |
mi4 r r2 |
r do\fp~ |
do1~ |
do2 fa~ |
fa mi~ |
mi \tieDashed re~ |
re1~ |
re4 \tieSolid r fad r |
r2 r8 r16 sol sol4 |
r2 r8 r16 fa!\p fa8 r\fermata |
mib1\fp~ |
mib |
r4 \once\slurDashed lab,16(\f sib, do sib,) lab,8 do mib do |
lab,4 r r2 |
solb4 r r2 |
fa4 \once\slurDashed reb16( mib fa mib) reb8 fa lab fa |
reb4 r r2 |
R1 |
mi!4 r r2 |
fa8\f fa,[ sol, lab,] sib,[ do re! mi!] |
fa fa sol lab sib do' re' mi' |
fa'4 r r2 |
R1 |
r4 sol do2 |
