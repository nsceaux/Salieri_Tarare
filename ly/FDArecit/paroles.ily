Ain -- si donc, a -- bu -- sant de vos char -- mes,
fausse Ir -- za, par de fein -- tes lar -- mes,
vous tri -- om -- phiez de me trom -- per ?
Je pré -- tends, a -- vant de frap -- per,
sa -- voir com -- ment ma puis -- san -- ce jou -- é -- e…

Une es -- cla -- ve fi -- dè -- le, hé -- las ! subs -- ti -- tu -- é -- e,
in -- no -- cem -- ment cau -- sa le dé -- sordre et l’er -- reur.

Ah ! cet -- te voix me fait hor -- reur !

Il est donc vrai, cet é -- chan -- ge fu -- nes -- te !
j’a -- do -- rais sous le nom d’Ir -- za…

Va, mal -- heu -- reu -- se, je dé -- tes -- te
l’in -- digne a -- mour qui pour toi m’em -- bra -- sa.
À la ri -- gueur des loix, a -- vec lui, sois li -- vré -- e !

Pon -- ti -- fe, dé -- ci -- dez leur sort.

Ils sont ju -- gés : le -- vez l’é -- ten -- dard de la mort.
De leurs jours cri -- mi -- nels la trame est dé -- chi -- ré -- e.
