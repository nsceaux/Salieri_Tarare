\clef "bass" <>^\markup\character-text Atar à Astasie enchaînée
r4 r8 sol16 la si4 si8 si |
si4 si8 do' re' re' r re'16 si |
sold4 r sold8 sold la si |
mi mi r16 si si do' re'8 re' mi' si |
do'4 r8 mi16 mi la8. la16 la8 la16 si |
do'4 r16 do' do' do' sol4 sol8 sol |
sol4 sol8 la sib sib
\ffclef "soprano/treble" <>^\markup\character Spinette
r8 sib'16 sib' |
sib'4 sib'8 sib' mi'' mi'' r sol'' |
mi'' mi'' re'' do'' fa'' fa'' r fa'' |
fa'' fa'' mi'' re'' dod''4 r8 la'16 si' |
dod''4 dod''8 re'' la'4 r |
\ffclef "tenor/G_8" <>^\markup\character-text Tarare sans regarder
la4 la8 re' re' la la la |
fa4
\ffclef "bass" <>^\markup\character Atar
r16 la la la re'4 la8 sib |
do'4 re'8 la sib sib r sol16 la |
\footnoteHere#'(0 . 0) \markup {
  Source : \score {
    \new Staff \withLyrics {
      \tinyQuote \clef "bass"
      sib8 sib sib sib do' re'4 r4*1/2\fermata
    } \lyricmode {
      - rais sous le nom d’Ir -- za
    }
    \layout { \quoteLayout }
  }
}
sib8 sib16 sib sib8 do' re'4 r\fermata |
<>^\markup\italic à Astasie.
mib'4 sib8 sib sol sol r sol16 lab |
sib8 sib16 sib do'8 reb' sol sol16 lab sib8 sib16 do' |
lab4 r r2 |
r lab8 lab16 lab lab8 sib |
do'4 r8 do'16 mib' do'4 do'8 reb' |
lab lab r4 r2 |
r4 <>^\markup\italic au grand Prêtre
r8 lab reb'4 reb'8 r |
lab lab lab lab fa4
\ffclef "bass" <>^\markup\character Arthénée
r16 lab lab reb' |
do'4 r8 do' do' sol16 lab sib8 sib16 do' |
lab4 r r2 |
R1 |
r4 r8 do'16 do' do'4 do'8 re' |
si!4 r8 fa' fa'8. si16 si8 do' |
sol sol r4
\tag #'recit <>^\markup\override#'(baseline-skip . 2) \right-align\italic\right-column {
  \line { Le grand Prêtre déchire }
  \line { la bannière de la vie. }
} r2
