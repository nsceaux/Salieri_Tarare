\clef "bass" do8\fp do do do do2:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
sol,1 |
do2:8 do:8 |
sol,1 |
do2:8\f do:8 |
do:8 do:8\p |
re:8-\sug\f re:8 |
sol,1\p~ |
sol,4 si, do do |
re re re re |
sol,2 r |
sol,1~ |
sol,4 si, do do |
re re re re |
sol2:8\f sol:8 |
la8 fad fad fad fad2:8 |
sol:8\p sol:8 |
la8 fad fad fad fad2:8 |
sol:8 sol:8 |
fa!:8 mi:8 |
re:8\f re:8 |
mi:8 mi4 r |
la4(\p sol fa mi) |
fa( mi re do) |
re r r2 |
mi4 r mi, r |
la,1 |
la4( sol fa mi) |
fa( mi re do) |
re4 r r2 |
mi4 r r2 |
mi4 r mi r |
la,4. si,8\f si,4.\trill la,16 si, |
do2:8\fp do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
do:8\f do:8 |
fa:8 fa:8\p |
sol:8 sol:8 |
fa:8\f fa:8 |
fa:8 fa:8\p |
sol:8\f sol:8 |
do1\p~ |
do8 do[ do do] do fa fa fa |
sol4 sol sol sol, |
do do do do |
do1\p |
mi2:8 fa:8 |
sol:8 sol,:8 |
do:8 fa:8\f |
sol4 r r2 |
sol8\f sol, sol, sol, sol,2:8 |
do:8\ff do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
do4 do' sol mi |
do2 r4\fermata r8 lab, |
