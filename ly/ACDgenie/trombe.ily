\clef "treble" <>-\sug\fp <<
  { do''2. mi''4 | do'' sol' mi' sol' | } \\
  { do'2. mi'4 | do' sol' mi' sol' | }
>>
<>^"[a 2]" do''4 do'8. do'16 do'2~ |
do'1~ |
do' |
do' |
R1*3 |
r4 <<
  { mi'4 sol' do'' | mi''2 } \\
  { do'4-\sug\f mi' sol' | sol'2 }
>> r |
r4 <>-\sug\f <<
  { re''8. re''16 re''4 re'' | } \\
  { re''8. re''16 re''4 re'' | }
>>
R1*3 |
r4 <<
  { sol'4 sol' sol' | } \\
  { sol'4 sol' sol' | }
>>
R1*3 |
re''1-\sug\f |
re'' |
re''\p |
re'' |
<<
  { re''4 sol'8. sol'16 sol'4 sol' | } \\
  { re''4 sol'8. sol'16 sol'4 sol' | }
>>
sol'1 |
re''-\sug\f |
<<
  { mi''8 mi'' mi'' mi'' mi''4 } \\
  { r8 mi' mi' mi' mi'4 }
>> r4 |
R1*11 |
<>-\sug\fp <<
  { do''2. mi''4 | do'' sol' mi' sol' | } \\
  { do'2. mi'4 | do' sol' mi' sol' | }
>>
<>^"[a 2]" do''4 do'8. do'16 do'2~ |
do'1~ |
do' |
<<
  { do'8 do'' do'' do'' do'' do'' do'' do'' | } \\
  { do'8 do''-\sug\f do'' do'' do'' do'' do'' do'' | }
>>
<< do''1~ { s2. <>\p } >> |
do''1 |
do''\f |
<<
  { do''2 re'' |
    re''4 re''8. re''16 re''2 | } \\
  { do''2 re''\p |
    sol'4\f sol'8. sol'16 sol'2 | }
>>
R1*3 |
r4 <<
  { do''4 do'' do'' | } \\
  { do'4\p do' do' | }
>>
R1*2 |
r2 r8 <<
  { re''8 re'' re'' | do''2 } \\
  { sol'8 sol' sol' | do''2 } \\
>> r2 |
r8 <<
  { re''16 re'' re''8 re'' re''2:8 |
    re''1 |
    mi'' |
    re'' |
    mi''1 |
    re'' |
    mi''~ |
    mi'' |
    mi''4 do'' do'' do'' |
    do''2 } \\
  { sol'16\f sol' sol'8 sol' sol'2:8 |
    sol'1 |
    do''-\sug\ff |
    sol' |
    do'' |
    sol' |
    sol' |
    sol' |
    sol'4 mi' mi' mi' |
    mi'2 }
>> r |
