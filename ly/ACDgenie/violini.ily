\clef "treble"
<<
  \tag #'violino1 {
    <mi' do''>16\fp q q q q4:16 q2:16 |
    q2:16 q2:16 |
    q2:16 q2:16 |
    <fa' la'>2:16 q2:16 |
    q2:16 q2:16 |
    sol'2. mi''8 sol'' |
    sol''( fa'' mi'' re'') do''( si' la' sol') |
    sol'4.( la'8) sol'4 mi''8 sol'' |
    sol''( fa'' mi'' re'') do''( si' la' sol') |
    sol'4 mi'\f sol' do'' |
    mi''2 \grace re''8 do''4\p si'8 la' |
    fad'8 \grace mi''8 re''16\f dod'' re''8 \grace sol''8 fad''?16 mi'' fad''8 \grace si''8 la''16 sol'' la''8 la'' |
    si'4.(\p do''8 do''4 re'') |
    re''2~ re''8 mi''( do'' la') |
    r8 sol' sol' sol' sol' sol' la' la' |
    la' si' si' si' si'2:8 |
    si'4.( do''8) do''4 re'' |
    re''2~ re''8 mi'' do'' la' |
    sol'2:8 sol'8 sol' fad' fad' |
    sol' sol''[\f sol'' sol''] \grace la''8 sol'' fad''16 mi'' \grace mi''8 re'' do''16 si' |
    do''8 la'' la'' la'' \grace si''8 la'' sol''16 fad'' \grace fad''8 mi'' re''16 do'' |
    si'8 sol''[\p sol'' sol''] \grace la''8 sol'' fad''16 mi'' \grace mi''8 re'' do''16 si' |
    do''8 la''[ la'' la''] \grace si''8 la'' sol''16 fad'' \grace fad''8 mi'' re''16 do'' |
    si'8 sol si re' sol' re' sol' si' |
    re''2:16 mi'':16 |
    fa'':16\f re''4:16 si':16 |
    sold'16
  }
  \tag #'violino2 {
    <sol mi'>16-\sug\fp q q q q4:16 q2:16 |
    q2:16 q2:16 |
    q2:16 q2:16 |
    <la fa'>2:16 q2:16 |
    q2:16 q2:16 |
    mi'16( do' mi' do' mi' do' mi' do' \rt#4 { mi' do') } |
    re'( fa' re' fa' re' fa' re' fa' \rt#4 { re' fa') } |
    mi'16( do' mi' do' mi' do' mi' do' \rt#4 { mi' do') } |
    re' fa' re' fa' re' fa' re' fa' \rt#4 { re' fa' } |
    mi'16 do' mi'-\sug\f do' mi' do' mi' do' \rt#4 { mi' do' } |
    << \rp#4 { do' mi' sol' mi' } { s2 <>-\sug\p } >> |
    << \rp#4 { la16 re' fad' re' } { s8 <>-\sug\f } >> |
    sol'4.(-\sug\p mi'8) mi'4 si |
    si sol'2 mi'8 do' |
    r si si si si si' fad' fad' |
    fad' sol' sol' sol' sol'2:8 |
    sol'4.( mi'8) mi'4 si |
    si sol'2 mi'8 do' |
    si2:8 si8 si la la |
    si16-\sug\f re' si re' si re' si re' \rt#4 { si re' } |
    \rt#4 { do' re' } \rt#4 { do' re' } |
    <>-\sug\p \rt#4 { si re' } \rt#4 { si re' } |
    \rt#4 { do' re' } \rt#4 { do' re' } |
    \rt#4 { si re' } \rt#4 { si re' } |
    si si' si' si' si'4:16 do''2:16 |
    la':16-\sug\f fa'4:16 re':16 |
    si16
  }
>> si' dod'' red'' mi'' fad'' sold'' la'' si''4 r |
<<
  \tag #'violino1 {
    la'8\p la'4 la' la' la'8~ |
    la' la'4 la' la' la'8 |
    r si' si' si' si' si' si' si' |
    r la' la' la' r sold' sold' sold' |
    la'1 |
    la'8 la'4 la' la' la'8~ |
    la' la'4 la' la' la'8 |
    r8 si' si' si' si'2:8 |
    r8 la' la' la' la'2:8 |
    r8 la' la' la' r sold' sold' sold' |
  }
  \tag #'violino2 {
    do'8-\sug\p do'4 do' do' do'8~ |
    do' do'4 do' do' do'8 |
    r fa' fa' fa' fa' fa' fa' fa' |
    r mi' mi' mi' r mi' mi' mi' |
    mi'1 |
    do'8 do'4 do' do' do'8~ |
    do' do'4 do' do' do'8 |
    r fa' fa' fa' fa'2:8 |
    r8 mi' mi' mi' mi'2:8 |
    r8 mi' mi' mi' r mi' mi' mi' |
  }
>>
la'4. si'8\f si'4.\trill la'16 si' |
<<
  \tag #'violino1 {
    <do'' mi'>2:16\fp q:16 |
    q:16 q:16 |
    q:16 q:16 |
    la':16 la':16 |
    la':16 la':16 |
    sol'8 \grace re'' do''16[\f si'] do''8 do'' do''2:8 |
    do''8(\f do''') do'''2 la''8(\p do''') |
    \grace do'''8 sib''4( la''8 sol'' fa'' mi'' re'' do'') |
    do''4\f la'-! do''-! fa''-! |
    la''2 \grace sol''8 fa''4\p mi''8 re'' |
    si'!8\f \grace la''8 sol''16 fad'' sol''8 si''16 la'' si''8 re'''16 do''' re'''8 re''' |
    mi''4.(\p fa''8) fa''4 sol'' |
    sol''2~ sol''8 la''( fa'' re'') |
    r8 do'' do'' do'' do'' do'' re'' re'' |
    re'' mi'' mi'' mi'' mi''2:8 |
    mi''4.(\p fa''8) fa''4 sol'' |
    sol''2:8 la'':8 |
    re''4. mi''16 fa'' sol''2:8 |
    mi'':8 la'':8\f |
    re''16
  }
  \tag #'violino2 {
    <mi' sol>2:16-\sug\fp q:16 |
    q:16 q:16 |
    q:16 q:16 |
    fa':16 fa':16 |
    fa':16 fa':16 |
    <mi' sol>:16-\sug\f q:16 |
    <la fa'>8(-\sug\fp la') la'2 la'4 |
    sib'8 sib'4 sib' sib' sib'8 |
    la'4-\sug\f \ru#5 { do'16 fa' la' fa' } <>-\sug\p \ru#2 { la re' fa' re' } |
    re'8-\sug\f si'16 la' <si' re'>8 q q2:8 |
    do''4.(-\sug\p la'8) la'4 mi' |
    mi'2~ mi'8 fa' la' fa' |
    r mi' mi' mi' mi' mi' si si |
    si( do') do' do' do'2:8 |
    do''4.(-\sug\p la'8) la'4 mi' |
    sol'8 do'' do'' do'' do''2:8 |
    si':8 si':8 |
    do'':8 do'':8-\sug\f |
    si'16
  }
>> sol16 la si do' re' mi' fad' sol'8 la'16 si' do'' re'' mi'' fad'' |
sol''(\f si'' re''' si'' re''' si'' re''' si'' \rt#4 { re''' si'') } |
do'''8\ff 
<<
  \tag #'violino1 {
    do'''8[ do''' do'''] do''' si''16 la'' \grace la''8 sol'' fa''16 mi'' |
    fa''8 re'''[ re''' re'''] re''' do'''16 si'' \grace si''8 la'' sol''16 fa'' |
    mi''8 do''' do''' do''' \acciaccatura re'''8 do'''8 si''16 la'' \acciaccatura la''8 sol'' fa''16 mi'' |
    fa''8 re''' re''' re''' re''' do'''16 si'' \grace si''8 la'' sol''16 fa'' |
    mi''4. \grace re'''8 do'''16 si'' do'''4. \grace la''8 sol''16 fad'' |
    sol''4. mi''8 sol'' mi'' sol'' mi'' |
    do''4
  }
  \tag #'violino2 {
    mi'16[ sol'] mi' sol' mi' sol' \rt#4 { mi' sol' } |
    \rt#4 { fa' sol' } \rt#4 { fa' sol' } |
    \rt#4 { mi' sol' } \rt#4 { mi' sol' } |
    \rt#4 { fa' sol' } \rt#4 { fa' sol' } |
    mi' <mi' do''> q q q4:16 q2:16 |
    q2:16 q2:16 |
    q4
  }
>> <sol mi' do''>4 q q |
q2 r4\fermata r8 <<
  \tag #'violino1 { mib'8 | }
  \tag #'violino2 { do' | }
>>
