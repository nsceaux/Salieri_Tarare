\piecePartSpecs
#`((reduction)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet#})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#})
   (oboi #:score "score-oboi")
   (violino1)
   (violino2)
   (viola)
   (basso)
   (trombe #:instrument , #{\markup\center-column { Trompettes [en ut] } #}
           #:music , #{
\quoteViolinoI "ACDviolin"
s1*6
<>^\markup\tiny "Vln" \cue "ACDviolin" { \mmRestDown s1*3 }
s1*3
<>^\markup\tiny "Vln" \cue "ACDviolin" s1*3
s1
<>^\markup\tiny "Vln" \cue "ACDviolin" s1*3
<>^"[a 2]" s1*8
\mmRestUp
<>_\markup\tiny "Vln" \cue "ACDviolin" {
  \mmRestDown s1*2 \mmRestUp s1 \mmRestDown s1*4
  \mmRestUp s1 \mmRestDown s1*2 \mmRestUp s1
}
s1*11
\mmRestDown
<>^\markup\tiny "Vln" \cue "ACDviolin" s1*3
s1
<>^\markup\tiny "Vln" \cue "ACDviolin" s2*5
          #}))
