\score {
  <<
    \new ChoirStaff <<
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        { s1*38 <>^\markup\character La Nature }
        \global \keepWithTag #'nature \includeNotes "voix"
      >> \keepWithTag #'nature \includeLyrics "paroles"
      \new Staff \with { instrumentName = \markup\character Le Génie }
      \withLyrics <<
        \global \keepWithTag #'genie \includeNotes "voix"
      >> \keepWithTag #'genie \includeLyrics "paroles"
    >>
    \new PianoStaff <<
      \new Staff = "dessus" << \global \includeNotes "reduction-dessus" >>
      \new Staff = "basse" << \global \includeNotes "reduction-basse" >>
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
