\tag #'(genie basse) {
  Gloire à l’é -- ter -- nel -- le sa -- ges -- se,
  qui, cré -- ant l’im -- mor -- tel a -- mour,
  vou -- lut que, par sa seule i -- vres -- se,
  l’ê -- tre sen -- si -- ble,
  l’ê -- tre sen -- sible ob -- tînt le jour,
  l’ê -- tre sen -- sible ob -- tînt le jour.
  Ah ! si ma flamme ar -- dente et pu -- re
  n’eût pas em -- bra -- sé vo -- tre sein,
  sté -- rile a -- mant de la Na -- tu -- re,
  j’eusse é -- té for -- mé sans des -- sein,
  sté -- rile a -- mant de la Na -- tu -- re,
  j’eusse é -- té for -- mé __ sans des -- sein.
}

Gloire à l’é -- ter -- nel -- le sa -- ges -- se,
qui, cré -- ant l’im -- mor -- tel a -- mour,
vou -- lut que, par sa seule i -- vres -- se,
l’ê -- tre sen -- si -- ble,
l’ê -- tre sen -- sible ob -- tînt le jour,
l’ê -- tre sen -- sible ob -- tînt le jour, ob -- tînt le jour.
