\clef "treble"
do''2. mi''4 |
do'' sol' mi' sol' |
do''( <do'' mi''> <sol'' do''>) <mi'' do'''> |
<do'' fa'' la''>1~ |
q |
<do'' mi'' sol'>8 q4 q q8 mi''8[ sol''] |
sol''( fa'' mi'' re'') do''( si' la' sol') |
sol'4.( la'8) sol'4 mi''8 sol'' |
sol''( fa'' mi'' re'') do''( si' la' sol') |
sol'4 <do' mi'>\f <mi' sol'> <sol' do''> |
<sol' mi''>2 \grace re''8 do''4\p si'8 la' |
fad'8 \grace mi''8 re''16\f dod'' re''8 \grace sol''8 fad''?16 mi'' fad''8 \grace si''8 la''16 sol'' la''8 la'' |
<sol' si'>4.(\p <mi' do''>8 q4 <si' re''>) |
<< { re''2~ \oneVoice re''8 mi''( <mi' do''> <do' la'>) } \\ { si'4 sol'2 } >> |
r8 <si re' sol'> q4:8 q:8 <re' fad' la'>:8 |
q8 <re' sol' si'> <sol' si'>8( <si' re''>) <si' re''>( sol'') <si' sol''>( <re'' si''>) |
<sol' si'>4.( <mi' do''>8) q4 <si' re''> |
<< { re''2~ \oneVoice re''8 mi''( <mi' do''> <do' la'>) } \\ { si'4 sol'2 } >> |
<si re' sol'>2:8 q4:8 fad'8 fad' |
sol' sol''[\f sol'' sol''] \grace la''8 sol'' fad''16 mi'' \grace mi''8 re'' do''16 si' |
do''8 la'' la'' la'' \grace si''8 la'' sol''16 fad'' \grace fad''8 mi'' re''16 do'' |
si'8 sol''[\p sol'' sol''] \grace la''8 sol'' fad''16 mi'' \grace mi''8 re'' do''16 si' |
do''8 la''[ la'' la''] \grace si''8 la'' sol''16 fad'' \grace fad''8 mi'' re''16 do'' |
si'8 sol' si' re'' sol'' re'' sol'' si'' |
<sol' si' re''>2:16 <sol' do'' mi''>:16 |
<la' re'' fa''>:16\f <re''' fa'' la''>4:16 <re'' fa'' si''>:16 |
sold''16 si' dod'' red'' mi'' fad'' sold'' la'' si''4 r |
<do' la'>8\p q4 q q q8~ |
q q4 q q q8 |
r <re' fa' si'> q q q2:8 |
r8 <do' mi' la'> q q r <si mi' sold'> q q |
<do' mi' la'>1 |
<do' la'>8 q4 q q q8~ |
q q4 q q q8 |
r8 <re' fa' si'> q q q2:8 |
r8 <do' mi' la'> q q q2:8 |
r8 q q q r <si mi' sold'> q q |
la'4. si'8\f si'4.\trill la'16 si' |
do''2. mi''4 |
do'' sol' mi' sol' |
do''( <do'' mi''> <sol'' do''>) <mi'' do'''> |
<do'' fa'' la''>1~ |
q |
sol''8 \grace re'' do''16[\f si'] do''8 do'' do''2:8 |
<do'' fa'' la''>8(\f <la'' do'''>) q2 la''8(\p do''') |
\grace do'''8 sib''4( la''8 sol'' fa'' mi'' re'' do'') |
do''4\f la'-! do''-! fa''-! |
<do'' la''>2 \grace sol''8 <fa'' re'' la'>4\p <fa' mi''>8 re'' |
si'!8\f \grace la''8 sol''16 fad'' sol''8 si''16 la'' si''8 re'''16 do''' re'''8 re''' |
<do'' mi''>4.(\p <la' fa''>8) q4 <mi'' sol''> |
q2~ q8 <fa'' la''>( fa'' re'') |
r8 <mi' sol' do''> q q q q <sol' si' re''> q |
q <sol' do'' mi''> <do'' sol' mi''>8. <mi'' do'' sol''>16 q2:8 |
<do'' mi''>4.(\p <la' fa''>8) q4 <mi'' sol''> |
<sol' sol''>8 <do'' sol''> q q <do'' la''>2:8 |
re''4. mi''16 fa'' sol''8 <si' re'' sol''> q q |
<do'' mi''>2:8 <do'' la''>:8\f |
re''16 sol16 la si do' re' mi' fad' sol'8 la'16 si' do'' re'' mi'' fad'' |
sol''(\f si'' re''' si'' re''' si'' re''' si'' \rt#4 { re''' si'') } |
do'''8\ff do'''8[ do''' do'''] do''' si''16 la'' \grace la''8 sol'' fa''16 mi'' |
fa''8 re'''[ re''' re'''] re''' do'''16 si'' \grace si''8 la'' sol''16 fa'' |
mi''8 do''' do''' do''' \acciaccatura re'''8 do'''8 si''16 la'' \acciaccatura la''8 sol'' fa''16 mi'' |
fa''8 re''' re''' re''' re''' do'''16 si'' \grace si''8 la'' sol''16 fa'' |
mi''4. \grace re'''8 do'''16 si'' do'''4. \grace la''8 sol''16 fad'' |
sol''4. mi''8 sol'' mi'' sol'' mi'' |
do''4 <sol' mi' do''>4 q q |
q2 r4\fermata r8 <do' mib'>8 |
