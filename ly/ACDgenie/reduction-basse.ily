\clef "bass" <do mi sol>2:16 q:16 |
q:16 q:16 |
q:16 q:16 |
<do fa la>2:16 q:16 |
q:16 q:16 |
\rt#4 { mi16 do } \rt#4 { mi do } |
<< { \rt#4 { re fa } \rt#4 { re fa } } \\ sol,1 >> |
\rt#4 { mi16 do } \rt#4 { mi do } |
<< { \rt#4 { re fa } \rt#4 { re fa } } \\ sol,1 >> |
\rt#4 { mi16 do } \rt#4 { mi do } |
\rp#4 { do mi sol mi } |
\rp#4 { la,16 re fad re } |
sol,1~ |
sol,4 si, do do |
re re re re |
sol,2 r |
sol,1~ |
sol,4 si, do do |
re re re re |
<< { \rt#4 { si16 re' } \rt#4 { si16 re' } } \\ { sol2 sol } >> |
<< { \rt#4 { do'16 re' } \rt#4 { do' re' } } \\ { la2 fad2 } >> |
<< { \rt#4 { si16 re' } \rt#4 { si16 re' } } \\ { sol2 sol } >> |
<< { \rt#4 { do'16 re' } \rt#4 { do' re' } } \\ { la2 fad2 } >> |
<< { \rt#4 { si16 re' } \rt#4 { si16 re' } } \\ { sol2 sol } >> |
fa!2:8 mi:8 |
re:8 re:8 |
<mi sold si>:8 q4 r |
la4( sol fa mi) |
fa( mi re do) |
re r r2 |
mi4 r mi, r |
la,1 |
la4( sol fa mi) |
fa( mi re do) |
re4 r r2 |
mi4 r r2 |
mi4 r mi r |
la,4. si,8 si,4.\trill la,16 si, |
<do mi sol>2:16 q:16 |
q:16 q:16 |
q:16 q:16 |
<do fa la>2:16 q:16 |
q:16 q:16 |
<do mi sol>:16 q:16 |
<fa la do'>8 q4 q q q8 |
<sol sib do' mi'> q4 q q q8 |
<fa la>4 \ru#5 { do16 fa la fa } \ru#2 { la, re fa re } |
<sol, si, re>2:8 q:8 |
do1~ |
do8 do[ do do] do fa fa fa |
sol4 sol sol sol, |
do do do do |
do1 |
mi2:8 fa2:8 |
<sol si re'>2:8 sol,:8 |
do:8 fa:8 |
sol8 <sol, si, re>16 q q8 q q2:8
sol8 sol, sol, sol, sol,2:8 |
\rt#4 { do16 mi } \rt#4 { do16 mi } |
\rt#4 { re si, } \rt#4 { re si, } |
\rt#4 { do16 mi } \rt#4 { do16 mi } |
\rt#4 { re si, } \rt#4 { re si, } |
<do mi sol>2:8 q:8 |
q:8 q:8 |
q4 do' sol mi |
do2 r4\fermata r8 lab, |
