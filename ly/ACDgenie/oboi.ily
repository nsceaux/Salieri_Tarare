\clef "treble" \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''2. mi''4 |
    do'' sol' mi' sol' |
    do''( mi'' sol'') do''' |
    la''1~ |
    la'' |
    sol''2 r4 <>^"Solo" mi''8 sol'' |
    sol''( fa'' mi'' re'' do'' si' la' sol') |
    sol'4.( la'8) sol'4 mi''8( sol'') |
    sol''( fa'' mi'' re'' do'' si' la' sol') |
    sol'4 }
  { do''2. mi''4 |
    do'' sol' mi' sol' |
    do''( mi'' sol'') mi'' |
    fa''1~ |
    fa'' |
    mi''2 r2 |
    R1*3 |
    r4 }
>> <>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi'4 sol' do'' | mi''2 do''4 la' | }
  { do'4 mi' sol' | sol'2 mi' | }
  { s2. | s2 <>-\sug\p }
>>
r4 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 fad''? la'' }
  { fad' la' fad' }
>>
R1*3 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { si'8( re'') re''( sol'') sol''( si'') | }
  { sol'8( si') si'4 si'8( re'') | }
>>
R1*3 <>-\sug\f |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { r8 sol'' sol'' sol'' \grace la'' sol'' fad''16 mi'' \grace mi''8 re'' do''16 si' |
    do''8 la'' la'' la'' \grace si''8 la'' sol''16 fad'' \grace fad'' mi''8 re''16 do'' |
    si'1 | }
  { si'1 | do''2. re'4 | sol'1 | }
  { s1*2 | <>-\sug\p }
>>
fad'8 la' la' la' \twoVoices #'(oboe1 oboe2 oboi) << do''2 la' >> |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { si'1 |
    re''2 mi'' |
    fa'' re'''4 si'' |
    sold''2 sold''4 }
  { sol'1 |
    si'2 do'' |
    la' fa'4 re'' |
    si'2 si'4 }
  { s1*2 | <>-\sug\f }
>> r4 |
R1*11 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''2. mi''4 |
    do'' sol' mi' sol' |
    do'' mi'' sol'' do''' |
    la''1 |
    la'' |
  }
  { do''2. mi''4 |
    do'' sol' mi' sol' |
    do'' mi'' sol'' mi'' |
    fa''1 |
    fa'' | }
>>
r8 \grace re''8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''16 si' do''8 do'' do''2:8 |
    do''1 |
    mi'' |
    fa'' |
    la''2 fa''4 mi''8 re'' |
    si'4 re''2 re''4 | }
  { do''16 si' do''8 do'' do''2:8 |
    la'1 |
    sib'1 |
    la' |
    do''2 la'4 fa' |
    re' si'2 si'4 | }
  { s2.. | s2. s4\p | s1 | s1\f | s2 s\p | s1\f }
>>
R1*3 |
r4 <>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''8. sol''16 sol''4 sol'' | }
  { sol'8. mi'16 sol'4 sol' | }
>>
R1*2 |
r2 r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''8 sol'' sol'' | mi''2 }
  { si'8 si' si' | do''2 }
>> r2 |
r8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''16 re'' re''8 re'' re''2:8 |
    sol''1 |
    mi''8 do'''[ do''' do'''] do''' si''16 la'' \grace la''8 sol'' fa''16 mi'' |
    fa''8 re'''[ re''' re'''] re''' do'''16 si'' \grace si''8 la'' sol''16 fa'' |
    mi''8 do''' do''' do''' \acciaccatura re'''8 do''' si''16 la'' \acciaccatura la''8 sol'' fa''16 mi'' |
    fa''8 re''' re''' re''' re''' do'''16 si'' \grace si''8 la'' sol''16 fa'' |
    mi''4. \grace re'''8 do'''16 si'' do'''4. \grace la''8 sol''16 fad'' |
    sol''4. mi''8 sol'' mi'' sol'' mi'' |
    do''4 do'' do'' do'' |
    do''2 }
  { si'16 si' si'8 si' si'2:8 |
    si'1 |
    do''4 mi''2 sol'4 |
    re'' si'2 si'4 |
    do''4 mi''2 sol'4 |
    re'' si'2 si'4 |
    do'' mi''2 mi''4 |
    mi''4. mi''8 sol'' mi'' sol'' mi'' |
    do''4 mi' mi' mi' |
    mi'2 }
  { s2.. | s1 | <>-\sug\ff }
>> r2 |
