\clef "alto" do'16-\sug\fp do' do' do' do'4:16 do'2:16 |
do'2:16 do'2:16 |
do'2:16 do'2:16 |
do'2:16 do'2:16 |
do'2:16 do'2:16 |
do'8 <sol mi'>4 q q q8 |
<sol fa'>1 |
<sol mi'>8 q4 q q q8 |
<sol fa'>1 |
<sol mi'>8 q4-\sug\f q q q8 |
do do' do' do' do'2:8-\sug\p |
re':8-\sug\f re':8 |
sol'4.(-\sug\p mi'8) mi'4 si |
si sol'2( mi'8 do') |
r re' re' re' re'2:8 |
re':8 re':8 |
sol'4.( mi'8) mi'4 si |
si sol'2 mi'8 do' |
si re' re' re' re'2:8 |
si16-\sug\f re' si re' si re' si re' \rt#4 { si re' } |
\rt#4 { do' re' } \rt#4 { do' re' } |
si8 sol-\sug\p sol sol sol2:8 |
la8 fad fad fad fad2:8 |
\rt#4 { sol16 si } \rt#4 { sol si } |
sol sol' sol' sol' sol'4:16 sol'2:16 |
fa':16-\sug\f la'4:16 fa':16 |
mi'8 <si sold'>[ q q] q4 r |
la'4(\p sol' fa' mi') |
fa'( mi' re' do') |
r8 re' re' re' re' re' re' re' |
r do' do' do' r si si si |
do'1 |
la4( sol fa mi) |
fa( mi re do) |
r8 re' re' re' re'2:8 |
r8 do' do' do' do'2:8 |
r8 do' do' do' r si si si |
la4. si8-\sug\f si4.\trill la16 si |
do'2:8-\sug\fp do':8 |
do':8 do':8 |
do':8 do':8 |
do':8 do':8 |
do':8 do':8 |
do':8-\sug\f do':8 |
do'8 do'4 do' do'-\sug\p do'8~ |
do' do'4 do' do' do'8 |
do'4-\sug\f \ru#5 { do'16 fa' la' fa' } <>-\sug\p \ru#2 { la re' fa' re' } |
re'8-\sug\f sol sol' sol' sol'2:8 |
do'4.(\p la8) la4 mi |
mi2~ mi8 fa fa fa |
r sol' sol' sol' sol'2:8 |
sol':8 sol':8 |
do'4.(-\sug\p la8) la4 mi |
sol'8 mi' mi' mi' fa'2:8 |
sol':8 sol:8 |
do':8 fa':8-\sug\f |
sol'4 r r2 |
sol'8 sol sol sol sol2:8 |
sol16-\sug\ff mi' do' mi' do' mi' do' mi' \rt#4 { do' mi' } |
\rt#4 { re' si } \rt#4 { re' si } |
\rt#4 { do' mi' } \rt#4 { do' mi' } |
\rt#4 { re' si } \rt#4 { re' si } |
<sol mi'>2:16 q:16 |
q:16 q:16 |
q4 do' sol mi |
do2 r4\fermata r8 lab |
