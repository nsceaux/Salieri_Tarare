\score {
   <<
    \new StaffGroup <<
      \new GrandStaff \with { \oboiInstr } <<
        \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
      >>
      \new Staff \with { \trombeInstr } << \global \includeNotes "trombe" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with { \natureInstr } \withLyrics <<
      \global \keepWithTag #'nature \includeNotes "voix"
    >> \keepWithTag #'nature \includeLyrics "paroles"
    \new Staff \with { \genieInstr } \withLyrics <<
      \global \keepWithTag #'genie \includeNotes "voix"
    >> \keepWithTag #'genie \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \keepWithTag #'basso \includeNotes "basso"
      \origLayout {
        s1*5\break s1*6\pageBreak
        s1*6\break s1*6\pageBreak
        s1*6\break s1*7\pageBreak
        s1*7\break s1*6\pageBreak
        s1*7\break s1*5\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
