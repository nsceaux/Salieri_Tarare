<<
  \tag #'nature { \clef "soprano/treble" R1*38 | }
  \tag #'(genie basse) {
    \clef "bass" \tag basse <>^\markup\character-text Le Génie chanté
    \tag #'genie <>^\markup\italic chanté do'2. mi'4 |
    do' sol mi sol8 sol |
    do'4 do r do'8 do' |
    la2 do'4 do' |
    fa'2. la4 |
    sol2 r |
    r r4 r8 sol |
    do'2. do'4 |
    re'4. sol8 sol4. sol8 |
    mi'4 do' r2 |
    mi'2 \grace re'8 do'4 si8[ la] |
    fad4 fad r2 |
    si4.( do'8) do'4 re' |
    re'2~ re'8[ mi'] do'[ la] |
    sol2. la4 |
    \grace la8 si2 r |
    si4.( do'8) do'4 re' |
    re'2~ re'8[ mi'] do'[ la] |
    sol2. fad4 |
    sol r r2 |
    R1 |
    re'2. si8 re' |
    do'4 do' do'4. do'8 |
    si4 si r r8 sol |
    re'2 mi'4. mi'8 |
    fa'2 re'4. si8 |
    sold2 r8 mi fad sold |
    la4. la8 la4. do'8 |
    la4 la r2 |
    si4. re'8 do'4 si |
    mi'2 mi4. mi8 |
    la2~ la8 do' mi' do' |
    la4. la8 la4 do' |
    la4 la r2 |
    si4. re'8 do'4 si |
    mi'2( mi'4. fa'8) |
    la2. sold4 |
    la2 r |
  }
>>
<<
  \tag #'(nature basse) {
    \tag #'basse { \ffclef "treble" <>^\markup\character La Nature }
    do''2. mi''4 |
    do'' sol' mi' sol'8 sol' |
    do''4 do' r do''8 do'' |
    la'2 do''4. do''8 |
    fa''2. la'4 |
    sol'2 r4 r8 do'' |
    do''2. do''4 |
    mi''4. mi''8 mi''4. mi''8 |
    fa''4 do'' r2 |
    la''2 fa''4 re'' |
    si'! si' r2 |
    mi''4.( fa''8) fa''4 sol'' |
    sol''2~ sol''8[ la''] fa''[ re''] |
    do''2. re''4 |
    \grace re''8 mi''2 r |
    mi''4.( fa''8) fa''4 sol'' |
    sol''2 la'' |
    re''2. sol''4 |
    mi''2 la'' |
    re''2. re''4 |
    sol''2. sol''4 |
    do''2 r |
  }
  \tag #'genie {
    do'2. mi'4 |
    do' sol mi sol8 sol |
    do'4 do r4 mi8 mi |
    fa2 la4. la8 |
    la2. fa4 |
    mi2 r4 r8 sol |
    la2. la4 |
    sib4. sib8 sib4. sib8 |
    la4 la r2 |
    do'2 la4 fa' |
    re' re' r2 |
    do'4.( la8) la4 mi |
    mi2~ mi8[ fa] la[ fa] |
    mi2. si4 |
    \grace si8 do'2 r |
    do'4.( la8) la4 mi |
    sol( do'2) do'4 |
    si2. si4 |
    do'2 do' |
    si2. si4 |
    si2. si4 |
    do'2 r |
  }
>>
R1*7 |
