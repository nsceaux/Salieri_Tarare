\piecePartSpecs
#`((violino1 #:score-template "score-part-voix" #:indent 0)
   (violino2 #:score-template "score-part-voix" #:indent 0)
   (viola #:score-template "score-part-voix" #:indent 0)
   (basso #:score-template "score-part-voix" #:indent 0)
   (oboi #:score "score-oboi")
   (clarinetti #:score "score-oboi")
   (fagotti #:score-template "score-part-voix"
            #:tag-notes fagotti #:indent 0)
   (corni #:score-template "score-part-voix"
          #:tag-notes corni #:tag-global ()
          #:instrument , #{\markup\center-column { Trompettes \line { Cors \small en Ut } } #})
   (trombe #:score-template "score-part-voix" #:notes "corni"
           #:tag-notes corni #:tag-global ()
           #:instrument , #{\markup\center-column { Trompettes \line { Cors \small en Ut } } #})
   (silence #:indent 0)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
