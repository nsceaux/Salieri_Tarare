\clef "alto" sol'8 sol[ sol sol] fad2:8 |
sol8 <re' si'>[-\sug\ff q q] q2:8 |
q:8 q:8 |
q:8 q:8 |
la':8 sol':8 |
sol'8 do'[ do' do'] do' do' do' mi' |
sold'2:8 sold':8 |
la':8 la':8 |
fa':8 dod':8 |
re':8 re':8 |
sol'4 sol'8 sol' sol'4 sol' |
do'4 r r2 |
r16 do'-\sug\ff re' mi' fa' sol' la' si' do''4 r |
r2 r16 fa sol la sib do' re' mi' |
fa'2 r |
R1 |
R1 |
sib2 sib4 sib |
sib r r2 |
R1 |
<mib sib sol'>4 q q r |
r2 lab4 r |
r2 lab'4 r |
r8. <re' si'>16 q8. q16 q4 r |
sol'1\fp~ |
sol'~ |
sol'~ |
sol'2 r4 do'~ |
do'8 re'16 mib' fa' sol' la' si' do''4 r |
R1 | \allowPageTurn
r2 do' |
r2 r4 re' |
mib'1\fp~ |
mib' |
r4 lab'-\sug\mf( fa' fa') |
fa'1 |
r2 r4 sol'\f |
do' r r2 |
fad'4 r r2 |
sol'4 r r2 | \allowPageTurn
r fad'4 r |
si sold?-\sug\p la-\sug\cresc lad |
si si dod' la8.-\sug\f la'16 |
re'4 r r2 |
r <sol fa'!>-\sug\fp~ |
q r4 sol'-\sug\f |
sold' r r2 |
r r8. la'16-\sug\p la'8. la'16 |
fa'4 r r8. fa'16 fa'4~ |
fa'2 r4 mi'-\sug\f |
