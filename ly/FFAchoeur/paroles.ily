\tag #'(vdessus1 vdessus2 vhaute-contre1 vhaute-contre2 basse) {
  - reur re -- de -- man -- de
}
\tag #'(vdessus1 vdessus2 vhaute-contre1 vhaute-contre2) { Ta -- ra -- re. }
\tag #'(vhaute-contre vtaille vbasse basse) {
  Ta -- ra -- re, Ta -- ra -- re, Ta -- ra -- re ;
  ren -- dez- nous no -- tre gé -- né -- ral.
  Son tré -- pas, dit- on, se pré -- pa -- re.
  S’il a re -- çu le coup fa -- tal,
  ven -- geons sa mort sur ce bar -- ba -- re.
}
\tag #'(recit basse) {
  \tag #'recit { Ar -- rê -- tez, }
  sol -- dats, ar -- rê -- tez.
  Quel ordre i -- ci vous a por -- tés ?
  Ô l’a -- bo -- mi -- na -- ble vic -- toi -- re !
  On sau -- ve -- rait mes jours, en flé -- tris -- sant ma gloi -- re !
  Un tas de re -- bel -- les mu -- tins
  de l’é -- tat fe -- rait les des -- tins !
  Est-ce à vous de ju -- ger vos maî -- tres ?
  N’ont- ils sou -- doy -- é que des traî -- tres ?
  Ou -- bli -- ez- vous, sol -- dats, u -- sur -- pant le pou -- voir,
  que le res -- pect des rois est le pre -- mier de -- voir ?
  Ar -- mes bas, fu -- ri -- eux ! votre em -- pe -- reur vous cas -- se.
  Sei -- gneur, ils sont sou -- mis ; je de -- man -- de leur grâ -- ce.
  
  Quoi ! tou -- jours ce fan -- tô -- me en -- tre mon peuple et moi !
  Dé -- fen -- seurs du sé -- rail, suis-je en -- cor vo -- tre roi ?
}
\tag #'recit Oui.
\tag #'(calpigi basse) { Non. }
\tag #'(vhaute-contre vtaille vbasse basse) { Non. }
\tag #'(vhaute-contre1 vtaille1 vbasse1 basse) { Non. }
\tag #'(calpigi basse) { C’est lui. }
\tag #'(recit basse) { Ja -- mais. }
\tag #'(vhaute-contre vtaille vbasse basse) { C’est toi, c’est toi. }
\tag #'(vhaute-contre1 vtaille1 vbasse1) { C’est toi. }
\tag #'(recit basse) {
  Mons -- tre !… Ils te sont ven -- dus… Rè -- gne donc à ma pla -- ce.
  
  Ah ! mal -- heu -- reux !
  
  La mort est moins dure à mes yeux…
  que de ré -- gner par toi… sur ce peuple o -- di -- eux.
}
