\clef "treble"
<<
  \tag #'violino1 {
    si'8 re'?[ re' re'] mib'' mib'[ mib' mib'] |
    re'
  }
  \tag #'violino2 {
    si'8 si[ si si] do'' do'[ do' do'] |
    si
  }
>> <si' sol''>\ff[ q q] q2:8 |
<re'' si''>2:8 q:8 |
<si' sol''>:8 q:8 |
<<
  \tag #'violino1 {
    fa''2:8 fa'':8 |
    mi''8 do'''[ do''' do'''] do''' do''' do''' mi''' |
    sold''2:8 sold'':8 |
    la'':8 la'':8 |
    re'''8 la'' la'' la'' la''2:8 |
    la'':8 la'':8 |
  }
  \tag #'violino2 {
    do''2:8 re'':8 |
    do''8 do''[ do'' do''] do'' do'' do'' mi'' |
    sold'2:8 sold':8 |
    la':8 la':8 |
    la'8 re'' fa'' re'' la' mi'' la'' mi'' |
    la' fa'' fa'' fa'' fa''2:8 |
  }
>>
sol4 <re'' si''>8 q q4 q |
<mi'' do'''>4 r r2 |
r16 do'\ff re' mi' fa' sol' la' si' do''4 r |
r2 r16 fa' sol' la' sib'? do'' re'' mi'' |
fa''2 r |
R1*2 | \allowPageTurn
<<
  \tag #'violino1 {
    <fa' re''>4. sib''8 sib''8. fa''16 fa''8. re''16 |
    re''4
  }
  \tag #'violino2 {
    <fa' re''>4. q8 re''8. re''16 re''8. sib'16 |
    fa'4
  }
>> r4 r2 |
R1 |
r16 mib' sol' sib' mib'' sib' sol' sib' mib''4 r |
r2 <lab mib' do''>4 r |
r2 <<
  \tag #'violino1 {
    fa''4 r |
    r8. <re'' re'>16 q8. q16 q4 r |
    <si' sol''>1\fp~ |
    q~ |
    q~ |
    q2
  }
  \tag #'violino2 { 
    do''4 r |
    r8. <re' si'!>16 q8. q16 q4 r |
    q1-\sug\fp~ |
    q~ |
    q~ |
    q2
  }
>> r4 do'~ |
do'8 re'16 mib' fa' sol' la' si' do''4 r |
R1 | \allowPageTurn
r2 <<
  \tag #'violino1 { fad''2 | }
  \tag #'violino2 { <la'! re'> | }
>>
r2 r4 <re' la' fad''>4 |
<<
  \tag #'violino1 {
    sib1\fp~ |
    sib2~ sib |
    r4 fa''(\mf re'' do'') |
    si'1 |
  }
  \tag #'violino2 {
    sol1-\sug\fp~ |
    sol2~ sol |
    r4 \once\slurDashed do''-\sug\mf( lab' lab') |
    re'1 |
  }
>>
r2 r4 <sol re' si'>4\f |
<sol mi'! do''>4 r r2 |
<<
  \tag #'violino1 {
    re''4 r r2 |
    re''4 r r2 |
    r red''4 r |
    mi'' mi''\p dod''\cresc fad'' |
    re'' sol'' mi'' la''8.\f
  }
  \tag #'violino2 {
    la'4 r r2 |
    si'4 r r2 |
    r la'4 r |
    sol' si'-\sug\p mi'-\sug\cresc dod'' |
    fad' re'' la' dod''8.-\sug\f
  }
>> <dod''' mi''>16 |
<fad'' re'''>4 r r2 |
r2 <<
  \tag #'violino1 {
    <re' si'>2\fp~ |
    q r4 <re' re''>4\f |
    <re'' si''> r r2 |
    r r8. do''16\p do''8. do''16 |
    do''4 r r8. <re' si>16 q4~ |
    q2
  }
  \tag #'violino2 {
    <re' fa'!>2-\sug\fp~ |
    q r4 <re' si'>-\sug\f |
    <re'' fa''> r r2 |
    r r8. mi'16-\sug\p mi'8. mi'16 |
    la'4 r r8. sold16 sold4~ |
    sold2
  }
>> r4 <mi' si' sold''>4\f |
