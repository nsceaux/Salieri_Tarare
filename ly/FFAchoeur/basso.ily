\clef "bass" sol8 sol,[ sol, sol,] fad,2:8 |
sol,8 sol\ff sol sol sol2:8 |
sol:8 sol:8 |
sol:8 sol:8 |
la:8 si:8 |
do':8 do'8 do' do' mi' |
sold2:8 sold:8 |
la:8 la:8 |
fa:8 dod:8 |
re:8 re:8 |
sol4 sol8 sol sol4 sol |
do4 r r2 |
r16 do-\sug\ff re mi fa sol la si do'4 r |
r2 r16 fa, sol, la, sib, do re mi |
fa2 r |
R1 |
R1 |
sib,2 sib,4 sib, |
sib, r r2 |
R1 |
mib4 mib mib r |
r2 lab,4 r |
r2 lab4 r |
r8. sol16 sol8. sol16 sol4 r |
sol1\fp~ |
sol~ |
sol~ |
sol2 r4 do~ |
do8 re16 mib fa sol la si do'4 r |
R1 | \allowPageTurn
r2 do |
r2 r4 re |
mib1\fp~ |
mib |
r4 lab-\sug\mf( fa fa) |
fa1 |
r2 r4 sol\f |
do r r2 |
fad4 r r2 |
sol4 r r2 |
r fad4 r |
mi4 sold-\sug\p la\cresc lad |
si si dod'\f la8. la,16 |
re4 r r2 |
r sol2\fp~ |
sol r4 sol\f |
sold r r2 |
r r8. la16\p la8. la16 |
fa4 r r8. fa16 fa4~ |
fa2 r4 mi\f |
