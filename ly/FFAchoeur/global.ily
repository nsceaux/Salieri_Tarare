\tag #'all \key lab \major \midiTempo#120
\time 4/4 \once\override Staff.TimeSignature.stencil = ##f s1*4 \bar "||"
\tag #'all \key do \major s1*30
\tempo "Andante" s1*7 s4
\tempo "un poco Allegro" s2. s1*5 s2
\tempo "Presto" s2 s1*2
