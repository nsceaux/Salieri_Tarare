\score {
  <<
    \new Staff \with {
      \remove "Page_turn_engraver" \tinyStaff \haraKiriFirst
    } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }

    \new GrandStaff \with {
      \haraKiriFirst
      \consists "Metronome_mark_engraver"
    } <<
      \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
      \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
    indent = 0
  }
}