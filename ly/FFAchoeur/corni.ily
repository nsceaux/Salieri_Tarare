\clef "treble" R1 |
\twoVoices#'(corno1 corno2 corni) <<
  { sol'1 |
    re''~ |
    re'' |
    do''2 re'' |
    mi''4 do''2 }
  { sol1 |
    sol'~ |
    sol' |
    do''2 sol' |
    do''4 do'2 }
  { s1-\sug\ff }
>> r4 |
R1*2 |
\tag #'corni <>^"unis" re''4 re''8 re'' mi''4 mi'' |
re''1 |
\twoVoices#'(corno1 corno2 corni) <<
  { re''1 | mi''4 }
  { sol'1 | do''4 }
>> r4 r2 |
R1*38 |
