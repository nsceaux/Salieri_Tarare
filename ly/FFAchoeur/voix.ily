<<
  %% Chœur d'esclaves
  \tag #'(vdessus1 basse) {
    \clef "soprano/treble" re''4 re''8 re'' mib''4 mib''8
    \tag #'vdessus1 {
      mib'' |
      re''2 re''4 r4 |
      R1*40 |
    }
  }
  \tag #'vdessus2 {
    \clef "soprano/treble" si'4 si'8 si' do''4 do''8 do'' |
    si'2 si'4 r |
    R1*40 |
  }
  \tag #'(vhaute-contre1 basse) {
    <<
      \tag #'basse { s1*42 }
      \tag #'vhaute-contre1 {
        \clef "alto/G_8" re'4 re'8 re' mib'4 mib'8 mib' |
        re'2 re'4 r |
        R1*40 | <>^\markup\character [Tout le Peuple]
      }
    >>
    re'4 \tag #'vhaute-contre1 {
      r4 r r8 la' |
      fad'4 r r2 |
      R1
    }
  }
  \tag #'vhaute-contre2 {
    \clef "alto/G_8" si4 si8 si do'4 do'8 do' |
    si2 si4 r |
    R1*40 |
  }
  \tag #'vtaille1 {
    \clef "tenor/G_8" R1*42 |
    re'4 r r r8 mi' |
    la4 r r2 |
    R1
  }
  \tag #'vbasse1 {
    \clef "bass" R1*42 |
    re'4 r r r8 dod' |
    re'4 r r2 |
    R1 |
    
  }
  %% Chœur de soldats
  \tag #'(vhaute-contre basse) {
    <<
      \tag #'basse { s2.. \ffclef "alto/G_8" }
      \tag #'vhaute-contre { \clef "alto/G_8" r2 r4 r8 }
    >> do'8 |
    sol'4 sol'8 sol' sol'4 sol'8 sol' |
    si'1 |
    sol'2 r4 sol'8 sol' |
    fa'2 fa'8 fa' fa' fa' |
    mi'2 r4 do'8 mi' |
    sold4 sold sold sold8 sold |
    la4 la r8 la la la |
    re'4. re'8 mi'4. mi'8 |
    fa'2 r8 fa' fa' re' |
    si2 sol'4 sol'8 sol' |
    mi'4 do'4 <<
      \tag #'basse { s2 | s1*29 | s2. \ffclef "alto/G_8" <>^\markup\character Ch. }
      \tag #'vhaute-contre {
        r2 | R1*29 r2 r4 <>^\markup\character [Tous les Soldats]
      }
    >> fad'4
    <<
      \tag #'basse { s2 s8 \ffclef "alto/G_8" <>^\markup\character Ch. }
      \tag #'vhaute-contre { r2 r8 }
    >> mi' la'8. la'16 |
    fad'4 r \tag #'vhaute-contre { r2 R1 }
  }
  \tag #'vtaille {
    \clef "tenor/G_8" r2 r4 r8 do' |
    re'4 re'8 re' re'4 re'8 re' |
    sol'1 |
    re'2 r4 si8 si |
    do'2 re'8 re' re' re' |
    do'2 r4 do'8 mi' |
    sold4 sold sold sold8 sold |
    la4 la r8 la la la |
    re'4. re'8 mi'4. mi'8 |
    fa'2 r8 fa' fa' re' |
    si2 re'4 re'8 re' |
    do'4 sol4 r2 |
    R1*29 |
    r2 r4 fad' |
    r2 r8 dod' mi'8. la16 |
    re'4 r r2 |
    R1 |
    
  }
  \tag #'vbasse {
    \clef "bass" r2 r4 r8 do' |
    sol4 si8 si si4 si8 si |
    re'1 |
    si?2 r4 sol8 sol |
    la2 si8 si si si |
    do'2 r4 do'8 mi' |
    sold4 sold sold sold8 sold |
    la4 la r8 la la la |
    la4. la8 la4. la8 |
    la2 r8 la la fa |
    sol2 si4 si8 si |
    do'4 do4 r2 |
    R1*29 |
    r2 r4 fad' |
    r2 r8 la dod'8. dod'16 |
    re'4 r r2 |
    R1 |
    
  }
  \tag #'(recit basse) {
    <<
      \tag #'basse {
        s1*11 s4.
        \ffclef "tenor/G_8" <>^\markup\character Tarare
      }
      \tag #'recit {
        \clef "tenor/G_8" R1*10 |
        r2 <>^\markup\character-text Tarare violemment ^\markup { \italic Parlé Récit. }
        r4 sol8 sol |
        do'4.
      }
    >> do'8 mi'4 mi'8 sol' |
    do'4 r r8 do' do' re' |
    mi' mi' mi' do' fa'4 r |
    fa'2~ fa'8 do' do' do' |
    do'4 la8 la fa fa r do' |
    do' do' do' re' mib'8. mib'16 mib' mib' fa' do' |
    re'8 re' r4 r2 |
    r4 r8 sib sib sib16 sib sib8 sib16 do' |
    re'4 r8 fa'16 fa' re'8. re'16 re'8 do'16 sib |
    mib'4 r r r8 sib16 sib |
    mib'8 mib'16 mib' mib'8 sib do' do' r do' |
    do'4 do'8 do' fa'4 fa'8 do' |
    re' re' r4 r8 re' re' re' |
    sol'4. re'8 si!4 si8 si |
    si4 do'8 re' sol4 r8 si |
    si si si do' re'4 r |
    fa'8 fa'16 fa' sol'8 re' mib'4 r |
    r2 r4 sol'8. sol'16 |
    sol'4 mib'8 mib' do'4 r |
    mib'4 mib'8 do' fad'4. sol'8 |
    sol'2 re'8 r r4 |
    r2 r4 r8 sib |
    |
    mib'2 r8^\markup\italic { il se jette à genoux } mib' reb' mib' |
    do'4 r r2 |
    r2 r4 re'8 re' |
    si4 si8 do' sol sol r4 |
    \ffclef "bass" <>^\markup\character-text Atar avec fureur
    do'4. do'8 do'4 re'8 mi' |
    re' re' r4 do'8 do'16 do' re'8 la |
    si4 r8 re'16 re' re'4 si8 si |
    sol4 r8 si16 si red'4 red'8 si |
    mi'4 r4 <<
      \tag #'basse { s2 s4. }
      \tag #'recit {
        \ffclef "bass" <>^\markup\character Un Eunuque mi'4 r4 |
        r4 r8
      }
    >>
    \ffclef "tenor/G_8" <>^\markup\character Tarare
    sol'8 mi'4 <<
      \tag #'basse { s2. }
      \tag #'recit { r4 | r2 }
    >>
    \ffclef "bass" <>^\markup\character-text Atar à Tarare
    re'8 re' r4 |
    re'8 re' do' re' si4 r8 re'16 fa' |
    si4 si8 do' sol sol-\tag #'recit ^\markup\right-align\italic\line { il se poignarde et tombe }
    \ffclef "tenor/G_8" <>^\markup\character-text Tarare avec douleur
    re'8 re'16 re' |
    fa'4
    \ffclef "bass" <>^\markup\character-text Atar se relève
    r8 sold si4 si8 si |
    si4 do'8 re' do'4 r |
    r8 la la16 la la la re'4 r8 re'16 fa' |
    sold4 sold8 la mi4 r^\markup\right-align\line\italic { il tombe mort. } |
  }
  \tag #'(calpigi basse) {
    <<
      \tag #'basse { s1*41 s2 \ffclef "alto/G_8" <>^\markup\character Cal. }
      \tag #'calpigi {
        \clef "alto/G_8" R1*41 | r2 <>^\markup\character Calpigi
      }
    >> dod'4 <<
      \tag #'basse { s4 | s8 \ffclef "alto/G_8" <>^\markup\character Cal. }
      \tag #'calpigi { r4 | r8 }
    >> re'8 sol'4
    \tag #'calpigi { r2 R1*7 }
  }
>>
  