\score {
  <<
    \new StaffGroup \with { \haraKiri }<<
      \new Staff \with { \oboiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column {
          Trompettes et \line { Cors \small [en Ut] }
        }
        shortInstrumentName = \markup\center-column { Tr. Cor. }
      } <<
        \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      instrumentName = \markup\center-column\smallCaps { Chœur d'Esclaves }
      shortInstrumentName = \markup\character Ch.
      \haraKiriFirst
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus1 \includeNotes "voix"
      >> \keepWithTag #'vdessus1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus2 \includeNotes "voix"
      >> \keepWithTag #'vdessus2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre1 \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre2 \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille1 \includeNotes "voix"
      >> \keepWithTag #'vtaille1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse1 \includeNotes "voix"
      >> \keepWithTag #'vbasse1 \includeLyrics "paroles"
    >>
    \new ChoirStaff \with {
      instrumentName = \markup\center-column\smallCaps { Chœur des Soldats }
      shortInstrumentName = \markup\character Ch.
      \haraKiri
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
        { s1*42\noBreak s1\noBreak }
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'calpigi \includeNotes "voix"
    >> \keepWithTag #'calpigi \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*6\pageBreak
        s1*6\break s1*4\break s1*3 s2 \bar "" \pageBreak
        s2 s1*3\break s1*4\break s1*5\break s1*5 s2 \bar "" \pageBreak
        s2 s1*3\break s1*4\pageBreak
        s1*3\break
      }
      \modVersion {
        s1*16\pageBreak
        s1*12\pageBreak
        s1*4\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
