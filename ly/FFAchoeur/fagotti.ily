\clef "bass" sol8 sol,[ sol, sol,] fad,2:8 |
sol,8 sol\ff sol sol sol2:8 |
sol:8 sol:8 |
sol:8 sol:8 |
la:8 si:8 |
do':8 do'8 do' do' mi' |
sold2:8 sold:8 |
la:8 la:8 |
fa:8 dod:8 |
re:8 re:8 |
sol4 sol8 sol sol4 sol |
do4 r r2 | \allowPageTurn
R1*38 |
