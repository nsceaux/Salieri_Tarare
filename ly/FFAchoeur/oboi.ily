\clef "treble"
\tag #'oboi <>^\markup\override#'(baseline-skip . 2)
\center-align\center-column { Hautbois et clarinettes }
\twoVoices#'(oboe1 oboe2 oboi) <<
  { si'4 }
  { si' }
>> r4 r2 |
\tag#'(oboi oboe1) <>^\markup Hautbois seuls
\twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''1 | si'' | sol'' | fa'' |
    mi''4 do'''2 do'''4 | sold''1 | la''~ | la''~ |
    la'' | si'' | do'''4 }
  { si'1 | re'' | si' | do''2 re'' |
    sol'4 do''2 do''4 | sold'1 | la' | re''2 mi'' |
    fa''1 | re'' | mi''4 }
  { s1-\sug\ff }
>> r4 r2 |
R1*17 \allowPageTurn
R1*21 |
