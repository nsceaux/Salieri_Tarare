\clef "alto" fad'4(\p mi'8) re' |
sol'2 mi' |
re'~ re'8 re'[ re' re'] |
dod'2 la |
si\mf~ si8 si'\p si' si' |
la'4 sol' fad' mi' |
re'4. re'8 fad'4 fad' |
sol'8 sol la si la4 la |
re'2 fad4 mi8 re |
sol1 |
la2~ la8 re'[ re' re'] |
dod'2 mi' |
re'-\sug\mf~ re'8 si-\sug\p si si |
la4( sol fad mi) |
re4. re8 fad4 fad |
sol la8 si la4. sol8 |
fad2 r |
