\clef "treble"
<re' la'>4\p <dod' sol'>8 <re' fad'> |
<re' si'>4. q8 <sol' dod''>4. q8 |
re''8 la' fad' la' re'' re''( dod'' si') |
<la' mi'> mi'( la' mi' dod' mi' dod' la) |
sol'8\mf sol( si re') sol' sol'(\p si' re'') |
<< { dod''4( si' la' <dod' sol'>) } \\ { mi'2 } >> |
<re' fad'>4. fad'8 la'4 re''8 dod'' |
<re' si'>4 dod''8 re'' <re' fad'>4. <dod' mi'>8 |
re'2 <re' la'>4 <dod' sol'>8 <re' fad'> |
<re' si'>4. q8 <sol' dod''>4. q8 |
re''( la' fad' la') re'' re''([ dod'' si']) |
<la' mi'> mi'( la' mi' dod' mi' dod' la) | |
sol'8\mf sol([ si re']) sol' sol'([\p si' re'']) |
<< { dod''4( si' la' <dod' sol'>) } \\ mi'2 >> |
fad'4. fad'8 la'( re' re'' dod'') |
<< { si'4 dod''8 re'' fad'4. mi'8 } \\ { re'2~ re'4. dod'8 } >> |
re'2 r |
