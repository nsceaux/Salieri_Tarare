\clef "treble"
<<
  \tag #'violino1 {
    la'4\p sol'8 fad' |
    si'4. si'8 dod''4. dod''8 |
    re''8 la' fad' la' re'' re''( dod'' si') |
    la'1 |
    sol'8\mf sol( si re') sol' sol'(\p si' re'') |
    dod''4( si' la' sol') |
    fad'4. fad'8 la'4 re''8 dod'' |
    si'4 dod''8 re'' fad'4. mi'8 |
    re'2 la'4 sol'8 fad' |
    si'4. si'8 dod''4. dod''8 |
    re''( la' fad' la') re'' re''([ dod'' si']) |
    la'1 |
    sol'8\mf sol([ si re']) sol' sol'([\p si' re'']) |
    dod''4( si' la' sol') |
    fad'4. fad'8 la'( re' re'' dod'') |
    si'4 dod''8 re'' fad'4. mi'8 |
    re'2 r |
  }
  \tag #'violino2 {
    re'4\p dod'8 re' |
    re'4. re'8 sol'4. sol'8 |
    fad'2~ fad'8 fad'[ fad' fad'] |
    mi' mi'( la' mi' dod' mi' dod' la) |
    re'2\mf~ re'8 re'\p re' re' |
    mi'2 la4( dod') |
    re'2. re'4 |
    re'4. re'8 re'4. dod'8 |
    re'2 re'4 dod'8 re' |
    re'2 sol' |
    fad'~ fad'8 fad'[ fad' fad'] |
    mi' mi'( la' mi' dod' mi' dod' la) |
    re'1\mf |
    mi'2-\sug\p la4( dod') |
    re'2. re'4 |
    re'2~ re'4. dod'8 |
    re'2 r |
  }
>>