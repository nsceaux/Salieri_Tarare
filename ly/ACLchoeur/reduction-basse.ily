\clef "bass"
fad4( mi8) re |
sol2 mi |
<re fad>~ q8 q[ q q] |
dod2 la, |
<si, re>~ q8 <si re'> q q |
la4 sol fad mi |
re4. re8 fad4 fad |
sol8 sol, la, si, la,4 la, |
re2 fad4 mi8 re |
sol2 mi |
<re fad la>~ q8 <re fad>[ q q] |
dod2 <mi la,> |
<si, re>~ q8 si si si |
la4( sol fad mi) |
re4. re8 fad4 fad |
\mergeDifferentlyDottedOn
sol( la8 si) << { la4. sol8 } \\ { la4 la, } >> |
<re fad>2 r |
