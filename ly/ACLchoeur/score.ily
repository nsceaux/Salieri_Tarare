\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with {
        instrumentName = \markup\center-column { Flûtes Hautbois }
        shortInstrumentName = \markup\center-column { Fl Htb }
      } <<
        \new Staff << \global \keepWithTag #'flauto1 \includeNotes "flauti" >>
        \new Staff << \global \keepWithTag #'flauto2 \includeNotes "flauti" >>
      >>
      \new GrandStaff \with { \clarinettiInstr } <<
        \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "basso"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      instrumentName = \markup\center-column\smallCaps { Chœur des Ombres }
      shortInstrumentName = \markup\character Ch.
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \keepWithTag #'basso \includeNotes "basso"
      \origLayout {
        s2 s1*4\pageBreak
        s1*6\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
