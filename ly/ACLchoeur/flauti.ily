\clef "treble" r2 |
R1*7 |
r2 <<
  \tag #'flauto1 {
    <>_"Flûtes seules" la''4 sol''8 fad'' |
    si''2 dod''' |
    re'''8( la'' fad'' la'') re''' re'''( dod''' si'') |
    la''1 |
    sol''8-\sug\mf sol'([ si' re'']) sol'' sol''([-\sug\p si'' re''']) |
    dod'''4( si'' la'' sol'') |
    fad''4. fad''8 la''( re'' re''' dod''') |
    si''4 dod'''8 re''' fad''4. mi''8 |
    re''2 r |
  }
  \tag #'flauto2 {
    re''4 dod''8 re'' |
    re''2 sol'' |
    fad''~ fad''8 fad''[ fad'' fad''] |
    mi'' mi''( la'' mi'' dod'' mi'' dod'' la') |
    re''1-\sug\mf |
    mi''2-\sug\p la'4( dod'') |
    re''2. re''4 |
    re''2~ re''4. dod''8 |
    re''2 r |
  }
>>