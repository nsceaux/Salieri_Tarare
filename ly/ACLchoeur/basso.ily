\clef "bass"
<<
  \tag #'basso {
    fad4(_\markup { \dynamic p \italic sempre } mi8) re |
    sol2 mi |
    re~ re8 re[ re re] |
    dod2 la, |
    si,\mf~ si,8 si\p si si |
    la4 sol fad mi |
    re4. re8 fad4 fad |
    sol8 sol, la, si, la,4 la, |
    re2
  }
  \tag #'fagotti { r2 | R1*7 | r2 }
>> fad4 mi8 re |
sol2 mi |
re~ re8 re[ re re] |
dod2 la, |
si,\mf~ si,8 si\p si si |
la4( sol fad mi) |
re4. re8 fad4 fad |
sol( la8 si) la4 la, |
re2 r |
