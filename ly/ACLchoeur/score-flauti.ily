\score {
  \new GrandStaff \with {
    instrumentName = \markup\center-column { Flûte et Hautbois }
  } <<
    \new Staff << \global \keepWithTag #'flauto1 \includeNotes "flauti" >>
    \new Staff << \global \keepWithTag #'flauto2 \includeNotes "flauti" >>
  >>
  \layout { indent = \largeindent }
}
