<<
  \tag #'(vdessus basse) {
    \clef "soprano/treble"
    <>^\markup\italic chanté ^\markup\character-text L'Ombre d'Astasie ton suppliant
    la'4 sol'8 fad' |
    si'4. si'8 dod''4. dod''8 |
    re''2 r8 re'' dod'' si' |
    la'4. la'8 la'4. si'8 |
    sol'4 sol' r8 sol' si' re'' |
    dod''4 si' la' sol' |
    fad'4. fad'8 la'4 re''8 dod'' |
    si'4 dod''8 re'' fad'4.( mi'8) |
    re'2 <>^"tutti" la'4 sol'8 fad' |
    si'4. si'8 dod''4. dod''8 |
    re''2 r8 re'' dod'' si' |
    la'4. la'8 la'4. si'8 |
    sol'4 sol' r8 sol' si' re'' |
    dod''4 si' la' sol' |
    fad'4. fad'8 la'4 re''8 dod'' |
    si'4 dod''8 re'' fad'4.\melisma mi'8\melismaEnd |
    re'2 r |
  }
  \tag #'vhaute-contre {
    \clef "alto/G_8" <>^\markup\character L'Ombre de Calpigi
    re'4 dod'8 re' |
    re'4. re'8 sol4. sol8 |
    la2 r8 fad' fad' fad' |
    mi'4. mi'8 dod'4. dod'8 |
    re'4 re' r8 re' re' re' |
    mi'4 mi' la dod' |
    re'4. re'8 re'4 re'8 re' |
    re'4 re'8 re' re'4.( dod'8) |
    re'2 <>^"tutti" re'4 dod'8 re' |
    re'4. re'8 sol4. sol8 |
    la2 r8 re' re' re' |
    mi'4. mi'8 dod'4 dod' |
    re' re' r8 re' re' re' |
    mi'4 mi' la dod' |
    re'4. re'8 re'4 re'8 re' |
    re'4 re'8 re' re'4.\melisma dod'8\melismaEnd |
    re'2 r |
  }
  \tag #'vtaille {
    \clef "tenor/G_8" <>^\markup\character L'Ombre de Tarare
    la4 la8 la |
    si4. si8 sol4. sol8 |
    fad2 r8 re' re' re' |
    mi'4. mi'8 mi'4. mi'8 |
    re'4 re' r8 si si si |
    la4 sol fad mi |
    re4. re8 fad4 fad8 fad |
    sol4 la8 si la2 |
    re2 <>^"tutti" la4 la8 la |
    si4. sol8 sol4. sol8 |
    fad2 r8 re' re' re' |
    mi'4. mi'8 mi'4. mi'8 |
    re'4 re' r8 si si si |
    la4 sol fad mi |
    re4. re8 fad4 fad8 fad |
    sol4 la8 si la2 |
    fad r |
  }
  \tag #'vbasse {
    \clef "bass/bass" <>^\markup\character L'Ombre d'Arthénée
    fad4 mi8 re |
    sol4. sol8 mi4. mi8 |
    re2 r8 re' re' re' |
    dod'4. dod'8 la4. la8 |
    si4 si r8 si si si |
    la4 sol fad mi |
    re4. re8 fad4 fad8 fad |
    sol4 la8 si la2 |
    re2 <>^"tutti" fad4 mi8 re |
    sol4. sol8 mi4. mi8 |
    re2 r8 re' re' re' |
    dod'4. dod'8 la4 la |
    si4 si r8 si si si |
    la4 sol fad mi |
    re4. re8 fad4 re8 re |
    sol4 la8 si la2 |
    re r |
  }
>>
