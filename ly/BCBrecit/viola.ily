\clef "alto" do'8.\p reb'16 |
reb'4( sib sol) mib' |
do' do'2.\sf |
sib4 sib(\p sib sib) |
lab2.~ lab8.-\sug\f mi!16 |
fa4. fa8 fa4. fa8 |
re1-\sug\p~ |
re |
lab1~ |
lab |
reb'1~ |
reb'2~ reb'4 r | \allowPageTurn
r2 r4 r8 do |
reb4\< mi! sol sib |
mi1-\sug\fp~ |
mi2 fa4 r |
r2 r4 <lab lab'>8-!-\sug\ff q-! |
q4 r r2 |
R1 | \allowPageTurn
r4 la!2. |
r8. fa'16-\sug\f fa'8. fa'16 fa'2~ |
fa' fa'4 r |
r2 r4 do''-\sug\fp~ |
do''1~ |
do''2 sib'4 r |
sol'-\sug\mf fa' mi'!2 |
R1 |
r2 fa4\fermata r |
r8. re16-\sug\f fa8.-\sug\p la16 re'4 r |
r8. sol16-\sug\f sib8. re'16 sol'4 r |
r8. dod16-\sug\fp mi8. la16 dod'4 r |
re'2~ re'4 re'8-\sug\p do' |
sib8 sib re' fa' sib'2 |
mi'8 mi4-\sug\f mi mi mi8 |
mi1 |
fa2:16-\sug\fp fa:16 |
la:16 la:16 |
la:16 la:16 |
la:16 la:16 |
la:16 sol:16 |
sib:16 sib:16 |
la8-\sug\mf-\sug\cresc la'4 la' la' la'8 |
la'8-\sug\cresc la'4 la' la' la'8 |
<re' la'>2:16-\sug\ff q:16 |
q4 r r2 |
R1 |
r2 sol'4 r |
r8. sol16 sol4 r2 |
r8 r16 fa fa4 r\fermata r |
r4 sib? mib'8 fa'16 sol' lab' sib' do'' re'' |
mib''4 sib'8 sol' <mib' sol>16 q q q q4:16 |
q2:16 q:16 |
q:16 q:16 |
<lab mib'>:16 <sol mi'>:16 |
q:16 q:16 |
<lab fa'>:16 q:16 |
q:16 q:16 |
q:16 lab'4 r |
r2 r4 mib' |
lab\p lab'\cresc lab' lab' |
lab' lab' lab' lab'\! |
lab'1~ |
lab' |
sol'2 r8. sol'16 sol'8. sol'16 |
do'2
