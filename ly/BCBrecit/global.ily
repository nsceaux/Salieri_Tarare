\key do \major \tempo "Andante" \midiTempo#100
\time 4/4 \partial 4 s4 s1*11 s2.
\tempo "Allegro" s4 s1*3 s2.
\tempo "Presto" s4 s1*3
\tempo "Allegro Maestoso" s1*5
\tempo "Andante" s1*2 s2
\tempo "Andante Maestoso" s2 s1*13
\tempo "Allegro" s1*6
\tempo "Presto" s1*12
\tempo "Allegro" s1*4 s2 s8.
\tempo "Allegro" s16 s4 s2 \bar "|."
