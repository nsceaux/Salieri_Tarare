O sort af -- freux, dont l’hor -- reur me pour -- suit !
du sein d’u -- ne pro -- fon -- de nuit,
quel -- le clar -- té triste et nou -- vel -- le…
Où suis- je ? Tout mon corps chan -- cel -- le.

Dans le pa -- lais d’A -- tar.

Cal -- pi -- gi, qu’elle est bel -- le !

Dans le pa -- lais d’A -- tar ! Ah ! quelle in -- di -- gni -- té !

D’A -- tar qui vous a -- do -- re.

Et c’est la ré -- com -- pen -- se,
ô mon é -- poux, de ta fi -- dé -- li -- té !

Mes bien -- faits la -- ve -- ront cet -- te lé -- gère of -- fen -- se.

Quoi, cru -- el ! par cet at -- ten -- tat,
vous pay -- ez la foi d’un sol -- dat
qui vous a con -- ser -- vé la vi -- e !
Vous lui ra -- vis -- sez As -- ta -- si -- e !

Grand Dieu ! ton pou -- voir in -- fi -- ni,
lais -- se -- ra- t-il donc im -- pu -- ni
ce crime a -- tro -- ce d’un par -- ju -- re,
et la plus ex -- é -- crable in -- ju -- re !
Ô Bra -- ma ! Dieu ven -- geur !…

Quel ef -- fray -- ant trans -- port !

Le voi -- le de la mort a cou -- vert sa pau -- piè -- re.

Quoi ! mal -- heu -- reux ! tu m’an -- non -- ces sa mort !
Meurs, toi- mê -- me.

Et vous tous, ren -- dez à la lu -- miè -- re
l’ob -- jet de mon fu -- neste a -- mour.
À sa dou -- leur trem -- blez qu’il ne suc -- com -- be ;
ré -- pon -- dez- moi de son re -- tour,
ou je lui fais de tous une hor -- rible hé -- ca -- tom -- be.

Dieux ! quel spec -- tacle a gla -- cé mes es -- prits !
