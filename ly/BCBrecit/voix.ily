\clef "bass" r4 |
R1*5 |
\ffclef "soprano/treble" <>^\markup\character-text Astasie parlé
r2 re''4 re''8 re'' |
si'4 si'8 do'' re''4 fa'8 sol' |
mib'2 r4 mib' |
do''4. do''8 do'' do'' do'' do'' |
reb''2 sib'4 sib'8 sib' |
sol'4 sol'8 sol' lab' sib' sib' r |
r4 r8 sol' do''4 do''8 r |
R1*2 |
sol'8 lab' sib' do'' lab' lab'
\ffclef "soprano/treble" <>^\markup\character Spinette
r8 lab' |
lab' lab' sib' do'' fa'4 r |
\ffclef "bass/bass" <>^\markup\character Atar
r lab8 lab reb'4 lab8 lab |
fa fa
\ffclef "soprano/treble" <>^\markup\character Astasie
r8 lab' lab' lab' lab' lab' |
reb''4 fa''2 do''16 do'' do'' do'' |
la'!4 r
\ffclef "bass/bass" <>^\markup\character Atar
r4 r8 fa |
la8. la16 la8 sib sib fa
\ffclef "soprano/treble" <>^\markup\character Astasie
r8 sib' |
sib' sib' sib' do'' re'' re'' mib''4~ |
mib'' re''8 mib'' do''4. do''8 |
do''8 do'' re'' la' sib'4 r |
\ffclef "bass/bass" <>^\markup\character Atar
r2 r4 sol8 sol |
do'4 do'8 do' sol4 r |
sib8 sib16 sib sib8 la fa fa
\ffclef "soprano/treble" <>^\markup\character-text Astasie douloureusement
fa''8. mi''16 | \noBreak
re''4. re''8 re''4 re''8 la' |
sib'2 r4 sib'8 sib' |
la'4. la'8 mi''4 mi''8 sol'' |
fa''2. fa''8 mi'' |
re''4 re''8 re'' re''4. mi''8 |
dod''4 dod'' r2 |
dod''8. dod''16 dod''8. re''16 mi''4 mi''8 la' |
re''4 re'' r^"Récitatif" re''4 |
fa''4. do''16 do'' do''4 do''8 do'' |
la'4 r do''8 do'' do'' re'' |
mib''4 mib''8 mib'' re''4 r16 re'' re'' la' |
do''8 do'' do'' re'' sib' sib' r re''16 re'' |
sol''4 sol''8 sol'' sol''4. re''8 |
mi''! mi'' r4 r dod''8. re''16 |
mi''2 sol''4. sol''8 |
fad''1 |
\ffclef "alto/G_8" <>^\markup\character Calpigi
r4 r8 re' fad' fad' mi' fad' |
re'4
\ffclef "bass/bass" <>^\markup\character Un esclave
r8 la re' re' re' re' |
la8 la16 sib do'8 do'16 re' sib8 sib
\ffclef "bass/bass" <>^\markup\character-text Atar tire son poignard
sol8 sol16 la |
sib4 r8 sib16 sib sib4 sib8 do' |
re'4 r^\markup\italic il le poignarde r re'8. mib'16 |
sib4 sib8 r r2 |
r <>^\markup\italic aux esclaves r4 r8 sib16 sib |
mib'4. sib8 sib sib lab sib |
sol sol r sol sib8. sib16 sib do' reb' mib' |
do'4 r r8 sol sol sol |
do'4. sol8 sib sib sib do' |
lab lab r4 r8 do' do' do' |
lab lab sib do' fa4 r |
lab8 lab16 lab lab8 lab reb'4. reb'16 fa' |
sol4 sol8 lab mib4 mib8 r |
\ffclef "soprano/treble" <>^\markup\character-text Astasie
revenant à elle, aperçoit l’esclave renversé qu’on enlève.
R1*2 |
fa''2. fa''8 lab'' |
lab''4 do''8 do'' do''4 do''8 re'' |
re''2 r |
r2
