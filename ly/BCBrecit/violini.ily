\clef "treble" do'8.(\p reb'16) |
reb'4( sib sol) mib' |
do' <lab lab'>2\sf~ lab'8 sol'16 fa' |
mi'!4 <<
  \tag #'violino1 {
     mi'(\p mi' mi') |
     fa'2. fa'8.\f sol'16 |
     lab'4. fa'8 re'4. do'8 |
     <si sol'>1\p~ |
     q |
  }
  \tag #'violino2 {
    reb'4(-\sug\p reb' reb') |
    do'2. do'4-\sug\f~ |
    do'4. lab8 lab4. lab8 |
    sol1-\sug\p~ |
    sol |
  }
>>
<do' mib'>1~ |
q |
<sib sol'>1~ |
q2~ q4 r |
r2 r4 r8 do' |
r reb'\< r mi'! r sol' r sib' |
mi'1\fp~ |
mi'2 fa'4 r |
r2 r4 <<
  \tag #'violino1 { lab'8\ff-! do''-! | reb''4 }
  \tag #'violino2 { do'8-!-\sug\ff solb'-! | fa'4 }
>> r4 r2 |
R1 |
<<
  \tag #'violino1 {
    r4 fa''2. |
    r8. fa''16-\sug\f fa''8. la'!16 la'2~ |
    la' sib'4 r |
    r2 r4 mib''\fp~ |
    mib''1~ |
    mib''2 re''4 r |
  }
  \tag #'violino2 {
    r4 fa'2. |
    r8. do''16-\sug\f do''8. do'16 do'2~ |
    do' sib4 r |
    r2 r4 la''-\sug\fp~ |
    la''1~ |
    la''2 sib''4 r |
  }
>>
sol'8.\mf la'16 fa'8. sol'16 mi'!2 |
R1 |
r2 fa'4\fermata <<
  \tag #'violino1 {
    fa''8.\f mi''16 |
    re''4 r r8. re''16\p re''8. la'16 |
    sib'4 r r8. re'16 sol'8. sib'16 |
    la'2 r8. la'16 mi''8. sol''16 |
    fa''8 re''([\fz fa'' la''] re''' la'' fa''\p mi'') |
    re''2~ re''8 re''( sol'' mi'') |
    dod''8 dod'4\f dod' dod' dod'8 |
    dod'1 |
    re'2:16\fp re':16 |
    fa':16 fa':16 |
    fa':16 fa':16 |
    fa':16 re':16 |
    re':16 re':16 |
    re'':16 re'':16 |
    dod''8\mf\cresc dod''4 dod'' dod'' re''8 |
    mi''\cresc mi''4 sol'' sol'' sol''8 |
    fad''16\ff do''' do''' do''' do'''4:16 do'''2:16 |
    do'''4
  }
  \tag #'violino2 {
    la'8.\f sol'16 |
    fa'4 r r8. << { la'16 la'8. la'16 } \\ { fa'16-\sug\p fa'8. fa'16 } >> |
    <re' sib'>4 r r8. re'16 sol'8. sib'16 |
    mi'!2 r8. mi'16 la'8. la'16 |
    la'8 re'-\sug\fz([ fa' la'] re'' la' fa'-\sug\p mi') |
    re'2~ re'8 re'( sol' mi') |
    dod'8 la4-\sug\f la la la8 |
    la1 |
    la2:16-\sug\fp la:16 |
    do':16 do':16 |
    do':16 do':16 |
    do':16 la':16 |
    la':16 sib':16 |
    sol':16 sol':16 |
    la'8-\sug\mf-\sug\cresc mi'!4 mi' mi' re'8 |
    dod'-\sug\cresc dod''4 dod'' << { mi''!4 mi''8 } \\ { dod''4 dod''8 } >> |
    re''16-\sug\ff <la' fad''>16[ q q] q4:16 q2:16 |
    q4
  }
>> r4 r2 |
R1 |
r2 <<
  \tag #'violino1 { sib''4 }
  \tag #'violino2 { <sib' sol''>4 }
>> r4 |
r8. <re' sib>16 q4 r2 |
r8 r16 <lab re'>16 q4 r\fermata r |
r4 <re' sib' fa''>4 mib'8 fa'16 sol' lab' sib' do'' re'' |
mib''8 sol''16 mib'' sib' mib'' sol' sib' <<
  \tag #'violino1 {
    <mib' sol>4 r |
    R1*2 |
    r16 lab'( sib' do'' sib' lab' sol' fa') mi'!2 |
    mi'1 |
    r16 fa' sol' lab' sib' do'' re''! mi''! fa''4 r |
    R1 |
    r2
  }
  \tag #'violino2 {
    mib'16 <sol' sib'> q q q4:16 |
    q2:16 q:16 |
    q:16 q:16 |
    <lab' do''>:16 <sol' do''>:16 |
    <sol' do''>:16 q:16 |
    <lab' do''>:16 q:16 |
    q:16 q:16 |
    q:16
  }
>> <lab fa' reb''>4 r |
r2 r4 <mib' sib' sol''> |
<mib' do'' lab''>8 <<
  \tag #'violino1 {
    mib''4\p mib'' mib'' mib''8\cresc~ |
    mib'' mib''4 mib'' mib'' mib''8\! |
    fa''1~ |
    fa'' |
    re''2
  }
  \tag #'violino2 {
    do''4-\sug\p do'' do'' do''8-\sug\cresc~ |
    do'' do''4 do'' do'' do''8\! |
    do''1~ |
    do'' |
    si'!2
  }
>> r8. <si' sol''>16 q8. q16 |
<do'' mi''!>2
