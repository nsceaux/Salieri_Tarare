\clef "bass" do8.\p reb16 |
reb4( sib, sol,) mib |
do lab,2.\f |
sol,4 sol\p sol sol |
lab2.~ lab8.\f mi16 |
fa4. fa8 fa4. fa8 |
sol1\p~ |
sol |
lab~ |
lab |
mi!~ |
mi2~ mi4 r |
r2 r4 do |
reb\< mi! sol sib |
mi1-\sug\fp~ |
mi2 fa4 r |
r2 r4 fa8\ff mib |
reb4 r r2 |
R1 |
r4 la,!2. |
r8. la,16\f la,8. mib16 mib2~ |
mib2 re!4 r |
r2 r4 fad\fp~ |
fad1~ |
fad2 sol4 r |
sol\mf fa mi!2 |
R1 |
r2 fa4\fermata r4 |
r8. re16\f fa8. la16 re'4\p r |
r8. sol,16\f sib,8. re16 sol4 r |
r8. dod16\fp mi!8. la16 dod'4 r |
re'2~ re'4. do'!8\p |
sib sib,( re fa) sib4 sol |
la la,\f la, la, |
sol,!1 |
fa,16\fp fa fa fa fa4:16 fa2:16 |
la,2:16 la,:16 |
la,2:16 la,:16 |
la,:16 fad:16 |
fad:16 sol:16 |
sib,:16 sib,:16 |
la,4\mf\cresc la la la |
la la,\cresc la, la, |
re8\ff re'4 re' re' re'8 |
re4 r r2 |
R1 |
r2 sol4 r |
r8. sol16 sol4 r2 |
r8 r16 fa! fa4 r\fermata r |
r4 sib mib8 fa16 sol lab sib do' re' |
mib'4 sib8 sol mib4 r |
R1*2 |
r16 lab( sib do' sib lab sol fa) mi!2 |
mi1 |
r16 fa sol lab sib do' re'! mi'! fa'4 r |
R1 |
r2 reb4 r |
r2 r4 mib |
lab,\p lab\cresc lab lab |
lab lab lab lab |
lab1\!~ |
lab |
sol2 r8. sol16 sol8. sol16 |
do2
