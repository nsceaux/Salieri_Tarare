\clef "bass" re4\ff fad si2 |
la4 mi dod la, |
re fad si2 |
la4 mi dod la, |
re fad si2 |
dod4 mi la2 |
si,4 re sol2 |
la,4 la,8 la, la,2:8 |
la,2\fermata r |
si1-\sug\fp~ |
si |
mi~ |
mi-\sug\p |
lad, |
si,4 \clef "tenor" sol'-\sug\ff( fad' mid') |
fad'2.\fermata r4 |
R1 |
\clef "bass" sol,4 si,-\sug\f re si, |
sol, sol2 sol4 |
sol, \clef "tenor" sol'16( fad' mi' re') dod'!4 si |
fad'2.\fermata r4 |
R1 |
\clef "bass" sol1\p |
mi\cresc |
fad |
si4 si,2 si,4 |
si,2.-\sug\ff dod4 |
re2-\sug\p r |
R1*12 |
r2\fermata r |
si1-\sug\fp~ |
si |
mi |
mi-\sug\p |
lad, |
si,4 \clef "tenor" sol'-\sug\ff( fad' mid') |
fad'2.\fermata r4 |
R1 |
\clef "bass" sol,4 si,-\sug\f re si, |
sol, sol2 sol4~ |
sol \clef "tenor" sol'16( fad' mi' re') dod'!4 si |
fad'2.\fermata r4 |
R1 |
\clef "bass" sol1\p\cresc |
mi |
fad-\sug\ff |
si,4 re sol2 |
fad4 dod lad, fad, |
si, re sol2 |
fad4 dod lad, fad, |
si, re sol2 |
dod4 mi la2 |
re4 fad8. la16 re'2~ |
re'4 re8 re re2:8 |
re2 r2 |
R1*4 |
