\clef "alto" re4-\sug\ff fad si2 |
la4 mi dod la |
re fad si2 |
la4 mi dod la |
re fad si2 |
dod4 mi la2 |
r4 re sol sol'16 fad' mi' re' |
dod'4 la16 sold la sold \rt#4 { la sold } |
la2\fermata fad'-\sug\p |
si'2:16-\sug\fp si':16 |
si'2:16 si':16 |
si'16 si si si si4:16-\sug\f si2:16 |
si16-\sug\p <sol mi'> q q q4:16 q2:16 |
<fad' fad>16 dod' dod' dod' dod'4:16 dod'2:16 |
si4 sol'-\sug\ff fad' mid' |
fad'2.\fermata fad4-\sug\p |
sol sol16 sol sol sol fad4 fad16 fad fad fad |
sol4 si-\sug\f re' si |
sol sol~ sol8 la16 si do' re' mi' fad' |
<< re'4 \\ si >> r mi' r |
dod'2.\fermata fad4-\sug\p |
sol sol16 sol sol sol fad4 fad16 fad fad fad |
sol-\sug\p sol' sol' sol' sol'4:16-\sug\cresc re'2:16 |
sol':16 sol':16 |
fad':16 fad':16 |
si'4\! si8 si si2:8 |
si4 si8.-\sug\ff si16 si4 dod' |
\rp#2 { re'8*2/3-\sug\p fad' la' re'' la' fad' } |
\rp#2 { re'8*2/3 fad' la' re'' la' fad' } |
re' sol' si' re'' si' sol' dod''![ re'' mi''] dod''[ re'' mi''] |
re''[ la' fad'] re'[ fad' la'] re''[ la' fad'] re'[-\sug\f fad' la'] |
sol'[ si' la'] sol'[ fad' sol'] mi'[ fad' sol'] mi'[ fad' sol'] |
fad'[ la' fad' re' fad' la'] re''[ la' fad' re' fad' la'] |
fad'4 red'2:16-\sug\p red'4:16 |
red'8 r red'4(\f fad' la') |
do''2 la'4(\p sol' |
fad' la' fad' red') |
mi'8 r si4(\f mi' sol') |
si'(\p sol' mi' fad') |
sol'( mi' dod' mi') |
la4.\fermata lad8-\sug\p lad4. lad8 |
si16-\sug\fp si' si' si' si'4:16 si'2:16 |
si':16 si':16 |
si'16 si si si si4:16-\sug\f si2:16 |
si16 <sol mi'> q q q4:16-\sug\p q2:16 |
fad16 dod'[ dod' dod'] dod'4:16 dod'2:16 |
si4 sol-\sug\ff( fad mid) |
fad2.\fermata re4-\sug\p |
sol sol16 sol sol sol fad4 fad16 fad fad fad |
sol4 si-\sug\f re' si |
sol sol~ sol8 la16 si do' re' mi' fad' |
sol'4 sol'16 fad' mi' re' dod'!4 mi'16 re' dod' si |
dod'2.\fermata fad4-\sug\p |
sol sol16 sol sol sol fad4 fad16 fad fad fad |
sol2:8-\sug\p sol:8-\sug\cresc |
mi:8 mi:8 |
fad:8-\sug\ff fad:8 |
si2~ si8 si16 dod' \grace mi'8 re' dod'16 si |
lad4 dod' fad'4. mi'16*2/3 re' dod' |
re'4 si4. si16 dod' \grace mi'8 re' dod'16 si |
lad4 dod' fad'4. mi'16*2/3 re' dod' |
re'4 si4. si16 re' sol'8 sol' |
sol'4 dod'4. dod'16 mi' la'8 la' |
la'4 re'4. fad'16 la' re''8 re'' |
<fad' la>4 q8 q q2:8 |
q2 r |
R1 |
r4 sol' sol r |
R1 |
r2 r4 <sol re' si'>4 |
