\clef "treble"
<< { re'2~ re'8[ re'16 mi'] } \\ re'2\ff >> \grace sol'8 fad' mi'16 re' |
dod'4 mi' la'~ la'16 sol' fad' mi' |
fad'4 re'4. re'16 mi' \grace sol'8 fad' mi'16 re' |
dod'4 mi' la'~ la'16 sol' fad' mi' |
fad'4 re'4. re'16 fad' si'8 si' |
si'4 dod'4. dod'16 mi' la'8 la' |
la'4 si4. si16 re' sol'8 sol' |
sol'4 la16 sold la sold \rt#4 { la sold } |
<<
  \tag #'violino1 {
    la4.\fermata fad''8\p fad''4. fad''8 |
    si''2.\fp si''4 |
    re'''4. si''8 si''4. fad''8 |
    sol''4 sol'16\f( fad' sol' fad' \rt#4 { sol' fad') } |
    sol'4\p sol''2 sol''4 |
    fad''4 lad''8. si''16 dod'''4 mi''8. fad''16 |
    re''16
  }
  \tag #'violino2 {
    la2\fermata dod''-\sug\p |
    <re' fad'>2:16-\sug\fp q:16 |
    q:16 q:16 |
    mi'4 mi'16-\sug\f( red' mi' red' \rt#4 { mi' red') } |
    mi'16-\sug\p si' si' si' si'4:16 si'2:16 |
    dod''16 <fad' lad> q q q4:16 q2:16 |
    <si fad'>16
  }
>> <re'' si''>\ff q q q4:16 q2:16 |
<lad'' dod''>2.\fermata <<
  \tag #'violino1 {
    re'4\p |
    dod'4 dod'16 dod' dod' dod' dod'4 dod'16 dod' dod' dod' |
    si4 si'':16\f re''':16 si'':16 |
    sol''4 <sol' sol>4~ sol'8 la'16 si' do'' re'' mi'' fad'' |
    sol''4
  }
  \tag #'violino2 {
    si4-\sug\p |
    si si16 si si si lad4 lad16 lad lad lad |
    si4 si':16-\sug\f re'':16 si':16 |
    sol'8 <re' si> q q q2:8 |
    q4
  }
>> sol''16( fad'' mi'' re'') dod''!4 mi''16( re'' dod'' si') |
lad'2.\fermata <<
  \tag #'violino1 {
    re'4\p |
    dod'4 dod'16 dod' dod' dod' dod'4 dod'16 dod' dod' dod' |
    si\p re''' re''' re''' re'''4:16\cresc re'''2:16 |
    dod''':16 dod''':16 |
    dod''':16 dod''':16 |
    si''4\!
  }
  \tag #'violino2 {
    si4\p |
    si si16 si si si lad4 lad16 lad lad lad |
    si16-\sug\p <re'' si''> q q q4:16-\sug\cresc q2:16 |
    <dod'' si''>2:16 q:16 |
    <dod'' lad''>:16 q:16 |
    <si' si''>4\!
  }
>> si16 lad si lad \rt#4 { si lad } |
si4 si'8.\ff si'16 si'4 dod'' |
<<
  \tag #'violino1 {
    \grace re''8 re'''2\p la''4. fad''8 |
    re''2. re''4 |
    sol''2 sol''8 sol'' \grace la'' sol'' fad''16 sol'' |
    fad''4 re'16 dod' re' dod' re'4 re''\f |
    si''2~ si''8 dod''' \grace re''' dod''' si''16 dod''' |
    re'''4 re''2 re''16 fad'' la'' re''' |
    do'''4 <do''' la'>2:16\p q4:16 |
    q8
  }
  \tag #'violino2 {
    \rp#2 { re'8*2/3-\sug\p fad' la' re'' la' fad' } |
    \rp#2 { re'8*2/3 fad' la' re'' la' fad' } |
    re' sol' si' re'' si' sol' dod''![ re'' mi''] dod''[ re'' mi''] |
    re''[ la' fad'] re'[ fad' la'] re''[ la' fad'] re'[-\sug\f fad' la'] |
    sol'[ si' la'] sol'[ fad' sol'] mi'[ fad' sol'] mi'[ fad' sol'] |
    fad' la' fad' re' fad' la' re'' la' fad' re' fad' la' |
    fad'4 <la' fad''>2:16-\sug\p q4:16 |
    q8
  }
>> r8 red'4(\f fad' la') |
do''2 la'4(\p sol' |
fad' la' fad' red') |
mi'8 r si4(\f mi' sol') |
si'(\p sol' mi' fad') |
sol'4( mi' dod' mi') |
la4.\fermata <<
  \tag #'violino1 {
    fad''8-\sug\p fad''4. fad''8 |
    si''2.\fp si''4 |
    re'''4. si''8 si''4. fad''8 |
    sol''4 sol'16\f fad' sol' fad' \rt#4 { sol' fad' } |
    sol'4 sol''2\p sol''4 |
    fad'' lad''8. si''16 dod'''4 mi''8. fad''16 |
    re''16
  }
  \tag #'violino2 {
    dod''8-\sug\p dod''4. dod''8 |
    <fad' re'>2:16-\sug\fp q:16 |
    q:16 q:16 |
    mi'4 mi'16-\sug\f red' mi' red' \rt#4 { mi' red' } |
    mi'16 si' si' si' si'4:16-\sug\p si'2:16 |
    dod''16 <fad' lad>[ q q] q4:16 q2:16 |
    <si fad'>16
  }
>> <re'' si''>16[\ff q q] q4:16 q2:16 |
<dod'' lad''>2.\fermata <<
  \tag #'violino1 {
    re'4\p |
    dod'4 dod'16 dod' dod' dod' dod'4 dod'16 dod' dod' dod' |
    si4 si'':16\f re''':16 si'':16 |
    sol''4 <sol' sol>4~ sol'8 la'16 si' do'' re'' mi'' fad'' |
    sol''4
  }
  \tag #'violino2 {
    si4-\sug\p |
    si si16 si si si lad4 lad16 lad lad lad |
    si4 si':16-\sug\f re'':16 si':16 |
    sol'8 <re' si>[ q q] q2:8 |
    q4
  }
>> sol''16 fad'' mi'' re'' dod''!4 mi''16 re'' dod'' si' |
lad'2.\fermata <<
  \tag #'violino1 {
    re'4\p |
    dod' dod'16 dod' dod' dod' dod'4 dod'16 dod' dod' dod' |
    si re'''\p re''' re''' re'''4:16\cresc re'''2:16 |
    dod''':16 dod''':16 |
    dod''':16\ff dod''':16 |
    <si'' si'>4
  }
  \tag #'violino2 {
    si4-\sug\p |
    si si16 si si si lad4 lad16 lad lad lad |
    si' <re'' si''>-\sug\p q q q4:16-\sug\cresc q2:16 |
    <si'' dod''>2:16 q:16 |
    <dod'' lad''>:16-\sug\ff q:16 |
    <si'' si'>4
    
  }
>> si4. si16 dod' \grace mi'8 re' dod'16 si |
lad4 dod' fad'4. mi'16*2/3 re' dod' |
re'4 si4. si16 dod' \grace mi'8 re' dod'16 si |
lad4 dod' fad'4. mi'16*2/3 re' dod' |
re'4 si4. si16 re' sol'8 sol' |
sol'4 dod'4. dod'16 mi' la'8 la' |
la'4 re'4. fad'16 la' re''8 re'' |
<<
  \tag #'violino1 {
    do''4 do'16 si do' si \rt#4 { do' si } |
    do'2
  }
  \tag #'violino2 {
    <fad' la'>4 la16 sold la sold \rt#4 { la sold } |
    la2
  }
>> r2 |
R1 |
r4 <re' si' sol''> sol r |
R1 |
r2 r4 <sol re' si'> |
