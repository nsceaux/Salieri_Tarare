\clef "treble" R1*8 |
r2\fermata r |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { fad''1~ | fad'' | sol''~ | sol'' |
    fad''1 | fad''4 si''2 si''4 | lad''2.\fermata }
  { re''1~ | re'' | mi''~ | mi'' |
    mi'' | re''4 re''2 re''4 | dod''2.\fermata }
  { s1*2-\sug\fp | s1 | s1*2-\sug\p | s4 s2.-\sug\ff }
>> r4 |
R1 |
r4 <>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { si''4 re''' si'' | sol'' }
  { si'4 re'' si' | sol' }
>> \tag #'oboi <>^"unis" \once\tieDashed sol'4~ sol'8 la'16 si' do'' re'' mi'' fad'' |
sol''4 sol''16( fad'' mi'' re'') dod''!4 si' |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { fad''2.\fermata }
  { lad'\fermata }
>> r4 |
R1 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { re'''1 | dod''' | dod''' | si''4 }
  { si''1 | si'' | lad'' | si''4 }
  { s1*3-\sug\p-\sug\cresc | s4\! }
>> \tag #'oboi <>^"unis" si'16 lad' si' lad' \rt#4 { si' lad' } |
si'2 r |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { fad''1~ | fad'' | sol'' | fad'' | si''2 dod''' | re'''4 }
  { la'1~ | la' | si'2 dod'' | la'1 | sol'' | fad''4 }
  { s1*4-\sug\p | s1-\sug\f }
>> r4 r2 |
r4 <>-\sug\p \twoVoices#'(oboe1 oboe2 oboi) <<
  { do'''2 do'''4~ | do''' }
  { fad''2 fad''4~ | fad'' }
>> r4 r2 |
R1*5 |
r2\fermata r |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { fad''1~ | fad'' | sol'' | sol'' |
    fad'' | fad''4 si''2 si''4 | lad''2.\fermata }
  { re''1~ | re'' | mi'' | mi'' |
    mi'' | re''4 re''2 re''4 | dod''2.\fermata }
  { s1*3-\sug\fp | s1*2-\sug\p | s4 s2.-\sug\ff }
>> r4 |
R1 |
r4 <>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { si'' re''' si'' | sol'' }
  { si'4 re'' si' | sol' }
>> \tag #'oboi <>^"unis" \once\tieDashed sol'4~ sol'8 la'16 si' do'' re'' mi'' fad'' |
sol''4 sol''16 fad'' mi'' re'' dod''!4 si' |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { fad''2.\fermata }
  { lad'\fermata }
>> r4 |
R1 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { re'''1 | dod''' | dod''' | si''4 }
  { si''1 | si'' | lad'' | si''4 }
  { s1*2-\sug\p -\sug\cresc | s1-\sug\ff }
>> r4 r2 |
R1*12 |
