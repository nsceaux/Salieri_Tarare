\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso)
   (oboi #:score-template "score-oboi")
   (fagotti #:tag-notes fagotti)
   (tromboni #:score "score-tromboni")
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
