Fan -- tô -- me vain ! i -- do -- le po -- pu -- lai -- re,
dont le nom seul ex -- ci -- tait ma co -- lè -- re,
Ta -- rare !… en -- fin tu mour -- ras cet -- te fois,
fan -- tô -- me vain !
Ta -- rare !… en -- fin tu mour -- ras cet -- te fois,
en -- fin tu mour -- ras cet -- te fois !
Ah ! pour A -- tar, quel bien cé -- les -- te,
quel bien cé -- les -- te,
d’im -- mo -- ler l’ob -- jet qu’il dé -- tes -- te,
a -- vec le fer sou -- ple des loix,
a -- vec le fer sou -- ple des loix !
Fan -- tô -- me vain ! i -- do -- le po -- pu -- lai -- re,
dont le nom seul ex -- ci -- tait ma co -- lè -- re,
Ta -- rare !… en -- fin tu mour -- ras cet -- te fois,
fan -- tô -- me vain !
Ta -- rare !… en -- fin tu mour -- ras cet -- te fois,
en -- fin tu mour -- ras cet -- te fois !

Trou -- ve- t-on Cal -- pi -- gi ?

Sei -- gneur, on suit sa tra -- ce.

À qui l’ar -- rê -- te -- ra, je don -- ne -- rai sa pla -- ce.
