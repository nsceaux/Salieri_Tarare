\clef "bass" re4\ff fad si2 |
la4 mi dod la, |
re fad si2 |
la4 mi dod la, |
re fad si2 |
dod4 mi la2 |
si,4 re sol2 |
la,4 la,8 la, la,2:8 |
la,2\fermata lad,\p |
si,2:8-\sug\fp si,:8 |
si,:8 si,:8 |
<< mi2:8 { s4 s\f } >> mi2:8 |
mi:8-\sug\p mi:8 |
lad,:8 lad,:8 |
si,4 sol(\ff fad mid) |
fad2.\fermata si,4\p |
mi4 mi16 mi mi mi fad4 fad16 fad fad fad |
sol4 si\f re' si |
sol2:8 sol:8 |
sol4 r sol r |
fad2.\fermata si,4\p |
mi4 mi16 mi mi mi fad4 fad16 fad fad fad |
sol2:8\p sol:8\cresc |
mi:8 mi:8 |
fad:8 fad:8 |
si4 si,8 si, si,2:8 |
si,4 si,\ff si, dod |
re4\p r re r |
re r re r |
re r re r |
re r re\f r |
re r re r |
re r re r |
red2:8 red:8\p |
red8 r red4(\f fad la) |
do'2 la4(\p sol |
fad la fad red) |
mi8 r si,4(\f mi sol) |
si(\p sol mi fad) |
sol( mi dod mi) |
la,2\fermata lad,\p |
si,2:8\fp si,:8 |
si,:8 si,:8 |
mi8 mi mi\f mi mi2:8 |
<< mi2:8 { s4 s-\sug\p } >> mi2:8 |
lad,:8 lad,:8 |
si,4 sol(\ff fad mid) |
fad2.\fermata si,4\p |
mi mi16 mi mi mi fad4 fad16 fad fad fad |
sol4 si,\f re si, |
sol,8 sol sol sol sol2:8 |
sol4 r sol r |
fad2.\fermata si,4\p |
mi mi16 mi mi mi fad4 fad16 fad fad fad |
sol2:8\p sol:8\cresc |
mi:8 mi:8 |
fad:8\ff fad:8 |
si,4 re sol2 |
fad4 dod lad, fad, |
si, re sol2 |
fad4 dod lad, fad, |
si, re sol2 |
dod4 mi la2 |
re4 fad8. la16 re'2~ |
re'4 re8 re re2:8 |
re2 r |
R1 |
r4 sol sol, r |
R1 |
r2 r4 sol |
