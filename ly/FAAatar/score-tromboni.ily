\score {
  \new StaffGroup <<
    \new Staff << \global \keepWithTag#'tr1 \includeNotes "tromboni" >>
    \new Staff << \global \keepWithTag#'tr2 \includeNotes "tromboni" >>
    \new Staff << \global \keepWithTag#'tr3 \includeNotes "tromboni" >>
  >>
  \layout { }
}
