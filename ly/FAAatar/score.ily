\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with { \tromboniInstr } <<
        \global \includeNotes "tromboni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Atar
      shortInstrumentName = \markup\character At.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*6\break s1*7\pageBreak
        s1*6\break s1*7\pageBreak
        s1*6\break s1*7\pageBreak
        s1*7\break s1*6\pageBreak
        s1*6\break s1*7\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
