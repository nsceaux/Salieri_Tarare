\clef "alto" R1*8 |
r2\fermata r |
<\tag#'tr3 si \tag#'tr2 re' \tag#'tr1 fad'>1-\sug\fp~ |
q |
<\tag#'tr3 si \tag#'tr2 mi' \tag#'tr1 sol'>4 q2-\sug\f q4~ |
q1-\sug\p |
<\tag#'tr3 dod' \tag#'tr2 mi' \tag#'tr1 fad'>1 |
<<
  \tag #'(tromboni tr1 tr2) \new Voice {
    \tag #'tromboni \voiceOne
    <\tag#'tr2 re' \tag#'tr1 fad'>4 <\tag#'tr2 re' \tag#'tr1 si'>2 q4 |
    <\tag#'tr2 dod' \tag#'tr1 lad'>2.\fermata
  }
  \tag #'(tromboni tr3) \new Voice {
    \tag #'tromboni \voiceTwo
    si4 sol fad mid |
    fad2.\fermata
  }
  { s4 s2.-\sug\ff }
>> r4 |
R1 |
\tag #'tromboni <>^"unis" sol!4 si\f re' si |
sol \once\tieDashed <\tag#'tr3 si \tag#'tr2 re' \tag#'tr1 sol'>2.~ |
q2 <\tag#'tr3 si \tag#'tr2 dod' \tag#'tr1 mi'> |
<\tag#'tr3 lad \tag#'tr2 dod' \tag#'tr1 fad'>2.\fermata r4 |
R1 |
\once\tieDashed <\tag#'tr3 sol \tag#'tr2 re' \tag#'tr1 si'~>1\p\cresc |
<\tag#'tr3 mi' \tag#'tr2 sol' \tag#'tr1 si'>1 |
<\tag#'tr3 dod' \tag#'tr2 fad' \tag#'tr1 lad'> |
<\tag#'tr3 re' \tag#'tr2 fad' \tag#'tr1 si'>4 si2 si4 |
si4\! r r2 |
R1*13 |
r2\fermata r |
<\tag#'tr3 si \tag#'tr2 re' \tag#'tr1 fad'>1_\markup\dynamic\concat { f \tiny p } ~ |
q |
<\tag#'tr3 si \tag#'tr2 mi' \tag#'tr1 sol'>4 q2-\sug\f \once\tieDashed q4~ |
q1-\sug\p |
<\tag#'tr3 dod' \tag#'tr2 mi' \tag#'tr1 fad'>1 |
<<
  \tag #'(tromboni tr1 tr2) \new Voice {
    \tag #'tromboni \voiceOne
    <\tag#'tr2 re' \tag#'tr1 fad'>4 <\tag#'tr2 re' \tag#'tr1 si'>2 q4 |
    <\tag#'tr2 dod' \tag#'tr1 lad'>2.\fermata
  }
  \tag #'(tromboni tr3) \new Voice {
    \tag #'tromboni \voiceTwo
    si4 sol fad mid |
    fad2.\fermata
  }
  { s4 s2.-\sug\ff }
>> r4 |
R1 |
\tag #'tromboni <>^"unis" sol!4 si\f re' si |
sol \once\tieDashed <\tag#'tr3 si \tag#'tr2 re' \tag#'tr1 sol'>2.~ |
q2 <\tag#'tr3 si \tag#'tr2 dod' \tag#'tr1 mi'> |
<\tag#'tr3 lad \tag#'tr2 dod' \tag#'tr1 fad'>2.\fermata r4 |
R1 |
<\tag#'tr3 sol \tag#'tr2 re' \tag#'tr1 si'~>1\p\cresc |
<\tag#'tr3 mi' \tag#'tr2 sol' \tag#'tr1 si'>1 |
<\tag#'tr3 dod' \tag#'tr2 fad' \tag#'tr1 lad'>-\sug\ff |
<\tag#'tr3 re' \tag#'tr2 fad' \tag#'tr1 si'>4 r4 r2 |
R1*12 |
