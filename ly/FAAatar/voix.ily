\clef "bass" <>^"Chanté" R1*8 |
r4\fermata r8 fad fad4. fad8 |
si2. si4 |
re'4. si8 si4. fad8 |
sol4 sol r2 |
r4 sol sol sol |
fad4 lad8. si16 dod'4 mi8. fad16 |
re4 re r r8 re' |
dod'2.\fermata si4 |
mi4 mi8 mi fad4 fad8 fad |
sol4 si re' si |
sol2 r |
r r4 r8 si |
fad'2.\fermata si4 |
mi mi8 mi fad4 fad8 fad |
sol2 r4 re' |
dod'2 dod'4. dod'8 |
fad'2 fad4. fad8 |
si2 r |
R1 |
re'2 la4. fad8 |
re2. re4 |
sol2. sol4 |
fad fad r re |
si2. dod'4 |
re' re r re8 re' |
do'4. do'8 do'4 do'8 do' |
do'4 red r \tag #'atar <>^\markup\italic { avec une joie concentrée } la |
do'2 la4 sol |
fad4( la) fad red |
mi2 r4 sol |
si2 mi4 fad |
sol4( mi) dod mi |
la,4.\fermata fad8 fad4. fad8 |
si2. si4 |
re'4. si8 si4. fad8 |
sol4 sol r2 |
r4 sol sol sol |
fad lad8 si dod'4 mi8 fad |
re4 re r re' |
dod'!2.\fermata si4 |
mi mi8 mi fad4 fad8 fad |
sol4 si re' si |
sol2 r |
r r4 si |
fad'2.\fermata si4 |
mi mi8 mi fad4 fad8 fad |
sol2 r4 re' |
dod'2 dod'4. dod'8 |
fad'2 fad4. fad8 |
si2 r |
R1*7 |
r4 r8^\markup\right-align Parlé \tag #'atar <>^\markup\italic aux Eunuques la16 la la4 la8 la |
re'4
\ffclef "bass" <>^\markup\character Un Eunuque
r8 la re'4 r16 re' do' re' |
si8 si r4
\ffclef "bass" <>^\markup\character Atar
r4 r8 sol |
re' re' mi' fa' si4 r8 sol |
si si si do' sol sol r4 |
