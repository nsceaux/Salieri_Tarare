\key si \minor
\tempo "Allegro Fièrement" \midiTempo#140
\time 2/2 s1*8 s4.
\tempo "Andante Maestoso" s8 s2 s1*18
\tempo "Allegro" s1*13 s4.
\tempo\markup { \concat { 1 \super o } Tempo } s8 s2 s1*16
\tempo "Allegro Fièrement" s1*13
