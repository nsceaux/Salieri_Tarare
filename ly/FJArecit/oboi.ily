\clef "treble" \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''1~ |
    fad''~ |
    fad'' |
    sol'' |
    fa''!2 mi''~ |
    mi'' fad''4 }
  { red''1~ |
    red''~ |
    red'' |
    si' |
    si'2 do''~ |
    do'' la'4 } |
  { s1-\sug\p }
>> r4 |
r <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''4 sol'' }
  { la' sol' }
>> r4 |
R1*10 |
