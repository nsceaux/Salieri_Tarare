\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with { \flautiInstr } <<
        { \noHaraKiri s1*7 \revertNoHaraKiri }
        \global \keepWithTag #'flauti \includeNotes "flauti"
      >>
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with { \fagottiInstr } <<
        { \noHaraKiri s1*7 \revertNoHaraKiri }
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroupNoBar <<
      \new StaffGroupNoBracket <<
        \new GrandStaff \with { \violiniInstr } <<
          \new Staff \with { \consists "Metronome_mark_engraver" } <<
            \global \keepWithTag #'violino1 \includeNotes "violini"
          >>
          \new Staff <<
            \global \keepWithTag #'violino2 \includeNotes "violini"
          >>
        >>
        \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      >>
      \new Staff \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with {
        \bassoInstr
        \consists "Metronome_mark_engraver"
      } <<
        \global \includeNotes "basso"
        \origLayout {
          s1*4\break s1*4\break s1*3 s2 \bar "" \pageBreak
          s2 s1*4\break
        }
      >>
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
