\clef "treble" <<
  \tag #'violino1 {
    si1\p~ |
    si~ |
    si~ |
    si |
    si'2 do''~ |
    do'' << la'4 \\ fad' >> r4 |
    r <re' la' fad''>\f <re' sib' sol''>2~ |
    sol''1\p~ |
    sol'' |
    fa''4. re''8\f re''4 re'' |
    do''1\p~ |
    do''2 do''4. do'''8\f |
    do'''4 dod''' re'''2~ |
    re'''1\p~ |
    re'''~ |
    re'''~ |
    re'''2.
  }
  \tag #'violino2 {
    fad'1-\sug\p~ |
    fad'~ |
    fad' |
    sol' |
    sol'~ |
    sol'2 <fad' la>4 r |
    r q-\sug\f re''2~ |
    re''1-\sug\p |
    <dod'' mi''>1 |
    re''4. <la' fa'>8-\sug\f q4 q |
    sol'1-\sug\p~ |
    sol'2 la'4. la''8-\sug\f |
    <la'' la'>4 <la' sol''> <la' fad''>2~ |
    q1-\sug\p~ |
    q |
    <si' sol''>1~ |
    q2.
  }
>> r4 |
r2 r4 <sol re' si'>4 |
