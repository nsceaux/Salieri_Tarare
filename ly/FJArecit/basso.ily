\clef "bass" si,1\p~ |
si,~ |
si, |
mi |
re2 do~ |
do~ do4 r |
r re\f sib,2~ |
sib,1\p |
la |
re4. re8\f re4 re |
mi1\p~ |
mi2 fa4. fa8\f |
fa4 mi re2~ |
re1\p~ |
re |
sol~ |
sol2. r4 |
r2 r4 sol |
