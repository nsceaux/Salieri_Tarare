\clef "bass" <>^\markup\character-text Le Génie Récit Parlé.
r2 r4 r8 fad |
si2 si8 r si8. si16 |
si4 fad8 sol la4 la8 si |
sol4 sol r sol8 la |
si4 si8 sol do'4 do'8 r |
r do' re' mi' fad4. sol8 |
re2
\ffclef "soprano/treble" <>^\markup\character La Nature
r4 sol' |
re''4. re''8 re'' re'' sol'' mi'' |
dod''4 r16 dod'' dod'' re'' mi''8. mi''16 sol''8 fa'' |
re''4 re''8 r r2 |
\ffclef "bass" <>^\markup\character Le Génie
r4 r8 sol do'4. do'8 |
do' do' do' do' la4 r |
r2 r4 la8 la |
la4 la8 la re'4. re'8 |
re'4 re' re' re' |
si2 si8 r re'4 |
fa'4 re'8 do' si4 si8 do' |
sol2 sol8 r r4 |
