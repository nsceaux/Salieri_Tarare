\clef "alto" red'1-\sug\p~ |
red'~ |
red' |
mi' |
fa'!2 mi'~ |
mi' re'4 r |
r re'-\sug\f sib2~ |
sib1-\sug\p |
la' |
la'4. re'8-\sug\f re'4 re' |
mi'1-\sug\p~ |
mi'2 fa'4. fa'8-\sug\f |
fa'4 mi' re'2 |
re'1-\sug\p~ |
re'~ |
re'~ |
re'2. r4 |
r2 r4 <sol re' si'> |
