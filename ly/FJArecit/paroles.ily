Na -- tu -- re, quel e -- xemple im -- po -- sant et fu -- nes -- te !
Le sol -- dat monte au trô -- ne, et le ty -- ran est mort !

Les dieux ont fait leur pre -- mier sort :
leur ca -- rac -- tère a fait le res -- te.

En -- core un gé -- né -- reux ef -- fort.
Dans le cœur des hu -- mains, d’un trait i -- nal -- té -- ra -- ble,
gra -- vons ce pré -- cepte ad -- mi -- ra -- ble.

De ce grand bruit, de cet é -- clat,
ô ciel ! ap -- prends- nous le mys -- tè -- re !
