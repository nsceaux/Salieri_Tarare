\clef "bass" r4 |
do\p r sol, r |
do r do r |
fa r fa r |
sol r sol, r |
r8. sol16\p sol8.\trill fad32 sol sol,4 r |
r8. do'16 do'8.\trill si32 do' do4 r |
re r re r |
r8. sol16\f sol8.\trill fad32 sol sol,4 r |
r8. sol16\p sol8.\trill fad32 sol sol,4 r |
r8. do'16 do'8.\trill si32 do' do4 r |
re r re r |
sol sol do\f do |
re re re re |
sol, sol8. sol16 sol4 sol |
sol r\fermata r2 |
sol,4\p r re r |
sol sol,\f si,\p re |
fa!2 r4 re' |
si4. si8 re'4. si8 |
sold4 mi\f r2 |
la,4-\sug\p ^\markup\italic Pizzicato r4 la, r |
mi r mi r |
mi r mi r |
la, r la r |
mi r do r |
sol r sol, r |
sol r sol, r |
do r r2 |
r fa4^\markup\italic col arco r |
r2 r4 sol |
do r r2 |
R1 |
r4 do'\f la si |
do' mi fa sol |
do4. do8 do4 do |
do2 r4 r8 do |
do2 re |
