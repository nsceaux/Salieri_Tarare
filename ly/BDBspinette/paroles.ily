\tag #'spinette {
Oui, Sei -- gneur, je veux la ré -- dui -- re,
vous li -- vrer son cœur, et l’ins -- trui -- re
du res -- pect, du re -- tour qu’el -- le doit à vos feux,
du res -- pect, du re -- tour qu’el -- le doit à vos feux,
qu’el -- le doit à vos feux.

Et… si ce grand suc -- cès cons -- ter -- ne
le chef… puis -- sant qui vous gou -- ver -- ne,
mon maî -- tre ap -- pré -- cie -- ra le zè -- le de tous deux,
mon maî -- tre ap -- pré -- cie -- ra le zè -- le de tous deux.
}
Je l’en -- chaîne à tes pieds, si tu rem -- plis mes vœux.
