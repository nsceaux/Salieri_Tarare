<<
  \tag #'spinette {
    \clef "soprano/treble" <>^\markup\italic chanté ^\markup\italic Coquettement
    mi''8 re'' |
    do''2 si'8 do'' re'' si' |
    do''4 sol' sol'8 la' si' do'' |
    la'2. fa''8 re'' |
    si'4 si' r si'8 do'' |
    re''2 r4 re''8 sol' |
    mi''2. sol''8 mi'' |
    re''2 la'4. re''8 |
    \grace do''8 si'2 r4 re''8 si' |
    sol'2 sol'8[ si'] re''[ sol''] |
    \grace fad''8 mi''2 r4 sol''8 mi'' |
    re''2 re''4. fad''8 |
    sol''2 mi''8[ fad''] sol''[ mi''] |
    re''2 re''4. re''8 |
    sol'2 r |
    r4^\markup\italic Ton réfléchi si'\fermata sol'8. la'16
    \footnoteHere#'(0 . 0) \markup {
      Source : \score {
        \new Staff \withLyrics {
          \tinyQuote \clef "soprano"
          r4 si'\fermata sol'8. la'16 si'4.*1/2 do''8*1/2 | re''2.
        } \lyrics { Et… si ce grand suc -- cès }
        \layout { \quoteLayout }
      }
    }
    si'8. do''16 |
    re''2. re''4 |
    si' re'' r re'' |
    fa''!2 r4 re'' |
    si'4. si'8 re''4. si'8 |
    sold'4 sold' r^\markup\italic Légèrement mi' |
    la'4. si'8 do''4 do''8. re''16 |
    si'2. si'4 |
    mi''4. re''8 do''4. si'8 |
    do''2 r4 r8 sol' |
    do''4. re''8 mi''4 mi''8. fa''16 |
    re''2. sol'4 |
    sol''4. fa''8 mi''4. re''8 |
    mi''2
    \ffclef "bass"
  }
  \tag #'basse {
    \clef "bass" r4 | R1*27 | r2
  }
>> <>^\markup\character Atar
r4 do'8 do' |
do'4 si8 do' la4. la8 |
la la si do' sol4 r |
R1*7 |
