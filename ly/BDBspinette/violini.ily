\clef "treble"
<<
  \tag #'violino1 {
    r4 |
    r \grace la''8 sol''16 fad''32 sol'' r8 \grace do''8 si'16 la'32 si' r8 r4 |
    r \grace re''8 do''16 si'32 do'' r8 sol'4\trill do''\trill |
    r do''\trill la''\trill fa''8. re''16 |
    si'4 re''\trill sol''\trill r |
    r8. sol'16\p sol'8.-\sug\trill fad'32 sol' sol4 r |
    r8. do''16 do''8.\trill si'32 do'' do'4 sol''8. mi''16 |
    re''8 re''[ si'' sol''] fad''16 sol'' la'' sol'' fad'' mi'' re'' do'' |
    si'8. sol'16\f sol'8.\trill fad'32 sol' sol4 r |
    r8. sol'16\p sol'8.\trill fad'32 sol' sol8 si'([ re'' fa'']) |
    mi''8. do''16 do''8.\trill si'32 do'' do'8 mi''[ sol'' mi''] |
    r8 re''-! si''( sol'') r la'-! la''( fad'') |
    r re''-! si''( sol'') mi''16\f fad'' sol'' fad'' \grace la''8 sol'' fad''16 mi'' |
    re''4 re''16 sol'' si'' re''' re'''4 \grace sol''8 fad'' mi''16 fad'' |
    sol''4
  }
  \tag #'violino2 {
    r4 |
    mi'8-\sug\p sol' mi' sol' fa' sol' fa' sol' |
    mi' sol' mi' sol' mi' sol' mi' sol' |
    la' do'' la' do'' la' do'' la' do'' |
    si' re'' si' re'' si' re'' si' re'' |
    si'-\sug\p re'' si' re'' si' re'' si' re'' |
    do'' mi'' do'' mi'' do'' mi'' do'' mi'' |
    si' re'' si' re'' la' re'' la' re'' |
    si' re''-\sug\f si' re'' si' re'' si' re'' |
    si'-\sug\p re'' si' re'' si' re'' si' re'' |
    do'' mi'' do'' mi'' do'' mi'' do'' mi'' |
    si' re'' si' re'' la' re'' la' re'' |
    si' re'' si' re'' do''-\sug\f mi'' do'' mi'' |
    si' re'' si' re'' la' fad'' la' fad'' |
    sol''4
  }
>> <re' si' sol''>8. si''16 q4 q |
q r\fermata <<
  \tag #'violino1 {
    sol'8.\p la'16 si'8. do''16 |
    re''8 re' re' re' re'2:8 |
    re'4
  }
  \tag #'violino2 {
    r2 |
    r8 re''-\sug\p \grace mi'' re'' do''16 si' la'8 si' do''16 si' do'' la' |
    si'4
  }
>> sol'8.\f sol'16 si'8.\p si'16 re''8. re''16 |
fa''!2 r4 re'' |
si'4. si'8 re''4. si'8 |
sold'4 <mi' si' sold''>4\f <<
  \tag #'violino1 {
    r4 r8 mi''\p |
    la''4. si''8 do'''4 do'''8. re'''16 |
    si''4 mi''16 red'' mi'' red'' mi''4 r8 si'' |
    mi'''4. re'''8 do'''4. si''8 |
    do'''4 do'''16 si'' do''' si'' do'''4 r8 sol'! |
    do''4. re''8 mi''4 mi''8. fa''16 |
    re''4 sol''16 fad'' sol'' fad'' sol''4 r8 re'' |
    sol''4. fa''8 mi''4 re'' |
    mi''4 r r2 |
    r2 la'4 r |
  }
  \tag #'violino2 {
    r2 |
    mi'8-\sug\p do'' mi' do'' mi' do'' mi' do'' |
    sold' si' sold' si' sold' si' sold' si' |
    sold' si' sold' si' la' mi' sold' mi' |
    mi' do'' mi' do'' mi' do'' mi' do'' |
    sol' do'' sol' do'' sol' do'' sol' do'' |
    si' re'' si' re'' si' re'' si' re'' |
    re' si' re' si' do'' sol' si' sol' |
    do''4 r r2 |
    r2 do'4 r |
  }
>>
r2 r4 <sol re' si'> |
<sol mi' do''>4 <<
  \tag #'violino1 {
    mi''16\p sol'' fa'' mi'' fa'' la'' sol'' fa'' re'' fa'' mi'' re'' |
    mi'' sol'' fa'' mi'' do'' mi'' re'' do'' la' do'' si' la' si' re'' do'' si' |
    do'' mi'' re'' do''
  }
  \tag #'violino2 {
    do'4\p la si |
    do' mi' fa' re' |
    mi'
  }
>> mi''16\f sol'' fa'' mi'' fa'' la'' sol'' fa'' re'' fa'' mi'' re'' |
mi'' sol'' fa'' mi'' do'' mi'' re'' do'' la' do'' si' la' si' re'' do'' si' |
do''4. do'8 do'4 do' |
do'2 r4 r8 do' |
do'2 re' |
