\clef "alto" r4 |
do'8-\sug\p mi' do' mi' re' fa' re' fa' |
do' mi' do' mi' do' mi' do' mi' |
fa' la' fa' la' fa' la' fa' la' |
re' si' re' si' re' si' re' si' |
re'-\sug\p si' re' si' re' si' re' si' |
mi' sol' mi' sol' mi' sol' mi' sol' |
sol' si' sol' si' fad' la' fad' la' |
sol' si'-\sug\f sol' si' sol' si' sol' si' |
sol'-\sug\p si' sol' si' re' si' re' si' |
mi' sol' mi' sol' mi' sol' mi' sol' |
sol' si' sol' si' fad' la' fad' la' |
sol' si' sol' si' sol'-\sug\f do'' sol' do'' |
sol' si' sol' si' fad' la' fad' la' |
sol'4 sol'8. sol'16 sol'4 sol' |
sol r\fermata sol8.\p la16 si8. do'16 |
re'8 si' \grace do'' si' la'16 sol' fad'8 sol' la'16 sol' la' fad' |
sol'4 sol8.-\sug\f sol16 si8.-\sug\p si16 re'8. re'16 |
fa'!2 r4 re' |
si4. si8 re'4. si8 |
sold4 mi'-\sug\f r2 |
do'8-\sug\p mi' do' mi' do' mi' do' mi' |
si sold' si sold' si sold' si sold' |
si sold' si sold' la' mi' sold' mi' |
do' mi' do' mi' do' mi' do' mi' |
do' sol' do' sol' mi' sol' mi' sol' |
re' si' re' si' re' si' re' si' |
si re' si re' sol' sol sol' sol |
do'4 r r2 |
r fa'4 r |
r2 r4 sol' |
do' do'-\sug\p la si |
do' mi' fa' re' |
mi' do'-\sug\f la si |
do' mi fa sol |
do'4. do'8 do'4 do' |
do'2 r4 r8 do' |
do'2 re' |
