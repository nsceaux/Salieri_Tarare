\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Spinette
      shortInstrumentName = \markup\character Sp.
      \haraKiri
    } \withLyrics <<
      { s4 s1*28 \set Staff.shortInstrumentName = ##f }
      \global \keepWithTag #'spinette \includeNotes "voix"
    >> \keepWithTag #'spinette \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      { s4 s1*35
        <>^\markup\right-align\italic {
          Spinette et Calpigi sortent en se menaçant.
        }
      }
      \global \includeNotes "basso"
      \origLayout {
        s4 s1*3\break s1*5\pageBreak
        s1*5\break s1*5\break s1*6\pageBreak
        s1*4\break s1*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
