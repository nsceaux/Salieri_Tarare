\clef "treble" sib4..\f sib16 re'8. fa'16 |
sib'2\fermata <<
  \tag #'violino1 {
    re''8.\p mib''16 |
    fa''4~ fa''8. re''16 re''8 sol'' |
    fa''\f \grace do''' sib''32 la'' sib'' do''' sib''8
  }
  \tag #'violino2 {
    sib'8.\p do''16 |
    re''4~ re''8. sib'16 sib'8 mib'' |
    re''-\sug\f \grace mib''16 re''32 do'' re'' mib'' re''8
  }
>> fa''8 re'' sib'\p |
la'8. sib'16 do''8 re'' mib'' <<
  \tag #'violino1 {
    sol''8 |
    fa''8 fa''4 fa'' \grace fa''8 mib''16 re''32 mib'' |
    re''8\f re'4 fa'8( sib' re'') |
    sol'4.\p sib'8( mib'' sol'') |
    fa''8 fa''4 re''16 fa'' sib'8. sib'16 |
    sol''8\f sol''4 sol'' \grace sol''8 fa''16 mib''32 fa'' |
    mi''8
  }
  \tag #'violino2 {
    re''8 |
    do'8 <fa' mib''>4 q q8 |
    <re'' fa'>16\f <re' fa'>8 q q q q q16 |
    <sib sol'>16\p q8 q q q q q16 |
    <sib fa'>16 q8 q q q q q16 |
    sib16-\sug\f <sol' sib'>8 q q q16 q8[ <la' fa'>] |
    sol'8
  }
>> \grace re'' do''32 si' do'' re'' do''8. do''16 do''8. do''16 |
<<
  \tag #'violino1 { <la' fa''>8 }
  \tag #'violino2 { <do'' fa'>8 }
>> \grace sol'' fa''32 mi'' fa'' sol'' fa''8\p do''-. la'-. fa'-. |
re''8 re''16.\f do''32 sib'8 la' <sol' sib' mi''>8. mi''16 |
<la' fa''>8. re''16-\sug\p do'' la'' fa'' do'''~ do''' mi'' sol'' mi'' |
fa''8.\f <fa' la>16 q4 <<
  \tag #'violino1 {
    la'8.\p sib'16 |
    do''8. do''16 do''8( fa'' mi'' sol'') |
    fa''8. do''16 do''4 do''8. mib''!16 |
    re''8.\fp re'16 re'8. re'16 re'8. do'16 |
  }
  \tag #'violino2 {
    fa'8.-\sug\p sol'16 |
    la'4. la'8( sol' sib') |
    la'8 la'4 la' la'8 |
    la'8.-\sug\fp la16 la8. la16 la8. la16 |
  }
>>
sib8 sol([\sf sib re' sol' sib']) |
<<
  \tag #'violino1 {
    mib''8\p mib'16( mib'') mib''8. mib''16 mib''8.\sf mib''16 |
    mib''8\f fa'4 do'' do''8\p reb''8.\cresc reb'16 reb'8. reb'16 <sib mi'>8.\f q16 |
    <la sol'>8
  }
  \tag #'violino2 {
    sib'4-\sug\p~ sib'8. <sib' mib'>16 q8.-\sug\sf q16 |
    <mib' do''>8-\sug\f <mib' do'>4 q q8-\sug\p |
    reb'8.-\sug\cresc sib16 sib8. sib16 sib8.-\sug\f sib16 |
    la8
  }
>> do'32 do' do' do' do'4 r |
<<
  \tag #'violino1 {
    r8 re''\p r re'' r mi'' |
    r dod'' r dod'' r re'' |
    r mi''( sol'' mi'' dod'' mi'') |
    re'' re''~ re''16(\sf dod'' re'' mi'' fa'' mi'' fa'' fad'') |
    sol''8.\fp sol''16 sol''8( fa'' mi'' re'') |
    dod'' dod'' r dod'' r re'' |
    r mi'' r sib''(\sf la'' dod'') |
    re''8.
  }
  \tag #'violino2 {
    r8 fa'-\sug\p r sol' r sol' |
    r sol' r sol' r fa' |
    r sib'4 sib'8( la' sol') |
    <fa' sib>4~ q16-\sug\sf( dod' re' mi' fa' mi' fa' fad') |
    sol'8-\sug\fp <sol' sib'> r q r q |
    r <mi' la'> r q r <re' la'> |
    r sib' r sol' r mi' |
    fa'8.
  }
>> re'16\f re''8 re'' do''!8.\trill sib'32 do'' |
sib'2 \fermata <<
  \tag #'violino1 {
    re''8. mib''16 |
    fa''4~ fa''8. re''16 re''8 sol'' |
    fa'' \grace do''' sib''32 la'' sib'' do''' sib''8
  }
  \tag #'violino2 {
    sib'8. do''16 |
    re''4~ re''8. sib'16 sib'8 mib'' |
    re'' \grace mib'' re''32 do'' re'' mib'' re''8
  }
>> fa''8 re'' sib' |
la'8. sib'16 do''8 re'' mib'' <<
  \tag #'violino1 {
    sol''8 |
    fa''8 fa''4 fa'' \grace fa''8 mib''16 re''32 mib'' |
    re''8 re'4( fa'8 sib' re'') |
    sol'4~ sol'8( sib' mib'' sol'') |
    fa''8 fa''4 re''8 sib'8. sib'16 |
    sol''16 sol''8 sol'' sol'' sol''16 sol''8 \grace sol'' fa''16 mi''32 fa'' |
    mi''2
  }
  \tag #'violino2 {
    re''8 |
    do' <fa' do''>4 q q8 |
    <fa' re''>16 <fa' re'>8 q q q q q16 |
    <sib sol'>16 q8 q q q q q16 |
    <sib fa'>16 q8 q q q q q16 |
    <sol' sib'>16 q8 q q q16 q8[ <la' fa'>] |
    sol'2
  }
>> do''8. do''16 |
<<
  \tag #'violino1 { <la' fa''>4 }
  \tag #'violino2 { <do'' fa'>4 }
>> fa''8-! do''-! la'-! fa'-! |
<<
  \tag #'violino1 {
    <fa' mib''!>2.\fermata |
    re'4\doux ~ re'8 do' re' mib' |
    fa'4~ fa'8 sib'( la' sol') |
    fa'4.( mib'8 re'4) |
  }
  \tag #'violino2 {
    <la' la>2.\fermata |
    sib4-\sug\p~ sib8 la sib do' |
    re'4~ re'8 sol'( fa' mib') |
    re'4.( do'8 sib4) |
  }
>>
<re' sib' sol''>8 mib''16[-!\ff sol'']-! sib''-! sol''-! mib''-! sol''-! sib'-! mib''-! sol'-! sib'-! |
<mib' sol>2 <<
  \tag #'violino1 {
    sol''8.\p sol''16 |
    fa''4 re''8 fa'' \grace fa'' mib'' re''16 do'' |
    re''8(\ff mib'' fa'' sol'' la''16 sib'' sol'' mib'') |
    fa''16
  }
  \tag #'violino2 {
    mib''8.-\sug\p mib''16 |
    re''4 sib'8 re'' \grace re'' do'' sib'16 la' |
    sib'8-\sug\ff sib'4 sib' sib'8 |
    sib'16
  }
>> <re'' sib''>16 q q q4:16 <la'' do''>:16 |
<sib' sib''>4 <<
  \tag #'violino1 {
    sib'8. re''16 sib'8. re''16 |
    sib'2.\fermata |
  }
  \tag #'violino2 {
    re'8. fa'16 re'8. fa'16 |
    re'2.\fermata |
  }
>>
