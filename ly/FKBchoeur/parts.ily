\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (flauti #:score-template "score-oboi" #:instrument "Hautbois")
   (oboi #:score-template "score-oboi")
   (clarinetti #:score-template "score-part-voix")
   (fagotti)
   (corni #:tag-global () #:score-template "score-part-voix"
          #:instrument "Cors in Mi♭")
   (basso)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
