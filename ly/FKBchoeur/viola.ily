\clef "alto" sib4..\f sib16 re'8. fa'16 |
sib'2\fermata r4 |
sib'8.-\sug\p sib16 sib8. sib16 sib8. sib16 |
sib2\f r8 sib\p |
fa'8. sol'16 la'8 sib' do'' sib' |
la'8 la la la la la |
sib16-\sug\f re'8 re' re' re' re' re'16 |
mib'-\sug\p mib'8 mib' mib' mib' mib' mib'16 |
re' re'8 re' re' re' re' re'16 |
do'-\sug\f do'8 do' do' do' do' do'16 |
do'2 do''4 |
fa' fa'8\p do'' la' fa' |
re'8 re'16.\f do'32 sib8 la sol sib' |
la'8. sib'16-\sug\p la'8 <fa' la> <sol mi'>16 sol'[-! sib'-! sol'] |
la'8.-\sug\f fa16 fa4 r |
fa'2:8 do'8 do' |
fa'2. |
fad'8.-\sug\fp fad16 fad8. fad16 fad8. fad16 |
sol2. |
sol4-\sug\p sol solb-\sug\sf( |
fa8-\sug\f) la4 la la8-\sug\p |
sib8.-\sug\cresc sib16 sib8. sib16 reb'8.-\sug\f reb'16 |
do'8 fa32[ fa fa fa] fa4 r |
sib8-\sug\p r sib r sib r |
r mi' r mi' r mi' |
r sol' r sol' r mi' |
re'4 sib r8 do'' |
sib'-\sug\fp sib' r sol r sol |
r la r la r la |
r re' r re' r dod' |
re'8. re16-\sug\f re'8 re' do'!8. sib32 do' |
sib2\fermata r4 |
sib'8. sib16 sib8. sib16 sib8. sib16 |
sib2 r8 sib |
fa'8. sol'16 la'8 sib' do'' sib' |
la' la la la la la |
sib16 sib8 sib sib sib sib sib16 |
mib' mib'8 mib' mib' mib' mib' mib'16 |
re'16 re'8 re' re' re' re' re'16 |
do' do'8 do' do' do' do' do'16 |
do'2 do'8. do'16 |
fa4 fa8 do'' la' fa' |
fa'2.\fermata |
sib8-\sug\p sib sib sib sib sib |
sib2.:8 |
sib8( re' fa' fa) sib[ sib] |
mib' <sib sol'>4-\sug\ff q q8 sib2 mib4-\sug\p |
fa fa fa |
sib8-\sug\ff( do' re' mib') fa'16( sol' mib' mib') |
fa'2:8 fa8 fa |
sib4 sib sib |
sib2.\fermata |
