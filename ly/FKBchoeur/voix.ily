<<
  \tag #'(vdessus basse) {
    <<
      \tag #'basse { s2.*14 | s2 \ffclef "soprano/treble" }
      \tag #'vdessus {
        \clef "soprano/treble" R2.*14 | r4 r
      }
    >> <>^\markup\character Deux Femmes
    <<
      { \voiceOne la'8. sib'16 |
        do''8. do''16 do''8[ fa''] mi'' sol'' |
        fa''4 do'' do''8. mib''!16 |
        re''4 re''8. re''16 re''8. do''16 |
        sib'2 sib'8. sib'16 |
        mib''8. mib''16 mib''4 mib''8. mib''16 |
        mib''4 fa'8 do'' do'' do'' |
        reb''4.. reb''16 mi''8. mi''16 |
        fa''2 \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo fa'8. sol'16 |
        la'8. la'16 la'4 sol'8 sib' |
        la'4 la' la'8. la'16 |
        la'4 la'8. la'16 la'8. la'16 |
        sol'2 sol'8. sol'16 |
        sib'8. sib'16 sib'4 sib'8. sib'16 |
        la'4 la'8 la' la' la' |
        sib'4.. sib'16 sib'8. sib'16 |
        la'2
      }
    >> r8 <<
      { \voiceOne fa'8 |
        re''!8. re''16 re''8 re'' re'' mi'' |
        dod''4 dod'' re''8. re''16 |
        mi''4 sol''8 mi'' dod'' mi'' |
        re''2 \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo fa'8 |
        fa'8. fa'16 sol'8 sol' sol' sol' |
        sol'4 sol' fa'8. fa'16 |
        sib'4 sib'8 sib' la' sol' |
        fa'2
      }
    >> r8 <<
      { \voiceOne re''8 |
        sol''8. sol''16 sol''8 fa'' mi'' re'' |
        dod''4 dod'' re''8. re''16 mi''4 mi''8 mi'' la''8. dod''16 |
        re''2 \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo re''8 |
        sib'8. sib'16 sib'8 sib' sib' sib' |
        la'4 la' la'8. la'16 sib'4 sib'8 sib' la'8. la'16 |
        re''2
      }
    >> r4 |
    \once\override Voice.Script.outside-staff-priority = #100
    \once\override Voice.TextScript.outside-staff-priority = #900
    <>^\markup\character [Chœur général] sib'2^\fermata re''8. mib''16 |
    fa''4. re''8 re'' sol'' |
    fa''2 r8 sib' |
    la'8. sib'16 do''8 re'' mib'' sol'' |
    fa''2~ fa''8.[ mib''16] |
    re''2 r4 |
    sol'4~ sol'8 sib' mib'' sol'' |
    fa''4( re'') sib'8. sib'16 |
    sol''2~ sol''8.[ fa''16] |
    mi''2 do''8. do''16 |
    fa''4 fa''8 do'' la' fa' |
    mib''!2.\fermata |
    <>^\markup\character Les deux Femmes
    <<
      { \voiceOne re''4~ re''8 do'' re'' mib'' |
        fa''4~ fa''8[ sib''] la'' sol'' |
        fa''4.( mib''8) re''4 | \oneVoice }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sib'4~ sib'8 la' sib' do'' |
        re''4~ re''8[ sol''] fa'' mib'' |
        re''4.( do''8) sib'4 |
      }
    >>
    \once\override Voice.Script.outside-staff-priority = #100
    \once\override Voice.TextScript.outside-staff-priority = #900
    <>^"Tutti" sol''2. |
    mib''2 sol''8.^\p sol''16 |
    fa''4 re''8 fa'' \grace fa'' mib'' re''16[ do''] |
    re''8[ mib''] fa'' sol'' la''16[ sib''] sol'' mib'' |
    fa''2~ fa''8. fa''16 |
    sib'2 r4 |
    R2.\fermataMarkup |
  }
  \tag #'(vhaute-contre basse) {
    \clef "alto/G_8" <>^\markup\character-text Calpigi seul R2. |
    sib2\fermata re'8. mib'16 |
    fa'4. re'8 re' sol' |
    fa'2 r8 sib |
    la8. sib16 do'8 re' mib' sol' |
    fa'2~ fa'8.[ mib'16] |
    re'2 r4 |
    sol4. sib8 mib' sol' |
    fa'4.( re'8) sib8. sib16 |
    sol'2~ sol'8.[ fa'16] |
    mi'2 do'8. do'16 |
    fa'4 fa'8 do' la fa |
    re'2 mi'8. mi'16 |
    fa'8. re'16 do'4 do'8. do'16 |
    fa2
    \tag #'vhaute-contre {
      r4 |
      R2.*7 |
      r4 r <>^\markup\character Calpigi r8 fa'8 |
      fa'8. fa'16 re'8 re' sol' sol' |
      mi'4 mi' fa'8 fa' |
      sol'4 sol'8 sol' mi' dod' |
      re'2 r8 re' |
      re'8. re'16 re'8 re' sib sib |
      mi'4 mi' re'8. re'16 |
      re'4 re'8 re' dod' mi' |
      re'2 r4 |
      \once\override Voice.Script.outside-staff-priority = #100
      \once\override Voice.TextScript.outside-staff-priority = #900
      <>^"Tutti" sib2\fermata re'8. mib'16 |
      fa'4. re'8 re' sol' |
      fa'2 r8 sib |
      la8. sib16 do'8 re' mib' sol' |
      fa'2. |
      fa'2 r4 |
      sol'4~ sol'8 sol' sol' sol' |
      fa'2 fa'8. fa'16 |
      sol'2~ sol'8.[ fa'16] |
      mi'2 do'8. do'16 |
      fa'4 fa'8 do' la fa |
      do'2.\fermata |
      <>^\markup\character Calpigi re'4~ re'8 do' re' mib' |
      fa'4~ fa'8[ sib'] la' sol' |
      fa'4.( mib'8) re'4 |
      \once\override Voice.Script.outside-staff-priority = #100
      \once\override Voice.TextScript.outside-staff-priority = #900
      <>^"Tutti" sib'2. |
      sol'2 sib8.^\p sib16 |
      fa'4 fa'8 fa' mib' mib' |
      re'[ mib'] fa' sol' la'16[ sib'] sol' mib' |
      re'2 do'4 |
      sib2 r4 |
      R2.^\fermataMarkup |
    }
  }
  \tag #'vtaille {
    \clef "tenor/G_8" R2.*31 |
    \once\override Voice.Script.outside-staff-priority = #100
    \once\override Voice.TextScript.outside-staff-priority = #900
    <>^"Tutti" sib2\fermata sib8. do'16 |
    re'4. sib8 sib mib' |
    re'2 r8 sib |
    fa8. sol16 la8 sib do' re' |
    do'2. |
    sib2 r4 |
    mib'4~ mib'8 mib' mib' mib' |
    re'2 sib8. sib16 |
    sib2~ sib8.[ la16] |
    sol2 do'8. do'16 |
    fa4 fa8 do' la fa |
    do'2.\fermata |
    R2.*3 |
    \once\override Voice.Script.outside-staff-priority = #100
    \once\override Voice.TextScript.outside-staff-priority = #900
    <>^"Tutti" sol'2. |
    sib2 mib'8.^\p mib'16 |
    re'4 sib8 re' \grace re' do' sib16[ la] |
    sib4 sib8 sib sib sib16 sib |
    sib2 la4 |
    sib2 r4 |
    R2.^\fermataMarkup |
  }
  \tag #'vbasse {
    \clef "bass" <>^\markup\character-text Urson seul R2. |
    sib2\fermata sib8. do'16 |
    re'4. sib8 sib mib' |
    re'2 r8 sib |
    fa8. sol16 la8 sib do' sib |
    la2~ la8.[ do'16] |
    sib2 r4 |
    mib4. sol8 sib mib' |
    re'2 sib8. sib16 |
    sib2~ sib8.[ la16] |
    sol2 do'8. do'16 |
    fa4 fa8 do' la fa |
    re'2 sib8. sib16 |
    la8. sib16 do'4 do'8. do'16 |
    fa2 r4 |
    R2.*7 |
    r4 r <>^\markup\character Urson r8 fa |
    sib8. sib16 sib8 sib sib sib |
    la4 la re8. re16 |
    sol4 sol8 sol la8. la16 |
    sib2 r8 fad |
    sol8. sol16 sol8 sol sol sol |
    sol4 sol fa8. fa16 |
    sol4 sol8 sol la la |
    re2 r4 |
    \once\override Voice.Script.outside-staff-priority = #100
    \once\override Voice.TextScript.outside-staff-priority = #900
    <>^"Tutti" sib2\fermata sib8. sib16 |
    sib4. sib8 sib sib |
    sib2 r8 sib, |
    fa8. sol16 la8 sib do' sib |
    la2. |
    sib2 r4 |
    sib4~ sib8 sib sib sib |
    sib2 re'8. re'16 |
    do'2. |
    do'2 do'8. do'16 |
    fa4 fa8 do' la fa |
    fa2.\fermata |
    <>^\markup\character Urson sib4~ sib8 sib sib sib |
    sib2 sib8 sib |
    sib4.( la8) sib4 |
    \once\override Voice.Script.outside-staff-priority = #100
    \once\override Voice.TextScript.outside-staff-priority = #900
    <>^"Tutti" mib'2. |
    mib2 mib8.^\p mib16 |
    fa4 fa8 fa fa fa |
    sib,[ do] re mib fa16[ sol] mib mib |
    fa2 fa4 |
    sib,2 r4 |
    R2.^\fermataMarkup |
  }
>>