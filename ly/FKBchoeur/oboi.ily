\clef "treble" R2. |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { sib'2\fermata re''8. mib''16 |
    fa''4~ fa''8. re''16 re''8 sol'' |
    fa''2 }
  { sib'2\fermata sib'8. do''16 |
    re''4~ re''8. sib'16 sib'8 mib'' |
    re''2 }
  { s2-\sug\f s4\p | s2. | s2-\sug\f }
>> r4 |
R2.*2 |
r8 \twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''4 fa'' fa''8 |
    sol'2. |
    fa'2 sib'4 |
    sol''2~ sol''8 fa'' |
    mi''2 }
  { re''4 re'' re''8 |
    mib'2. |
    re'2 sib'4~ |
    sib'2~ sib'8 la' |
    sol'2 }
  { s4\f s4. | s2.\p | s | s-\sug\f }
>> \tag #'oboi <>^"unis" do''4 |
fa'' fa''8-\sug\p do'' la' fa' |
re'' re''16.-\sug\f do''32 sib'8 la' \twoVoices#'(oboe1 oboe2 oboi) <<
  { sol'8 mi'' |
    fa''8. re''16 do''8 la'' sol''8.\trill fa''32 sol'' |
    fa''4 }
  { sol'8 sib' |
    la'8. sib'16 la'8 fa'' mi''8.\trill re''32 mi'' |
    fa''4 }
  { s4 | s8. s16-\sug\p s2 | s4-\sug\f }
>> r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { la'8. sib'16 |
    do''4 do''8( fa'' mi'' sol'') |
    fa''4 do'' }
  { fa'8. sol'16 |
    la'4. la'8( sol' sib') |
    la'2 }
  { s4\p }
>> r4 |
R2.*2 |
r4 r \twoVoices#'(oboe1 oboe2 oboi) <<
  { mib'' |
    mib''2. |
    reb''2 mi'4 |
    fa' }
  { sib'4 |
    la'2. |
    sib' |
    la'4 }
  { s4 | s2.-\sug\f | s2-\sug\p-\sug\cresc s4-\sug\f | }
>> r4 r |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { re''2( mi''4) |
    dod''2 re''4 | }
  { fa'4( sol'2) |
    sol'2 fa'4 | }
  { s2.\p | }
>>
r8 \twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''8( sol'' mi'' dod'' mi'') |
    re''8 re''~ re''16( dod'' re'' mi'' fa'' mi'' fa'' fad'') |
    sol''4-\sug\fp~ sol''8( fa'' mi'' re'') |
    dod''2( re''4) |
    mi''2 la''8( dod'') |
    re''4 }
  { sib'4 sib'8( la' sol') |
    fa'2 r8 do'' |
    sib'2.-\sug\fp |
    la' |
    sib'4 sol' mi' |
    re'4 }
>> r4 r |
\tag #'(oboi oboe1) {
  \once\override Voice.Script.outside-staff-priority = #100
  \once\override Voice.TextScript.outside-staff-priority = #900
  <>^"Clarinettes avec les Hautbois"
}
sib'2-\sug\f\fermata \twoVoices#'(oboe1 oboe2 oboi) <<
  { re''8. mib''16 |
    fa''4~ fa''8. re''16 re''8 sol'' |
    fa''2. |
    la'8. sib'16 do''8 re'' mib''8 sol'' |
    fa''2~ fa''8 mib'' |
    re''8 re'4( fa'8 sib' re'') |
    sol'4~ \slurDashed sol'8( sib' mib'' sol'') |
    fa''4. re''8 sib'8. sib'16 |
    sol''2~ sol''8 fa'' |
    mi''2 }
  { sib'8. do''16 |
    re''4~ re''8. sib'16 sib'8 mib'' |
    re''2. |
    la'8. sib'16 do''8 re'' mib''8 re'' |
    do''2. |
    re''8 re'4( fa'8 sib' re'') |
    mib'4~ mib'8( sol' sib' mib'') |
    re''4. sib'8 fa' sib' |
    sib'2~ sib'8 la' |
    sol'2 }
>> r4 |
R2. |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mib''!2.\fermata |
    re''4~ re''8 do'' re'' mib'' |
    fa''4~ fa''8 \once\slurDashed sib''( la'' sol'') |
    fa''4.( mib''8 re''4) |
    sol''2. |
    mib''2 }
  { do''2.\fermata |
    sib'4~ sib'8 la' sib' do'' |
    re''4~ re''8 \once\slurDashed sol''( fa'' mib'') |
    re''4.( do''8 sib'4) |
    sib'2. |
    sol'2 }
  { s2. | s2.\p }
>> r4 |
R2. |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { re''8-\sug\ff( mib'' fa'' sol'' la''16 sib'' sol'' mib'') |
    re''2 do''4 |
    sib' sib'8. re''16 sib'8. re''16 |
    sib'2.\fermata | }
  { sib'2.-\sug\ff |
    sib'2 la'4 |
    sib'4 re'8. fa'16 re'8. fa'16 |
    re'2.\fermata | }
>>
