\clef "bass" sib,4..\f sib,16 re8. fa16 |
sib2\fermata r4 |
sib8.-\sug\p sib,16 sib,8. sib,16 sib,8. sib,16 |
sib,2\f r8 sib,\p |
fa8. sol16 la8 sib do' sib |
la8 la, la, la, la, la, |
sib,2.:8\f |
sib,:8\p |
sib,2:8 re8 re |
do2.:8\f |
do2 do'4 |
fa fa8\p do' la fa |
re'8 re'16.\f do'32 sib8 la sol8. sol16 |
fa8. sib,16\p do4 do |
fa8.\f fa16 fa4 r |
fa2:8\p do8 do |
fa2.:8 |
fad8.\fp fad16 fad8. fad16 fad8. fad16 |
sol2. |
sol4\p sol( solb)\f( |
fa8)\f fa fa fa fa\p fa |
fa2.:8-\sug\cresc |
fa4 fa\ff r |
sib,8\p^"pizzicato" r sib, r sib r |
la r la r re r |
sol r sol r la r sib r sib, r sib, la, |
sol,-\sug\fp r sol r sol r |
sol r sol r fa r |
sol r sol r la r |
re4 re\f(^"con l’arco" do) |
sib,2\fermata r4 |
sib8. sib,16 sib,8. sib,16 sib,8. sib,16 |
sib,2 r8 sib, |
fa8. sol16 la8 sib do' sib |
la la, la, la, la, la, |
sib,2.:8 |
sib,:8 |
sib,2:8 re8 re |
do2.:8 |
do2 do8. do16 |
fa,4 fa,8 do' la fa |
fa2.\fermata |
sib,8\p sib, sib, sib, sib, sib, |
sib,2.:8 |
sib,8( re fa fa,) sib,[ sib,] |
mib\ff mib mib mib mib mib |
mib2 mib4\p |
fa fa fa, |
sib,8(\fp do re mib fa16 sol mib mib) |
fa2:8 fa,8 fa, |
sib,4 sib, sib, |
sib,2.\fermata |
