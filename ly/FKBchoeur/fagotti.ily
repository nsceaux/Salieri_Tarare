\clef "bass" sib,4..-\sug\f sib,16 re8. fa16 |
sib2\fermata r4 |
R2. |
sib,2.-\sug\f |
R2.*2 |
\clef "tenor" r8 re'4-\sug\f fa'8 sib re' |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { mib'2. | re' | }
  { sol2. | fa | }
  { s2.-\sug\p }
>>
\clef "bass" r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sib4 sib la8 | sol2 }
  { sol4 sol fa8 | mi2 }
  { s4-\sug\f }
>> do'4 |
fa fa8\p do' la fa |
re'8 re'16.\f do'32 sib8 la sol8. sol16 |
fa8.\p sib,16 do4 do |
fa8.\f fa16 fa4 r |
fa2:8\p do8 do |
\clef "tenor" r4 r \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { do'4 | re'2. | re' | }
  { la4 | la2. | sib | }
  { s4 | s2.-\sug\fp }
>>
R2. |
r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { do'4 do' do'8 | reb'2. | do'4 }
  { la4 la la8 | sib2. | la4 }
  { s2-\sug\f s8-\sug\p | s2.-\sug\cresc | s4-\sug\f }
>> r4 r |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { fa'4( sol'2) | sol'2 fa'4 | }
  { sib2. | la | }
  { s2.-\sug\p }
>>
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { s8 sib s sib s la | sib2 }
  { s8 sol s sol s la | sib2 }
  { r8 s r s r s | }
>> r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { fad'8 |
    sol'4~ sol'8 fa' mi' re' |
    dod'2 re'4 |
    mi'2. |
    re'4 }
  { do'8 |
    sol2. |
    la |
    sib8 re'4 re'8 dod' dod' |
    re'4 }
  { s8 | s2.-\sug\fp | }
>> r4 r |
\clef "bass" sib2-\sug\f\fermata r4 |
sib8. sib,16 sib,8. sib,16 sib,8. sib,16 |
sib,2 r8 sib, |
fa8. sol16 la8 sib do' sib |
la la, la, la, la, la, |
sib,2.:8 |
sib,:8 |
sib,2:8 re8 re |
do2.:8 |
do2 do8. do16 |
fa,4 fa,8 do' la fa |
fa2.\fermata |
sib,8\p sib, sib, sib, sib, sib, |
sib,2.:8 |
sib,8( re fa fa,) sib,[ sib,] |
mib\ff mib mib mib mib mib |
mib2 mib4\p |
fa fa fa, |
sib,8(\fp do re mib fa16 sol mib mib) |
fa2:8 fa,8 fa, |
sib,4 sib, sib, |
sib,2.\fermata |
