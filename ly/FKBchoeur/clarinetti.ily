\clef "treble" R2.*31 |
sib'2-\sug\f\fermata \twoVoices#'(clarinetto1 clarinetto2 clarinetti) <<
  { re''8. mib''16 |
    fa''4~ fa''8. re''16 re''8 sol'' |
    fa''2. |
    la'8. sib'16 do''8 re'' mib''8 sol'' |
    fa''2~ fa''8 mib'' |
    re''8 re'4( fa'8 sib' re'') |
    sol'4~ sol'8 sib' mib'' sol'' |
    fa''4. re''8 sib'8. sib'16 |
    sol''2~ sol''8 fa'' |
    mi''2 }
  { sib'8. do''16 |
    re''4~ re''8. sib'16 sib'8 mib'' |
    re''2. |
    la'8. sib'16 do''8 re'' mib''8 re'' |
    do''2. |
    re''8 re'4( fa'8 sib' re'') |
    mib'4~ mib'8 sol'( sib' mib'') |
    re''4. sib'8 fa' sib' |
    sib'2~ sib'8 la' |
    sol'2 }
>> r4 |
R2. |
\twoVoices#'(clarinetto1 clarinetto2 clarinetti) <<
  { mib''!2.\fermata |
    re''4~ re''8 do'' re'' mib'' |
    fa''4~ fa''8 sib'' la'' sol'' |
    fa''4.( mib''8 re''4) |
    sol''2. |
    mib''2 }
  { do''2.\fermata |
    sib'4~ sib'8 la' sib' do'' |
    re''4~ re''8 sol'' fa'' mib'' |
    re''4.( do''8 sib'4) |
    sib'2. |
    sol'2 }
  { s2. | s2.\p }
>> r4 |
R2. |
<>-\sug\ff \twoVoices#'(clarinetto1 clarinetto2 clarinetti) <<
  { re''8( mib'' fa'' sol'' la''16 sib'' sol'' mib'') |
    re''2 do''4 |
    sib' sib'8. re''16 sib'8. re''16 |
    sib'2.\fermata | }
  { sib'2. |
    sib'2 la'4 |
    sib'4 re'8. fa'16 re'8. fa'16 |
    re'2.\fermata | }
>>
