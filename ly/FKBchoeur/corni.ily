\clef "treble" \transposition mib
\twoVoices#'(corno1 corno1 corni) <<
  { sol'2 }
  { sol' }
  { s-\sug\f }
>> r4 |
\twoVoices#'(corno1 corno1 corni) <<
  { sol''2\fermata }
  { sol''\fermata }
>> r4 |
R2. |
\twoVoices#'(corno1 corno1 corni) <<
  { re''2. | }
  { sol' | }
  { s-\sug\f }
>>
R2.*2 |
r8 \twoVoices#'(corno1 corno1 corni) <<
  { re''4 re'' re''8 | mi''2. | re'' | }
  { sol'4 sol' sol'8 | do''2. | sol' | }
  { s4\f s4. | s2.\p }
>>
r8 \twoVoices#'(corno1 corno1 corni) <<
  { sol''4 sol'' fad''8 | mi''4 }
  { mi''4 mi'' re''8 | mi''4 }
  { s4-\sug\f }
>> r4 r |
\tag #'corni <>^"unis" re''4 r r |
R2.*2 |
r8 r16 re''16-\sug\f re''2 |
R2.*3 |
r8 mi''4 mi'' mi''8 |
r4 sol''8. sol''16 sol''8. sol''16 |
re''2.-\sug\f~ |
re''-\sug\p -\sug\cresc |
re''4-\sug\f r r |
R2.*8 |
<>-\sug\f \twoVoices#'(corno1 corno1 corni) <<
  { sol''2\fermata }
  { sol'\fermata }
>> r4 |
\twoVoices#'(corno1 corno1 corni) <<
  { re''2 mi''4 | re''2. | }
  { sol'2 do''4 | sol'2. | }
>>
R2. |
r8 \twoVoices#'(corno1 corno1 corni) <<
  { re''4 re'' re''8 | re''2. | mi'' | re'' | }
  { re''4 re'' re''8 | sol'2. | do'' | sol' | }
>>
r8 \twoVoices#'(corno1 corno1 corni) <<
  { sol''4 sol'' fad''8 | mi''2 }
  { mi''4 mi'' re''8 | do''2 }
>> r4 |
R2. |
\twoVoices#'(corno1 corno1 corni) <<
  { re''2.\fermata | }
  { re''2.\fermata | }
>>
R2.*3 |
\twoVoices#'(corno1 corno1 corni) <<
  { sol''2. |
    mi''2 mi''4 |
    re''2. |
    sol'' |
    re'' |
    sol'4 re'' re'' |
    re''2.\fermata | }
  { mi''2. |
    do''2 do''4 |
    re''2. |
    sol' |
    re'' |
    sol'4 sol' sol' |
    sol'2.\fermata | }
  { s2.\ff | s2 s4\p | s2. | s\ff }
>>
