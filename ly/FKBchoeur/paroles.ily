\tag #'(vhaute-contre vbasse basse) {
  Roi, nous met -- tons la li -- ber -- té
  aux pieds de ta ver -- tu su -- prê -- me.
  Rè -- gne sur ce peu -- ple qui t’ai -- me,
  par les lois et par l’é -- qui -- té,
  par les lois et par l’é -- qui -- té.
}
\tag #'(vdessus basse) {
  Et vous, reine, é -- pou -- se sen -- si -- ble,
  qui con -- nû -- tes l’ad -- ver -- si -- té,
  du de -- voir sou -- vent in -- fle -- xi -- ble
  a -- dou -- cis -- sez l’aus -- té -- ri -- té.
}
\tag #'(vdessus vhaute-contre vbasse basse) {
  Te -- nez son grand cœur ac -- ces -- si -- ble
  aux sou -- pirs de l’hu -- ma -- ni -- té.
  Te -- nez son grand cœur ac -- ces -- si -- ble
  aux sou -- pirs de l’hu -- ma -- ni -- té.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Roi, nous met -- tons la li -- ber -- té
  aux pieds de ta ver -- tu su -- prê -- me ;
  rè -- gne sur ce peu -- ple qui t’ai -- me,
  par les lois et par l’é -- qui -- té.
}
\tag #'(vdessus vhaute-contre vbasse basse) {
  Rè -- gne sur ce peu -- ple qui t’ai -- me,
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  rè -- gne par les lois et par l’é -- qui -- té,
  par les lois et par l’é -- qui -- té.
}
