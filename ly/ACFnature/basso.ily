\clef "bass" R2.*2 |
la,2.\p~ |
la, |
re4. r4 r8 |
sold2. |
la4. r4 r8 |
fad8( sold la) dod( re red) |
mi mi[\f mi] mi4.:8 |
mi4. mid\p |
fad2.\f( |
dod\p) |
re2.\pp\fermata~ |
re4.\fermata mi |
re8(\cresc re' dod') si4 la8 |
sold2.\f\cresc\fermata~ |
sold4\fermata\! r8 r4 r8 |
r4 r8 sold4.^\markup\italic "pizzicato" |
la4. fad |
dod8 la, la,\cresc la, la, la, |
la,2.:8 |
la,4. r4 r8 |
la4.\p^\markup\italic "pizzicato" r4 r8 |
mi4. r4 r8 |
la8^\markup\italic "con l’arco" la,[ si,] dod4 red8 |
la4. r4 r8 |
la,8\p la, la, la, la, la, |
la,2.:8\cresc |
la,4. r4 r8 |
la4.^\markup\italic "pizzicato" r4 r8 |
mi4. r4 r8 |
la4.\f r4 r8 |
<>^\markup\italic "con l’arco" fad8( sold la) dod( re red) |
mi2. |
r4 r8 sold4.( |
la) r4 r8 |
<< la,2.:8\p { s2 <>\cresc } >> |
la,2.:8\! |
la,4. r4 r8 |
la4.^\markup\italic "pizzicato" r4 r8 |
mi4. r4 r8 |
<>^\markup\italic "arco" la4 mi8 la4 mi8 |
la4
