Qu’un jeu -- ne cœur, ma -- lai -- sé -- ment,
voi -- le son trouble au doux mo -- ment
où l’a -- mour va s’en ren -- dre maî -- tre !
Moi- même, a -- près de longs hi -- vers,
quand vous ra -- ni -- mez l’u -- ni -- vers,
mes pre -- miers sou -- pirs font re -- naî -- tre
les fleurs __ qui par -- fu -- ment les airs.
airs,
les fleurs __ qui par -- fu -- ment les airs.
