\clef "soprano/treble" <>^\markup\italic "chanté" R2.*2 |
<>^\markup\italic { tirant le Génie à part, lui dit tendrement }
r8 r dod'' dod''4 si'8 |
la'4 la'8 la'[ si'] dod'' |
fad'4. re''8 si' sold' |
mi'4 sold'8 si'4 re''8 |
dod''4. la'8 dod'' mi'' |
mi''[ re''] dod'' dod''[ si'] la' |
sold'4 mi'8 r4 r8 |
r r sold' dod''4 dod''8 |
la'4.~ la'4 la'8 |
sold'4.~ sold'4 sold'8 |
fad'2.~ fad'4\fermata fad'8 fad' sold' lad' |
si'4( dod''8) re''4 mi''8 |
fad''2.\fermata~ |
fad''4\fermata fad''8 fad'' mi'' red'' |
mi''4( fad''8) si'[ dod''] re'' |
dod''[ si' dod''] re''4 re''8 |
mi''2.~ |
mi''~ |
mi''4. dod''8[ re''] mi'' |
la'2. |
si'4.~ si'4 mi''8 |
la'4. r4 r8 |
dod''4. r8 r la' |
mi''2.~ |
mi''~ |
mi''4. dod''8[ re''] mi'' |
la'2. |
si'4.~ si'4 mi''8 |
la'4. r4 r8 |
R2.*10 |
r4
