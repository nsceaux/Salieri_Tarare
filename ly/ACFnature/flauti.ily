\clef "treble" R2.*2 |
r8 r <>^"Solo" dod'''8 dod'''4 si''8 |
la''4 la''8 la''( si'' dod''') |
fad''4. re'''8( si'' sold'') |
mi''4( sold''8 si''4 re'''8) |
dod'''4. la''8( dod''' mi''') |
mi'''( re''' dod''') dod'''( si'' la'') |
sold''4. r4 r8 |
R2.*3 |
R2.^\fermataMarkup |
r8\fermata r fad'' fad''16(-\sug\cresc lad'' sold'' si'' lad'' dod''') |
si''4 dod'''8 re'''4 mi'''8 |
fad'''2.-\sug\f\fermata~ |
fad'''4\fermata r8 r4 r8 |
R2.*5 |
r16 mi'' fad'' sold'' la'' si'' dod'''4.:8 |
r16 sold'' la'' si'' dod''' re''' mi'''4.:8 |
la''4. r4 r8 |
la''4. r4 r8 |
R2.*3 |
r16 mi'' fad'' sold'' la'' si'' dod'''8 dod''' dod''' |
r16 sold'' la'' si'' dod''' re''' mi'''8 mi''' mi''' |
la''4.-\sug\f la''8( dod''' mi''') |
mi'''( re''' dod''') dod'''( si'' la'') |
sold''( mi' sold') si'( mi'' sold'') |
si''( la'' sold'' fad'' mi'' re'') |
dod''4. r4 r8 |
<< mi'''2.\p~ { s2 <>\cresc } >> |
mi'''2. |
mi'''4. dod'''8(\p re''' mi''') |
r16 mi'' fad'' sold'' la'' si'' dod'''4.:8 |
r16 sold'' la'' si'' dod''' re''' mi'''4.:8 |
la''4 \grace dod'''8 si'' la''4 \grace dod'''8 si'' |
la''4
