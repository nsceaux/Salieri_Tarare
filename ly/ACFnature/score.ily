\score {
   <<
    \new StaffGroup <<
      \new Staff \with { \flautoInstr } << \global \includeNotes "flauti" >>
      \new Staff \with { \oboiInstr } << \global \includeNotes "oboi" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors in A }
        shortInstrumentName = "Cor."
      } << \keepWithTag #'() \global \includeNotes "corni" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with { \natureInstr } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \keepWithTag #'basso \includeNotes "basso"
      \origLayout {
        s2.*5\break s2.*6\pageBreak
        s2.*7\break \grace s8 s2.*7\pageBreak
        s2.*7\break s2.*8\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
