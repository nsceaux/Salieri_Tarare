\clef "treble" R2.*12 |
R2.^\fermataMarkup |
R2. |
r8 <<
  { si'8 dod'' re''4 mi''8 | fad''2.\fermata~ | fad''4\fermata } \\
  { si'8-\sug\cresc dod'' re''4 mi''8 | re''2.\f~ | re''4 }
>> r8 r4 r8 |
R2.*2 |
<<
  { la''2.~ | la''~ | la''4. } \\
  { dod''2.-\sug\p~ | dod''~ | dod''4. }
>> r4 r8 |
R2.*4 |
<<
  { la''2.~ | la''~ | la''4. } \\
  { << { s2 <>\cresc } dod''2.\p~ >> | dod''~ | dod''4.\! }
>> r4 r8 |
R2.*2 |
r4 r8 <>^"[a 2]" la'8-\sug\f dod'' mi'' |
mi''( re'' dod'') dod''( si' la') |
sold'( mi' sold') si'( mi'' sold'') |
si''( la'' sold'' fad'' mi'' re'') |
dod''4. r4 r8 |
<>\p <<
  { s2 <>\cresc }
  << { la''2.~ | la'' | la''4. } \\ {  dod''2.~ | dod'' | dod''4. } >>
>>
dod''8(\p re'' mi'') |
la'2. |
si'4.~ si'4 mi''8 |
<<
  { la'4 \grace dod''8 si' la'4 \grace dod''8 si' | la'4 } \\
  { la'4 sold'8 la'4 sold'8 | la'4 }
>>
