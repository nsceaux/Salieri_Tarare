\clef "treble"
r8 r la8\p la4( <la dod'>8) |
q4 <dod' mi'>8 q( <re' fad'> <si sold'>) |
<dod' la'>4 <mi' dod''>8 q4( <re' si'>8) |
<dod' la'>4 la'8 la'( si' dod'') |
<re' fad'>4. re''8( si' sold') |
mi'4( sold'8 si'4 re''8) |
dod''4. la'8( dod'' mi'') |
mi''( re'' dod'') dod''( si' la') |
sold' mi'\f mi' mi' mi' mi' |
mi'4 sold'8\p dod''4 dod''8 |
la'2.\f( |
sold')\p |
fad'2.\pp\fermata~ |
fad'4\fermata fad'8 fad'16(\cresc lad' sold' si' lad' dod'') |
si'8( si' dod'' re''4 mi''8) |
<re'' fad''>2.\f\fermata\cresc~ |
q4\fermata fad''8 \grace sold''8 fad''(\p mi'' red'') |
mi''4 fad''8 si'( dod'' re'') |
\grace re''8 dod''( si' dod'') \grace mi'' re''( dod'' re'') |
mi''( mi''' dod'''\cresc la'' dod''' la'') |
mi''( dod''' la'') mi''( dod'' la') |
mi'4. r4 r8 |
r16\p mi'' fad'' sold'' la'' si'' dod'''4.:8 |
r16 sold'' la'' si'' dod''' re''' mi'''4.:8 |
la''8 la([ si]) dod'4 red'8 |
la''8 la([\f dod'] mi' la' dod'') |
mi''(\p mi''' dod''' la'' dod''' la'') |
mi''(\cresc dod''' la'' mi'' dod'' la') |
mi'4. r4 r8 |
r16\p mi'' fad'' sold'' la'' si'' dod'''8 dod''' dod''' |
r16 sold'' la'' si'' dod''' re''' mi'''8 mi''' mi''' |
la''4.\f la'8( dod'' mi'') |
mi''( re'' dod'') dod''( si' la') |
sold'( mi' sold') si'( mi'' sold'') |
si''( la'' sold'' fad'' mi'' re'') |
dod'' s4 mi'8 la' dod'' |
mi'' mi'''8(\p dod''' la'' dod'''\cresc la'') |
mi''( dod''' la'' mi'' dod'' la') |
mi'4. dod''8\p( re'' mi'') |
r16 mi'' fad'' sold'' la'' si'' dod'''4 \grace { si''16[ la'' sold''] } la''8 |
si''16 sold'' la'' si'' dod''' re''' mi'''4.:8 |
la''4 \grace dod'''8 si'' la''4 \grace dod'''8 si'' |
la''4
