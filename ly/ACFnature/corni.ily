\clef "treble" \transposition la
R2.*8 |
r8 <<
  { sol'8 sol' sol' sol' sol' | sol'4. } \\
  { sol'8\f sol' sol' sol' sol' | sol'4. }
>> r4 r8 |
<<
  { mi'2.~ | mi' | \once\tieDashed do'2.\fermata~ | do'4\fermata } \\
  { mi'2.-\sug\f~ | mi'2.\p | \once\tieDashed do'2.\pp~ | do'4 }
>> r8 r4 r8 |
R2. |
R2.^\fermataMarkup |
r4\fermata r8 r4 r8 |
R2.*2 |
<<
  { mi''2.~ | mi''~ | mi''4. } \\
  { sol'2.\p~ | sol'2.~ | sol'4. }
>> r4 r8 |
<<
  { do''2. | re'' | do''4. } \\
  { mi'2.-\sug\p | sol' | mi'4. }
>> r4 r8 |
<< do''4. \\ mi'4. >> r4 r8 |
<<
  { mi''2.~ | mi''2.~ | mi''4. } \\
  { << { s2 <>\cresc } sol'2.\p~ >> | sol'~ | sol'4.\! }
>> r4 r8 |
<<
  { do''2. | re'' | do''4. } \\
  { mi'2.\p | sol' | mi'4.-\sug\f }
>> r4 r8 |
R2. |
<<
  { sol'2. | sol' | mi'4. } \\
  { sol'2. | sol' | mi'4. }
>> r4 r8 |
<<
  { mi''2.~ | mi''2. | mi''4. } \\
  { << { s2 <>\cresc } sol'2.\p~ >> | sol' | sol'4.\! }
>> r4 r8 |
<<
  { do''2. | re''2. | do''4 re''8 do''4 re''8 | do''4 } \\
  { mi'2.\p | sol'2. | mi'4 sol'8 mi'4 sol'8 | mi'4 }
>>
