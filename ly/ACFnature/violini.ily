\clef "treble"
r8 r <<
  \tag #'violino1 {
    la8\p la4( dod'8) |
    dod'4 mi'8 mi'( fad' sold') |
    la'4 dod''8 dod''4( si'8) |
    la'4 la'8 la'( si' dod'') |
    fad'4. re''8( si' sold') |
    mi'4( sold'8 si'4 re''8) |
    dod''4. la'8( dod'' mi'') |
    mi''( re'' dod'') dod''( si' la') |
    sold' mi'\f mi' mi' mi' mi' |
    mi'4 sold'8\p dod''4 dod''8 |
    la'2.\f( |
    sold')\p |
    fad'2.\pp\fermata~ |
    fad'4\fermata
  }
  \tag #'violino2 {
    la8\p la4 la8 |
    la4 dod'8 dod'( re' si) |
    dod'4 mi'8 mi'4( re'8) |
    dod'2. |
    re'4. r4 r8 |
    mi'2. |
    mi'4. r4 r8 |
    la'8( si' mi') mi'( fad' fad') |
    si mi'-\sug\f[ mi'] mi' mi' mi' |
    mi'4. sold'-\sug\p |
    dod'2.-\sug\f( |
    si-\sug\p) |
    la-\sug\pp\fermata~ |
    la4\fermata
  }
>> fad'8 fad'16(\cresc lad' sold' si' lad' dod'') |
si'8( si' dod'' re''4 mi''8) |
<re'' fad''>2.\f\fermata\cresc~ |
q4\fermata <<
  \tag #'violino1 {
    fad''8 \grace sold''8 fad''(\p mi'' red'') |
    mi''4 fad''8 si'( dod'' re'') |
    \grace re''8 dod''( si' dod'') \grace mi'' re''( dod'' re'') |
    mi''( mi''' dod'''\cresc la'' dod''' la'') |
    mi''( dod''' la'') mi''( dod'' la') |
    mi'4. r4 r8 |
    mi'4.\p r4 r8 |
    sold'4. r4 r8 |
    la' la([ si]) dod'4 red'8 |
    la'8 la([\f dod'] mi' la' dod'') |
    mi''(\p mi''' dod''' la'' dod''' la'') |
    mi''(\cresc dod''' la'' mi'' dod'' la') |
    mi'4. r4 r8 |
    mi'4.\p r4 r8 |
    sold'4. r4 r8 |
    <dod' mi' la'>4.\f
  }
  \tag #'violino2 {
    r8 r4 r8 |
    r4 r8 mi'4.-\sug\p |
    mi' la' |
    la'8( mi'' dod''-\sug\cresc la' dod'' la') |
    mi'( dod'' la') mi'( dod' la) |
    dod'4. r4 r8 |
    dod'4.-\sug\p r4 r8 |
    si4. r4 r8 |
    la la([ si]) dod'4 red'8 |
    la8 la([\f dod'] mi' la' dod'') |
    mi''-\sug\p mi''( dod'') la'( dod'' la') |
    mi'(-\sug\cresc dod'' la' mi' dod' la) |
    dod'4. r4 r8 |
    dod'4.-\sug\p r4 r8 |
    si4. r4 r8 |
    <dod' mi' la'>4.\f
  }
>> la'8( dod'' mi'') |
mi''( re'' dod'') dod''( si' la') |
sold'( mi' sold') si'( mi'' sold'') |
si''( la'' sold'' fad'' mi'' re'') |
dod'' la([ dod'] mi' la' dod'') |
mi'' <<
  \tag #'violino1 {
    mi'''8(\p dod''' la'' dod'''\cresc la'') |
    mi''( dod''' la'' mi'' dod'' la') |
    mi'4. dod''8\p( re'' mi'') |
    la'4.~ la'4 \grace { si'16[ la' sold'] } la'8 |
    si'4.~ si'4 mi''8 |
    la'4 \grace dod''8 si' la'4 \grace dod''8 si' |
    la'4
  }
  \tag #'violino2 {
    mi''8(-\sug\p dod'') la'( dod''-\sug\cresc la') |
    mi'( dod'' la' mi' dod' la) |
    dod'4.\! r4 r8 |
    <dod' mi'>4.-\sug\p r4 r8 |
    <si mi'>4. r4 r8 |
    <<
      { mi'4 mi'8 mi'4 mi'8 | } \\
      { dod'4 re'8 dod'4 re'8 | }
    >>
    <dod' mi'>4
  }
>>
