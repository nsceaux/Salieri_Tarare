\tag #'all \key la \major \midiTempo#120
\tempo\markup { Andante con moto Poco Allegretto }
\time 6/8 s2.*24 \alternatives s2. s2. s2.*16
\time 4/4 s4 \bar "|."
