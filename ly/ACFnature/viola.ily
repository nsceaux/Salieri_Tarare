\clef "alto" R2.*2 |
la2.-\sug\p~ |
la |
re'4. r4 r8 |
si2. |
la4. r4 r8 |
\once\slurDashed fad8( sold la) dod'( re' red') |
mi'8 mi-\sug\f[ mi] mi mi mi |
mi4. mid-\sug\p |
fad2.-\sug\f |
dod-\sug\p |
re-\sug\pp\fermata~ |
re4.\fermata mi'-\sug\cresc |
fad'4( mi'8) re'( re'' dod'') |
si'2.-\sug\f\fermata~ |
si'4\fermata r8 r4 r8 |
r4 r8 sold4.-\sug\p |
la fad' |
dod'8 la la-\sug\cresc la la la |
la2.:8 |
la4. r4 r8 |
la4.-\sug\p r4 r8 |
mi4. r4 r8 |
la8 la([ si]) dod'4 red'8 |
la8 la([-\sug\f dod'] mi' la' dod'') |
la8\p la la la la la |
la2.:8\cresc |
la4. r4 r8 |
la'4.-\sug\p r4 r8 |
mi'4. r4 r8 |
la4.\f r4 r8 |
fad'8( sold' la') dod'( re' red') |
mi'2. |
r4 r8 sold'4.( |
la') r4 r8 |
<< { s2 <>\cresc } la2.:8\p >> |
la:8\! |
la4. r4 r8 |
la'4.-\sug\p r4 r8 |
mi'4. r4 r8 |
la'4 mi'8 la'4 mi'8 |
la'4
