\clef "bass" R2.*2 |
la,2.~ |
la, |
re4. r4 r8 |
<sold si>2. |
<la mi'>4. r4 r8 |
<fad la>8( <sold si> la) <dod mi>( <re fad> <red fad>) |
<mi si> mi[ mi] mi mi mi |
mi4. <mid sold> |
<fad dod'>2.( |
<dod si>) |
<re la>2.\fermata~ |
q4.\fermata mi |
<re fad>8( re' <dod' mi'>) si4 <la dod'>8 |
<sold si>2.\fermata~ |
q4\fermata r8 r4 r8 |
r4 r8 sold4. |
la4. fad |
<la, dod mi>2.:8 |
q2.:8 |
q4. r4 r8 |
<la dod' mi'>4. r4 r8 |
<mi si sold>4. r4 r8 |
la8 la,[ si,] dod4 red8 |
la4. r4 r8 |
<la, dod mi>2.:8 |
q:8 |
q4. r4 r8 |
<la dod' mi'>4. r4 r8 |
<mi sold si>4. r4 r8 |
<la dod' mi'>4. r4 r8 |
fad8( sold la) dod( re red) |
mi2. |
r4 r8 sold4.( |
la8) la[ dod'] r4 r8 |
<la, dod mi>2.:8 |
q:8 |
q4. r4 r8 |
<la dod' mi'>4. r4 r8 |
<mi si>4. r4 r8 |
<la dod'>4 <mi sold>8 <la dod'>4 <mi sold>8 |
<la dod' mi'>4
