\clef "bass" si,4 si, si,~ si,16 dod32 red mi fad sold lad |
si8. si16 si8. si16 si4 fad8. fad16 |
si,1 |
sol |
mid2. dod4 |
fad dod' la fad |
re32 la, si, dod re mi fad sol la sol fad mi re dod si, la, re2\fermata |
re1 |
sol2 la4 la, |
<re fad la>1~ |
q~ |
q~ |
q |
re4( la, fad, la,) |
<re fad la>1~ |
q~ |
q2 <sol si>~ |
q sold~ |
sold la~ |
la fa |
fa r4\fermata sol |
