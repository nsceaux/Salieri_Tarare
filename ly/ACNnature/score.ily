\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors en Ré }
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with {
          \consists "Metronome_mark_engraver"
        } << \global \keepWithTag #'violino1 \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with { \natureInstr } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr \consists "Metronome_mark_engraver" } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*3\pageBreak
        s1*3 s2 \bar "" \break s2 s1*4\pageBreak
        s1*5 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
