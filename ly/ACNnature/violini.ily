\clef "treble" si8\p si4 si si8~ si16\cresc dod'32 red' mi' fad' sold' lad' |
<>\fp <<
  \tag #'violino1 {
    si'4~ si'8. si'16 re''4 re'8. re'16 |
    <re'' fad''>32\fp q q q q q q q q4:32 q2:32 |
    <si'' si'>2:32 q:32 |
    <dod'' si''>2:32 q:32 |
    <dod'' la''!>16\f dod'''[-! la''-! fad''-!]
  }
  \tag #'violino2 {
    si'8. <re' fad'>16 q8. q16 q4 si8. si16 |
    re''32-\sug\fp re'' re'' re'' re'' re'' re'' re'' re''4:32 re''2:32 |
    re''2:32 re''2:32 |
    sold'':32 sold'':32 |
    fad''8-\sug\f la''16 fad''
  }
>> dod''16-!\f la''-! fad''-! dod''-! la'\f fad'' dod'' la' fad'\f dod'' la' fad' |
re'32(\ff la si dod' re' mi' fad' sol' la' sol' fad' mi' re' dod' si la) <<
  \tag #'violino1 { re'2\fermata | re'1 | }
  \tag #'violino2 { <fad' la>2\fermata | q1 | }
>>
<sol re' si'>4\f si'32 la' sol' fad' mi' re' dod'? si la4 <la' mi'' dod'''>4 |
re'''4~ re'''16( la'' fad'' la'' re''' la'' fad'' la'' fad'' re'' la'' fad'') |
re''( la' fad'' re'' la''\p fad'' re'' fad'' re'' la' fad'' re'' la' fad' re'' la') |
fad'( re' fad' la' re'' la' fad' la' fad' re' la' fad' re' la re' fad') |
re'8( la re' fad') re'( la re' fad') |
<<
  \tag #'violino1 {
    re'1~ |
    re'~ |
    re'~ |
    re'2 re'~ |
    re' mi''~ |
    mi''1~ |
    mi''2 re'' |
    si'\fermata
  }
  \tag #'violino2 {
    re'1 |
    la~ |
    la~ |
    la2 si~ |
    si si'~ |
    si' do''~ |
    do'' la' |
    sol'\fermata
  }
>> r4 <sol re' si'>\f |
