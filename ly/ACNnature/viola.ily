\clef "alto" si8-\sug\p si4 si si8-\sug\cresc~ si16 dod'32 red' mi' fad' sold' lad' |
si'8.-\sug\fp si16 si8. si16 si4 fad8. fad16 |
si'32\fp si' si' si' si' si' si' si' si'4:32 si'2:32 |
sol':32 sol':32 |
mid':32 mid':32 |
fad'16-\sug\f dod''-! la'-! fad'-! dod'-!-\sug\f la'-! fad'-! dod'-! la-\sug\f fad' dod' la fad-\sug\f dod' la fad |
re'32(-\sug\ff la si dod' re' mi' fad' sol' la' sol' fad' mi' re' dod' si la) re'2\fermata |
re'1 |
sol2-\sug\f la4 la |
re''~ re''16( la' fad' la' re'' la' fad' la' fad' re' la' fad') |
re'( la fad' re' la'-\sug\p fad' re' fad' re' la fad' re' la fad re' la) |
fad( re fad la re' la fad la fad re la fad re8 re16 fad) |
re1 |
re |
fad~ |
fad~ |
fad2 sol~ |
sol mi'~ |
mi'1~ |
mi'2 fa' |
re'\fermata r4 sol'\f |
