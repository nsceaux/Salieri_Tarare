\clef "treble" R1 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { si'2 re''4 re' |
    fad''1 |
    si''1 |
    si'' |
    la''4 dod'' la' fad' |
    re'2~ re'4\fermata }
  { si'2 re''4 re' |
    re''1 |
    re'' |
    sold'' |
    fad''4 dod'' la' fad' |
    re'2~ re'4\fermata }
  { s1 s-\sug\fp s1*2 s1-\sug\f <>-\sug\ff }
>> r4 |
R1 |
r2 r4 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4 | fad''1~ | fad''~ | fad''~ | fad''4 }
  { dod''4 | re''1~ | re''~ | re''~ | re''4 }
>> r4 r2 |
R1*7 |
r2 r4 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { si'4 }
  { re' }
>>
