\clef "bass" si,1\p |
si2. si,4 |
\clef "tenor" <>-\sug\fp \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fad'1 | } { re'1 | }
>>
R1 |
mid'2. dod'4 |
fad'?-\sug\f dod' la fad |
\clef "bass" re2-\sug\ff~ re4\fermata r |
R1 |
r2 r4 la-\sug\f |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la1~ |
    la~ |
    la~ |
    la~ |
    la~ |
    la~ |
    la~ |
    la2
  }
  { fad1~ |
    fad~ |
    fad~ |
    fad~ |
    fad~ |
    fad~ |
    fad~ |
    fad2
  }
>> sol2~ |
sol sold~ |
sold la~ |
la fa~ |
fa r4\fermata sol-\sug\f |
