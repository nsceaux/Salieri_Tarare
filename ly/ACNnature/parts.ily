\piecePartSpecs
#`((oboi #:tag-notes oboi #:score-template "score-part-voix")
   (corni #:tag-notes corni #:tag-global ()
           #:instrument "Cors en Ré"
           #:score-template "score-part-voix")
   (fagotti #:tag-notes fagotti
            #:score-template "score-part-voix")
   (violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (reduction)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet#})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#}))
