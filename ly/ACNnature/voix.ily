\clef "soprano/treble" <>^\markup\italic aux ombres très fièrement
R1 |
<>^\markup\italic tutto a rigore
si'8. si'16 si'8. si'16 re''4 re''8 si' |
fad''4 fad'' r re''8 re'' |
re''4. re''8 re''4 re''8 re'' |
dod'' dod'' r4 dod'' dod'8 mid'' |
fad''4 dod'' la' fad' |
re'2~ re'8\fermata r16 <>^\markup\italic senza rigor la' la'8. la'16 |
re''4. re''8 re''8. re''16 fad''8. re''16 |
si'2 r4 mi''8 mi''16 mi'' |
fad''4 r r2 |
R1 |
<>^\markup\italic { Les Ombres s’enfoncent sous terre et disparaissent }
R1*3 |
r4 r8^\markup\right-align\italic parlé <>^\markup\italic au Génie la'8 re''4 re''8 re'' |
re''4 re''8 mi'' do'' do'' r do'' |
la' la'16 si' do''8 do''16 re'' sol'4 r8 si' |
si' si' si' si' mi''4. si'16 do'' |
re''4 re''8 mi'' do''4 r8 do''16 do'' |
do''4 si'8 la' re''4 r8 re''16 fa'' |
si'4 si'8 do'' do''4\fermata sol'8 r |
