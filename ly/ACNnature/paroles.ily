Tels qu’u -- ne va -- peur é -- lan -- cé -- e,
par le froid en eau con -- den -- sé -- e,
tombe et se perd dans l’o -- cé -- an ; __
fu -- turs mor -- tels, ren -- trez dans le né -- ant,
dis -- pa -- rais -- sez.

Et nous, dont l’es -- sen -- ce pro -- fon -- de
dé -- vo -- re l’es -- pace et le temps ;
lais -- sons en un clin d’œil é -- cou -- ler qua -- rante ans ;
et voy -- ons- les a -- gir sur la scè -- ne du mon -- de.
