\clef "treble" si8\p si4 si si8~ si16\cresc dod'32 red' mi' fad' sold' lad' |
<>\fp si'8. <re' fad' si'>16 q8. q16 <re' fad' re''>4 <si re'>8. q16 |
<si' re'' fad''>1:32\fp |
<si'' re'' si'>1:32 |
<dod'' sold'' si''>1:32 |
<dod'' la''!>16\f dod'''[-! la''-! fad''-!]
dod''16-!\f la''-! fad''-! dod''-! la'\f fad'' dod'' la' fad'\f dod'' la' fad' |
re'32(\ff la si dod' re' mi' fad' sol' la' sol' fad' mi' re' dod' si la) <la fad' re'>2\fermata |
<fad' la re'>1 |
<sol' re' si'>4\f si'32 la' sol' fad' mi' re' dod'? si la4 <la' mi'' dod''>4 |
re'''4 re'''16( la'' fad'' la'' re''' la'' fad'' la'' fad'' re'' la'' fad'') |
re'' la' fad'' re'' la''(\p fad'' re'' fad'' re'' la' fad'' re'' la' fad' re'' la') |
fad'( re' fad' la' re'' la' fad' la' fad' re' la' fad' re' la re' fad') |
re'8( la re' fad') re'( la re' fad') |
re'1~ |
re'~ |
re'~ |
re'2 re'~ |
re' <si' mi''>~ |
q2 <do'' mi''>~ |
q2 <fa' la' re''> |
<sol' si' re'>\fermata r4 <sol' re' si'>\f |
