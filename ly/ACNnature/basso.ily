\clef "bass" si,4\p si,\cresc si,~ si,16 dod32 red mi fad sold lad |
si1 |
si,\fp |
sol |
mid2. dod4 |
fad\f dod' la fad |
<< {re32 la, si, dod re mi fad sol la sol fad mi re dod si, la, } \\ re2-\sug\ff >> re\fermata |
re1 |
sol2-\sug\f la4 la,\f |
re1~ |
re~ |
re~ |
re |
re4( la, fad, la,) |
re1~ |
re~ |
re2 sol~ |
sol sold~ |
sold la~ |
la fa~ |
fa r4\fermata sol-\sug\f |
