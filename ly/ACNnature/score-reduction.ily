\score {
  <<
    \new Staff \withLyrics <<
      <>_\markup\character La Nature
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new PianoStaff <<
      \new Staff = "dessus" << \global \includeNotes "reduction-dessus" >>
      \new Staff = "basse" << \global \includeNotes "reduction-basse" >>
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
