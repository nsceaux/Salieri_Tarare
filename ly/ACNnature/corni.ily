\clef "treble" \transposition re
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi'2. do'4 | mi''1 | }
  { do'2. do'4 | do''1 | }
  { s1 | s-\sug\fp }
>>
R1*3 |
<>-\sug\ff \twoVoices #'(corno1 corno2 corni) <<
  { do'2~ do'4\fermata }
  { do'2~ do'4\fermata }
>> r4 |
R1 |
r2 r4 <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { re''4 | mi''1~ | mi''~ | mi''4 }
  { sol'4 | sol'1~ | sol'~ | sol'4 }
>> r4 r2 |
R1*9 |
