\clef "bass" do'8\ff sol16 mi do4 r r8 do' |
re fa16 mi re do si, la, sol,4. sol8 |
do' sol16 la sib8 sib la8. sol16 fa8 fa |
mi8. re16 do8 si, la,8. la16 la8. la16 |
sol4. sol,8 sol,4 sol, |
sol,1\p~ |
sol,~ |
sol, |
r4 mi r2 |
r4 r8. do16 do8. re16 re8. re16 |
mi4 r r2 |
R1 |
r2 r8. mib16 mib4 |
R1 |
r2 r4 fa |
