\clef "bass" R1*5
\tag #'recit <>_"Parlé" ^\markup\character Urson
r4 r8 re sol4 r8 sol16 sol |
sol4 sol8 la si si r re' |
si4. si8 si si si do' |
sol4 r
\ffclef "bass" <>^\markup\character-text Atar à sa suite
r8 sol sol sol |
do'4 r r2 |
r4 r8 do' do'4. sol8 |
mi4 r16 do' do' re' sib4 r8 sol |
sol sol16 sol sol8 la16 sib la8 la r la |
do' do' re' mib' la4 r8 fa |
la la la sib fa4 r |
