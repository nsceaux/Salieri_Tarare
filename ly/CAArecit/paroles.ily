Sei -- gneur, le grand- prêtre Ar -- thé -- né -- e
de -- mande un en -- tre -- tien se -- cret.

E -- loi -- gnez- vous… Qu’il vienne. Ur -- son, que nul su -- jet,
dans cette a -- gré -- a -- ble jour -- né -- e,
d’un seul re -- fus d’A -- tar n’em -- por -- te le re -- gret.
