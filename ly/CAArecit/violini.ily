\clef "treble"
<<
  \tag #'violino1 {
    <sol mi' do''>4.\ff \grace re''16 do''32 si' do'' re'' mi''8-! do''-! sol'-! mi'-! |
    <fa' la>4. \grace do''16 si'32 la' si' do'' re''8-! si'-! sol'-! re'-! |
    <mi' sol>4. mi'8 fa' \grace sol'16 fa'32 mi' fa' sol' la'8 si' |
    do'' \grace re''16 do''32 si' do'' re'' mi''8 re'' do''8. fad''16 fad''8.\trill mi''32 fad'' |
    sol''4.
  }
  \tag #'violino2 {
    <sol mi'>16-\sug\ff q q q q4:16 q2:16 |
    re'16 <la fa'> q q q4:16 <si re'>2:16 |
    do'16 <sol mi'> q q q do' do' do' do'4:16 do'16 do' re' re' |
    sol sol' sol' sol' sol'4:16 mi'16 do'' do'' do'' do''4:16 |
    si'4.
  }
>> <re' si>8 q4 q |
q1\p~ |
q~ |
q |
r4 <mi' do'' sol''>4 r2 |
r4 r8. sol''16 sol''8. si'16 si'8.\trill la'32 si' |
do''4 r r2 |
R1 |
r2 r8. <do'' la''>16 q4 |
R1 |
r2 r4 <<  
  \tag #'violino1 { <fa' do'' la''>4 }
  \tag #'violino2 { <fa' do''>4 }
>>
