\clef "alto" do'16-\sug\ff do' do' do' do'4:16 do'2:16 |
re'8 fa'16 mi' re' do' si la sol2:16 |
do':16 do'4:16 do'16 do' re' re' |
sol sol' sol' sol' sol'4:16 mi'16 do' do' do' do'4:16 |
re'4. sol8 sol4 sol |
sol1-\sug\p~ |
sol~ |
sol |
r4 <sol mi' do''>4 r2 |
r4 r8. mi'16 mi'8. fa'16 fa'8. re''16 |
sol4 r r2 |
R1 |
r2 r8. mib'16 mib'4 |
R1 |
r2 r4 \sugNotes fa' |
