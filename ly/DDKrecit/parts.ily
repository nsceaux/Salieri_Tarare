\piecePartSpecs
#`((violino1 #:score-template "score-part-voix" #:system-count 1)
   (violino2 #:score-template "score-part-voix" #:system-count 1)
   (viola #:score-template "score-part-voix" #:system-count 1)
   (basso #:score-template "score-part-voix" #:system-count 1)
   (silence #:system-count 1)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #}))
