\clef "treble" R1 |
r8 r16 <<
  \tag #'violino1 { <dod'' la''>16 q8. q16 q4 }
  \tag #'violino2 <<
    { mi''16 mi''8. mi''16 mi''4 } \\
    { dod''16 dod''8. dod''16 dod''4 }
  >>
>> r4 |
<<
  \tag #'violino1 { <la' dod'''>2 }
  \tag #'violino2 << mi''2 \\ dod'' >>
>> r4 <la' mi'' dod'''>4 |

