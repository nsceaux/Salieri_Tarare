\clef "alto" R1*2 |
r8 fa'( mi' re') dod'2~ |
dod'1~ |
dod' |
re'4 r r2 |
R1 |
re'2 do'4 r |
R1 |
red'1~ |
red'2 mi'4 r |
