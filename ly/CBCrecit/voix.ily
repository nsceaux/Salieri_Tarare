\clef "bass" <>^\markup\italic étonné _\markup\right-align parlé
r8 sol sol sol do'8. do'16 do'8 re' |
sib8 sib r sib16 sib sol8 sol16 la sib8 do' |
la2 r4 r8 mi |
la4 la8 la dod'4 si!8 dod' |
la4 la8 la la mi16 fa sol8 la |
fa8 fa r la16 la la4 la8 la |
re'8 la16 la fa8 sol16 la re4 r8 la16 si |
sold4 sold16 la si do' la8 la r16 mi mi mi |
la4. la8 la la si do' |
si si r16 si si si fad4 r8 fad |
la la si fad sol4 r |
