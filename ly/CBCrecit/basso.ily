\clef "bass" R1*2 |
r8 fa( mi re) dod2~ |
dod1~ |
dod |
re4 r r2 |
R1 |
re2 do4 r |
R1 |
red1~ |
red2 mi4 r |
