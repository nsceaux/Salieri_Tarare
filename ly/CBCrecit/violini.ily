\clef "treble" R1*2 |
r8 fa'( mi' re') dod'2~ |
dod'1~ |
dod' |
re'4 r r2 |
R1 |
<<
  \tag #'violino1 { sold'2 la'4 }
  \tag #'violino2 { si2 mi'4 }
>> r4 |
R1 |
<<
  \tag #'violino1 { si'1~ | si'2 si'4 }
  \tag #'violino2 { fad'1~ | fad'2 sol'4 }
>> r4 |
