Ap -- prends- moi donc, ô chef des Bra -- mes !
ce qu’A -- tar doit pen -- ser de toi.
Ar -- dent zé -- la -- teur de la foi
du pas -- sage é -- ter -- nel des â -- mes !
Le plus vil a -- ni -- mal est nour -- ri de ta main ;
tu crain -- drais d’en pur -- ger la ter -- re !
Et ce -- pen -- dant, tu brû -- les, dans la guer -- re,
de voir cou -- ler des flots de sang hu -- main !
