\score {
  \new StaffGroup <<
    \new Staff \with { \violoncelliInstr \haraKiriFirst } <<
      \global \keepWithTag #'violoncelli \includeNotes "basso"
    >>
    \new Staff << \global \keepWithTag #'basso \includeNotes "basso" >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 5\mm
  }
}
