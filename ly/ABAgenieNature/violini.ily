\clef "treble" si8(\p sol si re' sol' si' re'' si') |
sol''( re'' si' sol') re'( sol' re' si) |
sol( si re' sol') si'( re'' sol'' re'') |
si''( sol'' re'' si') sol'( re' si la) |
sold( si mi' sold') si'( mi'' sold'' si'') |
re'''( si'' sold'' mi'') re''( si' sold' re') |
do'( la do' mi') la'( do'' mi'' la'') |
do'''( la'' mi'' do'') la'( si' do'' re'') |
mi''4 r r2 |
<<
  \tag #'violino1 {
    \grace { mi''16[ re'' do''] } re''4 r \grace { re''16[ do'' si'] } do''4 r |
    \grace { do''16[ si' la'] } si'4 r \grace { si'16[ la' sold'] } la'4 r |
    \grace { do''16[ si' la'] } si'4 r r2 |
    R1 | \allowPageTurn
    \grace { sol''16[ fa'' mi''] } fa''4\trill r mi''\trill r |
    re''\trill r do''\trill r |
    re''\trill r r2 |
    fad''4\trill r r2 |
    sol''4\trill r r2 |
    fad''4\trill r r2 |
    r8 re'( si' sol' do'' sol' mi'' do'') |
    si'( sol'' re'' si' do'' sol' mi' do') |
    si2.\fermata r8
  }
  \tag #'violino2 {
    \grace { do''16[ si' la'] } si'4 r \grace { si'16[ la' sold'] } la'4 r |
    \grace { la'16[ sold' fad'] } sold'4 r \grace { si'16[ la' sold'] } la'4 r |
    \grace { la'16[ sold' fad'?] } sold'4 r r2 |
    R1 | \allowPageTurn
    re''4\trill r do''\trill r |
    si'\trill r do''\trill r |
    si'\trill r r2 |
    la'4\trill r r2 |
    si'4\trill r r2 |
    la'4\trill r r2 |
    re'2( mi') |
    re'( mi') |
    re'2.\fermata r8
  }
>> <sol mi'>8\f |
<sol mi'>1 |
q\p |
<<
  \tag #'violino1 {
    sold'1 |
    si' |
    la' |
    la' |
    sol'~ |
    sol' |
    la'2( si'4 do'') |
    do''2 si' |
    r8 sol''\fz( mi'' do'') \once\slurDashed sol'( do'' mi' sol') |
    do'1\p |
    R1 |
    r8 fad''(\mf la'' fad'') re''( la' fad' la') |
    re'1\p |
    R1 |
    r8 si''\f-! re'''-! si''-! sol'' sol'4\p sol'8 |
    sol'2 la'8 sol' fad'4 |
    <si sol'>1\f |
    sol'4 <sol sol'>2\f \grace sol'8 fa'! mib'16 fa' |
    \grace { sol16[ mib'] } do''1\fp~ |
    do''~ |
    do'' |
    sib'2. sib'8. re''16 |
    do''4 do''8. do''16 do''2 |
    re''4 re''8. re''16 re''2 |
    mib''4 mib''8. mib''16 mib''4 mib''8. re''16 |
    do''4 do''8. do''16 do''8 lab'-! do''-! re''-! |
    mib''8. sib'16 sib'8.\trill la'32 sib' do''8. do''16 re''8. re''16 |
    mib''8. sib'16 sol''8. mib''16 sib''8 sol''-! mib''-! sib'-! |
    do''8. do''16 mib''8. do''16 sib'8-! sol'-! fa'-! sib'-! |
    sol'\f
  }
  \tag #'violino2 {
    <mi' re'>1~ |
    q |
    <mi' dod'> |
    re'' |
    si' |
    do'' |
    fa'2.( mi'4) |
    re'1 |
    <sol mi'>1\fz~ |
    q\p~ |
    q |
    <re' la'>-\sug\mf~ |
    q-\sug\p~ |
    q
    <re' si'>2\f~ q8 si'([\p do'' re'']) |
    mi''4. re''8 do''( si' la' re'') |
    <si' re'>1-\sug\f |
    q4 <sol' sol>2-\sug\f \grace sol'8 fa'! mib'16 fa' |
    sol'1-\sug\fp~ |
    sol'~ |
    sol' |
    fa'2. sib'4 |
    sib' sib'8. sib'16 sib'2 |
    sib'4 sib'8. sib'16 sib'2 |
    sib'4 sib'8. sib'16 sib'4 sib'8. sib'16 |
    lab'4 lab'8. lab'16 lab'8 mib' lab' fa' |
    mib' sib' sol' mib' mib' lab' \grace sib'4 lab'8 sol'16 lab' |
    sol'8. sol'16 sib'8. sib'16 sib'8 sib' sib' sib' |
    lab'4 do''8. lab'16 sol'8 mib' re' re' |
    mib'\f
  }
>> mib'' sol'' mib'' sib' mib'' sol' sib' |
<mib' sol>4 r r2 |
R1 |
<<
  \tag #'violino1 {
    la'!4 r r2 |
    R1 |
    sib'4 r r2 |
    re''4 r r2 |
    re''4 r r2 |
    dod''4 r r2 |
    re''2. re''8.\p do''16 |
    sib'16( fa' sib' fa' sib' fa' sib' fa' \rt#4 { sib' fa') } |
    \rt#4 { la' mi' } \rt#4 { la' mi' } |
    \rt#4 { fa' la' } fa' la' fa' la' fa' re'' fa' re'' |
    \rt#4 { dod'' mi' } fa' re'' fa' re'' fa'' re'' fa'' re'' |
    mi'' dod'' la' dod'' mi'' dod'' la' dod'' mi'' dod'' la' dod'' mi'' la' mi'' la' |
    fa'' re'' la' re'' fa'' la' fa'' la' fa'' la' fa'' la' fa'' re'' fa'' re'' |
    r re''\f sol'' si'' re''' si'' sol'' si'' re''4 r |
    r16 re'\p sol' si' sol' re' si re' sol4 r |
    r8 re''16 si' re'' si' re'' si' \rt#4 { re'' si' } |
    \rt#4 { re'' si' } \rt#4 { re'' si' } |
    \rt#4 { re'' si' } \rt#4 { re'' si' } |
    \rt#4 { re'' si' } \rt#4 { re'' si' } |
    \rt#4 { re'' si' } \rt#4 { re'' si' } |
    \rt#4 { do'' la' } \rt#4 { do'' la' } |
    \rt#4 { do'' la' } \rt#4 { re'' la' } |
    si'\f sold''( si'' sold'' si'' sold'' si'' sold'' \rt#4 { si'' sold'') } |
    si''\mf si'( mi'' si' mi'' si' mi'' si' \rt#4 { mi'' si') } |
    mi''\p si mi' si mi' si mi' si \rt#4 { mi' si } |
    sold1 |
    sold4
  }
  \tag #'violino2 {
    do'4 r r2 |
    R1 |
    fa'4 r r2 |
    <re' la'>4 r r2 |
    <re' sib'>4 r r2 |
    la'4 r r2 |
    la'2. la'4-\sug\p |
    fa'16( re' fa' re' fa' re' fa' re' \rt#4 { fa' re') } |
    \rt#4 { mi' dod' } \rt#4 { mi' dod' } |
    \rt#4 { re' la } \rt#4 { re' la } |
    \rt#4 { mi' dod' } re' fa' re' fa' la' fa' la' fa' |
    la' mi' dod' mi' la' mi' dod' mi' la' mi' dod' mi' dod'' mi' dod'' mi' |
    re'' la' fa' la' re'' fa' re'' fa' re'' fa' re'' fa' la' fa' la' fa' |
    <re' si'>1-\sug\f\>~ |
    q2-\sug\p~ si'8 si'16 re' si' re' si' re' |
    \rt#4 { si' re' } \rt#4 { si' re' } |
    \rt#4 { si' re' } \rt#4 { si' re' } |
    \rt#4 { si' re' } \rt#4 { si' re' } |
    \rt#4 { si' mi' } \rt#4 { si' mi' } |
    \rt#4 { si' mi' } \rt#4 { si' mi' } |
    \rt#4 { la' mi' } \rt#4 { la' mi' } |
    \rt#4 { la' mi' } \rt#4 { la' fa' } |
    sold'\f mi''( sold'' mi'' sold'' mi'' sold'' mi'' \rt#4 { sold'' mi'') } |
    sold''8\mf si'16( sold' si' sold' si' sold' \rt#4 { si' sold') } |
    si'16\p sold si sold si sold si sold \rt#4 { si sold } |
    \rt#4 { mi'\pp si } \rt#4 { mi' si } |
    mi'4
  }
>> r4 r2 |
R1*3 |
<<
  \tag #'violino1 {
    r4 \grace { fa''16[ mi'' re''] } mi''4\p \grace { re''16[ do'' si'] } do''4 r |
    r \grace { si''16[ la'' sol''] } la''4 \grace { sol''16[ fad'' mi''] } fad''4 r |
    r \grace { la''16[ sol'' fad''] } sol''4 \grace { mi''16[ re'' dod''] } re''4 r |
    R1 |
  }
  \tag #'violino2 {
    r4 \grace { re''16[ do'' si'] } do''4-\sug\p \grace { si'16[ la' sold'] } la'8. do''16[ si'8. do''16] |
    la'8. fad'16 la'8. la'16 re''8. re''16 do''8. re''16 |
    si'8. si'16 re''8. si'16 sol'8. re'16 sol'8. la'16 |
    si'8. sol'16 si'8. do''16 re''8. fa'!16 fa'8. sol'16 |
  }
>>
r16 do'\f re' mi' fa' sol' la' si' do''8 re''16 mi'' fa'' sol'' la'' si'' |
do'''4 r r2 |
R1 |
r16 fa' sol' la' sib' do'' re'' mi'' fa''4 r |
R1 |
r4 <fa' do'' la''> <fa' re'' sib''> r |
