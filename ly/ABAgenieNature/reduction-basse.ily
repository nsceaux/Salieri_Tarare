\clef "bass" <sol, si, re>1~ |
q |
<fa, si, re>~ |
q |
<mi, sold, si,>~ |
q |
<la, do mi> |
<fa la do'>2. <do' fa la>4 |
<mi sold si>4 r r2 |
r8 mi( fad sold) la( si do' la) |
re'( mi' re' si) do'( mi' do' la) |
mi2. re8 do |
si,4 sol, r2 |
r8 sol( la si \change Staff = "dessus" \voiceTwo do' re' mi' do') |
fa'( sol' fa' re') mi'( sol' mi' do') |
\change Staff = "basse" \oneVoice sol4 <sol si re'>2 q4 |
<re fad la do'>2. <fad la>4 |
<sol si>1 |
<re fad la do'>2. <fad la>4 |
<sol, si, re>4 r <sol, do mi> r |
<sol, si, re>4 r <sol, do mi> r |
<sol, si, re>2.\fermata r8 do |
do1~ |
do |
<si, re>1 |
<sold, re> |
<sol,! dod> |
<fad, re> |
<fa,! re> |
<mi, do>2 <do, mi,>4 <do mi> |
fa2( re4 <do sol>) |
sol2 sol, |
<do mi sol>1~ |
q~ |
q |
<fad la re'>~ |
<fad la>~ |
q |
<sol si re'>2~ q8 sol,([ la, si,]) |
do4. si,8 la,4 re |
sol,8 si-![ re'-! si-!] sol-! re-! si,-! re-! |
sol,4 sol2 \grace sol8 fa! mib16 fa |
<mib sol>1~ |
q~ |
q |
<re fa>2. <re fa sib>4 |
mib2~ mib8. mib16 mib8. mib16 |
fa2~ fa8. fa16 fa8. fa16 |
mib2( sol) |
lab2 lab,8 lab, lab, lab, |
sol,8. sol16 sol8. sol16 <lab mib'>8. q16 <fa re'>8. q16 |
<mib sol sib>8. q16 q8. q16 q8 q8 <sol sib mib'> q |
<lab mib'>4 <lab do'> sib sib, |
mib8 mib' sol' mib' sib mib' sol sib |
<mib sol>4 r r2 |
R1 |
<mib do'>4 r r2 |
R1 |
<re sib>4 r r2 |
<fad la>4 r r2 |
<sol re'>4 r r2 |
<sol mi'>4 r r2 |
fa4 r r fa |
re16( fa re fa re fa re fa \rt#4 { re fa) } |
\rt#4 { dod mi } \rt#4 { dod mi } |
\rt#4 { re fa } \rt#4 { re fa } |
\rt#4 { la, la } \rt#4 { la, la } |
la, dod mi dod la, dod mi dod la, dod mi dod la, dod mi dod |
\rt#4 { re fa } \rt#4 { re fa } |
<sol, si, re>1~ |
<sol, si, re> |
\rt#4 { sol16 si } \rt#4 { sol si } |
\rt#4 { sol si } \rt#4 { sol si } |
\rt#4 { sol si } \rt#4 { sol si } |
\rt#4 { sold mi' } \rt#4 { sold mi' } |
\rt#4 { sold mi' } \rt#4 { sold mi' } |
\rt#4 { la do' } \rt#4 { la do' } |
\rt#4 { la do' } \rt#4 { fa la } |
\rt#4 { mi mi' } \rt#4 { mi mi' } |
\rt#4 { sold mi } \rt#4 { sold mi } |
\rt#4 { sold mi } \rt#4 { sold mi } |
mi1~ |
mi4 r4 r2 |
R1*3 |
la,4 r r la, |
re8. fad16 fad8. fad16 <re la>8. fad16 fad8. fad16 |
sol8. <si re'>16 <re' si>8. <si re'>16 <sol si>8. sol16 si8. re'16 |
sol4 r sol, r |
do r r16 do\f re mi fa sol la si |
do'4 r r2 |
R1 |
r16 fa, sol, la, sib, do re mi fa4 r |
R1 |
r4 fa sib, r |
