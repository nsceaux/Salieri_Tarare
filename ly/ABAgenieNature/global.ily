\time 4/4 \midiTempo#120
\tag #'all \key do \major \tempo "Andante" s1*45 s2
\tempo "Andante" s2 s1*16 s2
\tempo "Andante con Moto" s2 s1*15
\tempo "piu Allegro" s1*8 s4
\tempo "Andante" s2. s1*3
\tempo "Allegro" s1*6 \bar "|."
