\piecePartSpecs
#`((reduction)

   (tromboni #:on-the-fly-markup ,#{\markup\tacet#})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#})
   (flauti #:score-template "score-part-voix")
   (oboi #:score-template "score-part-voix")
   (fagotti #:score-template "score-part-voix")

   (violino1 #:indent 0)
   (violino2 #:indent 0)
   (viola #:indent 0)
   (basso #:score "score-basso")
   (silence #:indent 0)
   (trombe #:tag-global ()
           #:instrument ,#{\markup\center-column { Trompettes en ut}#}
           #:score-template "score-part-voix"
           #:music ,#{ <>^"[a 2]" s1*13 <>^"[a 2]" s1*9\break #}))
