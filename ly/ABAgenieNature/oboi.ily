\clef "treble" <sol' si'>1-\sug\p~ |
q |
<si' re''>~ |
q |
<si' sold''>~ |
q |
<la'' do''> |
<< { la''1 | r8 } \\ { do''2. la'4 | si'8 }
>> <>^\markup\italic Solo mi''8 mi'' mi'' mi'' mi'' mi'' mi'' |
mi''1 |
mi''~ |
mi''8 mi'' mi'' mi'' mi'' mi'' fa''! fad'' |
sol''2:8 sol'':8 |
sol''1~ |
sol'' |
r8 si''\trill re''' si'' sol'' re'' si' sol' |
fad'4 r r2 |
r8 si''\trill re''' si'' sol'' re'' si' sol' |
fad'4 r r2 |
<< sol''4\trill \\ si'\trill >> r4 << mi''\trill \\ do''\trill >> r |
<< re''\trill \\ si'\trill >> r << mi''\trill \\ do''\trill >> r |
<< re'' \\ si' >> r\fermata r2 |
R1*10 |
<mi'' sol'>1 |
q4 r r2 |
R1 |
r8 \slurDashed fad''(\mf la'' fad'') re''( la' fad' la') | \slurSolid
re'1\p |
R1 |
<< { re''2 re''8 } \\ { si'2-\sug\f si'8 } >> r r4 |
R1 |
<si' re''>1-\sug\f |
q4 r r2 |
R1*11 |
r8 mib''-\sug\f sol'' mib'' sib' mib'' sol' sib' |
mib'4 r r2 |
R1*41 |

