\clef "treble" sol'1-\sug\p |
sol' |
sol' |
sol' |
mi' |
mi' |
<mi' do''> |
r4 << { do''2 do''4 | mi''4 } \\ { do''2 do''4 | mi' } >> r r2 |
<mi' mi''>1~ |
q |
q4 r r2 |
R1 |
sol'1~ |
sol' |
<< { sol'4 re''2 re''4 } \\ { sol'4 re''2 re''4 } >> |
re''1~ |
re'' |
re'' |
sol' |
sol' |
<< sol'2.\fermata \\ sol'2. >> r4 | \allowPageTurn
R1*74 |
