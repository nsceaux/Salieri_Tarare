\clef "treble" R1*9 |
<si'' re'''>4\trill r <la'' do'''>\trill r |
<si'' sold''>\trill r << la''\trill \\ la'' >> r |
<sold'' si''>4\trill r r2 |
R1 |
<re''' fa'''>4\trill r <do''' mi'''>\trill r |
<si'' re'''>4\trill r << do'''\trill \\ do''' >> r |
<si'' re'''>4\trill r r2 |
r8 \once\override Script.avoid-slur = #'outside do'''(\trill la'' fad'' re''16 mi'' fad'' sol'' la'' si'' do''' re''') |
si''4\trill r r2 |
r8 do'''\trill la'' fad'' re''16 mi'' fad'' sol'' la'' si'' do''' re''' |
si''4\trill r << do'''\trill \\ mi''\trill >> r |
<< si''\trill \\ re''\trill >> r << do'''\trill \\ mi''\trill >> r |
<< si''\trill \\ re''\trill >> r\fermata r2 |
R1*10 |
r8 sol''( mi''' do''' sol'' do''' mi'' sol'') |
do''4 r r2 |
R1 |
<re'' la''>1~ |
q4 r r2 |
R1 |
r8 si''-\sug\f\trill re'''-! si''-! sol''-! r r4 |
R1 |
<si' sol''>1-\sug\f |
<< sol''4 \\ si' >> r r2 |
R1*11 |
r8 mib''-\sug\f sol'' mib'' sib' mib'' sol' sib' |
mib'4 r r2 |
R1*8 |
<fa' sib'>1-\sug\p |
<mi' la'> |
<<
  { la'2. re''4 |
    dod''2 re''4 fa'' |
    mi''1 |
    fa'' |
    re'' |
    re'' |
    re'' |
    re'' |
    fa'' |
    mi''~ |
    mi''~ |
    mi'' |
    do''2 re'' |
    si'1~ |
    si' |
    si'4 } \\
  { fa'2. fa'4 |
    mi'2 fa'4 re'' |
    dod''1 |
    la' |
    si'-\sug\f\> |
    si'-\sug\p |
    si' |
    si' |
    si' |
    si'~ |
    si' |
    do'' |
    la' |
    sold'-\sug\f~ |
    sold'-\sug\mf |
    sold'4-\sug\p
  }
>> r4 r2 |
R1*15 |


    