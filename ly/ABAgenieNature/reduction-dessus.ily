\clef "treble" si8(\p sol si re' sol' si' re'' si') |
sol''( re'' si' sol') re'( sol' re' si) |
sol( si re' sol') si'( re'' sol'' re'') |
si''( sol'' re'' si') sol'( re' si la) |
sold( si mi' sold') si'( mi'' sold'' si'') |
re'''( si'' sold'' mi'') re''( si' sold' re') |
do'( la do' mi') la'( do'' mi'' la'') |
do'''( la'' mi'' do'') la'( si' do'' re'') |
mi''8 mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
\grace { mi''16[ re'' do''] } <re'' si'>4\trill r \grace { re''16[ do'' si'] } <do'' la'>4\trill r |
\grace { do''16[ si' la'] } <si' sold'>4\trill r \grace { si'16[ la' sold'] } la'4\trill r |
\grace { do''16[ si' la'] } <si' sold'>8\trill mi'' mi'' mi'' mi'' mi'' fa''! fad'' |
sol''2:8 sol'':8 |
\grace { sol''16[ fa'' mi''] } <fa'' re''>4\trill r \voiceOne <mi'' do''>\trill r |
<re'' si'>\trill r do''\trill r |
\oneVoice <re'' si'>8\trill si'' re''' si'' sol'' re'' si' sol' |
fad'\trill do''( la' fad' re'16 mi' fad' sol' la' si' do'' re'') |
si'8\trill si'' re''' si'' sol'' re'' si' sol' |
fad'8\trill do'' la' fad' re'16 mi' fad' sol' la' si' do'' re'' |
r8 re'( si' sol' do'' sol' mi'' do'') |
si'( sol'' re'' si' do'' sol' mi' do') |
si2.\fermata r8 <sol mi'>8\f |
<sol mi'>1 |
q\p |
<mi' sold'>1 |
<mi' si'> |
<mi' la'> |
<la' re''> |
<sol' si'>~ |
<sol' do''> |
<do' la'>2( <re' si'>4 <mi' do''>) |
<do'' re'>2 <si' re'> |
r8\fz sol''( mi'' do'') sol' do'' mi' sol' |
do'1\p |
R1 |
r8\mf fad''( la'' fad'') re''( la' fad' la') |
re'1\p~ |
re' |
r8\f si''-! re'''-! si''-! sol'' << { si'([ do'' re'']) } \\ { sol'4\p sol'8 } >>|
<< { mi''4. re''8 do''( si' la' re'') } \\ { sol'2 la'8 sol' fad'4 } >> |
<si' sol'>1\f |
<sol' si' re''>4 sol'2\f \grace sol'8 fa'! mib'16 fa' |
\grace { sol16[ mib'] } do''1\fp~ |
do''~ |
do'' |
sib'2. sib'8. re''16 |
<sol' sib' do''>4 q8. q16 q2 |
<lab' sib' re''>4 q8. q16 q2 |
< sol' sib' mib''>4 q8. q16 <mib'' sib'>4 q8. <sib' re''>16 |
<mib' lab' do''>4 q8. q16 q8 <do' mib' lab'>-! <mib' lab' do''>-! <lab' fa' re''>-! |
<sol' mib''>8. sib'16 sib'8.\trill la'32 sib' do''8. lab'16 \grace sib'4 lab'8 sol'16 lab' |
sol'8. sib'16 sol''8. mib''16 sib''8 sol''-! mib''-! sib'-! |
do''8. do''16 mib''8. do''16 <sib' sol'>8-! <sol' mib'>-! <fa' re'>-! sib'-! |
sol'\f mib'' sol'' mib'' sib' mib'' sol' sib' |
mib'4 r r2 |
R1 |
<fa' la'!>4 r r2 |
R1 |
<fa' sib'>4 r r2 |
<re' la' re''>4 r r2 |
<sib' re''>4 r r2 |
<la' dod''>4 r r2 |
<re'' la'>2. q8. do''16\p |
sib'16( fa' sib' fa' sib' fa' sib' fa' \rt#4 { sib' fa') } |
\rt#4 { la' mi' } \rt#4 { la' mi' } |
\rt#4 { fa' la' } fa' la' fa' la' fa' re'' fa' re'' |
\rt#4 { dod'' mi' } fa' re'' fa' re'' fa'' re'' fa'' re'' |
mi'' dod'' la' dod'' mi'' dod'' la' dod'' mi'' dod'' la' dod'' mi'' la' mi'' la' |
fa'' re'' la' re'' fa'' la' fa'' la' fa'' la' fa'' la' fa'' re'' fa'' re'' |
r re''\f sol'' si'' re''' si'' sol'' si'' re''4 r |
r16 re'\p sol' si' sol' re' si re' sol8 si'16 re' si' re' si' re' |
\rt#4 { re'' si' } \rt#4 { re'' si' } |
\rt#4 { re'' si' } \rt#4 { re'' si' } |
\rt#4 { re'' si' } \rt#4 { re'' si' } |
\rt#4 { re'' si' } \rt#4 { re'' si' } |
\rt#4 { re'' si' } \rt#4 { re'' si' } |
\rt#4 { do'' la' } \rt#4 { do'' la' } |
\rt#4 { do'' la' } \rt#4 { re'' la' } |
si'\f sold''( si'' sold'' si'' sold'' si'' sold'' \rt#4 { si'' sold'') } |
si''\mf si'( mi'' si' mi'' si' mi'' si' \rt#4 { mi'' si') } |
mi''\p si mi' si mi' si mi' si \rt#4 { mi' si } |
\rt#4 { mi'\pp si } \rt#4 { mi' si } |
<sold si mi'>4 r4 r2 |
R1*3 |
r4\p \grace { fa''16[ mi'' re''] } <do'' mi''>4 \grace { re''16[ do'' si'] } <la' do''>8. <la' do''>16[ <sol' si'>8. <la' do''>16] |
<la' fad'>4 \grace { si''16[ la'' sol''] } la''4 \grace { sol''16[ fad'' mi''] } fad''8. re''16 do''8. re''16 |
si'4 \grace { la''16[ sol'' fad''] } sol''4 \grace { mi''16[ re'' dod''] } re''8. re'16 sol'8. la'16 |
    si'8. sol'16 <sol' si'>8. <la' do''>16 <si' re''>8. <re'' fa'!>16 <re'' fa'>8. <mi'' sol'>16 |
do''16 do'\f re' mi' fa' sol' la' si' do''8 re''16 mi'' fa'' sol'' la'' si'' |
do'''4 r r2 |
R1 |
r16 fa' sol' la' sib' do'' re'' mi'' fa''4 r |
R1 |
r4 <fa' do'' la''> <fa' re'' sib''> r |
