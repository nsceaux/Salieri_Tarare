\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \flautiInstr } <<
        \global \includeNotes "flauti"
        { \beginMark "Plus lent"
          s1. \beginMark "Le jour" s2
          s1*20 \beginMark "La Danse cesse" }
      >>
      \new Staff \with { \oboiInstr } << \global \includeNotes "oboi" >>
      \new Staff \with { \fagottiInstr } << \global \includeNotes "fagotti" >>
    >>
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \smaller en mi♭ }
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'corni \global \includeNotes "corni"
        { \noHaraKiri s1*21 \revertNoHaraKiri }
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \smaller en ut }
        shortInstrumentName = "Tr."
      } <<
        \keepWithTag #'trompettes \global \includeNotes "trombe"
        { \noHaraKiri s1*21 \revertNoHaraKiri }
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new StaffGroup <<
      \new Staff \with { \violoncelliInstr \haraKiriFirst } <<
        \global \keepWithTag #'violoncelli \includeNotes "basso"
      >>
      \new Staff \with {
        \bassoInstr
        \consists "Metronome_mark_engraver"
      } <<
        \global \keepWithTag #'basso \includeNotes "basso"
        \modVersion {
          s1*22\break
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
