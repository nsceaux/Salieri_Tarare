\clef "bass/bass" R1*22 |
<>^\markup\character Le Génie du feu
r2 r4 sol |
do'4 do'8 do' do'4 si8 la |
sold2. sold8 la |
si4. si8 si4 dod'8 re' |
dod'2 dod'8 la si dod' |
re'2 do'!4 do'8 re' |
si2 si4 sol8 sol |
do'2^\markup\italic à rigueur do'4. do'8 |
la2 si4. do'8 |
sol2 sol4. sol8 |
do'2 r |
\ffclef "soprano/treble"
<>^\markup\character-text La Nature Récit Majestueusement
r4 sol' sol' sol' |
do'' do''8 do'' do''4 re''8 mi'' |
re''1 |
re''4. re''8 re''4 re'' |
la'4. la'8 re''4 do'' |
si'2 si'8 si' do'' re'' |
mi''4. re''8 do'' si' la' re'' |
si'2 r |
R1 |
r4 r8 sol' sol'16 sol' sol' sol' sol'8 sol' |
do''4 do''8 r do'' do'' do'' re'' |
mib''2 re''4 do'' |
sib'2 sib'4^\markup\italic a Tempo sib'8 re'' |
do''4 do''8. do''16 do''4. mib''8 |
re''2 r8 sib' do'' re'' |
mib''4 mib''8 mib'' mib''4 mib''8 re'' |
do''4 do'' r8 do'' do'' re'' |
mib''4 sib' do'' re'' |
mib'' mib'' r mib''8 mib'' |
do''4 do'' sib' sib'8 sib' |
sol'2 r |
\ffclef "bass/bass" <>^\markup\character-text Le Génie du feu Récit Simplement
r4 sol8 sol sol4 sol8 lab |
sib4. sib8 sib sib sib do' |
la! la r la la la la si |
do'4. do'8 la la sol fa |
sib4
\ffclef "soprano/treble" <>^\markup\character La Nature
sib'4 sib'8 sib' do'' re'' |
la'4. la'8 do'' do'' do'' re'' |
sib'2 r4 sol'8 re'' |
dod''2 mi''8 mi'' dod'' la' |
re''4 re'' r^\markup\italic a Tempo re''8 do'' |
sib'4. sib'8 sib'4 sib'8 sib' |
la'2 r8 la' la' la' |
la'4. la'8 la'4. re''8 |
dod''4. dod''8 re''4. fa''8 |
mi''4 mi'' r mi''8 mi'' |
fa''4 re''8 r16 re'' re''4 fa''8 re'' |
si'2 r4 re''8 si' |
sol'4 sol' r r8 sol' |
re''4 re''8 re'' re''4 re''8 re'' |
si'2 r8 sol' si' re'' |
fa''4 re'' si' fa' |
mi' mi' r8 si' si' si' |
mi''4 si' si'4. re''8 |
do''2 r4 r8 do'' |
la'4 la'8 la' re''4 re''8 la' |
si' si' r4 r2 |
R1*3 |
\ffclef "bass/bass" <>^\markup\character-text Le Génie du feu Récit Simplement
r4 r8 mi sold4 r16 sold8*1/2 sold la |
si4. si8 si si la si |
sold4 sold8 la si4 si8 do' |
re'4 sold8 la si4 si8 do' |
la4 la8 r
\ffclef "soprano/treble" <>^\markup\character-text La Nature souriant
r8 do'' si' do'' |
la'4 la'8 la' re''4 do''8 re'' |
si'4 si' r8 sol' sol' la' |
si'4 si'8 do'' re''4 re''8 mi'' |
do''4 r r2 |
r2 sol'4. sol'8 |
do''2 do''8 do'' sib' do'' |
la' la' r4 r8 do'' mib''16 do'' do'' sib' |
la'4 r8 do'' la'4 r16 la' la' sib' |
fa'8 fa' r4 r2 |
