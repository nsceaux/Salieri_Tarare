\clef "alto" <si re'>1-\sug\p |
q |
q |
q |
<si sold'>1~ |
q |
<do' la'>~ |
q2. << { la'4 | sold' } \\ { la | si } >> r r2 |
r8 mi( fad sold) la( si do' la) |
re'( mi' re' si) do'( mi' do' la) |
mi2 r |
R1 | \allowPageTurn
r8 sol( la si do' re' mi' do') |
fa'( sol' fa' re') mi'( sol' mi' do') |
sol4 << { sol'2 sol'4 } \\ { si2 si4 } >> |
<la fad'>1 |
<si sol'> |
<la fad'> |
si2( do') |
si( do') |
si2.\fermata r8 do'-\sug\f |
do'1~ |
do'-\sug\p |
re'1 |
re' |
dod' |
re'~ |
re' |
do'2 mi'4 do' |
do'2 re'4 sol |
sol1 |
sol-\sug\fz~ |
sol-\sug\p~ |
sol |
fad-\sug\mf~ |
fad-\sug\p~ |
fad |
<< sol-\sug\f { s2 s8 s-\sug\p } >> |
do'4. si8 la4 re' |
sol8 si'[\f-! re''-! si'-!] sol'-! re'-! si-! re'-! |
sol4 sol'2-\sug\f \grace sol'8 fa'!8 mib'16 fa' |
mib'1-\sug\fp~ |
mib'~ |
mib' |
re'2. fa'4 |
sol' sol'8. sol'16 sol'2 |
lab'4 lab'8. lab'16 lab'2 |
sol'4 sol'8. sol'16 mib'2~ |
mib'4 mib'8. mib'16 mib'8 do' mib' lab' |
sib' sol sib mib' mib'8. mib'16 fa'8. fa'16 |
sib8. sib16 sol'8. sol'16 sol'8 sol' mib' mib' |
mib'4 lab8. lab16 sib4 sib |
mib'8\f mib'' sol'' mib'' sib' mib'' sol' sib' |
mib'4 r r2 |
R1 |
fa'4 r r2 |
R1 |
sib4 r r2 |
la4 r r2 |
sol4 r r2 |
mi'4 r r2 |
re'2. re'4-\sug\p |
fa16( sib fa sib fa sib fa sib \rt#4 { fa sib) } |
\rt#4 { mi la } \rt#4 { mi la } |
\rt#4 { la re' } \rt#4 { la re' } |
\rt#4 { mi' la } fa' la fa' la la la' la la' |
dod' mi' la' mi' dod' mi' la' mi' dod' mi' la' mi' dod' mi' la' mi' |
re' fa' la' fa' la' re' la' re' la' re' la' re' re' la re' la |
<si sol'>1-\sug\f\>~ |
q-\sug\p |
\rt#4 { re'16 sol } \rt#4 { re' sol } |
\rt#4 { re'16 sol } \rt#4 { re' sol } |
\rt#4 { re'16 sol } \rt#4 { re' sol } |
\rt#4 { si sold' } \rt#4 { si sold' } |
\rt#4 { si sold' } \rt#4 { si sold' } |
\rt#4 { mi' la } \rt#4 { mi' la } |
\rt#4 { mi' la } \rt#4 { fa' re' } |
mi'\f si'( sold' si' sold' si' sold' si' \rt#4 { sold' si') } |
sold'-\sug\mf \once\slurDashed mi'( sold' mi' sold' mi' sold' mi' \rt#4 { sold' mi') } |
sold'8-\sug\p sold16 mi sold mi sold mi \rt#4 { sold mi } |
si1 |
si4 r r2 |
R1*3 |
la4-\sug\p r r8. la'16 sol'8. la'16 |
fad'8. re'16 fad'8. fad'16 la'8. fad'16 fad'8. fad'16 |
sol'8. re'16 si8. re'16 si8. sol16 si8. re'16 |
sol'8. re'16 sol'8. la'16 si'8. re''16 re''8. mi''16 |
do''4 r r16 do'\f re' mi' fa' sol' la' si' |
do''4 r r2 |
R1 |
r16 fa sol la sib do' re' mi' fa'4 r |
R1 |
r4 fa' sib r |
