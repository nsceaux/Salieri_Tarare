\clef "bass" R1*15 |
<<
 { si1 | do'2. la4 | si1 | do'2. la4 | sol2 } \\
 { sol1 | la2. fad4 | sol1 | la2. fad4 | sol2 }
>> r2 |
R1*12 |
<mi sol>1~ |
q4 r r2 |
R1 |
\clef "tenor" <la re'>~ |
q4 r r2 |
R1 |
<si re'>1-\sug\f |
R1 |
\clef "bass" <si re'>1-\sug\f |
q4 r r2 |
R1*11 |
r8 mib'-\sug\f sol' mib' sib mib' sol sib |
mib4 r r2 |
R1*8 |
\clef "bass" re1-\sug\p |
dod |
re |
la, |
la, |
re |
sol-\sug\f\>~ |
sol-\sug\p |
sol |
sol |
<sol, sol> |
<sold, sold>1~ |
q |
<la, la>~ |
q2 <fa, fa> |
<mi, mi>1-\sug\f~ |
q-\sug\mf |
q4-\sug\p r r2 |
R1*15 |
