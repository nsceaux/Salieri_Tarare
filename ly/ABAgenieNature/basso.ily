\tag #'violoncelli \startHaraKiri
\clef "bass" sol,1\p~ |
sol, |
fa~ |
fa |
mi~ |
mi |
la |
fa |
mi4 r r2 |
<<
  \tag #'violoncelli {
    \stopHaraKiri r8 mi( fad sold) la( si do' la) |
    re'( mi' re' si) do'( mi' do' la) | \startHaraKiri
  }
  \tag #'basso {
    R1 |
    mi4 r la, r |
  }
>>
mi2. re8 do |
si,4 sol, r2 |
<<
  \tag #'violoncelli {
    \stopHaraKiri r8 sol( la si do' re' mi' do') |
    fa'( sol' fa' re') mi'( sol' mi' do') |
    sol4 \startHaraKiri
  }
  \tag #'basso {
    sol,4_"piz" r r2 |
    sol,4 r do r |
    sol,
  }
>> r4 r2 |
re4 r r2 |
sol,4 r r2 |
re4 r r2 |
sol,1_"col’arco" ~ |
sol, |
sol,2.\fermata r8 do\f |
do1~ |
do\p |
si,1 |
sold, |
sol,! |
fad, |
fa,! |
mi,2 do,4 mi |
fa2( re4 do) |
sol2 sol, |
do1\fz~ |
do\p |
do |
fad\mf~ |
fad-\sug\p~ |
fad |
sol2\f~ sol8 sol,([\p la, si,]) |
do4. si,8 la,4 re |
sol,8 si-![\f re'-! si-!] sol-! re-! si,-! re-! |
sol,4 sol2\f \grace sol8 fa! mib16 fa |
mib1\fp~ |
mib~ |
mib |
re2. re4 |
mib2~ mib8. mib16 mib8. mib16 |
fa2~ fa8. fa16 fa8. fa16 |
mib2( sol) |
lab2 lab,8 lab, lab, lab, |
sol,8. sol16 sol8. sol16 lab8. lab16 fa8. fa16 |
mib4 mib mib sol |
lab lab sib sib, |
mib8\f mib' sol' mib' sib mib' sol sib |
mib4 r r2 |
R1 |
mib4 r r2 |
R1 |
re4 r r2 |
fad4 r r2 |
sol4 r r2 |
sol4 r r2 |
fa4 r r fa\p |
<<
  \tag #'violoncelli {
    \stopHaraKiri re16( fa re fa re fa re fa \rt#4 { re fa) } |
    \rt#4 { dod mi } \rt#4 { dod mi } |
    \rt#4 { re fa } \rt#4 { re fa } |
    \rt#4 { la, la } \rt#4 { la, la } |
    la, dod mi dod la, dod mi dod la, dod mi dod la, dod mi dod |
    \rt#4 { re fa } \rt#4 { re fa } | \startHaraKiri
  }
  \tag #'basso {
    re1 |
    dod |
    re |
    la, |
    la, |
    re |
  }
>>
sol\f\>~ |
sol\p |
<<
  \tag #'violoncelli {
    \stopHaraKiri \rt#4 { sol16 si } \rt#4 { sol si } |
    \rt#4 { sol si } \rt#4 { sol si } |
    \rt#4 { sol si } \rt#4 { sol si } |
    \rt#4 { sold mi' } \rt#4 { sold mi' } |
    \rt#4 { sold mi' } \rt#4 { sold mi' } |
    \rt#4 { la do' } \rt#4 { la do' } |
    \rt#4 { la do' } \rt#4 { fa la } |
    \rt#4 { mi-\sug\f mi' } \rt#4 { mi mi' } |
    mi1\mf~ |
    mi\p~ |
    mi~ |
    mi4 \startHaraKiri
  }
  \tag #'basso {
    sol,1~ |
    sol,~ |
    sol, |
    sold,~ |
    sold, |
    la, |
    la,2 fa, |
    mi,1-\sug\f~ |
    mi,\mf~ |
    mi,\p~ |
    mi,~ |
    mi,4
  }
>> r4 r2 |
R1*3 |
la,4\p r r la, |
re r re r |
sol r sol r |
sol r sol, r |
do r r16 do\f re mi fa sol la si |
do'4 r r2 |
R1 |
r16 fa, sol, la, sib, do re mi fa4 r |
R1 |
r4 fa sib, r |
