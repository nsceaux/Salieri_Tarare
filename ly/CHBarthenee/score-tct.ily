\score {
  <<
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'recit \includeLyrics "paroles" }
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \small en Ut }
        shortInstrumentName = "Tr."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'trombe \includeNotes "trombe"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small en Mi♭ }
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
        { s1*34\allowPageTurn s1*44\noBreak }
      >>
      \new Staff \with { \tromboniInstr \haraKiri } <<
        \global \includeNotes "tromboni"
      >>
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 1\cm
  }
}
