\tag #'all \key do \major \midiTempo#100
\time 4/4 s1*7
\tempo "Un poco Andante" s1*6
\tempo "Allegro" \midiTempo #120 s1*13 \bar "||"
\tag #'all \key mib \major s1*5
\tempo "Allegro" \grace s8 s1*14 \bar "||"
\tag #'all \key do \major \tempo "Andante" \midiTempo#100 s1*12 \bar "||"
\tempo "Andante con Moto" \time 2/2 s1*7
\tempo "Largo" s1*4 \bar "||"
\tag #'all \key mib \major s1*9 s2..
\tempo "Allegro assai" \midiTempo#120 s8 s1*10
\midiTempo#100 s1*28
\tempo "Maestoso" s1*4 \bar "|."
