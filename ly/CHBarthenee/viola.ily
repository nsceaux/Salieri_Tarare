\clef "alto" <mi' do''>1-\sug\fp~ |
q |
<re' re''>1 |
<re' si'>1-\sug\fp |
<mi' do''>2~ q4 r |
la'1-\sug\fp~ |
la' |
<fa' re''>8-\sug\f q4 q q q8 |
q1\p |
<re' si'>2.\fermata r4 |
r8. <sol sol'>16 q2*3/2 |
r8. q16 q8. q16 q2 |
R1 |
r4 r8 do-\sug\f mi4 sol |
do'8 mi fa sol la do' si re' |
do' mi sol mi do mi sol do' |
do' la si do' si si re' sol' |
mi'4 do8. do16 do4\fermata r4 |
do'4\p do' do' do' |
re' re' re' re' |
re' re' re' re' |
sol2 r8.\fermata re'16\ff mi'8. fad'16 |
sol'2. si4 |
mi'4. mi'8 dod'4 dod' |
re'4. re'8 re'4 re' |
sol2 r\fermata |
mib'2-\sug\f r |
R1*4 | \allowPageTurn \grace s8
mib'2:16\ff mib':16 |
mib'8 sol' sib' sol' mib' sol' sib' sol' |
mib'1\fermata |
mi!4\p mi2 mi4~ |
mi mi2-\sug\cresc mi4 |
fa4-\sug\f fa2 fa4 |
fa fa2 fa4 |
fa'2-\sug\mf fa' |
fa' fa' |
sol'1 |
fad' |
sol' |
fad' |
sol'2.\fermata r4 | \allowPageTurn
do'4^"pizzicato" r do' r |
do' r do' r |
do' r do' r |
do' r do' r |
si r si r |
la r la r |
mi32-\sug\fp mi' mi' mi' mi' mi' mi' mi' mi'2.:32 |
fa'2:32-\sug\fp fa':32 |
re''2:32-\sug\fp re'':32 |
<mi' mi''>2:32\< q:32 |
q:32\! q:32 |
<la la'>2 r\fermata | \allowPageTurn
r4 do'-!-\sug\f sib-! la-! |
re'2 r |
r4 mi-! fa-! sol-! |
fa2 r |
re'1 |
sol2 do4. mi8 |
sol1 |
do2.-\sug\fp fa8.-\sug\f( mi16) |
mi2. la8.-\sug\f( sol16) |
sol2~ sol4.\fermata sol8 |
la2\f si |
do'1-\sug\fp~ |
do' |
si~ |
si~ |
si2 do' |
do'1 |
do'~ |
do' |
sib2~ sib~ |
sib2~ sib4.\fermata sib8-\sug\ff |
sib2 sib |
sib8 <fa' re''>4 q q q8 |
q q4 q q q8 |
mib'' mib' re' re'' do'' do' la la' |
sib' fa fa fa fa sib sib sib |
sib2:8 sib:8 |
mib'8 mib mib mib mib mib' mib' mib' |
mib'2 mib' |
mib'8\< <mib' mib''>4 q q q8~ |
q q4 q q q8\! |
q4 r r2 |
R1 |
lab4. lab'8-\sug\f lab'4 lab' |
sib'1 |
lab'4 r r2 |
r2 sol'4 r |
R1 |
r4 sol'8.\f sol'16 sol'4 sol' |
mib''4 do'' si' si |
do' do' do'8 mib'' mib'' mib'' |
fa''4 re'' si' sol' |
do''8 do' do' do' do'4 mib'8 sol' |
do'' do' do'' do'' do'' sib' la' sol' |
fa' la' do'' la' fa'2:8 |
fa':8 fa':8 |
sib4 sib'2\ff fa'4 |
re' sib re' fa' |
re'8 sib sib sib sib sib sib' sib' |
sib2:8 sib:8 |
mib':8 mib':8 |
mib'2 mib' |
mib'8\< <mib' mib''>4 q q q8~ |
q q4 q q q8 |
q2\! r |
R1 |
r2 lab'!4 r |
R1 |
solb'4 r r2 |
r8. fa'16-\sug\f fa'8. fa'16 fa'4 r |
R1 |
r8 r16 solb' solb'4 r8 r16 dob' dob'4 |
r2 r4 reb' |
