\clef "bass" do1\fp~ |
do |
si, |
sol,\fp |
do2 do4\ff sib, |
la,1\fp |
la, |
re'4-\sug\f la fa la |
re1\p |
sol2.\fermata r4 |
r8. sol16 sol2*3/2 |
r8. sol16 sol8. sol16 sol2 |
R1 |
r4 r8 do\f mi4 sol |
do do do do |
do do do do |
fa fa sol sol, |
do do8. do16 do4\fermata r4 |
do4\p do do do |
re re re re |
re re re re |
sol,2 r8.\fermata re16\ff mi8. fad16 |
sol2. si,4 |
mi4. mi8 dod4 dod |
re4. re8 re4 re |
sol,2 r\fermata |
mib2\f r |
R1*2 | \allowPageTurn
fa1\pp |
sib, |
mib2:16\ff mib:16 |
mib8 sol sib sol mib sol sib sol |
mib1\fermata |
mi2\p mi |
mi\cresc mi |
fa\f fa |
fa fa |
fa\f fa |
fa fa |
sol1 |
la |
sol |
la |
sol2.\fermata r4 |
do4_"pizzicato" r do r |
do r do r |
do r do r |
do r do r |
si, r si, r |
la, r la, r |
sol,!1\fp^"col arco" |
fa,2\fp~ fa,8 re' la fa |
re1\fp |
mi2:8\< mi2:8 |
mi:8\! mi:8 |
la,2 r2\fermata |
<>^"Violoncelli soli" r4 la-!\f sol-! fa-! |
sol2 r |
r4 do-! re-! mi-! |
fa2 r |
fa1 |
mi2 do4. mi8 |
sol2 sol, |
<>^"Tutti" do2.-\sug\fp fa8.\f mi16 |
mi2. la8.\f( sol16) |
sol2~ sol4.\fermata sol8 |
la2\f si |
do1\fp~ |
do |
si,~ |
si,~ |
si,2 do |
sib,1 |
lab,!~ |
lab,2 la, |
sib,1~ |
sib,2~ sib,4\fermata r4 |
sib2\ff sib |
sib4 sib sib sib |
sib sib sib sib |
mib' re' do' la? |
sib8 sib, sib, sib, sib, sib sib sib |
sib2:8 sib:8 |
mib'8 mib mib mib mib2:8 |
mib2 mib |
mib4\< mib mib mib |
mib mib mib mib |
mib4\! r r2 | \allowPageTurn
R1 |
lab,4. lab8\f lab4 lab |
mi1 |
fa4 r r2 |
r2 fa4 r |
R1 |
r4 sol8.\f sol16 sol4 sol |
mib'4 do' si si, |
do do do8 mib' mib' mib' |
fa'4 re' si sol |
do'8 do do do do4 mib8 sol |
do' do do' do' do' sib la sol |
fa la do' la fa2:8 |
fa:8 fa:8 |
sib,4 sib2\ff fa4 |
re sib, re fa |
re8 sib, sib, sib, sib, sib, sib sib |
sib,2:8 sib,:8 |
mib:8 mib:8 |
mib2 mib |
mib4\< mib mib mib |
mib mib mib mib |
mib2\! r |
R1 |
r2 lab4 r |
R1 |
solb4 r r2 |
r8. fa16\f fa8. fa16 fa4 r |
R1 |
r8 r16 solb solb4 r8 r16 dob dob4 |
r2 r4 reb |
