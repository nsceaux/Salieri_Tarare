\tag #'(recit basse) {
  Prê -- tres du grand Bra -- ma ! Roi du Gol -- fe Per -- si -- que !
  Grands de l’em -- pi -- re ! Peu -- ple i -- non -- dant le por -- ti -- que !
  La na -- ti -- on, l’ar -- mé -- e at -- tend un gé -- né -- ral.
}
\tag #'(vhaute-contre vtaille vbasse basse) {
  Pour nous pré -- ser -- ver d’un grand mal,
  que le choix de Bra -- ma s’ex -- pli -- que !
}
\tag #'(recit basse) {
  Vous pro -- met -- tez tous d’o -- bé -- ir
  au chef que Bra -- ma va choi -- sir ?
}
\tag #'(vhaute-contre vtaille vbasse basse) {
  Nous le ju -- rons sur cet au -- tel an -- ti -- que.
}
\tag #'(recit basse) {
  Dieu __ su -- bli -- me dans le re -- pos,
  ma -- gni -- fi -- que dans la tem -- pê -- te,
  soit que ton souffle é -- lève aux cieux les flots,
  soit __ que ton re -- gard les ar -- rê -- te ;
  Per -- mets que le nom d’un Hé -- ros,
  sor -- tant d’u -- ne bouche in -- no -- cen -- te,
  de -- vien -- ne cher à ses ri -- vaux ;
  et porte __ à l’en -- ne -- mi le trouble et l’é -- pou -- van -- te !
  
  Et vous, en -- fant, par le ciel ins -- pi -- ré !
  nom -- mez, nom -- mez sans crainte un hé -- ros pré -- fé -- ré.
  %% Elamir
  Peu -- ple que la ter -- reur é -- ga -- re,
  qui vous fait re -- dou -- ter ces sau -- va -- ges Chré -- tiens ?
  L’é -- tat man -- que- t-il de sou -- tiens ?
  Comp -- tez, aux pieds du Roi, vos dé -- fen -- seurs, Ta -- ra -- re…
}
\tag #'(vhaute-contre vtaille vbasse basse) {
  Ta -- ra -- re ! Ta -- ra -- re ! Ta -- ra -- re !
  Ah ! pour nous Bra -- ma se dé -- cla -- re :
  l’en -- fant vient de nom -- mer Ta -- ra -- re.
  Ta -- ra -- re ! Ta -- ra -- re ! Ta -- ra -- re !
}
\tag #'(recit basse) {
  Ar -- rê -- tez ce fou -- gueux trans -- port !
  
  Peu -- ple, c’est une er -- reur !
  
  Mon fils, que Dieu vous tou -- che !

  Le ciel m’ins -- pi -- rait Al -- ta -- mort ;
  Ta -- rare est sor -- ti de ma bou -- che.
}
\tag #'(vtaille vbasse basse) {
  Par l’en -- fant, Ta -- rare in -- di -- qué,
  n’est point un ha -- sard sans mys -- tè -- re.
  Plus son choix est in -- vo -- lon -- tai -- re,
  plus le vœu du ciel est mar -- qué.
}
\tag #'(vhaute-contre vtaille vbasse basse) {
  Oui, pour nous Bra -- ma se dé -- cla -- re ;
  l’en -- fant vient de nom -- mer Ta -- ra -- re.
  Ta -- ra -- re ! Ta -- ra -- re ! Ta -- ra -- re !
}

\tag #'(recit basse) {
  Ta -- rare est re -- te -- nu par un pre -- mier ser -- ment :
  son grand cœur s’est li -- é d’a -- van -- ce
  à suivre u -- ne jus -- te ven -- gean -- ce.

  Sei -- gneur, je rem -- pli -- rai le double en -- ga -- ge -- ment
  de la ven -- gean -- ce et du com -- man -- de -- ment.
}
