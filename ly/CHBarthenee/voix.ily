%% Arthénée
<<
  \tag #'vhaute-contre { \clef "alto/G_8" R1*13 | r2 }
  \tag #'vtaille { \clef "tenor/G_8" R1*13 | r2 }
  \tag #'vbasse { \clef "bass" R1*13 | r2 }
  \tag #'(recit basse) {
    \clef "bass"
    \tag #'recit {
      <>^\markup { Récit \italic avec orgueil }
      <>^\markup\italic {
        Atar monte sur un trône sur le côté du temple
        tous ses courtisans debout au pied.
      }
    }
    \tag #'basse <>^\markup\character Arthénée
    do'1~ |
    do'4 do'8 do' do'4. do'8 |
    sol2 r |
    re'4. re'8 re'4 si8 sol |
    do'2 do'4 r |
    dod'2 dod'4 mi' |
    la2 la4 r |
    re'1~ |
    re'8 re' re' re' re'4 fa'8 re' |
    si2 si8.\fermata sol16 sol8. sol16 |
    si2 r4 r8 si |
    re'2 re'8 r re'4 |
    si4. si8 si4. do'8 |
    sol2
  }
>>
%% Chœur
<<
  \tag #'(vhaute-contre basse) {
    \tag #'basse \clef "alto/G_8"
    r4 <>^\markup\character Chœur universel sol4 |
    do'4 re'8 mi' fa'4 fa'8 fa' |
    mi'2 r4 mi'8 mi' |
    re'4 re'8 re' sol'4. sol'8 |
    mi'4 do'
  }
  \tag #'vtaille {
    r4 sol |
    do'4 re'8 mi' fa'4 fa'8 fa' |
    mi'2 r4 do'8 do' |
    do'4 do'8 do' si4. si8 |
    do'4 do'
  }
  \tag #'vbasse {
    r4 sol4 |
    mi fa8 sol la4 si8 si |
    do'2 r4 do'8 do' |
    fa4 fa8 fa sol4. sol8 |
    do'4 do
  }
  \tag #'recit { r2 | R1*3 | r2 }
>>
%% Arthénée
<<
  \tag #'(vhaute-contre vtaille vbasse) { r2\fermata | R1*3 | r2 }
  \tag #'(recit basse) {
    \tag #'basse { \ffclef "bass" <>^\markup\character Arthénée }
    r8 sol la si |
    do'2 do'4 do'8 do' |
    la2 r4 la |
    re'4 re'8 re' re'4 re'8 la |
    si2
  }
>>
%% Chœur
<<
  \tag #'(vhaute-contre basse) {
    \tag #'basse \ffclef "alto/G_8"
    r8\fermata re' mi'8. fad'16 |
    sol'2. si4 |
    mi'4. mi'8 dod'4 dod' |
    re'1 |
    sol4 r r2\fermata |
  }
  \tag #'vtaille {
    r8\fermata re'8 mi'8. fad'16 |
    sol'2. si4 |
    mi'4. mi'8 dod'4 dod' |
    re'1 |
    sol4 r r2\fermata |
  }
  \tag #'vbasse {
    r8\fermata re8 mi8. fad16 |
    sol2. si4 |
    mi'4. mi'8 dod'4 dod' |
    re'1 |
    sol4 r r2\fermata |
  }
  \tag #'recit {
    r2 |
    R1*3 |
    R1^\fermataMarkup |
  }
>>
%% Arthénée
<<
  \tag #'(vhaute-contre vtaille vbasse) { R1*42 | }
  \tag #'(recit basse) {
    \tag #'basse { \ffclef "bass" <>^\markup\character Arthénée }
    \tag #'recit <>^\markup { Récit \italic ton inspiré }
    mib'!1~ |
    mib'2. sib!4 |
    sib4 sib8 sib sib4. do'8 |
    lab2. lab8 lab |
    re'4 re'8 re' re'4 sib |
    mib'1 |
    mib'2 r |
    sib2 do'4 reb' |
    do'2. do'4 |
    do'4. do'8 do'4 do' |
    do'1 |
    fa'~ |
    fa'4 re' re'4. re'8 |
    re'2 re'4. re'8 |
    re'1~ |
    re'~ |
    re' |
    re' |
    r2\fermata r4 <>^\markup\italic ton plus doux sol |
    do' do'8 do' \grace do' mi'4 re'8 do' |
    la2 r4 la |
    la4 la8 si \grace re' do'4 si8 la |
    sol4 sol r8 do' re' mi' |
    sold4. sold8 si4. re'8 |
    do'2 r4 r8 la |
    <>^\markup\italic { sempre a rigore, ton terrible } dod'2~ dod'8 dod' dod' dod' |
    re'2 r4 r8 re' |
    fa'2~ fa'8 fa' re' si |
    mi'1~ |
    mi' |
    la2 r4\fermata <>^\markup\italic avec amour la |
    do'2. re'4 |
    sib2 sol4. sol8 |
    do'2 do'4. do'8 |
    la2 r4 r8 la |
    si!4. si8 re'4 sol |
    mi'2 do'4. do'8 |
    sol2 sol4. sol8 |
    do2 r |
    R1*3 |
  }
>>
%% Elamir
<<
  \tag #'(vhaute-contre vtaille vbasse) { R1*9 | r2 r4\fermata }
  \tag #'(recit basse) {
    \ffclef "soprano/treble" <>^\markup\character\line { Récit \smallCaps Elamir }
    \tag #'recit \set Staff.shortInstrumentName = \markup\character El.
    mib''2 mib''4 r8 mib'' |
    mib''4 mib'' sol'' mib'' |
    re''2 re''4 r8 re''16 re'' |
    re''4 re''8 re'' sol''4. re''16 mib'' |
    fa''4 sol''8 re'' mib''4 r8 do'' |
    mi''4 mi''8 mi'' sol''4 mi''8 do'' |
    fa''2 r4 r8 do'' |
    do'' do'' do'' do'' fa''8. fa''16 mib''8 fa'' |
    re''2 r4 r8 re'' |
    fa''2 fa''4
  }
>>
%% Chœur
<<
  \tag #'(vhaute-contre basse) {
    \tag #'basse \ffclef "alto/G_8"
    r8 <>^\markup\character Chœur universel re' |
    re'4 sib8 fa' fa'4 re'8 sib' |
    sib'1 |
    fa'2 r4 fa'8 fa' |
    sol'4 fa' mib' mib'8 fa' |
    re'4 re' r sib |
    re'8 mib' fa' sol' lab'4. sib'8 |
    sol'2 sol'4 r8 mib' |
    mib'4 sib8 sol' sol'4 mib'8 sib' |
    sib'1~ |
    sib' |
    sol'4
  }
  \tag #'vtaille {
    r8 re'8 |
    re'4 sib8 fa' fa'4 re'8 fa' |
    fa'1 |
    re'2 r4 fa'8 fa' |
    sol'4 fa' mib' do'8 do' |
    re'4 re' r sib |
    sib8 do' re' mib' fa'4. re'8 |
    mib'2 mib'4 r8 mib' |
    mib'4 sib8 sol' sol'4 mib'8 sol' |
    sol'1~ |
    sol' |
    mib'4
  }
  \tag #'vbasse {
    r8 sib8 |
    sib4 fa8 re' re'4 sib8 re' |
    re'1 |
    sib2 r4 re'8 re' |
    mib'4 re' do' la?8 la |
    sib4 sib r sib |
    sib8 sib sib sib sib4. sib8 |
    mib'2 mib'4 r8 mib' |
    mib'4 sib8 mib' mib'4 sib8 mib' |
    mib'1~ |
    mib' |
    sib4
  }
  \tag #'recit {
    r4 |
    \set Staff.shortInstrumentName = ##f
    R1*10 |
    r4
  }
>>
%% Altamort, Arthénée, Elamir
<<
  \tag #'(vhaute-contre vtaille vbasse) { r4 r2 | R1*6 r2 }
  \tag #'(recit basse) {
    \ffclef "bass" <>^\markup\character-text Altamort avec fureur
    r8 sib16 mib' mib'8 sib16 sib sib8 sib |
    sol4
    \ffclef "bass" <>^\markup\character Arthénée
    mib'4 mib'8 r mib' reb'16 mib' |
    do'4 r r2 |
    r4 r8 <>^\markup\italic à l’enfant sib reb'4 r16 sib reb' do' |
    lab8 lab
    \ffclef "soprano/treble" <>^\markup\character Elamir
    r8 do'' fa''4 do''8 do'' |
    do''4 do''8 re''8 si'4 r8 sol' |
    si'4 si'8 re'' si'4 si'8 do'' |
    sol'4 sol'
  }
>>
%% Chœur
<<
  \tag #'(vhaute-contre vtaille vbasse basse) {
    <<
      \tag #'vhaute-contre { r2 R1*7 r4 }
      \tag #'(vtaille basse) {
        \tag #'basse \ffclef "tenor/G_8"
        r4 sol8 sol |
        do'4 mib' re' sol'8 sol' |
        mib'2 r4 sol' |
        lab'4 fa'8 fa' re'4 sol'8 sol' |
        mib'4 mib' r do'8 re' |
        mib'2 mib'8 re' do' sib |
        la4 la do'8 do' do' do' |
        fa'2 fa'4 fa' |
        re'4
      }
      \tag #'vbasse {
        r4 sol8 sol |
        mib'4 do' si4 si8 si |
        do'2 r4 mib' |
        fa' re'8 re' si4 sol8 sol |
        do'4 do' r mib8 sol |
        do'2 do'8 sib la sol |
        fa4 fa la8 la la la |
        la2 la4 la |
        sib4
      }
    >>
    <<
      \tag #'(vhaute-contre basse) {
        \tag #'basse \ffclef "alto/G_8"
        sib'2 fa'4 |
        re' sib re' fa'8 fa' |
        re'4 sib r sib |
        re'8 mib' fa' sol' lab'4. sib'8 |
        sol'2 sol'4 r8 mib' |
        mib'4 sib8 sol' sol'4 mib'8 sib' |
        sib'1~ |
        sib' |
        sol'2
      }
      \tag #'vtaille {
        sib2 fa'4 |
        re' sib re' fa'8 fa' |
        re'4 sib r sib |
        sib8 do' re' mib' fa'4. re'8 |
        mib'2 mib'4 r8 sib |
        sib4 sol8 mib' mib'4 sib8 sol' |
        sol'1~ |
        sol' |
        mib'2
      }
      \tag #'vbasse {
        sib2 fa'4 |
        re' sib re' fa'8 fa' |
        re'4 sib r sib |
        sib8 sib sib sib sib4. sib8 |
        mib'2 mib'4 r8 mib' |
        mib'4 sib8 mib' mib'4 sib8 mib' |
        mib'1~ |
        mib' |
        sib2
      }
    >>
  }
  \tag #'recit { r2 | R1*15 | r2_"parlé" }
>>
%% Atar, Tarare
<<
  \tag #'(vhaute-contre vtaille vbasse) { r2 R1*8 }
  \tag #'(recit basse) {
    \ffclef "bass" <>^\markup\character Atar r4 r8 sib |
    sib sib sib sib sol4 r8 sib |
    reb' reb' reb' mib' do'4 r8 lab16 sib |
    do'8 do'16 do' sib8 do' lab lab r mib' |
    do'4 lab8 sib do'4 do'8 reb' |
    lab lab
    \ffclef "tenor/G_8" <>^\markup\character-text Tarare \line {
      fier et lent, la main sur le cœur
    }
    r8 lab reb'4 r16 reb' reb' reb' |
    lab4. lab8 dob'8 dob' dob' reb' |
    sib4 r16 reb' mib' fab' mib'8 mib' r4 |
    fa'!8 fa'16 fa' fa'8 solb' reb'4 r |
  }
>>

