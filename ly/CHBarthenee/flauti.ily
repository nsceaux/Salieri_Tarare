\clef "treble" R1*26 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''1~ | sol'' | sol'' | do'' | re'' |
    sol'''1 | sol''' | sol'''4 }
  { sib'1~ | sib' | sib' | lab' | lab' |
    \appoggiatura sol'8 mib'''1 | mib''' | mib'''4 }
  { s1\f\> | s\! | s | s1*2\pp | s1-\sug\ff }
>> r4 r2\fermata |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { do''2( mi'' | sol'' do''') | do'''1~ | do'''2 lab'' |
    fa'' re'' | re'' re'' | re''1~ | re'' |
    re'' | re'' | re''2\fermata }
  { sol'2( do'' | mi'' sol'') | lab''1~ | lab''2 do'' |
    lab' lab' | do'' do'' | si'1 | do'' |
    si' | do'' | si'2\fermata }
  { s1-\sug\p | s1-\sug\cresc | s1\f | s2 s\mf | }
>> r2 |
<<
  \tag #'(flauto1 flauti) {
    \tag #'flauti <>^\markup\concat { 1 \super re }
    do'''4 do'''8. do'''16 \grace do'''8 mi'''4 \grace mi'''8 re'''8. do'''16 |
    \grace si''8 la''2 r8 do'' fa'' la'' |
    la''4 la''8 si'' \grace re''' do'''4 si''8 la'' |
    la'' sol'' sol''4 r8 do'''([ re''' mi''']) |
    sold''4. sold''8 si''4. re'''8 |
    do'''2 r |
  }
  \tag #'flauto2 { R1*6 }
>> \allowPageTurn
R1*3 |
<< { mi'''1~ | mi'''1 | } { s4 s2.\< | s4 s2.\! } >>
la''2 r\fermata |
R1*7 |
%% Largo
R1*13 |
r2 r4\fermata r8 \twoVoices #'(flauto1 flauto2 flauti) <<
  { re''8 |
    re''2 fa'' |
    sib''1 |
    fa'' |
    sol''4 fa'' mib''2 |
    re''~ re''8 sib' sib' sib' |
    re''8 mib'' fa'' sol'' lab''4. sib''8 |
    sol''1 |
    mib''2 sol'' |
    sib''1 |
    mib'''1 |
    mib'''4 }
  { fa'8 |
    fa'2 re'' |
    re''1 |
    re'' |
    sib'2 la'?4 do'' |
    fa'2~ fa'8 sib' sib' sib' |
    sib'8 do'' re'' mib'' fa''4. re''8 |
    mib''1 |
    sol'2 mib'' |
    sol''1~ |
    sol''1 |
    mib''4 }
  { s8-\sug\ff s1*8 s1*2\< s4\! }
>> r4 r2 |
R1*14 |
r4 <>^"a 2" sib''2-\sug\ff fa''4 |
re'' sib' re'' fa'' |
re''8 sib' sib' sib' sib'4. sib'8 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re''8 mib'' fa'' sol'' lab''4. sib''8 |
    sol''2~ sol''4. mib''8 |
    mib''2 sol'' |
    sib''1 |
    mib'''~ |
    mib'''2 }
  { sib'8 do'' re'' mib'' fa''4. re''8 |
    mib''2~ mib''4. sol'8 |
    sol'2 sib' |
    sol''1 |
    sol''~ |
    sol''2 }
  { s1*2 s1-\sug\f s1*2\< s4\! }
>> r2 |
R1*8 |
