\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with { \consists "Metronome_mark_engraver" \flautiInstr } <<
        \global \keepWithTag #'flauti \includeNotes "flauti"
      >>
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
        { \noHaraKiri s1*26 \revertNoHaraKiri }
      >>
      \new Staff \with { \clarinettiInstr \haraKiri } <<
        \global \keepWithTag #'clarinetti \includeNotes "clarinetti"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \small en Ut }
        shortInstrumentName = "Tr."
        \haraKiri
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'trombe \includeNotes "trombe"
        { \noHaraKiri s1*26 \revertNoHaraKiri }
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small en Mi♭ }
        shortInstrumentName = "Cor."
        \haraKiri
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with { \tromboniInstr \haraKiri } <<
        \global \includeNotes "tromboni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
        { \noHaraKiri s1*26 \revertNoHaraKiri }
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { 
          \consists "Metronome_mark_engraver"
        } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      \haraKiri
      instrumentName = \markup\center-column\smallCaps { Chœur universel }
      shortInstrumentName = "Ch."
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
        { \noHaraKiri s1*26 \revertNoHaraKiri }
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
        { \noHaraKiri s1*26 \revertNoHaraKiri }
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
        { \noHaraKiri s1*26 \revertNoHaraKiri }
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\character Arthénée
      shortInstrumentName = \markup\character Ar.
    } \withLyrics <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*6\break s1*7\pageBreak
        s1*5\pageBreak
        s1*6\pageBreak
        s1*7\pageBreak
        \grace s8 s1*5\pageBreak
        s1*5\break s1*6\pageBreak
        s1*6\pageBreak
        s1*8\break s1*7\pageBreak
        s1*5\break s1*4\pageBreak
        s1*6\pageBreak
        s1*6\break s1*4\pageBreak
        s1*5\break s1*5\pageBreak
        s1*5\pageBreak
        s1*6\break s1*3 s2 \bar "" \pageBreak
        
      }
      \modVersion { s1*14\noBreak s1*12\break }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
