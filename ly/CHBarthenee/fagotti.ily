\clef "bass" R1*13 |
r4 r8 do\f mi4 sol |
do'8 mi fa sol la do' si re' |
do' mi sol mi do mi sol do' |
do' la si do' si si re' sol' |
mi'4 do8. do16 do4\fermata r4 |
R1*3 |
r2 r8.\fermata re16-\sug\ff mi8. fad16 |
sol2. si,4 |
mi4. mi8 dod4 dod |
re4. re8 re4 re |
sol,2 r\fermata |
\clef "tenor" \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mib'1~ | mib'~ | mib' | do' | re' | mib' | mib'~ | mib'4 }
  { sol1~ | sol~ | sol | lab | lab | sol | sol~ | sol4 }
  { s1\f\> | s\! | s | s1*2-\sug\pp | s1-\sug\ff }
>> r4 r2\fermata |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'1~ | do' | do'~ | do' |
    do'~ | do'2 fa' | re'1 | re' |
    re' | re' | re'2\fermata }
  { sol1~ | sol | lab~ | lab |
    lab~ | lab2 do' | si1 | do' |
    si | do' | si2\fermata }
  { s1\p\> | s\!-\sug\cresc | s1*2-\sug\f | s1-\sug\mf }
>> r2 |
\clef "bass" \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol1 | la | la | sol | re' | do' | }
  { mi1 | fa | fa | mi | sold | la | }
  { s1-\sug\p }
>>
\clef "tenor" sol'1-\sug\fp |
fa'-\sug\fp |
re'-\sug\fp |
<< { mi'1~ | mi' | } { s4 s2.\< | s4 s2.\! } >>
la2 r\fermata |
\clef "bass" r4 la-!\f sol-! fa-! |
sol2 r |
r4 do-! re-! mi-! |
fa2 r |
fa1 |
mi2 do4. mi8 |
sol2 sol, |
do2.-\sug\fp fa8.-\sug\f mi16 |
mi2. la8.-\sug\f( sol16) |
sol2~ sol4.\fermata sol8 |
la2\f si |
do'2 r | \allowPageTurn
R1*9 |
sib2\ff sib |
sib4 sib sib sib |
sib sib sib sib |
mib' re' do' la? |
sib8 sib, sib, sib, sib, sib sib sib |
sib2:8 sib:8 |
mib'8 mib mib mib mib2:8 |
mib2 mib |
mib4\< mib mib mib |
mib mib mib mib |
mib4\! r r2 |
R1*14 |
r4 sib2-\sug\ff fa4 |
re sib, re fa |
re8 sib, sib, sib, sib, sib, sib sib |
sib,2:8 sib,:8 |
mib:8 mib:8 |
mib2-\sug\f mib |
mib4\< mib mib mib |
mib mib mib mib |
mib2\! r |
R1*8 |
