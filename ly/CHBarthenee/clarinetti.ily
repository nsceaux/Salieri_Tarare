\clef "treble" R1*26 |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { sol''1~ | sol'' | }
  { sib'1~ | sib' | }
  { s1\f\> | s\! | }
>>
R1*3 |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { mib''4 sib''2 sib''4 | sib''1 | sib''4 }
  { sol'4 sol''2 sol''4 | sol''1 | sol''4 }
  { s1-\sug\ff }
>> r4 r2\fermata |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { do''2( mi'' | sol'' do''') | do'''1~ | do'''2 lab'' |
    fa'' re'' | re'' re'' | }
  { sol'2( do'' | mi'' sol'') | lab''1~ | lab''2 do'' |
    lab' lab' | do'' do'' | }
  { s1-\sug\p | s-\sug\cresc | s1-\sug\f | s2 s-\sug\mf | }
>>
R1*24 |
%% Largo
R1*13 | \allowPageTurn
r2 r4\fermata r8 \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { re''8 |
    re''2 fa'' |
    sib''1 |
    fa'' |
    sol''4 fa'' mib''2 |
    re''~ re''8 sib' sib' sib' |
    re''8 mib'' fa'' sol'' lab''4. sib''8 |
    sol''1 |
    mib''2 sol'' |
    sib''1~ |
    sib''~ |
    sib''4 }
  { fa'8 |
    fa'2 re'' |
    re''1 |
    re'' |
    sib'2 la'?4 do'' |
    fa'2~ fa'8 sib' sib' sib' |
    sib'8 do'' re'' mib'' fa''4. re''8 |
    mib''1 |
    sol'2 mib'' |
    sol''1~ |
    sol''~ |
    sol''4 }
  { s8-\sug\ff s1*8 s1*2\< s4\! }
>> r4 r2 | \allowPageTurn
R1*31 |
