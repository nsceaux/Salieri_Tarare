\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''1~ | mi'' | re'' | sol''~ | sol''2~ sol''4 }
  { sol'1~ | sol'~ | sol' | si' | do''2~ do''4 }
  { s1*3-\sug\fp | s1-\sug\fp }
>> r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1~ | sol'' | fa''~ | fa'' | re''2.\fermata }
  { mi''1~ | mi'' | re''~ | re'' | si'2.\fermata }
  { s1*2-\sug\fp | s1-\sug\f | s1-\sug\p }
>> r4 |
R1*3 |
r4 r8 <>\f do'8 mi'4 sol' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 re''8 mi'' fa''2 | mi''2. mi''4 | re''2 re'' | }
  { mi'4 fa'8 sol' la'4 si' | do''2. do''4 | do''2 si' | }
>>
mi''4 do'8. do'16 do'4\fermata r4 |
R1*8 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1~ | sol'' | sol'' | do'' | re'' |
    mib''4 sib''2 sib''4 | sib''1 | sib''4 }
  { sib'1~ | sib' | sib' | lab' | lab' |
    sol'4 sol''2 sol''4 | sol''1 | sol''4 }
  { s1\f\> | s\! | s | s\pp | s | s-\sug\ff }
>> r4 r2\fermata |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''2( mi'' | sol'' do''') | do'''1~ | do'''2 lab'' |
    fa'' re'' | re'' re'' | re''1~ | re'' |
    re'' | re'' | re''2\fermata }
  { sol'2( do'' | mi'' sol'') | lab''1~ | lab''2 do'' |
    lab' lab' | do'' do'' | si'1 | do'' |
    si' | do'' | si'2\fermata }
  { s1-\sug\p | s-\sug\cresc | s1-\sug\f | s2 s-\sug\mf | }
>> r2 | \allowPageTurn
R1*6 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''1 | re'' | fa'' | la'' | sold'' | la''2 }
  { sib'1 | la' | la' | do'' | si' | la'2 }
  { s1-\sug\fp | s1-\sug\fp | s-\sug\fp | s4 s2.\< | s4 s2.\! }
>> r2\fermata |
R1*7 |
<>^"a due" do'2.-\sug\fp fa'8.-\sug\f mi'16 |
mi'2. la'8.-\sug\f sol'16 |
sol'2~ sol'4.\fermata sol'8 |
la'2-\sug\f si' |
do''2 r |
R1*8 |
r2 r4\fermata r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8 |
    re''2 fa'' |
    sib''1 |
    fa'' |
    sol''4 fa'' mib''2 |
    re''~ re''8 sib' sib' sib' |
    re''8 mib'' fa'' sol'' lab''4. sib''8 |
    sol''1 |
    mib''2 sol'' |
    sib''1~ |
    sib''~ |
    sib''4 }
  { fa'8 |
    fa'2 re'' |
    re''1 |
    re'' |
    sib'2 la'?4 do'' |
    fa'2~ fa'8 sib' sib' sib' |
    sib'8 do'' re'' mib'' fa''4. re''8 |
    mib''1 |
    sol'2 mib'' |
    sol''1~ |
    sol''~ |
    sol''4 }
  { s8-\sug\ff s1*8 s1*2\< s4\! }
>> r4 r2 |
R1*14 |
r4 <>^"a 2" sib''2-\sug\ff fa''4 |
re'' sib' re'' fa'' |
re''8 sib' sib' sib' sib'4. sib'8 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8 mib'' fa'' sol'' lab''4. sib''8 |
    sol''2~ sol''4. mib''8 |
    mib''2 sol'' |
    sib''1~ |
    sib''~ |
    sib''2 }
  { sib'8 do'' re'' mib'' fa''4. re''8 |
    mib''2~ mib''4. sol'8 |
    sol'2 sib' |
    sol''1~ |
    sol''~ |
    sol''2 }
  { s1*2 s1-\sug\f s1*2\< s4\! }
>> r2 |
R1*8 |
