\score {
  <<
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'basse \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff \with {
        \consists "Metronome_mark_engraver"
      } <<
        \global \keepWithTag #'clarinetto1 \includeNotes "clarinetti"
        { s1*103 \mmRestDown
          \override Staff.MultiMeasureRest.stencil = ##f
          \new CueVoice \notemode {
            r4^"Hautbois" sib''2\ff fa''4 |
            re'' sib' re'' fa'' |
            re''8 sib' sib' sib' sib'4. sib'8 |
            re''8 mib'' fa'' sol'' lab''4. sib''8 |
            sol''2~ sol''4. mib''8 |
            mib''2\f sol'' |
            sib''1~ |
            sib''\<~ |
            sib''2\! r |
          }
          \revert Staff.MultiMeasureRest.stencil \mmRestCenter
        }
      >>
      \new Staff << \global \keepWithTag #'clarinetto2 \includeNotes "clarinetti"
        { s1*103 \mmRestDown
          \override Staff.MultiMeasureRest.stencil = ##f
          \new CueVoice \notemode {
            r4 sib''2\ff fa''4 |
            re'' sib' re'' fa'' |
            re''8 sib' sib' sib' sib'4. sib'8 |
            sib'8 do'' re'' mib'' fa''4. re''8 |
            mib''2~ mib''4. sol'8 |
            sol'2\f sib' |
            sol''1~ |
            sol''\<~ |
            sol''2\! r |
          }
          \revert Staff.MultiMeasureRest.stencil \mmRestCenter
        }
      >>
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
}
