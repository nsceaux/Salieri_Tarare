\clef "treble"
<<
  \tag #'violino1 {
    do'''1\fp~ |
    do''' |
    sol'' |
    re'''\fp |
    do'''2~ do'''4 r |
    <mi'' dod'''>1\fp~ |
    q |
    re'''8\f re'''4 re''' re''' re'''8 |
    re'''1\p |
    si''2.\fermata r4 |
    r8. <re'' si''>16 q2*3/2 |
    r8. <re'' re'''>16 q8. q16 q2 |
    R1 |
  }
  \tag #'violino2 {
    <sol' mi''>1-\sug\fp~ |
    q |
    <re'' sol'> |
    <si' sol''>-\sug\fp |
    <do'' mi''>2~ q4 r |
    <la' sol''>1-\sug\fp~ |
    q |
    <la' fa''>8-\sug\f q4 q q q8 |
    q1-\sug\p |
    <sol' re''>2.\fermata r4 |
    r8. <si' sol''>16 q2*3/2 |
    r8. <si' si''>16 q8. q16 q2 |
    R1 |
  }
>>
r4 r8 do'\f mi'4 sol' |
<<
  \tag #'violino1 {
    <sol mi' do''>4 re''8 mi'' fa''4 fa''8 fa'' |
    mi''4 <sol'' do'' mi'> <sol mi' do'' mi''> mi''8 mi'' |
    re''4 re''8 re'' re''4 <sol' re'' si''> |
    <sol' mi'' do'''>4
  }
  \tag #'violino2 {
    <sol mi' do''>8 mi' fa' sol' la' do'' si' re'' |
    do'' mi' sol' mi' do' mi' sol' do'' |
    do'' la' si' do'' si' si' re'' sol'' |
    mi''4
  }
>> do'8. do'16 do'4\fermata r4 |
r8 mi'\p sol' do'' mi'' do'' la' sol' |
fad' la re' la fad' re' la' fad' |
<<
  \tag #'violino1 {
    re''8 fad'' la'' fad'' re'' fad' la' re' |
    si'2
  }
  \tag #'violino2 {
    <fad' la>1 |
    <si sol'>2
  }
>> r8\fermata r16 re''\ff mi''8. fad''16 |
sol''2. si'4 |
mi''4. mi''8 dod''4 dod'' |
re''4. re'8 re'4 re' |
sol2 r\fermata |
<mib' sib' sol''>2\f r |
R1*2 | \allowPageTurn
<<
  \tag #'violino1 { lab'1-\sug\pp | fa' | }
  \tag #'violino2 { do'1-\sug\pp | re' | }
>>
mib''16\ff fa'' sol'' lab'' sib'' lab'' sol'' fa'' mib''8 re''16 do'' sib' lab' sol' fa' |
mib' re' mib' re' mib' re' mib' re' \rt#4 { mib' re' } |
mib'1\fermata |
do'8*2/3\p( mi' sol' do'' sol' mi') mi'( sol' do'' mi'' do'' sol') |
sol'( do'' mi'' sol'' mi''\cresc do'') do''( mi'' sol'' do''' sol'' mi'') |
do''\f( fa'' lab'' do''' lab'' fa'') do''( fa'' lab'' do''' lab'' fa'') |
do''( fa'' lab'' do''' lab'' fa'') do''( fa'' sol'' lab'' fa'' do'')
lab'(\mf do'' fa'' fa'' do'' lab') fa'( sol' lab') fa'( sol' lab') |
fa'( sol' lab') fa'( sol' lab') fa'( sol' lab') fa'( sol' lab') |
\tuplet 3/2 { sol'4( si' do'' } \tuplet 3/2 { re'' si' sol') } |
fad'4*2/3 ( la' si' do'' la' fad') |
sol'( si' do'' re'' si' sol') |
fad'( la' si' do'' la' fad') |
sol'2.\fermata r4 |
mi'8\p sol' mi' sol' mi' sol' mi' sol' |
<<
  \tag #'violino1 {
    la'8 do'' la' do'' la' do'' la' do'' |
    la' do'' la' do'' la' do'' la' do'' |
    sol' do'' sol' do'' sol' do'' sol' mi'' |
    sold'8 sold'4 sold' sold' sold'8 |
    la' do'' la' do'' la' do'' la' la' |
    dod'32\fp dod'' dod'' dod'' dod'' dod'' dod'' dod'' dod''2.:32 |
    re''2:32\fp re'':32 |
    <re'' fa''>:32\fp q:32 |
    do''32\< <do'' la''> q q q q q q q2.:32 | <>\!
  }
  \tag #'violino2 {
    fa'8 la' fa' la' fa' la' fa' la' |
    fa'8 la' fa' la' fa' la' fa' la' |
    mi' sol' mi' sol' mi' sol' mi' sol' |
    re'( mi' re' mi' re' mi' re' mi') |
    do' mi' do' mi' do' mi' do' do' |
    sol32\fp sib' sib' sib' sib' sib' sib' sib' sib'2.:32 |
    la'2:32-\sug\fp la':32 |
    <fa' la'>2:32-\sug\fp q:32 |
    la'32\< <do'' la''> q q q q q q q2.:32 | <>\!
  }
>>
<si' sold''>2:32 q:32 |
<la' la''>2 r4 <<
  \tag #'violino1 {
    la''4\pp |
    do'''2. re'''4 |
    sib''2 sol''4. sol''8 |
    do'''2 do'''4. do'''8 |
    la''2 r4 r8 la'' |
    sol''4. sol''8 sol''4 sol'' |
    sol''2 do'''4. do'''8 |
    do'''2 si'' |
  }
  \tag #'violino2 {
    la'4-\sug\pp |
    do''2. re''4 |
    sib'2 sol'4. sol'8 |
    do''2 do''4. do''8 |
    do''2 r4 r8 do'' |
    si'!4. si'8 si'4 si' |
    do''2 mi''4. mi''8 |
    re''1 |
  }
>>
%% Largo
do'2.\fp fa'8.\f mi'16 |
mi'2. la'8.\f sol'16 |
sol'2~ sol'4.\fermata sol'8 |
la'2\f si' |
<<
  \tag #'violino1 {
    mib''1\fp~ |
    mib'' |
    re''~ |
    re''~ |
    re''2 mib'' |
    mi''1 |
    fa''~ |
    fa'' |
    re''2 re''~ |
    re''2~ re''4.\fermata re''8\ff |
    re''4( sib'8) fa'' fa''4( re''8) sib'' |
    <re'' sib''>8 q4 q q q8 |
    <sib' fa''>8 q4 q q q8 |
  }
  \tag #'violino2 {
    sol'1-\sug\fp~ |
    sol' |
    sol'~ |
    sol'~ |
    sol' |
    sol' |
    <fa' do''>1~ |
    q |
    <fa' re''>2~ q~ |
    q~ q4.\fermata fa'8-\sug\ff |
    fa'4( re'8) re'' re''4( sib'8) re'' |
    <sib' fa''>8 q4 q q q8 |
    <fa' re''>8 q4 q q q8 |
  }
>>
sol''8 sol' fa' fa'' mib'' la'' do''' mib'' |
re'' <re' sib>8 q q q sib' sib' sib' |
<<
  \tag #'violino1 { re''8 mib'' fa'' sol'' lab''!4. sib''8 | sol'' }
  \tag #'violino2 { sib'8 do'' re'' mib'' fa'' mib'' re'' re'' | mib'' }
>> <mib' sol>8 q q q <<
  \tag #'violino1 {
    <mib' mib''>8 q q |
    q4( sib'8) sol'' sol''4( mib''8) sib'' |
    sib'' sib''4\< sib'' sib'' sib''8~ |
    sib'' <mib'' mib'''>4 q q q8\! |
    q4
  }
  \tag #'violino2 {
    <sib sol'>8 q q |
    q4. sib'8 sib'4( sol'8) sol'' |
    <sib' sol''>8\< q4 q q q8~ |
    q q4 q q q8\! |
    q4
  }
>> r4 r2 |
R1 | \allowPageTurn
lab4. <<
  \tag #'violino1 { do''8\f do''4 do'' | reb''1 | do''4 }
  \tag #'violino2 { mib'8-\sug\f mib'4 mib' | sol'1 | do'4 }
>> r4 r2 |
r2 <<
  \tag #'violino1 { si'4 }
  \tag #'violino2 { re' }
>> r4 |
R1 |
r4 <sol' sol>8.\f q16 q4 q |
do''8 sol' mib'' do'' re'' sol' sol'' re'' |
do'' do' do'16 re' mib' re' do'8 sol'' sol'' sol'' |
lab'' lab'' \grace sol''16 fa''8 mib''16 fa'' re''4 <re'' si''>8 q |
<mib'' do'''>8 do'16 si do' si do' si do'8 do'' do'' re'' |
mib'' mib' mib'' mib'' mib'' re'' do'' sib' |
la' do' fa' la' do''16 la' do'' la' do'' la' do'' la' |
<<
  \tag #'violino1 {
    fa''8 \tuplet 3/2 { sol''16 fa'' mib'' } fa''8 fa'' fa''2:8 |
    re''4
  }
  \tag #'violino2 {
    \rt#4 { do''16 la' } \rt#4 { do''16 la' } |
    sib'4
  }
>> <sib' sib''>2\ff fa''4 |
re'' sib' re'' fa'' |
re''8 sib' sib' sib' sib' sib sib' sib' |
<<
  \tag #'violino1 { re''8 mib'' fa'' sol'' lab''4. sib''8 | sol'' }
  \tag #'violino2 { sib'8 do'' re'' mib'' fa'' mib'' re'' re'' | mib'' }
>> <mib' sol>8 q q q <sol' mib''>[ q q] |
<mib'' sol'>4\f( sib'8) <<
  \tag #'violino1 {
    sol''8 sol''4(\f mib''8) sib'' |
    sib''\< sib''4 sib'' sib'' sib''8~ |
    sib'' <mib'' mib'''>4 q q q8 |
    q2\!
  }
  \tag #'violino2 {
    sib'8 sib'4-\sug\f( sol'8) <sib' sol''> |
    q8\< q4 q q q8~ |
    q q4 q q q8 |
    q2\!
  }
>> r2 |
R1 |
r2 <do'' mib'>4 r |
R1 |
q4 r r2 |
r8. <<
  \tag #'violino1 { lab''16\f lab''8. lab''16 lab''4 }
  \tag #'violino2 { <reb'' fa'>16-\sug\f q8. q16 q4 }
>> r4 |
R1 |
r8 r16 <reb''? sib''>16 q4 r8 r16 <dob''' mib''>16 q4 |
r2 r4 reb' |
