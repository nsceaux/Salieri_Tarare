\clef "alto" R1*31 |
< \tag #'tr1 mib' \tag #'tr2 sib \tag #'tr3 sol >1-\sug\ff |
q~ |
q4 r r2\fermata |
R1*17 |
< \tag #'tr1 mi' \tag #'tr2 dod' \tag #'tr3 sib >1\fp |
< \tag #'tr1 fa' \tag #'tr2 re' \tag #'tr3 la >1\fp |
< \tag #'tr1 fa' \tag #'tr2 re' \tag #'tr3 la >1\fp |
<<
  { < \tag #'tr1 mi' \tag #'tr2 do' \tag #'tr3 la >1~ |
    < \tag #'tr1 mi' \tag #'tr2 si \tag #'tr3 sold >1 | }
  { s4 s2.\< | s4 s2.\! }
>>
la2 r\fermata |
R1*7 |
%% Largo
R1*4 |
R1*18 |
< \tag #'tr1 sib' \tag #'tr2 sol' \tag #'tr3 mib' >1\<~ |
q~ |
q4\! r r2 |
R1*20 |
< \tag #'tr1 sib' \tag #'tr2 sol' \tag #'tr3 mib' >1\<~ |
q~ |
q4\! r r2 |
R1*8 |
