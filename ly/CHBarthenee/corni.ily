\clef "treble" \transposition mib
R1*26 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''1 | mi'' | mi'' | re'' |
    re'' | mi''4 sol''2 sol''4 | sol''1 | sol''4
  }
  { do''1~ | do'' | do'' | re'' |
    sol' | do''4 mi''2 mi''4 | mi''1 | mi''4
  }
  { s1\f\> | s\! | s | s1*2-\sug\pp | s1-\sug\ff }
>> r4 r2\fermata |
R1*23 |
R1*7 |
%% Largo
R1*4 |
R1*9 |
r2 r4\fermata r8 <>-\sug\ff \twoVoices #'(corno1 corno2 corni) <<
  { re''8 |
    sol''2 sol'' |
    sol''1~ |
    sol'' |
    mi''4 re''2 re''4 |
    re''2~ re''4 }
  { sol'8 |
    sol'2 sol' |
    sol'1~ |
    sol' |
    do''4 re''2 re''4 |
    sol'2~ sol'4 }
>> r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol''1 | mi'' | mi''2 mi'' | sol''1~ |
    sol''~ | sol''4 }
  { sol'1 | do'' | do''2 do'' | mi''1~ |
    mi''~ | mi''4 }
  { s1*3 | s1*2\< | s4\! }
>> r4 r2 | \allowPageTurn
R1*14 |
r4 <>-\sug\ff \twoVoices #'(corno1 corno2 corni) <<
  { sol''2 }
  { sol' }
>> r4 |
R1 |
r8 \twoVoices #'(corno1 corno2 corni) <<
  { sol''8 sol'' sol'' sol''2:8 |
    sol''1 |
    mi'' |
    mi''1 |
    sol''~ |
    sol''~ |
    sol''2 }
  { sol'8 sol' sol' sol'2:8 |
    sol'1 |
    do'' |
    do''1 |
    mi''~ |
    mi''~ |
    mi''2 }
  { s2.. | s1*2 | s1-\sug\f | s1*2\< | s4\! }
>> r2 |
R1*8 |
