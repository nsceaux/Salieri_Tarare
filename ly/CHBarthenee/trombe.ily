\clef "treble" R1*13 |
r4 r8 <>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { do'8 mi'4 sol' |
    do''2 do''4 re'' |
    sol' do''8. do''16 do''4 mi'' |
    re''4. re''8 re''4 re'' |
    mi'' do'8. do'16 do'4\fermata }
  { do'8 mi'4 sol' |
    do''2 do''4 re'' |
    sol' mi'8. mi'16 mi'4 do'' |
    do''4. do''8 sol'4 sol' |
    do'' do'8. do'16 do'4\fermata }
>> r4 |
R1*3 |
r2 r8.\fermata <>-\sug\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''16 re''8. re''16 | sol''2 }
  { re''16 re''8. re''16 | sol'2 }
>> r2 |
R1 |
r4 r8 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8 re''4 re'' | sol'2 }
  { re''8 re''4 re'' | sol'2 }
>> r2\fermata |
R1*25 |
<>^"a 2" mi''1\fp |
re''\fp |
re''-\sug\fp |
<< { mi''1~ | mi'' | } { s4 s2.\< | s4 s2.\! } >>
\twoVoices #'(tromba1 tromba2 trombe) <<
  { mi'2 }
  { mi' }
>> r\fermata | \allowPageTurn
R1*7 |
R1*56 |
