\clef "alto" <>-\sug\ff <do sol mi' do''>4 r8\fermata r |
r16 mi'-\sug\ff mi'4\fermata r8 |
r16 fa'\noBeam fa'8\fermata~ fa'16 fad' fad' fad' |
sol'4. sol8 |
do'4 r8\fermata r |
r16 sold'-\sug\f sold'4\fermata r8 |
r16 mi'\f mi' mi' mi'4:16 |
mi'4. mi'8 |
la'4 r8\fermata fa' |
sol'\p sol' mi' mi' |
fa' fa' re' re' |
sib sib si si |
do'\cresc do' do' do' |
dod'2:8 |
re'8 re' sib sib |
sol sol la la |
re'2:8\cresc |
red':8 |
mi'8\f mi' do' do' |
la la si si |
mi'4 r8\fermata r |
r16 re'-\sug\ff re'4 r8\fermata |
r16 do' do'4\fermata do''16. re''32 |
mi''8. mi'16 fa'8 sol' |
la'4 r16\fermata r8. |
r16 sol'\f sol'4.\fermata |
r16 do' do'4\fermata do'8 |
fa'8. fa'16 sol'8 sol |
do'4 do'8 do' |
do'4 r |
