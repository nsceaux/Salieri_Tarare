\clef "bass" r4^"Chanté" r16\fermata do' do' do' |
mi8 mi\fermata r16 do' do' do' |
fa8 fa8.\fermata fad16 fad fad |
sol8. si16 re'8 sol' |
mi'4 r16\fermata do' re' mi' |
sold8 sold\fermata sold la16 si |
mi8 mi r mi |
mi' mi'16 mi' mi'8 mi'16 mi' |
la4 r16 la la la |
sib8 sib sib sib16 sib |
la8 la r16 la la la |
sol8. sol16 sol8 sol |
sol8. sol16 do'8. do'16 |
sib8 sib16 sib sib8. sib16 la8 la re'16 re' re' re' |
sol4 la8. la16 |
re4 r16 la la re' |
do'8 do'16 do' do'8. do'16 |
si8 si r16 mi' mi' mi' |
fad8. fad16 si8. si16 |
mi4 r16\fermata <>^\markup\italic plus appuyé sol sol sol |
si8 si r16\fermata re' si sol |
do'8 do' r\fermata do'16 re' |
mi'8. mi16 fa8 sol16 sol |
la4 r16\fermata la la la |
si8 si r16\fermata re' si sol |
do'8 do' r\fermata do'16 do' |
re'8. re'16 mi'8 sol16 sol |
do'4 r |
R2 |
