\clef "treble" <>-\sug\ff <sol mi' do''>4 r8\fermata r |
r16 mi'\ff mi'4\fermata r8 |
r16 fa'\noBeam fa'8\fermata~ fa'16 fad' fad' fad' |
sol'8. si'16 re''8 <re' si' sol''> |
<sol mi' do'' mi''>4 r8\fermata r |
r16 sold'\f sold'4\fermata r8 |
r16 mi'32(\f red' mi' red' mi' red' mi' red' mi' red' mi' red' mi' red') |
mi'16 fad'32 sold' la' si' dod'' red'' <<
  \tag #'violino1 {
    mi'' fad'' sold'' la'' si'' dod''' red''' mi''' |
    la''4 r8\fermata la' |
    sib'16.\p sib'32 sib'16. sib'32 sib'16. sib'32 sib'16. sib'32 |
    la'16. la'32 la'16. la'32 la'16. la'32 la'16. la'32 |
    sol'16. sol'32 sol'16. sol'32 sol'16. sol'32 sol'16. sol'32 |
    sol'16.\cresc sol'32 sol'16. sol'32 do''16. do''32 do''16. do''32 |
    sib'16. sib'32 sib'16. sib'32 sib'16. sib'32 sib'16. sib'32\! |
    la'16. la'32 la'16. la'32 re''16. re''32 re''16. re''32 |
    re''16. re''32 re''16. re''32 dod''16. dod''32 dod''16. dod''32 |
    re''16. re''32 re''16. re''32 re''16.\cresc re''32 re''16. re''32 |
    do''16. do''32 do''16. do''32 do''16. do''32 do''16. do''32 |
    si'16.\f si'32 si'16. si'32 mi''16. mi''32 mi''16. mi''32 |
    mi''16. mi''32 mi''16. mi''32 red''16. red''32 red''16. red''32 |
    mi''4
  }
  \tag #'violino2 {
    mi''8 <mi' si' sold''> |
    <mi' do'' la''>4 r8\fermata la'~ |
    la'16.-\sug\p la'32 la'16. la'32 sol'16. sol'32 sol'16. sol'32 |
    sol'16. sol'32 sol'16. sol'32 fa'16. fa'32 fa'16. fa'32 |
    fa'16. fa'32 fa'16. fa'32 fa'16. fa'32 fa'16. fa'32 |
    mi'16.-\sug\cresc mi'32 mi'16. mi'32 mi'16. mi'32 mi'16. mi'32 |
    sol'16. sol'32 sol'16. sol'32 sol'16. sol'32 sol'16. sol'32\! |
    sol'16. sol'32 sol'16. sol'32 fa'16. fa'32 fa'16. fa'32 |
    mi'16. mi'32 mi'16. mi'32 mi'16. mi'32 mi'16. mi'32 |
    fa'16. fa'32 fa'16. fa'32 fa'16.-\sug\cresc fa'32 fa'16. fa'32 |
    la'16. la'32 la'16. la'32 la'16. la'32 la'16. la'32 |
    la'16.-\sug\f la'32 la'16. la'32 sol'16. sol'32 sol'16. sol'32 |
    fad'16. fad'32 fad'16. fad'32 fad'16. fad'32 fad'16. fad'32 |
    mi'4
  }
>> r16\fermata r8. |
<<
  \tag #'violino1 {
    r16 si''16-\sug\ff si''4 r8\fermata |
    r16 do''' do'''4\fermata do'''16. re'''32 |
    mi'''8.
  }
  \tag #'violino2 {
    r16 fa''-\sug\ff fa''4 r8\fermata |
    r16 mi'' mi''4\fermata do''16. re''32 |
    mi''8.
  }
>> mi'16 fa'8 sol' |
la'4 r16\fermata r8. |
<re'' si''>2*1/4:32\fp\fermata s4.-\tag #'violino1 _\markup\italic avec la voix |
<mi'' do'''>2:32\fp\fermata |
<re'' do'''>4:32\fp <re'' si''>:32 |
<mi'' do'''>8*1/2 r16 sol'32 fa' mi' re' do'8 do' |
do'4 r |
