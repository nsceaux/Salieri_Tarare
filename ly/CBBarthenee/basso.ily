\clef "bass" do4\ff r8\fermata r |
r16 mi-\sug\ff mi4\fermata r8 |
r16 fa\noBeam fa8\fermata~ fa16 fad fad fad |
sol4. sol,8 |
do4 r8\fermata r |
r16 sold-\sug\f sold4\fermata r8 |
r16 mi\f mi mi mi4:16 |
mi4. mi8 |
la4 r8\fermata fa |
sol\p sol mi mi |
fa fa re re |
sib, sib, si, si, |
do\cresc do do do |
dod2:8 |
re8 re sib, sib, |
sol, sol, la, la, |
re2:8\cresc |
red:8 |
mi8\f mi do do |
la, la, si, si, |
mi4 r8\fermata r |
r16 re-\sug\ff re4 r8\fermata |
r16 do do4\fermata do'16. re'32 |
mi'8. mi16 fa8 sol |
la4 r16\fermata r8. |
r16 sol\f sol4.\fermata |
r16 do do4\fermata do8 |
fa8. fa16 sol8 sol, |
do4 do8 do |
do4 r |
