\clef "treble"
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 }
  { mi' }
>> r8\fermata r |
r16 mi'\ff mi'4\fermata r8 |
r16 fa'\noBeam fa'8\fermata~ fa'16 fad' fad' fad' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'4. sol''8 | mi''4 }
  { sol'4. si'8 | do''4 }
>> r8\fermata r |
r16 sold'-\sug\f sold'4\fermata r8 |
r mi'4-\sug\f mi'8~ |
mi'4. \twoVoices #'(oboe1 oboe2 oboi) <<
  { sold''8 | la''4 }
  { si'8 | do''4 }
>> r8\fermata r |
R2*3 |
r4 <>\mf \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 | sib'2 | la'4 re''~ | re'' dod'' |
    re''2 | do'' | si'4 mi'' | mi'' red'' | mi'' }
  { mi'4 | sol'2~ | sol'4 fa' | mi'2 |
    fa' | la' | la'4 sol' | fad'2 | sol'4 }
  { s4 | s2*3 | s2-\sug\cresc | s2 | s2-\sug\f }
>> r16\fermata r8. |
\grace s8 \ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa'2\fermata | mi'4.\fermata }
  { re'2\fermata | do'4.\fermata }
>> do''16. re''32 |
mi''8. mi'16 fa'8 sol' |
la'4 r16\fermata r8. |
r16 \twoVoices #'(oboe1 oboe2 oboi) <<
  { si''16 si''4.\fermata }
  { re''16 re''4.\fermata }
>>
r16 \twoVoices #'(oboe1 oboe2 oboi) <<
  { do'''16 do'''4.\fermata |
    do'''4 si'' |
    do'''4 do'8 do' |
    do' }
  { mi''16 mi''4.\fermata |
    re''2 |
    do''4 do'8 do' |
    do'4 }
>> r4 |
