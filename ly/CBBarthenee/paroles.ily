Qu’il faut com -- bat -- tre,
qu’il faut a -- bat -- tre
un en -- ne -- mi pré -- somp -- tu -- eux :
le sol a -- ri -- de
de la Tor -- ri -- de
a soif de son sang o -- di -- eux.
Par des me -- su -- res
promp -- tes et sû -- res,
que l’ar -- mée ait un com -- man -- dant,
vail -- lant, fi -- dè -- le,
rem -- pli de zè -- le :
mais sur ce de -- voir im -- por -- tant,
que le ca -- pri -- ce
de ta mi -- li -- ce
ne rè -- gle point le choix d’A -- tar :
que le mur -- mu -- re,
comme une in -- ju -- re,
soit pu -- ni d’un coup de poi -- gnard.
Que le mur -- mu -- re,
comme une in -- ju -- re,
soit pu -- ni d’un coup de poi -- gnard.
