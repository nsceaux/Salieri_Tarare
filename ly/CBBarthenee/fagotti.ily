\clef "bass" do4\ff r8\fermata r |
r16 mi-\sug\ff mi4\fermata r8 |
r16 fa\noBeam fa8\fermata~ fa16 fad fad fad |
sol4. sol,8 |
do4 r8\fermata r |
r16 sold-\sug\f sold4\fermata r8 |
r8 mi4-\sug\f mi8~ |
mi4. mi8 |
la,4 r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { la8 | sib2 | la | sol |
    sol4 do' | sib2 | la4 re'~ | re' dod' |
    re'2 | do' | si4 mi' | mi' red' | mi' }
  { la8~ | la4 sol | sol fa | fa2 |
    mi4 mi | sol2~ | sol4 fa | mi2 |
    fa | la~ | la4 sol | fad2 | mi4 }
  { s8 | s2-\sug\p | s2*2 | s4-\sug\cresc s-\sug\mf | s2*2 |
    s2-\sug\cresc | s2*2 | s2-\sug\f }
>> r16\fermata r8. |
\grace s8\ff \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { fa2\fermata | mi4.\fermata }
  { re2\fermata | do4.\fermata }
>> do'16. re'32 |
mi'8. mi16 fa8 sol |
la4 r16\fermata r8. |
r16 sol, sol,4.\fermata |
r16 do do4.\fermata |
fa4 sol8 sol, |
do4 do8 do |
do4 r |
