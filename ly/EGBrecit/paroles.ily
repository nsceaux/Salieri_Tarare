\tag #'(recit basse) {
  Ur -- son, ex -- pli -- quez- vous.
  
  Le sul -- tan a -- gi -- té,
  sur l’ef -- fet d’un cour -- roux qu’il a trop é -- cou -- té,
  veut que l’af -- freux mu -- et soit mas -- so -- lé,
  je -- té dans la mer, et pour sé -- pul -- tu -- re,
  y serve aux mons -- tres de pâ -- tu -- re.
  
  Le voi -- ci : de sa mort, Ur -- son, je prends le soin.
  Les jar -- dins du sé -- rail sont com -- mis à ma gar -- de ;
  mes eu -- nu -- ques sont prêts.
  
  Pour que rien ne re -- tar -- de,
  son ordre est que j’en sois té -- moin.
  Mar -- chez sol -- dats, qu’on s’en em -- pa -- re.

  Ce n’est point un mu -- et.

  Quel qu’il soit.
  
  C’est Ta -- ra -- re.

  Ta -- ra -- re !…
  
  Ta -- ra -- re !
}

Ta -- ra -- re !

\tag #'(recit basse) {
  Un tel cou -- pable, Ur -- son, de -- vient trop im -- por -- tant,
  pour qu’on l’o -- se frap -- per sans l’or -- dre du sul -- tan.
  
  En sus -- pen -- dant leurs coups, je te sau -- ve peut- ê -- tre.
  
  Ta -- rare in -- for -- tu -- né ! qui peut le dé -- sar -- mer ?
  Nos lar -- mes con -- tre toi vont en -- cor l’a -- ni -- mer !
}

Ta -- rare in -- for -- tu -- né ! qui peut le dé -- sar -- mer ?
Nos lar -- mes, con -- tre toi, vont en -- cor l’a -- ni -- mer !

\tag #'(recit basse) {
  Ne plai -- gnez point mon sort, res -- pec -- tez vo -- tre maî -- tre ;
  puis -- siez- vous un jour l’es -- ti -- mer,
  puis -- siez- vous un jour l’es -- ti -- mer !
  
  Cal -- pi -- gi, songe à toi ; la foudre est sur deux tê -- tes.
}
