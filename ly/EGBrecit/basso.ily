\clef "bass" R1*2 |
mi!4 r r2 |
R1 |
fa4 r r2 |
r2 si,!4 r |
R1 |
r2 do4 r |
r2 r4 re |
sol, r r2 |
r sol4 r |
fa r r2 |
r2 r4 re |
sold r la r |
r2 sol!4 r |
r2 r4 fad |
re r r2 |
r sol4 r |
R1 |
r2 r16 do\ff re mi fa sol la si |
do'4 r r2 |
do2:8\p do:8 |
do4 r r2 |
R1 |
fad4 r r2 |
R1 |
sol4 r r2 |
R1 |
r2\fermata mib2\p |
lab8 do'([\mf lab fa]) mi2\p |
fa4 r fa8 r fa r |
mib r mib r lab, r lab, r |
sol,4 r sol sol, |
do8 \once\slurDashed do'([\sf mib' do']) lab!2 |
sol4 r sol8 r re r |
mib r do r re r re r |
sol8\f sib( la sol) fad(\p re mi fad) |
sol4 sol8.\f sol16 re4 re8. re16 |
mib8 mib16. mib32 mib8 mib mib4 r |
lab2(\p mi) |
fa2. sib,4\f |
mib!4. lab8\p sib4 sib, |
mib2\f sib, |
mib8 sib([ sol mib]) sib,4. sib,8\p |
mib8. mib16 mib8 mib mib2 |
R1*2 |
