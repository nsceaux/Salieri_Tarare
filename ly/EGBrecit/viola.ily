\clef "alto" R1*2 |
mi'!4 r r2 |
R1 |
fa'4 r r2 |
r2 si!4 r |
R1 |
r2 do'4 r |
r2 r4 re' |
sol r r2 |
r mi'!4 r |
re' r r2 |
r2 r4 re'' |
mi'! r mi' r |
r2 la'4 r |
r2 r4 fad' |
re' r r2 |
r sol'4 r |
R1 |
r2 r16 do'-\sug\ff re' mi' fa' sol' la' si' |
do''4 r r2 |
do'8\p do do do do2:8 |
do4 r r2 |
R1 |
re'4 r r2 |
R1 |
sol'4 r r2 |
R1 |
r2\fermata mib'2-\sug\p~ |
mib'8 do'([\mf lab fa]) mi2\p |
fa4 r r16 sol' sol' sol' r sol' sol' sol' |
r sol' sol' sol' r sol' sol' sol' r do' do' do' r do' do' do' |
r re' re' re' re'8 r sol4 sol |
do'8 do'([-\sug\sf mib' do']) lab2 |
sol4 r r16 sol' sol' sol' r do' do' do' |
r sib sib sib r sol' sol' sol' r sol' sol' sol' r fad' fad' fad' |
sol8 re'4-\sug\f re' re'-\sug\p re'8 |
sol'4 sol'8.-\sug\f sol'16 re'4 re'8. re'16 |
mib'8 sib16. sib32 sib8 sib sib4 r |
r8 mib'-\sug\p mib' mib' r sol' sol' sol' |
r do' do' do' do'4 fa'8.-\sug\f fa'16 |
mib'8. mib'16 mib'8.-\sug\p mib'16 mib'4 re'8. re'16 |
mib'8 sol([-\sug\f sib mib']) re'( lab' fa' re') |
mib' sib'([ sol' mib']) sib4. sib8-\sug\p |
sib8. mib16 mib8 mib mib2 |
R1*2 |
