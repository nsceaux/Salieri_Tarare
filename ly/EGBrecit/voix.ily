<<
  \tag #'(recit basse) {
    \clef "alto/G_8" <>^\markup\character Calpigi _\markup\center-align Parlé
    r4 r8 sib mib' sib sib sib |
    sol4
    \ffclef "bass" <>^\markup\character Urson
    sol8 sol sol4 sol8 sol |
    do'4 do'8 do' do'4 do'8 reb' |
    sib4 sol8 lab sib4 sib8 do' |
    lab4 r do'8 do'16 do' do'8 do' |
    lab4 r sol r8 sol16 la! |
    si!8 r si si do'16 re' sol4 r8 |
    re'8 re' si sol do' do' r16 do' do' mib' |
    do'8 do' do' sib sol sol r4 |
    \ffclef "alto/G_8" <>^\markup\character-text Calpigi se met entre eux et Tarare
    r4 r8 re'16 re' sib4 r8 sib16 do' |
    re'4. mi'8 dod'4 r16 dod' dod' re' |
    la4 r8 la16 la la4 la8 la |
    re'8 re'16 re' fa'8 mi'16 fa' re'8 re' r4 |
    r8 mi'16 mi' mi'8 re'16 mi' do'4
    \ffclef "bass" <>^\markup\character Urson
    r8 la16 la |
    la4 la8 si dod' dod' r mi'16 mi' |
    dod'4 la16 si dod' re' la4 r |
    r r8 la re'4. la8 |
    fad4 r16 re' do' re' si8 si
    \ffclef "alto/G_8" <>^\markup\character Calpigi
    r8 re'16 re' |
    re'4 re'8 re' sol'4
    \ffclef "bass" <>^\markup\character Urson
    r8 si16 re' |
    sol4
    \ffclef "alto/G_8" <>^\markup\character Calpigi
    r8 sol' sol' mi' mi'
    \ffclef "bass" <>^\markup\character-text Urson chanté
    mi |
    sol4 sol8
    \ffclef "bass" <>^\markup\character Soldats
    sol8 do'4 do'8 do' |
    sol1\fermata |
    sol4
    \ffclef "alto/G_8" <>^\markup\character-text Calpigi d’un ton réfléchi ^"Parlé"
    r8 sol sol sol sol sol |
    do'4 do'8 do' do'4 re'8 mib' |
    re'4 r8 re'16 re' re'4 re'8 re' |
    la4. la8 do' do' do' re' |
    sib4 r8 <>^\markup\italic { à Tarare (bas) } sol8 sol sol sol la |
    sib4 r8 re'16 re' sib4 sib8 re' |
    sol sol
    \ffclef "bass" <>^\markup\character-text Urson avec douleur ^"Chanté"
    r8 sib sib8. sol16 mib'8. re'16 |
    do'4 r8 do' do'8. do'16 reb'8 sol |
    lab4 r8 do' si si r re'16 sol |
    do'4 do'8 do' mib'4 re'8 do' |
    sol4
    \ffclef "bass" <>^\markup\character-text Soldats ton pénétré
    r8 sol sol8. sol16 sol8. sol16 |
    do'4 r8 do' lab!8. lab16 lab8 lab |
    sol4 r8 sol sol sol r re16 re |
    mib4 do8 do re4 fad8. fad16 |
    sol4
    \ffclef "tenor/G_8" <>^\markup\character-text Tarare avec dignité
    r8 sol re'8. re'16 re'8. re'16 |
    sib4 sib8. sib16 fa'4 fa'8. fa'16 |
    sol'8 mib' r4 r mib'8. mib'16 |
    do'4. do'8 reb'4 reb'8. reb'16 |
    do'2 r4 re'!8. re'16 |
    mib'4. do'8 sib4 sib8. sib16 |
    mib4 r r2 |
    R1 |
    \ffclef "bass" <>^\markup\character-text Urson \line {
      \normal-text Récit bas à Calpigi
    } _\markup\italic "(Parlé)"
    r2 r4 sol8. lab16 |
    sib4 sib8 sib sol4 r8 mib |
    sol sol lab sib mib mib r4^\markup\right-align\line\italic { (il sort) } |
  }
  \tag #'vhaute-contre {
    \clef "alto/G_8" R1*20 |
    r2 <>^\markup\character Chœur des Eunuques
    r4 r8 do' |
    mib'1\fermata |
    mib'4 r4 r2 |
    R1*9 |
    r4 r8 re'^\p re'8. re'16 sol'8. fa'16 |
    mib'4 r8 mib' fa'8. fa'16 fa'8 do' |
    re'4 r8 re' re' re' r fad'16 fad' |
    sol'4 mib'8 mib' re'4 re'8. re'16 |
    sib4 r r2 |
    R1*10 |
  }
  \tag #'vtaille {
    \clef "tenor/G_8" R1*20 |
    r2 <>^\markup\italic { ils reculent tous par respect }
    r4 r8 do' |
    do'1\fermata |
    do'4 r4 r2 |
    R1*9 |
    r4 r8 si si8. si16 si8. si16 |
    do'4 r8 do' do'8. do'16 do'8 do' |
    si4 r8 sib sib sib r la16 la |
    sol4 do'8 do' sib4 la8. la16 |
    sol4 r r2 |
    R1*10 |
  }
>>
