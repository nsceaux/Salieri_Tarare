\clef "bass" R1*28 |
r2\fermata <>-\sug\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib2 | do'4 }
  { sol2 | lab4 }
>> r4 <>-\sug\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'4 sib | lab }
  { sol2 | fa4 }
>> r4 r2 |
R1 |
\clef "tenor" r2 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'2 | mib' fa' | re' }
  { si | do' do' | si }
>> r2 |
R1 |
\clef "bass" sol8\f sib( la sol) fad(\p re mi fad) |
sol4 sol8.\f sol16 re4 re8. re16 |
mib8 mib16. mib32 mib8 mib mib4 r |
R1*3 |
<>-\sug\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol2 lab | }
  { mib fa | }
>>
\clef "tenor" \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol8 }
  { mib }
>> r r4 r8 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib4 re'8 | mib'8. sib16 sib8 sib sib2 | }
  { sib4 lab8 | sol8. sol16 sol8 sol sol2 | }
  { s4 s8-\sug\p }
>>
R1*2 |
