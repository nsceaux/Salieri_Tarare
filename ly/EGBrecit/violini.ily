\clef "treble" R1*2 |
<<
  \tag #'violino1 {
    do''4 r r2 |
    R1 |
    do''4 r r2 |
    r2 re''4 r |
    R1 |
    r2 mib''4 r |
  }
  \tag #'violino2 {
    sol'4 r r2 |
    R1 |
    lab'4 r r2 |
    r2 sol'4 r |
    R1 |
    r2 sol'4 r |
  }
>>
r2 r4 <re' la' fad''>4 |
<re' sib' sol''>4 r r2 |
<<
  \tag #'violino1 {
    r2 dod''4 r |
    re'' r r2 |
    r2 r4 fa'' |
    mi''! r mi'' r |
    r2 mi''4 r |
    r2 r4 <re' la' fad''> |
    <re' la' la''>4 r r2 |
  }
  \tag #'violino2 {
    r2 la'4 r |
    la' r r2 |
    r2 r4 la' |
    si'! r do'' r |
    r2 dod''4 r |
    r2 r4 <re' la'> |
    <re' la' fad''>4 r r2 |
  }
>>
r2 <re' si' sol''>4 r |
R1 |
r2 r16 do''\ff re'' mi'' fa'' sol'' la'' si'' |
do'''4 r r2 |
<<
  \tag #'violino1 {
    mib''16\p <mib' sol> q q q4:16 q2:16 |
    q4 r4 r2 |
    R1 |
    re''4 r r2 |
    R1 |
    re''4 r r2 |
    R1 |
    r4\fermata r8. sib'16\p sib'8. sib'16 sib'8. sib'16 |
    do''4 r r8 do''([-\sug\p reb''8. sol'16]) |
    lab'4 r r16 si'! si' si' r si' si' si' |
    r16 do'' do'' do'' r do'' do'' do'' r do'' do'' do'' r do'' do'' do'' |
    r si' si' si' si'8. re''16 re''8. re''16 sol''8. fa''16 |
    mib''4 r8 do'' fa''8. fa''16 fa''8. do''16 |
    re''4 r r16 re'' re'' re'' r fad'' fad'' fad'' |
    r sol'' sol'' sol'' r mib'' mib'' mib'' r re'' re'' re'' r re'' re'' re'' |
    re''8 re'4\f re'' re''\p re''8 |
    sib'4 sib'8.\f sib'16 fa''4 fa''8. fa''16 |
    sol''8
  }
  \tag #'violino2 {
    sol'16-\sug\p do' do' do' do'4:16 do'2:16 |
    do'4 r r2 | \allowPageTurn
    R1 |
    la'!4 r r2 |
    R1 |
    sib'4 r r2 |
    R1 |
    r4\fermata r8. sol'16-\sug\p sol'8. sol'16 sol'8. sol'16 |
    lab'4 r sol'2-\sug\p |
    do'4 r r16 re' re' re' r re' re' re' |
    r do' do' do' r do' do' do' r fad' fad' fad' r fad' fad' fad' |
    r sol' sol' sol' sol'8. si'16 si'8. si'16 si'8. si'16 |
    do''4 r8 do'' do''8. do''16 do''8. do''16 |
    si'4 r r16 sib' sib' sib' r la' la' la' |
    r sol' sol' sol' r do'' do'' do'' r sib' sib' sib' r la' la' la' |
    sib'8 re''([-\sug\f do'' sib']) la'(-\sug\p fad' sol' la') |
    re'4 re'8.-\sug\f re'16 sib'4 sib'8. sib'16 |
    sib'8
  }
>> <mib' sol>16. q32 q8 q q4 r |
<<
  \tag #'violino1 {
    r8 do''\p do'' do'' r reb'' reb'' reb'' |
    r do'' do'' do'' do''4 re''!8.\f re''16 |
    mib''8. mib''16 mib''8.\p do''16 sib'8 sib4 sib8 |
    sol \once\slurDashed sol'([\f sib' mib'']) re''( lab'' fa'' re'') |
    mib''
  }
  \tag #'violino2 {
    r8 lab'-\sug\p lab' lab' r sib' sib' sib' |
    r lab' lab' lab' lab'4 lab'8.-\sug\f lab'16 |
    sol'8. sol'16 sol'8.-\sug\p lab'16 sol'4 fa'8. fa'16 |
    <mib' sol>2-\sug\f <re' sib'> |
    sol'8
  }
>> sib'([ sol' mib']) sib4. <<
  \tag #'violino1 {
    re'8\p | mib'8. sib16 sib8 sib sib2 |
  }
  \tag #'violino2 {
    lab8-\sug\p | sol8. sol16 sol8 sol sol2 |
  }
>>
R1*2 |
