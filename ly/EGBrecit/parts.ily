\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (fagotti #:tag-notes fagotti #:score-template "score-part-voix"
            #:music , #{
\quoteBasso "EGBbasso"
s1*2 <>^\markup\tiny Basso
\cue "EGBbasso" { \mmRestDown s1 \mmRestCenter }
s1
\cue "EGBbasso" { \mmRestDown s1*2 \mmRestCenter }
s1
\cue "EGBbasso" { \mmRestDown s1*11 \mmRestCenter }
s1
\new CueVoice {
  \mmRestDown r2 r16 do\ff re mi fa sol la si |
  do'4 r r2 |
  do2:8\p do:8 |
  do4 r r2 | \mmRestCenter \allowPageTurn
}
s1
\cue "EGBbasso" { \mmRestDown s1 \mmRestCenter }
s1
\cue "EGBbasso" { \mmRestDown s1 \mmRestCenter }
s1\allowPageTurn
s1*3
\cue "EGBbasso" { \mmRestUp s1 \clef "bass" s2 \mmRestCenter }
\clef "tenor" s2 s1 s2 \clef "bass"
\cue "EGBbasso" { \mmRestUp s2 s1 \mmRestCenter }
s1*3
\new CueVoice {
  \mmRestDown lab2(\p mi) |
  fa2. sib,4\f |
  mib!4. lab8\p sib4 sib, |
  \mmRestCenter
}
                        #})
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
