\clef "bass" r8 |
R1 |
r8. mib16\ff mib8. mib16 mi4 r |
R1 |
r2 do4 r |
r2 r8 fa,\f lab, do |
fa4 r r2 |
R1 |
sol,1\p |
sol,-\sug\fp |
do2~ do~ |
do fad~ |
fad sol~ |
sol sol |
r2 r4 la |
