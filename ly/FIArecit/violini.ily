\clef "treble" r8 |
R1 r8. <<
  \tag #'violino1 {
    mib''?16\ff mib''8. reb''16 do''4
  }
  \tag #'violino2 <<
    { sib'16 sib'8. sib'16 sib'4 } \\
    { sol'16-\sug\ff sol'8. sol'16 sol'4 }
  >>
>> r4 |
R1 |
r2 <<
  \tag #'violino1 { mi''!4 }
  \tag #'violino2 << sib'4 \\ sol' >>
>> r4 |
r2 r8 fa'16.[\f fa'32] lab'16.[ lab'32] do''16.[ do''32] |
fa''4 r r2 |
R1 |
<si'! re'>1\p |
<<
  \tag #'violino1 {
    <re' si'>1-\sug\fp |
    <do'' mib'>2~ q~ |
    q <re' do''>~ |
    q <sib' re'>~ |
    q
  }
  \tag #'violino2 {
    <si' sol''>1\fp |
    <do'' sol''>2~ q~ |
    sol'' la''~ |
    la'' sib''~ |
    sib''
  }
>> <mi'! dod''>2 |
r2 r4 <la' mi'' dod'''>4 |
