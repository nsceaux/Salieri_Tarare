\clef "tenor/G_8" <>^\markup\italic Avec majesté sib8 |
mib'2 r8 sib sib sib |
sol4 r r r8 do' |
do' do' do' reb' sib4 r8 sib16 sib |
sib4 sib8 sib mi'!4 mi'8 sol' |
mi'4 mi'8 fa' do' do' r4 |
r r8 do' do' do' do' do' |
fa'4 fa'8 fa' fa'4 re'8 re' |
si!4 r8 re'16 re' re'4 re'8 re' |
sol'4. re'16 mib' fa'4 fa'8 sol' |
mib'8 mib' r4 mib' mib'8 mib' |
mib'4 mib' re'4. re'8 |
re'8 re' fad' re' sol'4 r8 re' |
sol'8 sol' sol' mi'! dod'4 r8 mi'16 mi' |
dod'4 dod'8 re' la4 r |
