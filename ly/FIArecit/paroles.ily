En -- fants, vous m’y for -- cez, je gar -- de -- rai ces fers ;
ils se -- ront à ja -- mais ma roy -- a -- le cein -- tu -- re.
De tous mes or -- ne -- ments de -- ve -- nus les plus chers,
puis -- sent- ils at -- tes -- ter à la ra -- ce fu -- tu -- re
que, du grand nom de roi si j’ac -- cep -- tai l’é -- clat,
ce fut pour m’en -- chaî -- ner au bon -- heur de l’é -- tat !
