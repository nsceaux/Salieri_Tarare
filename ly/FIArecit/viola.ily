\clef "alto" r8 |
R1 |
r8. mib'16-\sug\ff mib'8. mib'16 mi'4 r |
R1 |
r2 do'4 r |
r2 r8 fa-\sug\f lab do' |
fa'4 r r2 |
R1 |
sol1-\sug\p |
sol-\sug\fp |
do'2~ do'~ |
do' fad~ |
fad sol~ |
sol sol |
r2 r4 la |
