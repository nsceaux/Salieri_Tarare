\clef "bass" <>^\markup { Récit \italic { d’un ton profond } } r2 r4 mib |
lab lab8 lab lab4 lab8 do' |
lab lab^\markup\italic a Tempo r4 r lab8 lab |
do'4 do'8. do'16 do'4. do'8 |
reb'8 reb' r4 r reb'8. reb'16 |
mib'4 mib'8. mib'16 mib'4. solb8 |
fa reb r4 r2 |
r fa8. fa16 fa8 fa |
sib4 sib8 sib fa8. fa16 fa8 sol |
lab lab lab sib sol4 r8 sol |
sib4. sib16 sib sib4 do'8 reb' |
mi!8 mi r sol16 sol sol4 sol8 sol |
do'4. sol8 sib sib sib lab |
fa fa r4 r2 |
r2 sol4 sol8 la! |
si!4. si8 si si do' re' |
sol4. sol8 sol8*3/2 re16 fa fa fa sol |
mib4 r <>^\markup\italic { ton sombre et bas } mib4 mib8 fa |
sol sol sol la! fad fad r4 |
fad16 fad fad sol la8 la16 sib sol4 r8 sol16 sib |
la!4 la16 la sol la fa4 r8 re'16 re' |
sold8 sold16 sold sold8 la mi! mi r4 |
R1 |
r2 <>^\markup\italic ton Brillant fa8 fa16 fa fa8 sol |
la4 la8 sib16 do' fa4 r |
r8 do' do' re' sib4 r8 sib |
sib sib do' re' la la r16 la la sib |
do'8 do' do' re' si! si r re'16 re' |
si4 la8 sol do'4 r |
la!8 la16 la si8 do' sol4 r |
R1 |
