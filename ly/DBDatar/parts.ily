\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix" #:tag-notes basso)
   (fagotti #:score-template "score-part-voix"
            #:tag-notes fagotti
            #:notes "basso")
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #}))
