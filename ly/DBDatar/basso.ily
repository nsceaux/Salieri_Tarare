\clef "bass" lab,1\fp~ |
lab, |
<<
  \twoVoices #'(fagotto1 fagotto2 fagotti) <<
    { lab1~ | lab~ | lab | lab | }
    { lab,1~ | lab,~ | lab, | lab, | }
  >>
  \tag #'basso {
    lab,8 lab, lab, lab, lab, lab, lab, lab, |
    lab,8 lab, lab, lab, lab, lab, lab, lab, |
    lab,8 lab, lab, lab, lab, lab, lab, lab, |
    lab,8 lab, lab, lab, lab, lab, lab, lab, |
  }
  \tag #'tutti <<
    \new Voice { \voiceOne lab1~ | lab~ | lab | lab | }
    \new Voice { \voiceTwo lab,1~ | lab,~ | lab, | lab, | }
    \new Voice { \voiceThree \ru#4 { lab,8 lab, lab, lab, lab, lab, lab, lab, | } }
  >>
  { s1*2-\sug\p | s1\cresc | s4\f s2.\cresc | <>\ff }
>>
reb4 reb'16 lab fa lab reb8. reb16 reb8 reb |
re!4 r r2 |
R1 |
r2 mib4\p r |
R1 |
do4 r r2 |
R1 |
r8. do16\f do8 do fa lab16 sol fa mib re do |
si,!4 r r2 |
<<
  \tag #'(fagotto1 fagotto2 fagotti) { R1*6 r2 r4 }
  \tag #'(basso tutti) {
    R1*2
    <>^\markup\italic { Violoncelli soli }
    do1\p~ |
    do2 la,!~ |
    la, sib, |
    dod re~ |
    re r4 <>^\markup\italic tutti
  }
>> mi4\f |
la,8. la16 la8 la sol sol16. sol32 sol8 sol |
fa4 r r2 |
r r4 r16. fa32 fa16. fa32 |
mib2 re4 r |
r2 fad~ |
fad fa~ |
fa mi!4 r |
fa r r sol |
do2 r |
