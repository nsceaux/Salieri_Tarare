\clef "treble" lab1\fp~ |
lab~ |
lab16 <<
  \tag #'violino1 {
    lab'8\p lab' lab' lab' lab' lab' lab' lab'16 |
    do'' do''8 do'' do'' do'' do'' do'' do'' do''16 |
    reb''\cresc reb''8 reb'' reb'' reb'' reb'' reb'' reb'' reb''16 |
    do''\f <do'' lab''>8 q q\cresc q q q q q16 |
    fa''16\ff
  }
  \tag #'violino2 {
    <do' mib'>8-\sug\p q q q q q q q16 |
    <lab solb'>16 q8 q q q q q q q16 |
    <lab fa'>16 q8 q q q q q q q16 |
    mib'16-\sug\f <mib' mib''>8 q q-\sug\cresc q q q q q16 |
    reb''-\sug\ff
  }
>> reb''' lab'' fa'' reb'' lab' fa' lab' reb'8. reb'16 reb'8 reb' |
<re'! sib' sib''>4 r r2 |
R1 |
r2 <<
  \tag #'violino1 { sol''4\p }
  \tag #'violino2 { sib'4-\sug\p }
>> r4 |
R1 |
<<
  \tag #'violino1 { sol''4 }
  \tag #'violino2 { sib' }
>> r4 r2 |
R1 |
r8. do''16\f do''8 do'' fa'' lab''16 sol'' fa'' mib'' re'' do'' |
<re' si'! sol''>4 r r2 |
R1*2 |
<<
  \tag #'violino1 {
    sol'1\p~ |
    sol'2 fad'~ |
    fad' sol'~ |
    sol' fa'~ |
    <fa' sold>2
  }
  \tag #'violino2 {
    mib'1-\sug\p~ |
    mib'2 re'~ |
    re'~ re' |
    mi' re'~ |
    re'
  }
>> r4 <si sold'>4\f |
<do' la'>8. do''16 <do'' mi'>8 q <sib' mi''>8 q16. q32 q8 q |
<fa'' la'>4 r r2 |
r r4 r16. <<
  \tag #'violino1 {
    do''32 do''16. do''32 |
    do''2 sib'4
  }
  \tag #'violino2 {
    <fa' la'>32 q16. q32 |
    q2 fa'4
  }
>> r4 |
r2 <<
  \tag #'violino1 {
    re''2~ |
    re''~ re''~ |
    re''
  }
  \tag #'violino2 {
    la'2~ |
    la' si'~ |
    si'
  }
>> do''4 r |
<do'' la''!>4 r r <sol re' si'>4 |
<sol mi'! do''>2 r |
