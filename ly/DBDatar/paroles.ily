Par -- tout il a donc l’a -- van -- ta -- ge !
Ah ! mon cœur en fré -- mit de ra -- ge !
Ah ! mon cœur en fré -- mit de ra -- ge !
Quand, par le com -- bat, Al -- ta -- mort
vou -- lut hi -- er ré -- gler leur sort,
Ur -- son, je sen -- tais bien d’a -- van -- ce,
qu’il al -- lait de sa mort
pay -- er cette im -- pru -- den -- ce.
Sans les cla -- meurs d’un père é -- pou -- van -- té,
le temple é -- tait en -- san -- glan -- té ;
mais son pou -- voir for -- ça le nô -- tre
d’ar -- rê -- ter un crime op -- por -- tun,
qui m’of -- frait, dans le mort de l’un,
un pré -- tex -- te pour per -- dre l’au -- tre.

Tout le sé -- rail i -- ci por -- te ses pas.
Re -- ti -- re- toi ; que cette af -- freuse i -- ma -- ge,
se dis -- si -- pant comme un nu -- a -- ge,
fas -- se place aux plai -- sirs, et ne les trou -- ble pas.
