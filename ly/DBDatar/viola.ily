\clef "alto" lab1-\sug\fp~ |
lab~ |
lab16 lab8-\sug\p lab lab lab lab lab lab lab16 |
<lab solb'>16 q8 q q q q q q q16 |
<lab fa'>16 q8 q q q q q q q16 |
mib'-\sug\f <mib' do''>8 q q-\sug\cresc q q q q q16 |
reb'16-\sug\ff reb'' lab' fa' reb' lab fa lab reb8. reb'16 reb'8 reb' |
re'!4 r r2 |
R1 |
r2 mib'4-\sug\p r |
R1 |
mi'!4 r r2 |
R1 |
r8. do'16-\sug\f do'8 do' fa' lab'16 sol' fa' mib' re' do' |
si!4 r r2 |
R1*2 |
do'1-\sug\p~ |
do'2~ do'~ |
do' sib |
la~ la |
si! r4 mi'-\sug\f |
la8. la'16 la'8 la' sol' sol'16. sol'32 sol'8 sol' |
fa'4 r r2 |
r r4 r16. fa'32 fa'16. fa'32 |
mib'2 re'4 r |
r2 re'~ |
re' re'~ |
re' sol'4 r |
fa' r r sol' |
do'2 r |
