Ah ! d’une an -- tique ab -- sur -- di -- té,
lais -- sons à l’hin -- dou les chi -- mè -- res.
Brame et Sou -- dan doi -- vent en frè -- res
sou -- te -- nir leur au -- to -- ri -- té.
Tant qu’ils s’ac -- cor -- dent bien en -- sem -- ble,
que l’es -- clave ain -- si gar -- rot -- té,
souf -- fre, o -- bé -- it, et croit, et trem -- ble,
le pou -- voir est en sû -- re -- té.

Dans ta po -- li -- ti -- que nou -- vel -- le,
com -- ment mes in -- té -- rêts sont- ils u -- nis aux tiens ?

Ah ! si ta cou -- ron -- ne chan -- cel -- le,
mon temple, à moi, tombe a -- vec el -- le.
A -- tar, ces fa -- rou -- ches chré -- tiens
au -- ront des Dieux ja -- loux des miens :
ain -- si qu’au trô -- ne, tout par -- ta -- ge,
en fait de culte, est un ou -- tra -- ge.
Pour les domp -- ter, fais que nos In -- di -- ens
pen -- sent que le ciel même a con -- duit nos me -- su -- res :
le nom du chef dont nous se -- rons d’ac -- cord,
je l’in -- si -- nue aux en -- fants des au -- gu -- res.
Qui veux- tu nom -- mer ?

Al -- ta -- mort.

Mon fils !

J’ac -- quitte un grand ser -- vi -- ce.

Que de -- vient Ta -- ra -- re ?

Il est mort.

Il est mort !

Oui, de -- main, j’or -- don -- ne qu’il pé -- ris -- se.

Jus -- te ciel ! crains, A -- tar…

Quoi crain -- dre ? mes re -- mords ?
