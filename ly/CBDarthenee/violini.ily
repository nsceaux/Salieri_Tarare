\clef "treble" <sol sol'>2\f si'4. do''8 |
re''4 re'' re'' re'' |
<sol mi'>1\fp~ |
q4 do''8 si' la'4 la''8 sol'' |
fad''4 re'16\f dod' re' dod' re'4 <re' la' fad''> |
re'1\fermata |
<<
  \tag #'violino1 {
    r4 r8 r16 si'\f si''2~ |
    si''4.. si'16 si''2~ |
    si''4.. si'16 si''2~ |
    si''4.. si'16 si''4.. si''16 |
    mi''2 r8 sol''\mf-! la''-! si''-! |
    do'''4 do''' do''' do''' |
    la''2. la''8. la''16 |
    re'''4 la'' fad'' la'' |
    re'' r \appoggiatura { re'16[ la'] } fad''2\mf |
    \appoggiatura { re'16[ la'] } la''2\cresc \appoggiatura { re'16[ la'] } do'''2\!
    \appoggiatura { re'16[ si'] } si''2\f    
  }
  \tag #'violino2 {
    red'2\fp~ red'4.. red'16\f |
    mi'2-\sug\p~ mi'4.. mi'16\f |
    fad'!2~ fad'4 fad'8 sol' |
    la'4 fad'!8. fad'16 red'4 fad' |
    sol'2 r8 sol'-!-\sug\mf la'-! si'-! |
    do''4 do'' do'' do'' |
    la' fad'16 mi' fad' sol' fad'4 fad' |
    <fad' la>1-\sug\p~ |
    q2 \appoggiatura { la16[ fad'] } re''2-\sug\mf |
    \appoggiatura { re'16[ la'] } fad''2-\sug\cresc \appoggiatura { re'16[ la'] } fad''2 |
    \appoggiatura { re'16[ si'] } sol''2-\sug\f
  }
>> r4\fermata sol'8.\p si'16 |
do'4. do'8 re'4. re'8 |
sol2 r |
R1*3 |
<<
  \tag #'violino1 {
    do''1\p~ |
    do''~ |
    do''2 do''16( fa'-. fa'-. fa'-. fa'4:16\dotFour) |
    fa'2:16\dotEight fa'2:16\dotEight |
    fa'2:16\dotEight fa':16 |
    fa'4
  }
  \tag #'violino2 {
    sol'1-\sug\p~ |
    sol'~ |
    sol'2 la'16( do'-. do'-. do'-. do'4:16\dotFour) |
    do'2:16\dotEight do':16\dotEight |
    do':16\dotEight do':16 |
    do'4
  }
>> r4 r2 |
<<
  \tag #'violino1 { dod''4 }
  \tag #'violino2 { sol' }
>> r4 r2 |
r8 r16 <<
  \tag #'violino1 { re''16\f re'8. re'16 re'4 }
  \tag #'violino2 { <fa' la'>16-\sug\f re'8. re'16 re'4 }
>> r4 |
<<
  \tag #'violino1 {
    sold'1\fp~ |
    sold'~ |
    sold'2 la'~ |
    la'1~ |
    la'~ |
    la'2 sol'~ |
    sol' fa'! |
    <mi' sol>4
  }
  \tag #'violino2 {
    mi'1-\sug\fp~ |
    mi'~ |
    mi'2 mi'~ |
    mi'1( |
    <fad' si'>1)~ |
    q2 <sol' si'>2~ |
    q q |
    <sol' do''>4
  }
>> r4 r2 |
r2 <<
  \tag #'violino1 { <la fa'>4 }
  \tag #'violino2 { <fa' la'>4 }
>> r4 |
R1 |
r4 <<
  \tag #'violino1 { <fa' do'' la''>4 r <re' sib' sib''>4 | }
  \tag #'violino2 { <la' fa''>4 r <re' sib' fa''>4 | }
>>
R1 |
r2 <sib sol'>4 r |
<<
  \tag #'violino1 {
    do''2:16\< do'':16 |
    do'':16 do'':16\! |
    do''4
  }
  \tag #'violino2 {
    sol'2:16\< sol':16 |
    sol':16 sol':16\! |
    fad'4
  }
>> r r2 |
<re' sib'>4 r r2 |
