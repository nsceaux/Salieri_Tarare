\clef "bass" <>^\markup\character-text Arthénée Fièrement ^"chanté"
sol2 si4. do'8 |
re'4 si sol4. fa8 |
mi2 r4 sol |
do'4 do'8 si la4 la8 sol |
fad!4 re r2 |
R1 |
si2 si4. si8 |
si2 si4 si8 si |
si4 si r4 si8 si |
si4 si8 si si4 si |
mi2 r8 sol la si |
do'4. do'8 do'4 do' |
la la r la8 la |
re'4 la fad la8 la |
re2 fad8 fad r fad16 fad |
la4. la8 do'4. re'8 |
si4 si r\fermata sol8 si |
do4. do8 re4 re8 re |
sol,2 r^"parlé" |
\ffclef "bass" <>^\markup\character-text Atar plus surpris
sol8. sol16 sol8 sol sol4 sol8 la |
si si r si re' re' do' re' |
si4 r8 re' si si la sol |
do'4
\ffclef "bass" <>^\markup\character-text Arthénée échauffé
sol4~ sol8 sol sol sol |
sol4 sol8 do' do' sol r16 sol sol sol |
mi4 sib8 sol16 la fa8 fa r\fermata do |
fa4 r8 fa16 fa fa4 fa8 sol |
la4 r16 do' do' do' la8. la16 sib8 do' |
fa4 r16 fa fa sol la8 la r la16 sib |
sol8 sol r16 sol sol sol dod'8 dod' dod' re' |
re4 re r8^"Récit" la la la |
sold4 r8 mi16 fad sold4 sold8 la |
si4 r8 si16 si si4 si8 do' |
re' sold16 la si8 si16 do' la8 la r16 mi mi mi |
la4. la8 la la si do' |
si4 r16 si si si fad4 fad8 sol |
la4 la8 si sol8 sol r4 |
sol8 sol sol la si4
\ffclef "bass" <>^\markup\character Atar
r8 si16 do' |
sol4
\ffclef "bass" <>^\markup\character Arth.
r8 mi sol4
\ffclef "bass" <>^\markup\character Atar
r8 sol |
sib sib sib la fa fa r4 |
\ffclef "bass" <>^\markup\character Arthénée
fa8 fa fa sol la la
\ffclef "bass" <>^\markup\character Atar
r8 la16 do' |
fa4
\ffclef "bass" <>^\markup\character Arthénée
r8 fa16 fa sib4 r |
\ffclef "bass" <>^\markup\character Atar
sib4 r8 sib fa4. fa8 |
lab8 lab lab sol mib mib
\ffclef "bass" <>^\markup\character Arthénée
r8 sol16 sol |
do'2 r |
mib'2. mib'4 |
fad4
\ffclef "bass" <>^\markup\character Atar
r8 fad la la r re'16 la |
sib2 r |
