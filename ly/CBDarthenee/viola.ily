\clef "alto" R1*2 |
do2-\sug\f mi4. sol8 |
do'2 mi' |
la4 re'16-\sug\f dod' re' dod' re'4 re |
re1\fermata |
si2-\sug\fp~ si4.. si16-\sug\f |
dod'2\p~ dod'4.. dod'16\f |
red'2 r4 red'8 mi' |
fad'!4 red' si red' |
mi'2 r |
r2 r8 la-\sug\mf si do' |
re'4 re' re' re' |
re'1-\sug\p~ |
re'2 \appoggiatura { la16[ fad'] } re''2-\sug\mf |
\appoggiatura { la16[ fad'] } re''2-\sug\cresc \appoggiatura { la16[ fad'] } re''2 |
\appoggiatura { sol16[ re'] } re''2-\sug\f r4\fermata sol'4\p |
do'4. do'8 re'4. re'8 |
sol2 r |
R1*3 |
do'1-\sug\p~ |
do'~ |
do'2 do'16( la-. la-. la-. la4:16\dotFour) |
la2:16\dotEight la2:16\dotEight |
la2:16\dotEight la:16 |
la4 r r2 |
mi'4 r r2 |
r8 r16 re'-\sug\f re'8. re'16 re'4 r |
si!1-\sug\fp~ |
si~ |
si2 la~ |
la1 |
red'~ |
red'2 mi'~ |
mi' re' |
do'4 r r2 |
r2 fa'4 r |
R1 |
r4 mib' r re' |
R1 |
r2 mib'4 r |
mib'2:16\< mib':16 |
mib'2:16 mib':16\! |
re'4 r r2 |
re'4 r r2 |
