\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { Bassons Basses }
      shortInstrumentName = \markup\center-column { Bn. B. }
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      { s1*19 \set Staff.shortInstrumentName = "B." }
      \origLayout {
        s1*5\break s1*6\break s1*5\pageBreak
        \grace s8 s1*4\break s1*4\break s1*2 s2 \bar "" \pageBreak
        s2 s1*2\break s1*3 s2 \bar "" \break s2 s1*2 s2 \bar "" \pageBreak
        s2 s1*2 s2 \bar "" \break s2 s1*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
