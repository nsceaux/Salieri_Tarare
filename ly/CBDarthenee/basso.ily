\clef "bass" R1*2 |
do2\f mi4. sol8 |
do'4 do' do' do |
re re16\f dod re dod re4 re |
re1\fermata |
si,2\fp si,4. si,8\f |
dod2\p~ dod4. dod8\f |
red2 r4 red8 mi |
fad!4 red si, red |
mi2 r |
r r8 la,\mf si, do |
re4 re re re |
re1\p~ |
re2 re\mf |
re\cresc re |
sol\f r4\fermata sol4\p |
do4. do8 re4. re8 |
sol,2 r |
R1*3 | \allowPageTurn
mi1\p~ |
mi~ |
mi2 fa16(-. fa-. fa-. fa-. fa4:16^\dotFour) |
fa2:16^\dotEight fa2:16^\dotEight |
fa2:16^\dotEight fa2:16 |
fa4 r r2 |
mi4 r r2 |
r8 r16 re\f re8. re16 re4 r |
mi!1\fp~ |
mi~ |
mi2 do~ |
do1 |
red~ |
red2 mi~ |
mi re |
do4 r r2 |
r2 fa4 r |
R1 |
r4 mib r re |
R1 |
r2 mib4 r |
mib2:16\< mib:16 |
mib:16 mib:16\! |
re4 r r2 |
sol4 r r2 |
