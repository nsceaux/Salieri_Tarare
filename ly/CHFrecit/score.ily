\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff \with { shortInstrumentName = "Tr." } <<
        { s1*44 <>^"Trompettes en Ré" }
        \global \keepWithTag #'trombe \includeNotes "trombe"
      >>
      \new Staff \with { shortInstrumentName = "Tim." } <<
        { s1*44 <>^"Timballes" }
        \global \includeNotes "timpani"
      >>
    >>
    \new Staff \with { \haraKiri } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*3 s2 \bar "" \break s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*4\break s1*4\break s1*4\break s1*3\pageBreak
        s1*3\break s1*4\break s1*6\pageBreak
        s1*4\break s1*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
