\clef "treble" R1*4 |
r8 mi'(\sf sol' si) do'4 r |
R1*2 |
<si' mi''>4 r r2 |
r16 la''\ff sold'' la'' mi'' la'' do'' mi'' la'4 r |
<mi' dod''>4 r r2 |
re''16 re''' dod''' re''' la'' re''' fa'' la'' re''4 r |
<<
  \tag #'violino1 { <la' la''>4 }
  \tag #'violino2 { <mib'' mib'>4 }
>> r4 r2 |
sib'16 sib'' la'' sib'' fa'' sib'' re'' fa'' sib'4 r |
r2 <<
  \tag #'violino1 { <fa' re''>4 }
  \tag #'violino2 { sib'4 }
>> r4 |
<<
  \tag #'violino1 { <sol mib' mib''>4 }
  \tag #'violino2 { <sol mib' sib'>4 }
>> r4 r2 |
R1 |
r2 <mib' do''>4 r |
<<
  \tag #'violino1 { do''8. do''16 fa''8. re''16 <si'! re'>4 }
  \tag #'violino2 { mib'8. mib'16 lab'8. lab'16 <sol' sol>4 }
>> r4 |
r2 r4 <sol re' si'>4 |
lab8 sib16 do' reb' mib' fa' sol' lab'4 r |
R1 |
<<
  \tag #'violino1 { reb''4 r fa'' r | }
  \tag #'violino2 { lab''4 r do'' r | }
>>
r4 <reb'' fa''>4\ff <sib'' reb'' fa'> r |
R1 |
<<
  \tag #'violino1 { fa'1\p | fa'4 }
  \tag #'violino2 { re'!1-\sug\p | re'4 }
>> r4 r2 |
<<
  \tag #'violino1 { sol'4 }
  \tag #'violino2 { mib' }
>> r4 r2 |
r2 r8. mib''16\ff \grace fa''16 mib''8 re''16 mib'' |
<re'' re'>4 <re' la' fad''> <<
  \tag #'violino1 { <la'' la'>4 }
  \tag #'violino2 { <re' la' fad''>4 }
>> r4 |
R1 |
r4 <sol' re'' sib''> <<
  \tag #'violino1 { <sol' re'' re'''>4 }
  \tag #'violino2 { <sol' re'' sib''>4 }
>> r4 |
r4 r8 <<
  \tag #'violino1 { sib'8\p sib'4 si'( | do''2)~ do''4 }
  \tag #'violino2 { sol'8-\sug\p sol'2~ | sol' la'4 }
>> r4 |
R1*2 |
<<
  \tag #'violino1 {
    r8 fa''\f fa'' fa'' fa''16 la'' do''' sib'' la'' sol'' fa'' mib'' |
    re'' fa'' mib'' re'' mib'' fa'' sol'' la'' sib'' la'' sol'' fa'' mib'' re'' do'' sib' |
    la'1 |
  }
  \tag #'violino2 {
    <do' la'>2:16-\sug\f q:16 |
    <re' sib'>2:16 q:16 |
    <do' la'>1 |
  }
>>
<re' la'>4 r r2 |
<<
  \tag #'violino1 { re'4 }
  \tag #'violino2 { sib' }
>> r4 r2 |
r2 r4 <<
  \tag #'violino1 {
    la'4 |
    sold'8.\ff re''16 re''8 re'' sold''8. re'''16 re'''8 re''' |
    dod'''4 r16 la'32\ff la' la'16 la' re''8 la''-! fa''-! re''-! |
    la'4
  }
  \tag #'violino2 {
    <dod' mi'>4 |
    re'8.-\sug\ff sold'16 sold'8 sold' re''8. sold''16 sold''8 sold'' |
    la''4 r16 la32-\sug\ff la la16 la re'8 la'-! fa'-! re'-! |
    la4
  }
>> r16 la32\p la la16 la re'8 la'-! fa'-! re'-! |
la16 dod'32\p dod' mi'16 dod' la si32 dod' re' mi' fad' sold' la'16 dod''32 dod'' mi''16\cresc dod'' la' si'32 dod'' re'' mi'' fad'' sold'' |
la''16\f dod'''32 dod''' mi'''16 dod''' la'' la'' dod''' la'' mi'' la''32 la'' dod'''16 la'' mi'' dod'' mi'' dod'' |
la' la32 la la16 la la4:16 la2_\fermata ^\trill |
