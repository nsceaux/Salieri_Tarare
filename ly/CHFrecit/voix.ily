\clef "bass" <>^\markup\character-text Altamort avec fureur ^"Parlé"
r4 r8 sol sol sol sol la |
si4 r8 re'16 re' re'4 do'8 re' |
si8 si
\ffclef "tenor/G_8" <>^\markup\character-text Tarare dédaigneux
r8 si16 si si4 si8 do' |
re'4 r8 re'16 re' si4 la8 sol |
do'4 r r r8 sol |
sol sol sol sol do'4. do'8 |
mi'8 mi' re' mi' do'4 r16 do' re' mi' |
si4. si8 re' re' mi' si |
do' do' r4 r8 mi' mi' mi' |
dod'4 dod'8 dod' mi'4 dod'8 la |
re'4 r r r8 re'16 re' |
mib'2 mib'8 mib' fa' do' |
re'4 r r r8 fa' |
fa'4 fa'8 fa' re'4 fa'8 sib |
mib' mib' r4
\ffclef "bass" <>^\markup\character Altamort
sib2 |
sib8 sib sib sib mib'4 r |
reb'8 reb'16 reb' reb'8 mib' do'4 r8 lab |
do'8. do'16 fa'8. re'16 si!4 r8 re' |
si si si do' sol4 r |
\ffclef "bass" <>^\markup\character Arthénée
r2 mib'4 mib'8 mib' |
do'4 r16 do' reb' mib' lab8 lab r lab |
reb'4
\ffclef "bass" <>^\markup\character-text Altamort furieux
r16 reb' mib' fa' do'8. do'16 do' reb' mib' reb' |
sib8 sib r4 r2 |
\ffclef "tenor/G_8" <>^\markup\character-text Tarare d’un ton glacé
sib8 sib sib sib sib4 fa8 fa |
re!4 r16 fa sib? la sib8. sib16 sib8 do' |
lab8 lab r16 lab sib do' fa8 fa r sib16 sib |
sol8 sol r sib16 sib mib'4 mib'8 do' |
la!4. sib8 fa4 r |
\ffclef "bass" <>^\markup\character-text Arthénée s’écrit
r2 r4 r8 la |
re' re' re' re' la la16 sib do'8 re'16 la |
sib8 sib r4
\ffclef "bass" <>^\markup\character Atar
r4 re'8. re'16 |
sib4 r
\ffclef "tenor/G_8" <>^\markup\character Tarare
r2 |
r4 r8 do'16 do' la4 r |
<>^\markup\italic { prenant la main d’Altamort }
fa4 r8 fa la4 r8 la16 do' |
do'8 fa r4 r2 |
R1*2 |
<>^\markup\italic { à Calpigi (à part) }
r4 r8 la do'4 r16 do' re' mib' |
re'4 r8 re'16 re' re'4 do'8 re' |
sib4 r8 sol16 la sib4 la8 sol |
dod'8 mi'16 mi' dod'8 dod'16 re' la4 r |
R1*6 |
