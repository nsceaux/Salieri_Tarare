\clef "bass" R1*4 |
r8 mi(\sf sol si,) do4 r |
R1*2 |
sold4 r r2 |
la,8\ff la mi do la,4 r |
sol!4 r r2 |
fa8 re' la fa re4 r |
do r r2 |
sib,8 sib fa re sib,4 r |
r2 lab4 r |
sol r r2 |
R1 |
r2 lab,4 r |
lab8. lab16 fa8. fa16 fa4 r |
r2 r4 sol, |
lab,8 sib,16 do reb mib fa sol lab4 r |
R1 |
fa4 r la! r |
r sib\ff sib, r |
R1 |
sib,1\p |
sib,4 r r2 |
mib4 r r2 |
r2 r4 fa\ff |
fad re re r |
R1 |
r4 sol sol, r |
r2 sol4(\p fa!) |
mi2 fa4 r |
R1*2 |
fa2:8\f fa:8 |
fa:8 fa:8 |
fa1 |
fad4 r r2 |
sol4 r r2 |
r r4 la, |
sib,8.-\sug\ff sib,16 sib,8 sib, sib,8. sib,16 sib,8 sib, |
la,4 r r2 |
r4 r16 la,\p la, la, re8 la-! fa-! re-! |
<>\p \ru#8 la,32 la,4:32 la,2:32\cresc |
la,:32\f la,:32 |
la,16 la,32 la, la,16 la, la,4:16 la,2\fermata |
