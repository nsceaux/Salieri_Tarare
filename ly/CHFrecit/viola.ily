\clef "alto" R1*4 |
r8 mi'(\sf sol' si) do'4 r |
R1*2 |
sold'4 r r2 |
la8\ff la' mi' do' la4 r |
sol'!4 r r2 |
fa'8 re'' la' fa' re'4 r |
do' r r2 |
sib8 sib' fa' re' sib4 r |
r2 lab'4 r |
sol' r r2 |
R1 |
r2 lab4 r |
lab'8. lab'16 fa'8. fa'16 fa'4 r |
r2 r4 sol |
lab8 sib16 do' reb' mib' fa' sol' lab'4 r |
R1 | \allowPageTurn
fa'4 r la'! r |
r <sib fa' reb''>-\sug\ff q r |
R1 |
sib1-\sug\p |
sib4 r r2 |
sib4 r r2 |
r2 r4 la'!-\sug\ff |
<re' la'> <fad' la'> q r |
R1 |
r4 sol' sol r |
r4 r8 re'-\sug\p re'2 |
do'~ do'4 r |
R1*2 |
fa'2:16-\sug\f fa':16 |
fa'2:16 fa':16 |
fa'1 |
fad'4 r r2 |
sol'4 r r2 |
r2 r4 la |
re'8.-\sug\ff re'16 re'8 re' re'8. re'16 re'8 re' |
mi'4 r16 la32-\sug\ff la la16 la re'8 la'-! fa'-! re'-! |
la4 r16 la32-\sug\p la la16 la re'8 la'-! fa'-! re'-! |
la16-\sug\p dod' mi' dod' la si32 dod' re' mi' fad' sold' la'32\cresc <dod' mi'> q q q q q q q4:32 |
q2:32\f q:32 |
la16 la32 la la16 la la4:16 la2\fermata |
