\score {
  <<
    \new Staff \with { \haraKiri \tinyStaff } \withLyrics <<
      \global \includeNotes "voix"
    >> { \set fontSize = -2 \includeLyrics "paroles" }
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff <<
        { s1*44 <>^"Trompettes en Ré" }
        \global \keepWithTag #'trombe \includeNotes "trombe"
      >>
      \new Staff <<
        { s1*44 <>^"Timballes" }
        \global \includeNotes "timpani"
      >>
    >>
  >>
  \layout { }
}
