\key do \major \midiTempo#100
\time 4/4 s1*4
\tempo "Andante" s1*4
\tempo "Allegro" s1*9
\tempo "Maestoso" s1*2
\tempo "Allegro" s1*8 s2
\tempo "Allegro" s2 s1*3 s4.
\tempo "Andante" s8 s2 s1*3
\tempo "Allegro Maestoso" s1*6
\tempo "Andante Maestoso" \midiTempo#60 s1*6 \bar "|."
