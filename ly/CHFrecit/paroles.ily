Sans le res -- pect d’A -- tar, vil ob -- jet de ma hai -- ne…

Du des -- tin de l’é -- tat tu pré -- tends dé -- ci -- der !
Fou -- gueux a -- do -- les -- cent, qui veux nous com -- man -- der !
Pour titre i -- ci n’as- tu que des in -- ju -- res ?
Quels en -- ne -- mis t’a- t-on vu ter -- ras -- ser ?
Quels tor -- rents o -- sas- tu pas -- ser ?
Où sont tes ex -- ploits, tes bles -- su -- res ?

Toi, qui de ce haut rang brû -- les de t’ap -- pro -- cher,
ap -- prends que sur mon corps il te fau -- dra mar -- cher.

Ô dé -- ses -- poir ! ô fré -- né -- si -- e !
Mon fils !…

À ce bri -- gand j’ar -- ra -- che -- rai la vi -- e.

Cal -- me ta fu -- reur, Al -- ta -- mort.
Ce som -- bre feu, quand il s’al -- lu -- me,
dé -- truit les for -- ces, nous con -- su -- me :
le guer -- rier, en co -- lère, est mort.

Le tem -- ple de nos dieux est- il donc une a -- rè -- ne ?

Ar -- rê -- tez.

J’o -- bé -- is…

Toi, ce soir, à la plai -- ne.

Et toi, fi -- dèle a -- mi, sans fa -- nal et sans bruit,
au ver -- ger du sé -- rail at -- tends- moi cet -- te nuit.
