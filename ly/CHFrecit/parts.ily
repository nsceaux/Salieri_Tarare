\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (trombe #:score "score-tt")
   (timpani #:score "score-tt")
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #}))
