\clef "treble" \transposition re
R1*44
\twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'1~ |
    sol'2 sol'16 re''32 re'' re''16 re'' re''4:16 |
    sol''16 sol'32 sol' sol'16 sol' sol'4:16 sol'2\fermata | }
  { sol1~ |
    sol2 sol16 sol'32 sol' sol'16 sol' sol'4:16 |
    sol'16 sol'32 sol' sol'16 sol' sol'4:16 sol'2\fermata | } |
  { s2-\sug\p s-\sug\cresc | s-\sug\f }
>>