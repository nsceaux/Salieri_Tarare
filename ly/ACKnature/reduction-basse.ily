\clef "bass" << \rp#4 { dod16 mi la mi } \\ la,4 >> |
<< \rp#2 { dod16 mi la mi } \\ la,4 >> la,4 r\fermata |
<la dod'>2( <re fad>) |
la4 mi dod la, |
<re si,>2( <si, sold,>4 <sold, mi,>) |
r2 sold( |
la) r |
\rp#4 { mi16 la dod' la } |
\rp#4 { mi sold si sold } |
\rp#4 { mi sold si sold } |
<< { \rt#4 { la si } \rt#4 { la si } } \\ { fad2:8 fad8 fad fad mi } >> |
<< { \rt#4 { fad16 si } \rt#4 { fad si } } \\ { red2:8 red:8 } >> |
<< { \rt#4 { fad16 si } \rt#4 { fad si } } \\ { red2:8 red:8 } >> |
mi4 mi8. mi16 mi4 re |
do1 |
R1 |
<do sol mi>8 q4 q8 <si, sol fa re> q4 q8 |
<do mi sol>8 q4 q8 <si, re sol fa> q4 q8 |
do2:8 do:8 |
do:8 do:8 |
dod:8 dod:8 |
dod8 <la, dod mi> q q q2:8 |
sol,8 sol sol sol sol2:8 |
sol8 sol sol mi dod2:8 |
re4 re re re |
re r r |
r8 re16. mi32 fa4( fad) |
sol sol, r |
r8 mi( sold si mi' mi) |
la4 la,8 la( si do') |
re'8 re' re' re' re' re |
mi8. mi16 mi4 r |
dod8. <la dod>16 q8. q16 q8. q16 |
<re la>8. q16 q8. q16 q8. q16 |
mi8. mi16 mi8. mi16 mi8. mi16 |
fad2.\fermata r4 |
<dod mi la>8 q4 q q q8 |
re2:8 re:8 |
mi:8 mi:8 |
<dod mi la>:8 <re fad la>:8 |
<mi la dod'>:8 q:8 |
<mi sold si>:8 q:8 |
<la, mi>2\fermata
