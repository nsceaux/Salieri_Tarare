\clef "treble"
<<
  \tag #'violino1 {
    <mi' dod'' la''>4..\f mi''16 mi''4.. dod'''16 |
    <dod'''  mi''>4 <la'' dod''>8. q16 q4 r\fermata |
    mi''4.(\p dod''8) fad''4. fad''8 |
    mi''2.(\cresc dod''8 la') |
    la'(\f sold' fad' mi' mi' sold' si' re'') |
    re''2~ re''4. si'8 |
    dod''2 r |
    r4 dod''(\p mi'' la'') |
    r si''( sold'' mi'') |
    si'4.( si''8) si''4. mi''8 |
    red''4.\cresc si'16 dod'' red''8( mi'' fad'' sold'') |
    la''8\f la''4 la'' la'' la''8 |
    la''2\p si |
    mi'4
  }
  \tag #'violino2 {
    dod'16-\sug\f mi' la' mi' \rp#3 { dod' mi' la' mi' } |
    \rp#2 { dod' mi' la' mi' } dod'4 r\fermata |
    dod''4.-\sug\p la'8 re''4. re''8 |
    dod''2.-\sug\cresc dod'4 |
    re'2-\sug\f( si4 sold) |
    << { si'1 | dod''2 } \\ { sold'2 mi' | mi' } >> r |
    \rp#4 { mi'16-\sug\p la' dod'' la' } |
    \rp#4 { mi' sold' si' sold' } |
    \rp#4 { mi' sold' si' sold' } |
    \rt#4 { la'-\sug\cresc si' } \rt#4 { la' si' } |
    \rt#4 { fad'-\sug\f si' } \rt#4 { fad' si' } |
    \rt#4 { fad'-\sug\p si' } \rt#4 { fad' si' } |
    sold'4
  }
>> mi'8.\ff mi'16 mi'4 re' |
do'1 |
do''4 sol'8. sol'16 do''4 mi'' |
<<
  \tag #'violino1 {
    sol''2:16-\sug\ff sol'':16 |
    sol''2:16 sol'':16 |
    sol''8 mi'4\p mi' mi' mi'8 |
    mi' mi'4 mi' mi' mi'8 |
    mi' mi'4 mi' mi' mi'8 |
    <dod' mi' la'>4 la'8.\mf la'16 dod''4 mi'' |
    <dod'' sib''>8\f q4 q q\p q8 |
    <mi' dod''>4.\fp mi''8 sol''4.\fp la''8 |
    fa''8.\f <re'' re'''>16 q8. <re'' la''>16 q8. <la' fa''>16 q8. <fa' re''>16 |
    re''8 re'([\mf fa' la' re'' mi'']) |
    fa''8 fa''16.\p mi''32 re''8 re'4 do''8 |
    do''( si') si'8. si'16 si'8.\f si'16 |
    mi''8\fp mi'4 mi' mi'8 |
    mi'8 mi'4 do''8( re'' mi'') |
    fa'' fa''4 re''8( si' la') |
    sold'4 r mi''8. mi''16 |
    <mi' dod'' la''>8.\fp mi'16 mi'8. mi'16 mi'8. mi'16 |
    fad'8. fad'16 fad'8. fad'16 fad'8. fad'16 |
    mi'8. mi'16 mi'8. mi'16 sold'8. sold'16 |
    la'2.\fermata mi''8. dod''16 |
    la'4( dod'' mi'' la'') |
    <la' fad''>8\f q4 q q q8 |
    <dod'' mi''>4\p q8. q16 <si' mi''>4 q |
    la''2:16\cresc re''':16 |
    dod'''4:16 mi''':16 dod''':16 la'':16 |
    sold'':16\ff si'':16 mi''':16 sold'':16 |
    la''2\fermata
  }
  \tag #'violino2 {
    sol''4.\ff \acciaccatura fa''8 mi''16 re''32 mi'' fa''8 re'' si' fa' |
    mi'8 sol' do'' mi'' fa'' re'' si' fa' |
    mi' sol'4-\sug\p sol' sol' sol'8 |
    sib'1 |
    la'8 la'4 la' la' la'8 |
    la' <dod' mi'>-\sug\mf q q q2:8 |
    <dod'' mi''>8-\sug\f q4 q q-\sug\p mi'8 |
    sib'4.-\sug\fp sib'8 <la' mi''>8-\sug\fp q4 q8 |
    <la' fa''>8.-\sug\f q16 q8. q16 q8. <fa' re''>16 q8. <fa' la'>16 |
    q8 r r4 r |
    r8 fa'16.-\sug\p sol'32 la'4. la'8 |
    re'4~ re'8 r r4 |
    r8 <si sold'>4\p q q8 |
    <do' la'>8 q4 la' la'8 |
    la' la'4 fa' fa'8 |
    re'4 r r |
    r8. la16-\sug\p la8. la16 la8. la16 |
    la8. la16 la8. la16 la8. la16 |
    dod'8. dod'16 dod'8. dod'16 si8. mi'16 |
    dod'2.\fermata r4 |
    <mi' la'>8 q4 q q q8 |
    <fad' la'>-\sug\f q4 q q q8 |
    <mi' dod''>4-\sug\p q8. q16 <mi' si'>4 q |
    la'8:16-\sug\cresc dod'':16 mi'':16 la':16 fad':16 la':16 re'':16 fad'':16 |
    mi''16 <dod'' la''> q q q4:16 q2:16 |
    <si' sold''>2:16-\sug\ff q:16 |
    <la' la''>2\fermata
  }
>>
