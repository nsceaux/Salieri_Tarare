\clef "treble" R1*14 |
do'1 |
R1 |
<>\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4. do''8 re''4. re''8 |
    mi''4. do''8 re''4. re''8 |
    mi''4 }
  { mi'4. mi'8 sol'4. sol'8 |
    do''4. mi'8 sol'4. sol'8 |
    do''4 }
>> r4 r2 |
R1*3 |
<>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) << mi''1 sol' >>
R1 |
r8. <>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''16 re''8. re''16 re''8. re''16 re''8. re''16 | re''4 }
  { re''16 re''8. re''16 re''8. re''16 re''8. re''16 | re''4 }
>> r4 r |
R2.*9 |
r2\fermata r4 r |
R1*2 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''4 mi''8. mi''16 mi''4 mi'' |
    mi''2 fad'' | }
  { mi'4 mi'8. mi'16 mi'4 mi' |
    mi'2 re'' | }
  { s1\p <>-\sug\cresc }
>>
mi''1 |
mi''-\sug\ff |
mi'2\fermata
