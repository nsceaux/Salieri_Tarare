\clef "treble"
<mi'' dod'' la''>4..\f <dod'' mi''>16 q4.. <mi'' dod'''>16 |
q4 <la'' mi'' dod''>8. q16 q4 r\fermata |
<dod'' mi''>4.(\p <la' dod''>8) <re'' fad''>4. q8 |
<dod'' mi''>2.(\cresc dod''8 la') |
la'(\f sold' fad' mi' mi' sold' si' re'') |
<< { <si' re''>2~ q4. si'8 } \\ { sold'2 mi' } >> |
<dod'' mi'>2 r |
r4 dod''(\p mi'' la'') |
r si''( sold'' mi'') |
si'4.( si''8) si''4. mi''8 |
red''4.\cresc si'16 dod'' red''8( mi'' fad'' sold'') |
<fad'' la''>8\f q4 q q q8 |
q2\p si' |
<sold' mi'>4 mi'8.\ff mi'16 mi'4 re' |
do'1 |
do''4 sol'8. sol'16 do''4 mi'' |
sol''4.\ff \acciaccatura fa''8 mi''16 re''32 mi'' fa''8 re'' si' fa' |
mi'8 sol' do'' mi'' fa'' re'' si' fa' |
<mi' sol'>8 q4\p q q q8 |
<mi' sol' sib'>8 q4 q q q8 |
<mi' sol' la'> q4 q q q8 |
<dod' mi' la'>4 la'8.\mf la'16 dod''4 mi'' |
<dod'' mi'' sib''>8\f q4 q q\p q8 |
<mi' sib' dod''>4.\fp <sib' mi''>8 <la' mi'' sol''>8\fp q4 <la' mi'' la''>8 |
<la' fa''>8.\f <fa'' la'' re'''>16 q8. <re'' fa'' la''>16 q8. <la' re'' fa''>16 q8. <fa' la' re''>16 |
q8 re'([\mf fa' la' re'' mi'']) |
fa''8 <fa' fa''>16.\p <sol' mi''>32 <la' re''>8 re'4 <la' do''>8 |
<< { do''( si') si'8. si'16 } \\ { re'4~ re'8 } >> si'8.\f si'16 |
mi''8\fp <sold' si' mi''>4 q q8 |
<la' do'' mi''>8 q4 do''8( re'' mi'') |
<la' fa''> q4 re''8( do'' si') |
<re' sold'>4 r mi''8. mi''16 |
<mi'' dod'' la''>8.\fp mi'16 mi'8. mi'16 mi'8. mi'16 |
fad'8. fad'16 fad'8. fad'16 fad'8. fad'16 |
<dod' mi'>8. q16 q8. q16 <si sold'>8. <mi' sold'>16 |
<dod' la'>2.\fermata mi''8. dod''16 |
la'4( dod'' mi'' la'') |
<la' fad''>8\f q4 q q q8 |
<la' dod'' mi''>4\p q8. q16 <si' sold' mi''>4 q |
la'8:16\cresc dod'':16 mi'':16 la':16 fad':16 la':16 re'':16 fad'':16 |
dod'''4:16 mi''':16 dod''':16 la'':16 |
sold'':16\ff si'':16 mi''':16 sold'':16 |
la''2\fermata
