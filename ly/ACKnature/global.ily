\tag #'all \key la \major \tempo "Allegro" \midiTempo#120
\time 4/4 s1*14 \bar "||"
\tag #'all \key la \minor s1*11 \bar "||"
\time 3/4 \tempo "Andante" \midiTempo#96 s2.*6 \bar "||"
\tag #'all \key la \major s2.*4
\time 4/4 s2. \tempo "Allegro" \midiTempo#120 s4 s1*6 s2 \bar "|."
