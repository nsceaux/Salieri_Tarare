\clef "bass" R1*3 |
r2 r4 dod'8(-\sug\f la) |
la( sold fad mi) mi( sold si re') |
re'2~ re'4. si8 |
dod'2 r |
R1*6 |
r4 mi8.-\sug\ff mi16 mi4 re |
do1 |
R1*3 |
<>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol1 | sol | la | }
  { mi | mi | mi | }
>>
dod8 la,\mf la, la, la,2:8 |
sol,8\f sol sol sol sol2:8\p |
sol8\fp sol sol mi dod2:8\fp |
re4\f re re re |
re r r |
R2.*6 |
dod2.-\sug\fp |
re |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { dod'2 si4 | }
  { la2 sold4 | }
>>
la2.\fermata r4 |
dod1 |
re-\sug\f |
mi-\sug\p |
dod2-\sug\cresc re |
mi4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { dod'2 dod'4 | si1 | }
  { la2 la4 | sold1 | }
  { s2. <>-\sug\ff }
>>
la2\fermata
