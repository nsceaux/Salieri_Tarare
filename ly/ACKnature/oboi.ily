\clef "treble"
<>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''4.. mi''16 mi''4.. mi''16 |
    mi''4 mi''8. mi''16 mi''4 }
  { dod''4.. dod''16 dod''4.. dod''16 |
    dod''4 dod''8. dod''16 dod''4 }
>> r4\fermata |
R1*2 |
r2 <>^"solo" mi'8(-\sug\f sold' si' re'') |
re''2~ re''4. si'8 |
dod''2 r |
R1*4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''1~ | la'' | sold''4 mi'8. mi'16 mi'4 re' | }
  { fad''1~ | fad'' | mi''4 mi'8. mi'16 mi'4 re' | }
  { s1-\sug\f | s-\sug\p | s4 <>-\sug\ff }
>>
do'1 |
R1 |
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4. do''8 re''4. re''8 |
    mi''4. do''8 re''4. re''8 |
    mi''4 }
  { mi'4. mi'8 sol'4. sol'8 |
    do''4. mi'8 sol'4. sol'8 |
    do''4 }
>> r4 r2 |
R1*2 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'4 dod'' mi'' | sib''1 | }
  { la'4 dod'' mi'' | dod''1 }
  { s2.-\sug\mf | s1-\sug\f }
>>
R1 |
r8. <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re'''16 re'''8. la''16 la''8. fa''16 fa''8. re''16 | la'4 }
  { fa''16 fa''8. fa''16 fa''8. re''16 re''8. la'16 | fa'4 }
>> r4 r |
R2.*6 |
<>\fp \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''2. | fad'' | }
  { la'2. | la' | }
>>
R2. |
r2\fermata r4 r |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''1 |
    fad'' |
    dod''2 si' |
    mi'' fad'' |
    mi''4 la''2 la''4 |
    sold''1 |
    la''2\fermata }
  { la'1 |
    la' |
    la'2 sold' |
    la' la' |
    dod''1 |
    si' |
    la'2\fermata }
  { s1 s1-\sug\f s-\sug\p s1*2-\sug\cresc <>-\sug\ff }
>>
