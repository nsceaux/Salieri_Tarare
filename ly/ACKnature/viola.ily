\clef "alto" la2:16-\sug\f la:16 |
la:16 la4 r\fermata |
<<
  << { mi'2 fad' | mi'2. } \\ { dod'2 re' | dod'2. } >>
  { s1-\sug\p | s2.-\sug\cresc }
>> dod'4 |
re'2-\sug\f si4 sold |
r2 sold |
la r |
dod'8(\p mi' dod' mi' dod' mi' dod' mi') |
si( mi' si mi' si mi' si mi') |
si( mi' si mi' si mi' si mi') |
fad'2:8-\sug\cresc fad'8 fad' fad' mi' |
red'-\sug\f red'4 red' red' red'8 |
red'2:8-\sug\p red':8 |
mi'4 mi'8.-\sug\ff mi'16 mi'4 re' |
do'1 |
R1 |
<sol mi'>8-\sug\ff q4 q8 <sol fa'> q4 q8 |
<sol mi'>8 q4 q8 <sol fa'> q4 q8 |
<sol mi'>1-\sug\p |
q |
q |
r8 la-\sug\mf la la la2:8 |
sol8\f sol' sol' sol' sol'2:8\p |
sol'8\fp sol' sol' mi' dod'2:8\fp |
re'4\f re' re' re' |
re'8 re([\mf fa la re' mi']) |
fa' re16.[\p mi32] fa4( fad) |
sol sol r |
r8 mi(-\sug\fp sold si mi' mi) |
la4. la8( si do') |
re' re'4 re'8 re' re |
mi8. mi16 mi4 r |
dod8.\fp dod16 dod8. dod16 dod8. dod16 |
re8. re16 re8. re16 re8. re16 |
mi8. mi16 mi8. mi16 mi8. mi16 |
fad2.\fermata r4 |
dod'2:8 dod':8 |
re':8-\sug\f re':8 |
mi':8\p mi':8 |
dod':8\cresc re':8 |
mi':8 mi':8 |
mi':8\ff mi':8 |
la\fermata
