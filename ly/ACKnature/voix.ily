\clef "soprano/treble" R1 |
r2 r4 r8\fermata mi'' |
mi''4. dod''8 fad''4. fad''8 |
mi''2 r |
R1*2 |
r2 r4 r8 la' |
dod''2 \grace fad''8 mi''4 re''8 dod'' |
si'4 si' r2 |
si'4 si'8 si' si'4. mi''8 |
red''4. red''8 red'' mi'' fad'' sold'' |
la''1~ |
la''4 fad''8 red'' si'4 si'8 la' |
sold'2 r |
R1 |
do''4 sol'8 sol' do''4 mi'' |
sol''1~ |
sol''~ |
sol''2. sol'4 |
sib'4. sib'8 sib'4. sib'8 |
la'2 r |
la'4 la'8 la' dod''4 mi'' |
sib''2. dod''4 |
dod''4. mi''8 sol''4. la''8 |
fa''4 fa'' r r8 la' |
re''4. re''8 re''8. mi''16 |
fa''8 fa''16 mi'' re''4 re''8. do''16 |
si'4. si'8 si'8. si'16 |
mi''4. si'8 si'8. re''16 |
do''4. do''8 re'' mi'' |
fa''4. re''8 si'8. la'16 |
sold'8 sold' r4 mi''8. mi''16 |
la''2 la'8. la'16 |
fad'2. |
mi'4 mi'8. mi'16 mi''8. re''16 |
dod''2. mi''8 dod'' |
la'4( dod'') mi'' la'' |
fad''1 |
mi''4 mi''8 mi'' mi''4 mi'' |
la''2 r4 fad'' |
mi''2. mi''4 |
mi''2. mi''4 |
la'2\fermata
