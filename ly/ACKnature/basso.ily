\clef "bass" la,2:8\f la,:8 |
la,4 la,8. la,16 la,4 r\fermata |
la2(\p re) |
la4\cresc mi dod la, |
si,2\f( sold,4 mi,) |
r2 sold( |
la) r |
la2:8\p la:8 |
sold:8 sold:8 |
sold:8 sold:8 |
fad:8\cresc fad8 fad fad mi |
red2:8\f red:8 |
red:8\p red:8 |
mi4 mi8.\ff mi16 mi4 re |
do1 |
R1 |
do8\ff do do do si,2:8 |
do2:8 si,:8 |
do:8\p do:8 |
do:8 do:8 |
dod:8 dod:8 |
dod8 la,\mf la, la, la,2:8 |
sol,8\f sol sol sol sol2:8\p |
sol8\fp sol sol mi dod2:8\fp |
re4\f re re re |
re r r |
r8 re16.\p mi32 fa4( fad) |
sol sol, r |
r8 mi(\fp sold si mi' mi) |
la4 la,8 <>^"Violoncelli" la( si do') |
re'8 re' re' re' re' re |
mi8. mi16 mi4 r |
<>^"tutti" dod8.\fp dod16 dod8. dod16 dod8. dod16 |
re8. re16 re8. re16 re8. re16 |
mi8. mi16 mi8. mi16 mi8. mi16 |
fad2.\fermata r4 |
dod2:8 dod:8 |
re:8\f re:8 |
mi:8\p mi:8 |
dod:8\cresc re:8 |
mi:8 mi:8 |
mi:8\ff mi:8 |
la,2\fermata
