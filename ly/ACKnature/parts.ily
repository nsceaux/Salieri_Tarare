\piecePartSpecs
#`((oboi #:tag-notes oboi #:score-template "score-part-voix")
   (trombe #:tag-notes trombe #:tag-global ()
           #:score-template "score-part-voix"
           #:music ,#{ s1*14\break <>^\markup\large { Trompettes en Ut } _"[a 2]" #})
   (fagotti #:tag-notes fagotti
            #:score-template "score-part-voix")
   (violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (reduction)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet#})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#}))
