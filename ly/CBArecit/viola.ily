\clef "alto" sib'2:16 sib':16 |
mib':16 <mib' la'>:16 |
<re' sib'>4. re'8 mib' sib' re' sib' |
dod'4. dod'8 dod'8. re'16 re'8.\trill dod'32 re' |
dod'1~ |
dod' |
re'~ |
re'~ |
re'~ |
re' |
re' |
r8. do'16 do'4 r2 |
r red'4 r |
R1*2 |
\sugNotes { mi'4 r mi' re' | do' r r2 | }
R1 |
r2 do'~ |
do'~ do'4 r |
r2 fa'-\sug\fp |
mi'4 r
