\clef "bass" sib8 fa16 re sib,4 r r8 sib |
do' mib'16 re' do' sib la sol fa4. fa8 |
sib8 fa16 sol lab8 lab sol4 fa |
mi!4. mi8 mi4 re |
dod1~ |
dod |
re~ |
re~ |
re~ |
re |
re |
r8. do16 do4 r2 |
r red4 r |
R1*2 |
mi4 r mi re |
do r r2 |
R1 |
r2 do~ |
do fa4 r |
r2 fa2\fp |
mi4 r
