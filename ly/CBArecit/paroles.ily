Les sau -- va -- ges d’un au -- tre mon -- de,
me -- na -- cent d’en -- va -- hir ces lieux ;
au loin dé -- jà la fou -- dre gron -- de ;
ton peu -- ple su -- pers -- ti -- tieux,
pres -- sé com -- me les flots, in -- non -- de
le par -- vis sa -- cré de nos Dieux.

De vils bri -- gands u -- ne poi -- gné -- e,
sor -- tant d’u -- ne terre é -- loi -- gné -- e,
pour -- rait- elle en -- va -- hir ces lieux ?
Pon -- ti -- fe, votre âme é -- ton -- né -- e…
ce -- pen -- dant, par -- lez, Ar -- thé -- né -- e,
que dit l’in -- ter -- prè -- te des Dieux ?
