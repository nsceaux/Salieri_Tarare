\piecePartSpecs
#`((violino1 #:score-template "score-part-voix" #:indent 0)
   (violino2 #:score-template "score-part-voix" #:indent 0)
   (viola #:score-template "score-part-voix" #:indent 0)
   (basso #:score-template "score-part-voix" #:indent 0)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#}))
