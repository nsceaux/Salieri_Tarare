\clef "treble"
<<
  \tag #'violino1 {
    <fa' re'' sib''>4. \grace do'''16 sib''32 la'' sib'' do''' re'''8 sib'' fa'' re'' |
    mib''4. mib''8 do''' la'' fa'' do'' |
    re''4.
  }
  \tag #'violino2 {
    <fa' re''>16 q q q q4:16 q2:16 |
    sol':16 <mib' la'>:16 |
    <re' sib'>4.
  }
>> re'8 mib' sib' re' sib' |
dod'4. dod'8 dod'8. re'16 re'8.\trill dod'32 re' |
<mi' la>1~ |
q |
<la fa'>1~ |
q~ |
q2 <si! sold'>2~ |
q1 |
q |
r8. <mi' la'>16 q4 r2 |
r2 <<
  \tag #'violino1 { si'!4 }
  \tag #'violino2 { <si! fad'>4 }
>> r4 | \allowPageTurn
R1*2 |
<si sol'>4 r <<
  \tag #'violino1 {
    <si sol'>4 <sol re' si'> |
    <sol mi' do''>
  }
  \tag #'violino2 {
    <sol sol'>4 <fa' sol> |
    <sol mi'>
  }
>> r4 r2 |
R1 |
r2 <<
  \tag #'violino1 {
    <sol' sib'>2~ |
    q la'4 r |
    r2 re''\fp |
    si'4 r
  }
  \tag #'violino2 {
    <sol mi'>2~ |
    q <la fa'>4 r |
    r2 la'-\sug\fp |
    sold'4 r
  }
>>
