\clef "bass" R1*4 |
<>^\markup\character Arthénée
r4 r8 mi16 mi la8 la16 la la8 dod' |
la la r la la mi sol16 sol sol la |
fa4 r8 la re'4. la8 |
fa fa sol la re re r re |
la la16 la la8 si sold4 r8 si |
sold sold la si mi4 r8 mi |
sold sold sold16 sold sold la si4 si8 do' |
la4 r
\ffclef "bass" <>^\markup\character-text Atar dédaigneusement
r8 mi mi mi |
la8. la16 si8 do' si si r si |
si4 fad8 fad fad4 fad8 fad |
red8 red r fad16 sol la8 la16 la si8 fad |
sol4 r r2 |
r4 r8 sol do'4 do'8 r16 sol |
mi4 mi8 fa sol4 sol8 r |
<>^\markup\italic Ton modéré r4 sol8 sol sib4 r8 sol |
sib4 sib8 do' la la r la |
la4 la8 la re'4 re'8 la |
si4 r
