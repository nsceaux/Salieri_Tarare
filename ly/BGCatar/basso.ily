\clef "bass" do2\f do4. do8 |
do2. do'4\p |
si la sol fa |
mi2. do4 |
la,2 fa, |
fa1 |
sol\f |
sol2 sol4\p sol |
do'2. la4 |
fa2 sol4 sol, |
do2\ff do4. do8 |
do4 do mi sol |
do'2\p do'4. do'8 |
do'4 do'\f mi' re' |
dod'2\p dod'4. dod'8 |
dod'4 dod' mi' dod' |
la4 la, si, dod |
<<
  \tag #'fagotti {
    re1-\sug\f~ |
    re4 r r2 |
    R1*3 |
    r4 fad\f la re' |
    re1*1/2 s2\p |
    re1 |
  }
  \tag #'basso {
    re2:8\f re:8 |
    re1\pp~ |
    re1~ |
    re |
    re2 r |
    r4 fad8\ff fad la4 re' |
    re re re\p re |
    re4 re re re |
  }
>>
sol4 si,\f re si, |
sol,2 r |
r4 sol,\p si, re |
sol re si, sol, |
do do mi do |
la,2 fa, |
fa1 |
sol\f~ |
sol2 sol\p |
do2.\f do'4\p |
fa2 sol |
do2.\f re8 mi |
fa mi fa sol la sol la si |
<<
  \tag #'fagotti {
    do'2-\sug\fp r |
    do'-\sug\fp r2 |
    do' r |
    do' r |
    si,1-\sug\p |
    si, |
    r4
  }
  \tag #'basso {
    do'4\fp do do do |
    do\fp do do do |
    do\fp do do do |
    do mi\f re do |
    si,1\p~ |
    si, |
    do4
  }
>> do'8\ff do' do'2:8 |
do'2 r4 do8 re16 mi |
fa2 r4 fa8 sol16 la |
sib4. sib16 la sol4. sol8 |
fa1~ |
fa4 la\p sol fa |
mi1\f~ |
mi2 re4. mi16 fa |
sol2.\fp mi8 fa16 sol |
la2.\fp la,8 si,16 dod |
re2\ff re4. re8 |
re4 <<
  \tag #'fagotti {
    r4 r2 |
    R1*6 |
    R1^\fermataMarkup |
  }
  \tag #'basso {
    re4\p mi fa |
    dod re mi re |
    dod re mi dod |
    re mi fa mi |
    re mi fa mi |
    re fa re fa |
    re re re re |
    mi1\fermata |
  }
>>
mi4\f fad8 sold la si dod' red' |
mi'4 mi8 mi mi4 mi |
<<
  \tag #'fagotti {
    << la1~ { s4. s8-\sug\p } >> |
    la1~ |
    la-\sug\f~ |
    la |
    sol |
    la |
    si~ |
    si |
    <>-\sug\ff <<
      { mi2 mi4. mi8 | mi2 } \\
      { mi2 mi4. mi8 | mi2 }
    >> r2 |
    <>-\sug\fp <<
      { fad1~ | fad | sol~ | sol |
        fad4 re'2 re'4 | la1 | si4 } \\
      { red1~ | red | mi-\sug\fp~ | mi |
        re4 re2 re4 | fad1 | sol4 }
    >> si,-\sug\f re si, |
    sol,2 r |
    r4 sol2-\sug\sf la4 |
    si1 |
  }
  \tag #'basso {
    la4 la\p la la |
    la la la la |
    la\f la la la |
    la la-\sug\p la la |
    sol2:8 sol:8\cresc |
    la:8 la:8 |
    si2:8\f si:8 |
    si,:8 si,:8 |
    mi2\ff mi4. mi8 |
    mi4 mi\p sol mi |
    red\fp red red red |
    red red red red |
    mi\fp mi mi mi |
    mi mi mi mi |
    re re re re |
    re re re re |
    sol si,\f re si, |
    sol, si,\p si, fad, |
    sol, sol2\sf la4 |
    si si, si, si, |
  }
>>
do4 do8\f do mi4 fa! |
sol4 sol\p la si |
do' sol\f do' mi' |
sol' mi' re' do' |
sol2 sol4. sol8 |
<<
  \tag #'fagotti {
    sol2 r |
    R1*7 |
    r2\fermata r |
    R1*8 | \allowPageTurn
    do1-\sug\f~ |
    do~ |
    do~ |
    do~ |
    do~ |
    << do~ { s4 s2.-\sug\p } >> |
    do1~ |
    do~ |
    do2 r |
    R1*7 |
    sol2-\sug\f sol4. sol8 |
    sol2 r |
    R1*4 |
  }
  \tag #'basso {
    sol4 si\p re' si |
    fad sol la sol |
    fad sol la fad |
    sol si sol si |
    sol si sol sol |
    fad fad fad fad |
    fad fad fad fad |
    sol2 sol,\f |
    r\fermata r |
    R1*8 |
    do2:8\f do:8 |
    do:8 do:8 |
    do:8 do:8 |
    do:8 do:8 |
    do:8 do:8 |
    do:8 do:8\p |
    do:8 do:8 |
    do:8 do:8 |
    do:8 do:8 |
    do4 do do do |
    sold, sold, sold, sold, |
    sold sold sold sold |
    la la la la |
    sol! sol sol sol |
    fa r dod r |
    re r re r |
    sol,8\f sol sol sol sol2:8 |
    sol2. sol16\p fa mi re |
    do4 do'2 do'8 sib |
    la4 la8 sol fa4 fa |
    sol sol8 sol sol4 sol |
    sol sol, sol, sol, |
  }
>>
do8\fp do' do' do' do'2:8 |
sib:8\cresc sib:8 |
la2:8 la:8 |
fa:8 fa:8 |
sol2.\fp do'4 |
mi' do' mi' do' |
sol2:8\ff sol:8 |
sol:8 sol:8 |
do2 do4. do8 |
do2. do'4 |
si4 la sol fa |
mi sol mi do |
si,1 |
do2 mi |
fa sol |
do2~ do4. do8 |
do2 do |
do~ do4 sol,8 la,16 si, |
do2 re |
