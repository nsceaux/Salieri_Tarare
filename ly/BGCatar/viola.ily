\clef "alto" do'2\f do'4. do'8 |
do'2. do''4\p |
si' la' sol' fa' |
mi'2. do'4 |
la2 fa |
fa'1 |
r8 re'-\sug\f mi' fa' sol' la' si' do'' |
re'' do'' si' la' sol'-\sug\p fa' mi' re' |
do'2. la4 |
fa2 sol |
<sol mi' do''>2\f q4. <mi' do''>8 |
do'4 do' mi' sol' |
do''2\p do''4. do''8 |
do''4 do''\f mi'' re'' |
dod''2-\sug\p dod''4. dod''8 |
dod''4 dod'' mi'' dod'' |
la' la si dod' |
re'2:8-\sug\f re':8 |
fad16-\sug\pp fad fad fad fad4:16 fad2:16 |
fad2:16 fad2:16 |
fad2:16 fad2:16 |
fad4 r r2 |
r4 fad8-\sug\ff fad la4 re' |
<la fad'>1*1/2 s2\p |
re'4 re' re' re' |
sol' si\f re' si |
sol2 r |
r4 sol-\sug\p si re' |
sol' re' si sol |
do' do' mi' do' |
la2 fa |
fa'1 |
r8 re'-\sug\f mi' fa' sol' la' si' do'' |
re'' do'' si' la' sol'-\sug\p fa' mi' re' |
do'2.-\sug\f do'4 |
fa2 sol |
do2.-\sug\f re8 mi |
fa mi fa sol la sol la si |
do'4-\sug\fp <sol mi'>2 q4 |
<la fa'>4-\sug\fp q2 q4 |
<sol mi'>4-\sug\fp q2 q4 |
q mi'-\sug\f re' do' |
si1-\sug\p~ |
si |
do'4 do''8-\sug\ff do'' do''2:8 |
do''2 r4 do'8 re'16 mi' |
fa'2 r4 fa'8 sol'16 la' |
sib'4. sib'16 la' sol'4. sol'8 |
fa'1 |
fa'4 la'\p sol' fa' |
mi'1\f |
mi'2 re'4. mi'16 fa' |
sol'2.\fp mi'8 fa'16 sol' |
la'2.\fp la8 si16 dod' |
re'2-\sug\ff re'4. re'8 |
re'4 re-\sug\p mi fa |
dod re mi re |
dod re mi dod |
re mi fa mi |
re mi fa mi |
re fa re fa |
re' re' re' re' |
mi'1\fermata |
mi'4\f fad'8 sold' la' si' dod'' red'' |
mi''4 mi'8 mi' mi'4 mi' |
la' la'-\sug\p la' la' |
la' la' la' la' |
red'1-\sug\f~ |
red'4 fad'-\sug\p mi' red' |
si'8 sol sol sol sol2:8-\sug\cresc |
la:8 la:8 |
si:8 si:8 |
si:8 si:8 |
mi'2-\sug\ff mi'4. mi'8 |
mi'4 mi'\p sol' mi' |
red'\fp red' red' red' |
red' red' red' red' |
mi'\fp mi' mi' mi' |
mi' mi' mi' mi' |
re' re' re' re' |
re' re' re' re' |
sol'4 si\f re' si |
sol si\p si fad |
sol sol'2\sf la'4 |
si' si si si |
do' do'8\f do' mi'4 fa' |
sol'4 sol'-\sug\p la' si' |
do' sol do' mi' |
sol' mi' re' do' |
sol2 sol4. sol8 |
sol4 si-\sug\pp re' si |
fad sol la sol |
fad sol la fad |
sol si sol si |
sol si sol sol |
fad fad fad fad |
fad fad fad fad |
sol2 sol-\sug\f |
r4\fermata r si2\p |
do'1 |
mi' |
fa'4 mi' re' do' |
si1 |
do'4 sol do' re' |
mi'4. do'8 fa'4. fa8 |
sol2:8 sol:8 |
sol:8 sol:8 |
do'8-\sug\f <sol mi'> q q q2:8 |
q4 mi'8 do' fa' do' sol' do' |
la'2 fa' |
fa'2.\trill mi'8 fa' |
mi' do' re' mi' fa' sol' la' si' |
do'' sol' mi'-\sug\p do' fa' do' sol' do' |
la'2 fa' |
fa'2.\trill mi'8 fa' |
<sol mi'>2:8 q:8 |
q4 do' do' do' |
sold sold sold sold |
sold sold sold sold |
la la la la |
sol'! sol' sol' sol' |
fa' r dod' r |
re' r re' r |
sol8\f sol' sol' sol' sol'2:8 |
sol':8 sol':8-\sug\p |
sol':8 sol':8 |
fa':8 fa':8 |
sol':8 sol':8 |
sol':8 sol':8 |
sol':8-\sug\fp sol':8 |
sol':8 sol':8-\sug\cresc |
fa':8 fa':8 |
fa':8 fa':8 |
sol':8-\sug\fp sol':8 |
sol':8 sol':8 |
sol':8-\sug\ff sol':8 |
sol':8 sol':8 |
do' do'4. do'8 |
do'2. do''4 |
si'4 la' sol' fa' |
mi' sol' mi' do' |
si1 |
do'2 mi' |
fa' sol' |
do'~ do'4. do'8 |
do'2 do' |
do'~ do'4 sol8 la16 si |
do'2 re' |
