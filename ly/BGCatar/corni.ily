\clef "treble" R1*107 |
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { do''1~ | do''4 }
  { mi'1~ | mi'4 }
>> r4 r2 |
R1*2 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''1~ | do''4 }
  { mi'1~ | mi'4 }
>> r4 r2 |
R1*10 |
<>\f \twoVoices #'(corno1 corno2 corni) <<
  { re''2 re''4. re''8 | re''2 }
  { sol'2 sol'4. sol'8 | sol'2 }
>> r2 |
R1*4 |
do''1\fp~ |
do''-\sug\cresc~ |
do''~ |
do'' |
<>\fp \twoVoices #'(corno1 corno2 corni) <<
  { sol'2. do''4 |
    mi'' do'' mi'' do'' |
    sol'2 re''4. re''8 |
    re''2 re'' |
    do'' do''4. do''8 |
    do''2 }
  { sol'2. do'4 |
    mi' do' mi' do' |
    sol2 sol'4. sol'8 |
    sol'2 sol' |
    mi' mi'4. mi'8 |
    mi'2 }
  { s1*2 | s1-\sug\ff }
>> r2 |
R1 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { sol'4 mi' do' | }
  { sol'4 mi' do' | }
>>
sol'1 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol'4. mi''8 mi''4. mi''8 |
    re''2 re'' |
    do''~ do''4. do'8 |
    do'2 do' |
    do' }
  { sol'4. do''8 do''4. do''8 |
    do''2 sol' |
    mi'~ mi'4. do'8 |
    do'2 do' |
    do' }
>> r2 |
R1 |
