\clef "treble"
<>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { do'''2 do'''4. do'''8 | do'''2. }
  { mi''2 mi''4. mi''8 | mi''2. }
>> r4 |
R1*4 |
r8 re''-\sug\f mi'' fa'' sol'' la'' si'' do''' |
re''' do''' si'' la'' sol''-\sug\p fa'' mi'' re'' |
mi''2 r |
R1 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''2 mi''4. mi''8 | do'''4 do'' mi'' sol'' | do''2 }
  { sol'2 sol'4. sol'8 | mi''4 do'' mi'' sol'' | do''2 }
  { s1-\sug\ff | s1 | s2-\sug\p }
>> r2 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { do''4 do''8 do'' mi''4 re'' |
    dod''2 dod''4. dod''8 |
    dod''4 dod'' mi'' dod'' |
    la' la' si' dod'' | }
  { do''4 do''8 do'' mi''4 re'' |
    dod''2 dod''4. dod''8 |
    dod''4 dod'' mi'' dod'' |
    la' la' si' dod'' | }
  { s4 s2.-\sug\f | s1-\sug\p }
>>
re'1-\sug\f |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { re'4 }
  { re' }
>> r4 r2 |
R1*2 |
r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { re''4 fad'' la'' |
    do'''1~ |
    do''' |
    la''4 si'' do''' re''' |
    si'' si' re'' si' |
    sol' }
  { re''4 fad'' la'' |
    fad''1~ |
    fad'' |
    fad''4 sol'' la'' fad'' |
    sol'' si' re'' si' |
    sol' }
  { s2.-\sug\f | s1-\sug\f | s2 s-\sug\p | s1 | s4 s2.\f }
>> r4 r2 |
R1 |
r4 <>-\sug\p \twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''2 fa''4 | mi''2 }
  { si'2 si'4 | do''2 }
>> r2 |
R1*2 |
<<
  \tag #'(oboe1 oboi) {
    r8^"solo" re''-\sug\f mi'' fa'' sol'' la'' si'' do''' |
    re''' do''' si'' la'' sol''-\sug\p fa'' mi'' re'' |
  }
  \tag #'oboe2  R1*2
>>
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''4 do'''8 do''' do'''4 mi'' |
    re''2 sol'' |
    do'2. re'8 mi' |
    fa' mi' fa' sol' la' sol' la' si' |
    do''2 }
  { do''4 mi''8 mi'' mi''4 do'' |
    do''2 si' |
    do'2. re'8 mi' |
    fa' mi' fa' sol' la' sol' la' si' |
    do''2 }
  { s4 s2-\sug\f s4-\sug\p | s1 | s1*2\f | s4-\sug\fp }
>> r2 |
<>-\sug\fp \twoVoices#'(oboe1 oboe2 oboi) << { la''2 } { fa'' } >> r2 |
\twoVoices#'(oboe1 oboe2 oboi) << { sol''2 } { mi'' } >> r2 |
\twoVoices#'(oboe1 oboe2 oboi) << { sol''2 } { mi'' } >> r2 |
R1*2 |
r4 <>-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''8 mi'' mi''2:8 | mi''4 }
  { do''8 do'' do''2:8 | do''4 }
>> r4 r2 |
R1*2 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''1 | }
  { la' | }
>>
R1*2 |
r2 \twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''2 |
    mi''1 |
    mi'' |
    fa''2 fa''4. fa''8 |
    fa''4 }
  { re''2 |
    re''1 |
    dod'' |
    re''2 la'4. la'8 |
    la'4 }
  { s2 | s1-\sug\fp | s-\sug\fp | s-\sug\ff }
>> r4 r2 |
R1*6 |
R1^\fermataMarkup | \allowPageTurn
<>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''1 | mi''2 sold'' | la'' }
  { mi'1 | mi'2 si' | do'' }
>> r2 |
R1 |
<>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { fad''1~ | fad''4 }
  { red''1~ | red''4 }
>> r4 r2 |
R1*2 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mi''1 | red'' | sol''2 sol''4. sol''8 | sol''2 }
  { sol'1 | fad' | si'2 si'4. si'8 | si'2 }
  { s1*2 | s1-\sug\ff }
>> r2 |
\tag #'oboi <>^"[a 2]" si'1\fp~ |
si' |
do''\fp~ |
do''4 dod''2 dod''4 |
re''1~ |
re'' |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { re''4 si' re'' si' | sol' }
  { re''4 si'\f re'' si' | sol' }
>> r4 r2 |
R1*2 |
r4 <>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { do'4 mi' fa' | sol' sol' la' si' | }
  { do'4 mi' fa' | sol' sol' la' si' | }
  { s2. | s4 s2.-\sug\p | }
>>
r4 <>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { sol'4 do'' mi'' |
    sol'' mi'' re'' do'' |
    sol''2 sol''4. sol''8 |
    sol''2 }
  { sol'4 do'' mi'' |
    sol'' mi'' re'' do'' |
    si'2 si'4. si'8 |
    si'2 }
>> r2 |
R1*7 |
r2\fermata r |
R1*8 |
<>\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { do''1~ | do''4 }
  { mi'1~ | mi'4 }
>> r4 r2 |
R1*2 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { do''1~ | do''4 }
  { mi'1~ | mi'4 }
>> r4 r2 |
R1*10 |
<>\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''2 sol''4. sol''8 | sol''2 }
  { si'2 si'4. si'8 | si'2 }
>> r2 |
R1*4 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { do'''1~ | do'''~ | do''' | do''' |
    do'''~ | do''' | si''~ | si'' |
    do'''2 do'''4. do'''8 | do'''2. do'''4 |
    si''4 la'' sol'' fa'' | mi'' sol'' mi'' do'' |
    si' re''8 do'' si' la' sol' fa' | mi'4. do'''8 do'''4. mi''8 |
    re''2 re'' | do''~ do''4. do'8 | do'2 do' | do' }
  { mi''1~ | mi''( | fa'') | la'' |
    mi''~ | mi'' | re''~ | re'' |
    mi''2 mi''4. mi''8 | mi''2. do'''4 |
    si''4 la'' sol'' fa'' | mi'' sol'' mi'' do'' |
    si' re''8 do'' si' la' sol' fa' | mi'4. mi''8 mi''4. do''8 |
    do''2 si' | do''2~ do''4. do'8 | do'2 do' | do' }
  { s1-\sug\fp | s-\sug\cresc | s1*2 | s1*2-\sug\fp | s1-\sug\ff }
>> r2 |
R1 |
