\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        \consists "Metronome_mark_engraver"
        \oboiInstr
      } << \global \keepWithTag #'oboi \includeNotes "oboi" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes in C }
        shortInstrumentName = "Tr."
        \haraKiri
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'trombe \includeNotes "trombe"
        { \noHaraKiri s1*105 \revertNoHaraKiri }
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors in C }
        shortInstrumentName = "Cor."
        \haraKiriFirst
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
        { s1*107 <>^"Cors" }
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "basso"
      >>
      \new Staff \with { \timpaniInstr } <<
        \keepWithTag #'() \global \includeNotes "timpani"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \with {
        shortInstrumentName = \markup\character Ta.
      } \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      \new Staff \with {
        shortInstrumentName = \markup\character Al.
      } \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
      \new Staff \with {
        shortInstrumentName = \markup\character At.
      } \withLyrics <<
        \global \keepWithTag #'voix4 \includeNotes "voix"
      >> \keepWithTag #'voix4 \includeLyrics "paroles"
    >>
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \keepWithTag #'basso \includeNotes "basso"
      \origLayout {
        s1*6\break s1*8\pageBreak
        s1*5\break s1*5\pageBreak
        s1*8\break s1*8\pageBreak
        s1*8\break s1*8\pageBreak
        s1*8\break s1*7\pageBreak
        s1*8\break s1*7\pageBreak
        s1*6\break s1*7\pageBreak
        s1*9\pageBreak
        s1*8\pageBreak
        s1*8\pageBreak
        s1*7\pageBreak
        s1*8\pageBreak
      }
      \modVersion { s1*106\break }
      \modVersion { \ru#13 { s1\noPageBreak } }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
