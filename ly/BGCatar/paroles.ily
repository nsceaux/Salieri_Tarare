\tag #'(voix1 basse) {
  Brave Al -- ta -- mort, a -- vant le point du jour,
  de -- main qu’une es -- ca -- dre soit prê -- te
  à par -- tir du pied de la tour.
  Suis mon sol -- dat, sers son a -- mour
  dans les com -- bats, dans la tem -- pê -- te.

  S’il re -- voit ja -- mais ce sé -- jour,
  tu m’en ré -- pon -- dras sur ta te -- te.

  Et toi, __ jus -- qu’à cet -- te con -- que -- te,
  de tout ser -- vice en -- vers ton Roi,
  sol -- dat, je dé -- ga -- ge ta foi ;
  j’en ju -- re par Bra -- ma.

  Je jure en sa pré -- sen -- ce,
  de ne po -- ser ce fer san -- glant,
  qu’a -- près a -- voir, du plus lâ -- che bri -- gand,
  pu -- ni le cri -- me, et ven -- gé mon of -- fen -- se.

  Tu viens d’en -- ten -- dre son ser -- ment ;
  il touche a plus d’une e -- xis -- ten -- ce :
  vole, Al -- ta -- mort, et plus prompt que le vent,
  re -- viens jou -- ir de ma re -- con -- nais -- san -- ce.

  No -- ble Roi, re -- çois le ser -- ment
  de ma plus prompte o -- bé -- is -- san -- ce.
  Com -- mande, A -- tar, je cours a -- veu -- glé -- ment
  ser -- vir l’a -- mour, la haine ou la ven -- gean -- ce.

  De son dan -- ger, se -- crè -- te -- ment,
  il faut lui don -- ner con -- nais -- san -- ce.
  Qui sert mon maître, et le sert pru -- dem -- ment,
  peut bien comp -- ter sur sa
  \tag #'basse { mu -- ni -- fi -- cen --   }
  \tag #'voix1 { mu -- ni -- fi -- cen -- ce. }
}
\tag #'voix1 {
  De son dan -- ger, se -- crè -- te -- ment,
  il faut lui don -- ner con -- nais -- san -- ce.
  De son dan -- ger,
  il faut lui don -- ner con -- nais -- san -- ce.
}
\tag #'(voix2 basse) {
  Bra -- ma __ je jure en ta pré -- sen -- ce,
  de ne po -- ser ce fer san -- glant,
  qu’a -- près a -- voir, du plus lâ -- che bri -- gand,
  pu -- ni le crime, et ven -- gé mon of -- fen -- se,
  pu -- ni le crime, et ven -- gé mon of -- fen -- se,
  et ven -- gé mon of -- fen -- se.
}
\tag #'voix3 {
  Com -- mande, A -- tar, je cours a -- veu -- glé -- ment
  ser -- vir l’a -- mour, la haine ou la ven -- gean -- ce.
}
\tag #'voix4 {
  Tu viens d’en -- ten -- dre son ser -- ment ;
  il touche a plus d’une e -- xis -- ten -- ce :
  Vole, Al -- ta -- mort,
  vole, Al -- ta -- mort, et plus prompt que le vent,
  re -- viens jou -- ir de ma re -- con -- nais -- san -- ce.
}
