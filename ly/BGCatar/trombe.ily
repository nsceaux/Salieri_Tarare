\clef "treble"
<>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2 do''4. do''8 | do''2. }
  { mi'2 mi'4. mi'8 | mi'2. }
>> r4 |
R1*8 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2 do''4. do''8 | do''4 do' mi' sol' | do''2 }
  { mi'2 mi'4. mi'8 | mi'4 do' mi' sol' | do''2 }
  { s1-\sug\ff | s1 | s2-\sug\p }
>> r2 |
R1*8 |
r4 <>\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8 re'' re''4 re'' | }
  { re''8 re'' re''4 re'' | }
>>
re''1-\sug\ff~ |
re''1*1/2 s2\p |
re''1 |
re''4 r r2 |
R1 |
r4 <>-\sug\p \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8 re'' re''4 re'' |
    re'' re'' re'' re'' |
    mi''2 }
  { sol'8 sol' sol'4 sol' |
    sol' sol' sol' sol' |
    do''2 }
>> r2 |
R1*2 |
<>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''1~ | re''2 }
  { sol'1~ | sol'2 }
>> r2 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2. mi''4 | re''2 re'' | }
  { do''2. do''4 | do''2 sol' | }
  { s2.\f s4\p }
>>
do'1 |
R1 |
<>-\sug\fp \twoVoices #'(tromba1 tromba2 trombe) << { do''2 } { do'' } >> r2 |
<>-\sug\fp \twoVoices #'(tromba1 tromba2 trombe) << { do''2 } { do'' } >> r2 |
\twoVoices #'(tromba1 tromba2 trombe) << { do''2 } { do'' } >> r2 |
\twoVoices #'(tromba1 tromba2 trombe) << { do''2 } { do'' } >> r2 |
R1*2 |
r4 <>-\sug\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''8 do'' do''2:8 | do''4 }
  { mi'8 mi' mi'2:8 | mi'4 }
>> r4 r2 |
R1*2 |
do''1 |
R1*5 |
<>-\sug\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''2 re''4. re''8 | re''4 }
  { re''2 re''4. re''8 | re''4 }
>> r4 r2 |
R1*6 |
R1^\fermataMarkup |
<>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''1 | mi''2. mi''4 | do''2 }
  { mi'1 | mi'2. mi'4 | mi'2 }
>> r2 |
R1*7 |
<>-\sug\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''2 mi''4. mi''8 | mi''2 }
  { mi'2 mi'4. mi'8 | mi'2 }
>> r2 |
R1*8 | \allowPageTurn
r4 <>\fp \twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'8 sol' sol'2~ | sol'1 | sol'4 }
  { sol'8 sol' sol'2~ | \tag #'trombe \hideNotes sol'1 |
    \tag #'trombe \unHideNotes sol'4 }
>> r4 r2 |
R1 |
r4 <>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'4 do'' mi'' |
    sol'' mi'' re'' do'' |
    re''2 re''4. re''8 |
    re''2 }
  { sol'4 do'' mi'' |
    sol'' mi'' re'' do'' |
    sol'2 sol'4. sol'8 |
    sol'2 }
>> r2 |
R1*7 |
r2^\fermata r |
R1*49 |
