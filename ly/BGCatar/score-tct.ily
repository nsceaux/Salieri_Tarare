\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Trompettes in C }
      shortInstrumentName = "Tr."
      \haraKiri
    } <<
      \keepWithTag #'() \global
      \keepWithTag #'trombe \includeNotes "trombe"
      { \noHaraKiri s1*105 \revertNoHaraKiri }
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Cors in C }
      shortInstrumentName = "Cor."
      \haraKiri
    } <<
      \keepWithTag #'() \global
      \keepWithTag #'corni \includeNotes "corni"
      { s1*107\break <>^"Cors" }
    >>
    \new Staff \with { \timpaniInstr } <<
      \keepWithTag #'() \global \includeNotes "timpani"
    >>
  >>
  \layout {
    indent = \largeindent
    short-indent = 10\mm
  }
}
