\clef "treble"
<<
  \tag #'violino1 {
    <sol' mi'' do'''>2\f <mi'' do'''>4. q8 |
    <sol' mi'' do'''>2. do'''4\p |
    si''8 do''' la'' si'' sol'' la'' fa'' sol'' |
    mi''4    
  }
  \tag #'violino2 {
    <sol mi' do'' mi''>2-\sug\f <sol' mi''>4. q8 |
    q2. do''4-\sug\p |
    si'8 do'' la' si' sol' la' fa' sol' |
    mi'4
  }
>> mi'8 mi' mi'4 mi' |
fa'8-! fa'([ sol' fa']) la'-! la'( si' la') |
<<
  \tag #'violino1 {
    re''4 fa''8 mi'' re'' mi'' do'' re'' |
    si' re''\f mi'' fa'' sol'' la'' si'' do''' |
    re''' do''' si'' la'' sol''\p fa'' mi'' re'' |
    mi''4 <mi'' do'''>8 q q4 mi'' |
    re''2 re''4 re'' |
  }
  \tag #'violino2 {
    la2 la' |
    <si re'>1-\sug\f~ |
    q2 <re' si'>4-\sug\p q |
    <mi' do''> <sol' mi''>8 q q4 do'' |
    <fa' do''>2 <re' si'> |
  }
>>
<sol' mi'' do'''>2\ff <mi'' do'''>4. q8 |
<sol' mi'' do'''>4 do'' mi'' sol'' |
<mi'' do'''>2\p q4. q8 |
q4 <<
  \tag #'violino1 {
    do'''8\f do''' mi'''4 re''' |
    dod'''2\p dod'''4. dod'''8 |
    dod'''4 dod''' mi''' dod''' |
    la'' la'' si'' dod''' |
  }
  \tag #'violino2 {
    do''8-\sug\f do'' mi''4 re'' |
    dod''2-\sug\p dod''4. dod''8 |
    dod''4 dod'' mi'' dod'' |
    la' la' si' dod'' |
  }
>>
re'8\f mi'16 fad' sol' la' si' dod'' re'' mi'' fad'' sol'' la'' si'' dod''' re''' |
<<
  \tag #'violino1 {
    re'16\pp re' re' re' re' re' re' re' re'2:16 |
    re':16 re':16 |
    re':16 re':16 |
    re'4 re''8\f re'' fad''4 la'' |
    do'''8\ff re'' do''' re'' do''' re'' do''' re'' |
    do''' re'' do''' re'' do''' re'' do'''\p re'' |
    la''8 re'' si'' re'' do''' re'' re''' re'' |
    si''4
  }
  \tag #'violino2 {
    la16-\sug\pp la la la la4:16 la2:16 |
    la:16 la:16 |
    la:16 la:16 |
    la4 re'8-\sug\f re' fad'4 la' |
    la'-\sug\ff la''8 re'' la'' re'' la'' re'' |
    la'' re'' la'' re'' la'' re'' la''-\sug\p re'' |
    fad'8 re' sol' re' la' re' fad' re' |
    sol'4
  }
>> si8\f si re'4 si |
sol <<
  \tag #'violino1 {
    si''4\p si'' do''' |
    re'''2. re'''4 |
    re'''2. fa''4 |
  }
  \tag #'violino2 {
    sol8-\sug\p sol' sol sol' la sol' |
    si sol' si sol' si sol' si sol' |
    si sol' si sol' si sol' si sol' |
  }
>>
mi''2 r4 mi' |
fa'8-! fa'( sol' fa') la'-! la'( si' la') |
<<
  \tag #'violino1 {
    re''!4 fa''8( mi'' re'' mi'' do'' re'') |
    si' re''\f mi'' fa'' sol'' la'' si'' do''' |
    re''' do''' si'' la'' sol''\p fa'' mi'' re'' |
    mi''4 <mi'' do'''>8\f q q4 mi''\p |
    re''2 <re' si' sol''>2 |
  }
  \tag #'violino2 {
    la'4. sol'8 fa'4 mi' |
    <si re'>1-\sug\f~ |
    q2 <re' si'>2-\sug\p |
    <mi' do''>4 <sol' mi''>8-\sug\f q q4 do''-\sug\p |
    do''2 <re' si'> |
  }
>>
do'2.\f re'8 mi' |
fa' mi' fa' sol' la' sol' la' si' |
do''\fp mi'' sol'' mi'' sol'' mi'' sol'' mi'' |
do''\fp fa'' la'' fa'' la'' fa'' la'' fa'' |
do'' mi'' sol'' mi'' do'' mi'' sol'' mi'' |
<<
  \tag #'violino1 {
    do''4 do''\f re'' mi'' |
    fa''\p la''8 fa'' re''4 fa''8 re'' |
    si'4 re''8 do'' si' la' sol' fa' |
    mi'4 <mi'' do'''>8\ff q q2:8 |
    q4 do'' do'' do'' |
    la'2 do''4. do''8 |
    re''4. re''8 mi''4. mi''8 |
  }
  \tag #'violino2 {
    <sol mi'>4 sol'-\sug\f fa' mi' |
    <re' sol>1-\sug\p~ |
    q |
    <sol mi'>4 <sol' mi''>8-\sug\ff q q2:8 |
    q2 r4 mi'8 fa'16 sol' |
    do'2 r4 fa' |
    fa' fa'8 sol'16 la' sib'4. sib'8 |
  }
>>
fa''8 fa' sol' la' sib' do'' re'' mi'' |
fa''4 <<
  \tag #'violino1 {
    fa''4\p mi'' re'' |
    dod''4\f dod''8 dod'' dod''4 dod'' |
    dod''2 re''4 re'' |
    <<
      { mi''4 mi''8 mi'' mi''4 mi'' | } \\
      { <sol' sib'>4\ff sib'8 sib' sib'4 sib' }
    >>
    <la' la''>2.\fp la''4 |
    <la' fa'' re'''>2\ff <fa'' re'''>4. q8 |
    q4 fa'\p mi' re' |
    mi'4 fa' sol' fa' |
    mi' fa' sol' mi' |
    fa' sol' la' sol' |
    fa' sol' la' sol' |
    fa' la' fa' la' |
    re'' do''8 re'' si' do'' la' si' |
    sold'1\fermata |
  }
  \tag #'violino2 {
    fa'4-\sug\p sol' la' |
    <sol' sib'>4-\sug\f q8 q q4 q |
    <<
      { sib'2 la'4 fa'' |
        mi''2:8 mi'':8 |
        mi'':8 mi'':8 | } \\
      { sol'2 fa'4 re'' |
        re''2:8-\sug\fp re'':8 |
        dod'':8-\sug\fp dod'':8 | }
    >>
    <re' la' fa''>2-\sug\ff <la' fa''>4. q8 |
    q4 la-\sug\p la la |
    la la la la |
    la la la la |
    la la la la |
    la la la la |
    la la la la |
    fa' mi' re' do' |
    si1\fermata |
  }
>>
mi'4\f fad'8 sold' la' si' dod'' red'' |
mi'' fad'' sold'' la'' si'' sold'' mi'' re'' |
do'' la' do'' mi''\p la'' mi'' do'' mi'' |
la'' mi'' do'' mi'' la'' mi'' do'' la' |
si'4\f dod''8 red'' mi'' fad'' sold'' la'' |
si''4 <<
  \tag #'violino1 {
    si''4\p dod''' red''' |
    mi'''2:8 mi''':8\cresc |
    mi''':8 mi''':8 |
    mi'''2:8 mi''':8 |
    red''':8 red''':8 |
    <mi'' mi'''>2\ff q4. q8 |
    q4
  }
  \tag #'violino2 {
    red''4-\sug\p mi'' fad'' |
    si'8 si'' si'' si'' si''2:8-\sug\cresc |
    do''':8 do''':8 |
    <si' sol''>2:8 q:8 |
    <si' fad''>2:8 q:8 |
    <si' sol''>2-\sug\ff q4. q8 |
    q4
  }
>> mi'8\p fad' \grace la'8 sol'4 fad'8 mi' |
si'\fp fad' red' si <<
  \tag #'violino1 {
    si'4 si' |
    si' red'' fad'' si' |
    do''!8\fp sol' mi' do' do''4 do'' |
    do'' dod'' dod'' dod'' |
    re'' re''8 mi'' fad''4 fad''8 sol'' |
    la''4 fad'' re'' do'' |
    si'4
  }
  \tag #'violino2 {
    fad'4 fad' |
    fad' <si fad'>2 q4 |
    sol-\sug\fp mi'8 do' sol'4 sol' |
    sol' sol'8 la' sol' la' sol' la' |
    fad' re' fad' sol' la' si' la' sol' |
    fad' la' fad' la' fad' la' fad' la' |
    sol'4
  }
>> si8\ff si re'4 si |
sol <<
  \tag #'violino1 {
    \grace la''8 sol''\p fad'' sol''4 la'' |
    si''2. do'''4 |
    re''' si'' sol'' fa''! |
    mi''
  }
  \tag #'violino2 {
    re''8-\sug\p re'' re''4 re'' |
    re''8 si' re'' si' sol'2~ |
    sol'4 <sol' sol>2 q4 |
    q4
  }
>> do'8\f do' mi'4 fa' |
sol'4 sol'8\p sol' la'4 si' |
do'' sol'8\f sol' do''4 mi'' |
sol'' mi'' re'' do'' |
<re' si' sol''>2 <<
  \tag #'violino1 { <si' sol''>4. q8 | q4 }
  \tag #'violino2 { <re' si'>4. q8 | q4 }
>> sol'4\pp si' sol' |
<<
  \tag #'violino1 {
    la'4 si' do'' si' |
    la' si' do'' la' |
    si' re'' si' re'' |
    si' re'' si' re'' |
    mib''8 mib' mib' mib' mib'2:8 |
    mib':8 mib':8 |
    re'2
  }
  \tag #'violino2 {
    re'4 re' re' re' |
    re' re' re' re' |
    re' re' re' re' |
    re' re' re' si' |
    do''8 do' do' do' do'2:8 |
    do':8 do':8 |
    si2
  }
>> <re' si' sol''>2\f |
r4\fermata r <<
  \tag #'violino1 {
    re''2\p |
    mi''2 sol''4 mi'' |
    do''2 mi''4 do'' |
    la'2. \grace si'8 la' sol'16 la' |
    sol'4 re'' mi'' fa'' |
    mi''2. fa''4 |
    sol''4. mi''8 la''4. fa''8 |
    mi''2:8 mi'':8 |
    mi'':8 re'':8 |
    <do'' mi'>1\f~ |
    q4 do'' re'' mi'' |
    fa''8 sol'' la'' fa'' re'' mi'' fa'' re'' |
    si'4 re''8 do'' si' sol' la' si' |
    <mi' do''>1~ |
    q4 do''\p re'' mi'' |
    fa''8 sol'' la'' fa'' re'' mi'' fa'' re'' |
    si'4 re''8 do'' si' sol' la' si' |
    do'' sol' mi' sol' do'' sol' mi' sol' |
    do'' mi' do'' mi'
  }
  \tag #'violino2 {
    si2-\sug\p |
    do'1 |
    mi' |
    fa'4 mi' re' do' |
    si1 |
    do'4 sol do' re' |
    mi'4. do'8 fa'4. re''8 |
    do''2:8 do'':8 |
    do'':8 si':8 |
    do''8\f <sol mi'> q q q2:8 |
    q4 mi'8 do' fa' do' sol' do' |
    la'2 fa' |
    fa'2.\trill mi'8 fa' |
    mi' do' re' mi' fa' sol' la' si' |
    do'' sol' mi'-\sug\p do' fa' do' sol' do' |
    la'2 fa' |
    fa'2.\trill mi'8 fa' |
    <sol mi'>2:8 q:8 |
    q4 do''8 mi'
  }
>> do''8 mi' do'' mi' |
si' mi' si' mi' si' mi' si' mi' |
si' mi' si' mi' si' mi' si' mi' |
do'' mi' do'' mi' do'' mi' do'' mi' |
dod'' mi' dod'' mi' dod'' mi' dod'' mi' |
re'' la' la' la' mi'' la' la' la' |
fa'' la' la' la' fa'' la' la' la' |
<<
  \tag #'violino1 {
    si'8\f re'' sol'' re'' si' re'' sol'' re'' |
    si' re'' re'' re'' re''2:8\p |
    mi'':8 mi'':8 |
    fa'':8 la'':8 |
    mi'':8 mi'':8 |
    re'':8 re'':8 |
    mi''8\fp do'''[ do''' do'''] do'''2:8 |
    do''':8 do''':8\cresc |
    do''':8 do''':8 |
    do''':8 do''':8 |
  }
  \tag #'violino2 {
    <re' si'>2:8-\sug\f q:8 |
    q8 si[ si si] si-\sug\p si' si' si' |
    do''2:8 do'':8 |
    do'':8 do'':8 |
    do'':8 do'':8 |
    si':8 si':8 |
    <mi' do''>2:8-\sug\fp q:8 |
    mi'':8 mi'':8-\sug\cresc |
    fa'':8 fa'':8 |
    la'':8 la'':8 |
  }
>>
<mi'' do'''>2:8\fp q2:8 |
q2:8 q2:8 |
<re'' si''>:8\ff q2:8 |
q2:8 q2:8 |
<sol' mi'' do'''>2 <mi'' do'''>4. q8 |
q2. do'''4 |
si''8 do''' la'' si'' sol'' la'' fa'' sol'' |
mi''4 sol'' mi'' do'' |
si' re''8 do'' si' la' sol' fa' |
mi'4. <<
  \tag #'violino1 {
    do'''8 do'''4. mi''8 |
    re''2
  }
  \tag #'violino2 {
    do''8 do''4. do''8 |
    do''2
  }
>> <re' si' sol''>2 |
do''2~ do''4. do'8 |
do'2 do' |
do'2~ do'4 sol8 la16 si |
do'4. re'8 re'4.\trill do'16 re' |
