\clef "bass" do2-\sug\f do4. do8 |
do2 r |
R1*8 |
do2-\sug\ff do4. do8 |
do2 r |
R1*5 |
do2:16-\sug\f do:16 |
do4 r r2 |
R1*35 |
do2-\sug\ff do4. do8 |
do4 r r2 |
R1*6 |
R1^\fermataMarkup |
R1*10 |
sol,2-\sug\ff sol,4. sol,8 |
sol,2 r |
sol, r |
R1*13 |
sol,2 sol,4. sol,8 |
sol,2 r |
R1*7 |
r2\fermata r |
R1*8 |
r2 do\f |
R1*15 |
sol,2-\sug\f sol,4. sol,8 |
sol,2 r |
R1*8 |
sol,2 r |
R1*3 |
do2-\sug\ff do4. do8 |
do2 r |
R1*3 |
r4 r8 do do4. do8 |
do2 sol, |
do r4 r8 do |
do2 do |
do r |
R1 |
