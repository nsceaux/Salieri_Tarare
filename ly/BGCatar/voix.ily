<<
  \tag #'(voix1 basse) {
    \clef "bass" <>^\markup\character-text Atar Ton brillant
    \tag #'voix1 \set Staff.shortInstrumentName = \markup\character At.
    do'2 do'4. do'8 |
    do'2. do'4 |
    si la sol fa |
    mi2 r4 mi |
    fa2 la4. la8 |
    re'2 re'4 do' |
    si2 si |
    r re'4 re' |
    mi'2. do'4 |
    fa2 sol4. sol8 |
    do2 r |
    R1 |
    do'2 do'4 do' |
    do'2 r |
    dod'2 dod'4. dod'8 |
    dod'4 dod' mi' dod' |
    la la si dod' |
    re'1~ |
    re'4 re4 r r8^"Récit" re8*1/2 re |
    re8.^\markup\italic { bas à Altamort } re16 re8 re16 mi fad4 r |
    la8 la la la fad4 fad8 la |
    re4 re r re^\markup\italic { à Tarare } |
    do'1~ |
    do'2 do'4 do' |
    la4( si) do' re' |
    si2 si |
    r4 si si do' |
    re'2. re'4 |
    re'2. fa4 |
    mi2 r4 mi |
    fa2 la4. la8 |
    re'2 re'4. re'8 |
    si2 r |
    r re' |
    mi'2. do'4 |
    fa2 sol |
    do r |
    \ffclef "tenor/G_8" <>^\markup\character Tarare
    \tag #'voix1 \set Staff.shortInstrumentName = \markup\character Ta.
    r2 r4 sol |
    do'2. do'4 |
    do'2. do'4 |
    mi'2 do' |
    r4 do' re' mi' |
    fa'2 re' |
    si fa |
    mi r |
    r4 do' do' do' |
    la2 do'4. do'8 |
    re'2 mi'4. mi'8 |
    fa'1 |
    r4 fa' mi' re' |
    dod'2 dod' |
    r re'4 re' |
    mi'1 |
    la'2. la'4 |
    fa'2 re' |
    \ffclef "bass" <>^\markup\character-text Atar à Altamort
    \tag #'voix1 \set Staff.shortInstrumentName = \markup\character At.
    r4 fa mi re |
    la2. la4 |
    la2. la4 |
    fa2 r |
    r la |
    re'2. re'4 |
    re' do' si la |
    sold2 sold |
    mi'1 |
    mi'2. mi'4 |
    do'2 la4 la |
    la2 si4 do' |
    si1 |
    r4 si dod' red' |
    mi'2. mi'4 |
    mi' do' do' la |
    si1~ |
    si |
    mi2 r |
    \ffclef "bass" <>^\markup\character Altamort
    \tag #'voix1 \set Staff.shortInstrumentName = \markup\character Al.
    r2 sol4 mi |
    si2. si4 |
    si2 si4 si |
    do'!1~ |
    do'4 dod' dod' dod' |
    re'2. re'4 |
    re'2. re'4 |
    si2 si |
    r4 sol sol la |
    si2. do'4 |
    re' si sol fa! |
    mi2 r |
    r4 sol la si |
    do'2. mi'4 |
    sol' mi' re' do' |
    si2 sol |
    \ffclef "alto/G_8" <>^\markup\character-text Calpigi à part
    \tag #'voix1 \set Staff.shortInstrumentName = \markup\character Ca.
    r4 sol si sol |
    re'2. re'4 |
    re'2. re'4 |
    si1 |
    r2 re' |
    mib'2 mib'4. mib'8 |
    mib'2 mib'4. mib'8 |
    re'2 si |
    r4\fermata <>^\markup\italic haut re'4 re' re' |
    mi'!2 sol'4 mi' |
    do'2 mi'4 do' |
    la1 |
    r4 re' mi' fa' |
    mi'2. fa'4 |
    sol'4. mi'8 la'4. fa'8 |
    mi'1~ |
    <<
      \tag #'basse { mi'2 re'4 }
      \tag #'voix1 {
        <>^\markup\character Calpigi mi'2\melisma re'\melismaEnd |
        do' r |
        \noHaraKiri R1*12 |
        r4 la la la |
        la2. la4 |
        la2. la4 |
        sol1 |
        r2 sol |
        do'2 do'4 do' |
        do'2 do'4 do' |
        mi'1\melisma |
        re'\melismaEnd |
        mi'2 r |
        R1 |
        r4 do' do' do' |
        fa'2. fa'4 |
        mi'2 mi'4 mi' |
        mi'2 mi'4 mi' |
        re'1~ |
        re' |
        do'2 r |
        R1*10 |
      }
    >>
  }
  \tag #'(voix2 basse) {
    <<
      \tag #'basse {
        s1*106 s2. \ffclef "tenor/G_8" <>^\markup\character Tarare
      }
      \tag #'voix2 {
        \clef "tenor/G_8" R1*106 |
        \noHaraKiri <>^\markup\character Tarare
        r2 r4
      }
    >> sol4 |
    do'1~ |
    do'2. do'4 |
    fa'2 re' |
    si2. sol4 |
    do'2 do' |
    r4 do' re' mi' |
    fa'2 re' |
    si sol |
    do'1 |
    r4 do' do' re' |
    mi'2 re'4 do' |
    si2 do'4 re' |
    do'1 |
    r4 dod' dod' dod' |
    re'2 mi'4 mi' |
    fa'2 re'4 do' |
    si2 si |
    r4 re' re' re' |
    mi'2 do'4 do' |
    fa'2 la'4 la' |
    sol'1~ |
    sol' |
    mi'2 r |
    mi' mi' |
    fa'1 |
    la'2. la'4 |
    sol'1~ |
    sol'~ |
    sol'~ |
    sol' |
    do'2 r |
    R1*10 |
  }
  \tag #'voix3 {
    \clef "bass" R1*106 |
    <>^\markup\character Altamort
    R1 \noHaraKiri R1*21 |
    r4 si si si |
    do'2. do'4 |
    do' do' do' do' |
    do'1 |
    r4 do' do' do' |
    do'2. do'4 |
    do' do' do' do' |
    si1~ |
    si |
    do'2 r |
    R1*10 |
  }
  \tag #'voix4 {
    \clef "bass" R1*106 |
    <>^\markup\character Atar
    R1 \noHaraKiri R1*5 |
    r4 mi fa sol |
    la2 fa |
    fa fa |
    mi2. sol4 |
    do'2. do'4 |
    sold2. si4 |
    mi'2. mi'4 |
    la2 la |
    R1*3 |
    re'2 re'4 re' |
    sol2 r |
    R1*3 |
    sol2 sol4 sol |
    do'2 do'4 do' |
    sib2 sib4 sib |
    la1 |
    r4 fa fa fa |
    sol2. do'4 |
    mi' do' mi' do' |
    sol1~ |
    sol |
    do2 r |
    R1*10 |    
  }
>>
