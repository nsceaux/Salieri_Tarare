\clef "treble"
\twoVoices#'(tromba1 tromba2 trombe) <<
  { do''2 }
  { do' }
>> r2 |
r2\fermata r |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { re''4 }
  { re'' }
>> r4 r2 |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { sol'4 }
  { sol' }
>> r4 r2 |
r4 <>-\sug\ff \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''2 do''4 | do'' }
  { do''2 do''4 | do'' }
>> r4 r2 |
R1*3 |
do''1 |
R1 |
<>\p \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''2 re''4. do''8 | do''4 }
  { do''2 sol'4. mi'8 | mi'4 }
>> r4 r2 |
r4 \twoVoices#'(tromba1 tromba2 trombe) <<
  { do'2 do'4 | do'2 }
  { do'2 do'4 | do'2 }
>> r2 |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { do''4 }
  { mi' }
>> r4 r2 |
R1*3 |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { re''4 s re'' s | sol'' }
  { re'' s re'' s | sol' }
  { s4\p r s r }
>> r4 r2 |
R1 |
\twoVoices#'(tromba1 tromba2 trombe) <<
  { sol''1 | sol'' | re''4 re'' re'' re'' | re''2 }
  { sol'1 | sol' | re''4 re'' re'' re'' | re''2 }
  { s1-\sug\p -\sug\cresc | s1-\sug\f }
>> r2 |
<>^"unis" sol'1\fp~ |
sol'~ |
sol'4 \twoVoices#'(tromba1 tromba2 trombe) <<
  { re''2 re''4 | re''1 | re''2 }
  { sol'2 sol'4 | sol'1 | mi'2 }
  { s2.-\sug\f | s1-\sug\fp }
>> r2 |
R1*2 |
r2 \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''4. do''8 | do''1\fermata~ | do''2 }
  { do''4. do''8 | sol'1\fermata~ | sol'2 }
  { s2-\sug\f | s1\ff }
>> r2 |
r8 r16 <>-\sug\ff \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''16 do''4 }
  { do''16 do''4 }
>> r2 |
r8 r16 \twoVoices#'(tromba1 tromba2 trombe) <<
  { re''16 re''4 }
  { re''16 re''4 }
>> r2 |
r \twoVoices#'(tromba1 tromba2 trombe) <<
  { mi''2\fermata | do''8 do'' do'' do'' do''2:8 | do''4 }
  { mi''2\fermata | do''8 do'' do'' do'' do''2:8 | do''4 }
  { s2-\sug\ff | s1-\sug\f | s4-\sug\fp }
>> r4 \twoVoices#'(tromba1 tromba2 trombe) <<
  { do''2 | re'' do'' | do''4 }
  { do''2 | re'' do'' | do''4 }
  { s2-\sug\fp | s-\sug\f s-\sug\ff }
>> r4 r2 |
<>-\sug\p \twoVoices#'(tromba1 tromba2 trombe) <<
  { sol'4 }
  { sol' }
>> r4 r2 |
r2 r4 \twoVoices#'(tromba1 tromba2 trombe) <<
  { re''4 | do''2 }
  { sol'4 | do'2 }
>> r2 |
