\clef "alto" <sol mi' do''>2 r |
r2\fermata r |
re'2:16-\sug\fp re':16 |
<re' sol>:16-\sug\fp q:16 |
do'8 r la'2-\sug\ff sol'8-! fa'-! |
mi'8 do'4-\sug\p do' do' do'8 |
do'8 do'4 do' do' do'8 |
la4( dod' re') la' |
sol'1 |
mi'2:8\cresc mi':8 |
fa':8\f fa':8 |
sol':8-\sug\p sol:8 |
do'4 do'16(\ff re' mi' fa') sol'8-! fa'-! mi'-! re'-! |
do'4 do'16( si do' si \rt#4 { do' si) } |
do'2 r |
<mi' do''>2:16-\sug\fp q:16 |
q:16 <mi' la'>:16 |
<fad' la'>4 re'16-\sug\ff mi' fad' sol' la'8-! sol'-! fad'-! mi'-! |
re'16 dod' re' dod' re' dod' re' dod' \rt#4 { re' dod' } |
re'4-\sug\p fad'16 sol' la' si' do'' si' la' sol' fad' mi' re' do' |
si4 r sol8-\sug\ff la si do' |
re'2( do')-\sug\p |
sib4(-\sug\cresc la) sol2~ |
sol8-\sug\f sol'4 sol' sol' sol'8 |
la'4 re'16 la si dod' re' la si dod' re' la si dod' |
re'2 r |
sol'2:8\fp sol':8 |
sol':8 sol':8 |
sol'2*1/2:8 s4\f sol'2:8 |
sol:8\fp sol:8 |
sold8 si' si' si' si'2:8 |
si':8 si':8 |
la':8 la':8 |
la':8 fa':8-\sug\f |
mi'1-\sug\ff\fermata~ |
mi'2 r |
r8 r16 fa'-\sug\ff fa'4 r2 |
r8 r16 fa' fa'4 r2 |
r mi'-\sug\ff\fermata |
la'8-\sug\f la' la' la' mi'2:8 |
fa':16-\sug\fp fa':16-\sug\fp |
fa':16-\sug\f mi'8-\sug\ff sol' do'' do' |
fa'4 r r2 |
fa'4-\sug\p r r2 |
r2 r4 <sol re' si'> |
<sol mib' do''>2 r |
