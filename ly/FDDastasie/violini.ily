\clef "treble" <sol' mi'' do'''>2 r |
r\fermata r |
<<
  \tag #'violino1 {
    si'2:16\fp si':16 |
    si':16\fp si':16 |
    do''8 r do'''4.\ff re'''16 do''' si''8 la'' |
    sol''8 r sol''4(\p mi'' do'') |
    la'8 la'4 la' la' la'8 |
    re''4( mi'' fa'' re'') |
    do''( mi'' re''4. si'8) |
    sol''2.(\cresc sold''4) |
    la''4\f fa''( mi'' re'') |
    do''2\p <si' re'>4. q8 |
    do''4
  }
  \tag #'violino2 {
    <re' fa'>2:16-\sug\fp q:16 |
    q:16-\sug\fp q:16 |
    <do' mi'>8 r la'2-\sug\ff sol'8-! fa'-! |
    mi'8 <mi' sol>4-\sug\p q q q8 |
    <la fa'>8 q4 q q q8 |
    la'4( sol' fa' la') |
    mi'( sol' fa'4. re'8) |
    do'8-\sug\cresc do''4 do'' do'' do''8 |
    do''4-\sug\f la'( sol' fa') |
    mi'8-\sug\p sol' mi' sol' fa' re' fa' re' |
    <mi' sol>4
  }
>> do'16(\ff re' mi' fa') sol'8-! fa'-! mi'-! re'-! |
do'4 do'16( si do' si \rt#4 { do' si) } |
do'2 r |
<<
  \tag #'violino1 {
    <sol mi' do''>4-\sug\fp \grace re''8 do''16 si' do'' re'' do''4 r |
    r mi''8. mi''16 do''8. do''16 la'8. la'16 |
    fad'4
  }
  \tag #'violino2 {
    <sol mi'>2:16-\sug\fp q:16 |
    q:16 <do' mi'>:16 |
    <la fad'>4
  }
>> re'16\ff mi' fad' sol' la'8-! sol'-! fad'-! mi'-! |
<<
  \tag #'violino1 {
    re'2 r |
    <re' la' fad''>4\p r q r |
    <re' si' sol''>
  }
  \tag #'violino2 {
    re'16 dod' re' dod' re' dod' re' dod' \rt#4 { re' dod' } |
    re'4-\sug\p fad'16 sol' la' si' do'' si' la' sol' fad' mi' re' do' |
    si4
  }
>> r4 sol8:16\ff la:16 si:16 do':16 |
<<
  \tag #'violino1 {
    re'2 re''8-!\p mi''-! fad''-! re''-! |
    sol''4:16\cresc la'':16 sib'':16 si'':16 |
    do'''2:16\f dod''':16 |
    re'''4
  }
  \tag #'violino2 {
    re'2( do')-\sug\p |
    sib4(-\sug\cresc la) sol2~ |
    sol8-\sug\f sol''4 sol'' sol'' sol''8 |
    fad''4
  }
>> re'16 la si dod' re' la si dod' re' la si dod' |
re'2 r |
<<
  \tag #'violino1 {
    re''2:16\fp re'':16 |
    mi'':16 mi''4. fad''8 |
    sol''4 sol':16\f si':16 re'':16 |
    fa''!2:16\fp fa'':16 |
    mi''8 mi'' mi'' mi'' mi''2:8 |
    mi'':8 mi''2:8 |
    mi'':8 mi''2:8 |
    mi'':8 la''4.\f la''8 |
    sol''1\fermata\ff~ |
    sol''2 r^"con la voce" |
  }
  \tag #'violino2 {
    si'2:16-\sug\fp si':16 |
    do'':16 do'':16 |
    <si' re'>2*1/2:16 s4-\sug\f q2:16 |
    q:16-\sug\fp q:16 |
    <re' re''>8 re''[ re'' re''] re''2:8 |
    re''2:8 re'':8 |
    do'':8 do'':8 |
    do'':8 do'':8-\sug\f |
    do''1-\sug\ff\fermata~ |
    do''2 r |
  }
>>
r8 r16 <do'' la''>\ff q4 r2 |
r8 r16 <si' sold''> q4 r2 |
r2 <mi' si' sold''>2\fermata\ff |
<<
  \tag #'violino1 {
    la''8\f do''' do''' do''' do'''2:8 |
    la'':16\fp do''':16\fp |
    re''':16\f mi''':16\ff |
    fa'''4
  }
  \tag #'violino2 {
    <mi' do'' la''>8-\sug\f mi''[ mi'' mi''] mi''2:8 |
    do''2:16-\sug\fp la'':16-\sug\fp |
    sib'':16-\sug\f sib''16-\sug\ff( sol'' sib'' sol'' sib'' sol'' sib'' sol'') |
    la''4
  }
>> r4 r2 |
<re' si'!>4\p r r2 |
r2 r4 <sol' re'' si''> |
<sol' mib'' do'''>2 r |
