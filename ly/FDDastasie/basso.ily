\clef "bass" do2 r |
r\fermata r |
re4\fp r r2 |
sol,4\f r r2 |
do2*1/2:8 s4\f do2:8 |
do2*1/2:8 s4\p do2:8 |
fa:8 fa:8 |
fa4( mi re fa) |
sol2:8 sol,:8 |
mi:8\cresc mi:8 |
fa:8\f fa:8 |
sol:8-\sug\p sol,:8 |
do4 do16(\ff re mi fa) sol8-! fa-! mi-! re-! |
do4 do16( si, do si, \rt#4 { do si,) } |
do2 r |
do2:8\fp do:8 |
do:8 do:8 |
re4 re16\ff mi fad sol la8-! sol-! fad-! mi-! |
re4 r r2 |
re4\p r re r |
sol, r sol,8-!\ff la,-! si,-! do-! |
re2 r |
mib2:8\p\cresc mib:8 |
mib:8-\sug\f mib:8 |
re4 re16 la, si, dod re la, si, dod re la, si, dod |
re2 r |
sol2:8\fp sol:8 |
sol:8 sol:8 |
sol2*1/2:8 s4\f sol2:8 |
sol:8\fp sol:8 |
sold r |
R1 |
la2 r |
r fa4.\f fa8 |
mi1\fermata\ff~ |
mi2 r |
r8 r16 fa\ff fa4 r2 |
r8 r16 fa fa4 r2 |
r mi2\ff\fermata |
la,8\f la la la mi2:8 |
fa:8-\sug\fp fa:8-\sug\fp |
sib,:8\f do:8-\sug\ff |
fa4 r r2 |
fa4\p r r2 |
r r4 sol |
do2 r |
