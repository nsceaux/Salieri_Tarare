\clef "treble"
\twoVoices#'(oboe1 oboe2 oboi) <<
  { do''2 }
  { mi'2 }
>> r |
r\fermata r |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { si'4 }
  { fa'4 }
>> r4 r2 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { si'4 }
  { fa'4 }
>> r4 r2 |
r4 <>-\sug\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { do'''4. re'''16 do''' si''8 la'' | sol''4 }
  { la''4. si''16 la'' sol''8 fa'' | mi''4 }
>> r4 r2 |
R1*3 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { r4 sol''2\sf sold''4 |
    la'' fa''( mi'' re'') |
    do''2 re''4. si'8 |
    do''4 }
  { R1 |
    r4 la'( sol' fa') |
    mi'2 fa'4. re'8 |
    mi'4 }
  { s1*2 | s1-\sug\p }
>> \tag #'oboi <>^"unis" \once\slurDashed do'16(-\sug\ff re' mi' fa') sol'8-! fa'-! mi'-! re'-! |
do'4 do'2 do'4 |
do'2 r |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { do''4 }
  { mi' }
>> r4 r2 |
R1 |
r4 \tag #'oboi <>^"unis" re'16-\sug\ff mi' fad' sol' la'8-! sol'-! fad'-! mi'-! |
re'1~ |
re'4-\sug\p r \twoVoices#'(oboe1 oboe2 oboi) <<
  { fad''4 }
  { la' }
>> r |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { sol'' }
  { si' }
>> r4 r2 |
R1 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''4( la'' sib'' si'') |
    do'''2 dod''' |
    re'''4 }
  { sib'4( la') sol'2~ |
    sol'4 sol''2 sol''4 |
    fad'' }
  { s1-\sug\p \cresc | s-\sug\f }
>> \tag #'oboi <>^"unis" re'4 re' re' |
re'2 r |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { re''1 | mi''2~ mi''4. fad''8 | sol''4 sol''( si'' re''') | fa''!1 |
    mi''~ | mi''~ | mi''~ | mi''2 la'' |
    sol''1~\fermata | sol''2 }
  { si'1 | do'' | si'4 si'2 si'4 | si'1 |
    si'~ | si' | do''1~ | do''2 do'' |
    do''1~\fermata | do''2 }
  { s1*2\fp | s4 s2.\f | s1*4-\sug\fp | s2 s-\sug\f | s1\ff }
>> r2 |
r8 r16 <>\ff \twoVoices#'(oboe1 oboe2 oboi) <<
  { la''16 la''4 }
  { do''16 do''4 }
>> r2 |
r8 r16 \twoVoices#'(oboe1 oboe2 oboi) <<
  { sold''16 sold''4 }
  { si'16 si'4 }
>> r2 |
r \twoVoices#'(oboe1 oboe2 oboi) <<
  { sold''\fermata |
    la''8 do''' do''' do''' do'''2:8 |
    la''4 }
  { sold''2\fermata |
    do''8 mi'' mi'' mi'' mi''2:8 |
    do''4 }
  { s2-\sug\ff | s1-\sug\f | s4-\sug\fp }
>> r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { do'''2 | re''' mi'' | fa''4 }
  { la''2 | sib'' sol'' | fa''4 }
  { s2-\sug\fp | s-\sug\f s-\sug\ff }
>> r4 r2 |
<>-\sug\p \twoVoices#'(oboe1 oboe2 oboi) <<
  { si'!4 }
  { re' }
>> r4 r2 |
r2 r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { si''4 | do'''2 }
  { re''4 | mib''2 }
>> r2 |
