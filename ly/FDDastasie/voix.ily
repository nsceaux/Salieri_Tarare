\clef "soprano/treble" <>^\markup\character-text Astasie chanté
r2 do''2*1/2 s4\fermata |
do''4 sol' r do''8 do'' |
si'2 re''4 re'' |
fa''2 fa''4. sol''8 |
mi''4 mi'' r2 |
r4 sol'' mi'' do'' |
la'2 r4 la' |
re''( mi'') fa'' re'' |
do''2 re''4. si'8 |
sol''2. sold''4 |
la''( fa'') mi'' re'' |
do''2 re''4. si'8 |
do''2 r |
R1 |
r2 sol'4. sol'8 |
do''2. do''4 |
mi''2 do''4. la'8 |
fad'4 fad' r2 |
r la'8 la' la' la' |
re''2 re''4 re'' |
si'2 r |
r re''8 re'' re'' re'' |
sol''1 |
sol''2. sol''4 |
re''2 r |
r r4 re'' |
re''2 si'4. sol'8 |
mi''2 mi''4 fad'' |
sol'' re'' r2 |
fa''!2. re''4 |
si'1 |
r2 mi''4. mi''8 |
do''4 la' r2 |
r la''4. la''8 |
sol''2.( mi''4) |
do''2 r4 <>^\markup senza rigore r8 do'' |
la'2 r4 r8 la' |
re''2 r8 sold' sold' sold' |
mi'2 mi'8 r r4 |
\ffclef "bass" <>^\markup\character Atar
do'8 do' do' do' do'4 do'8 do' |
la4 r8 do' fa4 r8 do' |
re'4 re' r8 mi' mi' mi' |
fa' fa
\ffclef "soprano/treble" <>^\markup\character Astasie
do''16 do'' do'' do'' fa''4 re''8 do'' |
si'4 r8 re''16 re'' si'4. sol'8 |
si'4 si'8 do'' sol' sol'
\ffclef "bass" <>^\markup\character Atar
sol'8 sol'16 sol' |
mib'2 r |
