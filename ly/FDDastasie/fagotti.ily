\clef "bass" do2 r |
r\fermata r |
re4 r r2 |
sol,4 r r2 |
r4 do'2-\sug\ff do'4~ |
do' r r2 |
R1*6 |
r4 do16(-\sug\ff re mi fa) sol8-! fa-! mi-! re-! |
do4 do16( si, do si, \rt#4 { do si,) } |
do2 r |
do4 r r2 |
R1 |
r4 re16-\sug\ff mi fad sol la8-! sol-! fad-! mi-! |
re2 r |
r4 fad16-\sug\p sol la si do' si la sol fad mi re do |
si,4 r sol,8-\sug\ff la, si, do |
re2 do'-\sug\p |
\once\slurDashed sib4(-\sug\cresc la) sol2~ |
sol4-\sug\f mib2 mib4 |
re4 re16 la, si, dod re la, si, dod re la, si, dod |
re2 r |
R1*2 |
r4 sol2-\sug\f sol4 |
sol1-\sug\fp |
sold2 r |
R1 |
la2 r |
r fa-\sug\f |
mi1-\sug\ff\fermata~ |
mi2 r |
r8 r16 <>-\sug\ff fa fa4 r2 |
r8 r16 fa fa4 r2 |
r mi-\sug\ff\fermata |
la2-\sug\f mi |
fa-\sug\fp fa'-\sug\fp |
sib-\sug\f do'-\sug\ff |
fa4 r r2 |
fa4-\sug\p r r2 |
r2 r4 sol |
do2 r |
