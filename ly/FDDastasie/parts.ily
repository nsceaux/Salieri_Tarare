\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (oboi #:score "score-oboi")
   (fagotti #:score-template "score-part-voix" #:tag-notes fagotti)
   (trombe #:score-template "score-part-voix" #:tag-notes trombe
           #:instrument , #{\markup\center-column { Trompettes \small en Ut } #})
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
