Ô ti -- gre ! mes dé -- dains ont trom -- pé ton at -- ten -- te,
et, mal -- gré toi, je goûte un ins -- tant de bon -- heur,
je goûte un ins -- tant de bon -- heur :
j’ai bra -- vé ta faim dé -- vo -- ran -- te,
le ru -- gis -- se -- ment de ton cœur,
le ru -- gis -- se -- ment de ton cœur.
Pour prix de ta lâche en -- tre -- pri -- se,
vois, A -- tar, je l’a -- do -- re,
je l’a -- do -- re, et toi, et toi, je te mé -- pri -- se.


Ar -- ra -- chez- la tous de ses bras.
Cou -- rez. Qu’il meu -- re et qu’el -- le vi -- ve !

Si quel -- qu’un vers lui fait un pas,
je suis morte a -- vant qu’il ar -- ri -- ve.

Ar -- rê -- tez- vous !
