\clef "treble" R1 |
r2 r4 fa'8\p \grace fa'8 mib'16 re'32 mib' |
re'4 r r2 |
r2 <<
  \tag #'violino1 re'4
  \tag #'violino2 sib
>> r4 |
R1*2 |
r2 r4 <la mi'! dod''>\p |
sib8 do'16 re' mib' fa' sol' la' sib'4 r |
R1*2 |
