\clef "soprano/treble" <>^\markup\italic { d’un ton très décidé } _"parlé"
r4 r8 fa' do''4. do''8 |
la'4 la'8 sib'16 do'' do''8 fa' r4 |
r r8 la' re''4. la'8 |
do'' do'' do'' re'' sib'4 r8 re'' |
sib'4. sib'8 sib' sib' do'' re'' |
sol'4 r8 sol' dod''4. mi''8 |
dod'' dod'' dod'' re'' la' la' r4 |
\tag #'recit <>^\markup\italic { (Elle s’assied sur un sopha.) }
R1 |
r8 sib' sib' do'' re''8. re''16 mib''8 fa'' |
sib'8 sib' r4 r2 |
