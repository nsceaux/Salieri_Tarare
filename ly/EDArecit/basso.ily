\clef "bass" R1 |
r2 r4 fa8\p fa |
fad4 r r2 |
r sol4 r |
R1*2 |
r2 r4 la,\p |
sib,8 do16 re mib fa sol la sib4 r |
R1*2 |
