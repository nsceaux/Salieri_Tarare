\clef "treble" <sol mib'>2.\f sib'4 |
mib''2 sib' |
sol'4. mib'8 sol'4.\trill fa'16 mib' |
sib'4 sib' do'' re'' |
<<
  \tag #'violino1 {
    mib''2 mib''8 mib'' \grace fa'' mib'' re''16 mib'' |
    do''4.\fp mib''8 lab''4. do''8 |
    sib'4 sib''8.\f sib''16 sol''8. sol''16 mib''8. mib''16 |
    fa''4. sib'8 <re' sib' fa''>4 q |
    <mib' sib' sol''>2 r4
  }
  \tag #'violino2 {
    mib''4. sib'8 sol'4. sol'8 |
    lab'4.-\sug\fp do''8 do''4. lab'8 |
    sol' << { sib'8 sib' sib' sib'2:8 } \\ { sol'8-\sug\f sol' sol' sol'2:8 } >> |
    <fa' sib'>2:8 q:8 |
    <sol' sib'>:8 q4
  }
>> sib'8. sib'16 |
mib''4 re''8. re''16 do''4. sib'8 |
la'8 la la la la4 do''8. do''16 |
fa''4 <<
  \tag #'violino1 {
    fa'8. fa'16 fa'4 fa''4-\sug\ff |
    re''4. sib'8
  }
  \tag #'violino2 {
    la'8. la'16 do''8 la'[-\sug\ff sib' do''] |
    fa'4. sib'8
  }
>> <mib' sib' sol''>4 q |
<re' sib' fa''> <re' sib' sib''> <<
  \tag #'violino1 {
    <re' sib' fa''> r |
    r4 r8
  }
  \tag #'violino2 {
    <re' sib' fa''>8 fa'[ sol' la'] |
    sib'4.
  }
>> sib'8 <mib' sib' sol''>4 q |
<re' sib' fa''> <re' sib' sib''> <re' sib' fa''> q |
<<
  \tag #'violino1 {
    fa''4 re''8. re''16 sol''4 sol''8 sol'' |
    sol'' do''' do''' do''' do'''4
  }
  \tag #'violino2 {
    re''4 <re'' re'>8. q16 q4 q8. q16 |
    <mib' mib''>8 <mib'' sol'> q q q4
  }
>> r4 |
R1 |
r8 re'' la' fad' re'4 <re' la' fad''>4 |
<re' sib' sol''>4 <sib' sol''>8 q <re' la' fad''>4 <la' fad''>8 q |
<re' sib' sol''>8 <sib re'>[ q q] q4 <mib' sib' sol''>4 |
q <sib' sol''>8 q <re' sib' fa''>4 <<
  \tag #'violino1 {
    <sib' fa''>8 q |
    <sib' sol''>8
  }
  \tag #'violino2 {
    re'8 re' |
    mib'8
  }
>> <sib' sol''>8 q <sib' sib''> <sib' sol''>4 r |
r8 do'' re'' mi'' fa''2 |
r8 re'' fa'' re'' sib'4 <re' sib' fa''>4 |
<mib' sib' sol''>4 <sib' sol''>8 q <do'' lab''>4 q8 q |
<<
  \tag #'violino1 { fa''8 }
  \tag #'violino2 { re' }
>> <re'' sib''>4 q q q8 |
sol'' mib'[ sol' mib'] sib4 <re' sib' fa''>4 |
<mib' sib' sol''> <sib' sol''>8 q <do'' lab''>4 q8 q |
<<
  \tag #'violino1 { <sib' fa''>8 }
  \tag #'violino2 { re'8 }
>> <re'' sib''>4 q q q8 |
<mib' sib' sol''>2 r |
