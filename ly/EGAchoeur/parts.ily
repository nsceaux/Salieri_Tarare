\piecePartSpecs
#`((violino1 #:system-count 5)
   (violino2 #:system-count 5)
   (viola)
   (basso)
   (oboi #:score-template "score-oboi")
   (corni #:instrument "Cors in Mi♭" #:tag-global ())
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
