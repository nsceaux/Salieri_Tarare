<<
  %% Calpigi
  \tag #'(calpigi basse) {
    <<
      \tag #'basse { s1*5 \ffclef "alto/G_8" }
      \tag #'calpigi { \clef "alto/G_8" R1*5 | }
    >> <>^\markup\character Calpigi r2 do'4 mib'8 do' |
    sib2 r |
    r4 r8 sib fa'4. fa'8 |
    sol'2 <<
      \tag #'basse { s2 s1 s2 \ffclef "alto/G_8" <>^\markup\character Calpigi }
      \tag #'calpigi { r2 R1 r2 }
    >> r4 do'8 do' |
    fa'4 fa'8 fa' fa'4. fa'8 |
    re'4 re'8
    \ffclef "alto/G_8" \tag #'calpigi <>^\markup\character Chœur et Calpigi
    sib sol'4 sol' |
    fa'2 <<
      \tag #'basse { s2 s4. \ffclef "alto/G_8" <>^\markup\character Calpigi }
      \tag #'calpigi { r2 r4 r8 }
    >> re'8 sol'4 sol' |
    fa'2. fa'4 |
    fa'4 re'8 re' sol'4 sol'8 sol' |
    mib'2 mib'4 <<
      \tag #'basse { s4 s1 s2. \ffclef "alto/G_8" <>^\markup\character Eunuques }
      \tag #'calpigi { r4 R1 r2 r4 }
    >> re'4 |
    sol' sol'8 sol' fad'4 fad'8 fad' |
    sol'2 sol'4 sol' |
    sol' sol'8 sol' fa'!4 fa'8 fa' |
    sol'2 <<
      \tag #'basse { sol'8 s4. s8 \ffclef "alto/G_8" <>^\markup\character Eunuques }
      \tag #'calpigi { sol'4 r | r8 }
    >> do'8 re' mi' <<
      \tag #'basse { fa'8 s4. s8 \ffclef "alto/G_8" <>^\markup\character Eunuques }
      \tag #'calpigi { fa'2 | r8 }
    >> re'8 fa' re' sib4 fa' |
    sol'4 sol'8 sol' lab'4 lab'8 lab' |
    fa'1 |
    sol'2 r4 fa' |
    sol' sol'8 sol' lab'4 lab'8 lab' |
    fa'1 |
    mib'2 r |
  }
  \tag #'choeur-eunuques {
    \clef "tenor/G_8" R1*12 |
    r4 r8 sib mib'4 mib' |
    re'2 r |
    r4 r8 sib mib'4 mib' |
    re'2. re'4 |
    re' sol8 sol re'4 re'8 re' |
    do'2 do'4 r |
    R1 |
    r2 r4 re' |
    sib sib8 sib la4 la8 la |
    sib2 sib4 sib |
    sib sib8 sib re'4 re'8 re' |
    mib'2 mib'4 r |
    r8 do' re' mi' fa'2 |
    r8 re' fa' re' sib4 sib |
    sib sib8 sib mib'4 mib'8 mib' |
    re'1 |
    mib'2 r4 sib |
    sib sib8 sib do'4 do'8 do' |
    re'1 |
    mib'2 r |
  }
  %% Urson
  \tag #'(urson basse) {
    \clef "bass" <>^\markup\character Urson r2 r4 sib |
    mib'2 sib |
    sol r |
    r4 sib do' re' |
    mib'2 r |
    <<
      \tag #'basse { s1*3 s2 \ffclef "bass" <>^\markup\character Urson }
      \tag #'urson { R1*3 r2 }
    >> r4 sib8 sib |
    mib'4 re'8 re' do'4. sib8 |
    la4 la <<
      \tag #'basse {
        s2 s1*2 s2 \ffclef "bass" <>^\markup\character Soldats
      }
      \tag #'urson {
        r2 |
        r2 <>^\markup\character Chœur des Soldats r8 fa8 sol la |
        sib1 |
        r2
      }
    >> r8 fa sol la |
    <<
      \tag #'basse {
        sib4. s8 s2 s1*2 s2.
        \ffclef "bass" <>^\markup\character Urson
      }
      \tag #'urson {
        sib1 |
        R1*2 |
        r2 r4
      }
    >> sol4 |
    do'4. mib'8 do' sib la sol |
    fad2 fad4 <<
      \tag #'basse { s4 s1*3 s2 s8 \ffclef "bass" <>^\markup\character Soldats }
      \tag #'urson {
        re4 |
        sol4. sol8 re' re' re' re' |
        sol2 sol4 mib |
        mib'4. mib'8 sib sib sib sib |
        mib'2 mib'8
      }
    >> mib fa sol |
    <<
      \tag #'basse { lab8 s4. s8 \ffclef "bass" <>^\markup\character Soldats }
      \tag #'urson { lab2 r8 }
    >> fa8 sol la |
    <<
      \tag #'basse { sib8 }
      \tag #'urson {
        sib2 r4 sib |
        mib'4. mib'8 lab lab lab lab |
        sib1 |
        mib'2. re'4 |
        mib'4. mib'8 lab lab lab lab |
        sib1 |
        mib2 r |
      }
    >>
  }
>>
