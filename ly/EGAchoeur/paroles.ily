\tag #'(urson basse) {
  Mar -- chez, sol -- dats, dou -- blez le pas.
}
\tag #'(calpigi basse) {
  Quoi ! des sol -- dats ! n’a -- van -- cez pas.
}
\tag #'(urson basse) {
Sui -- vez l’or -- dre que je vous don -- ne.
}
\tag #'(calpigi basse) {
  Ne lais -- sez a -- van -- cer per -- son -- ne,
}
\tag #'urson {
  Dou -- blons le pas.
}
\tag #'(calpigi basse choeur-eunuques) {
  N’a -- van -- cez pas.
}
\tag #'(urson basse) {
  Dou -- blons le pas.
}
\tag #'(calpigi basse choeur-eunuques) {
  N’a -- van -- cez pas.
  Pour tous, cette en -- ceinte est sa -- cré -- e.
}
\tag #'(urson basse) {
  Notre ordre est d’en for -- cer l’en -- tré -- e,
}
\tag #'urson {
  notre ordre est d’en for -- cer l’en -- tré -- e,
  notre ordre est d’en for -- cer l’en -- tré -- e.
}
\tag #'(calpigi basse choeur-eunuques) {
  Pour tous, cette en -- ceinte est sa -- cré -- e,
  pour tous, cette en -- ceinte est sa -- cré -- e.
}
\tag #'(urson basse) {
  Dou -- blons le pas.
}
\tag #'(calpigi basse choeur-eunuques) {
  N’a -- van -- cez pas.
}
\tag #'(urson basse) {
  Dou -- blons le pas.
}
\tag #'(calpigi basse choeur-eunuques) {
  N’a -- van -- cez pas.
  Pour tous, cette en -- ceinte est sa -- cré -- e,
  pour tous, cette en -- ceinte est sa -- cré -- e.
}
\tag #'urson {
  Notre ordre est d’en for -- cer l’en -- tré -- e,
  notre ordre est d’en for -- cer l’en -- tré -- e.
}

\tag #'urson2 {
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  Dou -- blez le pas.
  Dou -- blez le pas.
}