\clef "treble" mib'2.-\sug\f sib'4 |
mib''2 sib' |
sol'4. mib'8 sol'4. fa'16 mib' |
sib'4 sib' do'' re'' |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { mib''2. mib''4 |
    do''1 |
    sib' |
    fa'' |
    sol''2~ sol''4 }
  { mib''2. sol'4 |
    lab'1 |
    sol' |
    sib' |
    sib'2~ sib'4 }
  { s1 | s\fp | s-\sug\f }
>> r4 |
R1*3 |
r4 r8 \twoVoices#'(oboe1 oboe2 oboi) <<
  { sib'8 sol''4 sol'' | fa''2 fa''8 }
  { sib'8 mib''4 mib'' | re''2 re''8 }
>> r8 r4 |
r4 r8 \twoVoices#'(oboe1 oboe2 oboi) <<
  { sib'8 sol''4 sol'' |
    fa''2~ fa''4 fa''~ |
    fa'' re''8. re''16 sol''4. sol''8 |
    mib''2~ mib''4 }
  { sib'8 mib''4 mib'' |
    re''2~ re''4 re''~ |
    re'' sol'2 re''4 |
    sol'2~ sol'4 }
>> r4 |
R1*3 |
r2 r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''4 |
    sol'' sol''8 sol'' fa''4 fa''8 fa'' |
    sol''4 sol''8 sib'' sol''4 }
  { mib''4 |
    sib' sib'8 mib'' re''4 re''8 re'' |
    mib''4 mib''8 sol'' mib''4 }
>> r4 |
R1 |
r2 r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''4 |
    sol'' sol''8 sol'' lab''4 lab''8 lab'' |
    fa''8 sib''4 sib'' sib'' sib''8 |
    sol''2 }
  { re''4 |
    mib'' sib'8 sib' do''4 do''8 do'' |
    re''8 re''4 re'' re'' re''8 |
    mib''2 }
>> r4 \twoVoices#'(oboe1 oboe2 oboi) <<
  { fa''4 |
    sol'' sol''8 sol'' lab''4 lab''8 lab'' |
    fa''8 sib''4 sib'' sib'' sib''8 |
    sol''2 }
  { re''4 |
    mib'' sib'8 sib' do''4 do''8 do'' |
    re'' re''4 re'' re'' re''8 |
    mib''2 }
>> r |
