\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small en Mi♭ }
        shortInstrumentName = "Cor."
      } << \keepWithTag#'() \global \keepWithTag #'corni \includeNotes "corni" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff <<
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'calpigi \includeNotes "voix"
      >> \keepWithTag #'calpigi \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'choeur-eunuques \includeNotes "voix"
      >> \keepWithTag #'choeur-eunuques \includeLyrics "paroles"
      \new Staff \withLyricsB <<
        \global \keepWithTag #'urson \includeNotes "voix"
      >> \keepWithTag #'urson \includeLyrics "paroles"
      \keepWithTag #'urson2 \includeLyrics "paroles"
    >>
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*7\break s1*5\pageBreak
        s1*6\break s1*5\pageBreak
        s1*4\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
