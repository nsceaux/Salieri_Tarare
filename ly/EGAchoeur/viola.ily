\clef "alto" mib2.-\sug\f sib4 |
mib'2 sib |
sol4. mib8 sol4. fa16 mib |
sib4 sib do' re' |
mib'2:8 mib':8 |
mib'2:8-\sug\fp mib':8 |
mib'2:8-\sug\f mib':8 |
re':8 re':8 |
mib':8 mib'4 sib8. sib16 |
mib'4 re'8. re'16 do'4. sib8 |
la2:8 la4 r |
r fa8. fa16 la8 fa[-\sug\ff sol la] |
sib2:8 sib:8 |
sib:8 sib8 fa[ sol la] |
sib2:8 sib:8 |
sib2:8 sib:8 |
si8 re' sol' sol' sol' sol sol sol |
sol mib' sol' do'' do4 sol |
do'4. mib'8 do' sib la sol |
fad re'' la' fad' re'4 re' |
sol' sol'8 sol' re'4 re'8 re' |
sol'2 sol'4 mib' |
mib' mib'8 mib' sib4 sib8 sib |
mib'2~ mib'8 mib[ fa sol] |
lab2~ lab8 la'[ sib' do''] |
fa'8 re' fa' re' sib4 sib |
mib' mib'8 mib' lab'4 lab'8 lab' |
sib'8 re'' fa'' re'' sib' fa' re' sib |
mib'4 sol'8 mib' sib4 sib |
mib'4 mib'8 mib' lab'4 lab'8 lab' |
sib' re'' fa'' re'' sib' fa' re' sib |
mib'2 r |
