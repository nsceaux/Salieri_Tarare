\clef "bass" mib2.\f r4 |
R1 |
r4 r8 mib sol4. mib8 |
sib4 sib, do re |
mib2:8 mib:8 |
mib2:8\fp mib:8 |
mib2:8\f mib:8 |
re:8 re:8 |
mib:8 mib4 sib8. sib16 |
mib'4 re' do' sib |
la8 la, la, la, la,4 r |
r2 r8 fa\ff sol la |
sib sib, sib, sib, sib,2:8 |
sib,:8 sib,8 fa[ sol la] |
sib2:8 sib:8 |
sib:8 sib:8 |
si:8 si:8 |
do':8 do'4 sol |
do'4. mib'8 do' sib la sol |
fad re' la fad re4 re |
sol sol8 sol re4 re8 re |
sol2 sol4 mib |
mib mib8 mib sib,4 sib,8 sib, |
mib2~ mib8 mib[ fa sol] |
lab2~ lab8 fa[ sol la] |
sib2. sib,4 |
mib mib8 mib lab4 lab8 lab |
sib8 re' fa' re' sib fa re sib, |
mib4 sol8 mib sib,4 sib, |
mib4 mib8 mib lab4 lab8 lab |
sib re' fa' re' sib fa re sib, |
mib2 r |
