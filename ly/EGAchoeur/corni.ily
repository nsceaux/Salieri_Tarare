\clef "treble" \transposition mib
<>-\sug\f \twoVoices#'(corno1 corno2 corni) <<
  { do''2. }
  { mi' }
>> r4 |
R1 |
r4 r8 \twoVoices#'(corno1 corno2 corni) <<
  { do''8 mi''4 do'' | sol'2 }
  { do''8 mi''4 do'' | sol'2 }
>> r2 |
\twoVoices#'(corno1 corno2 corni) <<
  { do''1~ |
    do''~ |
    do''4 mi''2 mi''4 |
    re''4 }
  { do'1~ |
    do'~ |
    do'4 sol'2 do''4 |
    sol'4 }
  { s1 | s-\sug\fp | s4 s2.-\sug\f | }
>> r4 r2 |
r8 \twoVoices#'(corno1 corno2 corni) <<
  { sol''8 sol'' sol'' sol''4 }
  { mi''8 mi'' mi'' mi''4 }
>> r4 |
R1*2 |
r2 r8 <>^"unis" re''8-\sug\ff mi'' fad'' |
sol''1~ |
sol''2~ sol''8 re'' mi'' fad'' |
sol''1 |
sol''2~ sol''4 r |
R1 |
r8 \twoVoices#'(corno1 corno2 corni) <<
  { mi''8 mi'' mi'' mi''4 }
  { do''8 do'' do'' do''4 }
>> r4 |
R1*3 |
r2 r4 \twoVoices#'(corno1 corno2 corni) <<
  { do''4 |
    mi'' mi''8 mi'' re''4 re''8 re'' |
    mi''4 mi''8 sol'' mi''4 }
  { do''4 |
    do''4 do''8 do'' sol'4 sol'8 sol' |
    do''4 do''8 mi'' do''4 }
>> r4 |
R1 |
r2 r4 \twoVoices#'(corno1 corno2 corni) <<
  { re''4 | mi''2 }
  { sol'4 | do''2 }
>> r2 |
\twoVoices#'(corno1 corno2 corni) <<
  { re''8 re''4 re'' re'' re''8 |
    mi'' do''[ mi'' do''] sol'4 re'' |
    mi''2 }
  { sol'8 sol'4 sol' sol' sol'8 |
    do'' do''[ mi'' do''] sol'4 sol' |
    do''2 }
>> r2 |
r8 \twoVoices#'(corno1 corno2 corni) <<
  { re''4 re'' re'' re''8 | do''2 }
  { sol'4 sol' sol' sol'8 | mi'2 }
>> r2 |

