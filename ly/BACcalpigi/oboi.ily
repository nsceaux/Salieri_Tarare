\clef "treble" r4 |
<>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib'1 | sol'~ | sol' | la'~ |
    la' | sib'~ | sib' | mi''~ |
    mi'' | fa''4 }
  { re'1 | mib'~ | mib' | mib'~ |
    mib' | re'~ | re' | sib'~ |
    sib' | la'4 }
  { s1*7 s1-\sug\f s1-\sug\p }
>> r4 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2~ | la''~ la''4\fermata }
  { do''2~ | do''~ do''4\fermata }
>> r4 |
R1*3 |
r8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 re''8 re''4 }
  { lab'4 lab'8 lab'4 }
>> r4 |
R1*14 |
r2
