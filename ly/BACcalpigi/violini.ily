\clef "treble"
<<
  \tag #'violino1 {
    r4 |
    r sib'2\p do''8 re'' |
    mib''4 <mib' sol>2 do''8. re''16 |
    mib''4. mib''8 sol''4. mib''8 |
    do''4 la2 do''4 |
    fa''4. fa''8 fa''4. fa''8 |
    r8. fa'16 re''8. do''16 sib'8. fa'16 re''8. do''16 |
    sib' fa' re'' do'' sib' fa' re'' do'' sib' fa' re'' do'' sib'32( do'' re'' mib'' fa'' sol'' la'' sib'') |
    <mi' sol>2\f~ q4 r |
    r16 sib''8\p sol'' mi'' do'' sib' sol' mi' sib16 |
    la8. do''16\f do''8([ la'16]) fa'' fa''8([ do''16]) la'' la''8([ fa''16]) do''' |
    <la' do'''>2~ q4\fermata r |
    do''4\p( re''8 mib'' fa''4 sol''8. fa''16) |
    fa''8( mib'') mib''8. re''16 mib''8( sol'' mib'' re'') |
    do'' fa'( la' do'') do''4 re''8 mib'' |
    mib''8.(\f re''16 re''8. fa''16) re''8 sib'[\p sib' sib'] |
    mib''8.\fp mib'32 re' mib'8. mib''16\f fa''8. \grace do''8 sib'32 la'! sib'8. fa''16 |
    <mib' sib' sol''>4 r r2 |
  }
  \tag #'violino2 {
    r4 |
    r8 fa'-\sug\p fa' fa' fa' sib' sib' sib' |
    sol'2:8 sol':8 |
    sol':8 sol'8 mib' mib' mib' |
    mib'2:8 mib':8 |
    do':8 la':8 |
    sib'16 fa' fa' fa' fa'4:16 fa'2:16 fa':16 fa'4:16 fa'16 sib' sib' sib' |
    sib'2:16-\sug\f sib':16 |
    <sib' sol'>16\p q8 q q q q q q q16 |
    <la' fa'>8. do'16-\sug\f do'8([ la16]) fa' fa'8([ do'16]) la' la'8([ fa'16]) do'' |
    <do'' la''>2~ q4\fermata r |
    r8 la8(-\sug\p sib do') re'4( si) |
    si8( do') do'8. si16 do'8( mib' do' sib) |
    la1 |
    lab2\f~ lab8 lab'-\sug\p lab' lab' |
    sol'8.-\sug\fp \grace fa'8 mib'32 re' mib'8. sib'16-\sug\f sib'8. \grace do'8 sib32 la! sib8. sib'16 |
    sib'4 r r2 |
  }
>>
r2 <lab mib' do''>4 r |
R1*3 |
r2
<<
  \tag #'violino1 { mi''!4 r | fa'' }
  \tag #'violino2 { sib'4 r | lab' }
>> r4 r2 |
R1 |
<<
  \tag #'violino1 { si'!4 }
  \tag #'violino2 { re'4 }
>> r4 r2 |
R1 |
do'16\ff do'' si'! la' sol' fa' mi'! re' do'8 re'16 mi' fa' sol' la' si' |
do''4 r r2 |
r8 do'16\f re' mi' fa' sol' la' sib'4 <sib' sol''> |
<<
  \tag #'violino1 { <sib'' sib'>4 }
  \tag #'violino2 { <sol' sib' mi''>4 }
>> r4
