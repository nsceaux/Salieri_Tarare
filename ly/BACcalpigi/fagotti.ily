\clef "bass" r4 |
sib1-\sug\p |
do'~ |
do' |
do'~ |
do' |
sib |
sib |
do'-\sug\f~ |
do'-\sug\p~ |
do'4 r fa'2-\sug\f~ |
fa'~ fa'4\fermata r4 |
\clef "tenor" <>^\markup\italic Solo r8 la(-\sug\p sib do') re'4( si) |
si8( do') do'8. si16 do'8( mib' do' sib) |
la1 |
lab2-\sug\f~ lab8 fa'-\sug\p fa' fa' |
re'1~ |
re'4 r r2 |
R1*12 |
r2
