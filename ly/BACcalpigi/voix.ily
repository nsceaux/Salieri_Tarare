\clef "alto/G_8" <>^"chanté" ^\markup\character-text Calpigi avec sensibilité
re'8. fa'16 |
sib4. sib8 sib4 do'8 re' |
mib'2 r4 do'8 re' |
mib'4 mib'8 mib' sol'4. mib'8 |
do'4 do' r8 do' do' do' |
fa'4. fa'8 fa'4. fa'8 |
sib2 r4 re'8 do' |
sib4 sib8 sib sib4. sib8 |
mi'4 mi' r8 mi' mi' fa' |
sol'4. sol'8 mi'4. do'8 |
fa'4 r8. do'16 la'2~ |
la'8\fermata
\footnoteHere#'(0 . 0) \markup {
  Source :
  \score {
    \new Staff \withLyrics {
      \clef "alto" \key sib \major \tinyQuote
      la'8\repeatTie\fermata fa'16 fa' fa'8 do' do'16 do' la4*1/2 r4
    } \lyrics { - re est l’e -- cho gé -- né -- ral ; }
    \layout { \quoteLayout }
  }
}
fa'16 fa' do'8 do'16 do' la4 r4 |
do'8 do' re' mib' fa'4 sol'8. fa'16 |
fa'8[ mib'] mib'4 r mib'8. re'16 |
do'4. do'8 do'4 re'8 mib' |
re'4 re' r8 sib sib sib |
mib'4. mib'8 fa'4. fa'8 |
sol'
\ffclef "bass" <>^"parlé" ^\markup\character-text Atar en colère
sib!8 sib sib mib'!4 sib8 do' |
reb'4 mib'8 sib do'4 r16 do' do' do' |
mib'8 do' do' do' lab lab r16 lab lab sib |
do'4
\ffclef "alto/G_8" <>^\markup\character Calpigi
r8 lab do'4 r8 mib' |
do'4 r16 do' reb' mib' lab?4 r8 do' |
do'4 do'8 re' mi'!4 mi'8 fa' |
do' do' r do'16 fa' fa'4 do'16 do' sib do' |
lab4 r16 lab lab sib do'8 do' do' re' |
si! si r16 si do' re' sol4 sol16 sol sol la |
si4 si16 si si do' re'4 re'16 re' re' mi'! |
do'4 r r2 |
\ffclef "bass" <>^\markup\character-text Atar furieux
r8 sol sol sol do' do'16 do' mi'8 re'16 mi' |
do'4 r r2 |
r2
