Il est vrai, son nom a -- do -- ré,
dans la bou -- che de tout le mon -- de,
est un pro -- ver -- be ré -- vé -- ré.
Par -- le- t-on des fu -- reurs de l’on -- de,
ou du flé -- au le plus fa -- tal ;
Ta -- rare ! __ est l’é -- cho gé -- né -- ral :
com -- me si ce nom se -- cou -- ra -- ble
é -- loi -- gnait, ren -- dait in -- croy -- a -- ble
le mal, hé -- las ! le plus cer -- tain…

Fi -- ni -- ras- tu, mé -- pri -- sa -- ble chré -- tien ?
Eu -- nu -- que vil et dé -- tes -- ta -- ble ;
la mort de -- vrait…

La mort, la mort, tou -- jours la mort !
Ce mot é -- ter -- nel me dé -- so -- le :
ter -- mi -- nez u -- ne fois mon sort ;
et puis cher -- chez qui vous con -- so -- le
du triste en -- nui de la sa -- tié -- té,
de l’oi -- si -- ve -- té,
de la roy -- au -- té.

Je pu -- ni -- rai cet ex -- cès d’ar -- ro -- gance.
