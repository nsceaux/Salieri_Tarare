\tag #'all \key sib \major
\tempo "Larghetto" \midiTempo#80
\time 2/2 \partial 4 s4 s1*17
\tag #'all \key do \major s1*9
\tempo "Allegro" s1*3 s2 \bar "|."
