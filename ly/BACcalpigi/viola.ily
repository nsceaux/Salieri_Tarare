\clef "alto" r4 |
r8 re'\p re' re' re'2:8 |
mib':8 mib':8 |
mib':8 mib'8 do' do' do' |
do'2:8 do':8 |
la:8 do':8 |
re'16 re' re' re' re'4:16 re'2:16 |
re':16 re'4:16 re':16 |
sol'2:16-\sug\f sol':16 |
<sol mi'>16-\sug\p q8 q q q q q q q16 |
<la fa'>8. do'16\f do'8([ la16]) fa' fa'8([ do'16]) la' la'8([ fa'16]) do'' |
do''2~ do''4\fermata r |
r8 la(\p sib do') re'4( si) |
si8( do') do'8. si16 do'8( mib' do' sib) |
la1 |
lab2-\sug\f~ lab8 fa'-\sug\p fa' fa' |
sib4.-\sug\fp sib8 sib4.-\sug\fp sib8 |
mib'4 r r2 |
r2 lab4 r |
R1*3 |
r2 sol'4 r |
fa' r r2 |
R1 |
sol'4 r r2 |
R1 |
do'16-\sug\ff do'' si'! la' sol' fa' mi'! re' do'8 re'16 mi' fa' sol' la' si' |
do''4 r r2 |
r8 do'16\f re' mi' fa' sol' la' sib'4 mi' |
do' r
