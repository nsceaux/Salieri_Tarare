\clef "treble" \transposition mib
r4 |
R1*3 |
<>^"[a 2]" re''1-\sug\p~ |
re'' |
\twoVoices #'(corno1 corno2 corni) <<
  { re''~ | re'' | sol''~ | sol'' | fad''4 }
  { sol'1~ | sol' | mi''~ | mi'' | re''4 }
  { s1*2 s1\f s\p }
>> r4 r2 |
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { re''2~ re''4\fermata }
  { re''2~ re''4\fermata }
>> r4 |
R1*3 |
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { re''2~ re''4\fermata }
  { re''2~ re''4\fermata }
>> r4 |
\footnoteHere#'(0 . 0) \markup {
  Source : \score {
    { \tinyQuote r8. << { do''16 do''4 } \\ { do''16\< do''4\! } >>
      r8. << { re''16 re''4*1/2 re'' | mi''4 }
        \\ { sol'16 sol'4*1/2 sol' | do''4 } >> }
    \layout { \quoteLayout }
  }
}
r8. \twoVoices #'(corno1 corno2 corni) <<
  { do''16 do''4 }
  { do''16 do''4 }
  { s16\< s4 }
>> r8. \twoVoices #'(corno1 corno2 corni) <<
  { re''16 re''8 re'' | mi''4 }
  { sol'16 sol'8 sol' | do''4 }
  { s16 s8 s\! }
>> r4 r2 |
R1*12 |
r2
