\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (oboi #:score-template "score-part-voix" #:tag-notes oboi)
   (clarinetti #:score-template "score-part-voix"
               #:notes "oboi" #:tag-notes oboi)
   (corni #:tag-notes corni
          #:tag-global ()
          #:instrument "Cors in Mi♭"
          #:score-template "score-part-voix")
   (fagotti #:tag-notes fagotti
            #:score-template "score-part-voix"
            #:music , #{
\quoteBasso "BACbasso"
s4 s1*17 \clef "bass" <>^\markup\tiny Basso
\cue "BACbasso" { \mmRestDown s1 \mmRestCenter }
s1*3
\cue "BACbasso" { \mmRestDown s1*2 \mmRestCenter }
s1
\cue "BACbasso" { \mmRestDown s1 \mmRestCenter }
s1
\cue "BACbasso" { \mmRestDown s1*3 s2 \mmRestCenter }
#})
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#}))
