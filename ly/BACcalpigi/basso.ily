\clef "bass" r4 |
sib,-\sug\p r r2 |
do4 r r2 |
do4 r r2 |
fa4 r r2 |
fa4 r r2 |
<<
  { \grace <>^\markup\right-align "Violonc." sib2:16 sib:16 | sib:16 sib:16 | } \\
  { \grace <>_\markup\right-align "Basso" sib,4 r sib, r | sib, r r2 | }
>>
<>^\markup { \concat { Viol \super li } et B. }
do16-\sug\f[~ do32*2 re mi fa sol la! si] do'8\p do' do' do' |
do'2:8 do':8 |
fa4 r r2 |
fa2\f\fermata~ fa4 r |
fa1\p~ |
fa~ |
fa |
fa8\f fa fa fa fa re\p re re |
mib4\fp sol8. sol16 re4\fp~ re8. re16 |
mib4 r r2 |
r2 lab4 r |
R1*3 |
r2 sol4 r |
fa r r2 |
R1 |
sol4 r r2 |
R1 |
do16\ff do' si! la sol fa mi! re do8 re16 mi fa sol la si |
do'4 r r2 |
r8 do16\f re mi fa sol la sib4 mi |
do r
