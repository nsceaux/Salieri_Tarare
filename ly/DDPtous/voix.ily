<<
  \tag #'(recit basse) {
    \clef "soprano/treble" <>^\markup\override#'(baseline-skip . 0.8) \column {
      \smallCaps Astasie \italic faisant un cri
    }
    do''8 |
    fad''2 fad''4
    \ffclef "bass" \tag #'didas <>^\markup\character-text Atar \line {
      (il renverse la table d’un coup de pied.)
    }
    r8 re' |
    mib'1 |
    mib'4 r r2 |
    \ffclef "soprano/treble" <>^\markup\character-text Spinette à Astasie
    mib''4. sib'16 sib' sol'8. sol'16 lab'8 sib'8 |
    mib'4
    \ffclef "bass" <>^\markup\character Atar
    r8 sol16 lab sib4 sib16 sib sib sib |
    mib'8 mib' r mib' mib' sib16 do' reb'8 reb'16 mib' |
    \tag #'recit <>^\markup\italic { Il tire son poignard }
    do'4 r r2 |
    \tag #'recit <>^\markup\italic { Tout le monde s’enfuit. }
    R1*3 |
    \ffclef "soprano/treble" <>^\markup\character-text Spinette soutenant Astasie
    r8 mi''16 sol'' sol''2 do''8 r |
    \tag #'didas <>^\markup\override #'(line-width . 120) \justify {
      Atar rappelé à lui par ce cri, laisse aller Calpigi et les autres
      esclaves, et revient vers Astasie, que des femmes emportent chez
      elle ; Atar y entre, en jettant à la porte sa simarre et ses
      brodequins, à la manière des orientaux.
    }
    R1*29
  }
  \tag #'vdessus {
    \clef "soprano/treble" r8 |
    r4 r8 fad'' la''4 la''8 r |
    R1*39
  }
  \tag #'vhaute-contre {
    \clef "alto/G_8" r8 |
    r4 r8 la la'4 la'8 r |
    R1*39
  }
  \tag #'vtaille {
    \clef "tenor/G_8" r8 |
    r4 r8 re' fad'4 fad'8 r |
    R1*39
  }
  \tag #'vbasse {
    \clef "bass" r8 |
    r4 r8 re re'4 re'8 r |
    <>_\markup\override #'(line-width . 45) \override #'(baseline-skip . 0.8) \italic\justify {
      Astasie se lève troublée. Spinette la soutient. Au bruit qui se fait,
      Tarare, à moitié descendu, se jette en bas dans l’obscurité.
    }
    R1*39
  }
>>