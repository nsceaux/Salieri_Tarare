\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (oboi #:score-template "score-part-voix" #:tag-notes oboi)
   (fagotti #:tag-notes fagotti #:score-template "score-part-voix")
   (corni #:score-template "score-part-voix" #:tag-notes corni #:tag-global ()
          #:instrument , #{\markup\center-column { Cors \small en Mi♭ } #})
          (flauti #:tag-notes flauti #:music , #{
s8 s1*6 \mmRestDown
\new CueVoice {
  <<
    \new CueVoice { \voiceOne mib''4^"Hautbois" s mib'' s | mib'' s mib'' s | mib'' }
    \new CueVoice { \voiceTwo do''4 s do'' s | do'' s do'' s | do'' }
    { s4-\sug\ff r s r | s r s r | }
  >> r4 r2 |
  r8 <<
    \new CueVoice { \voiceOne do''8 reb'' do'' sib' lab' sol' fa' | mi'!4 }
    \new CueVoice { \voiceTwo do''8 reb'' do'' sib' lab' sol' fa' | mi'!4 }
  >> r4 r2 |
  \twoVoices #'(oboe1 oboe2 oboi) <<
    \new CueVoice {
      \voiceOne do''4 s do'' s |
      do'' s mi''! s |
      do'' s do'' s |
      do'' s mi'' s |
      do''1 |
      do''4 }
    \new CueVoice {
      \voiceTwo sol'4 s sol' s |
      sol' s sol' s |
      sol' s sol' s |
      sol' s sol' s |
      sol'2 sib' |
      la'4 }
    { s4-\sug\f r s r | s r s r | s4-\sug\mf r s r | s r s r | }
  >> r4 r2 | \mmRestCenter
}
#})
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
