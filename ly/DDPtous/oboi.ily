\clef "treble" r8 |
R1 |
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''1~ | sib''~ | sib''4 }
  { sol''1~ | sol''~ | sol''4 }
>> r4 r2 |
R1*2 |
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { mib''4 s mib'' s | mib'' s mib'' s | mib'' }
  { do''4 s do'' s | do'' s do'' s | do'' }
  { s4 r s r | s r s r | }
>> r4 r2 |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''8 reb'' do'' sib' lab' sol' fa' | mi'!4 }
  { do''8 reb'' do'' sib' lab' sol' fa' | mi'!4 }
>> r4 r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 s do'' s |
    do'' s mi''! s |
    do'' s do'' s |
    do'' s mi'' s |
    do''1 |
    do''4 }
  { sol'4 s sol' s |
    sol' s sol' s |
    sol' s sol' s |
    sol' s sol' s |
    sol'2 sib' |
    la'4 }
  { s4-\sug\f r s r | s r s r | s4-\sug\mf r s r | s r s r | }
>> r4 r2 |
R1*3 |
<>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''1~ | la'' | la'' | la'' | sib''4 }
  { do''1~ | do'' | re'' | re'' | re''4 }
>> r4 r2 |
R1*15 |
