\clef "bass" r8 |
re4\f ^"con l’arco" r re r |
mib2:8 mib:8 |
mib:8 mib:8 |
mib4 r r2 |
mib2:16-\sug\p mib:16 |
mib2:16 mib:16 |
lab,4-\sug\ff r lab, r |
lab, r lab, r |
lab, lab, lab, lab, |
lab,8 do' reb' do' sib lab sol fa |
mi!4 r r2 |
mi4-\sug\f r mi r |
mi r do r |
mi\mf r mi r |
mi r do r |
mi2:8 mi:8 |
fa8 fa-\sug\p la fa sib fa sib fa |
la fa la fa sol mi do mi |
fa fa la fa sib fa sib fa |
la fa la fa sol mi do mi |
fa2:8\f fa:8 |
fa:8 fa:8 |
fad:8 fad:8 |
fad:8 fad:8 |
sol8 sol\p sib sol do' sol do' sol |
sib sol sib sol fad la re fad |
sol sol sib sol do' sol do' sol |
sib sol sib sol fad la re fad |
sol4 sib\pp re' sib |
sol re sib, sol, |
re1~ |
re~ |
re2 sib, |
fad, sol, |
re1~ |
re~ |
re2 sib, |
fad, sol, |
re1~ |
re1*15/16~ \hideNotes re16 |
