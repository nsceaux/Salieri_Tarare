\clef "treble" r8 |
<re' la' fad''>4\f ^"con l’arco" r <<
  \tag #'violino1 { <la'' la'>4 }
  \tag #'violino2 { <re' la' fad''>4 }
>> r4 |
<mib' sol>8 sol''[ sib'' sol''] mib'' re''16 do'' sib' lab' sol' fa' |
mib'8 sol'' mib'' sib' sol' fa'16 mib' re' do' sib lab |
<<
  \tag #'violino1 {
    sol2:16\fp sol:16 |
    sol16 sib' sib' sib' sib'4:16 sib'2:16 |
    sib':16 sib':16 |
  }
  \tag #'violino2 {
    sol16-\sug\fp sib sib sib sib4:16 sib2:16 |
    sib16 sol' sol' sol' sol'4:16 sol'2:16 |
    sol':16 sol':16 |
  }
>>
lab'16\ff mib' fa' sol' lab' sib' do'' sib' lab'8 sib'16 do'' reb'' mib'' fa'' sol'' |
lab''8 sol''16 fa'' mib'' reb'' do'' sib' lab'8 sol'16 fa' mib' reb' do' sib |
lab sib do' sib lab sib do' sib lab sib do' sib lab sib do' sib |
lab8 do''16 do'' reb'' reb'' do'' do'' sib' sib' lab' lab' sol' sol' fa' fa' |
<<
  \tag #'violino1 {
    mi'! do'' do'' do'' do''4:16 do''2:16 |
  }
  \tag #'violino2 {
    mi'!16 sol' sol' sol' sol'4:16 sol'2:16 |
  }
>>
do'16\f sol la si do' re' mi' fa' sol' do' re' mi' fa' sol' la' si' |
do'' mi'' re'' do'' si' la' sol' fa' mi' do'' si' la' sol' fa' mi' re' |
<<
  \tag #'violino1 {
    do'16\mf sol la si do' re' mi' fa' sol' do' re' mi' fa' sol' la' si' |
    do'' mi'' re'' do'' si' la' sol' fa' mi' do'' si' la' sol' fa' mi' re' |
    do'8
  }
  \tag #'violino2 {
    do'4-\sug\mf r do' r |
    do' r do' r |
    r8
  }
>> do'16 re' do' re' do' re' sib do' sib do' sib do' sib do' |
la8 fa'\p \grace sol' fa'16 mi' fa'8 r re' \grace mi' re'16 do' re'8 |
r8 do' \grace re' do'16 si do'8 r sib' \grace do'' sib'16 la' sib'8 |
la' fa' \grace sol' fa'16 mi' fa'8 r re' \grace mi' re'16 do' re'8 |
r8 do' \grace re' do'16 si do'8 r sib' \grace do'' sib'16 la' sib'8 |
la'4. fa''16(\f la'') do'''8 do''' do''' do''' |
do'''4 mib'16 fa' mib' fa' mib' fa' mib' fa' mib' fa' mib' fa' |
re'4. fad''16( la'') re'''8 re''' re''' re''' |
re'''4 do'16 re' do' re' do' re' do' re' do' re' do' re' |
sib8 sol'\p \grace la' sol'16 fad'? sol'8 r mib'8 \grace fa' mib'16 re' mib'8 |
r8 re' \grace mib' re'16 dod' re'8 r do''! \grace re'' do''16 sib' do''8 |
sib' sol' \grace la' sol'16 fad' sol'8 r mib' \grace fa'! mib'16 re' mib'8 |
r re' \grace mib' re'16 dod' re'8 r do'' \grace re'' do''16 sib' do''8 |
sib'8 <<
  \tag #'violino1 {
    re'4\pp re' re' re'8 |
    re' re'4 re' re' re'8 |
    re'1~ |
    re'~ |
    re'~ |
    re'~ |
    re'~ |
    re'~ |
    re'~ |
    re'~ |
    re' |
    re' |
  }
  \tag #'violino2 {
    sib4-\sug\pp sib sib sib8 |
    sib sib4 sib sib sib8 |
    la2( sib |
    do') la( |
    sib) re' |
    do'( sib) |
    la( sib |
    do') la( |
    sib) re' |
    do'( sib) |
    la( sib) |
    la( sib) |
  }
>>
