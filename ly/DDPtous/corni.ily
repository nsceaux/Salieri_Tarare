\clef "treble" \transposition mib
r8 |
R1 |
<>-\sug\ff \twoVoices #'(corno1 corno2 corni) <<
  { sol''1~ | sol''~ | sol''4 }
  { mi''1~ | mi''~ | mi''4 }
>> r4 r2 |
R1*36 |
