\tag #'(recit basse) {
  Ta -- ra -- re !
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Ta -- ra -- re !
}
\tag #'(recit basse) {
  Ta -- ra -- re !

  Dieux ! que ce nom l’a cour -- rou -- cé !
  
  Que la mort, que l’en -- fer s’em -- pa -- re
  du trai -- tre qui l’a pro -- non -- cé !

  Elle ex -- pi -- re !
}
