\clef "treble" r8 |
R1 |
<>\ff \twoVoices #'(flauto1 flauto2 flauti) <<
  { mib'''1~ | mib'''~ | mib'''4 }
  { sol''1~ | sol''~ | sol''4 }
>> r4 r2 |
R1*16 |
<>-\sug\f \twoVoices #'(flauto1 flauto2 flauti) <<
  { la''1~ | la'' | la'' | la'' | sib''4 }
  { do''1~ | do'' | re'' | re'' | re''4 }
>> r4 r2 |
R1*15 |
