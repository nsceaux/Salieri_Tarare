\clef "alto" r8 |
re'4-\sug\f r re' r |
<sib sol'>2:16 q:16 |
q:16 q:16 |
q16-\sug\fp mib mib mib mib4:16 mib2:16 |
mib16 mib' mib' mib' mib'4:16 mib'2:16 |
mib'2:16 mib'2:16 |
<lab mib' do''>4-\sug\ff r q r |
q r q r |
lab16 sib do' sib lab sib do' sib lab sib do' sib lab sib do' sib |
lab8 do'' reb'' do'' sib' lab' sol' fa' |
mi'!2:16 mi':16 |
sol4-\sug\f r do' r |
sol r sol r |
sol-\sug\mf r sol r |
sol r sol r |
sol2:8 sol:8 |
fa8 fa-\sug\p la fa sib fa sib fa |
la fa la fa sol mi do mi |
fa fa la fa sib fa sib fa |
la fa la fa sol mi do mi |
fa16 la'\f la' la' la'4:16 la'2:16 |
la2:16 la:16 |
la16 la' la' la' la'4:16 la'2:16 |
la2:16 la:16 |
sol8 sol-\sug\p sib sol do' sol do' sol |
sib sol sib sol fad la re fad |
sol sol sib sol do' sol do' sol |
sib sol sib sol fad la re fad |
sol8 sol4-\sug\pp sol sol sol8 |
sol sol4 sol sol sol8 |
fad2( sol |
la) fad( |
sol) sol |
la( sol) |
fad( sol |
la) fad( |
sol) sol |
la( sol) |
fad( sol) |
fad( sol) |
