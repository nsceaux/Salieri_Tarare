\clef "bass" r8 |
R1 |
mib'1-\sug\ff~ |
mib'~ |
mib'4 r r2 |
R1*2 |
lab,4-\sug\ff r lab, r |
lab, r lab, r |
lab, lab, lab, lab, |
lab,8 do' reb' do' sib lab sol fa |
mi!4 r r2 |
mi4-\sug\f r mi r |
mi r do r |
mi\mf r mi r |
mi r do r |
mi2:8 mi:8 |
fa4 r r2 |
R1*3 |
<>-\sug\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa4 do'2 do'4 |
    do'1 |
    re' |
    re' |
    re'4 }
  { fa4 la2 la4 |
    la1 |
    la |
    la |
    sib4 }
>> r4 r2 |
R1*15 |
