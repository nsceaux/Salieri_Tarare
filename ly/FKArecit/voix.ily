\clef "bass" R1 |
r4 r8 sol sol sol sol la |
sib4 r sib8 sib16 sib do'8 re' sol4 r r2 |
r4 r8 do' do' do' do' do' |
la do'16 mib' la8 la16 sib fa4 r |
