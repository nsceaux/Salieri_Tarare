\piecePartSpecs
#`((violino1 #:score-template "score-part-voix" #:indent 0)
   (violino2 #:score-template "score-part-voix" #:indent 0)
   (viola #:score-template "score-part-voix" #:indent 0)
   (basso #:score-template "score-part-voix" #:indent 0)
   (oboi #:score-template "score-part-voix" #:system-count 2 #:indent 0)
   (trombe #:score-template "score-part-voix" #:system-count 2 #:indent 0 #:tag-global ())
   (fagotti #:score-template "score-part-voix" #:system-count 2 #:indent 0)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
