\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes \small in D }
        shortInstrumentName = \markup Tr.
      } << \keepWithTag #'() \global \keepWithTag #'trombe \includeNotes "trombe" >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroupNoBar <<
      \new StaffGroupNoBracket <<
        \new GrandStaff \with { \violiniInstr } <<
          \new Staff \with { \consists "Metronome_mark_engraver" } <<
            \global \keepWithTag #'violino1 \includeNotes "violini"
          >>
          \new Staff <<
            \global \keepWithTag #'violino2 \includeNotes "violini"
          >>
        >>
        \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      >>
      \new Staff \with {
        instrumentName = \markup\character Urson
        shortInstrumentName = \markup\character Ur.
      } \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with {
        \bassoInstr
        \consists "Metronome_mark_engraver"
      } <<
        \global \keepWithTag #'basso \includeNotes "basso"
        \origLayout { s1*5\break }
      >>
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
