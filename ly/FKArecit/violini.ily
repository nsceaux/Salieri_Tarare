\clef "treble"
<<
  \tag #'violino1 {
    re''2 re'4 do'8 si16 do' |
    sib2
  }
  \tag #'violino2 {
    re''2 re'4 <re' la' fad''!> |
    <re' sib' sol''>2
  }
>> r2 |
R1 |
r2 <sol re' sib'>4 <sol re' si'> |
<sol mi'! do''>2 r |
<fa' do'' la''>4 r r q |
