\clef "bass" mib1\fermata~ |
mib\f |
lab,2. do8. sib,16 |
lab,1 |
sib, |
sib,4 sib,8. sib,16 sib,4 sib, |
mib4. sib16 do'32 re' mib'8 sib sol mib |
lab,2 sib, |
mib4. sib,16 do32 re mib4 mib |
mib2~ mib4 r |
do4.\fp sol,8 do4.-\sug\fp sol,8 |
do2:8\f do:8 |
si,:8\p si,:8 |
do:8\f do:8 |
do:8\fp sib,:8\fp |
mib:8\f mib:8 |
lab,:8 lab,:8 |
sol,2.\fermata r4 |
mib8\p sol sib sol fa lab sib lab |
sol sib sol sib sol sib sol sib |
re sib re sib mib sib mib sib |
fa sib fa sib fa sib re sib |
mib sol sib sol mib sol sib sol |
lab-\sug\cresc do' lab do' lab do' sib lab |
sol sib sol sib sol\p sol lab lab |
sib4 r sib, r |
mib2.\f re8 re |
do4.\fp sol,8 do4.\fp sol,8 |
do2:8-\sug\f do:8 |
si,:8 si,:8 |
<<
  { do2:16 do:16 } \\
  { do:8\f do:8 }
>>
do2:8\fp re:8\fp |
mib:8\f mib:8 |
lab,:8 lab,:8 |
sol,2\fermata r2 |
mib8\p sol sib sol fa lab sib lab |
sol sib sol sib sol sib sol sib |
re sib re sib mib sib mib sib |
fa sib fa sib fa sib re sib |
mib sol sib sol mib sib sol mib |
lab\cresc do' lab do' lab do' sib lab |
sol\fp sib sol sib sol sol lab lab |
sib4 r sib, r |
mib8\f mib fa fa sol sol lab lab |
sol mib sol mib sol mib lab-\sug\p lab |
sib2:8 sib,:8 |
mib8\f sib sol sib lab sib lab sib |
sol sib sol sib lab sib lab sib |
sol mib mib mib mib2:8 |
mib:8 mib:8 |
mib4. mib8 mib4 mib |
mib2 mib |
mib r |
