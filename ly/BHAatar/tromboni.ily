\clef "alto" R1*11 |
<\tag#'tr3 do' \tag#'tr2 mib' \tag#'tr1 sol'>1\ff |
<\tag#'tr3 re' \tag#'tr2 fa' \tag#'tr1 sol'>\p |
<\tag#'tr3 do' \tag#'tr2 mib' \tag#'tr1 sol'>-\sug\f |
R1 |
<\tag#'tr3 sib \tag#'tr2 mib' \tag#'tr1 sol'>-\sug\f |
<\tag#'tr3 do' \tag#'tr2 mib' \tag#'tr1 sol'>2
<\tag#'tr3 do' \tag#'tr2 mib' \tag#'tr1 fad'> |
<\tag#'tr3 si \tag#'tr2 re' \tag#'tr1 sol'>2.\fermata r4 |
R1*10 |
<\tag#'tr3 do' \tag#'tr2 mib' \tag#'tr1 sol'>1-\sug\f |
<\tag#'tr3 re' \tag#'tr2 fa' \tag#'tr1 sol'> |
<\tag#'tr3 do' \tag#'tr2 mib' \tag#'tr1 sol'>\f |
R1 |
<\tag#'tr3 sib \tag#'tr2 mib' \tag#'tr1 sol'>\f |
<\tag#'tr3 do' \tag#'tr2 mib' \tag#'tr1 sol'>2
<\tag#'tr3 do' \tag#'tr2 mib' \tag#'tr1 fad'> |
<\tag#'tr3 si \tag#'tr2 re' \tag#'tr1 sol'>2.\fermata r4 |
R1*18 |
