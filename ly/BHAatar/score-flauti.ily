\score {
  \new Staff <<
    \global \includeNotes "flauti"
    { \quoteViolinoI "BHAviolino1"
      <>^\markup\tiny "Vln." \cue "BHAviolino1" {
        \mmRestUp s1*2 \mmRestDown s1*3 \mmRestUp s1*6
        \mmRestDown s1 \mmRestUp s1 \mmRestDown s1 \mmRestUp s1
        \mmRestDown s1 \mmRestUp s1
      }
      s1*10
      \cue "BHAviolino1" {
        \mmRestUp s1 \mmRestDown s1 \mmRestUp s1 \mmRestDown s1
        \mmRestUp s1 \mmRestDown s1 \mmRestUp s1
      }
    }
  >>
  \layout { }
}
