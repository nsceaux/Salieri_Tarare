\clef "treble" R1*17 |
r2 r4 r8^\markup\whiteout\right-align\line {
  une flûte avec le \concat { 1 \super er } Violon
} sib''!8 |
sib''4. sol''8 \grace sib''8 lab''4 sol''8 fa'' |
mib''2. mib''8. mib''16 |
fa''2 sol''4. lab''8 |
sib''2. \grace sib''8 lab'' sol''16 lab'' |
sol''2 r4 mib''8. mib''16 |
do'''2\cresc do'''4. re'''8 |
mib'''2. re'''8\p do''' |
\grace do''' sib''4 sol''8 sib'' \grace sib'' lab''4 sol''8 fa'' |
mib''4 r4 r2 |
R1*7 |
r2 r4 r8 sib'' |
sib''4. sol''8 \grace sib'' lab''4 sol''8 fa'' |
mib''2. mib''8. mib''16 |
fa''2 sol''4. lab''8 |
sib''2. \grace sib''8 lab'' sol''16 lab'' |
sol''2. mib''8. mib''16 |
do'''2\cresc do'''4. re'''8\! |
mib'''2.\fp re'''8 do''' |
\grace do''' sib''4 sol''8 sib'' \grace sib'' lab''4 sol''8 fa'' |
mib'''1\ff~ |
mib'''2. re'''8\p do''' |
sib''4 sol''8 sib'' \grace sib'' lab''4 sol''8 fa'' |
mib''2\f re'''4. mib'''16 fa''' |
mib'''8 sib'' sib'' sib'' re'''4. mib'''16 fa''' |
mib'''4. mib'''8 mib'''4. sib''8 |
sib''4. \new CueVoice {
  sol''8 sol''4. mib''8 |
  mib''4. mib''8 mib''4 mib'' |
  mib''2 sol'' |
  mib'' r |
}
