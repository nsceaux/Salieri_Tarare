\clef "treble" <sol mib'>2\fermata~ q4. <<
  \tag #'violino1 {
    sib'8\f |
    sib'4. sol'8 sol'4. mib'8 |
    do''2. lab''8. sib''16 |
    do'''2 lab''4. fa''8 |
    re''1 |
    sib'4 sib'8. sib'16 sib'4 sib' |
    sol'1 |
    fa'4 fa'8. fa'16 fa'4 fa' |
  }
  \tag #'violino2 {
    sol'8-\sug\f |
    sol'4. sib8 sib4. mib'8 |
    mib'2. mib'8. mib'16 |
    mib'2 do''4. lab'8 |
    fa'1 |
    <<
      { fa'4 fa'8. fa'16 fa'4 fa' | } \\
      { re'4 re'8. re'16 re'4 re' | }
    >>
    mib'1 |
    mib'4 mib'8. mib'16 re'4 re' |
  }
>>
mib'4. sib16 do'32 re' mib'4 mib' |
<mib' sol>2~ q4 r |
<<
  \tag #'violino1 {
    do''4:16\fp do''8. si'16 do''4:16\fp do''8. si'16 |
    do''16\f re'' mib'' re'' do'' re'' mib'' re'' do'' re'' mib'' re'' do'' re'' mib'' do'' |
    re''8\p sol''16 re'' si'8 re''16 si' sol'8 re''16 si' sol'8 fa' |
  }
  \tag #'violino2 {
    mib'4:16-\sug\fp mib'8. re'16 mib'4:16-\sug\fp mib'8. re'16 |
    <mib' sol>2:16-\sug\f q:16 |
    <sol re'>:16-\sug\p q:16 |
  }
>>
mib'16-\sug\f do' re' mib' fa' sol' la' si' do'' si' re'' do'' mib'' re'' do'' sib' |
<<
  \tag #'violino1 {
    lab'!2:16\fp sib':16\fp |
  }
  \tag #'violino2 {
    lab'!16-\sug\fp mib' mib' mib' mib'4:16 fa'2:16-\sug\fp |
  }
>>
sol'16-\sug\f mib' fa' sol' lab' sib' do'' re'' mib'' re'' fa'' mib'' sol'' fa'' mib'' re'' |
do'' <<
  \tag #'violino1 {
    do'16 do' do' do'4:16 do'2:16 |
    si2.\fermata r8 sib''! |
    sib''4. sol''8 \grace sib''8 lab''4 sol''8 fa'' |
    mib''2. mib''8. mib''16 |
    fa''2 sol''4. lab''8 |
    sib''2. \grace sib''8 lab'' sol''16 lab'' |
    sol''2 r4 mib''8. mib''16 |
    do'''2\cresc do'''4. re'''8 |
    mib'''2. re'''8\p do''' |
    \grace do''' sib''4 sol''8 sib'' \grace sib'' lab''4 sol''8 fa'' |
    mib''4 sib'2:16\ff si'4:16 |
    do''4:16\fp do''8. si'16 do''4:16\fp do''8. si'16 |
    do''16\f re'' mib'' re'' do'' re'' mib'' re'' do'' re'' mib'' re'' do'' re'' mib'' do'' |
    re''8 sol''16 re'' si'8 re''16 si' sol'8 re''16 si' sol'8 fa' |
  }
  \tag #'violino2 {
    mib'16 mib' mib' mib'4:16 fad'2:16 |
    sol'2.\fermata r4 |
    sib'1-\sug\p~ |
    sib' |
    sib' |
    re'2. fa'4 |
    sib2. mib'8. mib'16 |
    do''2-\sug\cresc do''4. re''8 |
    mib''2~ mib''8 mib'-\sug\p mib' mib' |
    sol'4 mib'8 sol' \grace sol' fa'4 mib'8 re' |
    mib'4 sol'2:16-\sug\ff fa'4:16 |
    <mib' sol>4:16-\sug\fp q8. <sol re'>16 <sol mib'>4:16-\sug\fp q8. <sol re'>16 |
    <sol mib'>2:16-\sug\f q:16 |
    <sol re'>2:16 q:16 |
  }
>>
mib'16\f do' re' mib' fa' sol' la' si' do'' si' re'' do'' mib'' re'' do'' sib'! |
<<
  \tag #'violino1 {
    lab'!2:16\fp sib':16\fp |
  }
  \tag #'violino2 {
    lab'!16-\sug\fp mib' mib' mib' mib'4:16 <fa' sib>2:16-\sug\fp |
  }
>> |
sol'16\f mib' fa' sol' lab' sib' do'' re'' mib'' re'' fa'' mib'' sol'' fa'' mib'' re'' |
<<
  \tag #'violino1 {
    do'' mib'' fad' fad' fad'4:16 fad':16 fad':16 |
    sol'2\fermata r4 r8 sib''\p |
    sib''4. sol''8 \grace sib'' lab''4 sol''8 fa'' |
    mib''2. mib''8. mib''16 |
    fa''2 sol''4. lab''8 |
    sib''2. \grace sib''8 lab'' sol''16 lab'' |
    sol''2. mib''8. mib''16 |
    do'''2\cresc do'''4. re'''8\! |
    mib'''2.\fp re'''8 do''' |
    \grace do''' sib''4 sol''8 sib'' \grace sib'' lab''4 sol''8 fa'' |
    mib'''1\ff~ |
    mib'''2. re'''8\p do''' |
    sib''4 sol''8 sib'' \grace sib'' lab''4 sol''8 fa'' |
    mib''2\f re'''4. mib'''16 fa''' |
    mib'''8 sib'' sib'' sib'' re'''4. mib'''16 fa''' |
    mib'''4. mib'''8 mib'''4. sib''8 |
    sib''4.
  }
  \tag #'violino2 {
    do''16 do' do' do' do'4:16 do':16 do':16 |
    si2\fermata r2 |
    sib'1-\sug\p~ |
    sib'~ |
    sib' |
    re'2. fa'4 |
    sib2. mib'8. mib'16 |
    do''2-\sug\cresc do''4. re''8 |
    mib''2-\sug\fp~ mib''8 mib' mib' mib' |
    sol'4 mib'8 sol' \grace sol' fa'4 mib'8 re' |
    sol'-\sug\f sol' lab' lab' sib' sib' do'' do'' |
    sib' sol' sib' sol' sib' sol' mib'-\sug\p mib' |
    mib' sib mib' sol' \grace sol' fa'4 mib'8 re' |
    mib'8-\sug\f sol' sib' mib'' re''4. mib''16 fa'' |
    mib''8 sib' sib' sib' re''4. mib''16 fa'' |
    mib''4. <sib' sol''>8 q4. q8 |
    q4.
  }
>> <sib' sol''>8 q4. <sol' mib''>8 |
q4. <mib' sol>8 q4 q |
q2 <mib' sib' sol''> |
<mib' sol> r |
