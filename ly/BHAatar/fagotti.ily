\clef "bass" mib1\fermata |
R1*9 |
do4.-\sug\fp sol,8 do4.-\sug\fp sol,8 |
do2.-\sug\f do'4 |
si1-\sug\p |
do'-\sug\f |
do2-\sug\fp re-\sug\fp |
mib1-\sug\f |
lab, |
sol,2.\fermata r4 |
R1*9 |
do4.-\sug\fp sol,8 do4.-\sug\fp sol,8 |
do2:8-\sug\f do:8 |
si,:8 si,:8 |
do:8\f do:8 |
do:8\fp re:8\fp |
mib:8\f mib:8 |
lab,:8 lab,:8 |
sol,\fermata r |
R1*8 |
<>-\sug\f \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sol4 lab sib do' | sib2. }
  { mib4 fa sol lab | sol2. }
>> r4 |
R1 |
mib8-\sug\f sib sol sib lab sib lab sib |
sol sib sol sib lab sib lab sib |
sol mib mib mib mib2:8 |
mib:8 mib:8 |
mib4. mib8 mib4 mib |
mib2 mib |
mib r |

