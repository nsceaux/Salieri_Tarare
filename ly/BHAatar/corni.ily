\clef "treble" \transposition mib
\twoVoices#'(corno1 corno2 corni) <<
  { \tieDashed do''2\fermata~ do''4 }
  { \tieDashed mi'2\fermata~ mi'4 }
>> r4 |
R1*9 |
\twoVoices#'(corno1 corno2 corni) <<
  { mi''2 mi'' |
    mi''1 |
    mi'' |
    do'' |
    do''4 s re'' s |
    mi''1 |
    do''4 s do'' s |
    mi'2.\fermata s4 | }
  { do''4. re''8 do''4. re''8 |
    do''1 |
    mi'1 |
    mi' |
    do''4 s sol' s |
    do''1 |
    do''4 s do'' s |
    mi'2.\fermata s4 | }
  { s2-\sug\fp s-\sug\fp | s1-\sug\f | s1-\sug\p | s1-\sug\f |
    s4-\sug\fp r s-\sug\fp r | s1-\sug\f | s4 r s r | s2. r4 | }
>>
R1*7 |
r4 \twoVoices#'(corno1 corno2 corni) <<
  { mi''8. mi''16 re''4 re'' |
    do'' mi''2 mi''4 |
    mi''2 mi'' |
    mi''1 |
    mi'' |
    do''1 |
    do''4 s re'' s |
    mi''1 |
    do''2 do'' |
    mi''2.\fermata }
  { do''8. do''16 sol'4 sol' |
    mi' do''2 re''4 |
    do''4. re''8 do''4. re''8 |
    do''1 |
    mi' |
    mi' |
    do''4 s re'' s |
    do''1 |
    do''2 do'' |
    mi'2.\fermata }
  { s2. | s4 s2.\f | s2-\sug\fp s2-\sug\fp | s1*2-\sug\f | s1-\sug\f |
    s4-\sug\fp r s-\sug\fp r | s1-\sug\f }
>> r4 |
R1*10 |
r4 <>-\sug\p \twoVoices#'(corno1 corno2 corni) <<
  { mi''8. mi''16 re''4 re'' |
    do''8 sol'' sol'' sol'' sol''2~ |
    sol''1~ |
    sol''4. do''8 do''4. sol''8 |
    sol''4. mi''8 mi''4. do''8 |
    do''4. do''8 do''4 do'' |
    do''2 do'' |
    do'' }
  { do''8. do''16 sol'4 sol' |
    do''8 sol' sol' sol' sol'2~ |
    sol'1~ |
    sol'4. mi'8 mi'4. mi''8 |
    mi''4. do''8 do''4. mi'8 |
    mi'4. mi'8 mi'4 mi' |
    mi'2 mi' |
    mi' }
  { s2. | s1-\sug\f }
>> r2 |
