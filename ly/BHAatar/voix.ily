\clef "bass" <>^\markup\italic Fièrement
r2 r4 r8 sib |
sib4. sol8 sol4. mib8 |
do'4 lab r lab8 sib |
do'2 lab4. fa8 |
re1 |
sib4 sib8 sib sib4. sib8 |
sol4 sib r2 |
fa4 fa8 fa sib4 sib |
mib2 r |
<>^"Serré" r2 r4 sol |
do'4. si8 do'4. si8 |
do'2. do'4 |
re' si sol fa |
fa mib r2 |
lab4 lab8 lab sib4 sib |
sol2 sol4. sol8 |
do'2 mib'4. do'8 |
sol2.\fermata sib!4 |
<>^"Retenu" sib4. sol8 \grace sib8 lab4 sol8[ fa] |
mib2. mib8 mib |
fa2 sol4. lab8 |
sib2.( lab4) |
sol2 r4 mib8 mib |
do'2 do'4. re'8 |
mib'2. re'8[ do'] |
sib4 sol8[ sib] \grace sib lab4 sol8[ fa] |
mib2 r4 r8 sol |
do'4. si8 do'4. si8 |
do'2. do'4 |
re' si sol fa |
fa4 mib r2 |
lab4 lab8 lab sib4. sib8 |
sol2 sol4 sol |
mib'2 do'4. do'8 |
sol2.\fermata sib!4 |
sib4. sol8 \grace sib lab4 sol8[ fa] |
mib2. mib8 mib |
fa2 sol4. lab8 |
sib2.( lab4) |
sol2 r4 mib8. mib16 |
do'2 do'4. re'8 |
mib'2. re'8[ do'] |
\grace do'8 sib4 sol8[ sib] \grace sib8 lab4 r8 fa |
mib'1~ |
mib'2. re'8[ do'] |
sib4 sol8[ sib] \grace sib lab4 sol8[ fa] |
mib2 r |
R1*6 |
