\score {
  \new GrandStaff \with {
    instrumentName = \markup\center-column { Cors en Mi♭ }
  } <<
    \new Staff \notemode <<
      \keepWithTag #'corni \global
      \keepWithTag #'corno1 \includeNotes "corni"
      { \quoteViolinoI "BHAviolin"
        s1
        <>^\markup\tiny "Vln" \cueTransp "BHAviolin" do' {
          \mmRestDown s1*4 \mmRestUp s1*5
        }
        s1*7 s2..
        \cueTransp "BHAviolin" do' {
          \mmRestDown s8 s1*7 s4
        }
        s2. s1*8 s2..
        \cueTransp "BHAviolin" do' {
          \mmRestDown s8 s1*10 s4
        }
      }
    >>
    \new Staff \with { \haraKiriFirst } <<
      \keepWithTag #'corni \global
      \keepWithTag #'corno2 \includeNotes "corni"
    >>
  >>
  \layout { indent = \largeindent }
}
