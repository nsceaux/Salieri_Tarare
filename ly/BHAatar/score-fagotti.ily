\score {
  \new Staff <<
    \global \includeNotes "fagotti"
    { \quoteBasso "BHAbasso"
      s1
      <>^\markup\tiny "B." \cue "BHAbasso" { \mmRestDown s1*9 }
      s1*8
      \cue "BHAbasso" { \mmRestDown s1*9 }
      s1*8
      \cue "BHAbasso" { \mmRestDown s1*8 \mmRestCenter }
    }
  >>
  \layout { }
}
