\score {
  \new GrandStaff <<
    \new Staff \notemode <<
      \global \keepWithTag #'oboe1 \includeNotes "oboi"
      { \quoteViolinoI "BHAviolin"
        s1
        <>^\markup\tiny "Vln" \cue "BHAviolin" {
          \mmRestDown s1*4 \mmRestUp s1*5
        }
        s1*8
        \cue "BHAviolin" { \mmRestDown s1*8 }
        s1*9
        \cue "BHAviolin" { \mmRestDown s1*8 \mmRestCenter }
      }
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'oboe2 \includeNotes "oboi"
    >>
  >>
  \layout { }
}
