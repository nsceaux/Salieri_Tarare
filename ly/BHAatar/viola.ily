\clef "alto" mib'1\fermata |
mib'-\sug\f |
lab2. do'8. sib16 |
lab1 |
sib |
sib4 sib8. sib16 sib4 sib |
mib'4. sib'16 do''32 re'' mib''8 sib' sol' mib' |
lab2 sib |
mib'4. sib16 do'32 re' mib'4 mib' |
mib'2~ mib'4 r |
sol'4:16-\sug\fp sol'8. sol'16 sol'4:16-\sug\fp sol'8. sol'16 |
<mib' sol>2:16-\sug\f q:16 |
<sol re'>2:16-\sug\p q:16 |
<sol mib'>2:16-\sug\f q:16 |
do':16-\sug\fp sib:16-\sug\fp |
<sib sol'>2:16-\sug\f q:16 |
lab'16 lab lab lab lab4:16 lab2:16 |
sol2.\fermata r4 |
mib8\p sol sib sol fa lab sib lab |
sol sib sol sib sol sib sol sib |
re sib re sib mib sib mib sib |
fa sib fa sib fa sib re sib |
mib sol sib sol mib sol sib sol |
lab-\sug\cresc do' lab do' lab do' sib lab |
sol sib sol sib sol-\sug\p sib lab lab |
sib2:16 sib:16 |
mib'4 sol'2:16-\sug\ff fa'4:16 |
mib'4.-\sug\fp sol8 do'4.\fp sol8 |
do'2:8-\sug\f do':8 |
si:8 si:8 |
<sol mib'>2:16-\sug\f q:16 |
do':16-\sug\fp sib:16-\sug\fp |
<sib sol'>2:16-\sug\f q:16 |
lab'16 lab lab lab lab4:16 lab:16 lab:16 |
sol2\fermata r2 |
mib8\p sol sib sol fa lab sib lab |
sol sib sol sib sol sib sol sib |
re sib re sib mib sib mib sib |
fa sib fa sib fa sib re sib |
mib sol sib sol mib sib sol mib |
lab-\sug\cresc do' lab do' lab do' sib lab |
sol\fp sib sol sib sol sol lab lab |
sib2:8 sib:8 |
mib'8\f mib' fa' fa' sol' sol' lab' lab' |
sol' mib' sol' mib' sol' mib' lab'-\sug\p lab' |
sib'2:8 sib:8 |
mib'8\f sib' sol' sib' lab' sib' lab' sib' |
sol' sib' sol' sib' lab' sib' lab' sib' |
sol' mib' mib' mib' mib'2:8 |
mib':8 mib':8 |
mib'4. mib'8 mib'4 mib' |
mib'2 mib' |
mib' r |
