Ver -- tu fa -- rouche et fiè -- re,
qui je -- tait trop d’é -- clat,
ren -- tre dans la pous -- siè -- re,
fai -- te pour un sol -- dat.
Du cri -- me d’Al -- ta -- mort je vois la mer char -- gé -- e,
rendre à ton corps san -- glant les fu -- nè -- bres hon -- neurs.
Et nous, heu -- reux A -- tar, de ma belle af -- fli -- gé -- e,
dans la joie et l’a -- mour, nous sé -- che -- rons les pleurs.
Du cri -- me d’Al -- ta -- mort je vois la mer char -- gé -- e,
rendre à ton corps san -- glant les fu -- nè -- bres hon -- neurs.
Et nous, heu -- reux A -- tar, de ma belle af -- fli -- gé -- e,
dans la joie et l’a -- mour, nous sé -- che -- rons les pleurs, __
nous sé -- che -- rons les pleurs.
