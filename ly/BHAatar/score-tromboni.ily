\score {
  \new StaffGroup <<
    \new Staff <<
      \global \keepWithTag #'tr1 \includeNotes "tromboni"
      { \quoteViolinoI "BHAviolin"
        <>^\markup\tiny "Vln" \cueTreble "BHAviolin" {
          \mmRestUp s1*2 \mmRestDown s1*3 \mmRestUp s1*6 \mmRestCenter
        }
        s1*7
        \cueTreble "BHAviolin" {
          \mmRestDown s1*8 \mmRestUp s1*2 \mmRestCenter
        }
      }
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'tr2 \includeNotes "tromboni"
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'tr3 \includeNotes "tromboni"
    >>
  >>
  \layout { }
}
