\clef "treble"
\twoVoices#'(oboe1 oboe2 oboi) <<
  { \once\tieDashed mib'2\fermata~ mib'4 }
  { \once\tieDashed mib'2\fermata~ mib'4 }
>> r4 |
R1*9 |
\twoVoices#'(oboe1 oboe2 oboi) <<
  { do''4. si'8 do''4. si'8 |
    do''4 mib''2 mib''4 |
    re''1 |
    mib''1 |
    mib''4 s fa'' s |
    sol''1 |
    mib''2 fad'' |
    sol''2.\fermata s4 | }
  { mib'4. re'8 mib'4. re'8 |
    mib'4 sol'2 sol'4 |
    sol'1 |
    sol'1 |
    lab'!4 s fa' s |
    mib'1 |
    do''2 do'' |
    si'2.\fermata s4 | }
  { s2-\sug\fp s2-\sug\fp | s1-\sug\f s1-\sug\p | s-\sug\f |
    s4-\sug\fp r s-\sug\fp r | s1*2-\sug\f | s2. r4 | }
>>
R1*8 |
r4 <>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { sib'2 si'4 |
    do''4. si'8 do''4. si'8 |
    do''1 |
    re'' |
    mib''1 |
    mib''4 s fa'' s |
    sol''1 |
    mib''2 fad'' |
    sol''2.\fermata }
  { sol'2 fa'4 |
    mib'4. fa'8 mib'4. fa'8 |
    mib'1 |
    sol' |
    sol'1 |
    lab'!4 s fa' s |
    mib'1 |
    do''2 do'' |
    si'2.\fermata }
  { s2. | s2-\sug\fp s2-\sug\fp | s1*2-\sug\f | s1-\sug\f |
    s4-\sug\fp r s-\sug\fp r | s1-\sug\f }
>> r4 |
R1*8 |
<>-\sug\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { sol''4 lab'' sib'' do''' | sib''2. }
  { mib''1 | mib''2. }
>> r4 |
R1 |
<>\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { mib''2 fa'' |
    mib'' fa'' |
    mib''4. mib''8 mib''4. sib''8 |
    sib''4. sol''8 sol''4. mib''8 |
    mib''4. mib''8 mib''4 mib'' |
    mib''2 sol'' |
    mib'' }
  { mib''2 re'' |
    mib'' re'' |
    mib''4. sol'8 sol'4. sol''8 |
    sol''4. mib''8 mib''4. sol'8 |
    sol'4. sol'8 sol'4 sol' |
    sol'2 sib' |
    sol' }
>> r2 |
