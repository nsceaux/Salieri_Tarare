\tag #'all \key do \major \midiTempo#100
\time 4/4 s1*6
\tempo "Andante" s1*5
\tempo "Allegro assai" s1*2
\tempo "Allegro assai" s1*30 \bar "|."
