\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes et Cors en Mi♭ }
        shortInstrumentName = \markup\center-column { Tr. Cor. }
      } <<
        { \noHaraKiri s1*11 \revertNoHaraKiri }
        \keepWithTag#'() \global \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with { \tromboniInstr } <<
        { \noHaraKiri s1*11 \revertNoHaraKiri }
        \global \keepWithTag #'tromboni \includeNotes "tromboni"
      >>
      \new Staff \with { \fagottiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Tarare
      shortInstrumentName = \markup\character Ta.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*4\break s1*4\pageBreak
        s1*3\break s1*3\break s1*3\pageBreak
        s1*3 s2 \bar "" \break s2 s1*2 s2 \bar "" \break s2 s1*3\pageBreak
        s1*3\break s1*5\pageBreak
        s1*4\break
      }
      \modVersion { s1*12\break }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
