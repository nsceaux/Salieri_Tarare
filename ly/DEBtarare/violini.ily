\clef "treble" <>
<<
  \tag #'violino1 {
    sol'1~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol' |
    la'4~ la'8.
    
  }
  \tag #'violino2 {
    <sol mi'!>1~ |
    q~ |
    q~ |
    q~ |
    q~ |
    q |
    fa'4~ fa'8.
  }
>> \grace si8 la32 sold la4 <<
  \tag #'violino1 {
    la'4~ |
    la'4~ la'8.
  }
  \tag #'violino2 {
    fa'4~ |
    fa'4~ fa'8.
  }
>> \grace si8 la32 sold la4 <<
  \tag #'violino1 {
    la'4 |
    sold'1~ |
    sold'~ |
    sold' |
  }
  \tag #'violino2 {
    fa'4 |
    re'1~ |
    re'~ |
    re' |
  }
>>
la16(\f si do' re') mi'( re' do' si) la( si do' re') mi'( re' do' si) |
la4 r r2 |
la16(\f do' mi' la' la' mi' do' la) la( dod' mi' la' la' mi' dod' la) |
la( re' fa' la' la' fa' re' la) la( mi' sol' la' la' sol' mi' la) |
<la fa'>2 r |
re'16 fa' la' re'' re'' la' fa' la' re'4 r |
r8 r16 <<
  \tag #'violino1 { sib'16 sib'4 }
  \tag #'violino2 { fa'16 fa'4 }
>> r8 r16 <fa' re''>16 q4 |
r8 r16 <<
  \tag #'violino1 { fa''16 fa''4 }
  \tag #'violino2 { q16 q4 }
>> r4 <re' sib' sib''>4 |
lab''8 sol''16 fa'' mib'' re'' do'' sib' lab' sol' fa' mib' re' do' sib lab |
sol4 r r <re' si'! sol''>4 |
R1*2 |
r2 <<
  \tag #'violino1 { mib''4 }
  \tag #'violino2 { sol' }
>> r4 |
r2 <<
  \tag #'violino1 { re''4 }
  \tag #'violino2 { la' }
>> r |
R1 |
<<
  \tag #'violino1 {
    sol'2:16\p sol':16 |
    sol'2:16 << { sol'16 sib' sib' sib' sib'4:16 } \\ { s16 sol' sol' sol' sol'4:16 } >> |
    <<
      { sib'2:16 sib':16 |
        sib':16 sib':16 |
        sib':16 sib':16 | } \\
      { sol'2:16\cresc lab':16 |
        lab':16 lab':16 |
        lab':16\cresc lab':16\! }
    >>
    <sol' sib'>:16 q:16 |
    do''16\cresc <do'' lab''> q q q4:16 q2:16 |
  }
  \tag #'violino2 {
    re'2:16-\sug\p re':16 |
    re':16 re':16 |
    re':16-\sug\cresc <re' re''>:16 |
    q:16 <sib' fa''>:16 |
    q:16-\sug\cresc q:16 |
    <sib' sol''>:16 q:16 |
    <do'' lab''>:16-\sug\cresc q:16 |
  }
>>
<do'' lab''>2:16 q:16 |
q:16 q:16 |
r16 sol\ff la! si! do' re' mi'! fad' sol'8 la'16 si' do'' re'' mi'' fad'' |
sol''4 r r2 |
r16 do'' re'' mi''! fa'' sol'' la'' si'' do''' si'' do''' si'' do''' si'' do''' si'' |
do'''4 r <<
  \tag #'violino1 { mi''4 }
  \tag #'violino2 { si'! }
>> r4 |
<<
  \tag #'violino1 { mi''4 }
  \tag #'violino2 { do'' }
>> r4 r8. <fad' red''>16 q8 r8 |
<<
  \tag #'violino1 {
    mi''1\fp~ |
    mi''2 << { fa''2~ | fa'' } \\ { re''2~ | re'' } >>
  }
  \tag #'violino2 {
    si'1-\sug\fp~ |
    si'2 <si' re'>~ |
    q
  }
>> r4 <sol re' si'> |
