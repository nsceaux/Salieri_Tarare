\clef "tenor/G_8" r2 r4 sol |
sol4. sol8 sol sol sol sol |
do'4 r sol r16 sol sol sol |
sol4 fa8 sol mi mi r sol16 la |
sib8 sib16 sib sib8 re' sib4 r16 sib la sib |
sol2 sib4 sib8 do' |
la4 la r2 |
R1 |
r4 r16 sold sold la si8. si16 si si si si |
sold4 r8 mi sold8. sold16 sold sold sold la |
si4 r8 si re'8. si16 si do' re' mi' |
do'4 do' r2 |
r4 r8 la la la16 si do'8 re'16 mi' |
la4 la r2 |
R1 |
r16 la la la re'4 fa'8 fa'16 fa' mi'8 fa' |
re'4 r r8 re' re' re' |
sib8 sib r sib16 do' re'8 re' r re'16 mib' |
fa'8 fa' r re'16 fa' sib4 r |
R1 |
r8 re'16 re' re'8. re'16 sol'4 r8 re'16 re' |
si!8. si16 si8 do'16 re' sol8 sol r16 sol sol la |
si8 si si do' re'4 re'8 mib' |
fa'8 fa'16 fa' mib'8 re' mib'8 mib' r16 sol do' si! |
do'8 do' re' mib' re'4 r8 re'16 re' |
la4 r do'8 do' do' sib |
sol sol r4 r8 sol sol la |
sib sib do' re' sol4 r16 sol sol la |
sib8 sib sib do' re' re' r fa'16 fa' |
re'8. re'16 re'8 mib'16 fa' sib8 sib r8. sib16 |
re'4 re'8 mib' fa'4 fa'8 sol' |
mib'4 r8 sib mib'8. mib'16 mib' mib' reb' mib' |
do'4. mib'8 lab'4. mib'8 |
lab'1~ |
lab'4 mib' r8 lab' fa' re' |
si!4 r r2 |
r4 re'8 re'16 re' sol'8. re'16 fa'8 mi'! |
do' do' r4 r2 |
r8 do' do' re' mi' mi' re' mi' |
do'4 r16 la si do' si4 r16 si red' si |
mi'4. si16 si mi'4. mi'8 |
mi'4 fa'!8 sol' fa' fa' r fa'16 re' |
si8. si16 si8 si16 do' sol8 sol r4 |
