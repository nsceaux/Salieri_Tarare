\clef "bass" do1~ |
do~ |
do~ |
do~ |
do~ |
do |
fa2.( mi4) |
re2.( do4) |
si,1~ |
si,~ |
si, |
la,16-\sug\f( si, do re) mi( re do si,) la,( si, do re) mi( re do si,) |
la,4 r r2 |
la2-\sug\f( sol |
fa dod) |
re r |
re4 re re r |
r8 r16 re re4 r8 r16 sib, sib,4 |
r8 r16 sib, sib,4 r sib |
sib,1 |
si,!4 r r sol,~ |
sol,1~ |
sol,~ |
sol,2 do |
do fad~ |
fad1 |
sol2:16\p sol:16 |
sol:16 sol:16 |
sol:16\cresc fa:16 |
fa:16 sib,:16 |
sib,:16-\sug\cresc sib,:16 |
mib:16 mib:16 |
lab:16\cresc lab:16 |
lab:16 lab:16 |
lab:16 lab:16 |
r16 sol,\ff la,! si,! do re mi! fad sol8 la16 si do' re' mi' fad' |
sol'4 r r2 |
r16 do re mi! fa sol la si do' si do' si do' si do' si |
do'4 r sold r |
la r r8. la16 la8 r |
sol1\fp |
sol |
r2 r4 sol |
