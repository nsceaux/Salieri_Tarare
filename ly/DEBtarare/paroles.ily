Au sein de la pro -- fon -- de mer,
seul dans u -- ne bar -- que fra -- gi -- le,
au -- cun sou -- fle n’a -- gi -- tant l’air,
je sil -- lon -- nais l’on -- de tran -- quil -- le.
Des a -- vi -- rons le mo -- no -- to -- ne bruit,
au loin dis -- tin -- gué dans la nuit,
sou -- dain a fait son -- ner l’a -- lar -- me ;
j’a -- vais ce poi -- gnard pour toute ar -- me.
Deux cents ra -- meurs par -- tent du mê -- me lieu :
on m’en -- ve -- lop -- pe, on se croi -- se, on rap -- pel -- le,
j’é -- tais pris !… D’un grand coup d’é -- pieu,
je m’a -- bîme a -- vec ma na -- cel -- le,
et, me fray -- ant sous les vais -- seaux,
u -- ne rou -- te nou -- velle et su -- re ;
j’ar -- rive à terre en -- tre deux eaux,
dé -- ro -- bé par la nuit obs -- cu -- re.
J’en -- tend la clo -- che du bé -- froi.
Le son bruy -- ant de la trom -- pet -- te,
que le fond du gol -- phe ré -- pè -- te,
aug -- men -- te le trouble et l’ef -- froi.
On court, on crie aux sen -- ti -- nel -- les,
ar -- rête ! ar -- rête : on fond sur moi :
mais, s’ils cou -- raient, j’a -- vais des ai -- les.
J’at -- teins le mur comme un é -- clair :
on cherche au pied ; j’é -- tais dans l’air,
sur l’é -- chel -- le souple et ten -- du -- e,
que ton zèle a -- vait sus -- pen -- du -- e.
