\clef "treble" \transposition mib
R1*26 |
sol''1-\sug\p |
sol''~ |
sol''-\sug\cresc~ |
sol''~ |
sol''~ |
sol'' |
fa''4\! r r2 |
R1*10 |
