\clef "alto" R1*26 |
\twoVoices #'(tr1 tr2 tromboni) <<
  { sib'1 | sib'~ | sib'~ | sib'~ |
    sib'~ | sib' | mib'~ | mib'~ |
    mib' | sol'4 }
  { sib1 | sib~ | sib~ | sib~ |
    sib~ | sib | do'~ | do'~ |
    do' | si!4 }
  { s1*2-\sug\p | s1*4-\sug\cresc | s1*3-\sug\cresc | s4-\sug\ff }
>> r4 r2 |
R1*7 |
