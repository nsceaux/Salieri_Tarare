\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (fagotti #:tag-notes fagotti #:score-template "score-part-voix")
   (tromboni #:tag-notes tromboni #:score-template "score-part-voix")
   (corni #:score-template "score-part-voix"
          #:music , #{ \noHaraKiri s1 \revertNoHaraKiri #}
          #:instrument , #{ \markup\center-column { Trompettes et Cors en Mi♭ } #})
   (trombe #:score-template "score-part-voix" #:notes "corni"
          #:music , #{ \noHaraKiri s1 \revertNoHaraKiri #}
          #:instrument , #{ \markup\center-column { Trompettes et Cors in Mi♭ } #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
