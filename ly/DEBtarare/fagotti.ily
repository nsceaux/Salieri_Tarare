\clef "bass"
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol1~ | sol~ | sol~ | sol~ |
    sol~ | sol | la~ | la2. la4 |
    sold1~ | sold~ | sold | la4 }
  { mi1~ | mi~ | mi~ | mi~ |
    mi~ | mi | fa~ | fa2. fa4 |
    re1~ | re~ | re | do4 }
>> r4 r2 |
R1*14 |
sib1-\sug\p |
sib~ |
sib-\sug\cresc~ |
sib~ |
sib~ |
sib |
lab\cresc~ |
lab~ |
lab |
sol4-\sug\ff r r2 |
R1*7 |
