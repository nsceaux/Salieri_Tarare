\clef "alto" <do do'>1~ |
q~ |
do'~ |
do'~ |
do'~ |
do' |
<fa' la'>4~ q8. \grace si8 la32 sold la4 <fa' la'>4 |
q4~ q8. \grace si8 la32 sold la4 la' |
sold'1~ |
sold'~ |
sold' |
la16-\sug\f( si do' re') mi'( re' do' si) la( si do' re') mi'( re' do' si) |
la4 r r2 |
la'2-\sug\f( sol' |
fa' dod') |
re'2 r |
re'4 re' re' r |
r8 r16 re' re'4 r8 r16 sib sib4 |
r8 r16 sib sib4 r sib' |
<fa' re''>4 r r2 |
re4 r r <sol re' si'!> |
R1*2 |
r2 do'4 r |
r2 fad'4 r |
R1 |
sib2:16\p sib:16 |
sib:16 sol16 sol' sol' sol' sol'4:16 |
sol'2:16-\sug\cresc fa'!:16 |
fa':16 fa':16 |
fa':16-\sug\cresc fa':16 |
<sol mib'>:16 q:16 |
<lab mib'>16-\sug\cresc <mib' mib''> q q q4:16 q2:16 |
q:16 q:16 |
q:16 q:16 |
r16 sol la si do' re' mi' fad' sol'8 la'16 si' do'' re'' mi'' fad'' |
sol''4 r r2 |
r16 do' re' mi' fa' sol' la' si' do'' si' do'' si' do'' si' do'' si' |
do''4 r sold' r |
la'4 r4 r8. la'16 la'8 r |
sol'1\fp~ |
sol'~ |
sol'2 r4 sol' |
