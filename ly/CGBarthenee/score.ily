\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        \consists "Metronome_mark_engraver"
        \flautiInstr
      } << \global \keepWithTag #'flauti \includeNotes "flauti" >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { 
          \consists "Metronome_mark_engraver"
        } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Arthénée
      shortInstrumentName = \markup\character Ar.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s8 s2*9\break s2*7\pageBreak
        s2*6\break s2*6\pageBreak
      }
      \modVersion { s8 s2*16\break }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" } 
  }
  \midi { }
}
