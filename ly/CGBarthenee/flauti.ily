\clef "treble" fa''8\p |
la''4( sib''8 la'') |
la''( sol'' sol''4) |
sol''8-! sol''( la'' sib'') |
do'''( sib'' la''4) |
do'''8-\sug\f([ re''' mi''' fa''']) |
dod'''8 re''' \grace do'''16 sib''8 la''16 sol'' |
fa''4. \grace la''16*2 sol''8 |
fa''4 r8 fa'' |
la''4-\sug\p( sib''8 la'') |
la'' sol'' sol''4 |
sol''8[ sol''( la'' sib'')] |
do'''8( sib'') la''4 |
la''8[ la''( sib'' do''')] |
re'''4. do'''8 |
do'''( sib'') sib''( la'') |
sol''4 r |
sol''8[ sol'' sol'' do'''] |
do'''( si'') si''4 |
do'''8( mi''' re'''16 do''' si'' la'') |
sol''8([ fa'' mi'']) do'' |
sib''!2-\sug\cresc |
sib''4-\sug\fp\fermata la''8\fermata fa'' |
do'''4-\sug\mf re'''8 mib''' |
dod''' re''' \grace do'''16 sib''8-\sug\cresc la''16 sol'' |
fa''4. mi''8 |
fa''4\! r |
fa'' r |
la''4 sib''8 la'' |
la''8( sol'') sol''4 |
sol''8[ sol''( la'' sib'')] |
do'''( sib'') la''4 |
do'''8[( re''' mi''' fa''')] |
dod'''( re''' \grace do'''16 sib''8 la''16 sol'') |
fa''4. mi''8 |
fa''4 r |
