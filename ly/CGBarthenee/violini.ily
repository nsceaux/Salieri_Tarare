\clef "treble"
<<
  \tag #'violino1 {
    fa'8\p |
    la'4( sib'8 la') |
    la'( sol' sol'4) |
    sol'8-! sol'( la' sib') |
    do''( sib' la'4) |
    do''8([\f re'' mi'' fa'']) |
    dod'' re'' \grace do''16 sib'8 la'16 sol' |
    fa'4. \appoggiatura la'8 sol' |
    fa'4. fa'8 |
    la'4\p( sib'8 la') |
    la' sol' sol'4 |
    sol'8[ sol'( la' sib')] |
    do''( sib') la'4 |
    la'8[ la'( sib' do'')] |
    re''4. do''8 |
    do''( sib') sib'( la') |
    sol'4 r |
  }
  \tag #'violino2 {
    r8 |
    do'2\p~ |
    do'~ |
    do'4. fa'8 |
    mi' sol' do'4 |
    R2 |
    r8 fa'-\sug\f([ re' sib]) |
    la4.( sib8) |
    la4. fa'8 |
    do'2-\sug\p~ |
    do' |
    do'4.( fa'8) |
    mi' sol' do'4 |
    R2 |
    r8 \once\slurDashed fad'([ sol' la']) |
    re'4 sol'8 fa'! |
    mi'2 |
  }
>>
<<
  \tag #'violino1 {
    sol'8[ sol' sol' do''] |
    do''( si') si'4 |
    do''8( mi'' re''16 do'' si' la') |
    sol'8([ fa' mi']) do' |
    sib'!16\cresc sib'8 sib' sib' sib'16 |
    sib'4\fp\fermata la'8\fermata fa' |
    do''4\mf re''8 mib'' |
    dod'' re'' \grace do''16 sib'8\cresc la'16 sol' |
    fa'4. mi'8 |
    fa'4\! r |
    fa'8 la'([ do'' fa'']) |
    la''4 sib''8 la'' |
    la''8( sol'') sol''4 |
    sol''8[ sol''( la'' sib'')] |
    do'''( sib'') la''4 |
    do'''8[( re''' mi''' fa''')] |
    dod'''( re''' \grace do'''16 sib''8 la''16 sol'') |
    fa''4. \grace la''16 sol''8 |
    fa''4 r |
  }
  \tag #'violino2 {
    mi'16( sol' mi' sol' mi' sol' mi' sol') |
    fa'( sol' fa' sol' fa' sol' fa' sol') |
    mi'( sol' mi' sol' mi' sol' mi' do') |
    re'( si sol) sol sol( do' mi' sol') |
    sol'16-\sug\cresc sol'8 sol' sol' sol'16 |
    sol'4-\sug\fp\fermata fa'8\fermata r |
    fa'2-\sug\mf~ |
    fa'4~ fa'8-\sug\cresc sib |
    la4.( sol8) |
    la4\! r |
    la8_\markup\center-align\tiny\line\whiteout { Source : \italic fa } la([ do' fa']) |
    la'4 sib'8 la' |
    \slurDashed la'8( sol') sol'4 |
    sol'8[ sol'( la' sib')] |
    do''( sib') la'4 |
    \slurSolid do''8[( re'' mi'' fa'')] |
    \slurDashed dod''( re'' \grace do''16 sib'8 la'16 sol') |
    fa'4. \grace la'16 sol'8 |
    fa'4 r |
  }
>>
