\clef "alto" r8 |
\slurDashed fa4(-\sug\p sol8 fa) |
fa( mi mi4) |
r8 mi([ fa re]) |
do mi fa4 |
r la-\sug\f |
sib sib |
\slurSolid do'4.( mi'8) |
fa'4 r |
\slurDashed fa(-\sug\p sol8 fa) |
fa( mi mi4) |
mi8[( mi) fa( re)] |
do( mi fa4) |
R2 |
r8 re([ mi fad]) |
sol4 mi8 fa! |
do2 |
\slurSolid mi'16( sol' mi' sol' mi' sol' mi' sol') |
fa'( sol' fa' sol' fa' sol' fa' sol') |
mi'( sol' mi' sol' mi' sol' mi' do') |
re'( si sol sol) sol( do' mi' sol') |
mi'-\sug\cresc mi'8 mi' mi' mi'16 |
mi'4-\sug\fp\fermata fa'8\fermata r |
r8 \once\slurDashed la(-\sug\mf[ sib do']) |
sib4 sib-\sug\cresc |
do'2 |
fa4\! r |
fa r |
do'2~ |
do'8 do'4 do'8 |
r4 fa' |
mi'8 sol' do'4 |
r fa' |
fa' re'8 sib |
la4. sib8 |
la4 r |
