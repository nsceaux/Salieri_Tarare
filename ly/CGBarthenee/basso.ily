\clef "bass" r8 |
r fa([\p sol fa]) |
fa( mi mi4) |
r8 mi([ fa re]) |
do mi fa4 |
r la\f |
sib sib, |
r8 do[ do do] |
fa4 r |
fa\p( sol8 fa) |
fa( mi mi4) |
mi8( mi) fa( re) |
do( mi fa4) |
R2 |
r8 re[( mi fad)] |
sol4 mi8 fa! |
do2 |
r4 do |
re2 |
do |
si,4 do |
do8[(\cresc mi sol mi)] |
do4\fp\fermata fa8\fermata r |
r8 la,-\sug\mf[( sib, do)] |
sib,4 sib,-\sug\cresc |
do8[ do do do] |
fa4\! r |
fa r |
r8 fa([ sol fa]) |
fa8 mi mi4 |
r8 mi[( fa re)] |
do mi fa4 |
r la |
sib, sib, |
do do, |
fa, r |
