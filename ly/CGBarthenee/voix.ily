\clef "bass" r8^"Chanté" |
R2*7 |
<>^\markup\italic { Ton très mielleux }
r4 r8 fa |
la4 sib8 la |
la[ sol] sol4 |
sol8 sol la sib |
do'8.[ sib16] la4 |
la8 la sib do' |
re'4. do'8 |
do'[ sib] sib la |
sol4 r |
sol8 sol sol do' |
do'[ si] si4 |
do'8 mi' re'16[ do'] si[ la] |
sol8[ fa] mi do |
sib!4 sib8 sib |
do'8.[\fermata sib!16] la8\fermata fa |
do'4 re'8 mib' |
dod'[ re'] \grace do'16 sib8 la16[ sol] |
fa4. mi8 |
fa4 r |
fa r |
R2*8 |
