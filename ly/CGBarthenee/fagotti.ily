\clef "bass" r8 |
\new CueVoice {
  r8 fa([\p sol fa]) |
  fa( mi mi4) |
  r8 mi([ fa re]) |
  do mi fa4 |
  r la\f |
  sib sib, |
  r8 do[ do do] |
}
fa4 r |
\new CueVoice {
  fa4\p( sol8 fa) |
  fa( mi mi4) |
  mi8( mi) fa( re) |
  do( mi fa4) |
  R2 |
  r8 re[( mi fad)] |
  sol4 mi8 fa! |
  do2 |
}
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sol2~ | sol | sol | sol | }
  { mi2 | fa | mi | re4 mi | }
>>
do8[(\cresc mi sol mi)] |
do4\fp\fermata fa8\fermata r |
r8 la,-\sug\mf([ sib, do]) |
sib,4 sib,-\sug\cresc |
do8[ do do do] |
fa4\! r |
fa r |
do'2~ |
do'2 |
r4 fa' |
mi'8 sol' do'4 |
r fa' |
fa' re'8 sib |
la4. sib8 |
la4 r |
