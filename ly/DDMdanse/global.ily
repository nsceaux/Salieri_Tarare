\tag #'all \key re \major
\midiTempo#120
\time 2/4 s2*16 \bar "|." s2*16 \bar "|." s2*16 \bar ".|:" s2*8 \bar ":|.|:"
s2*32 \bar ":|." s2*23 \bar ".|:" s2*12 \bar ":|.|:" s2*17 \bar "|."
