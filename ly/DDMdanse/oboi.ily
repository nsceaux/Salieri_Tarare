\clef "treble" R2*4 |
<>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'8 mi'' mi'' mi'' |
    fad'' re'' re'' re'' |
    la' mi'' mi'' mi'' |
    fad'' re'' re''4 | }
  { mi'8 la' la' la' |
    la' fad' fad' fad' |
    mi' la' la' la' |
    la' fad' fad'4 | }
>>
R2*4 |
<>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'8 mi'' mi'' mi'' |
    fad'' re'' re'' re'' |
    la' mi'' mi'' mi'' |
    fad'' re'' re''4 | }
  { mi'8 la' la' la' |
    la' fad' fad' fad' |
    mi' la' la' la' |
    la' fad' fad'4 | }
>>
%%
<>-\sug\fp \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2~ |
    re''~ |
    re''~ |
    re'' |
    fad''4. la''8 |
    \grace si'' la'' sol'' \grace la'' sol'' fad'' |
    \grace sol'' fad'' mi'' \grace fad'' mi'' re'' |
    dod''4 la' | }
  { re''2~ |
    re''~ |
    re''~ |
    re'' |
    re''4. fad''8 |
    mi''4 mi''8 re'' |
    si'4 si' |
    mi' la' | }
  { s2*4 | s4.-\sug\f s8-\sug\p | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2~ |
    re''~ |
    re''~ |
    re'' |
    fad''4. la''8 |
    la''8-! si''-! sol''-! la''-! |
    fad''-! sol''-! mi''-! fad''-! |
    re''4 }
  { re''2~ |
    re''~ |
    re''~ |
    re'' |
    re''4. fad''8 |
    fad''-! sol''-! mi''-! fad''-! |
    re''-! mi''-! dod''-! re''-! |
    fad'4 }
  { s2*4-\sug\fp | <>-\sug\fp }
>> r4 |
%%
R2*4 |
<>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'8 mi'' mi'' mi'' |
    fad'' re'' re'' re'' |
    la' mi'' mi'' mi'' |
    fad'' re'' re''4 | }
  { mi'8 la' la' la' |
    la' fad' fad' fad' |
    mi' la' la' la' |
    la' fad' fad'4 | }
>>
R2*4 |
<>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'8 mi'' mi'' mi'' |
    fad'' re'' re'' re'' |
    la' mi'' mi'' mi'' |
    fad'' re'' re''4 | }
  { mi'8 la' la' la' |
    la' fad' fad' fad' |
    mi' la' la' la' |
    la' fad' fad'4 | }
>>
%%
<<
  \tag #'(oboe1 oboi) {
    R2*4 |
    <>^"solo" fad''4.-\sug\f( sol''8) |
    mi''4.( fad''8) |
    re''4. mi''8 |
    dod''4 fad' |
  }
  \tag #'oboe2 { R2*8 }
>>
%%
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4. re''8 |
    \grace fad'' mi'' re'' re''4 |
    fad''4. fad''8 |
    \grace la'' sol'' fad'' fad''4 | }
  { fad'4. fad'8 |
    \grace la' sol' fad' fad'4 |
    re''4. re''8 |
    \grace fad'' mi'' re'' re''4 | }
  { s4.-\sug\f s8-\sug\p | s2 | s4.-\sug\f s8-\sug\p }
>>
\tag #'oboi <>^"[à 2]" la''4.-\sug\f fad''16 re'' |
la'4-\sug\p la' |
la'8 si' dod'' la' |
re''4 re' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4. re''8 |
    \grace fad'' mi'' re'' re''4 |
    fad''4. fad''8 |
    \grace la'' sol'' fad'' fad''4 | }
  { fad'4. fad'8 |
    \grace la' sol' fad' fad'4 |
    re''4. re''8 |
    \grace fad'' mi'' re'' re''4 | }
  { s4.-\sug\f s8-\sug\p | s2 | s4.-\sug\f s8-\sug\p }
>>
\tag #'oboi <>^"[à 2]" la''4.-\sug\f fad''16 re'' |
la'4-\sug\p la' |
la'8 si' dod'' la' |
re''4 r |
%%
<<
  \tag #'(oboe1 oboi) {
    R2*4 |
    <>^"Hautbois seul" fad''4. sol''8 |
    mi''4. fad''8 |
    re''4. mi''8 |
    dod''4 fad' |
    R2*8 |
  }
  \tag #'oboe2 { R2*16 }
>> \allowPageTurn
%%
\tag #'oboi <>^"[à 2]" re'4-\sug\ff re'16 mi' fad' sol' |
la'8 la' la' re'' |
dod''-! re''-! dod''-! re''-! |
la'16 si' la' sol' fad'4 |
<>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'8 mi'' mi'' mi'' |
    fad'' re'' re'' re'' |
    la' mi'' mi'' mi'' |
    fad'' re'' re''4 | }
  { mi'8 la' la' la' |
    la' fad' fad' fad' |
    mi' la' la' la' |
    la' fad' fad'4 | }
>>
R2*4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la'8 mi'' mi'' mi'' |
    fad'' re'' re'' re'' |
    la' mi'' mi'' mi'' |
    fad'' re'' re'' re'' |
    re'' fad'' fad'' fad'' |
    sol'' mi'' mi'' mi'' |
    mi'' sol'' sol'' sol'' |
    la'' fad'' fad'' fad'' |
    fad'' la'' la'' la'' |
    si''2 |
    dod''' | }
  { mi'8 la' la' la' |
    la' fad' fad' fad' |
    mi' la' la' la' |
    la' fad' fad' fad' |
    fad' re'' re'' re'' |
    mi'' dod'' dod'' dod'' |
    dod'' mi'' mi'' mi'' |
    fad'' re'' re'' re'' |
    re'' fad'' fad'' fad'' |
    sol''2 |
    mi'' | }
  { s2*4 | s2\p | s2*5\cresc | s2\! }
>>
%%
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { re'''2 | }
  { re'' | }
>>
\tag #'oboi <>^"[à 2]" la''8 si'' la'' si'' |
la''8 si'' la'' si'' |
la''8 si'' la'' si'' |
la''16 si'' la'' sol'' fad''4 |
la''8 si'' la'' si'' |
la''8 si'' la'' si'' |
la''8 si'' la'' si'' |
la''16 si'' la'' sol'' fad''4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 |
    si'' |
    fad''4 mi'' | }
  { re''2 |
    re'' |
    re''4 dod'' | }
>>
%%
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''8 la'' la'' la'' |
    la''2 |
    si'' |
    fad''4 mi'' |
    fad'' mi'' |
    fad'' mi'' |
    fad'' mi'' |
    fad'' mi'' |
    fad''4. re'8 |
    re'4 re' |
    re' }
  { re''8 fad'' fad'' fad'' |
    re''2 |
    re'' |
    re''4 dod'' |
    re'' dod'' |
    re'' dod'' |
    re'' dod'' |
    re'' dod''4 |
    re''4. re'8 |
    re'4 re' |
    re' }
>> r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''4 }
  { fad'' }
>> r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad'' }
  { la' }
>> r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''4 }
  { fad'' }
>> r4 |
\ru#3 \twoVoices #'(oboe1 oboe2 oboi) <<
  { re'2 }
  { re' }
>> |


