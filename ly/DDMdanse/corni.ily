\clef "treble" \transposition re
R2*4 |
<>^"Cors seuls" -\sug\p
\twoVoices#'(corno1 corno2 corni) <<
  { sol'8 re'' re'' re'' |
    mi'' do'' do'' do'' |
    sol' re'' re'' re'' |
    mi'' do'' do''4 | }
  { sol8 sol' sol' sol' |
    sol' mi' mi' mi' |
    sol sol' sol' sol' |
    sol' mi' mi'4 | }
>>
<>^"Trompettes" -\sug\ff
\twoVoices#'(corno1 corno2 corni) <<
  { do'4 do'8 mi' |
    sol' sol' sol' do'' |
    sol'8 do'' sol' do'' |
    sol'4 mi' | }
  { do'4 do'8 mi' |
    sol' sol' sol' do'' |
    sol'8 do'' sol' do'' |
    sol'4 mi' | }
>>
<>^"Cors seuls" <>-\sug\p
\twoVoices#'(corno1 corno2 corni) <<
  { sol'8 re'' re'' re'' |
    mi'' do'' do'' do'' |
    sol' re'' re'' re'' |
    mi'' do'' do''4 | }
  { sol8 sol' sol' sol' |
    sol' mi' mi' mi' |
    sol sol' sol' sol' |
    sol' mi' mi'4 | }
>>
%%
<>^\markup\whiteout [Trompettes] \twoVoices#'(corno1 corno2 corni) <<
  { do''2~ |
    do''~ |
    do''~ |
    do'' |
    mi''4. sol''8 |
    \grace la''8 sol'' fa'' \grace sol'' fa'' mi'' |
    \grace fa'' mi'' re'' \grace mi'' re'' do'' |
    sol'4 sol' | }
  { do''2~ |
    do''~ |
    do''~ |
    do'' |
    do''4. mi''8 |
    re''4 re''8 do'' |
    R2*2 | }
  { s2*4\fp | <>^\markup\whiteout [Cors] s4.\f s2\p }
>>
<>^\markup\whiteout [Trompettes] \twoVoices#'(corno1 corno2 corni) <<
  { do''2~ |
    do''~ |
    do''~ |
    do'' |
    mi''4. sol''8 |
    sol''8-! la''-! fa''-! sol''-! |
    mi''-! fa''-! re''-! mi''-! |
    do''4 }
  { do''2~ |
    do''~ |
    do''~ |
    do'' |
    do''4. mi''8 |
    mi''-! fa''-! re''-! mi''-! |
    do''-! re''-! sol'-! sol'-! |
    mi'4 }
  { s2*4-\sug\fp | <>^\markup\whiteout [Cors] s2-\sug\fp }
>> r4 |
%%
R2*4 |
<>-\sug\p ^\markup\whiteout [Cors] \twoVoices#'(corno1 corno2 corni) <<
  { sol'8 re'' re'' re'' |
    mi'' do'' do'' do'' |
    sol' re'' re'' re'' |
    mi'' do'' do''4 | }
  { sol8 sol' sol' sol' |
    sol' mi' mi' mi' |
    sol sol' sol' sol' |
    sol' mi' mi'4 | }
>>
<>^"Trompettes" -\sug\ff
\twoVoices#'(corno1 corno2 corni) <<
  { do'4 do'8 mi' |
    sol' sol' sol' do'' |
    sol'8 do'' sol' do'' |
    sol'4 mi' | }
  { do'4 do'8 mi' |
    sol' sol' sol' do'' |
    sol'8 do'' sol' do'' |
    sol'4 mi' | }
>>
<>-\sug\p ^\markup\whiteout [Cors] \twoVoices#'(corno1 corno2 corni) <<
  { sol'8 re'' re'' re'' |
    mi'' do'' do'' do'' |
    sol' re'' re'' re'' |
    mi'' do'' do''4 | }
  { sol8 sol' sol' sol' |
    sol' mi' mi' mi' |
    sol sol' sol' sol' |
    sol' mi' mi'4 | }
>>
%%
<>^\markup\whiteout [Cors] \twoVoices#'(corno1 corno2 corni) <<
  { mi''8 s mi'' s |
    mi'' s mi'' s |
    fa'' s fa'' s |
    mi'' s mi'' s | }
  { do'' s do'' s |
    do'' s do'' s |
    re'' s re'' s |
    do'' s do'' s | }
  { s r s r |
    s r s r |
    s r s r |
    s r s r | }
>>
R2*4 |
%%
<>^\markup\whiteout [Trompettes] \twoVoices#'(corno1 corno2 corni) <<
  { do''4. do''8 |
    \grace mi'' re'' do'' do''4 |
    mi''4. mi''8 |
    \grace sol''8 fa'' mi'' mi''4 | }
  { mi'4. mi'8 |
    sol' mi' mi'4 |
    do''4. do''8 |
    \grace mi''8 re'' do'' do''4 | }
  { s4.-\sug\f s8-\sug\p | s2 | s4.-\sug\f s8-\sug\p }
>>
r4 \twoVoices#'(corno1 corno2 corni) <<
  { mi''4 | fa'' mi'' | re'' fa'' | mi'' }
  { do''4 | re'' do'' | sol' re'' | do'' }
  { s4-\sug\f | s2-\sug\p }
>> r4 |
\twoVoices#'(corno1 corno2 corni) <<
  { do''4. do''8 |
    \grace mi'' re'' do'' do''4 |
    mi''4. mi''8 |
    \grace sol''8 fa'' mi'' mi''4 | }
  { mi'4. mi'8 |
    sol' mi' mi'4 |
    do''4. do''8 |
    \grace mi''8 re'' do'' do''4 | }
  { s4.-\sug\f s8-\sug\p | s2 | s4.-\sug\f s8-\sug\p }
>>
r4 \twoVoices#'(corno1 corno2 corni) <<
  { mi''4 | fa'' mi'' | re'' fa'' | mi'' }
  { do''4 | re'' do'' | sol' re'' | do'' }
  { s4-\sug\f | s2-\sug\p }
>> r4 |
%%
<>^\markup\whiteout [Cors] \twoVoices#'(corno1 corno2 corni) <<
  { mi''8 s mi'' s |
    mi'' s mi'' s |
    fa'' s fa'' s |
    mi'' s mi'' s | }
  { do'' s do'' s |
    do'' s do'' s |
    re'' s re'' s |
    do'' s do'' s | }
  { s-\sug\p r s r |
    s r s r |
    s r s r |
    s r s r | }
>>
R2*4 |
\twoVoices#'(corno1 corno2 corni) <<
  { mi''8 s mi'' s |
    mi'' s mi'' s |
    re'' s mi'' s |
    re'' s re'' s |
    mi'' s mi'' s |
    mi'' s re'' s |
    mi'' s mi'' s |
    mi''4 }
  { do''8 s do'' s |
    do'' s do'' s |
    re'' s mi'' s |
    re'' s re'' s |
    mi'' s mi'' s |
    mi'' s re'' s |
    mi'' s mi'' s |
    mi''4 }
  { s8 r s r |
    s r s r |
    s r s r |
    s r s r |
    s r s r |
    s r s r |
    s r s r | }
>> r4 |
%%
<>^"Tutti" do'4-\sug\ff do'8 mi' |
sol' sol' sol' do'' |
sol' do'' sol' do'' |
sol'4 mi' |
<>^"Cors seuls" -\sug\p
\twoVoices#'(corno1 corno2 corni) <<
  { sol'8 re'' re'' re'' |
    mi'' do'' do'' do'' |
    sol' re'' re'' re'' |
    mi'' do'' do''4 | }
  { sol8 sol' sol' sol' |
    sol' mi' mi' mi' |
    sol sol' sol' sol' |
    sol' mi' mi'4 | }
>>
R2*4 |
<>^\markup\whiteout [Cors] \twoVoices#'(corno1 corno2 corni) <<
  { sol'8 re'' re'' re'' |
    mi'' do'' do'' do'' |
    sol' re'' re'' re'' |
    mi'' do'' do'' do'' |
    do''2~ |
    do''~ |
    do''~ |
    do''~ |
    do''~ |
    do''~ |
    do'' }
  { sol8 sol' sol' sol' |
    sol' mi' mi' mi' |
    sol sol' sol' sol' |
    sol' mi' mi' mi' |
    do'2~ |
    do'~ |
    do'~ |
    do'~ |
    do'~ |
    do'~ |
    do' }
  { s2*4 | <>^\markup\whiteout [Trompettes] s2-\sug\p | s2*5-\sug\cresc | s2\! }
>>
%%
<>-\sug\ff \ru#4 \twoVoices#'(corno1 corno2 corni) <<
  { do''2:8 }
  { do':8 }
>>
\twoVoices#'(corno1 corno2 corni) <<
  { do''4 }
  { do' }
>> r4 |
\ru#3 \twoVoices#'(corno1 corno2 corni) <<
  { do''2:8 }
  { do':8 }
>>
\twoVoices#'(corno1 corno2 corni) <<
  { do''4 }
  { do' }
>> r4 |
\twoVoices#'(corno1 corno2 corni) <<
  { do''4 do''8 do'' |
    do''4 do'' |
    mi'' re'' | }
  { do''4 do''8 do'' |
    do''4 do'' |
    do'' sol' | }
>>
do''8 sol' sol' sol' |
do''4 do''8 do'' |
do''4 do'' |
\twoVoices#'(corno1 corno2 corni) <<
  { mi''4 re'' |
    mi'' re'' |
    mi'' re'' |
    mi'' re'' |
    mi'' re'' |
    mi''4. do'8 |
    do'4 do' |
    do' }
  { do''4 sol' |
    do'' sol' |
    do'' sol' |
    do'' sol' |
    do'' sol' |
    do''4. do'8 |
    do'4 do' |
    do' }
>> r4 |
\twoVoices#'(corno1 corno2 corni) <<
  { sol''4 }
  { mi'' }
>> r4 |
\twoVoices#'(corno1 corno2 corni) <<
  { mi''4 }
  { sol' }
>> r4 |
\twoVoices#'(corno1 corno2 corni) <<
  { sol''4 }
  { mi'' }
>> r4 |
\ru#3 \twoVoices#'(corno1 corno2 corni) <<
  { do'2 | }
  { do' | }
>>
