\clef "bass" R2*4 |
\clef "tenor" <>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la8 mi' mi' mi' |
    fad' re' re' re' |
    mi mi' mi' mi' |
    fad' re' re'4 | }
  { la,8 la la la |
    la fad fad fad |
    la, la la la |
    la fad fad4 | }
>>
R2*4 |
<>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la8 mi' mi' mi' |
    fad' re' re' re' |
    la mi' mi' mi' |
    fad' re' re'4 | }
  { la,8 la la la |
    la fad fad fad |
    la, la la la |
    la fad fad4 | }
>>
\clef "bass"
%%
<>\fp \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la2 |
    si~ |
    si |
    si8 la la4 | }
  { fad2 |
    sol~ |
    sol |
    sol8 fad fad4 | }
>>
re'8\f la fad re |
dod4\p re |
sol sold |
la la, |
<>-\sug\fp \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la2 |
    si~ |
    si |
    si8 la la4 | }
  { fad2 |
    sol~ |
    sol |
    sol8 fad fad4 | }
>>
re'8\fp la fad re |
re4 sol |
la la, |
re r |
%%
R2*4 |
\clef "tenor" <>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la8 mi' mi' mi' |
    fad' re' re' re' |
    mi mi' mi' mi' |
    fad' re' re'4 | }
  { la,8 la la la |
    la fad fad fad |
    mi, la la la |
    la fad fad4 | }
>>
R2*4 |
<>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la8 mi' mi' mi' |
    fad' re' re' re' |
    la mi' mi' mi' |
    fad' re' re'4 | }
  { la,8 la la la |
    la fad fad fad |
    la, la la la |
    la fad fad4 | }
>>
\clef "bass"
%%
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fad8 s fad s |
    fad s fad s |
    sol s sol s |
    fad s fad s | }
  { re s re s |
    re s re s |
    mi s mi s |
    re s re s | }
  { s8 r8 s r |
    s r s r |
    s r s r |
    s r s r | }
>>
R2*4 |
%%
re4.-\sug\f r8 |
R2 |
re4.-\sug\f r8 |
R2 |
\clef "tenor" r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fad'4 | sol' fad' | mi' sol' | fad' }
  { re'4 | mi' re' | dod' mi' | re' }
  { s4\f | s2\p }
>> r4 |
\clef "bass" re4-\sug\f r |
R2 |
re4-\sug\f r |
dod'8-\sug\p re' re'4 |
\clef "tenor" r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fad'4 | sol' fad' | mi' sol' | fad' }
  { re'4 | mi' re' | dod' mi' | re' }
  { s4\f | s2\p }
>> r4 |
%%
\clef "bass" \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fad8 s fad s |
    fad s fad s |
    sol s sol s |
    fad s fad s | }
  { re s re s |
    re s re s |
    mi s mi s |
    re s re s | }
  { s8-\sug\p r8 s r |
    s r s r |
    s r s r |
    s r s r | }
>>
R2*4 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fad8 s fad s |
    fad s fad s |
    mi s fad s |
    mi s sol' s |
    fad' s mi' s |
    re' s dod' s |
    si s lad s |
    si4 }
  { re8 s re s |
    re s re s |
    dod s red s |
    mi s lad s |
    si s dod' s |
    si s lad s |
    si s fad s |
    fad4 }
  { s8 r8 s r |
    s r s r |
    s r s r |
    s r \clef "tenor" s r | 
    s r s r |
    s r s r |
    s r s r | }
>> r4 |
\clef "bass" re4-\sug\ff re16 mi fad sol |
la8 la la re' |
dod'-! re'-! dod'-! re'-! |
la8. sol16 fad4 |
\clef "tenor" <>-\sug\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la8 mi' mi' mi' |
    fad' re' re' re' |
    la mi' mi' mi' |
    fad' re' re'4 }
  { la,8 la la la |
    la fad fad fad |
    la, la la la |
    la fad fad4 }
>>
R2*4 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la8 mi' mi' mi' |
    fad' re' re' re' |
    la mi' mi' mi' |
    fad' re' re' re' |
    re' fad' fad' fad' |
    sol' mi' mi' mi' |
    mi' sol' sol' sol' |
    la' fad' fad' fad' |
    fad'2 |
    sol' |
    sol' | }
  { la,8 la la la |
    la fad fad fad |
    la, la la la |
    la fad fad fad |
    fad re' re' re' |
    mi' dod' dod' dod' |
    dod' mi' mi' mi' |
    fad' re' re' re' |
    la2 |
    si |
    dod' | }
  { s2*4 | s2-\sug\p | s2*5\cresc | s2\! }
>>
%%
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fad'4 }
  { re' }
  { s4\ff }
>> r4 |
\clef "bass" fad8 sol fad sol |
fad8 sol fad sol |
fad8 sol fad sol |
fad16 sol fad mi re4 fad8 sol fad sol |
fad sol fad sol |
fad sol fad sol |
fad16 sol fad mi re4 |
fad2:8 |
sol:8 |
la4 la, |
re2:8 |
fad:8 |
sol:8 |
la4 la, |
re la, |
re la, |
re la, |
re la, |
re4. re8 |
re4 re |
re r |
re r |
re r |
re r |
re2 |
re |
re |
