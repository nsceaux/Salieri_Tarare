\score {
  <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { Trompettes et Cors en Ré }
    } <<
      \new Staff <<
        \keepWithTag #'() \global \keepWithTag #'corno1 \includeNotes "corni"
      >>
      \new Staff <<
        \keepWithTag #'() \global \keepWithTag #'corno2 \includeNotes "corni"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Timbales \small en Ré }
      \haraKiri
    } <<
      { s2*88\break }
      \keepWithTag #'() \global \includeNotes "timpani"
    >>
  >>
  \layout { indent = \largeindent }
}
