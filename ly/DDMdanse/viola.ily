\clef "alto" re4-\sug\p re16 mi fad sol |
la8 la la re' |
dod' re' dod' re' |
la16 si la sol fad4 |
dod-\sug\p r |
re r |
dod r |
re r |
re'4-\sug\ff re'16 mi' fad' sol' |
la'8 la' la' re'' |
dod'' re'' dod'' re'' |
la'8. sol'16 fad'4 |
dod'\p r |
re' r |
dod' r |
re' r |
%%
fad'8-\sug\fp re' fad' re' |
sol' re' sol' re' |
sol' re' si' sol' |
sol' fad' fad' la' |
re''-\sug\f la' fad' re' |
dod'4 re' |
sol sold |
la la |
fad'8-\sug\fp re' fad' re' |
sol' re' sol' re' |
sol' re' si' sol' |
sol' fad' fad' la' |
re''-\sug\fp la' fad' re' |
re'4 sol' |
la' la |
re' r |
%%
re4-\sug\p re16 mi fad sol |
la8 la la re' |
dod'-! re'-! dod'-! re'-! |
la16 si la sol fad4 |
dod-\sug\p r |
re r |
dod r |
re r |
re'4-\sug\ff re'16 mi' fad' sol' |
la'8 la' la' re'' |
dod''-! re''-! dod''-! re''-! |
la'8. sol'16 fad'4 |
dod'\p r |
re' r |
dod' r |
re' r |
%%
si8 r si r |
si r si r |
mi' r mi' r |
re' r re' r |
r4 si-\sug\f |
r lad |
r si |
lad r |
%%
re8-\sug\f fad la-\sug\p re' |
dod'( re') re'4 |
re8-\sug\f fad la-\sug\p re' |
dod'( re') re'4 |
r4 re'-\sug\f |
mi'-\sug\p re' |
dod' mi' |
re' r |
re8-\sug\f fad la-\sug\p re' |
dod'( re') re'4 |
re8-\sug\f fad la-\sug\p re' |
dod'( re') re'4 |
r4 re'-\sug\f |
mi'-\sug\p re' |
dod' mi' |
re' r |
%%
si8-\sug\p r si r |
si r si r |
mi' r mi' r |
re' r re' r |
r4 si |
r lad |
r si |
lad r |
si8 r si r |
si r si r |
dod' r red' r |
mi' r mi' r |
re' r fad' r |
fad' r mi' r |
re' r dod' r |
re'4 r |
%%
re'4-\sug\ff re'16 mi' fad' sol' |
la'8 la' la' re'' |
dod''-! re''-! dod''-! re''-! |
la'8. sol'16 fad'4 |
dod'\p r |
re' r |
dod' r |
re' r |
re4-\sug\p re16 mi fad sol |
la8 la la re' |
dod' re' dod' re' |
la16 si la sol fad4 |
dod r |
re r |
dod r |
re8 re' re' re' |
<<
  { re'8 fad' fad' fad' |
    sol' mi' mi' mi' |
    mi' sol' sol' sol' |
    la' fad' fad' fad' |
    fad' la' la' la' |
    re' si' si' si' | } \\
  { re'8-\sug\p re' re' re' |
    mi'-\sug\cresc dod' dod' dod' |
    dod' mi' mi' mi' |
    fad' re' re' re' |
    re' fad' fad' fad' |
    re' sol' sol' sol'\! | }
>>
mi' <mi' dod''> q q |
%%
<fad' re''>4-\sug\ff r |
fad'8 sol' fad' sol' |
fad' sol' fad' sol' |
fad' sol' fad' sol' |
fad'16 sol' fad' mi' re'4 |
fad'8 sol' fad' sol' |
fad' sol' fad' sol' |
fad' sol' fad' sol' |
fad'16 sol' fad' mi' re'4 |
<re' la'>2:8 |
<re' si'>2:8 |
<fad' la'>8 q <mi' la'> q |
%%
<fad' la'>2:8 |
<re' la'>:8 |
<re' si'>:8 |
<fad' la'>8 q <mi' la'> q |
<la fad' re''>4 <la mi' dod''> |
<la fad' re''>4 <la mi' dod''> |
<la fad' re''>4 <la mi' dod''> |
<la fad' re''>4 <la mi' dod''> |
<la fad' re''>4. re'8 |
re'4 re' |
re' r |
<la fad' re''>4 r |
<la fad' re''>4 r |
<la fad' re''>4 r |
re'2 |
re' |
re' |
