\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        instrumentName = \markup\center-column { Hautbois et Clarinettes }
        shortInstrumentName = \markup\center-column { Htb. Cl. }
      } << \global \keepWithTag #'oboi \includeNotes "oboi" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes et Cors en Ré }
        shortInstrumentName = \markup\center-column { Tr. Cor. }
      } << \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni" >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff \with { \timpaniInstr } <<
        \keepWithTag #'() \global \includeNotes "timpani"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      \new Staff \with { \bassoInstr } <<
        \global \includeNotes "basso"
        \origLayout {
          s2*10\pageBreak
          s2*11\break \grace s8 s2*11\pageBreak
          s2*10\break s2*12\pageBreak
          s2*12\break s2*13\pageBreak
          s2*11\break s2*11\pageBreak
          s2*12\break s2*11\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
