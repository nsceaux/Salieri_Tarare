\clef "bass" re4\p re16 mi fad sol |
la8 la la re' |
dod' re' dod' re' |
la8. sol16 fad4 |
dod\p r |
re r |
dod r |
re r |
re\ff re16 mi fad sol |
la8 la la re' |
dod'8 re' dod' re' |
la8. sol16 fad4 |
dod\p r |
re r |
dod r |
re r |
fad8\fp re fad re |
sol re sol re |
sol re si sol |
sol fad fad la |
re'\f la fad re |
dod4\p re |
sol sold |
la la, |
fad8\fp re fad re |
sol re sol re |
sol re si sol |
sol fad fad la |
re'\fp la fad re |
re4 sol |
la la, |
re r |
re4\p re16 mi fad sol |
la8 la la re' |
dod'-! re'-! dod'-! re'-! |
la8. sol16 fad4 |
dod-\sug\p r |
re r |
dod r |
re r |
re4\ff re16 mi fad sol |
la8 la la re' |
dod'-! re'-! dod'-! re'-! |
la8. sol16 fad4 |
dod\p r |
re r |
dod r |
re r |
si,8 r si, r |
si, r si, r |
si, r si, r |
si, r si, r |
r4 si\f |
r lad |
r si |
fad r |
re8\ff fad la-\sug\p re' |
dod'( re') re'4 |
re8\f fad la\p re' |
dod'( re') re'4 |
R2*4 |
re8\f fad la\p re' |
dod'( re') re'4 |
re8\f fad la\p re' |
dod' re' re'4 |
R2*4 |
si,8-\sug\p r si, r |
si, r si, r |
si, r si, r |
si, r si, r |
r4 si |
r lad |
r si |
fad4 r |
si,8 r si, r |
si, r si, r |
dod r red r |
mi r mi r |
re r lad, r |
si, r mi r |
fad r fad, r |
si,4 r |
re4\ff re16 mi fad sol |
la8 la la re' |
dod'-! re'-! dod'-! re'-! |
la8. sol16 fad4 |
dod\p r |
re r |
dod r |
re r |
re\p re16 mi fad sol |
la8 la la re' |
dod' re' dod' re' |
la8. sol16 fad4 |
dod r |
re r |
dod r |
re2:8 |
re:8\p |
re:8\cresc |
re:8 |
re:8 |
re:8 |
re:8 |
re:8\f |
re4\ff r |
fad8 sol fad sol |
fad8 sol fad sol |
fad8 sol fad sol |
fad16 sol fad mi re4 fad8 sol fad sol |
fad sol fad sol |
fad sol fad sol |
fad16 sol fad mi re4 |
fad2:8 |
sol:8 |
la4 la, |
re2:8 |
fad:8 |
sol:8 |
la4 la, |
re la, |
re la, |
re la, |
re la, |
re4. re8 |
re4 re |
re r |
re r |
re r |
re r |
re2 |
re |
re |

