\clef "treble" re'4\p re'16 mi' fad' sol' |
la'8 la' la' re'' |
dod''-! re''-! dod''-! re''-! |
la'16 si' la' sol' fad'4 |
<<
  \tag #'violino1 {
    mi'4\p r |
    fad' r |
    mi' r |
    fad' r |
  }
  \tag #'violino2 {
    la4-\sug\p r |
    la r |
    la r |
    la r |
  }
>>
re'4\ff re'16 mi' fad' sol' |
la'8 la' la' re'' |
dod''8 re'' dod'' re'' |
la'16 si' la' sol' fad'4 |
<<
  \tag #'violino1 {
    mi'4\p r |
    fad' r |
    mi' r |
    fad' r |
  }
  \tag #'violino2 {
    la4-\sug\p r |
    la r |
    la r |
    la r |
  }
>>
%%
<re'' la''>4.(\fp re'''8) |
<<
  \tag #'violino1 {
    dod'''8 si'' si'' si'' |
    si'' si'' re''' si'' |
    \grace dod''' si'' la'' la''4 |
  }
  \tag #'violino2 {
    re''2:8 |
    re''8 re''4 re''8 |
    re'' re'' re''4 |
  }
>>
<re' la' fad''>4.\f <<
  \tag #'violino1 {
    la''8\p |
    \grace si'' la''( sol'') \grace la'' sol''( fad'') |
    \grace sol'' fad''( mi'') \grace fad'' mi''( re'') |
    dod''4 la' |
  }
  \tag #'violino2 {
    fad''8-\sug\p |
    mi''4 \grace fad''8 mi''( re'') |
    si'4 si' |
    mi' dod' |
  }
>>
<re'' la''>4.(\fp re'''8) |
<<
  \tag #'violino1 {
    dod'''8( si'') si'' si'' |
    si'' si'' re''' si'' |
    si'' la'' la''4 |
  }
  \tag #'violino2 {
    re''2:8 |
    re'':8 |
    re''8 re'' re''4 |
  }
>>
<re' la' fad''>4.\fp <<
  \tag #'violino1 {
    la''8 |
    la''-! si''-! sol''-! la''-! |
    fad''-! sol''-! mi''-! fad''-! |
    re''4 la' |
  }
  \tag #'violino2 {
    fad''8 |
    fad''-! sol''-! mi''-! fad''-! |
    re''-! mi''-! dod''-! re''-! |
    fad'4 r |
  }
>>
%%
re'4\p re'16 mi' fad' sol' |
la'8 la' la' re'' |
dod''-! re''-! dod''-! re''-! |
la'16 si' la' sol' fad'4 |
<<
  \tag #'violino1 {
    mi'4\p r |
    fad' r |
    mi' r |
    fad' r |
  }
  \tag #'violino2 {
    la4-\sug\p r |
    la r |
    la r |
    la r |
  }
>>
re'4\ff re'16 mi' fad' sol' |
la'8 la' la' re'' |
dod''8-! re''-! dod''-! re''-! |
la'16 si' la' sol' fad'4 |
<<
  \tag #'violino1 {
    mi'4\p r |
    fad' r |
    mi' r |
    fad' r |
  }
  \tag #'violino2 {
    la4-\sug\p r |
    la r |
    la r |
    la r |
  }
>>
%%
<<
  \tag #'violino1 {
    si'8 r re'' r |
    fad'' r si' r |
    sol'' r si' r |
    fad'' r si' r |
    fad''4.-\sug\f( sol''8) |
    mi''4.( fad''8) |
    re''4. mi''8 |
    dod''4 fad' |
  }
  \tag #'violino2 {
    <re' fad'>8 r q r |
    q r q r |
    <si sol'>8 r q r |
    <si fad'>8 r q r |
    r4 re'-\sug\f |
    r dod' |
    r4 si |
    lad r |
  }
>>
%%
<la fad' re''>4.\f <<
  \tag #'violino1 {
    re''8\p |
    \grace fad'' mi'' re'' re''4 |
  }
  \tag #'violino2 {
    fad'8-\sug\p |
    \grace la' sol' fad' fad'4 |
  }
>>
<re' la' fad''>4.\f <<
  \tag #'violino1 {
    fad''8\p |
    \grace la'' sol'' fad'' fad''4 |
    <la' la''>4.\f fad''16 re'' |
    la'4\p la' |
    la'8 si' dod'' la' |
    re''4 re' |
  }
  \tag #'violino2 {
    la'8-\sug\p |
    \grace fad'' mi'' re'' re''4 |
    r fad'\f |
    sol'\p fad' |
    mi' sol' |
    fad' r |
  }
>>
<la fad' re''>4.\f <<
  \tag #'violino1 {
    re''8\p |
    \grace fad'' mi'' re'' re''4 |
  }
  \tag #'violino2 {
    fad'8-\sug\p |
    \grace la' sol' fad' fad'4 |
  }
>>
<re' la' fad''>4.\f <<
  \tag #'violino1 {
    fad''8\p |
    \grace la'' sol'' fad'' fad''4 |
    <la' la''>4.\f fad''16 re'' |
    la'4\p la' |
    la'8 si' dod'' la' |
    re''4
  }
  \tag #'violino2 {
    la'8-\sug\p |
    \grace fad'' mi'' re'' re''4 |
    r fad'\f |
    sol'\p fad' |
    mi' sol' |
    fad'
  }
>> r4 |
%%
<<
  \tag #'violino1 {
    si'8-\sug\p r re'' r |
    fad'' r si' r |
    sol'' r si' r |
    fad'' r si' r |
    fad''4.( sol''8) |
    mi''4.( fad''8) |
    re''4. mi''8 |
    dod''4 fad' |
  }
  \tag #'violino2 {
    <re' fad'>8-\sug\p r q r |
    q r q r |
    <si sol'>8 r q r |
    <si fad'>8 r q r |
    r4 re' |
    r dod' |
    r4 si |
    lad r |
  }
>>
<<
  \tag #'violino1 {
    si'8 r re'' r |
    fad'' r si'' r |
    lad'' r la'' r |
    sold'' r sol'' r |
    fad'' r mi'' r |
    re'' r dod'' r |
    si' r lad' r |
    si'4 r |
  }
  \tag #'violino2 {
    <re' fad'>8 r q r |
    q r q r |
    mi' r <si fad'> r |
    <si mi'> r lad' r |
    si' r dod'' r |
    si' r sol' r |
    fad' r fad' r |
    fad'4 r |
  }
>>
%%
re'4\ff re'16 mi' fad' sol' |
la'8 la' la' re'' |
dod''-! re''-! dod''-! re''-! |
la'16 si' la' sol' fad'4 |
<<
  \tag #'violino1 {
    mi'4\p r |
    fad' r |
    mi' r |
    fad' r |
  }
  \tag #'violino2 {
    la4-\sug\p r |
    la r |
    la r |
    la r |
  }
>>
re'4\p re'16 mi' fad' sol' |
la'8 la' la' re'' |
dod''8 re'' dod'' re'' |
la'16 si' la' sol' fad'4 |
<<
  \tag #'violino1 {
    mi'4 r |
    fad' r |
    mi' r |
    fad' r |
    re''8-\sug\p fad'' fad'' fad'' |
    sol''\cresc mi'' mi'' mi'' |
    mi'' sol'' sol'' sol'' |
    la'' fad'' fad'' fad'' |
    fad'' la'' la'' la'' |
    re'' si'' si'' si'' |
    mi'' <mi'' dod'''> q q |
  }
  \tag #'violino2 {
    la4 r |
    la r |
    la r |
    la r |
    fad'8-\sug\p re'' re'' re'' |
    mi''-\sug\cresc dod'' dod'' dod'' |
    dod'' mi'' mi'' mi'' |
    fad'' re'' re'' re'' |
    re'' fad'' fad'' fad'' |
    re'' sol'' sol'' sol'' |
    dod'' <la' sol''> q q |
  }
>>
%%
<<
  \tag #'violino1 {
    <>\ff \ru#9 <re'' re'''>2:16 |
  }
  \tag #'violino2 {
    <la' fad''>4-\sug\ff r |
    <re'' la''>8 <re'' si''> <re'' la''>8 <re'' si''> |
    <re'' la''>8 <re'' si''> <re'' la''>8 <re'' si''> |
    <re'' la''>8 <re'' si''> <re'' la''>8 <re'' si''> |
    la''16 si'' la'' sol'' fad''4 |
    <re'' la''>8 <re'' si''> <re'' la''>8 <re'' si''> |
    <re'' la''>8 <re'' si''> <re'' la''>8 <re'' si''> |
    <re'' la''>8 <re'' si''> <re'' la''>8 <re'' si''> |
    la''16 si'' la'' sol'' fad''4 |
  }
>>
re'''16 dod''' si'' la'' sol'' fad'' mi'' re'' |
si'' la'' sol'' fad'' mi'' re'' dod'' si' |
la'4 <la' mi'' dod'''>4 |
%%
<la' fad'' re'''>8 la''[ la'' la''] |
re'''16 dod''' si'' la'' sol'' fad'' mi'' re'' |
si'' la'' sol'' fad'' mi'' re'' dod'' si' |
la'4 <la' mi'' dod'''>4 |
<<
  \tag #'violino1 {
    <la' fad'' re'''>4 \grace re'''8 dod''' si''16 dod''' |
    re'''4 \grace re'''8 dod''' si''16 dod''' |
    re'''4 \grace re'''8 dod''' si''16 dod''' |
    re'''4 \grace re'''8 dod''' si''16 dod''' |
    re'''4.
  }
  \tag #'violino2 {
    \ru#4 { <re' la' fad''>4 <la' dod'' mi''> } |
    <re' la' fad''>4.
  }
>> re'8 |
re'4 re' |
<re' la' fad''>4 r |
<re' la' la''>4 r |
<re' la' fad''>4 r |
<re' la' la''>4 r |
<< re'2 \\ re' >> |
<< re'2 \\ re' >> |
<< re'2 \\ re' >> |
