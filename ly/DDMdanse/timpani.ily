\clef "bass" R2*88 |
re4 r |
la, r8 re |
la, re la, re |
la,4 r |
R2*12 |
<< { \ru#7 re2:32 } { s2-\sug\p s2-\sug\cresc } >> |
re4-\sug\ff r |
R2*3 |
re4:16 re8 r |
R2*3 |
re4:16 re8 r |
re2:32 |
re2:32 |
la,4 la, |
re r |
re2:32 |
re2:32 |
la,4 la, |
re la, |
re la, |
re la, |
re la, |
re r |
re re |
re r |
re r |
re r |
re r |
re2 |
re |
re |

