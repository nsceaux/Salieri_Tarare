\tag #'tarare { - e. }

\tag #'(recit basse) {
  Non, par mes mains, le peuple en -- tier
  te fait son no -- ble pri -- son -- nier :
  il veut que de l’é -- tat tu sai -- sis -- ses les rê -- nes.
  Si tu re -- je -- tais no -- tre foi,
  nous a -- bu -- se -- rions de tes chaî -- nes
  pour te cou -- ron -- ner mal -- gré toi.
  
  Pon -- tife, à ce grand homme, A -- tar lè -- gue l’A -- si -- e ;
  con -- sa -- crez le seul bien qu’il ait fait de sa vi -- e :
  pre -- nez le di -- a -- dème, et ré -- pa -- rez l’af -- front
  que le ban -- deau des rois a re -- çu de son front.
}
