\clef "treble"
<<
  \tag #'violino1 { re''2\f }
  \tag #'violino2 { fad'2-\sug\f }
>> r2 |
R1 |
r2 <re' si'>4\mf r |
R1 |
r2 r4 q\f |
<<
  \tag #'violino1 { <re' re'' sol''>4 }
  \tag #'violino2 { <re' si' sol''>4 }
>> r4 r2 |
r8. <sol'' do''>16 q4 r2 |
r2 <do'' la''>4 r |
r2 r4 <sol' re'' si''> |
<sol' mi'' do'''> r r2 |
<<
  \tag #'violino1 {
    mi''4 r r2 |
    do''4 r r2 |
    dod''4 r r2 |
    re''4 r r2 |
    fa''4 r r2 |
    sol''4 r r2 |
    la''4 r r \custosNote mib'8
  }
  \tag #'violino2 {
    si'4 r r2 |
    mi'4 r r2 |
    mi'4 r r2 |
    la'4 r r2 |
    sib'4 r r2 |
    sib'4 r r2 |
    do''4 r r \custosNote la8
  }
>>
