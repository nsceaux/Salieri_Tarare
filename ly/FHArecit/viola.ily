\clef "alto" re'2-\sug\f r |
R1 |
r2 <re' si'>4-\sug\mf r |
R1 |
r2 r4 sol'-\sug\f |
fa'! r r2 |
r8. mi'16 mi'4 r2 |
r fa'4 r |
r2 r4 sol' |
do' r r2 |
mi'4 r r2 |
la4 r r2 |
la'4 r r2 |
re'4 r r2 |
re'4 r r2 |
mib'4 r r2 |
mib'4 r r \custosNote fad8
