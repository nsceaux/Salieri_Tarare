\clef "bass" re2\f r |
R1 |
r2 sol4\mf r |
R1 |
r2 r4 sol\f |
fa! r r2 |
r8. mi16 mi4 r2 |
r fa4 r |
r2 r4 sol |
do r r2 |
sold4 r r2 |
la4 r r2 |
sol!4 r r2 |
fa4 r r2 |
re4 r r2 |
mib4 r r2 |
<>^\markup\center-align\tiny { [Source : \concat { \italic fa♭ ] } }
mib4 r r \custosNote fad8
