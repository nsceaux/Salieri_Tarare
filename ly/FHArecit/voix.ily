<<
  \tag #'tarare {
    \clef "tenor/G_8" re' r r2 |
    R1*15 |
  }
  \tag #'(recit basse) {
    \clef "bass" \tag #'recit <>^\markup { Parlé. \italic Prenant sa chaîne par le milieu }
    re'4 r8 la16 la fad8. fad16 fad8 sol |
    la4. la8 do'4. mi'8 |
    do'8 do' do' re' si4 r8 sol |
    sol sol sol la si4 si8 si |
    si4 do'8 re' sol sol r4 |
    r8 sol sol sol16 la si4 si8 sol |
    do'4 r do'8 do' do' re' |
    sib4 sib8 do' la la r4 |
    do'16 do' fa' re' si!8 si16 do' sol4 r |
    r4 r8-\tag #'recit ^\markup\italic { au grand Prêtre } sol do'8. do'16 re'8 mi' |
    si4. si8 re'4 re'8 si16 do' |
    la8 la r la16 la la4 la8 si |
    dod'4 mi'8 mi' dod'4 dod'8 re' |
    la la r la re'4 re'8 re' |
    sib8 sib r sib sib sib re' sib |
    mib'4 r sib8 sib16 sib mib'8 do' |
    la8 fa16 sol la8 la16 sib fa4
  }
>>
