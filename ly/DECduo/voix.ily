<<
  \tag #'(recit1 basse) {
    \clef "tenor/G_8" <>^\markup\character Tarare _\markup\center-align "Chanté"
    r2 r8 do' do' do' |
    la4 fa'2 mi'8 re' |
    do'2 r8 do' do' re' |
    sib2 sib4 sib8 sib |
    mi'2 sol'4. mi'8 |
    mi'4. fa'8 sol'4 do'8 sib |
    la4 la r r8 la |
    la la la la dod'4. dod'8 |
    re'2 r4 r8 re' |
    fa'4 re'8 do' si4 mi'8 re' |
    do'4 do' r8 la' la' la' |
    si4. fa'8 mi'4. sold8 |
    la2 r8 do' re' mi' |
    fa'4. fa'8 fa' do' sib la |
    sol4 sol
    \ffclef "alto/G_8" <>^\markup\character Calpigi
    r8 do' do' do' |
    la4. fa'8 fa'4. re'8 |
    do'2^\markup\center-align\tiny\concat { [Source :  \italic mi ] } r8 do' re' mib' |
    re'2 re'4 re'8 do' |
    sib4 sib8 sib sib do' re' mib' |
    do' do' do' do' fa'4. fa'8 |
    re'2 re'8 mi'! fa' re' |
    do'2 do'4. fa'8 |
    mi'8\fermata mi'4 mi'8 fa' sol' la' fa' |
    re'4 fa'8 re' do'4 do'8 do' |
    fa4 <>^\markup { \italic Récit Parlé } r16 do' do' do' fa'8 do'16 do' do'8 do'16 do' la8 la r4 r2 |
    r4 r8 la16 la re'4 re'16 re' fa' re' |
    dod'4. la8 dod'8. mi'16 dod' dod' dod' re' |
    la4 \tag #'recit1 <>^\markup\italic { (Il prend un paquet dans une touffe d’arbres.) } r4 r2 |
    R1*2 |
    r8 sol sol lab sib sib sib sib |
    sol sol r8 sib mib' sib16 do' reb'8 reb'16 mib' |
    do'8 do' r4 r2 |
    R1*3 |
    r2 lab4 lab8 sib |
    do'4. mib'8 do' do' reb' mib' |
    do'4 \tag #'recit1 <>^\markup\italic { (Il l’habille en muet.) } r4 r2 |
    R1*3 |
    r8 do' do' do' fa'4. do'8 |
    do' do' do' re' si! si r16 si si do' |
    \tag #'recit1 <>^\markup\italic { (Il lui met un masque noir.) }
    re'4. sol8 re' re' re' mib' |
    do'4 r16 do' do' si! do'8 do' re' mib' |
    re' re' r re' sib4 sol'8 sol' |
    dod'4 dod'8 re' la la r4 |
    \tag #'recit1 <>^\markup\italic { (Ils s’avancent vers l’appartement d’Astasie.) }
    R1*3 |
    r2 <>^\markup\character-text Calp. l’arrête et recule r8 reb' reb' reb' |
    sib4 r8 sib16 do' reb'4 reb'8 mib' |
    dob'8 dob' r16 dob' dob' dob' fa'8 fa' fa' solb' |
    reb'4 r
    \ffclef "tenor/G_8" <>^\markup\character-text Tarare égaré, criant
    r8 lad lad si |
    dod'8 dod' r4 r2 |
    r dod'8 dod'16 dod' dod'8 fad' |
    fad' dod' r4 fad'16 dod' dod' red' mi'8 mi'16 fad' |
    red'4 r8 si red'4. si8 |
  }
  \tag #'recit2 { \clef "alto/G_8" R1*60 }
>>
<<
  \tag #'(recit1 basse) {
    <<
      \tag #'basse { fad'4 s2. | s4 \ffclef "tenor/G_8" }
      \tag #'recit1 { fad'1 | r4 }
    >>
    <>^\markup\character-text Tar. criant plus fort r8 si mi'4. si8 |
    <<
      \tag #'basse { sold'4 }
      \tag #'recit1 {
        sold'1 |
        <>^\markup\italic { il tombe sur le sein de Calpigi. }
        R1*2 |
      }
    >>
  }
  \tag #'(recit2 basse) {
    <<
      \tag #'basse { s4 \ffclef "alto/G_8" }
      \tag #'recit2 { r4 }
    >>
    <>^\markup\character-text Calpigi lui fermant la bouche r8 fad' red' red' red' mi' |
    si8 si <<
      \tag #'basse { s2. | s4 \ffclef "alto/G_8" <>^\markup\character Calpigi }
      \tag #'recit2 { r4 r2 | r4 }
    >> r8 si16 mi' mi'4 si8 si |
    sold sold r4 r2 |
    R1 |
  }
>>
