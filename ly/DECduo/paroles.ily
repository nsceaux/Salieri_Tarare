\tag #'(recit1 basse) {
  Je suis sau -- vé, grâce à ton cœur ;
  et pour pay -- er tant de fa -- veur,
  ô dou -- leur ! ô crime ex -- é -- cra -- ble !
  Trom -- pé par une a -- veugle er -- reur,
  j’al -- lais, d’u -- ne main mi -- sé -- ra -- ble,
  as -- sas -- si -- ner mon bien -- fai -- teur !
  Par -- donne, a -- mi, ce crime in -- vo -- lon -- tai -- re.
  
  Ô mon hé -- ros ! que me dois- tu ?
  Sans force, hé -- las ! sans ca -- rac -- tè -- re,
  le fai -- ble Cal -- pi -- gi, de tous les vents bat -- tu,
  se -- rait moins que rien sur la ter -- re,
  s’il n’é -- tait pas é -- pris de ta mâ -- le ver -- tu !
  Ne per -- dons point un ins -- tant sa -- lu -- tai -- re :
  au sé -- rail, la tran -- quil -- li -- té
  re -- naît a -- vec l’obs -- cu -- ri -- té.
  
  Sous cet ha -- bit d’un noir es -- cla -- ve,
  ca -- chons des guer -- riers le plus bra -- ve.
  D’homme é -- lo -- quent, de -- viens un vil mu -- et.
  
  Que mon hé -- ros, sur -- tout, ja -- mais n’ou -- bli -- e
  que sous ce masque, un mot est un for -- fait ;
  et qu’en ce lieu de ja -- lou -- si -- e,
  le moindre est pay -- é de la vi -- e.

  N’a -- van -- çons pas ! j’a -- per -- çois la si -- mar -- re,
  les bro -- de -- quins de l’em -- pe -- reur.
  
  A -- tar chez el -- le ! Ah ! mal -- heu -- reux Ta -- ra -- re !
  Rien ne re -- tien -- dra ma fu -- reur :
  Bra -- ma ! Bra -- ma !
}
\tag #'(recit2 basse) {
  Ren -- fer -- me donc ta pei -- ne !
}
\tag #'(recit1 basse) {
  Bra -- ma ! Bra -- ma !
}
\tag #'(recit2 basse) {
  No -- tre mort et cer -- tai -- ne.
}
