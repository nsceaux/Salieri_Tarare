\clef "treble" R1*2 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { r8 do''-\sug\p( fa'' la'') do'''( la'' do''' re''') | }
  { R1 | }
>>
r8 <>-\sug\sf \twoVoices #'(flauto1 flauto2 flauti) <<
  { \once\slurDashed sib''( re''' do''' sib'' la'' sol'' fa'') |
    mi''2 sol''4. fa''8 |
    \once\slurDashed fa''8( mi'' mi'' fa'' sol'' mi'' do''' sib'') |
    la''2 }
  { \once\slurDashed re''8( sib'' la'' sol'' fa'' mi'' re'') |
    do''2 sol'4. la'8 |
    sib'2~ \once\slurDashed sib'8( sol' mi' sol') |
    fa'2 }
  { s2.. | s2 s-\sug\sf | s-\sug\p }
>> r2 |
r4 r8 \twoVoices #'(flauto1 flauto2 flauti) <<
  { la''8 dod'''2 | re'''4 }
  { la''8 sol'' sib'' la'' sol'' | fa''4 }
>> r4 r2 |
R1*4 |
r2 r8 \twoVoices #'(flauto1 flauto2 flauti) <<
  { do'''8 sib'' la'' | la'' sol'' sol''4 }
  { la''8 sol'' fa'' | fa'' mi'' mi''4 }
>> r2 |
R1 |
<>-\sug\mf \twoVoices #'(flauto1 flauto2 flauti) <<
  { do'''1 |
    re'''~ |
    re'''2 mib'''~ |
    mib'''4. mi'''8 fa'''2 |
    re''' }
  { la''1 |
    la'' |
    sib''2 sib'' |
    do''' do''' |
    sib'' }
>> r2 |
<<
  \tag #'(flauto1 flauti) {
    <>^"Solo" r8 fa''-\sug\sf( la'' do''') sib''( la'' sol'' fa'') |
    mi''!4.\fermata r8 r2 |
  }
  \tag #'flauto2 { R1*2 }
>>
r8 <>-\sug\mf \twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''8 fa''' re''' do''' la'' sol''4 |
    \sug fa''4 }
  { sib''8 re''' sib'' la'' fa'' mi''4 |
    \sug fa''4 }
>> r4 r2 |
R1*40 |
