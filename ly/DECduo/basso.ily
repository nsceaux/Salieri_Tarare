\clef "bass" do2~ do8. do16\p re8. mi16 |
fa1~ |
fa |
sol |
do~ |
do-\sug\p |
fa8\f fa, la, do fa la-\sug\p do' la |
fa2 mi |
<< re1~ { s2-\sug\f s8 s-\sug\p } >> |
re2 sold, |
la,8 la,[\f do mi] la\mf mi do la, |
re4 re mi mi, |
la,2 r |
R1 |
r2 r8 do( re mi) |
fa1~ |
<<
  \new Voice {
    \voiceOne fa4_\sug\mf fa fa fa |
    fad fad fad fad |
    sol sol sol sol |
    la la la la |
  }
  { \voiceTwo fa1 |
    fad |
    sol |
    la \oneVoice | }
>>
sib4 sib, sib8 do' re' sib |
la1 |
sol4.\fermata sol8-\sug\p fa4 fa, |
sib, sib,\mf do do |
fa4 r r2 |
r fa4\mf mi! |
re r r2 |
la,1 |
re4 r r2 |
R1 |
sib,1-\sug\mf |
mib4 r r2 |
R1 |
lab,4-\sug\mf do mib lab |
lab,1 |
mib4 sol sib sol |
mib1 |
lab,4 r r2 |
R1 |
lab,4 do mib lab |
lab,1~ |
lab,4 lab( sol fa) |
mi!1 |
fa4 r r2 |
r fa4 r |
R1 |
mib4 r r2 |
fad4 r sol r |
r2 r4 la, |
sib, sib(\p la sol) |
fa( mib re do) |
sib, sol( fa mib) |
re reb2.\f~ |
reb1 |
reb4 r r2 |
r4 fad\f r2 |
r fad4\f fad |
fad r r2 |
R1 |
si,2:16\fp si,:16 |
si,:16 si,:16 |
mi:16 mi:16 |
mi:16 mi:16 |
mi:16\f mi:16 |
mi:16 mi:16 |
