\clef "bass" R1 |
r4 re'2-\sug\sf( do'8 sib) |
la1 |
sib |
sib2~ sib4. la8 |
sol2-\sug\p mi |
fa8-\sug\f fa, la, do fa la-\sug\p do' la |
fa2 mi |
re1 |
R1*3 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la2 sib! | la~ la8 la sib si | }
  { la2 sol | fa~ fa8 la sib si | }
  { s2 s-\sug\sf }
>>
do'1~ |
do'4.( re'8\mf) re'4.( sib8) |
la4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'2. |
    re'1 |
    re'2( mib') |
    mib'4. mi'8 fa'2 |
    re' }
  { la2. |
    la1 |
    sib2 sib |
    do' do' |
    sib }
>> r2 |
R1*2 |
\clef "tenor" r8 <>-\sug\mf \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'8 fa' re' do' la sol4 | }
  { sib8 re' sib la fa mi4 | }
>>
fa4 r r2 |
R1*2 |
\clef "bass" la,1 |
re4 r r2 |
r4 <>\mf \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa2 sol4 |
    lab sol fa lab |
    sol }
  { re2 mib4 |
    fa mib re fa |
    mib }
>> r4 r2 |
R1*33 |
