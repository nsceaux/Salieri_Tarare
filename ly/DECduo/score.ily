\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with { \flautiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'flauti \includeNotes "flauti"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'recit2 \includeNotes "voix"
    >> \keepWithTag #'recit2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit1 \includeNotes "voix"
    >> \keepWithTag #'recit1 \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*5\pageBreak
        s1*5\break s1*5\pageBreak
        s1*5\break s1*4\pageBreak
        s1*3\break s1*5\break s1*4\pageBreak
        s1*5\break s1*4\break s1*3 s2 \bar "" \pageBreak
        s2 s1*5\break s1*4\break s1*3\pageBreak
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
