\key fa \major \midiTempo#144
\time 2/2 s1*24
\key do \major \time 4/4 \midiTempo#100 s1 \tempo "Allegro" s1*3
\tempo "Andante" s1*5
\tempo "Allegretto" s1*6
\tempo "Allegretto" s1*10
\tempo "un poco lento" s1*14
\tempo "Allegro assai" s1*2
