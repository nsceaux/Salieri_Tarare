\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (flauti #:tag-notes flauti #:score-template "score-part-voix")
   (fagotti #:tag-notes fagotti #:score-template "score-part-voix"
            #:music , #{
\quoteBasso "DECbasso"
s1*33
<>_\markup\tiny Basso \cue "DECbasso" { \mmRestDown s1*32 }
            #})
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
