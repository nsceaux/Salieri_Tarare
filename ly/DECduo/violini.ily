\clef "treble" <sol mi' do''>2~ do''8. <<
  \tag #'violino1 {
    do''16\p do''8. do''16 |
    la'8 r fa''2(\sf mi''8 re'') |
    do'' do'([\p fa' la']) do''( la' do'' re'') |
    sib' sib'([\sf re'' do''] sib' la' sol' fa') |
    mi'2 sol''4.\sf fa''8 |
    fa''(\p mi'' mi'' fa'' sol'' mi'' do'' sib') |
    la'2~\f~ la'8 la'\p la' la' |
    la' la''(\sf sol''16 fa'' mi'' re'') dod''2 |
    re''8 re'[\f fa' la'] re'' fa'\p la' re'' |
    fa''2. mi''8 re'' |
    do''8
  }
  \tag #'violino2 {
    mi'16-\sug\p fa'8. sol'16 |
    fa'1~ |
    fa' |
    r8 re'(-\sug\sf sib' la' sol' fa' mi' re') |
    do'2 sol'4.-\sug\sf la'8 |
    sib'2-\sug\p~ sib'8( sol' mi' sol') |
    << do'1~ { s2-\sug\f s8 s-\sug\p } >> |
    do'4. la'8 sol'( sib' la' sol') |
    << <fa' la>1~ { s2-\sug\f s8 s-\sug\p } >> |
    fa'8 fa''([ re'' do'']) si'2 |
    do''8
  }
>> la-\sug\f[ do' mi'] la'\mf do'' mi'' la'' |
<<
  \tag #'violino1 {
    si'4. fa''8 mi''4. sold'8 |
    la'4 do''4. do''8 re'' mi'' |
    fa''( do'' la' do'') fa''( do'' sib' la') |
    la' sol' sol'4. do''8 do'' sib' |
    la'4.( fa''8\mf) fa''4.( re''8) |
    do'' do'' r do'' r do'' r mib'' |
    r re'' r re'' r re'' r re'' |
    r sib' r sib' r mib'' r mib'' |
    r mib'' r mi'' r fa'' r fa'' |
    r re'' r re'' re''( mi''! fa'' re'') |
    do'' fa''([\sf la'' do'''] sib'' la'' sol'' fa'') |
    mi''4.\fermata mi''8\p fa''( sol'' la'' fa'') |
    re''4 fa''8.(\mf re''16) do''8 la'-! sol'-! do''-! |
    fa''4
  }
  \tag #'violino2 {
    fa'4. re''8( do''4 si') |
    la'2( sib'!)\sf |
    la'2~ la'8 la'( sol' fa') |
    fa' mi' mi'4. mi'8( fa' sol') |
    fa'1-\sug\mf |
    fa'8 la' r la' r la' r la' |
    r la' r la' r la' r la' |
    r re' r re' r sib' r sib' |
    r do'' r do'' r do'' r do'' |
    r sib' r fa' fa'2 |
    fa'8 fa'-\sug\sf([ la' do''] sib' la' sol' fa') |
    sib'4.\fermata sib'8-\sug\p la'( sib' do'' la') |
    sib'4 re''8.-\sug\mf( sib'16) la'8 fa'-! mi'!-! mi'-! |
    fa'4
  }
>> r4 r2 |
<<
  \tag #'violino1 {
    r2 la'4-\sug\mf dod'' |
    re'' r r2 |
    dod''1 |
    re''8 r fa'4( sol' la') |
    sib'1~ |
    sib'~ |
    sib'4
  }
  \tag #'violino2 {
    r2 la'4-\sug\mf << { la' | la' } \\ { sol' | fa' } >> r r2 |
    <la' sol'>1 |
    << la'4 \\ fa' >> r4 r2 |
    r4 fa'2-\sug\mf( sol'4) |
    lab'( sol' fa' lab') |
    sol'
  }
>> r4 r2 |
R1 |
<<
  \tag #'violino1 {
    <mib' do''>4-\sug\mf r r2 |
    r8 \grace sib' lab'?16\mf sol' lab'8-! sib'-! do''-! reb''-! mib''-! mib''-! |
    mib''1~ |
    mib''8 mib'16( fa' sol'8) lab'-! sib'-! do''-! reb''-! mib''-! |
    do''4
  }
  \tag #'violino2 {
    lab'8\mf mib'4 mib' mib' mib'8 |
    mib'1~ |
    mib'8 sib?4 sib sib sib8 |
    sib1 |
    do'4
  }
>> r4 r2 |
R1 |
<<
  \tag #'violino1 {
    <do' mib'>4 r r2 |
    r8 \grace reb''8 do''16( sib' do''8) reb''-! mib''-! do''-! lab'-! do''-! |
    do'1~ |
    do'8 do'16( re'! mi'!8) fa'-! sol'-! lab'-! sib'-! do''-! |
    lab'4
  }
  \tag #'violino2 {
    lab'8 mib'4 mib' mib' mib'8 |
    mib'1~ |
    mib'4. do''4 sib' lab'8 |
    sol'1 |
    do'4
  }
>> r4 r2 |
r2 <<
  \tag #'violino1 { si'!4 }
  \tag #'violino2 { sol' }
>> r4 |
R1 |
<<
  \tag #'violino1 { do''4 }
  \tag #'violino2 { sol' }
>> r4 r2 |
<<
  \tag #'violino1 { re''4 r sib' r | }
  \tag #'violino2 { la'4 r re' r | }
>>
r2 r4 la4 |
sib re''(\p do'' sib') |
la'( sol' fa' mib') |
re' sib'( la' sol') |
fa' <<
  \tag #'violino1 { sib'2.-\sug\f~ | sib'1 | dob''4 }
  \tag #'violino2 { fa'2.-\sug\f~ | fa'1 | lab'4 }
>> r4 r2 |
r4 <<
  \tag #'violino1 { <fad'? lad' fad''>4-\sug\f }
  \tag #'violino2 { <dod' lad'>-\sug\f }
>> r2 |
r4 r8. <>-\sug\f <<
  \tag #'violino1 {
    fad''16 fad''8. lad''16 lad''8. dod'''16 |
    dod'''4
  }
  \tag #'violino2 {
    <dod' lad'>16 q8. dod''16 dod''8. lad'16 |
    lad'4
  }
>> r4 r2 |
R1 |
<fad' red''>2:16-\sug\fp q:16 |
q:16 q:16 |
<<
  \tag #'violino1 {
    <sold' mi''>2:16 q:16 |
    q:16 q:16 |
    mi''16-\sug\f si'( dod'' red'' mi'' fad'' sold'' la'') si''( la'' sold'' la'') si''( la'' sold'' la'') |
    si'' la'' sold'' la'' si'' la'' sold'' la'' si'' la'' sold'' la'' si'' la'' sold'' la'' |
  }
  \tag #'violino2 {
    <sold' mi''>16 <sold' si'> q q q4:16 q2:16 |
    q:16 q:16 |
    \slurDashed sold'8-\sug\f la'16( si' dod'' red'' mi'' fad'') sold''( fad'' mi'' fad'') sold''( fad'' mi'' fad'') |
    sold'' fad'' mi'' fad'' sold'' fad'' mi'' fad'' sold'' fad'' mi'' fad'' sold'' fad'' mi'' fad'' |
  }
>>
