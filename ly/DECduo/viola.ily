\clef "alto" do'2~ do'8. do16-\sug\p re8. mi16 |
fa8 r re'2(\sf do'8 sib) |
la1 |
sib |
sib2 sib'4.-\sug\sf la'8 |
sol'2-\sug\p~ sol' |
fa'8-\sug\f fa la do' fa' la'-\sug\p do'' la' |
fa'2 mi' |
<< re'1~ { s2-\sug\f s8 s-\sug\p } >> |
re'2. si8 mi' |
mi' la-\sug\f[ do' mi'] la'-\sug\mf do''-! mi'-! la'-! |
re4 re' mi' mi |
la2( sol-\sug\sf) |
fa2~ fa8 la sib si |
do'2~ do'8 do' re' mi' |
fa'4.( re'8-\sug\mf) re'4.( sib8) |
la8 r fa r fa r fa r |
fad r fad r fad r fad r |
sol r sol r sol r sol r |
la r la r la r la r |
sib r sib r sib8( do' re' sib) |
la do'4-\sug\sf do'8 do'2 |
sol'4.\fermata sol'8-\sug\p fa'4 fa |
sib sib-\sug\mf do' do |
fa4 r r2 |
r2 \sugNotes { fa4-\sug\mf mi! | re r r2 } |
mi'1 |
re'4 r r2 |
r4 re'2-\sug\mf( mib'4) |
fa'( mib' re' fa') |
mib' r r2 |
R1 |
do'8-\sug\mf do'4 do' do' do'8 |
do'1 |
sol8 sol4 sol sol sol8 |
sol1 |
lab4 r r2 |
R1 |
do'8 do'4 do' do' do'8 |
do'1~ |
do'8 do'4 do' mi'! fa'8 |
do'1 |
do'4 r r2 |
r2 re'!4 r |
R1 |
do'4 r r2 |
fad4 r sol r |
r2 r4 la |
sib re'-\sug\p( do' sib) |
la( sol fa mib) |
re sib( la sol) |
fa reb2.-\sug\f~ |
reb1 |
reb4 r r2 |
r4 fad-\sug\f r2 |
r fad'4-\sug\f fad' |
fad' r r2 |
R1 | \allowPageTurn
si2:16\fp si:16 |
si:16 si:16 |
mi':16 mi':16 |
mi':16 mi':16 |
mi':16\f mi':16 |
mi':16 mi':16 |
