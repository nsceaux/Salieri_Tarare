\clef "alto" r4 r2 |
R1 |
re'4 r r2 |
sol'4 r r2 |
r r4 sol'8 fa' |
mi'4 r r2 |
r2 la'4 r |
r2 la'4 r |
r2 r4 si' |
mi'2 r4
