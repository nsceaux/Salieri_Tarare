Qui vou -- lez- vous, Sei -- gneur, au -- près d’el -- le qu’on met -- te ?

L’Eu -- ro -- pé -- an -- ne ; al -- lez.

L’in -- tri -- gan -- te Spi -- net -- te ?

El -- le- mê -- me.

En ef -- fet, nulle i -- ci ne sait mieux
com -- ment il faut ré -- dui -- re un cœur né scru -- pu -- leux.
