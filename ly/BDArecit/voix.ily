\clef "alto/G_8" <>^\markup\character-text Calpigi au Sultan
r8 la la la la si |
dod'4 r8 mi'16 mi' dod'4 si8 la |
re' re'
\ffclef "bass" <>^\markup\character Atar
r16 re' re' re' re'8 la r re' |
si4
\ffclef "alto/G_8" <>^\markup\character Calpigi
r8 si16 si si4 si8 do' |
re' re'
\ffclef "bass" <>^\markup\character Atar
r8 si16 re' sol4 sol8 r |
\ffclef "alto/G_8" <>^\markup\character Calpigi
r4 r8 si16 si mi'4 r8 si16 do' |
re'4 re'8 mi' do'4 r8 mi' |
do' do' si la red' red' r fad' |
red' red' red' mi' si4 r |
r2 r4
