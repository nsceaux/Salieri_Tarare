\clef "bass" r4 r2 |
R1 |
fad4 r r2 |
sol4 r r2 |
r r4 sol8 fa |
mi4 r r2 |
r2 la4 r |
r2 la4 r |
r2 r4 si |
mi2 r4
