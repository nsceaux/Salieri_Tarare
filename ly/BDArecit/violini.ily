\clef "treble" r4 r2 |
R1 |
<<
  \tag #'violino1 { re''4 }
  \tag #'violino2 { la'4 }
>> r4 r2 |
<<
  \tag #'violino1 { si'4 }
  \tag #'violino2 { re'4 }
>> r4 r2 |
r2 r4 <<
  \tag #'violino1 { si'8 la' | <sold' si' mi''>4 }
  \tag #'violino2 { re'8 re' | mi'4 }
>> r4 r2 |
r2 <mi' do''>4 r |
r2 <fad' red''>4 r |
r2 r4 <si fad' red''>4 |
<si sol' mi''>2 r4
