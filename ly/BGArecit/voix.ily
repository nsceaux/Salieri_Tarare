\clef "bass" <>^\markup\character Atar
r4 r8 la16 la fad4 fad8 sol |
la4 r^\markup\italic (bas) fad8 fad16 fad sol8 la |
re8 re r4 r2 |
\ffclef "alto/G_8" <>^\markup\character Calpigi
r4 r8 sol si si r si16 do' |
re'4. re'8 re' re' do' re' |
si4
\ffclef "bass" <>^\markup\character Atar
r8 sol si4
\ffclef "alto/G_8" <>^\markup\character Calpigi
re'8 do'16 re' |
si8 si r si si si si do' |
sol4 r r2 |
R1 |
\ffclef "tenor/G_8" <>^\markup\character-text Tarare ardemment
r4 r8 sol do'4 r8 do'16 do' |
mi'4 re'8 mi' do' do' r4 |
r r8 do' sib8 sib16 sib mi'8 mi'16 fa' |
do'4^\markup\italic { il se met à genoux. } r4 r2 |
r8 do' do' re' mib'4. do'8 |
la4 la8 sib16 do' fa4 r8 do'16 do' |
fa'4. do'8 mib'4 mib'8 fa' |
re' re' r4 
\ffclef "bass" <>^\markup\character-text Atar d’un ton profond
r4 r8 la |
re'4 r8 la fad fad r la8*1/2 si! |
do'4 re'8 la si!4
\ffclef "tenor/G_8" <>^\markup\character Tarare
r16 re' re' re' |
si!4 mi'!4. si8 re'!8 re'16 mi' |
do'8 do' r4\fermata
\ffclef "bass" <>^\markup\character Atar
r4 r8 la |
la la si do' si4 r8 si |
fad4 la8 la16 si sol4 r8 mi |
lad8. dod'16 lad lad lad si fad8 fad r4 |
\ffclef "alto/G_8" <>^\markup\character-text Calpigi (à part)
r8. si16 re'8 r16 fad' re'8 re' mi' fad' |
si4 r\fermata
