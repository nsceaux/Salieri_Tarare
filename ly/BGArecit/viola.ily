\clef "alto" R1*2 |
r2 re'4 do' |
si r r2 |
R1*3 |
r4 sol'-\sug\f fa' re' |
sol' mi' <fa' sol>2 |
<sol mi'>4 r r2 |
r r8. mi'16-\sug\ff mi'8. mi'16 |
mi'4 r r2 |
r4 fa'2( sol'4) |
la'1~ |
la'~ |
la' |
r4 r8.\fermata fad16-\sug\f fad2 |
R1 |
r2 sol2~ |
sol4 mi'2.-\sug\sf |
r4 r8.\fermata la16-\sug\f la2 |
r red'4-\sug\p r |
r2 mi'4 r |
r2 r4 fad' |
fad'4 r r2 |
r2\fermata
