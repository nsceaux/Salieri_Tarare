\key do \major
\time 4/4 \midiTempo#100 s1*7
\tempo "Allegro" s1*3 s2 s8.
\tempo "Presto" s16 s4 s1*13 \bar "||"
\time 4/4 \key sol \major s1 s2 \bar "|."

