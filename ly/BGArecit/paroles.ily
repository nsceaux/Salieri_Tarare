Que veux- tu, Cal -- pi -- gi ?

Sois in -- in -- tel -- li -- gi -- ble.

Mon maî -- tre, cette Ir -- za si chère à ton a -- mour…

Eh bien ?

Elle est ren -- du -- e à la clar -- té du jour.

A -- tar, ta grande âme est sen -- si -- ble,
la joie a bril -- lé dans tes yeux.

Par cette Ir -- za, Sul -- tan, sois gé -- né -- reux,
à mes maux de -- viens ac -- ces -- si -- ble.

Dis- moi, Ta -- ra -- re, es- tu bien mal -- heu -- reux ?

Si je le suis ! ah ! peut- être elle ex -- pi -- re !

Sou -- hai -- te de -- vant moi qu’Ir -- za cède à mes vœux :
je fais ce que ton cœur dé -- si -- re.

Grand dieux ! je sers un homme af -- freux !
