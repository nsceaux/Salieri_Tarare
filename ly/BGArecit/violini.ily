\clef "treble" R1*2 |
r2 re'4 \grace re'8 do'! si16 do' |
si4 r r2 |
R1*3 |
r4 do'16\f mi' sol' do'' si'8 si' \grace do'' si' la'16 si' |
do''16 re'' mi'' fa'' sol'' la'' si'' do''' si''8 si'' \grace do''' si'' la''16 si'' |
do'''4 r r2 |
r r8. <do'' sol''>16\ff q8. q16 |
q4 r r2 |
r4 <<
  \tag #'violino1 { fa''2( mi''4) | mib''1~ | mib''~ | mib'' | }
  \tag #'violino2 { la'2( sib'4) | do''1~ | do''~ | do'' | }
>>
r4 r8.\fermata <<
  \tag #'violino1 { re'16\f re'2 | }
  \tag #'violino2 { la16-\sug\f la2 | }
>>
R1 |
r2 <<
  \tag #'violino1 { re'2~ | re'4 mi''2.\sf | }
  \tag #'violino2 { si!2~ | si4 re''2.-\sug\sf | }
>>
r4 r8.\fermata <do' mi'>16\f q2 |
r2 <<
  \tag #'violino1 { si'4\p }
  \tag #'violino2 { fad'-\sug\p }
>> r4 |
r2 <<
  \tag #'violino1 { si'4 }
  \tag #'violino2 { sol' }
>> r4 |
r2 r4 <<
  \tag #'violino1 { lad'4 | si' }
  \tag #'violino2 { dod'4 | re' }
>> r4 r2 |
r2\fermata
