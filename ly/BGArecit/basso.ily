\clef "bass" R1*2 |
r2 re4 do |
si, r r2 |
R1*3 |
r4 mi\f re sol |
mi do re sol, |
do r r2 |
r r8. mi16\ff mi8. mi16 |
mi4 r r2 |
fa4 r r2 |
fa1~ |
fa~ |
fa |
r4 r8.\fermata fad16\f fad2 |
R1 |
r2 sol~ |
sol4 sold2.\sf |
r4 r8.\fermata la,16\f la,2 |
r red4\p r |
r2 mi4 r |
r2 r4 fad |
si,4 r r2 |
r2\fermata
