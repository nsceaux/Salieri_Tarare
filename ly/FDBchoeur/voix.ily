<<
  \tag #'(vdessus basse) {
    \clef "soprano/treble" <>^"Chanté" R1*4 |
    r2 r4 <>^\p <<
      { \voiceOne sol'4 |
        re''4 re''8. re''16 re''4 re''8. re''16 |
        mib''2. mib''4 |
        re''4. re''8 mib'' re'' do'' sib' |
        sib'2 la'4 \oneVoice r |
        \voiceOne re''8. re''16 re''8. re''16 sol''4 fa''8 sol'' |
        mib''2 \oneVoice r4 \voiceOne fa'' |
        fa''4. mib''8 sol'' fa'' mib'' re'' |
        re''2.( do''4) |
        sib'2 \oneVoice r4 \voiceOne sol'' |
        fad''2 \oneVoice r4 \voiceOne mib'' |
        re''1 |
        re''8.^\p re''16 re''8. re''16 sol''4 fa''8 sol'' |
        mib''2. mib''4 |
        re''4. re''8 mib'' re'' mib'' do'' |
        sib'2.( si'4) |
        do''2 \oneVoice r4 \voiceOne mib'' |
        re''4. re''8 mib'' re'' mib'' do'' |
        sib'2.( la'4) |
        sol'4 \oneVoice
      }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sol'4 |
        sib'4 sib'8. sib'16 sib'4 sib'8. sib'16 |
        do''2. do''4 |
        sib'4. sib'8 do'' sib' la' sol' |
        sol'2 fad'4 s |
        sol'8. sol'16 sol'8. sol'16 sol'4 re''8 re'' |
        do''2 s4 do'' |
        re''4. do''8 mib'' re'' do'' sib' |
        sib'2.( la'4) |
        sib'2 s4 re'' |
        do''2 s4 sib' |
        la'1 |
        sib'8. sib'16 sib'8. sib'16 si'4 si'8 si' |
        do''2. fad'4 |
        sol'4. sol'8 sol' sol' sol' sol' |
        sol'2.( fa'!4) |
        mib'2 s4 fad' |
        sol'4. sol'8 sol' sol' sol' sol' |
        sol'2.( fad'4) |
        sol'4
      }
    >> r4 r2*1/4
  }
  \tag #'vhaute-contre {
    \clef "alto/G_8" R1*4 |
    r2 r4 <>^\p sol4 |
    re'4 re'8. re'16 re'4 re'8. re'16 |
    mib'2. mib'4 |
    re'4. re'8 mib' re' do' sib |
    sib2 la4 r |
    re'8. re'16 re'8. re'16 sol'4 fa'8 sol' |
    mib'2 r4 fa' |
    fa'4. re'8 sol' fa' mib' re' |
    re'2.( do'4) |
    sib2 r4 sol' |
    fad'2 r4 mib' |
    re'1 |
    re'8. re'16 re'8. re'16 sol'4 fa'8 sol' |
    mib'2. mib'4 |
    re'4. re'8 mib' re' mib' do' |
    sib2.( si4) |
    do'2 r4 mib' |
    re'4. re'8 mib' re' mib' do' |
    sib2.( la4) |
    sol4 r4 r2*1/4
  }
  \tag #'vtaille {
    \clef "tenor/G_8" R1*4 |
    r2 r4 <>^\p sol4 |
    sib4 sib8. sib16 sib4 sib8. sib16 |
    do'2. do'4 |
    sib4. sib8 do' sib la sol |
    sol2 fad4 r |
    sol8. sol16 sol8. sol16 sol4 re'8 re' |
    do'2 r4 do' |
    re'4. sib8 mib' re' do' sib |
    sib2.( la4) |
    sib2 r4 re' |
    do'2 r4 sib |
    la1 |
    sib8. sib16 sib8. sib16 si4 si8 si |
    do'2. fad4 |
    sol4. sol8 sol sol sol sol |
    sol2.( fa!4) |
    mib2 r4 fad |
    sol4. sol8 sol sol sol sol |
    sol2.( fad4) |
    sol4 r4 r2*1/4
  }
  \tag #'vbasse {
    \clef "bass" R1*4 |
    r2 r4 <>^\p sol4 |
    sol sol8. sol16 sol4 sol8. sol16 |
    fad2. fad4 |
    sol4. sol8 do re mib dod |
    re2 re4 r |
    sib,8. sib,16 sib,8. sib,16 si,4 si,8 si, |
    do2 r4 la, |
    sib,4. sib,8 mib mib mib mib |
    fa1 |
    sib,2 r4 sib |
    la2 r4 sol |
    fad1 |
    sol8. sol16 sol8. sol16 sol4 sol8 sol |
    do2 r4 do |
    sib,4. sib,8 do si, do mib |
    re1 |
    do2 r4 do |
    sib,4. sib,8 do si, do mib |
    re1 |
    sol,4 r4 r2*1/4
  }
  \tag #'arthenee {
    \clef "bass" R1*22 |
    r2 r4 <>^\markup\character Arthénée r8 re' |
    re'2\fermata~ re'8
  }
>>
%% Arthénée
<<
  \tag #'(arthenee basse) {
    \tag #'basse { \ffclef "bass" <>^\markup\character Arthénée }
    sib8 sib sib |
    sib4 sib8 sib sib4 sib8 sib |
    sib2 r4 r8 sib |
    do'4. do'8 re'4. re'8 |
    mib'1 |
    do'4. do'8 do'4 reb'8. sib16 |
    lab2 r4 r8 lab |
    re'4. re'8 re'4. do'8 |
    si2. do'8 do' |
    re'2 fa'4. si8 |
    do'2 r4 do'8 do' |
    re'2. re'8 fa' |
    lab'?2 si4. si8 |
    do'2 r\fermata |
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) { s4. | R1*13 | }
>>
%% Chœur
<<
  \tag #'(vdessus basse) {
    \tag #'basse \ffclef "soprano/treble"
    r2 r4 <<
      { \voiceOne sol'4 |
        re''4 re''8. re''16 re''4 re''8. re''16 |
        mib''2. mib''4 |
        re''4. re''8 mib'' re'' do'' sib' |
        sib'2 la'4 \oneVoice r |
        \voiceOne re''8. re''16 re''8. re''16 sol''4 fa''8. sol''16 |
        mib''2 \oneVoice r4 \voiceOne fa'' |
        fa''4. mib''8 sol'' fa'' mib'' re'' |
        re''2.( do''4) |
        sib'2 \oneVoice r4 \voiceOne sol'' |
        fad''2 \oneVoice r4 \voiceOne mib'' |
        re''1 |
        re''8.^\p re''16 re''8. re''16 sol''4 fa''8 sol'' |
        mib''2. mib''4 |
        re''4. re''8 mib'' re'' mib'' do'' |
        sib'2.( si'4) |
        do''2 \oneVoice r4 \voiceOne mib'' |
        re''4. re''8 mib'' re'' mib'' do'' |
        sib'2.( la'4) |
        sol'2 \oneVoice
      }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sol'4 |
        sib'4 sib'8. sib'16 sib'4 sib'8. sib'16 |
        do''2. do''4 |
        sib'4. sib'8 do'' sib' la' sol' |
        sol'2 fad'4 s |
        sol'8. sol'16 sol'8. sol'16 sol'4 re''8. re''16 |
        do''2 s4 do'' |
        re''4. do''8 mib'' re'' do'' sib' |
        sib'2.( la'4) |
        sib'2 s4 re'' |
        do''2 s4 sib' |
        la'1 |
        sib'8. sib'16 sib'8. sib'16 si'4 si'8 si' |
        do''2. fad'4 |
        sol'4. sol'8 sol' sol' sol' sol' |
        sol'2.( fa'!4) |
        mib'2 s4 fad' |
        sol'4. sol'8 sol' sol' sol' sol' |
        sol'2.( fad'4) |
        sol'2
      }
    >> r2 |
    R1 |
    R1^\fermataMarkup |
  }
  \tag #'vhaute-contre {
    r2 r4 sol4 |
    re'4 re'8. re'16 re'4 re'8. re'16 |
    mib'2. mib'4 |
    re'4. re'8 mib' re' do' sib |
    sib2 la4 r |
    re'8. re'16 re'8. re'16 sol'4 fa'8. sol'16 |
    mib'2 r4 fa' |
    fa'4. re'8 sol' fa' mib' re' |
    re'2.( do'4) |
    sib2 r4 sol' |
    fad'2 r4 mib' |
    re'1 |
    re'8. re'16 re'8. re'16 sol'4 fa'8 sol' |
    mib'2. mib'4 |
    re'4. re'8 mib' re' mib' do' |
    sib2.( si4) |
    do'2 r4 mib' |
    re'4. re'8 mib' re' mib' do' |
    sib2.( la4) |
    sol2 r2 |
    R1 |
    R1^\fermataMarkup |
  }
  \tag #'vtaille {
    r2 r4 sol4 |
    sib4 sib8. sib16 sib4 sib8. sib16 |
    do'2. do'4 |
    sib4. sib8 do' sib la sol |
    sol2 fad4 r |
    sol8. sol16 sol8. sol16 sol4 re'8 re' |
    do'2 r4 do' |
    re'4. sib8 mib' re' do' sib |
    sib2.( la4) |
    sib2 r4 re' |
    do'2 r4 sib |
    la1 |
    sib8. sib16 sib8. sib16 si4 si8 si |
    do'2. fad4 |
    sol4. sol8 sol sol sol sol |
    sol2.( fa!4) |
    mib2 r4 fad |
    sol4. sol8 sol sol sol sol |
    sol2.( fad4) |
    sol2 r2 |
    R1 |
    R1^\fermataMarkup |
  }
  \tag #'vbasse {
    r2 r4 sol4 |
    sol sol8. sol16 sol4 sol8. sol16 |
    fad2. fad4 |
    sol4. sol8 do re mib dod |
    re2 re4 r |
    sib,8. sib,16 sib,8. sib,16 si,4 si,8 si, |
    do2 r4 la, |
    sib,4. sib,8 mib mib mib mib |
    fa1 |
    sib,2 r4 sib |
    la2 r4 sol |
    fad1 |
    sol8. sol16 sol8. sol16 sol4 sol8 sol |
    do2 r4 do |
    sib,4. sib,8 do si, do mib |
    re1 |
    do2 r4 do |
    sib,4. sib,8 do si, do mib |
    re1 |
    sol,2 r2 |
    R1 |
    R1^\fermataMarkup |
  }
  \tag #'arthenee { R1*22 | }
>>
