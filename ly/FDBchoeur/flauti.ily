\clef "treble" r2 r4 <>-\sug\p \twoVoices#'(flauto1 flauto2 flauti) <<
  { sol'4 |
    re'' re''8. re''16 re''4 re'' |
    mib''2. mib''4 |
    re''4 do''8 sib' la'4 re'' |
    sib'2~ sib'4 sol' |
    re''4 re''8. re''16 re''4 re'' |
    mib''2. mib''4 |
    re''4. re''8 mib'' re'' do'' sib' |
    sib'2( la'4) }
  { sol'4 |
    sib' sib'8. sib'16 sib'4 sib' |
    do''2. do''4 |
    sib'4 la'8 sol' fad'2 |
    sol'2~ sol'4 sol' |
    sib'4 sib'8. sib'16 sib'4 sib' |
    do''2. do''4 |
    sib'4. sib'8 do'' sib' la' sol' |
    sol'2( fad'4) }
>> r4 |
\twoVoices#'(flauto1 flauto2 flauti) <<
  { re''2 sol''4 fa'' |
    mib''2. fa''4 |
    fa''4. re''8 sol''( fa'' mib'' re'') |
    re''2. do''4 |
    sib' sib''( sol''8) r sol''4( |
    fad''8) }
  { sol'2~ sol'4 re'' |
    do''2. do''4 |
    re''4. sib'8 mib''( re'' do'' sib') |
    sib'2. la'4 |
    sib'4 re''2 re''4( |
    do''8) }
  { s1*4 | s4 s2\sf s4\p }
>> r8 \twoVoices#'(flauto1 flauto2 flauti) <<
  { fad''4( sol''8) }
  { do''4( sib'8) }
  { s4\sf }
>> r8 \twoVoices#'(flauto1 flauto2 flauti) <<
  { mib''4 |
    re''1 |
    re''2 sol''4 fa'' |
    mib''2. mib''4 |
    re''4. re''8 mib'' re'' mib'' do'' |
    sib'2. si'4 |
    do''2. mib''4 |
    re''4. re''8 mib'' re'' mib'' do'' |
    sib'2. la'8. la''16 |
    sib''2\fermata~ sib''8
  }
  { sib'4 |
    la'1 |
    sib'2 si' |
    do''2. fad'4 |
    sol'1 |
    sol'2. fa'!4 |
    mib'2. fad'4 |
    sol'1 |
    sol'2. fad'8. re''16 |
    re''2\fermata~ re''8 }
  { s4\p | s2\< s\> | s1\p | s1*6 | s2\f }
>> r8 r4 |
R1*12 |
R1^\fermataMarkup |
r2 r4 \twoVoices#'(flauto1 flauto2 flauti) <<
  { sol'4 |
    re''4 re''8. re''16 re''4 re'' |
    mib''2. mib''4 |
    re''4. re''8 mib'' re'' do'' sib' |
    sib'2( la'4) }
  { sol'4 |
    sib'4 sib'8. sib'16 sib'4 sib' |
    do''2. do''4 |
    sib'4. sib'8 do'' sib' la' sol' |
    sol'2( fad'4) }
>> r4 |
\twoVoices#'(flauto1 flauto2 flauti) <<
  { re''2( sol''4) fa'' |
    mib''2. fa''4 |
    fa''4. re''8 sol''( fa'' mib'' re'') |
    re''2. do''4 |
    sib' sib''( sol''8) r sol''4( |
    fad''8) }
  { sol'2~ sol'4 re'' |
    do''2. do''4 |
    re''4. sib'8 mib''( re'' do'' sib') |
    sib'2. la'4 |
    sib'4 re''2 re''4( |
    do''8) }
  { s1*4 | s4 s2\sf s4\p }
>> r8 \twoVoices#'(flauto1 flauto2 flauti) <<
  { fad''4( sol''8) }
  { do''4( sib'8) }
  { s4\sf }
>> r8 \twoVoices#'(flauto1 flauto2 flauti) <<
  { mib''4 |
    re''1 |
    re''2 sol''4 fa'' |
    mib''2. mib''4 |
    re''4. re''8 mib'' re'' mib'' do'' |
    sib'2. si'4 |
    do''2. mib''4 |
    re''4. re''8 mib'' re'' mib'' do'' |
    sib'2. la'4 |
    sol'4 re''8. re''16 re''4 re'' |
    re''4. sib'8 sib'4. sol'8 | }
  { sib'4 |
    la'1 |
    sib'2 si' |
    do''2. fad'4 |
    sol'1 |
    sol'2. fa'!4 |
    mib'2. fad'4 |
    sol'1 |
    sol'2. fad'4 |
    sol'4 sib'8. sib'16 sib'4 sib' |
    sib'4. sol'8 sol'4. sol'8 | }
  { s4\p | s2\< s\> | s1\p }
>>
sol'1\fermata |
