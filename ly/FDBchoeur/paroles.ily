\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  A -- vec tes dé -- crets in -- fi -- nis,
  grand Dieu, si ta bon -- té s’ac -- cor -- de,
  ouvre à ces cou -- pa -- bles pu -- nis
  le sein de ta mi -- sé -- ri -- cor -- de !
  Grand Dieu, grand Dieu,
  ouvre à ces cou -- pa -- bles pu -- nis
  le sein de ta mi -- sé -- ri -- cor -- de,
  le sein de ta mi -- sé -- ri -- cor -- de !
}
\tag #'(arthenee basse) {
  \tag #'arthenee { Bra -- ma ! }
  de ce bû -- cher, par la mort ré -- u -- nis,
  ils mon -- tent vers le ciel ; qu’ils n’en soient point ban -- nis,
  ils mon -- tent vers le ciel ; qu’ils n’en soient point ban -- nis,
  qu’ils n’en soient, qu’ils n’en soient point ban -- nis !
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  A -- vec tes dé -- crets in -- fi -- nis,
  grand Dieu, si ta bon -- té s’ac -- cor -- de,
  ouvre à ces cou -- pa -- bles pu -- nis
  le sein de ta mi -- sé -- ri -- cor -- de !
  Grand Dieu, grand Dieu,
  ouvre à ces cou -- pa -- bles pu -- nis
  le sein de ta mi -- sé -- ri -- cor -- de,
  le sein de ta mi -- sé -- ri -- cor -- de !
}
