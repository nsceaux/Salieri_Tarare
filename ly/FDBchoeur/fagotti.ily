\clef "tenor" r2 r4 <>-\sug\p \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sol4 |
    re' re'8. re'16 re'4 re' |
    mib'2. mib'4 |
    re'4 do'8 sib la4 re' |
    sib2~ sib4 sol |
    re'4 re'8. re'16 re'4 re' |
    mib'2. mib'4 |
    re'4. re'8 mib' re' do' sib |
    sib2( la4) }
  { sol4 |
    sib sib8. sib16 sib4 sib |
    do'2. do'4 |
    sib4 la8 sol fad2 |
    sol2~ sol4 sol |
    sib4 sib8. sib16 sib4 sib |
    do'2. do'4 |
    sib4. sib8 do' sib la sol |
    sol2( fad4) }
>> r4 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { re'2 sol'4 fa' |
    mib'2. fa'4 |
    fa'4. re'8 sol'( fa' mib' re') |
    re'2. do'4 |
    sib re'~ re'8 \oneVoice r \voiceOne sol'4( |
    fad'8) }
  { sol2~ sol4 re' |
    do'2. do'4 |
    re'4. sib8 mib'( re' do' sib) |
    sib2. la4 |
    sib4 sib4( sol8) s re'4( |
    do'8) }
  { s1*4 | s4 s2\sf s4\p }
>> r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { fad'4( sol'8) }
  { do'4( sib8) }
  { s4\sf }
>> r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { mib'4 |
    re'1 |
    re'2 sol'4 fa' |
    mib'2. mib'4 |
    re'4. re'8 mib' re' mib' do' |
    sib2. si4 |
    do'2. mib'4 |
    re'4. re'8 mib' re' mib' do' |
    sib2. la8. re'16 |
    re'2\fermata~ re'8 }
  { sib4 |
    la1 |
    sib2 si |
    do'2. fad4 |
    sol1 |
    sol2. fa!4 |
    mib2. fad4 |
    sol1 |
    sol2. fad4 |
    sib2\fermata~ sib8 }
  { s4\p | s2\< s\> | s1\p | s1*6 | s2\f }
>> r8 r4 |
R1*12 |
R1^\fermataMarkup |
r2 r4 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { sol4 |
    re'4 re'8. re'16 re'4 re' |
    mib'2. mib'4 |
    re'4. re'8 mib' re' do' sib |
    sib2( la4) }
  { sol4 |
    sib4 sib8. sib16 sib4 sib |
    do'2. do'4 |
    sib4. sib8 do' sib la sol |
    sol2( fad4) }
>> r4 |
\twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { re'2( sol'4) fa' |
    mib'2. fa'4 |
    fa'4. re'8 sol'( fa' mib' re') |
    re'2. do'4 |
    sib re'( re'8) \oneVoice r \voiceOne sol'4( |
    fad'8) }
  { sol2~ sol4 re' |
    do'2. do'4 |
    re'4. sib8 mib'( re' do' sib) |
    sib2. la4 |
    sib4 sib4( sol8) s re'4( |
    do'8) }
  { s1*4 | s4 s2\sf s4\p }
>> r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { fad'4( sol'8) }
  { do'4( sib8) }
  { s4\sf }
>> r8 \twoVoices#'(fagotto1 fagotto2 fagotti) <<
  { mib'4 |
    re'1 |
    re'2 sol'4 fa' |
    mib'2. mib'4 |
    re'4. re'8 mib' re' mib' do' |
    sib2. si4 |
    do'2. mib'4 |
    re'4. re'8 mib' re' mib' do' |
    sib2. la4 |
    sol4 re'8. re'16 re'4 re' |
    re'4. sib8 sib4. sol8 | }
  { sib4 |
    la1 |
    sib2 si |
    do'2. fad4 |
    sol1 |
    sol2. fa!4 |
    mib2. fad4 |
    sol1 |
    sol2. fad4 |
    sol4 sib8. sib16 sib4 sib |
    sib4. sol8 sol4. sol8 | }
  { s4\p | s2\< s\> | s1\p }
>>
sol1\fermata |
