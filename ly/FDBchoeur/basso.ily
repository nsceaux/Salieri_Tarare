\clef "bass" <>\p \ru#8 sol,32 sol,2.:32 |
sol,2:32 sol,:32 |
fad,:32 fad,:32 |
sol,:32 re:32 |
sol,:32 sol,:32 |
sol,:32 sol,:32 |
fad,:32 fad,:32 |
sol,:32 do:32 |
re:32 re:32 |
sib,:32 si,:32 |
do2.:32 la,4:32 |
sib,2:32 mib:32 |
fa:32 fa:32 |
sib,4:32 sol,:32\f sib,2:32\p |
la,2:32\f sol,:32\p |
fad,:32\< fad,:32\> |
sol,:32\p sol,:32 |
do:32 do:32 |
sib,:32 do:32 |
re:32 re:32 |
do:32 do:32 |
sib,:32 do:32 |
re:32 re:32 |
sol,2\f\fermata~ sol,8 sol[\p sol sol] |
fa4 fa8. mib16 re4 re8. re16 |
mib4 mib mib mib |
mib\cresc mib mib mib |
mib\f mib mib mib |
mi!1\p |
fa4 fa\f fa fa |
fa fa\p fa fa |
sol2. mib4 |
fa fa fa fa |
mib\f mib mib mib\p |
fa8 fa fa fa fa2:8 |
fa2\fp sol |
do r\fermata |
\ru#8 sol,32 sol,2.:32 |
sol,2:32 sol,:32 |
fad,:32 fad,:32 |
sol,:32 do:32 |
re:32 re:32 |
sib,:32 si,:32 |
do2.:32 la,4:32 |
sib,2:32 mib:32 |
fa:32 fa:32 |
sib,4:32 sol,:32\sf sib,2:32\fp |
la,4:32 la,:32\sf sol,:32 sol,:32 |
fad,2:32\< fad,:32\> |
sol,:32\p sol,:32 |
do:32 do:32 |
sib,:32 do:32 |
re:32 re:32 |
do:32 do:32 |
sib,:32 do:32 |
re:32 re:32 |
sol,2:32 sol,:32 |
sol,2:32 sol,:32 |
sol,1\fermata |
