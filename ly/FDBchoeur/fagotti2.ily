\clef "bass" R1 |
sol1-\sug\p |
fad |
sol2 re |
sol~ sol4 r |
sol1 |
fad |
sol2 do8( re mib dod) |
re1 |
sib,2 si, |
do2. la,4 |
sib,2 mib, |
fa,1 |
sib,4 sol,\sf( sib,8) r sib4(\p |
la8) r la4(\sf sol8) r sol4\p |
fad1*1/2\< s2\> |
sol2\p sol, |
do2. do4 |
sib,4. sib,8 do si, do mib |
re1 |
do2. do4 |
sib,4. sib,8 do si, do mib |
re1 |
sol,4 r r2 |
R1*12 |
R1^\fermataMarkup |
R1 |
sol1 |
fad |
sol2 do8 re mib dod |
re1 |
sib,2 si, |
do2. la,4 |
sib,2 mib, |
fa,1 |
sib,4 sol,\sf( sib,8) r sib4(\p |
la8) r la4(\sf sol8) r sol4\p |
fad1*1/2\< s2\> |
sol2\p sol, |
do2. do4 |
sib,4. sib,8 do si, do mib |
re1 |
do2. do4 |
sib,4. sib,8 do si, do mib |
re1 |
sol,~ |
sol,~ |
sol,\fermata |

