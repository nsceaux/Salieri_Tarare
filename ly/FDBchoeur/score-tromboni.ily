\score {
  <<
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      \global \keepWithTag #'arthenee \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'arthenee \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiri } <<
      \new Staff \with { \consists "Metronome_mark_engraver" } <<
          <>^\markup\whiteout { les deux \concat { 1 \super ers } Trombones }
        \global \includeNotes "tromboni"
      >>
      \new Staff <<
          <>^\markup\whiteout { \concat { 3 \super e } Trombone }
        \global \includeNotes "fagotti2"
      >>
    >>
  >>
  \layout { \context { \Score \remove "Metronome_mark_engraver" } }
}
