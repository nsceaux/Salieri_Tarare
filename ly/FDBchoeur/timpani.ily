\clef "bass" <>\p \ru#8 sol,32 sol,2.:32 |
sol,2:32 sol,:32 |
sol,:32 sol,:32 |
sol,:32 re:32 |
sol,:32 sol,:32 |
sol,:32 sol,:32 |
sol,:32 sol,:32 |
sol,:32 sol,:32 |
re:32 re:32 |
sol,:32 sol,:32 |
sol,2.:32 re4:32 |
re2:32 sol,:32 |
re2.:32 sol,4:32 |
re2:32 re:32 |
re:32 re:32 |
re:32\< re:32\> |
sol,:32-\sug\p sol,:32 |
sol,:32 sol,:32 |
sol,:32 sol,:32 |
re:32 re:32 |
sol,:32 sol,:32 |
sol,:32 sol,:32 |
re:32 re:32 |
sol,2-\sug\f\fermata r |
R1*12 |
R1^\fermataMarkup |
\ru#8 sol,32 sol,2.:32 |
sol,2:32 sol,:32 |
sol,:32 sol,:32 |
sol,:32 sol,:32 |
re:32 re:32 |
sol,:32 sol,:32 |
sol,2.:32 re4:32 |
re2:32 sol,:32 |
re2.:32 sol,4:32 |
re2:32 re:32 |
re:32 re:32 |
re:32\< re:32\> |
sol,:32-\sug\p sol,:32 |
sol,:32 sol,:32 |
sol,:32 sol,:32 |
re:32 re:32 |
sol,:32 sol,:32 |
sol,:32 sol,:32 |
re:32 re:32 |
sol,:32 sol,:32 |
sol,:32 sol,:32 |
sol,1\fermata |
