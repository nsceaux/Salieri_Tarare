\score {
  <<
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      \global \keepWithTag #'arthenee \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'arthenee \includeLyrics "paroles" }
    \new Staff \with { \haraKiri instrumentName = "Timbales" } <<
      \keepWithTag #'() \global \includeNotes "timpani"
    >>
  >>
  \layout { indent = \largeindent }
}
