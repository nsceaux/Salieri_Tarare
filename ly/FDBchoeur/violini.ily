\clef "treble" R1*22 |
r2 r4 r8 <<
  \tag #'violino1 {
    <re'' re'''>8\f |
    re'''2\fermata~ re'''8 sib''[\p sib'' sib''] |
    sib''8(-. sib''-. sib''-. sib''-. sib''-. sib''-. sib''-. sib''-.) |
    sib''8 sib'4 sib' sib' sib'8 |
    do''8\cresc do''4 do''8 re''8 re''4 re''8 |
    mib''8\f sol''4 sib'' mib''' mib'''8 |
    do'''(\p sol'' mi'' do'') do''4 sib' |
    lab'8 r lab''4.(\f sol''8 fa'' mib'') |
    re''4\p re'''2( do'''4) |
    si''2.( do'''4) |
    r8 re'' re'' re'' re'' re'' re'' re'' |
    do''(\f mib'' sol'' do''') do''' do'''8 do'''4\p |
    re''8 re''4 re'' re''8( fa'' lab'') |
    lab''2\fp si' |
    do''1\fermata |
  }
  \tag #'violino2 {
    <re'' la''>8-\sug\f |
    <re'' sib''>2\fermata~ q8 re''[-\sug\p re'' re''] |
    lab''4 lab''8. sol''16 fa''4 fa''8. fa''16 |
    sol''8 sol'4 sol' sol' sol'8 |
    lab'8\cresc lab'4 lab'8 <lab' sib>8 q4 q8 |
    <sib sol'>8-\sug\f <sib' sol''>4 q q q8 |
    sol'1-\sug\p |
    do'8 r lab'4.-\sug\f lab'8 lab' lab' |
    lab'1-\sug\p |
    lab'4 sol'2 sol'4 |
    r8 lab' lab' lab' lab' lab' lab' lab' |
    sol'(-\sug\f do'' mib'' sol'') sol''( mib'' do''-\sug\p sol') |
    lab'8 lab'4 lab' lab' lab'8 |
    lab'2-\sug\fp sol' |
    sol'1\fermata |
  }
>>
R1*22 |
