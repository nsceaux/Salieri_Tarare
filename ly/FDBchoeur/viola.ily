\clef "alto" R1*22 |
r2 r4 r8 re'-\sug\f |
sol'2\fermata~ sol'8 re'[\p re' re'] |
re'4 re'8. mib'16 fa'4 sib~ |
sib8 mib'4 mib' mib' mib'8 |
mib'8-\sug\cresc mib'4 mib'8 fa' fa'4 fa'8 |
sib-\sug\f <sol' sib'>4 q q q8 |
do'1\p |
do' |
fa'-\sug\p |
re'2. do'4 |
r8 do'' do'' do'' si' si' si' si' |
do''8-\sug\f do''4 do'' do''-\sug\p do''8 |
fa' fa'4 fa' fa' fa'8 |
re'1-\sug\fp |
mib'\fermata |
R1*22 |
