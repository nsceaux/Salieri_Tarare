\score {
  <<
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      \global \keepWithTag #'arthenee \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'arthenee \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiri } <<
      \new Staff \with { \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'flauto1 \includeNotes "flauti"
      >>
      \new Staff << \global \keepWithTag #'flauto2 \includeNotes "flauti" >>
    >>
  >>
  \layout { \context { \Score \remove "Metronome_mark_engraver" } }
}
