\score {
  <<
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      \global \keepWithTag #'arthenee \includeNotes "voix"
    >> { \set fontSize = -2 \keepWithTag #'arthenee \includeLyrics "paroles" }
    \new GrandStaff \with { \haraKiri } <<
      \new Staff \with { \consists "Metronome_mark_engraver" } <<
        <>^\markup\whiteout { les deux \concat { 1 \super ers } Bassons }
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new Staff <<
        <>^\markup\whiteout { les deux autres Bassons }
        \global \includeNotes "fagotti2"
      >>
    >>
  >>
  \layout { \context { \Score \remove "Metronome_mark_engraver" } }
}
