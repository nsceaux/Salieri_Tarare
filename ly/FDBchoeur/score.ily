\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with {
        instrumentName = \markup\center-column {
          Clarinettes et Flûtes unies
        }
        shortInstrumentName = \markup\center-column { Cl. Fl. }
      } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'flauto1 \includeNotes "flauti"
        >>
        \new Staff << \global \keepWithTag #'flauto2 \includeNotes "flauti" >>
      >>
      \new GrandStaff \with { \fagottiInstr } <<
        \new Staff <<
          <>^\markup\whiteout { les deux \concat { 1 \super ers } Bassons }
          \global \keepWithTag#'fagotti \includeNotes "fagotti"
        >>
        \new Staff <<
          <>^\markup\whiteout { les deux autres Bassons }
          \global \includeNotes "fagotti2"
        >>
      >>
      \new GrandStaff \with { \tromboniInstr } <<
        \new Staff <<
          <>^\markup\whiteout { les deux \concat { 1 \super ers } Trombones }
          \global \includeNotes "tromboni"
        >>
        \new Staff <<
          <>^\markup\whiteout { \concat { 3 \super e } Trombone }
          \global \includeNotes "fagotti2"
        >>
      >>
      \new Staff \with { \timpaniInstr } <<
        \keepWithTag #'() \global \includeNotes "timpani"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          <>^\markup\whiteout { Les violons et altos comptent }
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      instrumentName = \markup\character Chœur
      shortInstrumentName = \markup\character Ch.
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with {
      \haraKiriFirst
      shortInstrumentName = \markup\character Ar.
    } \withLyrics <<
      \global \keepWithTag #'arthenee \includeNotes "voix"
    >> \keepWithTag #'arthenee \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \keepWithTag #'basso \includeNotes "basso"
      \origLayout {
        s1*6\pageBreak
        s1*6\break s1*7\pageBreak
        s1*7\pageBreak
        s1*5\break s1*6\pageBreak
        s1*5\break s1*5\pageBreak
        s1*5\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
