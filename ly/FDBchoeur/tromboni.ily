\clef "alto" r2 r4 <>-\sug\p \twoVoices#'(trombono1 trombono2 tromboni) <<
  { sol4 |
    re' re'8. re'16 re'4 re' |
    mib'2. mib'4 |
    re'4 do'8 sib la4 re' |
    sib2 sib4 sol |
    re'4 re'8. re'16 re'4 re' |
    mib'2. mib'4 |
    re'4. re'8 mib' re' do' sib |
    sib2( la4) }
  { sol4 |
    sib sib8. sib16 sib4 sib |
    do'2. do'4 |
    sib4 la8 sol fad4 fad |
    sol2 sol4 sol |
    sib4 sib8. sib16 sib4 sib |
    do'2. do'4 |
    sib4. sib8 do' sib la sol |
    sol2( fad4) }
>> r4 |
\twoVoices#'(trombono1 trombono2 tromboni) <<
  { re'2 sol'4 fa' |
    mib'2. fa'4 |
    fa'4. re'8 \once\slurDashed sol'( fa' mib' re') |
    re'2. do'4 |
    sib sib'4( sol'8) r sol'4( |
    fad'8) }
  { sol2~ sol4 re' |
    do'2. do'4 |
    re'4. sib8 \once\slurDashed mib'( re' do' sib) |
    sib2. la4 |
    sib4 re'2 re'4( |
    do'8) }
  { s1*4 | s4 s2\sf s4\p }
>> r8 \twoVoices#'(trombono1 trombono2 tromboni) <<
  { fad'4( sol'8) }
  { do'4( sib8) }
  { s4\sf }
>> r8 \twoVoices#'(trombono1 trombono2 tromboni) <<
  { mib'4 |
    re'1 |
    re'2 sol'4 fa'! |
    mib'2. mib'4 |
    re'4. re'8 mib' re' mib' do' |
    sib2. si4 |
    do'2. mib'4 |
    re'4. re'8 mib' re' mib' do' |
    sib2. la8. re'16 |
    re'2\fermata~ re'8 }
  { sib4 |
    la1 |
    sib2 si |
    do'2. fad4 |
    sol1 |
    sol2. fa!4 |
    mib2. fad4 |
    sol1 |
    sol2. fad4 |
    sib2\fermata~ sib8 }
  { s4\p | s2\< s\> | s1\p | s1*6 | s2\f }
>> r8 r4 |
R1 |
\clef "bass" <>-\sug\p \twoVoices#'(trombono1 trombono2 tromboni) <<
  { sib1 | do'2 re' | mib'1 | }
  { sol1 | lab2 lab | sol1 | }
>>
R1 |
r4 \twoVoices#'(trombono1 trombono2 tromboni) <<
  { lab?2 lab4 | }
  { fa2 fa4 | }
>> \allowPageTurn
R1*6 |
R1^\fermataMarkup |
\clef "alto" r2 r4 \twoVoices#'(trombono1 trombono2 tromboni) <<
  { sol4 |
    re'4 re'8. re'16 re'4 re' |
    mib'2. mib'4 |
    re'4. re'8 mib' re' do' sib |
    sib2( la4) }
  { sol4 |
    sib4 sib8. sib16 sib4 sib |
    do'2. do'4 |
    sib4. sib8 do' sib la sol |
    sol2( fad4) }
>> r4 |
\twoVoices#'(trombono1 trombono2 tromboni) <<
  { re'2( sol'4) fa' |
    mib'2. fa'4 |
    fa'4. re'8 \once\slurDashed sol'( fa' mib' re') |
    re'2. do'4 |
    sib sib'( la'8) r sol'4( |
    fad'8) }
  { sol2~ sol4 re' |
    do'2. do'4 |
    re'4. sib8 \once\slurDashed mib'( re' do' sib) |
    sib2. la4 |
    sib4 re'2 re'4( |
    do'8) }
  { s1*4 | s4 s2\sf s4\p }
>> r8 \twoVoices#'(trombono1 trombono2 tromboni) <<
  { fad'4( sol'8) }
  { do'4( sib8) }
  { s4\sf }
>> r8 \twoVoices#'(trombono1 trombono2 tromboni) <<
  { mib'4 |
    re'1 |
    re'2 sol'4 fa' |
    mib'2. mib'4 |
    re'4. re'8 mib' re' mib' do' |
    sib2. si4 |
    do'2. mib'4 |
    re'4. re'8 mib' re' mib' do' |
    sib2. la4 |
    sol4 re'8. re'16 re'4 re' |
    re'4. sib8 sib4. }
  { sib4 |
    la1 |
    sib2 si |
    do'2. fad4 |
    sol1 |
    sol2. fa!4 |
    mib2. fad4 |
    sol1 |
    sol2. fad4 |
    sol4 sib8. sib16 sib4 sib |
    sib4. sol8 sol4. }
  { s4\p | s2\< s\> | s1\p }
>>
\clef "bass" \twoVoices#'(trombono1 trombono2 tromboni) <<
  { sol8 | sol1\fermata | }
  { sib,8 | sib,1\fermata | }
>>
