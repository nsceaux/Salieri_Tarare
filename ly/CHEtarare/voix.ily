\clef "tenor/G_8" <>^\markup\italic avec hauteur ^"Chanté"
re'8 |
sol2 sol8 sol16 sol si8 do' |
re'4 re' r re'8. re'16 |
mi'4. do'8 sol4 do'8. mi'16 |
re'2 r8 si re' si |
la4. la8 dod'4. dod'8 |
re'4 re' r8 re' fad' re' |
si4. sol'8 mi'4. dod'8 |
re'2 <>^\markup\italic { du ton le plus méprisant } r4 re'8. re'16 |
mi'4 do' sol do'8 mi' |
re'4 re' r re'8 re' |
do'4 la fad do'8 do' |
si2 r8 re' re' re' |
re'4. re'8 re'4. re'8 |
si4 re' r sol'8 sol' |
mi'4 do'8 do' la4 re' |
sol r r2 |
