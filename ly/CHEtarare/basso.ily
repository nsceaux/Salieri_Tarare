\clef "bass" r8 |
sol2\f r |
r8. sol,16\f sol,8. sol,16 sol,4 r |
do4\f r r2 |
si,4\f r r2 |
fad4-\sug\f r mi r |
re r r re |
sol mi la la, |
re8 re16 re re8 re re4 si, |
do r do r |
si, r si, r |
la, r re, r |
sol, r re r |
sol, r re r |
sol, r r2 |
do4 la, re re |
sol, r r2 |
