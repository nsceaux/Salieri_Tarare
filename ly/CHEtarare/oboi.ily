\clef "treble" r8 |
R1*2 |
<>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'4 }
  { mi' }
>> r4 r2 |
<>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'4 }
  { re' }
>> r4 r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la'4 s la' s | la' }
  { re' s sol' s | fad' }
  { s4-\sug\f r s r }
>> r4 r2 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4 mi'' dod'' | }
  { sol''4 mi'' dod'' | }
>>
re''8 re'16 re' re'8 re' re'4 r |
r mi''\trill sol''\trill mi''\trill |
r re''\trill sol''\trill re''\trill |
r fad''\trill la''\trill do''\trill |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''8 sol'' sol'' fad''4\trill }
  { si'8 si' si' la'4\trill }
>> r4 |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''8 sol'' sol'' fad''4\trill }
  { si'8 si' si' la'4\trill }
>> r4 |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''8 sol'' sol'' sol''4\trill }
  { si'8 si' si' si'4\trill }
>> r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4\trill mi''\trill fad''\trill fad''\trill | sol'' }
  { do''4\trill do''\trill la'\trill la'\trill | si' }
>> r4 r2 |
