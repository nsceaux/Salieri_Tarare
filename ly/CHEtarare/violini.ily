\clef "treble" r8 |
<<
  \tag #'violino1 {
    sol'2\f sol'8 sol'16 sol' si'8 do'' |
    re''8.\f
  }
  \tag #'violino2 {
    <si re'>2-\sug\f si8 si16 si sol8 la |
    si8.-\sug\f
  }
>> <si re'>16 q8. q16 q4 r |
r8 <sol mi'>16\f q q8 <<
  \tag #'violino1 { sol'\p do'' mi'' sol'' do''' | }
  \tag #'violino2 { mi'8-\sug\p sol' do'' mi'' mi'' }
>>
r8 <sol re'>16\f q q8 <<
  \tag #'violino1 { sol'8\p si' re'' sol'' si'' | }
  \tag #'violino2 { re'8-\sug\p sol' si' re'' sol'' | }
>>
r8 <re' la'>16\f q q8 q r8 <<
  \tag #'violino1 {
    dod''16 dod'' dod''8 dod'' |
    r8 re''16 re'' re''8 re'' re'' re''[ fad'' re''] |
    si'8 si'16 si' sol''8 sol''16 sol'' mi''8 mi''16 mi'' dod''8 dod''16 dod'' |
    re''8 <fad'' re'''>16 q q4:16 q4 r |
  }
  \tag #'violino2 {
    << { la'16 la' la'8 la' } \\ { sol'16 sol' sol'8 sol' } >> |
    r8 << { la'16 la' la'8 la' } \\ { fad'16 fad' fad'8 fad' } >> <fad' la'> fad'[ la' <la' re'>] |
    <re' si'>8 q16 q <sol sol'>8 q16 q <la sol'>8 q16 q <la mi'>8 q16 q |
    <la fad'>8 <la' fad''>16 q q4:16 q r |
  }
>>
<>\p \ru#2 { \tuplet 6/4 { mi'8( sol' do'') mi''-! do''-! sol'-! } } |
\ru#2 { \tuplet 6/4 { re'8( sol' si') re''-! si'-! sol'-! } } |
\ru#2 { \tuplet 6/4 { do'8( fad' la') do''-! la'-! fad'-! } } |
<>^\markup\italic Staccato sol'8 <<
  \tag #'violino1 {
    sol'16[ la'] si' la' si' sol' la'8 la'16 si' do'' si' do'' la' |
    si'8 sol'16[ la'] si' la' si' sol' la'8 la'16 si' do'' si' do'' la' |
    r8 sol'16 la' si' do'' si' do'' re'' sol''-. si''-. la''-. sol''-. fad''-. mi''-. re''-. |
    r16*2/3 mi''( fad'' sol'' fad'' mi'') r do'''( re''' mi''' re''' do''') r fad''( sol'' la'' sol'' fad'') r re'''( mi''' fad''' mi''' re''') |
    sol'''4 r r2 |
  }
  \tag #'violino2 {
    si16[ re'] sol' re' sol' si re'8 fad'16 sol' la' sol' la' fad' |
    sol'8 si16[ re'] sol' re' sol' si re'8 fad'16 sol' la' sol' la' fad' |
    r8 si16 la sol la sol la si2 |
    do'4 r16*2/3 mi''( re'' do'' re'' mi'') r la'( sol' fad' sol' la') r fad''( sol'' la'' sol'' fad'') |
    sol''4 r r2 |
  }
>>
