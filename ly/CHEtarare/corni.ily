\clef "treble" \transposition sol
r8 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 }
  { mi' }
  { s-\sug\f }
>> r2 |
r8. <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { sol'16 sol'8. sol'16 sol'4 }
  { mi'16 mi'8. mi'16 mi'4 }
>> r4 |
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { do''4 }
  { do'' }
>> r4 r2 |
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { do''4 }
  { sol'4 }
>> r4 r2 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 s re'' s | re'' }
  { sol' s do'' s | sol' }
  { s4-\sug\f r s r }
>> r4 r8 \twoVoices #'(corno1 corno2 corni) <<
  { re''8 re'' re'' |
    mi''4. mi''8 re'' re''16 re'' re''8 re'' |
    sol'8 sol''16 sol'' sol''8 sol'' sol''4 }
  { sol'8 sol' sol' |
    do''4. do''8 re'' re''16 re'' re''8 re'' |
    sol' sol'16 sol' sol'8 sol' sol'4 }
>> r4 |
\tag #'corni <>^"a 2" do''1~ |
do'' |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2 sol' | do''4 }
  { re''2 sol' | mi'4 }
>> r4 r8 \twoVoices #'(corno1 corno2 corni) <<
  { re''8 re'' re'' | mi''4 }
  { sol'8 sol' sol' | do''4 }
>> r4 r8 \twoVoices #'(corno1 corno2 corni) <<
  { re''8 re'' re'' | do''1 | }
  { sol'8 sol' sol' | do'1 | }
>>
r2 r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''4 | do'' }
  { sol'4 | mi' }
>> r4 r2 |
