\score {
  \new GrandStaff \with {
    instrumentName = \markup\center-column { Cors \small en Sol }
  } <<
    \new Staff <<
      \keepWithTag #'() \global
      \keepWithTag #'corno1 \includeNotes "corni"
    >>
    \new Staff <<
      \keepWithTag #'() \global
      \keepWithTag #'corno2 \includeNotes "corni"
    >>
  >>
  \layout { indent = \largeindent }
}
