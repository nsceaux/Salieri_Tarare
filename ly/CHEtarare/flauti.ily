\clef "treble" r8 |
R1*11 |
r2 r8 \twoVoices #'(flauto1 flauto2 flauti) <<
  { la''16-. si''-. do'''-. si''-. do'''-. la''-. | si''4\trill }
  { fad''16-. sol''-. la''-. sol''-. la''-. fad''-. | sol''4\trill }
>> r4 r8 \twoVoices #'(flauto1 flauto2 flauti) <<
  { la''16 si'' do''' si'' do''' la'' | si''4\trill }
  { fad''16 sol'' la'' sol'' la'' fad'' | sol''4\trill }
>> r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { r16 sol''-. si''-. la''-. sol''-. fad''-. mi''-. re''-. |
    r16*2/3 mi''( fad'' sol'' fad'' mi'') r do'''( re''' mi''' re''' do''') r fad''( sol'' la'' sol'' fad'') r re'''( mi''' fad''' mi''' re''') |
    sol'''4 }
  { r2 |
    r r4 r16*2/3 fad''( sol'' la'' sol'' fad'') |
    sol''4 }
>> r4 r2 |
