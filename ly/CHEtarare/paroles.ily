Ap -- prends, fils or -- gueil -- leux des prê -- tres !
qu’é -- le -- vé par -- mi les sol -- dats,
Ta -- rare a -- vait, au lieu d’an -- cê -- tres,
dé -- jà vain -- cu dans cent com -- bats ;
qu’Al -- ta -- mort en -- fant, dans la plai -- ne,
pour -- sui -- vait les fleurs des char -- dons,
que les zé -- phyrs, de leur ha -- lei -- ne,
font vo -- ler au som -- met des monts.
