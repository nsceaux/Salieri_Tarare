\clef "alto" r8 |
sol'2\f r |
r8. sol16\f sol8. sol16 sol4 r |
do'4\f r r2 |
si4\f r r2 |
fad'4-\sug\f r mi' r |
re' r r re' |
sol' mi' la' la |
re'8 re'16 re' re'8 re' re'4 si |
do' r do' r |
si r si r |
la r re r |
sol r re' r |
sol r re' r |
sol r r2 |
do'4 la re' re' |
sol r r2 |
