\piecePartSpecs
#`((clarinetti #:score-template "score-clarinetti" #:system-count 6)
   (violino1)
   (violino2)
   (viola)
   (basso)
   (reduction)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet#})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#}))
