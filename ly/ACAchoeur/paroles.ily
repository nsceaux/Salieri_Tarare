Quel charme in -- con -- nu nous at -- ti -- re ?
Nos cœurs en sont é -- pa -- nou -- is.
D’un plai -- sir va -- gue je sou -- pi -- re ;
je veux l’ex -- pri -- mer ; je ne puis.
\tag #'vhaute-contre {
  En jou -- is -- sant, je sens que je dé -- si -- re !
}
\tag #'(vdessus basse) {
  je sens que je dé -- si -- re !
}
\tag #'vtaille {
  en dé -- si -- rant, je sens que je jou -- is !
}
\tag #'(vhaute-contre vbasse) {
  je sens que je jou -- is !
}
Quel charme in -- con -- nu nous at -- ti -- re ?
Nos cœurs en sont é -- pa -- nou -- is.
Nos cœurs en sont é -- pa -- nou -- is.
