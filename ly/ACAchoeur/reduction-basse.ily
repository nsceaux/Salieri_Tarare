\clef "bass" r8 |
lab,2.~ |
lab,2 reb4 |
mib mib r |
sol4. \grace { lab16[ sol fa] } sol8( sib sol) |
mib8 sib, sol, sib, mib sol, |
lab,2 fa4 |
<mib sib>4. q8 q q |
lab,2\fermata r4 |
lab,2.~ |
lab,2 reb4 |
mib mib r |
sol4. \grace { lab16[ sol fa] } sol8( sib sol) |
mib8 sib, sol, sib, mib sol, |
lab,2 reb4 |
mib4. mib8 mib mib |
lab,2.\fermata |
lab,4( do mib) |
lab( fa mib) |
<re fa>4. q8 mib mib |
fa2 r4 |
R2.*2 |
<< sib2. \\ { mib4 mib4. mib8 } >> |
lab,2 lab8. sol16 |
fa2 fa8.\f sol16 |
<< lab2.\> { s2 <>\p } >> |
sib2\fermata <sib, fa>4 |
mib2.\fermata |
<mi sol do'>2.:8 |
<fa lab do'>2.:8 |
<sol sib do'>:8 |
<fa lab do'>:8 |
<sol sib do'>:8 |
<fa lab do'>:8 |
r8 fa( re si, sol, fa) |
r8 mib( sol mib do do) |
r8 si,( re sol re si,) |
r do( mib sol do' do) |
r sol,( si, re sol sol,) |
do2\fermata r4 |
lab,2.~ |
lab,2 reb4 |
mib mib r |
sol4. \grace { lab16[ sol fa] } sol8( sib sol) |
mib8 sib, sol, sib, mib sol |
lab2 reb4 |
re4. mib8( mib mib |
fa4.) fa8( fa fa |
mib4.) reb!8 reb reb |
do2 reb4 |
mib4. mib8 mib mib |
<lab, mib>2.~ |
q |
q\fermata |
