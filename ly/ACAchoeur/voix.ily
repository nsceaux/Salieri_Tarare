<<
  \tag #'(vdessus basse) {
    \clef "soprano/treble" r8^\markup\italic Cantabile |
    R2.*7 |
    r4\fermata r r8 <<
      { \voiceOne mib''^\p |
        mib''4 do''4. reb''8 |
        mib''2 fa''8. reb''16 |
        sib'4 sib'
      }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo do''8 |
        do''4 lab'4. sib'8 |
        do''2 reb''8. sib'16 |
        sol'4 sol'
      }
    >> \oneVoice r4 |
    R2. |
    r4 r r8 <<
      { \voiceOne sib'8 |
        do''2 fa''4 |
        sib'4. mib''8 mib'' mib'' |
        do''2.\fermata |
      }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo mib'8 |
        lab'2 lab'4 |
        sol'4. sol'8 sol' sol' |
        lab'2.
      }
    >> \oneVoice
    R2. |
    <<
      { \voiceOne do''4 do'' do'' |
        sib'4. sib'8 sib' mib'' |
        \appoggiatura mib''8 re''4 re'' }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo lab'4 lab' lab' |
        lab'4. lab'8 sol' sol' |
        lab'4 lab'
      }
    >> \oneVoice r4 |
    R2. |
    r4 r r8 <<
      { \voiceOne sib'8 |
        mib''4 mib''4. mib''8 |
        do''2 }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sib'8 |
        sol'4 sol'4. sol'8 |
        lab'2
      }
    >> \oneVoice r4 |
    R2. |
    r4 r <<
      { \voiceOne fa''8 fa'' |
        re''\fermata }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo lab'8 lab' |
        fa'2
      }
    >> \oneVoice r4 |
    R2.^\fermataMarkup |
    R2.*3 |
    r4 r r8 <<
      { \voiceOne do''8 |
        mi''4. mi''8 mi'' mi'' |
        fa''4 do'' }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo do''8 |
        sib'4. sib'8 sib' sib' |
        lab'4 lab'
      }
    >> \oneVoice r4 |
    R2.*5 |
    r4\fermata r r8 <<
      { \voiceOne mib''8 |
        mib''4 do''4. reb''8 |
        mib''2 fa''8. reb''16 |
        sib'4 sib' }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo do''8 |
        do''4 lab'4. sib'8 |
        do''2 reb''8. sib'16 |
        sol'4 sol'
      }
    >> \oneVoice r4 |
    R2. |
    r4 r r8 <<
      { \voiceOne sib'8 |
        do''2 fa''4 |
        sib'4. do''8 do'' do'' |
        reb''2 }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo mib'8 |
        lab'2 lab'4 |
        lab'4. lab'8 lab' lab' |
        lab'2
      }
    >> \oneVoice r4 |
    r r <<
      { \voiceOne mib''4 |
        mib''2 fa''4 |
        sib'4. mib''8 mib'' mib'' |
        do''2. | }
      \new Voice \with { autoBeaming = ##f } {
        \voiceTwo sol'4 |
        lab'2 lab'4 |
        sol'4. sol'8 sol' sol' |
        lab'2. |
      }
    >> \oneVoice
  }
  \tag #'vhaute-contre {
    \clef "alto/G_8" r8 |
    R2.*7 |
    r4\fermata r r8 mib'^\p |
    mib'4 do'4. reb'8 |
    mib'2 fa'8. reb'16 |
    sib4 sib r |
    R2. |
    r4 r r8 sib |
    do'2 fa'4 |
    sib4. mib'8 mib' mib' |
    do'2.\fermata |
    R2. |
    do'4 do' do' |
    sib4. sib8 sib mib' |
    \appoggiatura mib'8 re'4 re' r |
    R2. |
    r4 r r8 sib |
    mib'4 mib'4. mib'8 |
    do'2 r4 |
    R2.*2 |
    r4\fermata r sib8 sib |
    sol2.\fermata |
    do'4 do' do' |
    do'2 r8 do' |
    mi'4. mi'8 mi' mi' |
    fa'4 do' r |
    R2.*5 |
    r4 r r8 do' |
    re'4. re'8 re' sol' |
    mib'2\fermata~ mib'8 mib' |
    mib'4 do'4. reb'8 |
    mib'2 fa'8. reb'16 |
    sib4 sib r |
    R2. |
    r4 r r8 sib |
    do'2 fa'4 |
    sib4. do'8 do' do' |
    reb'2 r4 |
    r r mib' |
    mib'2 fa'4 |
    sib4. mib'8 mib' mib' |
    do'2. |
  }
  \tag #'vtaille {
    \clef "tenor/G_8" r8 |
    R2.*7 |
    r4\fermata r r8 do'8^\p |
    do'4 lab4. sib8 |
    do'2 reb'8. sib16 |
    sol4 sol r |
    R2. |
    r4 r r8 sol |
    lab2 lab4 |
    sol4. sol8 sol sol |
    lab2.\fermata |
    R2. |
    lab4 lab lab |
    lab4. lab8 sol sol |
    lab4 lab r |
    R2. |
    r4 r r8 lab |
    sol4 sol4. sol8 |
    lab2 r4 |
    R2.*2 |
    r4\fermata r re8 re |
    mib2.\fermata |
    R2.*6 |
    si4 si si |
    do'2 r8 do' |
    re'?4. re'8 re' sol' |
    mib'2 r4 |
    R2. |
    r4\fermata r r8 do' |
    do'4 lab4. sib8 |
    do'2 reb'8. sib16 |
    sol4 sol r |
    R2. |
    r4 r r8 mib |
    lab2 lab4 |
    lab4. lab8 lab lab |
    lab2 r4 |
    r r sol |
    lab2 lab4 |
    sol4. sol8 sol sol |
    lab2. |
  }
  \tag #'vbasse {
    \clef "bass/bass" r8 |
    R2.*7 |
    r4\fermata r r8 lab^\p |
    lab4 lab4. lab8 |
    lab2 reb8. reb16 |
    mib4 mib r |
    R2. |
    r4 r r8 sol |
    lab2 reb4 |
    mib4. mib8 mib mib |
    lab2.\fermata |
    R2. |
    lab4 fa mib |
    re4. re8 mib mib |
    fa4 fa r |
    R2. |
    r4 r r8 fa |
    mib4 mib4. mib8 |
    lab2 r4 |
    R2.*2 |
    r4\fermata r sib8 sib |
    mib2.\fermata |
    R2.*9 |
    r4 r r8 do' |
    si4. si8 si si |
    do'2\fermata~ do'8 lab |
    lab4 lab4. lab8 |
    lab2 reb8. reb16 |
    mib4 mib r |
    R2. |
    r4 r r8 sol |
    lab2 reb4 |
    re4. mib8 mib mib |
    fa2 r4 |
    r r reb! |
    do2 reb4 |
    mib4. mib8 mib mib |
    lab,2. |
  }
>>
R2. |
R2.^\fermataMarkup |
