\clef "bass" r8 |
lab,2.~ |
lab,2 reb4 |
mib mib r |
R2. |
r4 r sol,\mf |
lab,2 reb4 |
mib4. mib8 mib mib |
lab,2\fermata r4 |
lab,2.\p~ |
lab,2 reb4 |
mib mib r |
R2. |
r4 r r8 sol,\p |
lab,2 reb4 |
mib4. mib8 mib mib |
lab,2.\fermata |
lab,4(\mf do mib) |
lab(\p fa mib) |
re4. re8 mib mib |
fa2 r4 |
R2.*2 |
mib4\p mib4. mib8 |
lab,2 lab8.\mf sol16 |
fa2 fa8.\f sol16 |
<< lab2.\> { s2 <>\p } >> |
sib2\fermata sib,4 |
mib2.\fermata |
mi8\p mi\cresc mi mi mi mi |
fa2.:8 |
sol:8 |
fa:8 |
sol:8\p fa:8 |
r8 fa( re si, sol, fa) |
r8 mib(\cresc sol mib do do) |
r8 si,( re sol re si,) |
r do( mib sol do' do) |
r sol,(\p si, re sol sol,) |
do2\fermata r4 |
lab,2.~ |
lab,2 reb4 |
mib mib r |
R2. |
r4 r r8 sol\p |
lab2 reb4 |
re4.\mf mib8(\cresc mib mib |
fa4.)\mf fa8( fa fa |
mib4.) reb!8\f reb reb |
do2 reb4\p |
mib4. mib8 mib mib |
lab,2.~ |
lab, |
lab,\fermata |
