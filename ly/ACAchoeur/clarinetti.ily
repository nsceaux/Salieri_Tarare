\clef "soprano/treble"
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { mib''8 | mib''4 do''4. reb''8 | mib''2 fa''8. reb''16 | sib'4 sib' }
  { do''8 | do''4 lab'4. sib'8 | do''2 reb''8. sib'16 | sol'4 sol' }
>> r4 |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { sol''4.\mf \grace { lab''16[ sol'' fa''] } \slurDashed sol''8( sib'' sol'') |
    mib''( sib' sol' sib' mib'8. sib'16) | \slurSolid
    do''2 fa''4 |
    sib'4. mib''8 mib'' mib'' |
    do''2\fermata }
  { R2. |
    r4 r r8 mib'8 |
    mib'4 lab' lab' |
    sol'4. sol'8 sol' sol' |
    lab'2\fermata }
>> r8 \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { mib''8 | mib''4 do''4. reb''8 | mib''2 fa''8. reb''16 | sib'4 sib' }
  { do''8 | do''4 lab'4. sib'8 | do''2 reb''8. sib'16 | sol'4 sol' }
  { s8\p }
>> r4 |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { sol''4.\mf \grace { lab''16[ sol'' fa''] } \slurDashed sol''8( sib'' sol'') |
    mib''( sib' sol' sib' mib' sib') | \slurSolid
    do''2 fa''4 |
    sib'4. mib''8 mib'' mib'' |
    do''2.\fermata |
    do'4( mib' lab') |
    do'' do'' do'' |
    sib'4. sib'8 sib' mib'' |
    \appoggiatura mib''8 re''4 re'' \grace { mib''16[ re'' do''] } re''8.\mf mib''16 |
    fa''2 \grace { sol''16[ fa'' mib''] } fa''8.\f sol''16 |
    lab''2
  }
  { R2. |
    r4 r r8 mib'8 |
    lab'2 lab'4 |
    sol'4. sol'8 sol' sol' |
    lab'2.\fermata |
    do'4( mib' lab') |
    lab'4 lab' lab' |
    lab'4. lab'8 sol' sol' |
    lab'4 lab' r |
    R2. |
    r4 r }
  { s2. | s2 s8 s\p | s2.*3 | s2.\mf | s\p }
>> r8 \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { sib'8 |
    mib''4 mib''4. mib''8 |
    do''2 \grace { sol''16[ fa'' mib''] } fa''8.\mf sol''16 |
    lab''2 \grace { sib''16[ lab'' sol''] } lab''8. sib''16 |
    do'''4(\> lab'')\! fa''8 fa'' |
    re''2\fermata }
  { sib'8 |
    sol'4 sol'4. sol'8 |
    lab'2 r4 |
    R2. |
    r4 r lab'8 lab' |
    fa'2\fermata }
  { s8\p | s2.*3 | s2 s4\p | }
>> r4 |
R2.^\fermataMarkup |
R2.*11 | \allowPageTurn
r4\fermata r r8 \twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { mib''8 | mib''4 do''4. reb''8 | mib''2 fa''8. reb''16 | sib'4 sib' }
  { do''8 | do''4 lab'4. sib'8 | do''2 reb''8. sib'16 | sol'4 sol' }
>> r4 |
\twoVoices #'(clarinetto1 clarinetto2 clarinetti) <<
  { sol''4.\mf \grace { lab''16[ sol'' fa''] } \slurDashed sol''8( sib'' sol'') |
    mib''( sib' sol' sib' mib' sib') | \slurSolid
    do''2 fa''4 |
    sib'4.( do''8 do'' do'') |
    reb''4.( re''8 re'' re'') |
    mib''8 sol''4 sol''8 sol'' sol'' |
    lab''2 fa''4 |
    sib'4. mib''8 mib'' mib'' |
    do''2.~ |
    do'' |
    do''\fermata | }
  { R2. |
    r4 r r8 mib' |
    lab'2 lab'4 |
    lab'4.( lab'8 lab' lab') |
    lab'4.( lab'8 lab' lab') |
    sol' sib'4 sib'8 sib' sib' |
    lab'2 lab'4 |
    sol'4. sol'8 sol' sol' |
    lab'2.~ |
    lab' |
    lab'\fermata | }
  { s2. | s2 s8 s\p | s2. | s4.\mf s\cresc | s2.\mf | s8 s\f s2 | s s4\p | }
>>
