\clef "treble" <do'' mib''>8 |
<do'' mib''>4 <lab' do''>4. <sib' reb''>8 |
<do'' mib''>2 <reb'' fa''>8. <sib' reb''>16 |
<sol' sib'>4 q r |
sol''4.\mf \grace { lab''16[ sol'' fa''] } sol''8( sib'' sol'') |
mib''( sib' sol' sib' mib'8. sib'16) |
<< { do''2 fa''4 } \\ { mib' lab' lab' } >> |
<sol' sib'>4. <sol' mib''>8 q q |
<lab' do''>2\fermata r8 <do'' mib''>\p |
<do'' mib''>4 <lab' do''>4. <sib' reb''>8 |
<do'' mib''>2 <reb'' fa''>8. <sib' reb''>16 |
<sol' sib'>4 q r |
sol''4.\mf \grace { lab''16[ sol'' fa''] } sol''8( sib'' sol'') |
mib''( sib' sol' sib' mib'8. sib'16) |
<lab' do''>2\p <lab' fa''>4 |
<sol' sib'>4. <sol' mib''>8 q q |
<lab' do''>2.\fermata |
do'4(\mf mib' lab') |
<lab' do''>4\p q q |
<lab' sib'>4. q8 <sol' sib'> <sol' mib''> |
\appoggiatura mib''8 <re'' lab'>2 \grace { mib''16[ re'' do''] } re''8.\mf mib''16 |
fa''2 \grace { sol''16[ fa'' mib''] } fa''8.\f sol''16 |
lab''2 r8 <sib' lab'>\p |
<sol' mib''>4 q4. q8 |
<lab' do''>2 \grace { sol''16[ fa'' mib''] } <do'' fa''>8.\mf sol''16 |
<do'' lab''>2 \grace { sib''16[ lab'' sol''] } lab''8.\f sib''16 |
do'''4\> lab'' <lab' fa''>8\p q |
<fa' re''>2\fermata <sib re'>4 |
<mib' sol>2.\fermata |
r8\p do'(\cresc mi' sol' do'') do'' |
r8 do'( fa' lab' do'') do'' |
r mi'( sol' do'' mi'') sol'' |
r8 do''( lab'' fa'' do'' lab') |
sib'(\pp sol' mi' do' sib do') |
lab( do' fa' lab' do'') do'' |
<sol' re' si'>8 q4 q8 q q |
<mib' sol' do''>\cresc q4 q8 q q |
<sol' si' re''> q4 q8 q q |
<sol' do''mib''> q4 q8 q q |
<sol' si' re''>\p q4 q8 q q |
<lab' do'' mib''>2\fermata r8 <do'' mib''> |
<do'' mib''>4 <lab' do''>4. <sib' reb''>8 |
<do'' mib''>2 <reb'' fa''>8. <sib' reb''>16 |
<sol' sib'>4 q r |
sol''4.\mf \grace { lab''16[ sol'' fa''] } sol''8( sib'' sol'') |
mib''( sib' sol' sib' mib'8.) sib'16\p |
<lab' do''>2 <lab' fa''>4 |
<lab' sib'>4.(\mf <lab' do''>8\cresc q q) |
<lab' reb''>4.(\mf <lab' re''>8 q q) |
<sol' mib''>8 <sib' sol''>4\f q8 <sib' sol'' mib''> q |
<mib'' lab''>2 <lab' fa''>4\p |
<sol' sib'>4. <sol' mib''>8 q q |
<lab' do''>2.~ |
q |
q\fermata |
