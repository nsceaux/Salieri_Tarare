\clef "treble" <>^\markup\whiteout "avec la sourdine"
<<
  \tag #'violino1 {
    mib''8 |
    mib''4 do''4. reb''8 |
    mib''2 fa''8. reb''16 |
    sib'4 sib' r |
    sol''4.\mf \grace { lab''16[ sol'' fa''] } sol''8( sib'' sol'') |
    mib''( sib' sol' sib' mib'8. sib'16) |
    do''2 fa''4 |
    sib'4. mib''8 mib'' mib'' |
    do''2\fermata r8 mib''\p |
    mib''4 do''4. reb''8 |
    mib''2 fa''8. reb''16 |
    sib'4 sib' r |
    sol''4.\mf \grace { lab''16[ sol'' fa''] } sol''8( sib'' sol'') |
    mib''( sib' sol' sib' mib'8. sib'16) |
    do''2\p fa''4 |
    sib'4. mib''8 mib'' mib'' |
    do''2.\fermata |
  }
  \tag #'violino2 {
    do''8 |
    do''4 lab'4. sib'8 |
    do''2 reb''8. sib'16 |
    sol'4 sol' r |
    sol'4.\mf \grace { lab'16[ sol' fa'] } sol'8( sib' sol') |
    mib'( sib sol sib mib' mib') |
    mib'( do' lab) lab' lab' lab' |
    sol'4. sol'8 sol' sol' |
    lab'2\fermata r8 do''\p |
    do''4 lab'4. sib'8 |
    do''2 reb''8. sib'16 |
    sol'4 sol' r |
    sol'4.\mf \grace { lab'16[ sol' fa'] } sol'8( sib' sol') |
    mib'( sib sol sib) << mib'4~ { s8 <>\p } >> |
    mib'8( do' lab) lab' lab' lab' |
    sol'4. sol'8 sol' sol' |
    lab'2. |
  }
>>
do'4(\mf mib' lab') |
<<
  \tag #'violino1 {
    do''2.\p |
    sib'4. sib'8 sib' mib'' |
    re''2 \grace { mib''16[ re'' do''] } re''8.\mf mib''16 |
    fa''2 \grace { sol''16[ fa'' mib''] } fa''8.\f sol''16 |
    lab''2 r8 sib'\p |
    mib''4 mib''4. mib''8 |
    do''2 \grace { sol''16[ fa'' mib''] } fa''8.\mf sol''16 |
    lab''2 \grace { sib''16[ lab'' sol''] } lab''8.\f sib''16 |
    do'''4(\> lab'' fa'')\p |
    re''2\fermata sib4 |
    sol2.\fermata |
    r8 do'\p( mi' sol'\cresc do'') do'' |
    r8 do'( fa' lab' do'') do'' |
    r mi'( sol' do'' mi'') sol'' |
    r8 do''( lab'' fa'' do'' lab') |
    sib'(\pp sol' mi' do' sib do') |
    lab( do' fa' lab' do'') do'' |
    si'8 si'4 si'8 si' si' |
    do''\cresc do''4 do''8 do'' do'' |
    re'' re''4 re''8 re'' re'' |
    mib'' mib''4 mib''8 mib'' mib'' |
    re''\p re''4 re''8 re'' re'' |
    mib''2\fermata r8 mib'' |
    mib''4 do''4. reb''8 |
    mib''2 fa''8. reb''16 |
    sib'4 sib' r |
    sol''4.\mf \grace { lab''16[ sol'' fa''] } sol''8( sib'' sol'') |
    mib''( sib' sol' sib' mib'8.) sib'16\p |
    do''2 fa''4 |
    sib'4.(\mf do''8\cresc do'' do'') |
    reb''4.(\mf re''8 re'' re'') |
    mib''8 sol''4\f sol''8 sol'' sol'' |
    lab''2 fa''4\p |
    sib'4. mib''8 mib'' mib'' |
    do''2.~ |
    do'' |
    do''\fermata |
  }
  \tag #'violino2 {
    do''4\p lab' lab' |
    lab'4. lab'8 sol' sol' |
    lab'2 \grace { mib'16[ re' do'] } re'8.\mf mib'16 |
    fa'2 \grace { sol'16[ fa' mib'] } fa'8. sol'16 |
    lab'2 r8 lab'\p |
    sol'4 sol'4. sol'8 |
    lab'2 \grace { sol'16[ fa' mib'] } fa'8.\mf sol'16 |
    lab'2 \grace { sib'16[ lab' sol'] } lab'8.\f sib'16 |
    do''4\>( lab' fa'\p) |
    re'2\fermata re'4 |
    mib'2.\fermata |
    sol8\p-.( sol\cresc-. sol-. sol-. sol-. sol-.) |
    lab2.:8 ^"[simile]" |
    sib:8 |
    lab:8 |
    sib:8\p |
    lab:8 |
    sol8 sol'4 sol'8 sol' sol' |
    sol'\cresc sol'4 sol'8 sol' sol' |
    sol' sol'4 sol'8 sol' sol' |
    sol' sol'4 sol'8 sol' sol' |
    si'\p si'4 si'8 si' si' |
    do''2\fermata r8 do'' |
    do''4 lab'4. sib'8 |
    do''2 reb''8. sib'16 |
    sol'4 sol' r |
    sol'4.\mf \grace { lab'16[ sol' fa'] } sol'8( sib' sol') |
    mib'( sib sol sib) mib'4\p |
    mib'8( do' lab) lab' lab' lab' |
    lab'4.\mf( lab'8\cresc lab' lab') |
    lab'4.\mf( lab'8 lab' lab') |
    sol' sib'4\f sib'8 sib' sib' |
    lab'2 lab'4\p |
    sol'4. sol'8 sol' sol' |
    lab'2.~ |
    lab' |
    lab'\fermata |
  }
>>
