\clef "alto" <<
  { mib'8 | mib'4 do'4. reb'8 | mib'2 fa'8. reb'16 | sib4 sib } \\
  { do'8 | do'4 lab4. sib8 | do'2 reb'8. sib16 | sol4 sol }
>> r4 |
R2. |
r4 r r8 <<
  { sib8 | do'2 fa'4 | sib4. sib8 sib sib | do'2\fermata } \\
  { mib8\mf | lab2. | sol4. sol8 sol sol | lab2\fermata }
>> r8 <<
  { mib'8 | mib'4 do'4. reb'8 | mib'2 fa'8. reb'16 | sib4 sib } \\
  { do'8\p | do'4 lab4. sib8 | do'2 reb'8. sib16 | sol4 sol }
>> r4 |
R2. |
r4 r r8 <<
  { sib8 | do'2 fa'4 | sib4. mib'8 mib' mib' | do'2.\fermata | } \\
  { mib8\p | lab2 lab4 | sol4. sol8 sol sol | lab2.\fermata | }
>>
R2. |
r4 re'?\p( mib'? |
fa'4.) fa'8 mib' mib' |
fa'2 r4 |
R2.*2 |
sib2.\p |
do'2 do'4\mf |
do'2 fa8.\f sol16 |
lab2\> lab'4\p |
fa'2\fermata fa4 |
mib2.\fermata |
do'8\p(-.\cresc do'-. do'-. do'-. do'-. do'-.) |
do'2.:8 ^"[simile]" |
do':8 |
do':8 |
do':8\p |
do':8 |
re'8 re'4 re'8 re' re' |
mib'\cresc mib'4 mib'8 mib' mib' |
si8 si4 si8 si si |
do' do'4 do'8 do' do' |
sol\p sol'4 sol'8 sol' sol' |
sol'2\fermata r8 <<
  { mib'8 | mib'4 do'4. reb'8 | mib'2 fa'8. reb'16 | sib4 sib } \\
  { do'8 | do'4 lab4. sib8 | do'2 reb'8. sib16 | sol4 sol }
>> r4 |
R2. |
r4 r r8 <<
  { sib8 | do'2 fa'4 | sib4. do'8 do' do' | reb'4. re'8 re' re' |
    mib' sol'4 sol'8 sol' sol' | lab'2 fa'4 | sib4. mib'8 mib' mib' |
    mib'2.~ | mib' | } \\
  { mib8\p | lab2 lab4 | lab4.\mf lab8\cresc lab lab | lab4.\mf lab8 lab lab |
    sol sib4\f sib8 sib sib | lab2 lab4\p | sol4. sol8 sib mib' |
    mib'2.~ | mib' }
>>
mib'2.\fermata |
