\clef "bass" re2\repeatTie r |
R1*2 |
sol4 r r2 |
r4 la, sib,2 |
R1*4 |
lab4 r r2 |
sol4 r r2 |
lab4 r r2 |
lab,1~ |
lab,~ |
lab,2 sol,4 r |
R1*5 |
