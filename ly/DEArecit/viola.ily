\clef "alto" fad2 r |
R1*2 |
sol4 r r2 |
r4 la sib2 |
R1*4 |
lab4 r r2 |
sol4 r r2 |
lab4 r r2 |
r8 do'4~ do'8 do'2~ |
do'1~ |
do'2 si!4 r |
R1*5 |
