\clef "alto/G_8" <>^\markup\character-text Calpigi s’écrie ^"parlé"
r8 re'16 re' fad'4 fad'8 r
\ffclef "tenor/G_8" <>^\markup\character-text Tarare avec un grand trouble
r8 re'16 re' |
re'4 la8 la fad fad r la16 la |
re'4 r do'8 do' do' re' |
sib4 r8 sol'16 sol' dod'8. la16 dod'8 dod'16 re' |
la4 r r2 |
\ffclef "alto/G_8" <>^\markup\character Calpigi
sib4 sib8 do' re'4. re'8 |
sib sib r re' re' re' mib' fa' |
sib sib r16 sib sib sib sib8 sib sib do' |
re'4
\ffclef "tenor/G_8" <>^\markup\character-text Tarare troublé
r8 re'16 mib' fa'4 fa'8 fa' |
re'4 r8 re' re' re' re' mib' |
sib4 r8 mib'16 mib' mib'4 reb'8 mib' |
do'4 r8 lab lab lab lab sib |
do' do' r4 r r8 do' |
do' do' do' do' fa'4 r8 do'16 do' |
do'4 do'8 si! re'8 re' r4 |
\ffclef "alto/G_8" <>^\markup\character Calpigi
re'4 re'8 re' si4 r8 sol |
sol sol sol la si4 si8 si16 do' |
re'4 r8 re' re' re' re' re' |
si4 r16 re' re' mib' fa'4 r |
re'8 re'16 re' re'8 mib' do' do' r4 |
