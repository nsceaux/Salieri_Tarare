\clef "treble"
<<
  \tag #'violino1 { re'2 }
  \tag #'violino2 { la2 }
>> r2 |
R1*2 |
<<
  \tag #'violino1 { re'4 }
  \tag #'violino2 { sib4 }
>> r4 r2 |
r4 la sib2 |
R1*4 |
<<
  \tag #'violino1 { re''4 }
  \tag #'violino2 { fa' }
>> r4 r2 |
<<
  \tag #'violino1 { mib''4 }
  \tag #'violino2 { sib' }
>> r4 r2 |
<<
  \tag #'violino1 { do''4 }
  \tag #'violino2 { mib' }
>> r4 r2 |
r8 <<
  \tag #'violino1 {
    mib''4( mi''8) fa''2~ |
    fa''1~ |
    fa''2 re''4
  }
  \tag #'violino2 {
    mib'4( mi'8) fa'2~ |
    fa'1~ |
    fa'2 sol'4
  }
>> r4 |
R1*5 |
