Ô Ta -- ra -- re !

Ô fu -- reur que j’a -- bhor -- re !
Mon a -- mi… s’il n’eût pas par -- lé,
de ma main é -- tait im -- mo -- lé !

Tu le de -- vais, Ta -- ra -- re ! il le fau -- drait en -- co -- re,
si quelque es -- cla -- ve cu -- ri -- eux…

Mil -- le cris de mon nom font re -- ten -- tir ces lieux !
Je me crois dé -- cou -- vert, et que la ja -- lou -- si -- e…
mou -- rir sans la re -- voir, et si près d’As -- ta -- si -- e !…

O mon hé -- ros ! tes vê -- te -- ments mouil -- lés,
d’al -- gues im -- purs et de li -- mon souil -- lés !…
un grand pé -- ril a me -- na -- cé ta vi -- e !
