\piecePartSpecs
#`((violino1 #:score-template "score-part-voix"
             #:indent 0 #:music ,#{ s2 <>^"Sourdine" #})
   (violino2 #:score-template "score-part-voix"
             #:indent 0 #:music ,#{ s2 <>^"Sourdine" #})
   (viola #:score-template "score-part-voix"
             #:indent 0 #:music ,#{ s2 <>^"Sourdine" #})
   (basso #:score-template "score-part-voix"
             #:indent 0 #:music ,#{ s2 <>^"Sourdine" #})
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
