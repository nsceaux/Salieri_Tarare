\clef "treble"
<<
  \tag #'violino1 {
    r8. do'''16\f do'''8. do'''16 |
    la''4 la''16 sib'' la'' sib'' do'''8 do''' do''' do''' |
    re'''4. fa'''8 mi''' re''' do''' sib'' |
    la''2. sol''4 |
    fa'' fa''16\ff sol'' la'' sol'' fa'' sol'' la'' sol'' fa'' sol'' la'' sol'' |
    fa''4 <fa' la>16 q q q q8 la''[ do''' la''] |
    sol''4.\fp sol''8 do'''4.\fp do'''8 |
    fad''\f \once\slurDashed mib'(-\sug\sf re') re' re'4 fad''8\p sol'' |
    la''(\f sold'' la'' sib'') do'''(\cresc si'' do''' mib''') |
    re'''2.\!( do'''4) |
    sib''2 r8. sib''16-\sug\f sib''8. sol''16 |
    la''4. la''8 re'''8. re'''16 re'''8. re'''16 |
    dod'''4
  }
  \tag #'violino2 {
    r8. do''16-\sug\f do''8. do''16 |
    do''4 fa'16 sol' fa' sol' la'8 la' la' la' |
    sib'4. re''8 do'' sib' la' sol' |
    fa'2. mi'4 |
    fa' la'16-\sug\ff sib' do'' sib' la' sib' do'' sib' la' sib' do'' sib' |
    la'4 <la fa'>16 q q q q4 r |
    r8 do'16 mi' sol'4. sol'16 mib' do'8 sol' |
    la' mib'(-\sug\sf re') re' re'4 re'8-\sug\p mi' |
    fad'-\sug\f( mi' fad' sol') la'-\sug\cresc( sold' la' sol') |
    fad'( la' fad' la' fad' la' fad' la')\! |
    sol'2 r8. re''16-\sug\f re''8. re''16 |
    re''4. re''8 sold''8. sold''16 sold''8. sold''16 |
    la''4
  }
>> <la'' dod'' mi'>4\ff la':16 sol':16 |
fa'4. la''8 <<
  \tag #'violino1 {
    re'''4.\fp re'''8 |
    mi''!2\fp si''4.\fp re'''8 |
    do'''2\f r8 la'' si''8. dod'''16 |
    re'''2~ re'''8. re'''16 mi'''8. fa'''16 |
    mi'''2:16\f mi''':16 |
    la''2\fermata r8. do'''16\p do'''8. do'''16 |
    la''4 la''16( sib'' la'' sib'') do'''4 do''' |
    re'''4. fa'''8 mi''' re''' do''' sib'' |
    la''2. sol''4 |
    la'' la''16\f sib'' la'' sib'' do'''2:8 |
    re'''4. fa'''8 mi''' re''' do''' sib'' |
    la''2.:16 sol''4:16 |
    fa''8-\sug\p fa'4^"Serré" fa' fa'' mi''8 |
    re''8\cresc <la' la''>4 q q q8 |
  }
  \tag #'violino2 {
    la'4.-\sug\fp la'8 |
    sold'8 mi'16[\f sold'] si'4. si'16 sold' mi'8 mi' |
    mi' do'16 mi' la'8 mi'16 la' do''8. dod''16 re''8. mi''16 |
    la'8 fa'16[ sol'] la' sol' fa' mi' re'8. la''16 la''8. la''16 |
    <la'' do''>2:16-\sug\f <sold'' si'?>:16 |
    <la' la''>2\fermata r8. la'16-\sug\p la'8. la'16 |
    fa'4 fa'16( sol' fa' sol') la'4 la' |
    sib'4. re''8 do'' sib' la' sol' |
    fa'2. mi'4 |
    fa' fa'16-\sug\f sol' fa' sol' la'2:8 |
    sib'8 fa' sib' re'' do'' sib' la' sol' |
    fa'16 fa'' fa'' fa'' fa''2:16 mi''4:16 |
    fa''8-\sug\p la4 la la' la'8 |
    la'-\sug\cresc <re'' re'>4 q q q8\! |
  }
>>
<si' sold''>8\ff q4 q q q8 |
q4. <<
  \tag #'violino1 {
    si''8 si''4. mi'''8 |
    mi'''2 r |
    r8. mi'16 sold'8. si'16 mi''8. re''16 \grace mi''8 re'' dod''16 re'' |
    dod''4 r r2 |
    dod''4 r r2 |
    r dod''4 r |
    R1 |
    r2 r8 la''( si'' dod''') |
    re'''4. re'''8 re'''( dod''' si'' la'') |
    sold''2~ sold''8 si''( dod''' re''') |
    dod'''4 dod'''16( re''' mi''' re''') dod'''4 dod''' |
    re'''4. dod'''8 si''4. mi'''8 |
    la''4
  }
  \tag #'violino2 {
    <si' sold''>8 q4. q8 |
    q2 r |
    r8. mi'16 sold'8. si'16 mi''4 sold' |
    la' r r2 |
    sold'4 r r2 |
    r2 <la' la>4 r |
    R1 |
    r2 r8 la'( si' dod'') |
    re''8-\sug\p re''4 re'' re'' re''8~ |
    re''4 re''16\mf( dod'' si' dod'') re''8 sold'([ la' si']) |
    la'4 la'16( si' dod'' si') la'4 la' |
    la'2. sold'4 |
    la'
  }
>>
