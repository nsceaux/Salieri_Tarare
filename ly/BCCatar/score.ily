\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \flautiInstr } <<
        \global \includeNotes "flauti"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors in Fa }
        shortInstrumentName = "Cor"
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroupNoBar <<
      \new StaffGroupNoBracket <<
        \new GrandStaff \with { \violiniInstr } <<
          \new Staff \with { \consists "Metronome_mark_engraver" } <<
            \global \keepWithTag #'violino1 \includeNotes "violini"
          >>
          \new Staff <<
            \global \keepWithTag #'violino2 \includeNotes "violini"
          >>
        >>
        \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      >>
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \keepWithTag #'recit \includeNotes "voix"
      >> \keepWithTag #'recit \includeLyrics "paroles"
      \new Staff \with { \haraKiri } \withLyrics <<
        \global \keepWithTag #'atar \includeNotes "voix"
      >> \keepWithTag #'atar \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\center-column { Bassons Basses }
        shortInstrumentName = \markup\center-column { Bn. B. }
        \consists "Metronome_mark_engraver"
      } <<
        \global \keepWithTag #'basso \includeNotes "basso"
        \origLayout {
          s2 s1*4\break s1*5\pageBreak
          s1*5\break s1*5\pageBreak
          s1*7\break s1*5\break s1*4\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
