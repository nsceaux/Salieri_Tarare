\clef "alto" r2 |
fa'8-\sug\f fa' fa' fa' fa'2:8 |
sib:8 sib:8 |
do':8 do':8 |
fa'4 fa'\ff fa' fa' |
fa' fa' fa' fa' |
mi'4.\fp mi'8 mib'4.\fp mib'8 |
re' \once\slurDashed mib''[(\sf re'') re''] re''4 r |
R1 |
r4 re'-\sug\p re' re' |
sol'2 r8. sol'16\f sol'8. sol'16 |
fa'!4. fa'8 sib4 sib' |
la' la\ff la' sol' |
fa'4. fa'8 fa'4.\fp fa'8 |
mi'!4.\fp mi'8 sold'4.\fp sold'8 |
la'2.\f sol'!4 |
fa'8 fa'16 sol' la' sol' fa' mi' re'8. fa'16 mi'8. re'16 |
mi'2:8\f mi':8 |
la'2\fermata r |
r4 fa'8\p fa' fa'4 fa' |
sib sib sib sib |
do'2:8 do':8 |
fa':8-\sug\f fa':8 |
sib:8 sib:8 |
do':8 do':8 |
fa'4\p fa' fa' fa' |
fa'-\sug\cresc fa' fa' fa' |
mi'(\ff fa' mi' fa') |
mi'4. mi'8 mi'4. mi'8 |
mi'2 r |
mi'2 r4 mi' |
mi' r r2 |
mid4 r r2 |
r fad4 r |
R1*2 |
la2.-\sug\p fad'4 |
si \once\slurDashed si16(-\sug\mf dod' re' dod') si8 mi'4 mi'8 |
mi'2. la4 |
fad'4. mi'8 re'4 mi' |
dod'4