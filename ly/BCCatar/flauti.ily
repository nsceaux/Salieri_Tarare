\clef "treble" r2 |
R1*34 |
r2 <>_\markup\italic { une flûte col Viol \concat { 1 \super o } } r8 la''( si'' dod''') |
re'''4. re'''8 re'''( dod''' si'' la'') |
sold''2~ sold''8 si''( dod''' re''') |
dod'''4 dod'''16( re''' mi''' re''') dod'''4 dod''' |
re'''4. dod'''8 si''4. mi'''8 |
la''4
