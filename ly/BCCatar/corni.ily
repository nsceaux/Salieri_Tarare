\clef "treble" \transposition fa
r2 |
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { do''4 mi''8. fa''16 sol''4 }
  { mi'4 do''8. re''16 mi''4 }
>> r |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''2. re''4 |
    do'' do'' do'' do'' |
    do'' do''8. do''16 do''4 }
  { do''2. sol'4 |
    mi' mi' mi' mi' |
    mi' mi'8. mi'16 mi'4 }
  { s1 s4 <>-\sug\ff }
>> r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2 re'' | mi'' }
  { sol'2 re'' | mi'' }
  { s2-\sug\fp s2-\sug\fp }
>> r |
R1*2 |
r2 r8. <>\f \twoVoices #'(corno1 corno2 corni) <<
  { re''16 re''8. re''16 |
    do''4. do''8 do''4 do'' |
    mi'' mi''2 re''4 }
  { re''16 re''8. re''16 |
    do''4. do''8 do''4 do'' |
    mi'' mi''2 re''4 }
  { s16 s4 s1 s4 s2-\sug\ff }
>>
R1*6 |
r4 <>-\sug\p \twoVoices #'(corno1 corno2 corni) <<
  { mi''8. fa''16 sol''4 }
  { do''8. re''16 mi''4 }
>> r4 |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''2. re''4 | }
  { do''2. sol'4 | }
>>
r4 <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { mi''8. fa''16 sol''4 }
  { do''8. re''16 mi''4 }
>> r4 |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''2. re''4 | do''1( | mi'') | }
  { do''2. sol'4 | mi'1 | do'' | }
  { s1 | s-\sug\p | s1\f }
>>
R1*13 |
r4
