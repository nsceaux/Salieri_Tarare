\tag #'(atar basse) {
  Je suis heu -- reux, vous ê -- tes ra -- ni -- mé -- e.
  Un lâche es -- cla -- ve par ses cris,
  m’a -- lar -- mait sur ma bien- ai -- mé -- e ;
  de son vil sang la terre est ar -- ro -- sé -- e :
  un coup de poi -- gnard est le prix
  de la fray -- eur __ qu’il m’a cau -- sé -- e.
  Je suis heu -- reux, vous ê -- tes ra -- ni -- mé -- e,
  vous ê -- tes
  \tag #'atar { ra -- ni -- mé -- e. }
  \tag #'basse { ra -- ni -- mé - }
}
\tag #'(recit basse) {
  Ô Ta -- ra -- re ! ô Bra -- ma ! Bra -- ma !
  
  Dans le sé -- rail qu’on la trans -- por -- te :
  que cent eu -- nu -- ques, à sa por -- te,
  at -- ten -- dent les or -- dres d’Ir -- za.

  C’est le doux nom qu’à ma bel -- le j’im -- po -- se ;
  c’est mon Ir -- za, plus fraî -- che que la ro -- se
  que je te -- nais lor -- qu’el -- le m’em -- bra -- sa.
}
