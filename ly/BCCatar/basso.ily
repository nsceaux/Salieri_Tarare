\clef "bass" r2 |
fa2:8\f fa:8 |
sib,:8 sib,:8 |
do:8 do:8 |
fa4 fa\ff fa fa |
fa fa fa fa |
mi4.\fp mi8 mib4.\fp mib8 |
re mib'[(\sf re') re'] re'4 r |
R1 |
r4 re\p re re |
sol2 r8. sol16-\sug\f sol8. sol16 |
fa!4. fa8 sib,4 sib |
la la,\ff la sol |
fa4. fa8 fa4.\fp fa8 |
mi!4.\fp mi8 sold4.\fp sold8 |
la2.\f sol!4 |
fa8 fa16 sol la sol fa mi re8. fa16 mi8. re16 |
mi2:8\f mi:8 |
la2\fermata r |
r4 fa8\p fa fa4 fa |
sib, sib, sib, sib, |
do2:8 do:8 |
fa:8\f fa:8 |
sib,:8 sib,:8 |
do:8 do:8 |
fa4\p fa fa fa |
fa\cresc fa fa fa |
mi(\ff fa mi fa) |
mi4. mi8 mi4. mi8 |
mi2 r |
mi2 r4 mi |
la r r2 |
mid4 r r2 |
r fad4 r |
R1*2 |
fad4\p fad fad re |
mi mi mi mi |
la,2. la4 |
fad4. mi8 re4 mi |
la,
