\piecePartSpecs
#`((violino1 #:score-template "score-part-voix" #:system-count 9)
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (flauti #:score-template "score-part-voix")
   (corni #:tag-notes corni
          #:tag-global ()
          #:instrument "Cors en Fa"
          #:score-template "score-part-voix")
   (fagotti #:notes "basso"
            #:score-template "score-part-voix")
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#}))
