\tag #'all \key fa \major
\tempo "Allegro animé" \midiTempo#120
\time 4/4 \partial 2 s2 s1*28
\time 4/4 \tag #'all \key do \major s1*6
\tempo "Andante" \midiTempo#100 s1*5 s4
