<<
  \tag #'(atar basse) {
    \clef "bass" <>^\markup\character Atar
    r8. do'16 do'8. do'16 |
    la2 r4 do' |
    re'4. fa'8 mi'[ re'] do'[ sib] |
    la2.\melisma sol4\melismaEnd |
    fa2 r |
    r r8 la do' la |
    sol4. sol8 do'4. do'8 |
    fad2 r4 fad8 sol |
    la2 do'8 si do' mib' |
    re'2.( do'4) |
    sib2 r8*3/2^\markup\italic Fortement sib8*1/2 sib8*3/2 sol8*1/2 |
    la4. la8 re' re' re'8. re'16 |
    dod'4 la r2 |
    r4 r8 la re'4 re'8 re' |
    mi2 si4. re'8 |
    do'2 r8 la si8*3/2 dod'8*1/2 |
    re'2~ re'8 re' mi' fa' |
    mi'1 |
    la2\fermata r8^\markup\italic Galamment do' do' do' |
    la2 r4 do' |
    re'4. fa'8 mi'[ re'] do'[ sib] |
    la2.\melisma sol4\melismaEnd |
    la2 r4 do' |
    re'4. fa'8 mi'[ re'] do'[ sib] |
    <<
      \tag #'basse {
        la2
      }
      \tag #'atar {
        la2.( sol4) |
        fa2 r |
        R1*14 |
        r4
      }
    >>
  }
  \tag #'(recit basse) {
    <<
      \tag #'recit { \clef "soprano/treble" r2 R1*23 r2 r4 }
      \tag #'basse { s2 s1*23 s2. \ffclef "soprano/treble" }
    >> <>^\markup\character-text Astasie au désespoir, s’évanouit
    do''8 do'' | \noBreak
    fa''4 fa'' r fa''8 mi'' |
    re''2. la''4 |
    sold''1 |
    R1 |
    \ffclef "bass" <>^\markup\character Atar
    r8 si si si sold sold la si |
    mi mi r4 r2 |
    r8 la la si dod'8. dod'16 dod'8 dod' |
    dod' sold r sold si4 si8 re' |
    si4 si8 dod' la4 r16 dod' dod' dod' |
    la4^\markup\italic Galamment la8 la la4 si8 dod' |
    fad4 fad r8 la si dod' |
    re'4. re'8 re' dod' si la |
    sold4 sold4. si8 dod' re' |
    dod'2. dod'4 |
    re'4. dod'8 si4. mi'8 |
    la4
  }
>>
