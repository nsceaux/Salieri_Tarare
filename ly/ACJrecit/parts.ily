\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (oboi #:score-template "score-part-voix")
   (clarinetti #:score-template "score-part-voix"
               #:notes "oboi")
   (corni #:score-template "score-part-voix"
          #:tag-global ()
          #:instrument "Cors en Mi♭")
   (fagotti #:score-template "score-part-voix")
   (reduction)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet#})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#}))
