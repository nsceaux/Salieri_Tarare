\clef "treble" \transposition mib
<<
  { re''1~ |
    re''~ |
    re''~ |
    re''~ |
    re'' |
    mi''~ |
    mi''2 mi''~ |
    mi''1( |
    re'')~ |
    re'' | } \\
  { sol'1\fp~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol' |
    do''~ |
    do''2 mi'' |
  }
>>
r4 mi''4 mi'' r |
R1*6 |
