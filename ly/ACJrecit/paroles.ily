Sois l’em -- pe -- reur A -- tar ; des -- po -- te de l’A -- si -- e,
règne à ton gré dans le pa -- lais d’Or -- mus.

Ô toi, sol -- dat, for -- mé de pa -- rents in -- con -- nus,
gé -- mis long -- temps de no -- tre fan -- tai -- si -- e.

Vous l’a -- vez fait sol -- dat ; mais n’al -- lez pas plus loin :
c’est Ta -- ra -- re. Bien -- tôt vous se -- rez le té -- moin
de leur dis -- sem -- blan -- ce fu -- tu -- re.
