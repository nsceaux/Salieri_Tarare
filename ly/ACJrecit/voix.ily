\clef "bass" <>_\markup\italic parlé
<>^\markup\character-text Le Génie imposant les mains sur l’ombre d’Atar
R1 |
fa2 fa8 fa fa fa |
sib4. sib8 re' re' do' re' |
sib sib r4 sib sib8 sib |
fa4. fa8 fa sol lab sib |
sol2 <>^\markup\italic imposant les mains sur l’ombre de Tarare r4 r8 sol |
sib4 r8 re' do'4 r8 do' |
sol4 sol8 lab sib4 sib8 do' |
lab2 r8 do' do' re' |
si4. sol8 si si si do' |
sol sol r4
\ffclef "soprano/treble" <>^\markup\character-text La Nature au Génie
sol'8 sol'16 sol' sol'8 sol' |
do''4 r do''8 do''16 do'' do''8 mi'' |
do''4 r r <>^\markup\italic Ferme do''8. do''16 |
do''8 sol' r sol' do''4 do''8 do'' |
do''4 re''8 mi'' si'4 r8 si' |
re''4 re''8 fa'' re''4 re''8 do'' |
la' la' r4 r2 |
