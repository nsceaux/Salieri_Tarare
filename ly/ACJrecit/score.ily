\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with {
        instrumentName = \markup\center-column { Hautbois et Clarinette }
        shortInstrumentName = \markup\center-column { Htb. Cl. }
      } << \global \includeNotes "oboi" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors en Mi♭ }
        shortInstrumentName = "Cor."
      } << \keepWithTag #'() \global \includeNotes "corni" >>
      \new Staff \with { \fagottiInstr } <<
        \global \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup \with { \haraKiri } <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*4\break s1*4\break s1*3\pageBreak
        s1*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
