\clef "bass" <<
  { sib1~ |
    sib~ |
    sib~ |
    sib~ |
    sib |
    sib~ |
    sib2 do'~ |
    do'1~ |
    do'( |
    re') | } \\
  { sib,1\fp~ |
    sib,~ |
    sib,~ |
    sib,~ |
    sib, |
    mib~ |
    mib2 mi~ |
    mi1( |
    fa)~ |
    fa | }
>>
r4 sol do r |
R1*6 |
