\clef "treble" <<
  { sib'1~ |
    sib'~ |
    sib'~ |
    sib'~ |
    sib' |
    sol'~ sol'2 do''~ |
    do''1~ |
    do''( |
    si') |
  } \\
  { re'1\fp~ |
    re'~ |
    re'~ |
    re'~ |
    re' |
    mib'~ |
    mib'2 sol'~ |
    sol'1( |
    lab')( |
    sol') | }
>>
r4 << { si' do'' } \\ { re' mi' } >> r4 |
R1*6 |
