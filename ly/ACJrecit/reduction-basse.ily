\clef "bass" <sib, sib>1~ |
q~ |
q~ |
q~ |
q |
<mib sib>1~ |
q2 <mi do'>2~ |
q1 |
<fa do'> |
<fa re'> |
r4 sol do r |
R1 |
r4 r8. do16 do2~ |
do4 r r2 |
r sold4 r |
R1 |
la4 r r2 |
