\clef "treble" <re' sib'>2 r |
R1*9 |
r4 <sol re' si'>4\f <sol mi' do''> r |
R1 |
r4 r8. <sol mi'>16\f q2~ |
q4 r r2 |
r <mi'? si'>4 r |
R1 |
<mi'? do''>4 r r2 |
