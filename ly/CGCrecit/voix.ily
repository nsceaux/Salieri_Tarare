\clef "bass" <>^\markup\italic { Il relève l’enfant d’un ton dogmatique }
r16_"Parlé" |
r2 r4 r8 sol16 sol |
sol4 sol8 la si!4 r8 re'16 do' |
si4 si8 do' do' sol r4 |
r4 r8 fa do'4 do'8 do' |
do'4 sib8 do' la4 r8 <>^\markup\italic { avec ironie } fa |
fa fa fa sol la4 la8 do' |
la4 la8 sib fa4 r |
r r8 fa fa4 fa8 fa |
sib4 sib8 sib la!8 la r16 la la la |
mi4 r16 sol sol la fa4 r8 <>^\markup\italic { lent et majestueux } la16 la |
re'4 la8 si sold4 r8 mi |
sold sold sold la mi4 r |
