\clef "treble"
<<
  \tag #'violino1 {
    re'16\f |
    re'1\p~ |
    re'~ |
    re'2 r8 do''4\f do''8 |
    do''1~ |
    do''~ |
    do''~ |
    do''2
  }
  \tag #'violino2 {
    si!16-\sug\f |
    si1-\sug\p~ |
    si~ |
    si2 r8 mi'(-\sug\f fa' sol') |
    <fa' la'>1~ |
    q~ |
    q~ |
    q2
  }
>> r16. sib32\f re'16. fa'32
<<
  \tag #'violino1 { sib'8 sib' | sib'4 }
  \tag #'violino2 { <sib fa'>8 q | q4 }
>> r4 r2 |
r2 r8. <mi'! la'>16 q8 r |
r2 <fa' la'>4 r |
R1 |
r2 r4 <mi' si' sold''>4 |
