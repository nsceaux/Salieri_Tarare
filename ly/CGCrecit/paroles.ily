Tout le peu -- ple, mon fils, sous nos voû -- tes ar -- ri -- ve.
A -- vant de nom -- mer son ven -- geur,
vous le fe -- rez rou -- gir de sa vai -- ne ter -- reur.
Il croit les chré -- tiens sur la ri -- ve ;
as -- su -- rez- le qu’ils sont bien loin ;
et du res -- te, mon fils, Bra -- ma pren -- dra le soin.
