\clef "alto" sol16-\sug\f |
sol1-\sug\p~ |
sol~ |
sol2 r8 do'\f( re' mi') |
mib'1~ |
mib'~ |
mib'~ |
mib'2 re'4\f re'8 re' |
re'4 r r2 |
r2 r8. dod'16 dod'8 r |
r2 re'4 r |
R1 |
r2 r4 mi' |
