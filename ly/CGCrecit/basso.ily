\clef "bass" sol16\f |
sol1\p~ |
sol~ |
sol2 r8 do\f( re mi) |
mib1~ |
mib~ |
mib~ |
mib2 re4\f re8 re |
re4 r r2 |
r2 r8. dod16 dod8 r |
r2 re4 r |
R1 |
r2 r4 mi |
