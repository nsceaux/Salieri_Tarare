\clef "treble" r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''4 fa''8. fa''16 la''4 la''8. la''16 |
    do'''4 la''8. do'''16 la''4 do'' |
    re''1 |
    do''2. do'''8. la''16 |
    sol''2 sol'' |
    la''4 fa''8. fa''16 fa''4 }
  { la'4 la'8. la'16 do''4 do''8. do''16 |
    la'4 do''8. la'16 do''4 la' |
    sib'1 |
    la'2. fa''4 |
    fa''2 mi'' |
    fa''4 la'8. la'16 la'4 }
  { s1-\sug\fp s s-\sug\fp s1*2 s4\f }
>> r4 |
r \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''8. sol''16 sol''4 }
  { do''8. do''16 do''4 }
>> r4
\twoVoices #'(oboe1 oboe2 oboi) <<
  { r8 fa''4 sol''16*2/3 fa'' mi'' fa''8. fa''16 fa''8. fa''16 |
    si''4. do'''8 re'''8. si''16 sol''8. fa''16 |
    mi''4 r r2 |
    do'''1~ |
    do''' |
    do'''2 si'' |
    do'''4. do''8 mi''4 sol'' |
  }
  { R1*3 |
    mi''1 |
    re'' |
    mi''2 re'' |
    mi''4. do''8 mi''4 sol'' | }
  { s1*3 s1-\sug\ff s-\sug\p -\sug\cresc | s2 s-\sug\f }
>>
do''1~ |
do''~ |
do'' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4. re''16 fa'' fa''8 re'' \grace do''8 si' la'16 si' |
    do''4 mi' mib'4. re'16 mib' |
    re'4 re'16 re'' dod'' re'' mib'' re'' dod'' re'' mib'' re'' dod'' re'' |
    fad''2 sol''4 la'' |
    sib''2 }
  { R1 |
    do''4 mi' mib'4.\f re'16 mib' |
    re'4 r r2 |
    la'2 sol'4 fad' |
    sol'2 }
  { s1 | s2 s\trill | s1-\sug\fp }
>> r2 |
R1 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''2 do'''4 |
    re'''2 \slurDashed fa''4( re''8 sib') |
    sol'4( la'8 sib' do'' re'' mib'' do'') | \slurSolid
    sib'4 sib''2 la''4 |
    sib''4 }
  { re''2 la'4 |
    sib'2 \slurDashed fa''4( re''8 sib') |
    sol'4( la'8 sib' do'' re'' mib'' do'') | \slurSolid
    sib'4 re''2 do''4 |
    re'' }
  { s2.\p | s2 s\f }
>> r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib'8 re''4 fa'' | sib'' sib'8. sib'16 sib'4 }
  { sib'8 re''4 fa'' | sib'' sib'8. sib'16 sib'4 }
>> r4\fermata |
<>-\sug\fp \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4 sol''8. sol''16 sib''4 sib'' |
    re'''2. sib''4 |
    do'''1 |
    fad'' |
    fad''4 }
  { sib'4 sib'8. sib'16 re''4 re'' |
    sib'2. re''4 |
    mib''1 |
    la' |
    la'4 }
  { s1 | s1-\sug\fp | s-\sug\fp | s-\sug\f | s4-\sug\fp }
>> r4 r2 |
R1 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''1~ |
    fad'' |
    sol'' |
    si''( |
    re'''4) fa''!8. fa''16 fa''4 fa'' |
    fa''4*1/2 }
  { la'1~ |
    la' |
    si' |
    re'' |
    si'4 si'8. si'16 si'4 si' |
    si'4*1/2 }
  { s1*2\cresc s1-\sug\f }
>> s8\fermata r4 r2 |
R1^\fermataMarkup |
R1*3 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do'''2. sol''4 | mi'' do'' mi'' sol'' | sol''1 | mi''1~ |
    mi''~ | mi''2 fa'' | mib''1 | re''4 }
  { do''2. sol''4 | mi'' do'' mi'' sol'' | mi''1 | sib'~ |
    sib'~ | sib'2 la'~ | la'1 | sib'4 }
  { s1*3 | s1-\sug\fp | s1 | s2 s-\sug\p }
>> r4 r2 |
R1*17 |
