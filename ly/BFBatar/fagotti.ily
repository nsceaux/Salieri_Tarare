\clef "bass" r4 |
fa1-\sug\fp~ |
fa~ |
fa~ |
fa |
sib2 do' |
fa4-\sug\f fa8. fa16 fa4 r |
r mi8. mi16 mi4 r |
r re8. re16 re4 r |
R1 |
r4 do8.-\sug\f do16 do4 do |
do'2.-\sug\ff do4-\sug\p |
fa,2 fa-\sug\cresc |
sol,1-\sug\f |
do4. do8 mi4 sol |
do'2. sol4\p |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la4 sol8. sol16 la4 fa | sol8 do mi do }
  { do4 r do r | do8 do mi do }
>> mi8 do mi do |
fa do fa do re fa sol fa |
mi2 mib-\sug\f |
re1-\sug\fp |
re1 |
sol2 r |
R1*2 |
<< sib1~ { s2 s-\sug\p } >> |
sib2 sol4 mib' |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'2. do'4 | sib4. sib,8 re4 fa | }
  { sib2. la4 | sib4. sib,8 re4 fa | }
>>
sib4 sib,8. sib,16 sib,4 r\fermata |
sol1-\sug\fp ~ |
sol-\sug\fp |
do'-\sug\fp |
re'-\sug\f |
re'4-\sug\fp re2 re4 |
dod1 |
do!2-\sug\cresc la, |
fad,1 |
sol,-\sug\f |
sol,~ |
sol, |
sol,4*1/2 s8\fermata r4 r2 |
R1^\fermataMarkup |
do4\p r4 r2 |
r2 si,4.\f dod16 red |
mi4 mi fa re |
do do'8. do'16 do'4 sol |
mi do mi sol |
do' do'2 do'4 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mi'1~ | mi'~ | mi'2 fa' | fa'1 | re'4 }
  { do1~ | do~ | do2 fa | fa1 | sib4 }
  { s1\fp | s1 | s2 s-\sug\p }
>> r4 r2 |
R1*17 |
