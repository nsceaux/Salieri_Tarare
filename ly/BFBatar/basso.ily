\clef "bass" r4 |
fa,8\fp fa fa fa fa2:8 |
fa:8 fa:8 |
fa:8\fp fa:8 |
fa:8 fa:8 |
sib,:8 do:8 |
fa:8\f fa:8\p |
mi:8 mi:8 |
re:8 re:8 |
sol,:8 sol,:8 |
do:8\f do:8 |
do2.\ff do4\p |
fa2:8 fa:8\cresc |
sol:8\f sol,:8 |
do4 do mi sol |
do'2. <<
  { sol4\p | la4 sol8. sol16 la4 fa | sol8 do mi do } \\
  { r4 | do4 r do r | do8 do mi do }
>> mi8 do mi do |
fa do fa do re fa sol fa |
mi2:8 mib:8\f |
re:8\fp re:8 |
re:8 re:8 |
sol:8 re:8\p |
mib:8\< mib:8\! |
fa:8\p fa,:8 |
sib,:8 re:8-\sug\f |
mib:8 mib:8 |
fa:8 fa,:8 |
sib,4. sib,8 re4 fa |
sib4 sib,8. sib,16 sib,4 r\fermata |
sol,8\fp sol sol sol sol2:8 |
sol:8\fp sol:8 |
do:8\fp do:8 |
re:8\f re:8 |
re:8\fp re:8 |
re:8 re:8 |
re:8\cresc re:8 |
re:8 re:8 |
sol:8\f sol:8 |
sol:8 sol:8 |
sol4 sol8. sol16 sol4 sol |
sol4*1/2 s8\fermata r4 r2 |
R1^\fermataMarkup |
do4\p r4 r2 |
r2 si,4.\f dod16 red |
mi4 mi fa re |
do do'8. do'16 do'4 sol |
mi do mi sol |
do'8 do do do do2:8 |
do4 r r2 |
R1 |
r2 fa4-\sug\p r |
fa r r2 |
sib,!4 r r2 |
fad4-\sug\p r fad r |
sol r sib,! r |
do8 r do r re r re r |
sol4 sol,\f~ sol,8 la,16 sib,! do re mi fad |
sol1 |
la4\p r la r |
sold\f r sold\p r |
la r r2 |
R1 |
re4 r r2 |
r mi4 r |
R1 |
r8. mi16\f mi8 r r8. sold16 sold8 r |
r2 la\p |
r r8. fa16\f fa8 r |
fa4 r r sol |
do2 r |
