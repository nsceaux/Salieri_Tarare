\clef "treble" r4 |
R1*28 |
R1^\fermataMarkup |
<>-\sug\fp \twoVoices #'(corno1 corno2 corni) <<
  { sol'4 sol'8. sol'16 sol'4 sol' | }
  { sol'4 sol'8. sol'16 sol'4 sol' | }
>>
sol'1-\sug\fp |
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 do'' | re''4 re''8. re''16 re''4 re'' | re'' }
  { do''2 do'' | re''4 re''8. re''16 re''4 re'' | re'' }
  { s1-\sug\fp | s-\sug\f | s4-\sug\fp }
>> r4 r2 |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''1~ | re'' | re'' | re''~ |
    re''4 re''8. re''16 re''4 re'' | re''4*1/2 }
  { re''1~ | re'' | sol' | sol'~ |
    sol'4 sol'8. sol'16 sol'4 sol' | sol'4*1/2 }
  { s1-\sug\cresc | s1 | s1-\sug\f }
>> s8\fermata r4 r2 |
R1^\fermataMarkup
R1*3 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''2. sol'4 | mi' do' mi' sol' | do''4. do''8 do''4. do''8 | do''4 }
  { do''2. sol'4 | mi' do' mi' sol' | do''4. do'8 do'4. do'8 | do'4 }
  { s1*3 | s4-\sug\fp }
>> r4 r2 |
R1*21 |
