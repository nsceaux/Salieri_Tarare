\tag #'tous {
  Re -- çois en pur don ce pa -- lais
  que dix mille es -- cla -- ves ma -- lais
  ont cons -- truit d’i -- voire et d’é -- bè -- ne :
  ce pa -- lais, dont l’as -- pect ri -- ant
  do -- mi -- ne la fer -- ti -- le plai -- ne,
  et la vas -- te mer d’o -- ri -- ent.
  Là, cent fem -- mes de Cir -- cas -- si -- e,
  plei -- nes d’at -- traits et de pu -- deur,
  at -- ten -- dront l’or -- dre de ton cœur,
  pour t’e -- ni -- vrer des tré -- sors de l’A -- si -- e,
  pour t’e -- ni -- vrer des tré -- sors de l’A -- si -- e.
  Puis -- se de ton bon -- heur l’en -- vieux s’ir -- ri -- ter !
  puis -- se l’in -- fa -- me ca -- lom -- ni -- e,
  pour te perdre, en vain s’a -- gi -- ter, en vain, en vain __
}
Mais, sei -- gneur, ta hau -- tesse ou -- bli -- e…

Je l’é -- lève, Al -- ta -- mort, pour le pré -- ci -- pi -- ter.

Al -- lez, Vi -- zir, que l’on pu -- bli -- e…

Ô mon roi ! ta bon -- té doit se faire a -- do -- rer.
Des maux du sort mon âme est peu sai -- si -- e ;
mais ce -- lui de mon cœur ne peut se ré -- pa -- rer,
le bar -- ba -- re em -- mène As -- ta -- si -- e.

Quelle est cet -- te femme, Al -- ta -- mort ?

Sei -- gneur, si j’en crois son trans -- port,
quelque es -- cla -- ve jeune et jo -- li -- e.

Une es -- cla -- ve ! une es -- cla -- ve ! ex -- cuse, ô roi d’Or -- mus !
À ce nom o -- di -- eux tous mes sens sont é -- mus.
