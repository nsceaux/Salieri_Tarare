\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with { \oboiInstr } << \global \includeNotes "oboi" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes en Ut }
        shortInstrumentName = "Tr."
      } <<
        { s4 s1*29 <>^"Trompettes en Ut" }
        \keepWithTag #'() \global \includeNotes "trombe"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors en Fa }
        shortInstrumentName = "Cor."
      } << \keepWithTag #'() \global \includeNotes "corni" >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'tous \includeNotes "voix"
    >> \keepWithTag #'tous \includeLyrics "paroles"
    \new Staff \with {
      instrumentName = \markup\center-column { Violoncelles et Basses }
      shortInstrumentName = "B."
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s4 s1*4\break s1*5\pageBreak
        s1*6\break s1*5\pageBreak
        \grace s8 s1*5\break s1*5\pageBreak
        s1*7\break s1*5\pageBreak
        s1*6\break s1*5\pageBreak
        s1*5\break s1*4 s2 \bar "" \break
        s2 s1*2\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
