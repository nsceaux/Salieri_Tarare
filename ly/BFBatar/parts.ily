\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (fagotti #:score-template "score-part-voix"
            #:music , #{
\quoteBasso "BFBbasso"
s4 s1*53 <>^\markup\tiny Basso
\cue "BFBbasso" { \mmRestDown s1*8 \mmRestCenter }
s1
\cue "BFBbasso" { \mmRestDown s1*2 \mmRestCenter }
s1
\cue "BFBbasso" { \mmRestDown s1*5 }
#})
   (oboi #:score-template "score-part-voix")
   (corni #:instrument "Cors en Fa" #:score-template "score-part-voix")
   (trombe #:instrument , #{\markup\center-column { Trompettes en Ut }#}
           #:score-template "score-part-voix")
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#}))
