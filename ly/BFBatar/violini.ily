\clef "treble" r4 |
<<
  \tag #'violino1 {
    <fa' la' fa''>4\fp fa''8. fa''16 la''4 la''8. la''16 |
    do'''4 la''8. do'''16 fa''4 do'''8. do'''16 |
    re'''4.\fp sib''8 fa''4 sib''8 re''' |
    do'''4 do'''16 sib'' la'' sib'' do'''4 do'''8. la''16 |
    sol''4. sol''8 do'''4 sib''8. do'''16 |
    la''4 <la' fa''>8. q16 q4 la''8.\p fa''16 |
    sol''2 do'''8. si''16 la''8. sol''16 |
    fa''4. sol''16*2/3 fa'' mi'' fa''8. fa''16 fa''8. fa''16 |
    si''4. do'''8 re'''8. si''16 sol''8. fa''16 |
    mi''4 r16 do''\f si' do'' re'' do'' si' do'' re'' do'' si' do'' |
  }
  \tag #'violino2 {
    la16-\sug\fp do' fa' do' la do' fa' do' la do' fa' do' la do' fa' do' |
    la do' fa' do' la do' fa' do' <la do' fa'>2:16 |
    <>-\sug\fp \ru#4 { sib16 re' fa' re' } |
    \ru#2 { la do' fa' do' } <la do' fa'>2:16 |
    re'16 fa' sol' fa' re' fa' sol' fa' mi'! sol' do'' sol' mi' sol' do'' sol' |
    fa'-\sug\f la' do'' la' fa' la' do'' la' <fa' la' do''>2:16-\sug\p |
    do''16 sol' mi' sol' do'' sol' mi' sol' do'2:16 |
    la'16 fa' la' fa' la' fa' la' fa' <la' fa'>2:16 |
    fa'16 si' re'' si' fa' si' re'' si' si re' sol' re' si re' sol' si |
    do'16<>-\sug\f mi' sol' mi' do' mi' sol' mi' do' mi' sol' mi' do' mi' sol' mi' |
  }
>>
do'16-\sug\ff <mi'' do'''> q q q4:16 q2:16 |
<re'' do'''>2:16\p q:16\cresc |
<mi'' do'''>:16 <re'' si''>:16\f |
<mi'' do'''>4. do''8 mi''8. mi''16 sol''8. sol''16 |
<<
  \tag #'violino1 {
    do'''8 do'' do'' do'' do''2:8\p |
    do'':8 do'':8 |
    do'':8 sol''8 la''16 sol'' fa''8 mi'' |
    la''4. la''8 si''4. si''8 |
    do'''2 r4 do'''8.\f do'''16 |
    re''4\fp~ re''16 re'' dod'' re'' mib'' re'' dod'' re'' mib'' re'' dod'' re'' |
    \grace re''8 re'''4. do'''8 sib''4. la''8 |
    sib''2 sib''4\p( re'''8 sib'') |
    sol''4\<( la''8 sib'' do'''\! re''' mib''' do''') |
    sib''2.\p do'''4 |
    re'''2 fa'''4(\f re'''8 sib'') |
    sol''4( la''8 sib'' do''' re''' mib''' do''') |
    sib''2. do'''4 |
    sib''4.
  }
  \tag #'violino2 {
    do'''2. mi''4-\sug\p |
    fa'' mi''8. mi''16 fa''4 re'' |
    mi''2 sol'8 la'16 sol' fa'8 mi' |
    la'4. la'8 si'4. si'8 |
    do''8 do''16 sol' mi'! sol' do'' sol' mib'\f sol' do'' sol' mib' sol' do'' sol' |
    fad'-\sug\fp la' fad' la' fad' la' fad' la' fad' la' fad' la' fad' la' fad' la' |
    fad'16 la' re'' la' fad' la' re'' la' sol' sib' re'' sib' fad' la' re'' la' |
    sol' sib' re'' sib' sol' sib' re'' sib' re'-\sug\p fa'! sib' fa' re' fa' sib' fa' |
    mib'\< sol' sib' sol' mib' sol' sib' sol' mib'\! sol' do'' sol' mib' sol' do'' sol' |
    re'-\sug\p fa' sib' fa' re' fa' sib' fa' re' fa' sib' fa' fa' la' do'' la' |
    fa' sib' re'' sib' fa' sib' re'' sib' re'-\sug\f fa' sib' fa' re' fa' sib' fa' |
    mib' sol' sib' sol' mib' sol' sib' sol' mib' sol' sib' sol' mib' sol' sib' mib' |
    re' fa' sib' fa' re' fa' sib' fa' re' fa' sib' fa' mib' fa' la' fa' |
    <re' sib'>4.
  }
>> \grace do''8 sib'16 la'32 sib' re''8 re''16 re'' fa''8 fa''16 fa'' |
sib''4 <re' sib'>8. q16 q4 r\fermata |
<<
  \tag #'violino1 {
    <re' sib' sol''>4\fp sol''8. sol''16 sib''4 sib'' |
    re'''2.\fp sol''4 |
    mib'''2 mib'''4. mib'''8 |
    fad''4\f fad''8. fad''16 la''4 la'' |
    re'''4\fp re''8 re'' re'' re'' re'' re'' |
    mi''2:16 mi'':16 |
    fad'':16\cresc la'':16 |
    re''':16 re''':16 |
    si''4:16\f re'':16 sol'':16 si'':16 |
    re'''8 r sol''4:16 si'':16 re''':16 |
    fa'''4 <sol fa'>8. q16 q4 q |
    q4*1/2
  }
  \tag #'violino2 {
    <re' sib'>2:16-\sug\fp q:16 |
    <re' sib'>2:16\fp q:16 |
    <mib' do''>:16\fp q:16 |
    <re' la'>:16\f q:16 |
    q4 fad'8\p fad' fad' fad' fad' fad' |
    sol'2:16 sol':16 |
    <fad' la'>:16\cresc q:16 |
    q:16 q:16 |
    <sol' si'>:16-\sug\f q:16 |
    <si' sol''>:16 <si' sol''>:16 |
    <re'' si''>4 <si re'>8. q16 q4 q |
    q4*1/2
  }
>> s8\fermata r4 r2 |
R1^\fermataMarkup |
<sol mi'>4\p r r2 |
r2 si4.\f dod'16 red' |
mi'4 \grace fa'8 mi' re'16 mi' fa'!4 re'! |
<<
  \tag #'violino1 {
    \grace { sol16[ mi' do''] } do'''2~ do'''8. sol''16 \grace la''8 sol'' fa''16 sol'' |
    mi''4 do''16 do'' do'' do'' mi''4:16 sol'':16 |
    do'''4 do'''8. do'''16 do'''4. re'''8 |
    sib''4 r r2 |
    R1 |
    r2 la''4\p r |
    <mib'' fa'>4 r r2 |
    <fa' re''>4 r r2 |
    r8 re'''\p re''' re''' r re''' re''' re''' |
    r sib''! sib'' sib'' r sol'' sol'' sol'' |
    r mib'' r do'' r la' r re'' |
    sol'4 sol'\f~ sol'8 la'16 sib' do'' re'' mi''! fad'' |
    sol''4 \grace la''8 sol''16 fad'' sol'' la'' sib''4. sib''8 |
    r dod''8\p dod'' dod'' r mi''! mi'' mi'' |
    r fa''\f fa'' fa'' r mi''\p mi'' mi'' |
    mi''4 r r2 |
    R1 |
  }
  \tag #'violino2 {
    \grace { sol16[ mi'] } do''2 do''8. sol'16 \grace la'8 sol' fa'16 sol' |
    mi'4 do'16 do' do' do' mi'4:16 sol':16 |
    <mi' do''>2:16 q:16 |
    q4 r r2 |
    R1 |
    r2 <fa' do''>4-\sug\p r |
    <do' la'> r r2 |
    <re' sib'!>4 r r2 |
    r8 la''-\sug\p la'' la'' r la'' la'' la'' |
    r sol'' sol'' sol'' r re'' re'' re'' |
    r sol' r sol' r fad' r fad' |
    sol'4 sol-\sug\f~ sol8 la16 sib do' re' mi'! fad' |
    sol'4 \grace la'8 sol'16 fad' sol' la' sib'4. sib'8 |
    r mi'!-\sug\p mi' mi' r dod'' dod'' dod'' |
    r re''-\sug\f re'' re'' r re''-\sug\p re'' re'' |
    dod''4 r r2 |
    R1 |
  }
>>
<la fad'>4 r r2 |
r <<
  \tag #'violino1 { si?4 }
  \tag #'violino2 { sold }
>> r4 |
R1 |
r8. <sold' si'>16\f q8 r r8. <si' mi''>16 q8\fermata r |
r2 <<
  \tag #'violino1 { mi'2\p }
  \tag #'violino2 { do'2-\sug\p }
>>
r2 r8. <<
  \tag #'violino1 { <re' re''>16\f q8 }
  \tag #'violino2 { <re' la'>16-\sug\f q8 }
>> r8 |
<re' si'>4 r r <sol re' si'> |
<sol mi' do''>2 r |
