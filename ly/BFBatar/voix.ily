\clef "bass"
<<
  \tag #'tous {
    <>^\markup\italic chanté ^\markup\character-text Atar d’un ton brillant
    do4 |
    fa fa8. fa16 la4 la8. la16 |
    do'2 r4 do'8 do' |
    re'4. sib8 fa4 sib8 re' |
    do'2 r4 do'8 la |
    sol4. sol8 do'4 sib8 do' |
    la4 fa r la8. fa16 |
    sol2 do'8 si la sol |
    fa2 r4 r8 fa |
    si4. do'8 re' si sol fa |
    mi4 mi r2 |
    do'2. do4 |
    fa2 la |
    sol2. sol8 sol |
    do2 r |
    do'2. mi4 |
    fa mi8 mi fa4. re8 |
    mi4 mi sol fa8 mi |
    la4. la8 si4. sol8 |
    do'2 r4 do'8 do' |
    re1 |
    re'4. do'8 sib4 la |
    sib2 sib4 re'8 sib |
    sol4 la8 sib do'[ re'] mib' do' |
    sib2.( do'4) |
    re'2 fa'4 re'8 sib |
    sol4 la8 sib do'[ re'] mib' do' |
    sib2.( do'4) |
    sib2 r |
    R1^\fermataMarkup |
    sol4 sol8 sol sib4 sib |
    re'2. sol4 |
    mib'2 mib'4. mib'8 |
    fad2 r |
    re'4 re8 re re re re re |
    mi!4 mi r mi8 mi |
    fad2 la |
    re'2 re'4. re'8 |
    si2 r4 sol |
    re'2. si4 |
    fa'1~ |
    fa'4
  }
  \tag #'basse { r4 R1*40 r4 }
>>
\ffclef "bass" <>^\markup\character-text Altamort \line { bas \normal-text Parlé }
r8 re16 re sol8 sol16 sol sol8 la |
fa4 fa
\ffclef "bass" <>^\markup\character-text Atar bas à Alta.
r8 re16 mi fa8 fa16 sol |
mi4 r mi8 mi16 mi red8 mi |
si,4 r r2 |
r r4 r8^\markup\italic (haut) sol |
do'2. sol4 |
mi2 r |
r4 do' do'4. re'8 |
sib8 sib
\ffclef "tenor/G_8" <>^\markup\character-text Tarare \normal-text Récit
sib!2 sib4 |
sol4. sib!16 sib mi'4. mi'16 sol' |
sol'4 sib!8 do' la4 r16 do' do' re' |
mib'4. do'8 mib' mib' mib' fa' |
re' re' r4 r2 |
re'4. re'8 re'4 re'8 re' |
sib!2 r4 sol' |
mib'4. do'8 la4. re'8 |
sol2 r |
r re'4. mi'!8 |
dod'4 dod' r mi' |
fa'2 mi'4. re'8 |
dod'4 la
\ffclef "bass" <>^\markup\character-text Atar avec un signe d’intelligence
r4 r8 mi | \noBreak
la4 la8 si sol4 sol8 mi |
fad4
\ffclef "bass" <>^\markup\character Altamort
r8 la re'4 r8 la16 la |
la4 la8 si? sold4 r8 mi16 fad |
sold8 sold sold sold16 la si8 si
\ffclef "tenor/G_8" <>^\markup\character-text Tarare indigné
r8 si?16 si |
sold8 sold r si16 si mi'8 mi' r\fermata mi' |
mi' si si do' la4 r8 la16 la |
la4 la8 la re'4 r8 re'16 fa' |
si4 si8 do' sol4 r |
R1 |

