\clef "treble" \transposition fa
r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''4 do''8. do''16 mi''4 mi''8. mi''16 |
    sol''2. do''8. do''16 |
    do''1 |
    do''4 sol''2 sol''8. mi''16 |
    re''2 re''4. re''8 |
    mi''4 do''8. do''16 do''4 }
  { mi'4 mi'8. mi'16 do'4 do''8. do''16 |
    mi''2. do'8. do'16 |
    do'1 |
    do'4 mi''2 mi''8. do''16 |
    do''2 sol'4. sol'8 |
    do''4 mi'8. mi'16 mi'4 }
  { s1-\sug\fp s s-\sug\fp s1*2 <>-\sug\f }
>> r4 |
r \twoVoices #'(corno1 corno2 corni) <<
  { re''8. re''16 re''4 }
  { sol'8. sol'16 sol'4 }
>> r4 |
R1 |
re''1 |
r8 <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { re''4 re'' re'' re''8 | re''1 | do'2 mi'' | }
  { sol'4 sol' sol' sol'8 | sol'1 | do'2 do'' | }
  { s8 s2. | s1-\sug\ff | s2-\sug\p s-\sug\cresc }
>>
re''1-\sug\f |
\twoVoices #'(corno1 corno2 corni) <<
  { sol'4 }
  { sol' }
>> r4 r2 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol''2. }
  { sol'2. }
>> r4 |
R1 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { re''4 re''8. re''16 | mi''2 re''4. re''8 | re''4 re''8. re''16 re''4 }
  { sol'4 sol'8. sol'16 | do''2 do'' | sol'4 sol'8. sol'16 sol'4 }
  { s2 s1 s2 s4-\sug\f }
>> r4 |
R1*4 |
r4 <>-\sug\p \twoVoices #'(corno1 corno2 corni) <<
  { do''2 do''4 | do''2 do'' }
  { do''2 do''4 | do''2 do'' }
  { s2. | s2 s-\sug\f }
>>
re''1 |
do'' |
\twoVoices #'(corno1 corno2 corni) <<
  { do''4 }
  { do''4 }
>> r4 r2 |
R1^\fermataMarkup |
R1*12
R1^\fermataMarkup |
R1*6 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { re''2 | re''1~ | re''2 mi'' | mi''~ mi'' | fa''4 }
  { sol'2 | sol'1~ | sol'2 sol' | sol'~ sol' | do''4 }
  { s2 | s1 | s2 s-\sug\p }
>> r4 r2 |
R1*17 |
