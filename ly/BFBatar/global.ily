\tag #'all \key fa \major
\tempo "Allegro Maëstoso" \midiTempo#120
\time 2/2 \partial 4 s4 s1*29
\tempo "piu Allegro" s1*14 s2
\tempo "Allegro" s2 s1*9
\tempo "Allegro" s1*17 \bar "|."
