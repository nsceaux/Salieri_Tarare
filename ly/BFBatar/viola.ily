\clef "alto" r4 |
<<
  { do'8 do'4 do' do' do'8~ |
    do' do'4 do' do' do'8 |
    re' re'4 re' re' re'8 |
    do' do'4 do' do' do'8 |
    re' re'4 re'8 mi' mi'4 mi'8 |
    fa' do'4 do' do' do'8 |
    do' do'4 do' do' do'8 |
    fa' fa'4 fa' fa' fa'8 |
    fa' fa'4 fa' re' re'8 |
    mi'8 mi'4 mi' mi' mi'8 |
    mi'16 sol' sol' sol' sol'4:16 sol'2:16 |
    la':16 la'2:16 | } \\
  { la8\fp la4 la la la8~ |
    la la4 la la la8 |
    sib-\sug\fp sib4 sib sib sib8 |
    la la4 la la la8 |
    sib sib4 sib8 sol sol4 sol8 |
    la-\sug\f la4 la la-\sug\p la8 |
    sol sol4 sol sol sol8 |
    la la4 la la la8 |
    si si4 si si si8 |
    do'8-\sug\f do'4 do' do' do'8 |
    do'16-\sug\ff mi' mi' mi' mi'4:16 mi'2:16 |
    fa':16-\sug\p fa':16-\sug\cresc | <>\! }
>>
sol'2:16 sol':16-\sug\f |
do'4. do'8 mi'8. mi'16 sol'8. sol'16 |
do'2. sol4-\sug\p |
la4 sol8. sol16 la4 fa |
sol2 mi'8 do' mi' do' |
fa' do' fa' do' re' fa' sol' fa' |
mi'2:8 mib':8\f re'8 <<
  { fad'4 fad' fad' fad'8 |
    fad' fad'4 fad'8 sol' sol' fad' fad' |
    sol' re'4 re'8 fa' fa'4 fa'8 |
    sol' sol'4 sol'8 mib' mib'4 mib'8 |
    re' re'4 re' re' mib'8 |
    re' re'4 re'8 fa' fa'4 fa'8 |
    sol' sol'4 sol' mib' mib'8 |
    re' re'4 re' re' do'8 | } \\
  { la4\fp la la la8 |
    la8 la4 la8 sib sib la la |
    sib sib4 sib8 sib\p sib4 sib8 |
    sib\< sib4 sib8 do'\! do'4 do'8 |
    sib\p sib4 sib sib do'8 |
    sib sib4 sib8 sib-\sug\f sib4 sib8 |
    sib sib4 sib do' do'8 |
    sib sib4 sib sib la8 | }
>>
sib4. sib8 re' re'16 re' fa'8 fa'16 fa' |
sib'4 sib8. sib16 sib4 r\fermata |
<sib sol'>2:16-\sug\fp q:16 |
q:16-\sug\fp q:16 |
sol':16-\sug\fp sol':16 |
fad':16-\sug\f fad':16 |
<fad' la'>4 re'8\p re' re' re' re' re' |
dod'1 |
do'!2:16-\sug\cresc do':16 |
<la fad'>2:16 q:16 |
sol'16-\sug\f <re' si'> q q q4:16 q2:16 |
q:16 q:16 |
si'4 sol8. sol16 sol4 sol |
sol4*1/2 s8\fermata r4 r2 |
R1^\fermataMarkup |
<sol mi'>4\p r r2 |
r si4.\f dod'16 red' |
mi'4 mi' fa' re' |
do' do''8. do''16 do''4 sol' |
mi' do' mi' sol' |
<mi' sol>2:16 q:16 |
q4 r r2 |
R1 |
r2 fa'4-\sug\p r |
fa' r r2 |
fa'4 r r2 |
r8 re''-\sug\p re'' re'' r re'' re'' re'' |
r re'' re'' re'' r sol' sol' sol' |
r8 do'' r mib' r re' r re' |
sol'4 sol\f~ sol8 la16 sib do' re' mi'! fad'! |
sol'4 \grace la'8 sol'16 fad' sol' la' sib'4. sib'8 |
la-\sug\p la' la' la' r la' la' la' |
sold'2-\sug\f r8 si'-\sug\p si' si' |
mi'4 r r2 |
R1 |
re'4 r r2 |
r mi'4 r |
R1 |
r8. mi'16\f mi'8 r r8. sold'16 sold'8\fermata r8 |
r2 la\p |
r r8. fa'16\f fa'8 r |
fa'4 r r sol' |
do'2 r |
