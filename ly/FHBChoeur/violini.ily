\clef "treble"
<<
  \tag #'violino1 {
    mib'4\f re'4. re'8 re'4 re' |
    mib'8 mib'4 mib' mib' mib'8 |
    re'4. re'8 sol'4. sol'8 |
    fa'1\p~ |
    fa'~ |
    fa'2 r4 r8 sib'\ff |
    <do'' fa''>2 q4. q8 |
    re''2 r |
    sol'
  }
  \tag #'violino2 {
    la4-\sug\f |
    la4. la8 la4 la |
    sib8 sib4 sib8 do'( la sib do') |
    sib4. sib8 mib'4. mib'8 |
    re'1-\sug\p~ |
    re'~ |
    re'2 r4 r8 sib'-\sug\ff |
    <do'' fa'>2 q4. q8 |
    <fa' re''>2 r |
    mib'2
  }
>> r4 sib'\ff |
<re' sib' fa''>2 <sib' fa''>4. q8 |
<sol'' sib'>4 <mib'' sol'> r2 |
<<
  \tag #'violino1 {
    do''2\p r4 lab''\ff |
    fa''2 <sol'' sib' mib'>4 q |
    <mib' do'' lab''>4 r r\fermata do''\p |
    fa'4.. fa'16 fa'4.. fa'16 |
  }
  \tag #'violino2 {
    mib'2-\sug\p r4 do''-\sug\ff |
    <reb'' fa' lab>2 <mib' reb''>4 q |
    <do'' mib'>4 r r\fermata mib'-\sug\p |
    re'!4.. re'16 re'4.. re'16 |
  }
>>
mib'4. <mib' sol>8 q4 q |
q2 r |
