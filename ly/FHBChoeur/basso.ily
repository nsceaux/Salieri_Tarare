\clef "bass" fa4\f |
fad1 |
sol4 sol \once\slurDashed la8( fa! sol la) |
sib4. sib,8 sib,4. sib,8 |
sib,1\p~ |
sib,~ |
sib,2 r4 r8 sib\ff |
la2 la,4. la,8 |
sib,2 r |
mib r4 sib\ff |
re2 re4. re8 |
mib4 mib r2 |
lab,2\p r4 lab\ff |
reb4. reb8 mib4 mib |
lab, r r\fermata lab,\p |
sib,4.. sib,16 sib,4.. sib,16 |
mib4. mib8 mib4 mib |
mib2 r |
