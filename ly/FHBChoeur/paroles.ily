\tag #'(recit basse) {
  Ta -- rare, il faut cé -- der !
}
\tag #'(choeur basse) {
  Ta -- rare, il faut cé -- der !
}
\tag #'(recit basse) {
  Leurs dé -- sirs sont ex -- trê -- mes.
}
\tag #'(choeur basse) {
  Nos dé -- sirs sont ex -- trê -- mes.
}
\tag #'(recit basse) {
  Sois donc le roi d’Or -- mus.
}
\tag #'(choeur basse) {
  Sois, sois le roi d’Or -- mus.
}
\tag #'(recit basse) {
  Il est des dieux su -- prê -- mes.
}
