\piecePartSpecs
#`((violino1 #:score-template "score-part-voix" #:indent 0)
   (violino2 #:score-template "score-part-voix" #:indent 0)
   (viola #:score-template "score-part-voix" #:indent 0)
   (basso #:score-template "score-part-voix" #:indent 0)
   (oboi #:score-template "score-part-voix" #:tag-notes oboi #:indent 0)
   (silence #:indent 0)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
