<<
  \tag #'(recit basse) {
    \clef "bass"
    \tag #'recit <>^\markup\italic { Prenant le diadème des mains d’Urson }
    r4 |
    R1*3 |
    r2 r4-\tag #'recit ^"Récit" -\tag #'basse ^\markup\character Arthénée r8 fa |
    sib2~ sib8 sib sib sib |
    fa2 <<
      \tag #'basse { s2 s1 s4 \ffclef "bass" <>^\markup\character Arthénée }
      \tag #'recit { r2 | R1 | r4\fermata <>^"Récit" }
    >> fa8 sol lab4 lab8 sib |
    sol4 sol <<
      \tag #'basse { s2 s1 s4. \ffclef "bass" <>^\markup\character Arthénée }
      \tag #'recit { r2 | R1 | r4 r8 }
    >> sol8 sib sib sib mib' |
    do'2 <<
      \tag #'basse { s2 s1 s2 \ffclef "bass" r4^\markup\character Arthénée }
      \tag #'recit { r2 | R1 | r2 r4\fermata }
    >> do'4 |
    fa4. fa8 sib4. sib8 |
    sol4 mib r2 |
    R1 |
  }
  \tag #'(vdessus basse) {
    <<
      \tag #'basse { s4 s1*5 s2 \ffclef "soprano/treble" <>^\markup\character Chœur }
      \tag #'vdessus { \clef "soprano/treble" r4 | R1*5 | r2 }
    >> r4 r8 sib' |
    fa''4. fa''8 fa''4. fa''8 |
    <<
      \tag #'basse {
        re''4 s2. s2 \ffclef "soprano/treble" <>^\markup\character Chœur
      }
      \tag #'vdessus { re''2 r | r2 }
    >> r4 sib'8 sib' |
    fa''2 fa''4. fa''8 |
    sol''4 <<
      \tag #'basse {
        mib''8 s8 s2 s2 \ffclef "soprano/treble" <>^\markup\character Chœur
      }
      \tag #'vdessus { mib''4 r2 | r  }
    >> r4 do'' |
    fa''4. fa''8 sol''4 sol'' |
    lab''4 r
    \tag #'vdessus { r2 | R1*3 | }
  }
  \tag #'vhaute-contre {
    \clef "alto/G_8" r4 |
    R1*5 |
    r2 r4 r8 sib |
    fa'4. fa'8 fa'4. fa'8 |
    fa'2 r |
    r r4 fa'8 fa' |
    sib'2 sib'4. sib'8 |
    sib'4 sol' r2 |
    r r4 do' |
    fa'4. fa'8 sol'4 sol' |
    lab'4 r r2 |
    R1*3 |
  }
  \tag #'vtaille {
    \clef "tenor/G_8" r4 |
    R1*5 |
    r2 r4 r8 re'8 |
    do'4. do'8 do'4. do'8 |
    re'2 r |
    r r4 sib8 sib |
    fa'2 fa'4. fa'8 |
    sol'4 mib' r2 |
    r r4 lab |
    reb'?4. reb'8 reb'4 reb' |
    do'4 r r2 |
    R1*3 |
  }
  \tag #'vbasse {
    \clef "bass" r4 |
    R1*5 |
    r2 r4 r8 sib8 |
    la4. la8 la4. la8 |
    sib2 r2 |
    r r4 sib8 sib |
    re'2 re'4. re'8 |
    mib'4 mib r2 |
    r2 r4 lab |
    reb'4. reb'8 mib'4 mib' |
    lab4 r r2 |
    R1*3 |
  }
>>
