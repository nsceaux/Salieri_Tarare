\clef "alto" fa4-\sug\f |
fad4. fad8 fad4 fad |
sol8 sol4 sol8 la( fa! sol la) |
sib4. sib8 sib4. sib8 |
sib1-\sug\p~ |
sib~ |
sib2 r4 r8 sib'-\sug\ff |
la'2 la4. la8 |
sib2 r |
mib' r4 sib'\ff |
re'2 re'4. re'8 |
mib'4 mib' r2 |
lab2\p r4 lab'\ff |
reb'4. reb'8 mib'4 mib' |
do''4 r r\fermata lab4-\sug\p |
sib4.. sib16 sib4.. sib16 |
sol4. sib8 sib4 sib |
sib2 r |
