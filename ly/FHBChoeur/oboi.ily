\clef "treble" r4 |
R1*6 |
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''1 | re''2 }
  { do''1 | sib'2 }
>> r2 |
R1 |
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''1 | sol''4 mib''! }
  { re''1 | mib''4 sol' }
>> r2 |
r r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { lab''4 | fa''2 sol'' | lab''4 }
  { do''4 | reb''2 sib' | do''4 }
>> r4 r2\fermata |
R1*3 |
