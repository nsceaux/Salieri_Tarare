\key do \major
\tempo "Andante Maestoso" \midiTempo#100
\time 4/4 \once\override Staff.TimeSignature.stencil = ##f
\partial 4 s4 s1*6
\tempo "Allegro" s1*8
\tempo "Maestoso" s1*3 \bar "|."
