\clef "treble" <sol mib'>4.\f q8 <sib sol'>4. q8 |
<<
  \tag #'violino1 { <mib' sib'>2. }
  \tag #'violino2 { <sib sol'>2. }
>> r4 |
<<
  \tag #'violino1 {
    sol'8.\p sol'16 sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
    fa'8. fa'16 fa'8. fa'16 fa'8. fa'16 fa'8. fa'16 |
    fa'8. fa'16 fa'8. fa'16 fa'8. fa'16 fa'8. fa'16 |
    sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 sib'8. sib'16 |
    do''2
  }
  \tag #'violino2 {
    sib8.-\sug\p sib16 sib8. sib16 sib8. sib16 sib8. sib16 |
    sib8. sib16 sib8. sib16 sib8. sib16 sib8. sib16 |
    re'8. re'16 re'8. re'16 re'8. re'16 re'8. re'16 |
    mib'8. sib16 sib8. sib16 sib8. sib16 sol'8. sol'16 |
    lab'2
  }
>> r2 |
r4 r8. <>\f <<
  \tag #'violino1 { fa'16 fa'4 fa' | fad'2. }
  \tag #'violino2 { re'16 re'4 re' | do'2. }
>> r8 r16 <<
  \tag #'violino1 { re''16 | re''4 }
  \tag #'violino2 { <la' re'>16 | q4 }
>> r4 r2 |
<<
  \tag #'violino1 { sib'4 }
  \tag #'violino2 { <sib' re'>4 }
>> r4 r2 |
<<
  \tag #'violino1 {
    sol''8. sol''16 sol''8. sol''16 sol''8. sol''16 sol''8. sol''16 |
    fa''8. fa''16 fa''8. fa''16 fa''8. fa''16 fa''8. fa''16 |
    fa''8. fa''16 fa''8. fa''16 fa''8. fa''16 fa''8. fa''16 |
    sol''4
  }
  \tag #'violino2 {
    sib'8. sib'16 sib'8. sib'16 sib'8. sib'16 sib'8. sib'16 |
    sib'8. sib'16 sib'8. sib'16 sib'8. sib'16 sib'8. sib'16 |
    re''8. re''16 re''8. re''16 re''8. re''16 re''8. re''16 |
    mib''4
  }
>> r4 r2 |
r2 <<
  \tag #'violino1 { sol''4 }
  \tag #'violino2 { do'' }
>> r |
R1 |
r2 <<
  \tag #'violino1 { reb''4 }
  \tag #'violino2 { lab'! }
>> r4 |
r2 r8 r16 <sib'! fa''> q4 |
r2 <<
  \tag #'violino1 { re''4 }
  \tag #'violino2 { lab'! }
>> r4 |
r2 <<
  \tag #'violino1 {
    mib''8 r8\fermata mib''4\p |
    do''8\mf r\fermata fa''4\p re''\mf fa'' |
    mib'' r4 r2 |
    re''4 r r2 |
    r4 r8. sol''16 sol''2 |
    sol''4.\fermata do''8-!\p re''-! si'-! |
    do''4 sol'8\noBeam do'' re'' si' |
    do''4 sol'8\noBeam do'' re'' mi'' |
    fa''8. mi''16 re''4. do''8 |
    si'4. si'8 mi'' re'' |
    \grace re'' do'' si'16 la' si'4. mi''8 |
    do''4 do''8. la'16 \tuplet 3/2 { la'8 si'! dod'' } |
    re''8*2/3 la' re'' re'' la' re'' mi'' la' mi'' |
    fa'' la' fa'' fa'' sol' fa'' mi'' fa'' sol'' |
    do'' do'' do'' do'' re'' mi'' re'' do'' si' |
    do'' mi' do'' do'' mi' do'' dod'' mi' dod'' |
    re'' la' re'' re'' la' re'' mi'' la' mi'' |
    fa'' la' fa'' fa'' mi'' re'' mi''\mf fa'' sol'' |
    do''\cresc mi' do'' do'' mi' mi'' re'' do'' si' |
    <do'' mi'>4\!
  }
  \tag #'violino2 {
    sol'8 r\fermata sib'4-\sug\p |
    mib'8-\sug\mf r\fermata do''4-\sug\p fa'-\sug\mf re'' |
    sol' r4 r2 |
    <la' re'>4 r r2 |
    r4 r8. <re' si'>16 q2 |
    q4.\fermata sol'8\p sol' sol' |
    sol'4. sol'8 sol' sol' |
    sol'4 r r |
    r la'2 |
    re' si'4 |
    mi'4. mi'8 sold' sold' |
    la'4 mi' \tuplet 3/2 { dod'8 si la } |
    la'8*2/3 re' la' la' re' la' la' mi' la' |
    la' fa' la' sol' sol' sol' sol' fa' mi' |
    mi' mi' mi' mi' fa' sol' fa' mi' re' |
    do' mi' la' la' do' la' la' mi' la' |
    la' re' la' la' re' la' la' mi' la' |
    la' fa' la' sol' sol' sol' sol'-\sug\mf fa' mi' |
    mi'-\sug\cresc sol mi' mi' do' sol' fa' mi' re' |
    <mi' sol>4\!
 }
>> r4 r2 |
R1*2 |
r2 <<
  \tag #'violino1 { do''4 }
  \tag #'violino2 { la' }
>> r4 |
R1 |
r2 <<
  \tag #'violino1 { re''4 }
  \tag #'violino2 { la'4 }
>> r4 |
R1 |
<<
  \tag #'violino1 {
    si'4 r8 do'' re'' si' |
    do''4 sol'8 do''[ re'' si'] |
    do''4 sol'8 do''[ re'' mi''] |
    fa''8. mi''16 re''4. do''8 |
    si'4. si'8 mi'' re'' |
    \grace re'' do'' si'16 la' si'4. mi''8 |
    do''4 do''8. la'16 \tuplet 3/2 { la'8 si' dod'' } |
    re''8*2/3 la' re'' re'' la' re'' mi'' la' mi'' |
    fa'' la' fa'' fa'' sol' fa'' mi'' fa'' sol'' |
    do'' do'' do'' do'' re'' mi'' re'' do'' si' |
    do'' mi' do'' do'' mi' do'' dod'' mi' dod'' |
    re'' la' re'' re'' la' re'' mi'' la' mi'' |
    fa'' la' fa'' fa'' mi'' re'' mi''\mf fa'' sol'' |
    do'' mi' do'' do'' mi' mi'' re'' do'' si' |
    do''4\f~ do''8*2/3 mi'' fa'' sol'' la'' sol'' |
    fa''4~ fa''8*2/3 re'' mi'' fa'' sol'' fa'' |
    mi''4~ mi''8*2/3 do''' si'' do''' sol'' mi'' |
    la'' fa'' re'' do''4 re''8. si'16 |
    do''4 r r |
  }
  \tag #'violino2 {
    re'8 sol' sol' sol' sol' sol' |
    sol'4. sol'8 sol' sol' |
    sol'4 r r |
    r la'2 |
    re' si'4 |
    mi'4. mi'8 sold' sold' |
    la'4 mi' \tuplet 3/2 { dod'8 si la } |
    la'8*2/3 re' la' la' re' la' la' mi' la' |
    la' fa' la' sol' sol' sol' sol' fa' mi' |
    mi' mi' mi' mi' fa' sol' fa' mi' re' |
    do' mi' la' la' do' la' la' mi' la' |
    la' re' la' la' re' la' la' mi' la' |
    la' fa' la' sol' sol' sol' sol'-\sug\mf fa' mi' |
    mi' sol mi' mi' do' sol' fa' mi' re' |
    mi'4-\sug\f~ mi'8*2/3 mi' fa' sol' la' sol' |
    fa'4~ fa'8*2/3 re' mi' fa' sol' fa' |
    mi'4~ mi'8*2/3 do'' si' do'' sol' mi' |
    la' fa' la' mi' sol' mi' fa' mi' re' |
    mi'4 r r |
  }
>>
