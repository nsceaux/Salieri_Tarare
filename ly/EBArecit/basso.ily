\clef "bass" mib4.\f mib8 mib4. mib8 |
mib2. r4 |
mib\p mib mib mib |
re re re re |
sib, sib, sib, sib, |
mib mib mib mib |
lab,2 r |
r4 r8. sib,16\f sib,4 sib, |
la,2. r8 r16 fad |
fad4 r r2 |
sol4 r r2 |
\clef "tenor" <>_"Violoncelli soli" mib'8. mib'16 mib'8. mib'16 mib'8. mib'16 mib'8. mib'16 |
re'8. re'16 re'8. re'16 re'8. re'16 re'8. re'16 |
sib8. sib16 sib8. sib16 sib8. sib16 sib8. sib16 |
mib'4 r r2 |
\clef "bass" r2 <>_"tutti" lab,4 r |
R1 |
r2 fa4 r |
r2 r8 r16 re! re4 |
r2 fa4 r |
r2 mib8 r\fermata sol4\p |
lab8\mf r8\fermata la4\p sib\mf si |
do' r r2 |
fad4 r r2 |
r4 r8. sol16 sol2 |
sol4.\fermata \clef "tenor" <>^"Violoncelli" mi'8\p fa' re' |
mi'4. mi'8 fa' re' |
mi'4. \clef "bass" <>_"tutti" mi8 fa sol |
la8. sol16 fa4 fad |
sol sol, sold |
la sold mi |
la la, sol! |
fa fa dod |
re8. do16 si,4 do |
sol4 sol sol, |
la, la sol! |
fa fa dod |
re8. do16 si,4 do\mf |
sol\cresc sol sol, |
do\! r r2 |
R1*2 | \allowPageTurn
r2 fa4 r |
R1 |
r2 fad4 r |
R1 | \allowPageTurn
sol4. \clef "tenor" mi'8 fa' re' |
mi'4. mi'8 fa' re' |
mi'4. \clef "bass" mi8 fa sol |
la8. sol16 fa4 fad |
sol4 sol, sold |
la sold mi |
la la, sol! |
fa fa dod |
re8. do!16 si,4 do |
sol sol sol, |
la, la sol! |
fa fa dod |
re8. do!16 si,4 do\mf |
sol4 sol sol, |
do8*2/3\f mi sol do'4 mi' |
re'8. do'16 si4 si, |
do8*2/3 mi sol do'4 mi |
fa sol sol, |
do r r |
