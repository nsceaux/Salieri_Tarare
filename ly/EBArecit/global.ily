\key mib \major \midiTempo#132
\time 2/2 s1*7 s4
\tempo "Allegro" \midiTempo#100 s2. s1*17 \bar "||"
\key do \major \time 3/4 s4. \midiTempo#120 \tempo "Allegretto" s4. s2.*13
\time 4/4 \midiTempo#100 s1*7
\time 3/4 \midiTempo#120 s2.*19 \bar "|."
