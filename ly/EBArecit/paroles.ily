Belle Ir -- za, l’em -- pe -- reur or -- don -- ne
qu’en ce mo -- ment vous re -- ce -- viez la foi
d’un nou -- vel é -- poux qu’il vous don -- ne.

Un é -- poux ! un é -- poux à moi ?

Com -- man -- dant d’un corps ri -- di -- cu -- le !
a -- brè -- ge- nous ton gra -- ve pré -- am -- bu -- le.
Ce nou -- vel é -- poux, quel est- il ?

C’est du sé -- rail le mu -- et le plus vil.

Un mu -- et !

Un mu -- et !

J’ex -- pi -- re.

L’or -- dre est que cha -- cun se re -- ti -- re.

Moi ?

Vous.

Moi ?

Vous ; vous, Spi -- net -- te ; il y va des jours
de qui trou -- ble -- rait leurs a -- mours.

O jus -- te ciel !

Dis à ton maî -- tre
que le grand- prê -- tre
se -- ra sans doute as -- sez sur -- pris,
qu’à la plu -- ra -- li -- té des fem -- mes,
on ose a -- jou -- ter, chez les Bra -- mes,
la plu -- ra -- li -- té des ma -- ris,
on ose a -- jou -- ter, chez les Bra -- mes,
la plu -- ra -- li -- té des ma -- ris.

Vo -- tre con -- seil au roi pa -- raî -- tra d’un grand prix.
J’en fe -- rai vo -- tre cour.

Vous l’ou -- bli -- e -- rez peut- ê -- tre ?

Non.

Vous le ren -- drez mieux, l’ay -- ant deux fois ap -- pris.

Dis à ton maî -- tre,
que le grand- prê -- tre
se -- ra sans doute as -- sez sur -- pris,
qu’à la plu -- ra -- li -- té des fem -- mes,
on ose a -- jou -- ter, chez les Bra -- mes,
la plu -- ra -- li -- té des ma -- ris,
on ose a -- jou -- ter, chez les Bra -- mes,
la plu -- ra -- li -- té des ma -- ris.
