\clef "alto" sib4.\f sib8 sol4. sol8 |
mib2. r4 |
mib'\p mib' mib' mib' |
re' re' re' re' |
sib sib sib sib |
mib' mib' mib' mib' |
lab2 r |
r4 r8. sib16\f sib4 sib |
la2. r8 r16 fad' |
fad'4 r r2 |
sol'4 r r2 |
mib'8. mib'16 mib'8. mib'16 mib'8. mib'16 mib'8. mib'16 |
re'8. re'16 re'8. re'16 re'8. re'16 re'8. re'16 |
sib8. sib16 sib8. sib16 sib8. sib16 sib8. sib16 |
mib'4 r r2 |
r2 mib'4 r |
R1 | \allowPageTurn
r2 fa'4 r |
r2 r8. re'!16 re'4 |
r2 fa'4 r |
r2 sib8 r\fermata sol4-\sug\p |
lab8-\sug\mf r\fermata la4-\sug\p sib-\sug\mf si |
do'4 r r2 |
fad4 r r2 |
r4 r8. sol16 sol2 |
sol4.\fermata mi'8-\sug\p fa' re' |
mi'4. mi'8 fa' re' |
mi'4. mi8 fa sol |
la8. sol16 fa4. fad8 |
sol2 sold?4 |
la sold mi |
la la sol |
fa fa' dod' |
re'8. do'16 si4 do' |
sol4 sol sol |
la la sol |
fa fa' dod' |
re'8. do'16 si4 do'-\sug\mf |
sol-\sug\cresc sol sol |
do'4\! r r2 |
R1*2 |
r2 fa'4 r |
R1 |
r2 re'4 r |
R1 |
sol4 r8 mi' fa' re' |
mi'4. mi'8 fa' re' |
mi'4. mi8 fa sol |
la8. sol16 fa4 fad |
sol2 sold4 |
la sold mi |
la la sol |
fa fa' dod' |
re'8. do'!16 si4 do' |
sol' sol' sol |
la la sol |
fa fa' dod' |
re'8. do'!16 si4 do'-\sug\mf |
sol'4 sol' sol |
do'8*2/3\f mi' sol' do''4 mi'' |
re''8. do''16 si'4 si |
do'8*2/3 mi' sol' do''4 mi' |
fa' sol' sol |
do' r r |
