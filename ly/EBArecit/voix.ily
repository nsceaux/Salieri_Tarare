\clef "alto/G_8" <>^\markup\character Calpigi
R1 |
r2 r4 sib8 sib |
mib'2 mib'8. mib'16 re'8. do'16 |
sib4 sib r8 sib sib sib |
re'4. re'8 fa' re' sib lab |
sol2 sib8. do'16 re'8. mib'16 |
do'2 re'4. mib'8 |
sib2 sib8 r r4 |
\ffclef "soprano/treble" <>^\markup\character Astasie
r2 fad'16 sol' la'4 r8 |
r2 re''8 re'' re'' la' |
sib'!2
\ffclef "soprano/treble" <>^\markup\character Spinette
r4 sib'8. sib'16 |
mib''4. mib''8 mib''4 re''8 do'' |
sib'4 sib' r8 sib' sib' do'' |
re''4. mib''8 fa'' re'' sib' lab' |
sol'4 sol' <>^"Récit" mib''8 mib'' mib'' fa'' |
reb''4 mib''!8 sib' do''4 r |
\ffclef "alto/G_8" <>^\markup\character Calpigi
mib'!4 mib'8 mib' do'4 lab!8 sib! |
do'4 do'8 reb' lab!4
\ffclef "soprano/treble" <>^\markup\character Astasie
r8 lab'!16 lab' |
reb''4
\ffclef "soprano/treble" <>^\markup\character Spinette
r8 reb''16 lab'! sib'!4
\ffclef "soprano/treble" <>^\markup\character Astasie
r8 fa'' |
fa'' sib'! r4
\ffclef "alto/G_8" <>^\markup\character Calpigi
re'8 re' r16 re' re' fa' |
re'4 re'8 mib'! sib! sib\fermata
\ffclef "soprano/treble" <>^\markup\character Spin.
mib''!4
\ffclef "alto/G_8" <>^\markup\character Calp.
do'8 r\fermata
\ffclef "soprano/treble" <>^\markup\character Spin.
fa''4
\ffclef "alto/G_8" <>^\markup\character Calpigi
re'4 sol'8. sol'16 |
mib'!8 mib' r4 do'8 do' re' mib' |
re'4. re'8 re' la16 si do'8 do'16 re' |
si4 r
\ffclef "soprano/treble" <>^\markup\character Astasie
re''4 re''8 re'' |
sol''4.\fermata
\ffclef "soprano/treble" <>^\markup\character-text Spinette chanté
do''8 re'' si' |
do''4 sol'8 do'' re'' si' |
do''4 sol'8 do'' re'' mi'' |
fa'' mi'' re''4. do''8 |
si'4. si'8 mi'' re'' |
do''8. la'16 si'4. mi''8 |
\omit TupletBracket
do''4 do''8 la' \tuplet 3/2 { la'8 si' dod'' } |
re''2 mi''8. mi''16 |
fa''4 fa''8. re''16 \tuplet 3/2 { mi''8 fa'' sol'' } |
do''2 re''8. si'16 |
do''4. la'8 \tuplet 3/2 { la'8 si' dod'' } |
re''2 mi''8. mi''16 |
fa''4 fa''8. re''16 \tuplet 3/2 { mi''8 fa'' sol'' } |
do''2 re''8. si'16 |
do''4 r
\ffclef "alto/G_8" <>^\markup\character-text Calpigi ironiquement
sol4 sol8 sol |
do'4 r8 do' mi'4 mi'8 mi' |
mi'4 re'8 mi' do'4 r8 do'16 do' |
do'4 si8 do' la4 r |
\ffclef "soprano/treble" <>^\markup\character-text Spinette ironiquement
do''8 do'' do'' do'' fa''4. do''8 |
la'8 la' r4
\ffclef "alto/G_8" <>^\markup\character Calpigi
re'4 r |
\ffclef "soprano/treble" <>^\markup\character Spinette
la'8 la' la' la' re''8. la'16 do'' do'' do'' do'' |
si'4 r8 do'' re'' si' |
do''4 sol'8 do'' re'' si' |
do''4 sol'8 do'' re'' mi'' |
fa''8. mi''16 re''4. do''8 |
si'4 r8 si' mi'' re'' |
do'' la' si'4. re''8 |
do''4 do''8 la' \tuplet 3/2 { la'8 si' dod'' } |
re''2 mi''8. mi''16 |
fa''4 fa''8. re''16 \tuplet 3/2 { mi''8 fa'' sol'' } |
do''2 re''8. si'16 |
do''4. la'8 \tuplet 3/2 { la'8 si' dod'' } |
re''2 mi''8. mi''16 |
fa''4 fa''8. re''16 \tuplet 3/2 { mi''8 fa'' sol'' } |
do''2 re''8. si'16 |
do''4 r r |
R2.*4 |
