\clef "alto" <sol mi' do''>4-\sug\ff r q r |
q r q r |
\rp#4 { do'16 sol la si } |
do'4 do' do' do' |
do'2. lab4-\sug\sf |
sib reb'-\sug\p( do' sib) |
lab2 mi!-\sug\sf |
fa4 fa'2-\sug\p fa'4 |
mi'2 <fa' lab>-\sug\sf |
<sol mi'>4. do'8-\sug\p do'4 do' |
mi1~ |
mi~ |
mi |
do'~ |
do' |
sib2 r |
<re' fa'>2.-\sug\fp sib8( re') |
re2 mib |
fa fad |
sol1 |
<do' mib'>1\f |
<sib re'>4 re''-\sug\f sib sib |
sib2. r4 |
mib'2:8\p mib':8 |
re':8 re':8 |
re':8 re':8 |
mib':8 mib':8 |
mib':8 do':8 |
<fad' la'>1 |
sol'8-\sug\fp sib sib sib sib2:8-\sug\fp |
do':8\fp re':8 |
sol'4 sol\f sol' sol' |
sol2 sib'4\p r |
la' r\fermata la\ff r |
re'4 r r2 |
R1 |
r2 si4 r |
R1 |
r2 r8. la16-\sug\p la8. la16 |
red'4 r r2 |
R1 |
r2 mi'4 r |
r8. fa'16-\sug\f fa'8. fa'16 fa'4 r |
r8 r16 do'\f do'8. do'16 do'4 r |
r2 r4\fermata r4 |
la1\fp |
sol\fp |
mi2-\sug\f sol-\sug\p |
la1-\sug\f |
sol2-\sug\f~ sol-\sug\p |
fa2. r4 |
r8 fa'16-\sug\f re' sib8 fa' la2 |
sib2. fa'4-\sug\p |
do'2 sib4 do' |
fa4. fa'8-\sug\f fa'4 \grace fa'8 mib' reb'16 mib' |
reb'8 mib'16 fa' sol' lab' sib' do'' reb''4 reb' |
reb'
