\clef "treble" do''16\ff( sol' la' si' do'' re'' mi'' fa'') sol''( fa'' mi'' re'' do'' si' la' sol') |
do''( mi'' re'' do'' si' la' sol' fa') mi'( do'' si' la' sol' fa' mi' re') |
\rp#4 { do' sol la si } |
do'4 do' do' do' |
do'2 <<
  \tag #'violino1 {
    do''2\sf |
    reb''4 mib'(\p fa' sol') |
    lab'2 sib'\sf |
    lab'1\p |
    sol'2 si'\sf |
    do''4.
  }
  \tag #'violino2 {
    r4 lab\sf |
    sib2(-\sug\p lab4 sol) |
    do'4 fa' sol'2-\sug\sf |
    fa'1-\sug\p |
    mi'2 <fa' lab>-\sug\sf |
    <sol mi'>4.
  }
>> do'8\p do'4 do' |
<<
  \tag #'violino1 {
    do'1~ |
    do'~ |
    do'~ |
    do'~ |
    do' |
    sib2 r |
  }
  \tag #'violino2 {
    sol1~ |
    sol~ |
    sol |
    <fa' la'!>1~ |
    q |
    <fa' sib'>2 r | \allowPageTurn
  }
>>
sib'16\fp^"a rigore" sib' sib' sib' sib'4:16 sib'4 sib'8( re'') |
re'2:16 mib':16 |
fa':16 fad':16 |
sol':16 sol':16 |
la'16 sib' do'' re'' mib'' re'' do'' sib' la' sib' do'' sib' la' sol' fa' mib' |
re'4 <re' sib' sib''>4\f sib sib |
sib2. r4 |
<<
  \tag #'violino1 {
    sol'16\p sib sol' sib sol' sib sol' sib \rt#4 { sol' sib } |
    \ru#4 \rt#4 { lab' sib } |
    \ru#2 \rt#4 { sol' sib } |
    <sol mib' do''>4 r <sol mib' mib''> r |
    re'8 mi'16 fad' sol' la' si' dod'' re''8 re'' re'' re'' |
    re''2:16\fp re'':16\fp |
    mib''4:16\fp do'':16 la'2:16 |
    sib'4
  }
  \tag #'violino2 {
    mib'16-\sug\p sib mib' sib mib' sib mib' sib \rt#4 { mib' sib } |
    \ru#4 \rt#4 { fa' sib } |
    \ru#2 \rt#4 { mib' sib } |
    do'8 re'16 mib' fa' sol' la'! si' do'' re'' mib'' fa'' sol'' la''! si'' do''' |
    re'''2~ re'''8 la' fad' re' |
    sol'2:16-\sug\fp sol':16-\sug\fp |
    sol':16-\sug\fp fad':16 |
    sol'4
  }
>> <re' sib' sol''>4\fp q <re' sib' sib''> |
sol r <<
  \tag #'violino1 { <re'' re'''>4\p r | <la' mi'' dod'''>4 }
  \tag #'violino2 { sol''4-\sug\p r | <mi'' dod''>4 }
>> r4\fermata <la' mi'' dod'''>4\f r |
<la' fa'' re'''>4 r r2 |
R1 |
r2 <<
  \tag #'violino1 { sold'4 }
  \tag #'violino2 { mi' }
>> r4 |
R1 |
r2 r8. <<
  \tag #'violino1 { do''16\p do''8. do''16 | si'4 }
  \tag #'violino2 { mi'16-\sug\p mi'8. mi'16 | fad'4 }
>> r4 r2 |
R1 |
r2 <<
  \tag #'violino1 { si'4 }
  \tag #'violino2 { sol' }
>> r4 |
r8. <<
  \tag #'violino1 { si''16\f si''8. si''16 si''4 }
  \tag #'violino2 { fa''16-\sug\f fa''8. fa''16 fa''4 }
>> r4 |
r8 r16 <<
  \tag #'violino1 { do'''16\f do'''8. do'''16 do'''4 }
  \tag #'violino2 { <mi'' sol'>16\f q8. q16 q4 }
>> r4 |
r2 r4\fermata r4 |
r8 \grace sol' fa'16\f mi'32 fa' la'8 fa' do'4 r |
r8 \grace la' sol'16\f fa'32 sol' mi'8 do' sol4 r |
r8 \grace do'' sib'16\f la'32 sib' sol'8 mi' do'4 r |
r8 \grace re' do'16\f si32 do' la8 do' fa'4 r |
r8 \grace re'' dod''16\f sib'32 dod'' mi''8 mi'' mi'2\p |
la2. fa'8. fa'16 |
<re' sib'>4. q8 <fa' do''>4 q8. q16 |
<fa' re''>8 \grace do' sib16[\f la32 sib] re'8 fa' sib' fa' re'' re'' |
mi'4 <<
  \tag #'violino1 { fa'4 sol'2 | }
  \tag #'violino2 { fa'2 mi'4 | }
>>
fa'4. fa'8\f fa'4 \grace fa'8 mib' reb'16 mib' |
reb'8 mib'16 fa' sol' lab' sib' do'' reb''4 <<
  \tag #'violino1 { fa''4 | lab'' }
  \tag #'violino2 { reb'' | fa'' }
>>
