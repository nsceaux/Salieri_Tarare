\key do \major \time 4/4
\tempo "Allegro assai" \midiTempo#120 s1*4
\tempo "Plus lent du double" s1*12 \bar "|."
\key sib \major \tempo "Allegro assai" s1*19 \bar "||"
\key do \major s1*3 s2
\tempo "Andante" s2 s1*3
\tempo "Allegro" s1*2 s2. \bar "|."
\tempo "Andante Maëstoso" \key fa \major s4 s1*9
\tempo "Allegro assai" s1*2 s4 \bar ""
