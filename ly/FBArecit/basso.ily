\clef "bass" do4\ff r do r |
do r do r |
do do do do |
do do do do |
do1~ |
do-\sug\p~ |
do |
fa4 reb(\p do si,!) |
do1 |
do4. do8\p do4 do |
do1~ |
do~ |
do |
mib~ |
mib |
re2 r |
sib,2.\fp sib8 re' |
re re re re mib2:8 |
fa2:8 fad:8 |
sol:8 sol:8 |
fa!:8 fa:8 |
sib4 sib,\f sib, sib, |
sib,2 r |
mib2:8\p mib:8 |
re:8 re:8 |
re:8 re:8 |
mib:8 mib:8 |
mib:8 do:8 |
do:8 do:8 |
sib,:8\fp sib,:8\fp |
do:8\fp re:8 |
sol4 sol,\f sol sol |
sol,2 sib4\p r |
la r\fermata la,\ff r |
re4 r r2 |
R1 |
r2 re4 r |
R1 |
r2 r8. la,16\p la,8. la,16 |
red4 r r2 |
R1 |
r2 mi4 r |
r8. re16\f re8. re16 re4 r |
r8 r16 do\f do8. do16 do4 r |
r2 r4\fermata r |
fa1\fp |
mi\fp |
do2\f mi\p |
fa1\f |
mi2\f dod\p |
re2. r4 |
sib,2\f la, |
sib,2. sib,4\p~ |
sib, la, sib, do |
fa4. fa8\f fa4 mib |
reb8 mib16 fa sol lab sib do' reb'4 reb |
reb
