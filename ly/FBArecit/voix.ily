\clef "bass" \tag #'recit <>^\markup\italic {
  Les Eunuques sortent en courant.
}
R1*10
\tag #'recit <>^\markup\character-text Arthénée s’avance bien sombre.
r2 r4 sol8. sol16 |
do'2 sol4 sol |
mi4 r8 sol sol sol la! sib |
la4. la8 do' do' do' si! |
do'4. do'8 la la do' fa |
sib2 r |
\ffclef "bass" <>^\markup\character Atar
sib2. sib8 re' |
re4 re8 re mib4 mib8 mib |
fa4 fa fad fad8 fad |
sol2. sol8 sol |
la2 do'4. mib'8 |
re'4 sib r2 |
r r8 sib sib sib |
sol4 sol sol sol |
lab2. lab4 |
fa4 fa8 fa sib4 sib8 sib |
sol2 r4 sol |
do' do'8 do' do'4 la!8 la |
fad4 fad r re |
re'4. re'8 sib4 sol |
mib'4 do'8 do' la4 re'8 re' |
sib4 sol r2 |
r sol4 re' |
dod'2\fermata dod'8 dod' dod' re' |
la4
\ffclef "bass" <>^\markup\character-text Arthénée réfléchi
r8 fa la4 r8 la |
fa fa sol la re4 r16 fa fa sol |
la8 la la si sold sold r mi16 fad |
sold8 sold16 sold sold8 la mi mi r mi |
la la do'16 do' si do' la4 r |
r r8 fad16 fad si4 r8 si |
si4 si8 red' si4 si8 si |
fad4 la16 la la sol mi8 mi
\ffclef "bass" <>^\markup\character Atar
r8 sol16 la |
si4 r r8 re' re' si16 sol |
do'8 do' r4 r8 sol sol sol |
do' sol sol sol mi4\fermata
\ffclef "bass" <>^\markup\character Arthénée
do8. do16 |
fa2 fa8. fa16 la8. do'16 |
do'4 do r r8 do' |
sib4. sol8 do'8. do'16 do'8. do'16 |
la2 r8 la la si |
dod'2 r8 sol sib la |
fa4 fa r fa8. fa16 |
sib4. sib8 do'4 do'8. do'16 |
re'2 r4 re'8. re'16 |
mi4 fa sol do'8. do'16 |
fa2 r |
R1 |
r4
