Que veux- tu, roi d’Or -- mus ? et quel nou -- veau mal -- heur
te for -- ce d’ar -- ra -- cher un père à sa dou -- leur ?

Ah ! si l’es -- poir d’u -- ne promp -- te ven -- gean -- ce
peut l’a -- dou -- cir, re -- çois- en l’as -- su -- ran -- ce.
Dans mon sé -- rail on a sur -- pris
l’af -- freux meur -- tri -- er de ton fils.
Je tiens la vic -- time en -- chaî -- né -- e,
et veux que par toi- même el -- le soit con -- dam -- né -- e.
Dis un mot, le tré -- pas l’at -- tend.

A -- tar, c’é -- tait en l’ar -- rê -- tant…
sans a -- voir l’air de la con -- naî -- tre,
il fal -- lait poi -- gnar -- der le traî -- tre :
je trem -- ble qu’il ne soit trop tard !
Chaque ins -- tant, le moin -- dre re -- tard,
sur ton bras peut fer -- mer le piè -- ge.

Quel dé -- mon, quel dieu le pro -- tè -- ge ?
Tout me con -- fond de cet -- te part !

Son dé -- mon, c’est une â -- me for -- te,
un cœur sen -- sible et gé -- né -- reux,
que tout é -- meut, que rien n’em -- por -- te ;
un tel homme est bien dan -- ge -- reux,
un tel homme est bien dan -- ge -- reux !
