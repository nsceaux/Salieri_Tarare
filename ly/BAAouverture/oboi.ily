\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re'''2 fad'' |
    si''2. \grace la''8 sold'' fad''16 sold'' |
    la''4 \grace la''8 sold'' fad''16 sold'' la''4 \grace la''8 sold'' fad''16 sold'' |
    la''2. }
  { fad''2 fad'' |
    fad''2. fa''4 |
    mi'' fa'' mi'' fa'' |
    mi''2. }
>> \grace la''8 sol''! fad''16 sol'' |
fad''4 \grace fad''8 mi'' re''16 mi'' re''4 \grace re''8 dod'' si'16 dod'' |
re''2 r |
R1*4
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re'''2 fad'' |
    si''2. \grace la''8 sold'' fad''16 sold'' |
    la''4 \grace la''8 sold'' fad''16 sold'' la''4 \grace la''8 sold'' fad''16 sold'' |
    la''2. }
  { fad''2 la' |
    fad''2. fa''4 |
    mi'' fa'' mi'' fa'' |
    mi''2. }
>> \grace la''8 sol''! fad''16 sol'' |
fad''4 \grace fad''8 mi'' re''16 mi'' re''4 \grace re''8 dod'' si'16 dod'' |
re''4 r r2 |
R1*5 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re'''1~ | re'''~ | re'''4 }
  { fad''1~ | fad''~ | fad''4 }
>> r4 r2 |
R1 |
<>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''1~ | la''2 si''4 do''' | mi''1~ | mi''2 si' |
    dod''1 | dod'' | si'4 sold''2 sold''4 | la''1 |
    sold'' | la'' | sold''4 }
  { fad''1 | fad'' | si'1~ | si'2 sold' |
    la'1 | la' | sold'4 si'2 si'4 | dod''1 |
    si' | dod'' | si'4 }
  { s1*2 | <>-\sug\ff }
>> r4 r2 |
R1 |
<>^"Hautbois seuls"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''2 re''4. red''8 |
    mi''2. mid''4 |
    fad'' la''2 sold''8 fad'' |
    mi''4 }
  { la'2 si'4. sid'8 |
    dod''2. dod''4 |
    re'' fad''2 mi''8 re'' |
    dod''4 }
  { s1*2 | s4 s2\sf }
>> r4 r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''2 re''4. red''8 |
    mi''2. re''8 dod'' |
    si'4 dod'' mi''4. re''8 |
    dod''4 }
  { la'2 si'4. sid'8 |
    dod''2. si'8 la' |
    sold'4 la' dod''4. si'8 |
    la'4 }
>> r4 r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''2 re''4. red''8 |
    mi''2. mi''4 |
    fad''2 sold'' |
    la''4 mi''2 mi''4 |
    fad''2 sold'' |
    la''2. \grace la''8 sol''! fad''16 sold'' |
    fad''1 |
    fad''4 }
  { la'2 si'4. sid'8 |
    dod''2. dod''4 |
    re''1 |
    dod''2. dod''4 |
    re''1 |
    dod''2. mi''4 |
    mi''1 |
    re''4 }
  { s1*2 <>-\sug\ff }
>> r4 r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''1 }
  { re'' }
>>
<<
  \tag #'(oboe1 oboi) {
    dod''4( la'') r <>^"solo" la'' |
    la''( sold'') r sold'' |
    la''( sol''!) r sol'' |
    sol''( fa'') r fa'' |
    la''( re'') r re'' |
    fa'' mib''2 mib''4 |
    mib''4. fa''16 sol'' fa''4 mib'' |
  }
  \tag #'oboe2 { R1*7 }
>>
<>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''2 re'' |
    sol''2. \grace fa''8 mi'' re''16 mi'' |
    fa''4 \grace fa''8 mi'' re''16 mi'' fa''4 \grace fa''8 mi'' re''16 mi'' |
    fa''2. \grace fa''8 mib''! re''16 mib'' |
    re''4 \grace re''8 do'' sib'16 do'' sib'4 \grace sib'8 la' sol'16 la' |
    sib''2 re'' |
    sol''2. \grace fa''8 mi'' re''16 mi'' |
    fa''4 \grace fa''8 mi'' re''16 mi'' fa''4 \grace fa''8 mi'' re''16 mi'' |
    fa''2. fad''8 mi''?16 fad'' |
    sol''1 |
    sib'' |
    la'' | }
  { sib'2 fa' |
    re''2. re''4 |
    do'' re'' do'' re'' |
    do''2. r4 |
    R1 |
    re''2 fa' |
    re''2. reb''4 |
    do'' reb'' do'' reb'' |
    do''2. do''4 |
    sib'1 |
    dod'' |
    re'' | }
  { s1*5 s1\f s1\f s1 | s2. s4^\markup\whiteout "Hautbois et Clarinettes" |
    s1-\sug\ff s1-\sug\f s1-\sug\f }
>>
R1 |
r2 r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''4 | la''1 | }
  { mib''!4~ | mib'' re''8 dod'' re''4 r | }
  { s4-\sug\sf }
>>
r2 r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''4 | sold''1 | sold'' | sol''! | sol'' | }
  { la'4 | si'1 | si' | dod'' | dod'' | }
  { s4-\sug\sf s1-\sug\ff }
>> <>^"Hautbois seuls"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''2 sol''4. sold''8 |
    la''2. lad''4 |
    si''4 re'''2 dod'''8 si'' |
    la''4 }
  { re''2 mi''4. mid''8 |
    fad''2. fad''4 |
    sol'' si''2 la''8 sol'' |
    fad''4 }
  { s1*2 s4 s2.\sf }
>> r4 r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''2 sol''4. sold''8 |
    la''2. sol''8 fad'' |
    mi''4 fad'' la'' \grace la''8 sol'' fad''16 sol'' |
    fad''4 }
  { re''2 mi''4. mid''8 |
    fad''2. mi''8 re'' |
    dod''4 re'' fad'' \grace fad''8 mi'' re''16 mi'' |
    re''4 }
>> r4 r2 |
R1*3 |
<>^\markup\whiteout "Hautbois et Clarinettes" r2 \tag #'oboi <>_"unis"
re''16-\sug\f mi'' fad'' sol'' la'' si'' dod''' re''' |
re''4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''8. fad''16 fad''4 fad'' |
    fa''2. re'''4 |
    dod'''4 mi''16 fad'' mi'' fad'' sol'' fad'' mi'' re'' dod'' mi'' re'' dod'' |
    re''4 si'' la'' dod'' |
    re'' fad''8. fad''16 fad''4 fad'' |
    fa''2. re'''4 |
    dod''' mi''16 fad''! mi'' fad'' sol'' fad'' mi'' re'' dod'' mi'' re'' dod'' |
    re''4 si'' la'' dod'' | }
  { re''8. re''16 re''4 re'' |
    re''2. re''4 |
    mi''4 dod''16 re'' dod'' re'' mi'' re'' dod'' si' la' sol' fad' mi' |
    fad'4 sol'' fad'' mi'' |
    re'' re''8. re''16 re''4 re'' |
    re''2. re''4 |
    mi'' mi''16 fad'' mi'' fad'' sol'' fad'' mi'' re'' dod'' si' la' sol' |
    fad'4 r fad'' mi'' | }
>>
\tag #'oboi <>^"[unis]" re''4 re''8 re'' \grace mi'' re'' dod''16 re'' si'8 dod'' |
re'' la' la' fad'' \grace mi'' re'' dod''16 re'' si'8 dod'' |
re''4 r r2 |
re''4. re''32 mi'' fad'' sol'' la''4. \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''16 si''32 dod''' | re'''1~ | re'''~ | re''' | }
  { la''8 | fad''1~ | fad''~ | fad'' | }
  { s8 s1-\sug\p s-\sug\cresc s\! }
>>
\tag #'oboi <>^"unis" re'''4.-\sug\ff re''8 re''4 re'' |
re''2 r |
R1*2 |
r4 r8. <>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''16 fad''4. si''8 | si''2 }
  { red''16 red''4. red''8 | red''2 }
>>
