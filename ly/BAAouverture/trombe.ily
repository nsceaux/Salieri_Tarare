\clef "treble" \transposition re
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2 mi' | mi''2. }
  { do''2 mi' | do''2. }
>> r4 |
R1*8 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2 mi' | mi''2. }
  { do''2 mi' | do''2. }
>> r4 |
R1*9 |
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 mi'' do'' |
    sol'4. do''8 mi'4. sol'8 |
    do'4 }
  { do''4 mi'' do'' |
    sol'4. do''8 mi'4. sol'8 |
    do'4 }
>> r4 r2 |
R1 |
r4 <>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 mi'' do'' |
    sol'2 mi'' | 
    re''1~ |
    re''~ |
    re'' | }
  { do''4 mi'' do'' |
    sol'2 mi'' |
    \tag #'trombe \hideNotes re''1~ |
    re''~ |
    re'' \tag #'trombe \unHideNotes }
  { s2. s1 <>-\sug\ff }
>>
re''1 |
re'' |
re'' |
re'' |
re''
\twoVoices #'(tromba1 tromba2 trombe) <<
  { re''4 }
  { re''4 }
>> r4 r2 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'1 | }
  { sol1 | }
>>
R1*10 |
<>-\sug\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'1 | sol' | sol' | re'' | re'' | do''4 }
  { sol1 | sol | sol | sol' | sol' | mi'4 }
>> r4 r2 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { re''1 | re''4 }
  { do''1 | sol'4 }
>> r4 r2 |
R1*6 |
<>-\sug\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2 do'' | do''2. }
  { do''2 do'' | do''2. }
>> r4 |
R1*3 |
<>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2 do'' | do''2. }
  { do''2 do'' | do''2. }
>> r4 |
R1*2 |
\tag #'trombe <>^"[a 2]" do''1-\sug\ff |
re''-\sug\f |
sol'-\sug\f |
R1*4 |
\tag #'trombe <>^"[a 2]" re''1-\sug\ff |
re'' |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { re''1 | re'' | do''4 }
  { sol'1 | sol' | do''4 }
>> r4 r2 |
R1*11 |
r4 <>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''8. mi''16 mi''4 mi'' |
    do''1 |
    sol'4 re''8. re''16 re''4 re'' |
    do''2 sol' |
    do''4 mi''8. mi''16 mi''4 mi'' | }
  { do''8. do''16 do''4 do'' |
    do'1 |
    sol4 sol'8. sol'16 sol'4 sol' |
    do''2 sol' |
    do''4 do''8. do''16 do''4 do'' | }
>>
do''1 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'4 re''8. re''16 re''4 re'' |
    do''2 mi''4 re'' |
    do''4 }
  { sol'4 sol'8. sol'16 sol'4 sol' |
    do''2 do''4 sol' |
    mi' }
>> r4 r2 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 }
  { do'' }
>> r4 r2 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 }
  { do''4 }
>> r4
\twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'4 }
  { sol' }
>> r4 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do'4 }
  { do' }
>> r
\twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'4 }
  { sol' }
>> r |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''1~ |
    mi''~ |
    mi'' |
    mi''4. do'8 do'4 do' |
    do'2 }
  { sol'1~ |
    sol'~ |
    sol' |
    sol'4. do'8 do'4 do' |
    do'2 }
  { s1\p s\cresc s s-\sug\ff }
>> r2 |
R1*2 |
r4 r8 <>-\sug\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''8 mi''4. mi''8 | mi''2 }
  { mi'8 mi'4. mi'8 | mi'2 }
>>
