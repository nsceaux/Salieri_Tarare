\clef "treble"
<<
  \tag #'violino1 {
    re'''2  fad'' |
    <si' si''>2. \grace la''8 sold'' fad''16 sold'' |
    la''4 \grace la''8 sold'' fad''16 sold'' la''4 \grace la''8 sold'' fad''16 sold'' |
    la''2. \grace la''8 sol''! fad''16 sol'' |
    fad''4 \grace fad''8 mi'' re''16 mi'' re''4 \grace re''8 dod'' si'16 dod'' |
    re''2
  }
  \tag #'violino2 {
    re''2 fad' |
    <si' fad''>2. \grace la'8 sold' fad'16 sold' |
    la'4 \grace la'8 sold' fad'16 sold' la'4 \grace la'8 sold' fad'16 sold' |
    la'2. \grace la'8 sol'! fad'16 sol' |
    fad'4 \grace fad'8 mi' re'16 mi' re'4 \grace re'8 dod' si16 dod' |
    re'2
  }
>> re''16(\sf dod'' si' la' sol' fad' mi' re') |
dod'( re' dod' si) la4 mi''16( re'' dod'' si' la' sol' fad' mi') |
re'( mi' fad' mi') re'4 fad''16(\sf mi'' re'' dod'' si' la' sol' fad') |
mi'( fad' mi' re') dod'4 sol''16( fad'' mi'' re'' dod'' si' la' sol') |
<<
  \tag #'violino1 {
    la''2 r16 re'' mi'' fad'' sol'' la'' si'' dod''' |
    re'''2 fad'' |
    si''2. \grace la''8 sold'' fad''16 sold'' |
    la''4 \grace la''8 sold'' fad''16 sold'' la''4 \grace la''8 sold'' fad''16 sold'' |
    la''2. \grace la''8 sol''! fad''16 sol'' |
    fad''4 \grace fad''8 mi'' re''16 mi'' re''4 \grace re''8 dod'' si'16 dod'' |
    re''4
  }
  \tag #'violino2 {
    fad'2 r16 re' mi' fad' sol' la' si' dod'' |
    re''2 fad' |
    si'2. \grace la'8 sold' fad'16 sold' |
    la'4 \grace la'8 sold' fad'16 sold' la'4 \grace la'8 sold' fad'16 sold' |
    la'2. \grace la'8 sol'! fad'16 sol' |
    fad'4 \grace fad'8 mi' re'16 mi' re'4 \grace re'8 dod' si16 dod' |
    re'4
  }
>> re'8 re' \grace mi'8 re' dod'16 re' si8 dod' |
re' la la fad' \grace mi' re' dod'16 re' si8 dod' |
re' la la mi'\trill fad'\fz re' re' sol' |
la'\fz fad' fad' si' la'\fz fad' fad' dod'' |
re''\fz la' la' mi'' fad''\fz re'' re'' sol'' |
la''\fz fad'' fad'' si'' la''\fz fad'' fad'' dod''' |
re''' <<
  \tag #'violino1 {
    <re''' re''>4 q q q8 |
    <fad'' re'''>8 q4 q q q8 |
    re'''16\p dod''' re''' dod''' \rt#2 { re''' dod''' } \rt#4 { re''' dod''' } |
    re'''16 dod''' re''' dod''' \rt#2 { re''' dod''' } \rt#4 { re''' dod''' } |
    <fad'' re'''>8\f q4 q q q8 |
    <fad'' red'''>8 q4 q q q8 |
    mi'''2\ff si'' |
    sold'' mi'' |
    mi''' dod''' |
    la'' mi'' |
    mi'''4 sold''16 la'' si'' dod''' re''' dod''' si'' la'' sold'' fad'' mi'' re'' |
    dod''4 dod''16 si' la' sold' la' si' dod'' re'' mi'' fad'' sold'' la'' |
    sold''4 sold''16 la'' si'' dod''' re''' dod''' si'' la'' sold'' fad'' mi'' re'' |
    dod''4 dod''16 si' la' sold' la' si' dod'' re'' mi'' fad'' sold'' la'' |
    sold''4
  }
  \tag #'violino2 {
    <la' fad''>4 q q q8 |
    q q4 q q q8 |
    fad''16\p mid'' fad'' mid'' \rt#2 { fad'' mid'' } \rt#4 { fad'' mid'' } |
    fad'' mid'' fad'' mid'' \rt#2 { fad'' mid'' } \rt#4 { fad'' mid'' } |
    fad'8\f <la' fad''>4 q q q8 |
    q q4 q <si' fad''> <do'' fad''>8 |
    <si' mi''>2\ff si' |
    sold' mi' |
    mi'' dod'' |
    la' mi' |
    mi''8 <sold' si'>4 q q q8 |
    <la' dod''>8 q4 q q q8 |
    <sold' si'>8 q4 q q q8 |
    <la' dod''>8 q4 q q q8 |
    <sold' si'>4
  }
>> mi'16 fad' sold' la' si' la' sold' fad' mi' re' dod' si |
la1 |
R1*3 |
r4 <<
  \tag #'violino1 {
    \grace la'8 sold'\p fad'16 sold' la'4 sold'8 fad'16 sold' |
    la'4
  }
  \tag #'violino2 {
    <re' fa'>4-\sug\p <dod' mi'> <re' fa'> |
    <dod' mi'>
  }
>> r4 r2 |
R1*2 | \allowPageTurn
r4 <<
  \tag #'violino1 {
    \grace mi''8 re'' dod''16 re'' dod''4 \grace mi''8 re'' dod''16 re'' |
    dod''4
  }
  \tag #'violino2 {
    \grace dod''8 si' la'16 si' la'4 \grace dod''8 si' la'16 si' |
    la'4
  }
>> r4 r2 |
R1 |
<<
  \tag #'violino1 {
    <re' la' fad''>2\ff sold''8 sold'' \grace la'' sold'' fad''16 sold'' |
    la''2 r |
    <re' la' fad''>2 sold''8 sold'' \grace la'' sold'' fad''16 sold'' |
    la''2. \grace la''8 sol''! fad''16 sol'' |
    fad''8 fad''4 fad'' fad'' fad''8 |
    fad''
  }
  \tag #'violino2 {
    re'8-\sug\ff <<
      { fad''4 fad''8 mi'' mi''4 mi''8 | mi'' mi''4 mi''8 } \\
      { re''4 re''8 re'' re''4 re''8 | dod'' dod''4 dod''8 }
    >> la''16 sold'' fad'' mi'' re'' dod'' si' la' |
    re'8 <<
      { fad''4 fad''8 mi'' mi''4 mi''8 | } \\
      { re''4 re''8 re'' re''4 re''8 | }
    >>
    <dod'' mi''>4 q8. q16 q4 q |
    q8 q4 q q q8 |
    re''8
  }
>> si''8 re''' dod''' si'' la'' sold'' fad'' |
mi'' <<
  \tag #'violino1 {
    mi''4 mi'' mi'' mi''8 |
    mi'' mi'\p mi' mi' r la' la' la' |
    r sold' sold' sold' r sold' sold' sold' |
    r sol'! sol' sol' r sol' sol' sol' |
    r fa' fa' fa' r fa' fa' fa' |
    r re' re' re' r re' re' re' |
    r mib' mib' mib' r mib' mib' mib' |
    r mib' mib' mib' r mib' mib' mib' |
  }
  \tag #'violino2 {
    <re'' mi'>4 q q q8 |
    <mi' dod''>8 dod'[-\sug\p dod' dod'] r dod' dod' dod' |
    r re' re' re' r re' re' re' |
    r mi' mi' mi' r mi' mi' mi' |
    r re' re' re' r re' re' re' |
    r la la la r la la la |
    r sib sib sib r sib sib sib |
    r do' do' do' r do' do' do' |
  }
>>
<sib' sib''>2-\sug\ff <<
  \tag #'violino1 {
    re'' |
    <re' sib' sol''>2. \grace fa''8 mi'' re''16 mi'' |
    fa''4 \grace fa''8 mi'' re''16 mi'' fa''4 \grace fa''8 mi'' re''16 mi'' |
    fa''2. \grace fa''8 mib''! re''16 mib'' |
    re''4 \grace re''8 do'' sib'16 do'' sib'4 \grace sib'8 la' sol'16 la' |
    <sib' sib''>2\ff re'' |
    <re' sib' sol''>2.\f \grace fa''8 mi'' re''16 mi'' |
    fa''4 \grace fa''8 mi'' re''16 mi'' fa''4 \grace fa''8 mi'' re''16 mi'' |
    fa''2. fad''8 mi''?16 fad'' |
    <sol'' sib' re'>2\ff <sol sol'> |
    <sib' sib''>2\f dod' |
    re'''\f re' |
    mib'2.\ff re'4( |
    mib' re' mib'8)
  }
  \tag #'violino2 {
    re'2 |
    sol'2. \grace fa'8 mi' re'16 mi' |
    fa'4 \grace fa'8 mi' re'16 mi' fa'4 \grace fa'8 mi' re'16 mi' |
    fa'2. \grace fa'8 mib'! re'16 mib' |
    re'4 \grace re'8 do' sib16 do' sib4 \grace sib8 la sol16 la |
    sib2-\sug\ff re' |
    sol'2.-\sug\f \grace fa'8 mi' re'16 mi' |
    fa'4 \grace fa'8 mi' re'16 mi' fa'4 \grace fa'8 mi' re'16 mi' |
    fa'2. \grace sol'8 fad' mi'16 fad' |
    sol'8-\sug\ff <sib re'>8 q q q2:8 |
    <sol mi'>2:8-\sug\f q:8 |
    <la re'>1-\sug\f |
    sib2.-\sug\ff la4( |
    sib la sib8)
  }
>> r <sib' sib''>4(\sf |
<la'' la'>2.)
<<
  \tag #'violino1 {
    dod'4( |
    re' dod' re'8) r la''4(\sf |
    sold''2) sold-\sug\ff |
    re''' sold'' |
    sol''! la |
    mi''' sol'' |
    fad''4
  }
  \tag #'violino2 {
    sol4( |
    la sol la8) r la'4-\sug\sf( |
    si'8) <si re'>4\ff q q q8~ |
    q q4 q q q8 |
    dod' <dod' mi'>4 q q q8~ |
    q q4 q q q8 |
    re'4
  }
>> r4 r2 |
R1*2 |
r4 <<
  \tag #'violino1 {
    \grace re''8 dod''\p si'16 dod'' re''4 dod''8 si'16 dod'' |
    re''4
  }
  \tag #'violino2 {
    << { sib'4 la' sib' | la' } \\ { sol'-\sug\p fad'! sol' | fad' }>>
  }
>> r4 r2 |
R1*2 |
r2 re''16(\f dod'' si' la' sol' fad' mi' re') |
dod' re' dod' si la4 mi''16 re'' dod'' si' la' sol' fad' mi' |
re' mi' re' dod' re'4 fad''16 mi'' re'' dod'' si' la' sol' fad' |
mi' fad' mi' re' dod'4 sol''16 fad'' mi'' re'' dod'' si' la' sol' |
fad'2 <<
  \tag #'violino1 {
    re''16 mi'' fad'' sol'' la'' si'' dod''' re''' |
    re''4 re'''8. re'''16 re'''4 re''' |
    fa'''4.*5/6 mi'''16 re''' dod''' re'''4 re''' |
    dod''' dod'''16 re''' dod''' re''' mi''' re''' dod''' si'' la'' sol'' fad'' mi'' |
    fad''4
  }
  \tag #'violino2 {
    re'16 mi' fad' sol' la' si' dod'' re'' |
    fad'8 << { fad''8 fad'' fad'' fad''2:8 } \\ { re''8 re'' re'' re''2:8 } >> |
    << { fa''2:8 fa''8 } \\ { re''2:8 re''8[ re''] } >> \grace mi'' re'' dod''16 re'' |
    << { mi''2:8 mi'':8 | fad''4 } \\ { dod''2:8 dod'':8 | re''4 } >>

  }
>> <re' si' si''>4
<<
  \tag #'violino1 {
    <la'' la'>4 <la mi' dod''> |
    <re'' re'> re'''8. re'''16 re'''4 re''' |
    fa'''4.*5/6 mi'''16 re''' dod''' re'''4 re''' |
    dod''' dod'''16 re''' dod''' re''' mi''' re''' dod''' si'' la'' sol'' fad'' mi'' |
    fad''4
  }
  \tag #'violino2 {
    <fad'' la'>4 <la' dod'' mi''>4 |
    re''8 <<
      { fad''8 fad'' fad'' fad''2:8 | fa'':8 fa''8 fa'' re'' re'' } \\
      { re''8 re'' re'' re''2:8 | re'':8 re''8 re'' re' re' }
    >>
    <mi' dod''>8 <dod'' mi''> q q << { mi''2:8 | fad''4 } \\ { dod''2:8 | re''4 } >>
  }
>> <re' si' si''> 
<<
  \tag #'violino1 { <la'' re''>4 <dod'' mi' la>4 | }
  \tag #'violino2 { <fad'' re''>4 <la' dod'' mi''>4 | }
>>
<la fad' re''>4 re'8 re' \grace mi' re' dod'16 re' si8 dod' |
re' la la fad' \grace mi' re' dod'16 re' si8 dod' |
re'4. re'32 mi' fad' sol' la'4. la'16 si'32 dod'' |
re''4. re''32 mi'' fad'' sol'' la''4. la''16 si''32 dod''' |
<fad'' re'''>2:16\p q:16\cresc |
q:16 q:16 |
q:16 q:16 |
q4.\ff re'8 re'4 re' |
re'2. r8 la16\ff si |
do'4. do'8 do'4 do' |
si4 si16 dod' red' mi' fad'8 sold' lad' si' |
si4.. <<
  \tag #'violino1 { 
    <si' fad''>16 q4.. <si' si''>16 |
    \custosNote q4
  }
  \tag #'violino2 {
    <fad' red''>16 q4.. q16 |
    \custosNote q4
  }
>> \stopStaff
