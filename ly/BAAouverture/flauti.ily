\clef "treble" \tag #'flauti <>_"[a 2]" re'''2 fad'' |
si''2. \grace la''8 sold'' fad''16 sold'' |
la''4 \grace la''8 sold'' fad''16 sold'' la''4 \grace la''8 sold'' fad''16 sold'' |
la''2. \grace la''8 sol''! fad''16 sol'' |
fad''4 \grace fad''8 mi'' re''16 mi'' re''4 \grace re''8 dod'' si'16 dod'' |
re''2 r |
R1*3 |
r2 r16 \tag #'flauti <>_"[a 2]" re'' mi'' fad'' sol'' la'' si'' dod''' |
re'''2 fad'' |
si''2. \grace la''8 sold'' fad''16 sold'' |
la''4 \grace la''8 sold'' fad''16 sold'' la''4 \grace la''8 sold'' fad''16 sold'' |
la''2. \grace la''8 sol''! fad''16 sol'' |
fad''4 \grace fad''8 mi'' re''16 mi'' re''4 \grace re''8 dod'' si'16 dod'' |
re''4 r r2 |
R1*5 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''1~ | re'''~ | re'''4 }
  { fad''1~ | fad''~ | fad''4 }
>> r4 r2 |
R1 |
<>-\sug\f \twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''1 | red''' | mi'''2 si'' | }
  { fad''1 | fad'' | mi''2 si'' | }
  { s1*2 | <>-\sug\ff }
>>    
\tag #'flauti <>^"[a 2]" sold''2 mi'' |
mi''' dod''' |
la'' mi'' |
mi'''4 sold''16 la'' si'' dod''' re''' dod''' si'' la'' sold'' fad'' mi'' re'' |
dod''4 dod''16 si' la' sold' la' si' dod'' re'' mi'' fad'' sold'' la'' |
sold''4 sold''16 la'' si'' dod''' re''' dod''' si'' la'' sold'' fad'' mi'' re'' |
dod''4 dod''16 si' la' sold' la' si' dod'' re'' mi'' fad'' sold'' la'' |
sold''4 r r2 |
R1 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { dod''2 re''4. red''8 |
    mi''2. mid''4 |
    fad'' la''2 sold''8 fad'' |
    mi''4 }
  { la'2 si'4. sid'8 |
    dod''2. dod''4 |
    re'' fad''2 mi''8 re'' |
    dod''4 }
  { s1*2 | s4 s2\sf }
>> r4 r2 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { dod''2 re''4. red''8 |
    mi''2. re''8 dod'' |
    si'4 dod'' mi''4. re''8 |
    dod''4 }
  { la'2 si'4. sid'8 |
    dod''2. si'8 la' |
    sold'4 la' dod''4. si'8 |
    la'4 }
>> r4 r2 |
R1*2 | \allowPageTurn
\tag #'flauti <>^"a due" fad''2\ff sold''8 sold'' \grace la'' sold'' fad''16 sold'' |
la''2 la''16 sold'' fad'' mi'' re'' dod'' si' la' |
fad''2 sold''8 sold'' \grace la'' sold'' fad''16 sold'' |
la''2. \grace la''8 sol''! fad''16 sol'' |
fad''4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { fad'''2 fad'''4 |
    fad''' re'''8 dod''' si'' la'' sold'' fad'' |
    mi''4 mi'''2 mi'''4 |
    mi''' }
  { dod'''2 dod'''4 |
    re''' r r2 |
    r4 si''2 si''4 |
    dod''' }
>> r r2 |
R1*6 | \allowPageTurn
\tag #'flauti <>_"unis" sib''2-\sug\ff
\twoVoices #'(flauto1 flauto2 flauti) << fa'' re'' >> |
sol''2. \grace fa''8 mi'' re''16 mi'' |
fa''4 \grace fa''8 mi'' re''16 mi'' fa''4 \grace fa''8 mi'' re''16 mi'' |
fa''2. \grace fa''8 mib''! re''16 mib'' |
re''4 \grace re''8 do'' sib'16 do'' sib'4 \grace sib'8 la' sol'16 la' |
sib''2\f re'' |
sol''2.\f \grace fa''8 mi'' re''16 mi'' |
fa''4 \grace fa''8 mi'' re''16 mi'' fa''4 \grace fa''8 mi'' re''16 mi'' |
fa''2. fad''8 mi''?16 fad'' |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''1 | sib'' | la'' | }
  { sib'1 | dod'' | re'' }
  { s1-\sug\ff s1-\sug\f s1-\sug\f }
>>
R1 |
r2 r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { sib''4 | la''1 }
  { mib''!4~ | mib'' re''8 dod'' re''4 r | }
  { s4-\sug\sf }
>>
r2 r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { la''4 |
    sold''1 |
    re''' |
    dod''' |
    mi'''2 sol'' |
    fad''2 sol''4. sold''8 |
    la''2. lad''4 |
    si''4 re'''2 dod'''8 si'' |
    la''4 }
  { la'4 |
    si'1 |
    sold'' |
    sol''! |
    dod'' |
    re''2 mi''4. mid''8 |
    fad''2. fad''4 |
    sol'' si''2 la''8 sol'' |
    fad''4 }
  { s4-\sug\sf | s1*6-\sug\ff | s4 s2\sf }
>> r4 r2 |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { fad''2 sol''4. sold''8 |
    la''2. sol''8 fad'' |
    mi''4 fad'' la'' \grace la''8 sol'' fad''16 sol'' |
    fad''4 }
  { re''2 mi''4. mid''8 |
    fad''2. mi''8 re'' |
    dod''4 re'' fad'' \grace fad''8 mi'' re''16 mi'' |
    re''4 }
>> r4 r2 |
R1*3 |
r2 \tag #'flauti <>^"unis" re''16-\sug\f mi'' fad'' sol'' la'' si'' dod''' re''' |
re''4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''8. re'''16 re'''4 re''' |
    fa'''4.*5/6 mi'''16 re''' dod''' re'''4 re''' |
    dod'''4 dod'''16 re''' dod''' re''' mi''' re''' dod''' si'' la'' sol'' fad'' mi'' |
    fad''4 si'' la'' dod'' |
    re'' re'''8. re'''16 re'''4 re''' |
    fa'''4.*5/6 mi'''16 re''' dod''' re'''4 re''' |
    dod''' dod'''16 re''' dod''' re''' mi''' re''' dod''' si'' la'' sol'' fad'' mi'' |
    fad''4 si'' la'' dod'' | }
  { fad''8. fad''16 fad''4 fad'' |
    fa''2. re''4 |
    mi''4 mi''16 fad''! mi'' fad'' sol'' fad'' mi'' re'' dod'' mi'' re'' dod'' |
    re''4 r fad'' mi'' |
    re'' fad''8. fad''16 fad''4 fad'' |
    fa''2. re''4 |
    mi'' mi''16 fad''! mi'' fad'' sol'' fad'' mi'' re'' dod'' mi'' re'' dod'' |
    re''4 r fad'' mi'' | }
>>
\tag #'flauti <>^"[unis]" re''4 re''8 re'' \grace mi'' re'' dod''16 re'' si'8 dod'' |
re'' la' la' fad'' \grace mi'' re'' dod''16 re'' si'8 dod'' |
re''4 r r2 |
re''4. re''32 mi'' fad'' sol'' la''4. \twoVoices #'(flauto1 flauto2 flauti) <<
  { la''16 si''32 dod''' | re'''1~ | re'''~ | re''' | }
  { la''8 | fad''1~ | fad''~ | fad'' | }
  { s8 s1\p s\cresc s\! }
>>
\tag #'flauti <>^"unis" re'''4.\ff re''8 re''4 re'' |
re''2 r |
R1*2 |
r4 r8. <>-\sug\ff \twoVoices #'(flauto1 flauto2 flauti) <<
  { fad''16 fad''4. si''8 | si''2 }
  { red''16 red''4. red''8 | red''2 }
>>
