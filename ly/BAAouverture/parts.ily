\piecePartSpecs
#`((flauti #:score-template "score-flauti")
   (oboi #:score-template "score-oboi")
   (clarinetti #:score-template "score-oboi")
   (fagotti #:score-template "score-fagotti")
   
   (violino1)
   (violino2)
   (viola)
   (basso)

   (trombe #:score-template "score-trombe"
          #:instrument , #{\markup\center-column { Trompettes in D }#})
   (timpani #:score "score-timpani")
   (corni #:score-template "score-trombe"
          #:instrument "Cors in D")
   (silence #:on-the-fly-markup , #{\markup\tacet #}))
