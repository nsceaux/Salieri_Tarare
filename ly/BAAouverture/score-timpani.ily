\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Timballes \smaller in D }
    } << \global \includeNotes "timpani" >>
    \new DrumStaff \with {
      drumStyleTable = #percussion-style
      \override StaffSymbol.line-count = #1
      instrumentName = \markup\center-column {
        Tambour et Cimballes
      }
    } << \global \includeNotes "tambour" >>
  >>
  \layout { indent = \largeindent }
}
