\tempo "Allegro Presto" \midiTempo#120
\tag #'all \key re \major
\time 4/4 s1*61 \bar "||"
\tag #'all \key sib \major s1*20 \bar "||"
\tag #'all \key re \major s1*32 s2 \bar "|."
