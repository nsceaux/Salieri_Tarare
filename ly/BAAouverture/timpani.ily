\clef "bass" re2 r |
re r4 re |
la, re la, re |
la,2 r4 re |
mi mi la, la, |
re2 r |
R1*4 |
re2 r |
re r4 re |
la, re la, re |
la,2 r |
r2 r4 la, |
re r r2 |
R1*11 |
re2-\sug\ff r |
re r2 |
la, r |
la, r |
re r |
la, r |
re r |
la, r |
re:16 re:16 |
la,2 r |
R1*24 |
re2-\sug\ff r |
re r |
R1*3 |
re2-\sug\ff r |
re-\sug\f r |
R1*2 |
re2-\sug\ff r |
re-\sug\f r |
la,-\sug\f r |
R1*6 |
la,2:16 la,:16 |
la,:16 la,:16 |
re4 r r2 |
R1*11 |
re4-\sug\f re8. re16 re4 re |
re2:16 re:16 |
la,4 r r la, |
re re la, la, |
re re8. re16 re4 re |
re2:16 re:16 |
la,4 r r la, |
re re8. re16 la,4 la, |
re r r2 |
re4 r r2 |
re4 r r2 |
R1 |
re2:16-\sug\p re:16-\sug\cresc |
re:16 re:16 |
re:16 re:16 |
re4.-\sug\ff re8 re4 re |
re2 r |
R1*3 |
r2
