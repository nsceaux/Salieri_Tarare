\clef "bass" re8 re re re re2:8 |
re8 re' re' re' re'2:8 |
dod'8 dod' re' re' dod' dod' re' re' |
dod'2.:8 si8 si |
la la sol sol fad fad mi mi |
re2 r |
la, r |
re2 re'16\sf( dod' si la sol fad mi re) |
dod( re dod si,) la,4 mi'16( re' dod' si la sol fad mi) |
re2 r |
re8 re re re re2:8 |
re8 re' re' re' re'2:8 |
dod'8 dod' re' re' dod' dod' re' re' |
dod'2. si8 si |
la la sol sol fad fad mi mi |
re4 re8 re re4 si,8 dod |
re la, la, fad re4 si,8 dod |
re la, la, mi fad\sf re re sol |
la\sf fad fad si la\sf fad fad dod' |
re'\sf la la mi fad\sf re re sol |
la\sf fad fad si la\sf fad fad dod' |
re'4 re' fad' re' |
la4. re'8 fad4. la8 |
re4\p re' fad' re' |
la4. re'8 fad4. la8 |
<>-\sug\f re4 re' fad' re' |
do'4. do'8 si4. la8 |
sold\ff mi sold mi sold mi sold mi |
si mi sold si re'8*2/3 dod' si re' dod' si |
dod'8 la mi la dod' la mi la |
dod' la mi la dod' mi' dod' la |
mi mi' mi mi' mi mi' mi mi' |
mi mi' mi mi' mi mi' mi mi' |
mi mi' mi mi' mi mi' mi mi' |
mi mi' mi mi' mi mi' mi mi' |
mi4 mi16 fad sold la si la sold fad mi re dod si, |
la,1 |
<>^"Violoncelli" la,8 la la la la2:8 |
la:8 la:8 |
la:8 la:8 |
la8 la,-\sug\p la, la, la,2:8 |
la,8 la la la la2:8 |
la:8 la:8 |
mi2:8 mi:8 |
la,4 sold8 sold la la sold sold |
la2:8 la:8 |
la:8 la:8 |
la,8\ff <>^"tutti" la la la la la la la |
la2:8 la:8 |
la,8 la la la la2:8 |
la:8 la:8 |
lad8 fad lad fad lad fad lad fad |
si1 |
sold8 mi sold mi sold mi sold mi |
la4 r la\p r |
si r si r |
dod' r la r |
re' r re r |
fa r fa r |
sol r sol r |
la r la, r |
sib,8\ff sib sib sib sib2:8 |
sib:8 sib:8 |
la8 la sib sib la la sib sib |
la2:8 la8 la sol sol |
fa fa mib fa re re do do |
sib,-\sug\ff sib sib sib sib2:8 |
sib:8-\sug\f sib:8 |
la8 la sib sib la la sib sib |
la2:8 la:8 |
sol,8-\sug\ff sol sol sol sol2:8 |
sol,8-\sug\f sol sol sol sol2:8 |
fa2:8\f fa:8 |
sol2.\ff fad4( |
sol fad sol8) r sol4-\sug\sf( |
fa2.) mi4( |
fa mi fa8) r fa4-\sug\sf( |
mi2:8)\ff mi:8 |
mi:8 mi:8 |
la,:8 la,:8 |
la,:8 la,:8 |
<>^"Violoncelli" re8 re' re' re' re'2:8 |
re':8 re':8 |
re':8 re':8 |
re'8 re-\sug\p re re re2:8 |
re:8 re:8 |
re:8 re:8 |
la,2:8 la,:8 |
re2 r |
<>^"tutti" la,2\f r |
re re'16 dod' si la sol fad mi re |
dod re dod si, la,4 mi'16 re' dod' si la sol fad mi |
re2:8 re:8 |
si,:8 si,:8 |
sib,:8 sib,:8 |
la,:8 la,:8 |
re4 sol la la, |
si,2:8 si,:8 |
sib,:8 sib,:8 |
la,:8 la,:8 |
re4 sol la la, |
re re8 re re4 si,8 dod |
re4 la,8 fad re4 si,8 dod |
re4. re32 mi fad sol la4. la,16 si,32 dod |
re4. re32 mi fad sol la4. la16 si32 dod' |
re'8\p re re\cresc re re2:8 |
re:8 re:8 |
re:8 re:8 |
re4.\ff re8 re4 re |
re2. r8 la,16\ff si, |
do4. do8 do4 do |
si, si,16 dod red mi fad8 sold lad si |
si,4.. si,16 si,4.. si,16 |
\custosNote si,4
\stopStaff
