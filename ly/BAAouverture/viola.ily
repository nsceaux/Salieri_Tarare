\clef "alto" <<
  { la'8 la' la' la' la'2:8 | } \\
  { fad'8 fad' fad' fad' fad'2:8 | }
>>
<fad' re''>2:8 q4:8 fa'8 fa' |
mi' mi' fa' fa' mi' mi' fa' fa' |
mi'2. re'8 re' |
dod' dod' si si la la sol sol |
fad2 r |
mi r |
re2 re'16(\sf dod' si la sol fad mi re) |
dod2 mi'16( re' dod' si la sol fad mi) |
re2 r |
re8 <fad' la'> q q q2:8 |
<fad' re''>2.:8 fa'8 fa' |
mi' mi' fa' fa' mi' mi' fa' fa' |
mi'2.:8 re'8 re' |
dod' dod' si si la la sol sol |
fad4 re'8 re' \grace mi' re' dod'16 re' si8 dod' |
re' la la fad' \grace mi' re' dod'16 re' si8 dod' |
re' la la mi'\trill fad'-\sug\fz re' re' sol' |
la'\fz fad' fad' si' la'\fz fad' fad' dod'' |
re''\fz la' la' mi' fad'\fz re' re' sol' |
la'\fz fad' fad' si' la'\fz fad' fad' dod'' |
re''4 re' fad' re' |
la4. re'8 fad4. la8 |
re4\p re' fad' re' |
la4. re'8 fad4. la8 |
re4\f re' fad' re' |
do'4. do'8 si4. la8 |
sold-\sug\ff mi sold mi sold mi sold mi |
si mi sold si re'8*2/3 dod' si re' dod' si |
dod'8 la mi la dod' la mi la |
dod' la mi la dod' mi' dod' la |
mi' mi' si re' si re' si re' |
la dod' la dod' la dod' la dod' |
si re' si re' si re' si re' |
la dod' la dod' la dod' la dod' |
sold8 si mi'16 fad' sold' la' si' la' sold' fad' mi' re' dod' si |
la1 |
<<
  { dod'2 re'4. red'8 |
    mi'2. mid'4 |
    fad' la'2 sold'8 fad' |
    mi'4 fa' mi' fa' |
    dod'2 re'4. red'8 |
    mi'2. re'8 dod' |
    si4 dod' mi'4. re'8 |
    dod'4 fa' mi' fa' |
    dod'2 re'4. red'8 |
    mi'2. mid'4 |
    fad'8 } \\
  { la2 si4. sid8 |
    dod'2. dod'4 |
    re' fad'2\sf mi'8 re' |
    dod'4 re'-\sug\p dod' re' |
    la2 si4. sid8 |
    dod'2. si8 la |
    sold4 la dod'4. si8 |
    la4 re' dod' re' |
    la2 si4. sid8 |
    dod'2. dod'4 |
    re'8-\sug\ff }
>> fad' re' dod' si si' si' si' |
la' mi' dod' mi' la'16 sold' fad' mi' re' dod' si la |
re8 fad'[ re' dod'] si si' si' si' |
la'4 la'8. la'16 la'4 la' |
lad'8 fad' lad' fad' lad' fad' lad' fad' |
re''1 |
<si' mi'>8 q4 q q q8 |
<mi' dod''>4 r la-\sug\p r |
si r si r |
dod' r la r |
re' r re' r |
fa r fa r |
sol r sol r |
la r la r |
<fa' re''>2:8-\sug\ff <sib re'>2:8 |
q2.:8 reb'8 reb' |
do' do' reb' reb' do' do' reb' reb' |
do'2.:8 sib8 sib |
la la sol sol fa fa mib mib |
re-\sug\ff re'' re'' re'' <re'' fa'>2:8 |
<re' re''>2.:8-\sug\f reb'8 reb' |
do' do' reb' reb' do' do' reb' reb' |
do'2.:8 do'8 do' |
sib-\sug\ff <sol re'> q q q2:8 |
dod'2:8-\sug\f dod':8 |
<re' la>1-\sug\f |
sol2.-\sug\ff fad4( |
sol fad sol8) r mib'!4-\sug\sf~ |
mib' re'8 dod' re'4 mi( |
fa mi fa8) r fa'4(-\sug\sf |
mi'8) mi4\ff mi mi mi8~ |
mi mi4 mi mi mi8 |
la la4 la la la8~ |
la la4 la la la8 |
<<
  { fad'2 sol'4. sold'8 |
    la'2. lad'4 |
    si'4 re''2 dod''8 si' |
    la'4 } \\
  { re'2 mi'4. mid'8 |
    fad'2. fad'4 |
    sol' si'2\sf la'8 sol' |
    fad'4 }
>> r4 r2 |
<<
  { fad'2 sol'4. sold'8 |
    la'2. sol'8 fad' |
    mi'4 fad' la' sol' |
    fad'2 } \\
  { re'2 mi'4. mid'8 |
    fad'2. mi'8 re' |
    dod'4 re' fad' mi' |
    re'2 }
>> r2 |
mi2-\sug\f r |
re re'16 dod' si la sol fad mi re |
dod2 mi'16 re' dod' si la sol fad mi |
re8 <la fad'>[ q q] q2:8 |
q8 <fad' re''>[ q q] q2:8 |
<fa' re''>2:8 q8 q <re' re''> q |
<mi' dod''>2:8 q:8 |
<fad' re''>4 sol la la |
si8 <fad' re''> q q q2:8 |
<fa' re''>2:8 q8 q <re' re''> q |
<mi' dod''>2:8 q:8 |
<fad' re''>4 sol la la |
re'4 re'8 re' \grace mi' re' dod'16 re' si8 dod' |
re' la la fad' \grace mi' re' dod'16 re' si8 dod' |
re'4. re32 mi fad sol la4. la16 si32 dod' |
re'4. re'32 mi' fad' sol' la'4. la'16 si'32 dod'' |
re''8-\sug\p <la' fad'>4 q-\sug\cresc q q8~ |
q q4 q q q8~ |
q q4 q q q8 |
re'4.-\sug\ff re'8 re'4 re' |
re'2. r8 la16\ff si |
do'4. do'8 do'4 do' |
si4 si16 dod' red' mi' fad'8 sold' lad' si' |
si4.. si16 si4.. si16 |
\custosNote si4
\stopStaff
