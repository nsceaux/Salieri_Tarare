\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \flautiInstr } <<
        \new Staff << \global \keepWithTag #'flauto1 \includeNotes "flauti" >>
        \new Staff << \global \keepWithTag #'flauto2 \includeNotes "flauti" >>
      >>
      \new GrandStaff \with {
        instrumentName = \markup\center-column { Hautbois et Clarinettes }
        shortInstrumentName = \markup\center-column { Htb Cl }
      } <<
        \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
      >>
      \new GrandStaff \with { \fagottiInstr } <<
        \new Staff << \global \keepWithTag #'fagotto1 \includeNotes "fagotti" >>
        \new Staff << \global \keepWithTag #'fagotto2 \includeNotes "fagotti" >>
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with {
        instrumentName = \markup\center-column { Cors et Trompettes \smaller in D }
        shortInstrumentName = \markup\center-column { Cor. Tr. }
      } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \keepWithTag #'trompettes \global
          \keepWithTag #'tromba1 \includeNotes "trombe"
        >>
        \new Staff <<
          \keepWithTag #'trompettes \global
          \keepWithTag #'tromba2 \includeNotes "trombe"
        >>
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Timballes \smaller in D }
        shortInstrumentName = "Timb."
      } << \global \includeNotes "timpani" >>
      \new DrumStaff \with {
        drumStyleTable = #percussion-style
        \override StaffSymbol.line-count = #1
        instrumentName = \markup\center-column {
          Tambour et Cimballes \smaller in D
        }
        shortInstrumentName = \markup\center-column { Tamb. Cymb. }
      } << \global \includeNotes "tambour" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'basso \includeNotes "basso"
        \origLayout {
          s1*7\pageBreak
          s1*7\pageBreak
          s1*7\break s1*8\pageBreak
          s1*6\break s1*9\pageBreak
          s1*6\break s1*7\pageBreak
          s1*7\break s1*8\pageBreak
          s1*9\break s1*8\pageBreak
          s1*6\pageBreak
          s1*7\pageBreak
          s1*7\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
