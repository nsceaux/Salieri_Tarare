\clef "bass" re1~ |
re4 re'2 \clef "tenor" \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa'4 |
    mi' fa' mi' fa' |
    mi'2. re'4 |
    dod' si la sol |
    fad2 }
  { re'4 |
    dod' re' dod' re' |
    dod'2. si4 |
    la sol fad mi |
    re2 }
>> r2 |
\clef "bass" la,2 r |
re2 re'16\sf( dod' si la sol fad mi re) |
dod( re dod si,) la,4 mi'16( re' dod' si la sol fad mi) |
re2 r |
\clef "tenor" \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fad'1~ fad'2. fa'4 |
    mi' fa' mi' fa' |
    mi'2. re'4 |
    dod' si la sol |
    fad }
  { re'1~ |
    re'2. re'4 |
    dod' re' dod' re' |
    dod'2. si4 |
    la sol fad mi |
    re }
>> r4 r2 |
R1*5 |
re'4 re' fad' re' |
la4. re'8 fad4. la8 |
re4\p re' fad' re' |
la4. re'8 fad4. la8 |
<>-\sug\f re4 re' fad' re' |
do'4. do'8 si4. la8 |
sold\ff mi sold mi sold mi sold mi |
si mi sold si re'8*2/3 dod' si re' dod' si |
dod'8 la mi la dod' la mi la |
dod' la mi la dod' mi' dod' la |
\clef "bass" \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { si1 | dod' | si | dod' | si4 }
  { sold1 | la | sold | la | sold4 }
>> mi16 fad sold la si la sold fad mi re dod si, |
la,1 |
R1*7 | \allowPageTurn
\clef "tenor" r4 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa'4 mi' fa' | mi' }
  { re'4 dod' re' | dod' }
>> r4 r2 |
R1 |
\clef "bass" la,1-\sug\ff |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la,4 dod'2 dod'4 | re'1 | dod'4 la2 la4 | }
  { la,4 mi2 mi4 | fad2 sold | la4 la2 la4 | }
>>
\tag#'fagotti <>^"[a 2]" lad8 fad lad fad lad fad lad fad |
si1 |
sold8 mi sold mi sold mi sold mi |
<>-\sug\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { dod'1 | re' | mi'2 dod' | }
  { la1 | si | dod'2 la | }
>>
re'2 re' |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la1 | sib | do' | }
  { fa1 | sol | la | }
>>
\clef "tenor" <>-\sug\ff \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { re'1 | re'2. reb'4 | do' reb' do' reb' | do'2. sib4 |
    la sol fa mib | re'1 | re'2. reb'4 | do' reb' do' reb' |
    do'1 | sib | sib | la1 | }
  { sib1 | sib2. sib4 | la sib la sib | la2. sol4 |
    fa mib re do | sib1 | sib2. sib4 | la sib la sib |
    la1 | sol | sol | fa | }
  { s1*5 s1-\sug\ff s-\sug\f s1*2 s1-\sug\ff s1-\sug\f s1-\sug\f }
>>
mib'!2.-\sug\ff re'4( |
mib' re' mib'8) r mib'4-\sug\sf~ |
mib' re'8 dod' re'4 dod' |
re'( dod' re'8) r fa'4-\sug\sf( |
si2) \clef "bass" sold,-\sug\ff |
re'~ re' |
dod' la, |
la,1 |
re4 r r2 |
R1*2 |
r4 \clef "tenor" \grace re'8 dod'-\sug\p si16 dod' re'4 \grace re'8 dod' si16 dod' |
re'4 r r2 |
R1*3 |
\clef "bass" la,2-\sug\f r |
re re'16 dod' si la sol fad mi re |
dod re dod si, la,4 mi'16 re' dod' si la sol fad mi |
re1 |
si, |
sib, |
la,2. la4 |
re sol la la, |
si,1 |
sib, |
la,2. la4 |
re sol la la, |
re re8 re re4 si,8 dod |
re4 la,8 fad re4 si,8 dod |
re4. re32 mi fad sol la4. la,16 si,32 dod |
re4. re32 mi fad sol la4. la16 si32 dod' |
re1\p~ |
re\cresc~ |
re |
re4.-\sug\ff re8 re4 re |
re2. r8 la,16\ff si, |
do4. do8 do4 do |
si, si,16 dod red mi fad8 sold lad si |
si,4.. si,16 si,4.. si,16 |
si,2
