Froids hu -- mains, non en -- cor vi -- vants ;
a -- tô -- mes per -- dus dans l’es -- pa -- ce :
que cha -- cun de vos é -- lé -- ments,
se rap -- proche et pren -- ne sa pla -- ce,
sui -- vant l’or -- dre, la pe -- san -- teur,
et tou -- tes les lois im -- mu -- a -- bles
que l’é -- ter -- nel dis -- pen -- sa -- teur
im -- pose aux ê -- tres vos sem -- bla -- bles.
Hu -- mains, non en -- core e -- xis -- tants,
à mes yeux pa -- rais -- sez vi -- vants.
