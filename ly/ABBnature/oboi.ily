\clef "treble" <>\f <<
  { mib''2 mib''4. sol''8 | sol''1\fermata |
    sib''2 sib''4. sib'8 | sib'2.\fermata } \\
  { sol'2 sol'4. sib'8 | sib'1\fermata |
    re''2 re''4. re'8 | re'2.\fermata }
>> r4 |
R1 |
<fa'' sib'>1\f |
R1 |
<< { sol''1~ | sol''2~ sol''4 } \\
  { mib''1\f~ | mib''2~ mib''4 } >> r4 |
R1 |
<do'' la'>1\p\cresc |
<< <fad'' la'>1-\sug\mf { s2 <>\cresc } >> |
<>\f << { sol''2 sol''4 } \\ { sib'2 sib'4 } >> r4 |
<>-\sug\fp <<
  { re''1~ | re'' | mib''~ | mib''2. mi''4 |
    fa''1 | si'2~ si'8 si' do'' re'' | mib''4. mib''8 si'4 do'' | sol''1~ |
    sol''4 re''8. re''16 re''4.\fermata } \\
  { re'1~ | re' | mib'\p~ | << mib'2. { s4. <>\cresc } >> mi'4 |
    fa'1 | re'2\f~ re'8 lab'\p sol' sol' | sol'4. sol'8 si'4 do'' | si'1\f~ |
    si'4 si'8. si'16 si'4.\fermata }
>> r8 |
<<
  { mib''1 | fa'' | sol''4 mib'' do'' re'' | mib'' sol'2 sol'4 |
    sib'1 | mib''2 sol'' | sib''1 | sol'' |
    mib''~ | mib''~ | mib''\fermata } \\
  { sol'1\f | sib'\f | sib'4 sib' lab' sib' | sol'\p mib'2 mib'4 |
    sol'1\cresc | sol'2\f mib'' | sol''1\p | mib'' |
    sol'~ | sol'~ | sol'\fermata | }
>>
