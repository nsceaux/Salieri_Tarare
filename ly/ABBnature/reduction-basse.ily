\clef "bass" mib,4. sol,16 sib, mib4. sib,16 sol, |
mib,1\fermata |
sib,4. re16 fa sib4. fa16 re |
sib,2.\fermata r4 |
mib4. sib16 do'32 re' mib'8 sib sol mib |
<re fa sib>1 |
sib,2~ <lab sib,> |
<sol sib mib'>1 |
mib4 mib mib mib |
mib mib mib mib |
re4 re4 re re |
re re re re |
sol sol sol, sol |
fad2 \voiceTwo fad, |
sol,4 sol, sol fa! |
mib2 \oneVoice <sol, mib> |
<lab, mib>4 q lab sol |
fa fa fa fa |
<fa lab re'> fa fa8 <re fa> mib si, |
<do sol>4. q8 <re sol> sol <mib sol> <do sol> |
sol,4. sol8 sol8. sol16 sol8. sol16 |
sol4 sol,8. sol,16 sol,4.\fermata r8 |
do4 r8 sol16 la32 si do'8 sol mib do |
re2~ re4 <sib re'>8. q16 |
mib'4 sol8. sol16 lab4 sib |
<mib sol>2:8 q:8 |
<mib sol sib>:8 q:8 |
q q |
q1~ |
q |
<mib sib>~ |
q~ |
q^\fermata |
