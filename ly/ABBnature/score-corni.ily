\score {
  <<
    \new GrandStaff \with { instrumentName = "Cors en mi♭" } <<
      \new Staff <<
        \keepWithTag #'() \global
        \keepWithTag #'corno1 \includeNotes "corni"
      >>
      \new Staff <<
        \keepWithTag #'() \global
        \keepWithTag #'corno2 \includeNotes "corni"
      >>
    >>
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
      \haraKiriFirst
    } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
  >>
  \layout { indent = \largeindent }
}
