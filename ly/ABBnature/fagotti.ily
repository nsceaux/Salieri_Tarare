\clef "bass" mib,4.\f sol,16 sib, mib4. sib,16 sol, |
mib,1\fermata |
sib,4. re16 fa sib4. fa16 re |
sib,2.\fermata r4 |
mib4.\f sib16\mf do'32 re' mib'8 sib sol mib |
<< re'1 \\ re\f >> |
R1 |
<<
  { mib'1~ | mib'8 sib4 sib sib sib8 |
    do'1 | do' |
    do' | sib | } \\
  { sol1\f~ | sol8 sol4 sol sol sol8 |
    sol1-\sug\p | fad\cresc |
    << la1-\sug\mf { s2 <>\cresc } >>| sol1\f | }
>>
r4 r8 <<
  { la32*2 sib do'8 do' do' do' | sib1 |
    sib4. sib32*2 do' reb'8 reb' reb' reb' | do'2. sib4 |
    lab2~ lab | re'2~ re'8 } \\
  { fad32*2 sol la!8 la la la | sol2. lab!4 |
    sol4.\p sol32*2 lab sib8 sib sib sib |
    << lab2. { s4 <>\cresc } >> sol4 |
    fa2~ fa | si\f~ si8 }
>> r8 r4 |
r4 r8 do-\sug\p re sol mib do |
sol,4.\f sol8 sol8. sol16 sol8. sol16 |
sol4 \clef "tenor" <<
  { re'8. re'16 re'4.\fermata } \\
  { si8. si16 si4.\fermata }
>> r8 |
\clef "bass" <<
  { do'1 | sib | sib4 } \\
  { do1-\sug\fp | re\f | mib4 }
>> sol lab sib |
mib2\p~ mib |
mib1\cresc~ |
mib\f~ |
mib\p~ |
mib |
<< 
  { sib1~ | sib~ | sib\fermata | } \\
  { mib1~ | mib~ | mib\fermata }
>>
