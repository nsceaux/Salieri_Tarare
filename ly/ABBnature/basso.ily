\clef "bass" mib,4.\f sol,16 sib, mib4. sib,16 sol, |
mib,1\fermata |
sib,4. re16 fa sib4. fa16 re |
sib,2.\fermata r4 |
mib4.\f sib16\mf do'32 re' mib'8 sib sol mib |
re1\f |
sib,2\p~ sib,4. sib16 do'32 re' |
mib'2\f~ mib'8 mib' sib sol |
mib4 mib mib mib |
mib\p mib mib mib |
re4\cresc re4 re re |
re\mf re re\cresc re |
sol\f sol sol, sol\p |
fad2\fp fad, |
sol,4 sol,\f sol fa! |
mib2\p sol, |
lab,4 lab,\cresc lab sol |
fa fa fa fa |
fa\f fa fa8 re\p mib si, |
do4. do8 re sol mib do |
sol,4.\f sol8 sol8. sol16 sol8. sol16 |
sol4 sol,8. sol,16 sol,4.\fermata r8 |
do4\f r8 sol16\mf la32 si do'8 sol mib do |
re2\f~ re4 re' |
mib' sol lab sib |
mib2:8\p mib:8 |
mib:8\cresc mib:8 |
mib:8\f mib:8 |
mib1\p~ |
mib~ |
mib |
R1 |
R1^\fermataMarkup |
