\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr } << \global \includeNotes "oboi" >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \smaller en mi♭ }
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'corni \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } << \global \includeNotes "fagotti" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character La Nature
      shortInstrumentName = \markup\character Nat
    } \withLyrics <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \keepWithTag #'basso \includeNotes "basso"
      \origLayout {
        s1*5\break s1*5\pageBreak
        s1*5\break s1*5\pageBreak
        s1*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
