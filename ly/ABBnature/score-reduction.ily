\score {
  <<
    \new Staff \withLyrics <<
      { s1*3 s2 <>^\markup\character La Nature }
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new PianoStaff <<
      \new Staff = "dessus" << \global \includeNotes "reduction-dessus" >>
      \new Staff = "basse" << \global \includeNotes "reduction-basse" >>
    >>
  >>
  \layout { }
  \midi { }
}
