\clef "treble" \transposition mib
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 do''4. mi''8 | mi''1\fermata |
    sol''2 sol''4. sol''8 | sol''2.\fermata }
  { mi'2 mi'4. sol'8 | sol'1\fermata |
    sol'2 sol'4. sol'8 | sol'2.\fermata }
  { s1\f }
>> r4 |
R1 |
\twoVoices #'(corno1 corno2 corni) << re''1 sol' s\f >> |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''1~ | mi''2~ mi''4 }
  { do''1~ | do''2~ do''4 }
  { s1\f }
>> r4 |
R1*5 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { mi''2 re''4 | do''2 }
  { mi''2 re''4 | do''2 }
  { s2\f }
>> r2 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { fa''2 mi''4 | re''1~ | re''2~ re''8 }
  { fa''2 mi''4 | \tag #'corni \hideNotes re''1~ |
    \tag#'corni \unHideNotes re''2~ re''8 }
  { s2.\cresc | s1 | s2\f }
>> r8 r4 |
r2 r8 \twoVoices #'(corno1 corno2 corni) << 
  { mi''8 mi'' mi'' | mi''1~ | mi''4 mi''8. mi''16 mi''4. }
  { mi''8 mi'' mi'' | mi'1~ | mi'4 mi'8. mi'16 mi'4. }
  { s4. | s1\f }
>> r8 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''1 | re'' | mi''4 mi''8. mi''16 re''2 | do''4 mi''2 mi''4 |
    sol''1~ | sol''~ | sol'' | mi'' |
    do''~ | do''~ | do''\fermata | }
  { do''1 | sol' | do''4 do''8. do''16 do''4 sol' | mi' do''2 do''4 |
    mi''1~ | mi''~ | mi'' | do'' |
    mi'~ | mi'~ | mi'\fermata | }
  { s1\f s\f s1 s1\p s1\cresc s1\f s1\p }
>>
