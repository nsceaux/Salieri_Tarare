\clef "soprano/treble"
<>^\markup\italic un poco Adagio _\markup\italic Sans lenteur
R1 |
R^\fermataMarkup |
R1 |
r2^\markup\italic chanté r4 sib'8. sib'16 |
mib''4 mib''8. mib''16 mib''4. mib''8 |
sib'2 r4 r8 sib' |
re''4 re''8. mib''16 fa''4 re''8 sib' |
mib''2. mib''8 r |
r2 r4 mib''8. re''16 |
do''4. do''8 do''4 do''8 do'' |
do''2 r4 do''8 do'' |
fad''4. fad''8 fad''4 fad''8 sol'' |
re''4 re'' r re''8 re'' |
re''4 re''8 re'' re''4. re''8 |
re''2 r4 r8 re'' |
mib''4 mib''8 mib'' mib''4 mib''8 mib'' |
mib''2 mib''8 mi'' mi'' mi'' |
fa''4. fa''8 fa''4. fa''8 |
si'2 r8 si' do'' re'' |
mib''4. mib''8 si'4 do'' |
sol''1 |
sol''2 r4\fermata r8 re'' |
mib''4 mib''8. mib''16 mib''4 mib''8 mib'' |
fa''2 r4 fa''8 fa'' |
sol''4 mib''8 mib'' do''4. re''8 |
mib''2 r |
R1*6 |
R1^\fermataMarkup |
