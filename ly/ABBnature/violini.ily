\clef "treble" <sol mib'>2\f q4. <sib sol'>8 |
q1\fermata |
<re' sib'>2 q4. <fa' re''>8 |
q2.\fermata r4 |
<<
  \tag #'violino1 {
    mib''1\fp |
    <re' sib' fa''>4.\f fa''16 sol''32 la'' sib''8-! fa''-! re''-! sib'-! |
    lab'2\p lab |
    sol\f~ sol~ |
    sol8 <sol mib'>4 q q q8 |
    do''8\p do''4 do'' do'' do''8~ |
    do''8 do''4\cresc do'' do'' do''8 |
    do'' <la' fad''>4\mf q\cresc q q8 |
    <sib' sol''>8\f q4 q re''\p re''8 |
    re''2\fp re' |
    re'8 re'4\f re' re' re'8 |
    mib'4\p mib''2 mib'4 |
    mib'8 mib'4 mib'\cresc mi' mi'8 |
    fa' lab'4 do'' fa'' lab''8 |
    si'\f si'4 si' si'8-!\p do''-! re''-! |
    mib''8. mib''16 sol''8. mib''16 si'4 do'' |
    <re' si' sol''>4.\f sol''8 sol''8. si''16 si''8. re'''16 |
    re'''4 sol'8. sol'16 sol'4.\fermata r8 |
  }
  \tag #'violino2 {
    sib'1\fp |
    sib'\f |
    re''2\p~ re''4. sib'16 do''32 re'' |
    mib''8.\f sol''32 lab'' sib''8 sol'' mib'' mib'' sib' sol' |
    mib' <sib sol'>4 q q q8 |
    sol'8\p sol'4 sol' sol' sol'8 |
    <fad' la'>8 q4\cresc q q q8 |
    q <re' do''>4\mf q\cresc q q8 |
    <re' sib'>8\f q4 q q\p q8 |
    <re' la'>4.\fp la32*2 sib do'8 do' do' do' |
    << sib4. { s8 <>\f } >> sol32*2 la sib8 sib lab lab |
    sol4.\p sib32*2 do' reb'8 reb' reb' reb' |
    do'4. lab32*2\cresc sib do'8 do' sib sib |
    lab8 fa'4 lab' do'' fa''8 |
    re'\f re'4 re' lab'8-!\p sol'-! sol'-! |
    sol'4. sol'8 sol'4 sol' |
    <re' si' sol''>4.\f <re' si'>8 q8. q16 q8. q16 |
    q4 <si re'>8. q16 q4.\fermata r8 |
  }
>>
<do' sol' mib''>4\f r4 r2 |
<re' sib' fa''>4.\f fa''32 mib'' re'' do'' <<
  \tag #'violino1 {
    sib'8-! re''-! fa''-! sib''-! |
    sol''4 mib''8. mib''16 do''4 re'' |
    mib''16\p
  }
  \tag #'violino2 {
    sib'8 re'' sib'8. sib'16 |
    sib'4 sib'8. sib'16 lab'4 lab' |
    sol'16\p
  }
>> mib' re' mib' fa' mib' re' mib' sol'( mib' sol' mib' sol' mib' sol' mib') |
\rt#4 { sib'(\cresc sol' } \rt#4 { sib' sol') } |
\rt#4 { mib''16(\f sib') } \rt#4 { sol''( mib'') } |
sib''4(\p sol'' mib'' sib') |
sol''( mib'' sib' sol') |
mib'1 |
R1 |
R1^\fermataMarkup |
