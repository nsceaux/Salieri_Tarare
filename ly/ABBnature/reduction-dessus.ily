\clef "treble" <sol' mib''>2\f q4. <sib' sol''>8 |
q1\fermata |
<re'' sib''>2 q4. <sib' re''>8 |
<fa' sib' re''>2.\fermata r4 |
<sol' sib' mib''>1\mf |
fa''4.\f fa''16 sol''32 la'' sib''8-! fa''-! re''-! sib'-! |
<fa' lab' re''>2\p~ q4. sib'16 do''32 re'' |
mib''8.\f sol''32 lab'' sib''8 sol'' mib'' mib'' sib' sol' |
mib'8 <sol' sib mib'>4 q q q8 |
<mib' sol' do''>8\p q4 q q q8~ |
< la' fad'do''>8 q4\cresc q q q8 |
q <la' do'' fad''>4\mf q\cresc q q8 |
<sib' re'' sol''>8\f q4 q <sib' re''>\p q8 |
< la' re''>4.\fp \change Staff = "basse" \voiceOne  <fad! la>32*2 <sol sib> <la do'>8 q q q |
<sib re'>8 q4^\f sol32*2 la <sib re'>8 q <lab re'> q |
<sol sib>4. q32*2 <lab do'> \change Staff = "dessus" \oneVoice <sib reb'>8 q q q |
do'4. lab32*2\cresc sib do'8 <do' mi'> <sib mi'> q |
fa'8 <fa' lab'>4 <lab' do''> <do'' fa''> <fa'' lab''>8 |
si'\f <re' si'>4 q <si' lab'>8-!\p <do'' sol'>-! <re'' sol'>-! |
mib''8. mib''16 sol''8. mib''16 si'4 do'' |
<re'' si' sol''>4.\f q8 q8. <re'' sol'' si''>16 q8. <sol'' si'' re'''>16 |
q4 <sol' si' re''>8. q16 q4.\fermata r8 |
<do'' sol' mib''>1\f |
<re'' sib' fa''>4.\f fa''32 mib'' re'' do''
sib'8-! re''-! fa''-! sib''-! |
<sib' sol''>4 <sib' mib''>8. q16 <lab' do''>4 <lab' sib' re''> |
mib''16\p mib' re' mib' fa' mib' re' mib' sol'( mib' sol' mib' sol' mib' sol' mib') |
\rt#4 { sib'(\cresc sol' } \rt#4 { sib' sol') } |
\rt#4 { mib''16(\f sib') } \rt#4 { sol''( mib'') } |
sib''4(\p sol'' mib'' sib') |
sol''( mib'' sib' sol') |
<mib' sol' mib''>1~ |
q1~ |
q1^\fermata |
