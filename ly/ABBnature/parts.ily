\piecePartSpecs
#`((reduction)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet#})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#})
   (corni #:score "score-corni")
   (oboi #:score-template "score-part-voix")
   (fagotti #:score-template "score-part-voix")

   (violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix"))
