\clef "alto" mib4.\f sol16 sib mib'4. sib16 sol |
mib1\fermata |
sib4. re'16 fa' sib'4. fa'16 re' |
sib2.\fermata r4 |
sol'1\fp |
re'1\f |
fa'2\p~ fa'4. sib16 do'32 re' |
mib'8.\f sol'32 lab' sib'8 sol' mib' mib' sib sol |
mib8\p mib4 mib mib mib8 |
mib mib'4 mib' mib' mib'8 |
re'8 re'4\cresc re' re' re'8 |
re' re'4\mf re'\cresc re' re'8 |
re'\f re'4 re' <re' sib'>4\p q8 |
<re' la'>4.\fp fad32*2 sol la8 la la la |
<< sol4. { s8 <>\f } >> sib32*2 la sol8 sib sib sib |
sib4.\p sol32*2 lab! sib8 sib sib sib |
lab4. do32*2\cresc mib lab8 do' do' do' |
do' do'4 fa' lab' do'8 |
lab'2\f~ lab'8 fa'-!\p mib'-! re'-! |
do'4. do'8 re'4 do' |
si4.\f sol8 sol4 sol |
sol sol8. sol16 sol4.\fermata r8 |
do'4\f r8 sol'16\mf la'32 si' do''8 sol' mib' do' |
\once\tieDashed re'2\f~ re'4 sib8. sib16 |
sib4 sol8. sol16 lab4 sib |
mib16\p sol sol sol sol4:16 sol2:16 |
\rt#4 { sol16(\cresc mib } \rt#4 { sol mib) } |
\rt#4 { sol\f sol' } \rt#4 { mib' sib } |
sol'4(\p mib' sib sol) |
mib'( sib sol mib) |
mib1 |
R1 |
R1^\fermataMarkup |
