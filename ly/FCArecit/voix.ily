\clef "bass" <>^\markup\character Atar
r8 lab reb' lab lab lab |
fa4 r8 lab16 lab reb'4 reb'8 mib' |
dob' dob' r8 dob' dob' dob' reb' mib' |
mib' lab r lab dob' dob' dob' sib |
solb solb r4 r2 |
\ffclef "tenor/G_8" <>^\markup\character-text Tarare froidement
dod'8 dod'16 dod' dod'8 red' sid4 r8 sold16 lad |
sid4 sid8 dod' sold4 r16 sold sold sold |
dod'4 dod'16 dod' dod' dod' si8 si r4 |
si8 si16 si mi'8 red' mi' mi'16 mi' mi'8 si16 si |
sold8 sold r4 re' re'8 mi' |
dod' dod' r4\fermata <>^\markup\italic { (le ton du désespoir.) } mi'4. mi'8 |
do'!4 si8 do' la4 r16 la la si |
do'4. do'8 do' do' do' re' |
sib sib r4 sib8 sib sib sib |
mi'4 mi'8 fa' do' do' r4 |
r8 fa' fa' re' si!4 r8 sol |
si4 r8 si re'4 r |
r4 <>^\markup\italic très appuyé re' re' re' |
sol'4. re'8 re' re' do' re' |
si si r4 re' mi'8 fa' |
si4. sol8 si si si do' |
sol sol r4 r2 |
\ffclef "bass" <>^\markup\character-text Atar vivement
r8 si si si sold sold sold la |
si4 r r r8 <>^\markup\italic aux Eunuques si |
mi' mi' re' mi' do'4 r |
r2 r4 r8 <>^\markup\italic à Tarare fad16 fad |
si4 si8 si si fad r16 fad fad sol |
la8 fad fad sol mi4
\ffclef "tenor/G_8" <>^\markup\character-text Tarare froidement
r16 sol sol la |
si8. si16 si8 do' do' sol r4 |
do'8. do'16 do'8 re' sib4 r8 sib16 do' |
la4 r
\ffclef "bass" <>^\markup\character-text Atar furieux
r4 r8 do'16 do' |
la4. fa8 do' do' do' re' |
mib'4 mib' r2 |
R1 |
