\clef "bass" r4 r2 |
R1 |
reb1\fp |
reb |
r4 fad\f fad r |
r2 fad4\p r |
r2 mi!4 r |
r2 sold4 r |
R1*2 | \allowPageTurn
la4 r la2:16\fp |
la:16 la:16 |
la:16 la:16 |
sol!:16 sol:16 |
sol:16 fa4 r |
r2 r8 r16 sol\f sol4 |
r8 r16 sol\f sol4 r8 r16 sol sol8. sol16 |
sol4 r r2 |
sol2:16\fp sol:16 |
sol4 r r2 |
R1 |
r4 sol\f sold2~ |
sold4 r r2 |
r4 mi mi r |
r2 la4 r16 mi\f fad sold |
la4\f la, si, r |
R1 |
r2 mi4 r |
re r do r |
R1 |
fa2:16\fp fa:16 |
fa:16 fa:16 |
fa4\ff la do' la |
fa r r2 |
