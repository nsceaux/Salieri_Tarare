Ap -- pro -- che, mal -- heu -- reux ! viens su -- bir le sup -- pli -- ce,
qu’un crime ir -- ré -- mis -- si -- ble ar -- rache à ma jus -- ti -- ce.

Qu’el -- le soit juste ou non, je de -- man -- de la mort.
De tes plai -- sirs j’ai vio -- lé l’a -- si -- le,
sans y trou -- ver l’ob -- jet d’une au -- dace i -- nu -- ti -- le,
mon As -- ta -- si -- e !… Ô ce fourbe Al -- ta -- mort !
Il l’a ra -- vie à mon sé -- jour cham -- pê -- tre,
sans la pré -- sen -- ter à son maî -- tre !
Tra -- his -- sant tout, hon -- neur, de -- voir…
il a pay -- é sa dou -- ble per -- fi -- di -- e ;
mais ton Ir -- za n’est point mon As -- ta -- si -- e.

El -- le n’est pas en mon pou -- voir ?

Que l’on m’a -- mène Ir -- za. Si ta bouche en im -- po -- se,
je la poi -- gnar -- de de -- vant toi.

La voir mou -- rir est peu de cho -- se ;
tu te pu -- ni -- ras, non pas moi.

De sa mort la tien -- ne su -- i -- vi -- e…
