\clef "treble" r4 r2 |
R1 |
<<
  \tag #'violino1 { dob''1\fp | dob'' | }
  \tag #'violino2 { lab'1-\sug\fp | lab' | }
>>
r4 fad''\f <fad' dod'' lad''> r |
r2 <<
  \tag #'violino1 {
    sid'4\p r |
    r2 dod''4 r |
    r2 si'4 r |
  }
  \tag #'violino2 {
    sold'4-\sug\p r |
    r2 sold'4 r |
    r2 mi'4 r |
  }
>>
R1*2 |
<<
  \tag #'violino1 {
    dod''4 r\fermata mi''2:16\fp |
    mi'':16 mi'':16 |
    mi'':16 mi'':16 |
    mi'':16 mi'':16 |
    mi'':16 fa''4 r |
  }
  \tag #'violino2 {
    mi'4 r\fermata do''!2:16-\sug\fp |
    do'':16 do'':16 |
    do'':16 do'':16 |
    sib':16 sib':16 |
    sib':16 la'4 r |
  }
>>
r2 r8. <re' si'!>16\f q4 |
r8 r16 <si' sol''>16\f q4 r8 r16 <re'' si''>16 q8. q16 |
q4 r r2 |
<si re'>2:16\fp q:16 |
q4 r r2 |
R1 |
r4 <sol sol'>4\f mi'8 fad'16 sold' la' si' dod'' red'' |
mi''4 r r2 |
<<
  \tag #'violino1 {
    r4 <mi' si' sold''> <mi' si' si''> r |
  }
  \tag #'violino2 {
    r4 <sold' si' mi''> <mi' si' sold''> r |
  }
>>
r2 <mi' do'' la''>4 r16 <<
  \tag #'violino1 {
    mi''\f fad'' sold'' |
    la''\f si'' do''' si'' la'' sol''! fad'' mi'' red''4
  }
  \tag #'violino2 {
    mi'16-\sug\f fad' sold' |
    la'-\sug\f si' do'' si' la' sol'! fad' mi' red'4
  }
>> r4 |
R1 |
r2 <<
  \tag #'violino1 {
    <si' sol''>4 r |
    si' r do'' r |
  }
  \tag #'violino2 {
    <sol' mi''>4 r |
    fa'!4 r mi' r |
  }
>>
R1 |
<<
  \tag #'violino1 {
    la''2:16\fp la'':16 |
    la'':16 la'':16 |
    <mib'' do'''>4:16\ff q:16 q:16 q:16 |
    q4 r r2 |
  }
  \tag #'violino2 {
    do''2:16-\sug\fp do'':16 |
    do'':16 do'':16 |
    <do'' la''>4:16-\sug\ff q:16 q:16 q:16 |
    q4 r r2 |
  }
>>
