\key do \major \midiTempo#100
\time 4/4 \partial 2. s2. s1*9 s2
\tempo "Lent" s2 s1*10
\tempo "Allegro" s1*11
\tempo "Allegro" s1*2 \bar "|."
