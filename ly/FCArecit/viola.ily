\clef "alto" r4 r2 |
R1 |
reb'-\sug\fp |
reb' |
r4 fad'-\sug\f fad r |
r2 red'4-\sug\p r |
r2 dod'4 r |
r2 sold4 r |
R1*2 | \allowPageTurn
la'4 r\fermata do'!2:16-\sug\fp |
do':16 do':16 |
do':16 do':16 |
do':16 do':16 |
do':16 do'4 r |
r2 r8 r16 sol'\f sol'4 |
r8 r16 sol'\f sol'4 r8 r16 sol' sol'8. sol'16 |
sol'4 r r2 |
sol2:16-\sug\fp sol:16 |
sol4 r r2 |
R1 |
r4 <<
  { re'4 mi'2~ | mi'4 } \\
  { si4-\sug\f si2~ | si4 }
>> r4 r2 |
r4 mi' mi' r |
r2 la'4 r16 mi'-\sug\f fad' sold' |
la'4-\sug\f la fad' r |
R1 |
r2 mi'4 r |
re' r do' r |
R1*5 |
