Spi -- net -- te, com -- ment fuir de cette hor -- rible en -- cein -- te ?

Cal -- mez le dé -- ses -- poir dont votre âme est at -- tein -- te.
\tag #'tous {
  Ô mort ! ter -- mi -- ne mes dou -- leurs,
  ter -- mi -- ne, ter -- mi -- ne mes dou -- leurs ;
  le cri -- me se pré -- pa -- re.
  Ar -- rache au plus grand des mal -- heurs,
  l’é -- pou -- se de Ta -- ra -- re.
  Ar -- rache au plus grand des mal -- heurs,
  l’é -- pou -- se de Ta -- ra -- re.
  Ar -- rache au plus grand des mal -- heurs,
  l’é -- pou -- se de Ta -- ra -- re.
}
Il sem -- blait que je pres -- sen -- tais
leur en -- tre -- prise in -- fâ -- me !
Quand il par -- tit, je ré -- pé -- tais,
hé -- las ! __ l’ef -- froi dans l’â -- me !
cru -- el ! pour qui j’ai tant souf -- fert,
c’est trop, c’est trop que ton ab -- sen -- ce
laisse As -- ta -- sie en un dé -- sert,
sans joie et sans dé -- fen -- se !
L’im -- pru -- dent n’a pas é -- cou -- té
sa com -- pagne é -- plo -- ré -- e :
aux mains d’un bri -- gand dé -- tes -- té
des bri -- gands l’ont li -- vré -- e.
\tag #'tous {
  Ô mort ! ter -- mi -- ne mes dou -- leurs,
  ter -- mi -- ne, ter -- mi -- ne mes dou -- leurs :
  le cri -- me se pré -- pa -- re.
  Ar -- rache au plus grand des mal -- heurs,
  l’é -- pou -- se de Ta -- ra -- re.
  Ar -- rache au plus grand des mal -- heurs,
  l’é -- pou -- se de Ta -- ra -- re.
  Ar -- rache au plus grand des mal -- heurs,
  l’é -- pou -- se de Ta -- ra -- re.
  Ar -- rache au plus grand des mal -- heurs,
  l’é -- pou -- se de Ta -- ra -- re.
}
