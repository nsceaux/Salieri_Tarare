\clef "treble" \transposition la
r8 |
<>-\sug\ff \twoVoices #'(corno1 corno2 corni) <<
  { mi''1 | }
  { do'' | }
>>
R1 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { mi''2 |
    mi''1~ |
    mi''2. mi''8. mi''16 |
    mi''4.. mi''16 mi''4.. mi''16 |
    mi''2 }
  { mi'2 |
    mi'1~ |
    mi'2. mi'8. mi'16 |
    mi'4.. mi'16 mi'4.. mi'16 |
    mi'2 }
>> r2 |
R1*3 |
r2 r4 r\fermata |
<>\fp \twoVoices #'(corno1 corno2 corni) <<
  { mi''2~ mi''4 }
  { do''2~ do''4 }
>> r4 |
R1*2 |
r2 r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''4 | mi''1 | mi'' | mi''2 }
  { re''4 | re''1 | re'' | do''2 }
  { s4 | s1-\sug\f | s4 s2.-\sug\p }
>> r2 |
R1*7 |
r8. <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { re''16 re''2 }
  { re''16 re''2 }
>> r4 |
R1*4 |
r8. <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { re''16 re''2 }
  { re''16 re''2 }
>> r4 |
R1*7 | \allowPageTurn
\twoVoices #'(corno1 corno2 corni) <<
  { re''2. | re''~ | re'' | do''~ | do''~ | do'' | }
  { re''2. | sol'~ | sol' | do'~ | do'~ | do' | }
  { s2.-\sug\p | }
>>
R2. |
<<
  \tag #'(corno1 corni) {
    r8^"solo" re''4 re''8 do'' do'' |
    do''2-\sug\f re''4 |
    do''2.-\sug\p |
    do''4 r r2 |
    R1*11 |
  }
  \tag #'corno2 { R2.*3 R1*12 }
>>
<>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { mi''2~ mi''4 }
  { do''2~ do''4 }
>> r4 |
R1*2 |
r2 r4 re''4 |
<>\f \twoVoices #'(corno1 corno2 corni) <<
  { mi''1 | mi'' | mi''2 }
  { re''1 | re'' | do''2 }
  { s1 | s-\sug\p }
>> r2 |
R1*7 |
r8. <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { re''16 re''2. | }
  { re''16 re''2. | }
>>
R1*4 |
r8. <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { re''16 re''2 }
  { re''16 re''2 }
>> r4 |
R1*2 |
do'1-\sug\ff~ |
do' |
do' |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2\fermata~ re''4 }
  { re''2\fermata~ re''4 }
>> r4 |
R1*2 |
r8 <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { mi''8 mi'' mi'' mi''2~ | mi''1~ | mi''2 }
  { mi'8 mi' mi' mi'2~ | mi'1~ | mi'2 }
>> r2 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { re''4 mi'' | do''2 }
  { re''4 mi'' | do''2 }
>> r2 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { re''4 mi'' | do''2 }
  { re''4 mi'' | do''2 }
>> r2 |
R1 |
