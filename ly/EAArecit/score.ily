\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \oboiInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'oboe1 \includeNotes "oboi"
        >>
        \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small en La }
        shortInstrumentName = \markup\center-column { Cor. }
      } << \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni" >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'tous \includeNotes "voix"
    >> \keepWithTag #'tous \includeLyrics "paroles"
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s8 s1*5\pageBreak
        s1*4\break s1*4\pageBreak
        s1*6\break s1*5\pageBreak
        s1*6\break s1*6\pageBreak
        s1*2 s2.*2\break s2.*5\pageBreak
        s2.*3 s1*2\break s1*5\pageBreak
        s1*8\break \grace s8 s1*6 s2 \bar "" \pageBreak
        s2 s1*4\break s1*5\pageBreak
        s1*5\break s1*6\pageBreak \grace s8
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
