\clef "treble"
\twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''8 |
    fad''2. dod''8 re'' |
    re''4( dod''8) re'' re''4( dod''8) re'' |
    re'' dod'' dod''4 sold''4.( la''16 si'') |
    la''4 sold''8 fad'' sold''4.( la''16 si'') |
    si''4 la''2 fad''8. sol''16 |
    sol''4.. mi''16 mi''4.. dod''16 |
    dod''2 }
  { r8 |
    la'1 |
    sold'2 sold' |
    sold' dod'' |
    dod''1~ |
    dod''2. la'4 |
    lad'4.. lad'16 lad'4.. lad'16 |
    lad'2 }
  { s8 | s1-\sug\ff | s2-\sug\sf s-\sug\sf | }
>> r2 |
R1*3 |
r2 r4 r\fermata |
<>-\sug\fp \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''2~ fad''4 }
  { la'2~ la'4 }
>> r4 |
R1 |
<<
  \tag #'(oboe1 oboi) {
    r4^"solo" la'4( dod'' fad'') |
    r re''( re''' si'') |
  }
  \tag #'oboe2 { R1*2 | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sold''1 | sold'' | la''2 }
  { si'1 | si' | la'2 }
  { s1-\sug\f | s4 s2.-\sug\p }
>> r2 |
R1*3 |
r8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re'4 fad' la' re''8 | }
  { re'4 fad' la' re''8 | }
>>
<>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''2~ fad''8 la'' sold'' fad'' | mid''4 }
  { la'4 si' sid'2 | dod''4 }
>> r4 r2 |
R1 |
r8. <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { si''16 si''2 }
  { re''16 re''2 }
>> r4 |
R1*2 |
r4 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'4( dod'' fad'') }
  { la'4( dod'' fad'') }
>>
R1 |
r8. <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re'''16 re'''2 }
  { sold''16 sold''2 }
>> r4 |
R1*2 |
r4 r8 fad'-\sug\f fad'4 mi' |
re'2 r |
R1*3 | \allowPageTurn
r8 <>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''!4 la'' la''8 | sold''2.~ | sold'' | sol''~ |
    sol'' | fad'' | }
  { red''4 red'' red''8 | r8 re''!4 re'' re''8 | re''2. | dod''~ |
    dod'' | re'' | }
>>
R2. |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4 mi'' mi''8 |
    la''4. fad''8 \once\slurDashed mi''( fad''16 sol'') |
    re''2 dod''4 |
    re'' }
  { re''4 re''8 dod'' mi'' |
    la' re''4 re''8 si' sol'? |
    fad'2 mi'4 |
    fad' }
  { s8 s2 | s2.-\sug\f | s-\sug\p }
>> r4 r2 |
R1*11 |
<>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''2~ fad''4 }
  { la'2~ la'4 }
>> r4 |
R1 |
<<
  \tag #'(oboe1 oboi) {
    r4^"solo" la'4( dod'' fad'') |
    r re''( re''' si'') |
  }
  \tag #'oboe2 { R1*2 | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sold''1~ | sold'' | la''2 }
  { si'1~ | si' | la'2 }
  { s1-\sug\f | s-\sug\p }
>> r2 |
R1*3 |
r8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re'4 fad' la' re''8 |
    fad''2~ fad''8 la'' sold'' fad'' |
    mid''4 dod''2 }
  { re'4 fad' la' re''8 |
    la'4 si' sid'2 |
    dod''2. }
>> r4 |
R1 |
r8. <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { si''16 si''2. | }
  { re''16 re''2. | }
>>
R1*2 |
r4 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'4( dod'' fad'') | }
  { la'4( dod'' fad'') | }
>>
R1 |
r8. <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re'''16 re'''2 }
  { sold''16 sold''2 }
>> r4 |
R1*2 |
<>\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad'1 |
    fad' |
    fad' |
    mid''2\fermata~ mid''4 }
  { re'1~ |
    re' |
    re' |
    sold'2\fermata~ sold'4 }
>> r4 |
R1 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2. sold''4 |
    fad''2 sold''4. la''16 si'' |
    \grace si''8 la''4 sold''8 fad'' sold''4. la''16 si'' |
    \grace si''8 la''4 sold''8 fad'' si''4. dod'''16 re''' |
    dod'''4 la'' sold'' dod''' |
    la''2 si''4. dod'''16 re''' |
    dod'''4 la'' sold'' dod''' | }
  { fad''2. mid''4 |
    fad''2 dod''~ |
    dod''1~ |
    dod''2 fad''4 re'' |
    fad' fad''2 mid''4 |
    fad''2. re''4 |
    fad' fad''2 mid''4 | }
  { s1 | s-\sug\f }
>>
fad''4. fad'8 fad'4 fad' |
fad'2 r |
