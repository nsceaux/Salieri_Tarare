\tag #'all \key fad \minor
\tempo "Allegro agitato" \midiTempo#100
\time 4/4 \partial 8 s8 s1*38
\time 3/4 \tempo "Un poco Andante" s2.*10
\time 4/4 s1*2
\tempo "Allegro" s1*3 s2
\tempo "Primo tempo" s2 s1*42 \bar "|."
