\clef "treble"
<<
  \tag #'violino1 {
    dod''8 |
    fad''2.\ff dod''8( re'') |
    re''4(\sf dod''8) re'' re''4(\sf dod''8) re'' |
    re'' dod'' dod''4 sold''4.( la''16 si'') |
    la''4 sold''8 fad'' sold''4.( la''16 si'') |
    si''4 la''2 fad''8. sol''16 |
    sol''4.. mi''16 mi''4.. dod''16 |
    << lad'1~ { s2 s-\sug\p } >> |
    lad'2 dod''\fp~ |
    dod'' re''4 r |
    R1 |
    r2 r4 r8\fermata dod'' |
    fad''2.\fp dod''4 |
    re''4. re''8 dod''4. si'8 |
    la'8 r la'4( dod'' fad'') |
    r re''( re''' si'') |
    sold''8\f sold''4 sold'' sold'' sold''8 |
    sold''4 mid''\p dod''4. si'8 |
    la'2 r8 la'16 dod'' fad''8 fad'' |
    mi''4.\p mi'8 mi''4. re''8 |
    dod''4 la16 si dod' si la4 r8 mi' |
    la'( si' dod'' re'') mi''8. dod'16 \grace re'8 dod' si16 dod' |
    re'8 re'4\f fad' la' re''8 |
    fad''\p fad''4 fad'' la''8 sold'' fad'' |
    mid''4 dod''2 dod''8.(\f la''16) |
    la''8(\sf sold'' fad'' mid'') fad''(\sf mi'' re'' dod'') |
    re''8. si''16\f <si'' re''>2 re''4\p |
    dod''4( fad'' \grace mi''8 re''4 dod''8 si') |
    la'2. sold'4 |
    la'4 la':16\f dod'':16 fad'':16 |
    la''8(\sf sold'' fad'' mid'') fad''(\sf mi'' re'' dod'') |
    re''8. re'''16\f re'''2 re''4\p |
    dod''4 fad'' \grace mi''8 re''4 dod''8 si' |
    la'2. sold'4 |
  }
  \tag #'violino2 {
    r8 |
    <dod' la'>16\ff q q q q2:16 la'4:16 |
    sold'2\sf sold'\sf |
    sold'2 dod''16 dod'' dod'' dod'' dod''4:16 |
    dod''2:16 dod'':16 |
    dod'':16 dod''4 la' |
    lad'4.. lad'16 lad'4.. lad'16 |
    << dod'1~ { s2 s-\sug\p } >> |
    dod'2 sol''-\sug\fp |
    sol'' fad''4 r |
    R1 |
    r2 r4 r\fermata |
    <dod' la'>2.:16\fp la'4:16 |
    sold'2:16 sold':16 |
    <dod' la'>2:16 q:16 |
    <re' si'>:16 q:16 |
    <sold' si'>8-\sug\f q4 q q q8 |
    << { s4 <>-\sug\p } { q2:16 q:16 } >> |
    <fad' la'>:16 q:16-\sug\fp |
    <mi' si'>:16-\sug\p q:16 |
    <mi' dod''>:16 q:16 |
    <mi' la'>:16 q:16 |
    << { s8 <>-\sug\f } { <fad' la'>2:16 q:16 } >> |
    la'4:16-\sug\p si':16 sid'2:16 |
    dod''2. dod'8.(-\sug\f la'16) |
    la'8(-\sug\sf sold' fad' mid') fad'(-\sug\sf mi' re' dod') |
    re'8. <si' sold''>16-\sug\f q2 sold'4-\sug\p |
    la'2 si'4 la'8 sold' |
    fad'2. mid'4 |
    fad' la(-\sug\f dod' fad') |
    la'8(-\sug\sf sold' fad' mid') fad'(-\sug\sf mi' re' dod') |
    re'8. <si' sold''>16-\sug\f q2 sold'4-\sug\p |
    la' fad'2 fad'4 |
    fad'2. mid'4 |
  }
>>
fad'4. fad'8\f fad'8. fad'16 mi'!8. mi'16 |
<< re'1~ { s4 s2.\p } >> |
re'1 |
re'2 <<
  \tag #'violino1 {
    lad'2~ |
    lad'4 fad''2.\fp |
  }
  \tag #'violino2 {
    mi'2~ |
    mi'4 dod''2.-\sug\fp |
  }
>>
%% 3/4
<<
  \tag #'violino1 {
    r16 red''\p red'' red'' r red'' red'' red'' r red'' red'' red'' |
    r re''! re'' re'' r re'' re'' re'' r re'' re'' re'' |
    r re'' re'' re'' r re'' re'' re'' r re'' re'' re'' |
    r dod'' dod'' dod'' r dod'' dod'' dod'' r dod'' dod'' dod'' |
    r dod'' dod'' dod'' r dod'' dod'' dod'' r dod'' dod'' dod'' |
    r re'' re'' re'' r re'' re'' re'' r re'' re'' re'' |
    re''8 dod''16.( re''32) mi''8.( re''16 sol''8. fad''16) |
    mi''8\fp mi''4 mi'' mi''8 |
    la''4.\f fad''8 mi''( fad''16 sol'') |
    re''16\p re''8 re'' re'' re''16 dod'' dod''8 dod''16 |
    re''4
  }
  \tag #'violino2 {
    r16 la'!-\sug\p la' la' r la' la' la' r la' la' la' |
    r si' si' si' r si' si' si' r si' si' si' |
    r si' si' si' r si' si' si' r si' si' si' |
    r sol' sol' sol' r sol' sol' sol' r sol' sol' sol' |
    r sol' sol' sol' r sol' sol' sol' r sol' sol' sol' |
    r16 la' la' la' r la' la' la' r la' la' la' |
    la'4 dod''8.( re''16 re''8. dod''16) |
    re''8-\sug\fp re''4 re''8 dod'' mi'' |
    la'-\sug\f re''4 re''8 si' sol' |
    fad'16-\sug\p fad'8 fad' fad' fad'16 mi' mi'8 mi'16 |
    fad'4
  }
>> r4 r2 |
R1 | \allowPageTurn
r4 <<
  \tag #'violino1 {
    si''8.\f si''16 sol''8. sol''16 mi''8. mi''16 |
    <dod'' mi''>1 |
    re''4\f r mid''2\f |
  }
  \tag #'violino2 {
    si'8.-\sug\f si'16 re''8. re''16 si'8. si'16 |
    <lad' dod''>1 |
    si'4-\sug\f r <sold' si'>2-\sug\f |
  }
>>
r2 r4 dod''8.\p re''16 |
re''4( dod''8.) re''16 re''4( dod''8.) re''16 |
re''4( dod'') <<
  \tag #'violino1 {
    sold''4. la''16 si'' |
    \grace si''8 la''4 sold''8 fad'' sold''4. la''16 si'' |
    si''2( la''4 sold'') |
    fad''2~ fad''8 sold''16 la'' sold''8 fad'' |
    fad''2( mid''4) r8 dod'' |
    fad''2.\f dod''4\p |
    re''4. re''8 dod''4. si'8 |
    la'8 r la'4( dod'' fad'') |
    r re''( re''' si'') |
    sold''8\f sold''4 sold'' sold'' sold''8~ |
    sold''4\p mid'' dod''4. si'8 |
    \grace si'8 la'2 r8 la'16\f dod'' fad''8 fad'' |
    mi''4.\p mi'8 mi''4. re''8 |
    dod''4 la16 si dod' si la4 r8 mi' |
    la'( si' dod'' re'') mi''8. dod'16 \grace re'8 dod' si16 dod' |
    re'8 re'4\f fad' la' re''8 |
    fad'' fad''4 fad'' la''8 sold'' fad'' |
    mid''4 dod''2 dod''8.(\f la''16) |
    la''8\sf( sold'' fad'' mid'') fad''(\sf mi'' re'' dod'') |
    re''8. <re'' si''>16\f q2 re''4\p |
    dod'' fad'' \grace mi''8 re''4 dod''8 si' |
    la'2. sold'4 |
    la'8 r la'4:16\f dod'':16 fad'':16 |
    la''8(\sf sold'' fad'' mid'') fad''(\sf mi'' re'' dod'') |
    re''8. re'''16\f re'''2 re''4\p |
    dod'' fad'' \grace mi''8 re''4 dod''8 si' |
    la'2. sold'4 |
  }
  \tag #'violino2 {
    dod''8 dod''4 dod''8 |
    dod'' dod''4 dod'' dod'' dod''8 |
    dod''1~ |
    dod''4 fad' si'2 |
    dod''2~ dod''4 r |
    <la' dod'>2.:16-\sug\f la'4:16-\sug\p |
    sold'2:16 sold':16 |
    <la' dod'>2:16 q:16 |
    <re' si'>:16 q:16 |
    <sold' si'>8-\sug\f q4 q q q8 |
    <sold' si'>2:16-\sug\p q:16 |
    <fad' la'>:16 q:16-\sug\f |
    <mi' si'>:16-\sug\p q:16 |
    <mi' dod''>:16 q:16 |
    <mi' la'>:16 q:16 |
    << { s8 s-\sug\f } { <fad' la'>2:16 q:16 } >> |
    la'4:16 si':16 sid'2:16 |
    dod''2. dod'8.-\sug\f( la'16) |
    la'8-\sug\sf( sold' fad' mid') fad'-\sug\sf( mi' re' dod') |
    re'8. <si' sold''>16-\sug\f q2 sold'4-\sug\p |
    la'2 si'4 la'8 sold' |
    fad'2. mid'4 |
    fad'8 r la4-\sug\f( dod' fad') |
    la'8-\sug\sf( sold' fad' mid') fad'-\sug\sf( mi' re' dod') |
    re'8. <si' sold''>16-\sug\f q2 sold'4-\sug\p |
    la' fad'2 fad'4 |
    fad'2. mid'4 |
  }
>>
<fad' la>2\ff~ fad'8 sol'16 la' si' dod'' re'' mi'' |
fad''4. <<
  \tag #'violino1 {
    re''8 re''4. <fad' la'>8 |
    q4. <re' fad'>8 q4. re'8 |
    mid''2.\fermata mid''4\p |
    r4 fad'' r si' |
    la'2. sold'4 |
  }
  \tag #'violino2 {
    <fad' la'>8 q4. <re' fad'>8 |
    q4. <la re'>8 q4. la8 |
    <sold' si'>2.\fermata sold'4-\sug\p |
    r <fad' dod''> r sold' |
    fad'2. mid'4 |
  }
>>
fad'8\f la' dod'' fad'' <<
  \tag #'violino1 {
    sold''4. la''16 si'' |
    \grace si''8 la''4 sold''8 fad'' sold''4. la''16 si'' |
    \grace si''8 la''4 sold''8 fad'' si''4. dod'''16 re''' |
    dod'''4:16 la'':16 sold'':16 dod''':16 |
    la''2 si''4. dod'''16 re''' |
    dod'''4:16 la'':16 sold'':16 dod''':16 |
  }
  \tag #'violino2 {
    dod''2:16 |
    dod'':16 dod'':16 |
    dod'':16 fad''4:16 re'':16 |
    fad'16 fad'' fad'' fad'' fad''2:16 mid''4:16 |
    fad''2:16 fad''4:16 re'':16 |
    dod''16 fad'' fad'' fad'' fad''2:16 mid''4:16 |
  }
>>
fad''4. <fad' la>8 q4 q |
q2 r |
