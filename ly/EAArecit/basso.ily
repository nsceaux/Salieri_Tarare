\clef "bass" r8 |
fad\ff fad fad fad fad2:8 |
mid2\sf mid\sf^\markup\tiny * ^\markup\tiny\center-align\concat { [source :  \italic fa♯ ] } |
mid mid8 dod mid dod |
fad dod fad dod mid dod mid dod |
fad dod la, dod fad, fad fad fad |
mi4.. mi16 mi4.. mi16 |
<< mi1~ { s2 s\p } >> |
mi2 lad,\fp~ |
lad, si,4 r |
R1 |
r2 r4 r\fermata |
fad8\fp fad fad fad fad2:8 |
mid:8 mid:8 |
fad:8 fad:8 |
si,:8 si,:8 |
mid:8-\sug\f mid:8 |
<< mid:8 { s4 s\p } >> mid2:8 |
fad2:8 fad:8\fp |
sold:8\p sold:8 |
la:8 la:8 |
dod:8 dod:8 |
<< re:8 { s8 s4.\f } >> re2:8 |
re:8\p re:8 |
dod2 r |
R1 |
r8. mid16\f mid2 mid4\p |
fad2( si,) |
dod:8 dod:8 |
fad4 r r2 |
R1 |
r8. mid16\f mid2 mid4\p |
fad la, si, re |
dod2 dod |
fad4. fad8\f fad4 mi |
<< re1~ { s4 s2.\p } >> |
re1 |
re2 dod~ |
dod4 lad,2.\fp |
si,4\p si, si, |
mi mi mi |
mi mi mi |
la, la, la, |
la, la, la, |
re re re |
\clef "alto/tenor" <>_"Violoncelli" \slurDashed
fad'8 mi'16.( fad'32) sol'8.( fad'16 si'8. la'16) | \slurSolid
sold'!2\fp sol'4 |
fad'8\noBeam\f \clef "bass" <>^"tutti" fad8 fad re sol mi |
la\p la la la la, la, |
re4 r r2 |
R1 | \allowPageTurn
r4 sol\f si sol |
fad1 |
si4\f r si,2\f |
r2 r4 dod'8.\p re'16 |
re'4( dod'8.) re'16 re'4( dod'8.) re'16 |
re'4( dod') mid8 dod mid dod |
fad dod la dod mid dod mid dod |
fad2( mi! |
re) re' |
dod'~ dod'4 r |
fad2:8\f fad:8\p |
mid:8 mid:8 |
fad:8 fad:8 |
si,:8 si,:8 |
mid:8\f mid:8 |
mid:8\p mid:8 |
fad:8 fad:8\f |
sold:8\p sold:8 |
la:8 la:8 |
dod:8 dod:8 |
<< { s8 s-\sug\f } { re2:8 re:8 } >> |
re:8 re:8 |
dod2 r |
R1 | \allowPageTurn
r8. mid16\f mid2 mid4\p |
fad2 si, |
dod:8 dod:8 |
fad2 r |
R1 |
r8. mid16\f mid2 mid4\p |
fad la, si, re |
dod2:8 dod:8 |
re:8\ff re:8 |
re:8 re:8 |
re:8 re:8 |
si,2.\fermata si4\p |
la r si si, |
dod2:8 dod:8 |
fad8\f dod fad dod mid dod mid dod |
fad dod fad dod mid dod mid dod |
fad dod fad dod re re si, si, |
la, la, re re si, si, dod dod |
fad fad la fad re re si, si, |
la, la, re re si, si, dod dod |
fad,4. fad8 fad4 fad |
fad2 r |
