\clef "soprano/treble" <>^\markup\character Astasie r8 |
R1*6 |
r2 <>^\markup { \italic Récit chanté } r4 r8 lad' |
dod''8 dod'' r mi''16 fad'' sol''4. dod''8 |
mi'' mi'' re'' dod'' re'' re''
\ffclef "soprano/treble" <>^\markup\character Spinette
r8 si' |
si' si' si' dod'' re''4 r8 re''16 re'' |
re''4 mi''8 fad'' si' si'
<<
  \tag #'basse { r4\fermata R1*23 }
  \tag #'tous {
    \ffclef "soprano/treble" r8\fermata <>^\markup\character Astasie dod'' |
    fad''2. dod''4 |
    re''4. re''8 dod''4. si'8 |
    la'2 r4 fad'' |
    re'' re'' r re'' |
    sold''1~ |
    sold''4 mid'' dod''4. si'8 |
    la'2 r4 fad'' |
    mi''4. mi''8 mi''4. re''8 |
    dod''4 dod'' r mi' |
    la'8[ si'] dod'' re'' mi''4 mi''8 mi'' |
    fad'2. fad'4 |
    fad''2~ fad''8 la'' sold'' fad'' |
    mid''4 dod'' r dod'' |
    la''8[ sold''] fad'' mid'' fad''[ mi''] re'' dod'' |
    re''2. re''4 |
    dod'' fad'' \grace mi''8 re''4 dod''8[ si'] |
    la'2.\melisma sold'4\melismaEnd |
    \grace sold'8 la'2 r4 dod'' |
    la''8[ sold''] fad'' mid'' fad''[ mi''] re'' dod'' |
    re''2. re''4 |
    dod'' fad'' \grace mi''8 re''4 dod''8[ si'] |
    la'2.( sold'4) |
    fad'2 r |
  }
>>
<>^"Récit" r4 r8 fad'16 sol' la'8. la'16 la'8 la'16 la' |
re''4. re''8 fad'' fad'' mi'' fad'' |
re'' re'' r16 re'' dod'' si' lad'4 r16 lad' lad' si' |
dod''8. dod''16 fad''4~ fad''8. dod''16 mi''8 fad'' |
red''4 red''4. mi''8 |
re''2 r8 si' |
re''4. fad''8 mi''8. re''16 |
dod''4 r8 dod'' mi''8. dod''16 |
sol''4. fad''16[ mi''] mi''[ re''] re''[ dod''] |
re''4 re'' r |
re''8 dod''16. re''32 mi''8 re'' sol''8. fad''16 |
mi''2 r8 mi'' |
la''4. fad''8 mi''8. sol''16 |
re''2\melisma mi''4\melismaEnd |
re''4 r8 re''16 mi'' do''8. do''16 do''8 si'16 do'' |
la'4 la'8 si' do''4 do''8 re'' |
si' si' r4 r r8 si' |
dod''!4 dod''8 re'' mi''4 re''8 dod'' |
re''4 r8 re''16 si' mid''4 mid''8 fad'' |
dod''! dod'' r4 r2 |
R1*5 |
<<
  \tag #'basse { R1*37 }
  \tag #'tous {
    r2 r4 dod'' |
    fad''2. dod''4 |
    re''4. re''8 dod''4. si'8 |
    la'2 r4 fad'' |
    \grace mi''8 re''4 re'' r re'' |
    sold''1~ |
    sold''4 mid'' dod'' si' |
    la'2 r4 r8 fad'' |
    mi''!4. mi''8 mi''4. re''8 |
    dod''4 la' r mi' |
    la'8[ si'] dod'' re'' mi''4 mi''8 mi'' |
    fad'2. fad'4 |
    fad''2~ fad''8 la'' sold'' fad'' |
    mid''4 dod'' r dod'' |
    la''8[ sold''] fad'' mid'' fad''[ mi''] re'' dod'' |
    re''2. re''4 |
    dod'' fad'' \grace mi''8 re''4 dod''8[ si'] |
    la'2.( sold'4) |
    la'2 r4 dod'' |
    la''8[ sold''] fad'' mid'' fad''8[ mi''] re'' dod'' |
    re''2. re''4 |
    dod'' fad'' \grace mi''8 re''4 dod''8[ si'] |
    la'2.\melisma sold'4\melismaEnd |
    fad'2. fad'4 |
    fad''4.( re''8) re''4. la'8 |
    la'4.( fad'8) fad'4. re'8 |
    mid''2.\fermata mid''4 |
    \grace sold''8 fad''4 mid''8[ fad''] \grace mi''8 re''4 dod''8[ si'] |
    la'2.( sold'4) |
    fad'2 r |
    R1*7 |
  }
>>