\clef "alto" r8 |
fad'16-\sug\ff fad' fad' fad' fad'2:16 fad'4:16 |
mid'2-\sug\sf mid'-\sug\sf |
mid' mid'8 dod' mid' dod' |
fad' dod' fad' dod' mid' dod' mid' dod' |
fad' dod' la dod' fad la' la' la' |
sol'4.. sol'16 sol'4.. sol'16 |
<< sol'1~ { s2 s-\sug\p } >> |
sol'2 mi'\fp~ |
mi' re'4 r |
R1 |
r2 r4 r\fermata |
fad'2:16-\sug\fp fad':16 |
mid':16 mid':16 |
fad'2:16 fad':16 |
fad'2:16 fad':16 |
dod''8-\sug\f dod''4 dod'' dod'' dod''8 |
<< { s4 <>-\sug\p } { dod''2:16 dod'':16 } >> |
dod''2:16 dod'':16-\sug\fp |
sold':16-\sug\p sold':16 |
la':16 la':16 |
dod':16 dod':16 |
<< { s8 <>-\sug\f } { re'2:16 re':16 } >> |
la4:16-\sug\p si:16 sid2:16 |
dod'2. dod'8.(-\sug\f la'16) |
la'8(-\sug\sf sold' fad' mid') fad'(-\sug\sf mi' re' dod') |
re'8. mid'16-\sug\f mid'2 mid'4-\sug\p |
fad'2. si4 |
dod'1 |
fad4 la(-\sug\f dod' fad') |
la'8(-\sug\sf sold' fad' mid') fad'(-\sug\sf mi' re' dod') |
re'8. mid'16-\sug\f mid'2 mid'4-\sug\p |
fad' la si re' |
dod'1 |
fad'4. fad'8\f fad'8. fad'16 mi'8. mi'16 |
<< re'1~ { s4 s2.\p } >> |
re'1 |
re'2 dod'~ |
dod'4 fad'2.-\sug\fp |
r16 fad'-\sug\p fad' fad' r fad' fad' fad' r fad' fad' fad' |
r sold' sold' sold' r sold' sold' sold' r sold' sold' sold' |
r sold' sold' sold' r sold' sold' sold' r sold' sold' sold' |
r mi' mi' mi' r mi' mi' mi' r mi' mi' mi' |
r mi' mi' mi' r mi' mi' mi' r mi' mi' mi' |
r16 fad' fad' fad' r fad' fad' fad' r fad' fad' fad' |
fad'8 mi'16.( fad'32) sol'8.( fad'16 si'8. la'16) |
sold'!2\fp sol'4 |
fad'8\f fad' fad' re' sol'? sold |
la16\p la'8 la' la' la'16 sol' sol'8 sol'16 |
fad'4 r r2 |
R1*3 | \allowPageTurn
re'4-\sug\f r <re' si'>2-\sug\f |
r2 r4 dod'8.-\sug\p re'16 |
re'4( dod'8.) re'16 re'4( dod'8.) re'16 |
re'4( dod') mid'8 dod' mid' dod' |
fad' dod' la' dod' mid' dod' mid' dod' |
fad'2( mi' |
re'2.) fad'4 |
sold'2~ sold'4 r |
fad'2:16-\sug\f fad':16-\sug\p |
mid':16 mid':16 |
fad'2:16 fad':16 |
fad'2:16 fad':16 |
dod''8-\sug\f dod''4 dod'' dod'' dod''8 |
dod''2:16-\sug\p dod'':16 |
dod''2:16 dod'':16-\sug\f |
sold':16-\sug\p sold':16 |
la':16 la':16 |
dod':16 dod':16 |
<< { s8 s-\sug\f } { re'2:16 re':16 } >> |
la4:16 si:16 sid2 |
dod'2. dod'8.-\sug\f( la'16) |
la'8-\sug\sf( sold' fad' mid') fad'-\sug\sf( mi' re' dod') |
re'8. mid'16-\sug\f mid'2 mid'4-\sug\p |
fad'2. si4 |
dod'1 |
dod'8 r la4-\sug\f( dod' fad') |
la'8-\sug\sf( sold' fad' mid') fad'-\sug\sf( mi' re' dod') |
re'8. mid'16-\sug\f mid'2 mid'4-\sug\p |
fad' la si re' |
dod'1 |
re2-\sug\ff~ re8 mi16 fad sol la si dod' |
re'4. re'8 re'4. re'8 |
re'4. la8 la4. fad8 |
re''2.\fermata re'4-\sug\p |
dod' r si r |
dod'2:8 dod':8 |
fad'8\f dod' fad' dod' mid' dod' mid' dod' |
fad' dod' fad' dod' mid' dod' mid' dod' |
fad' dod' fad' dod' re' re' si si |
la la re' re' si si dod' dod' |
fad' fad' la' fad' re' re' si si |
la la re' re' si si dod' dod' |
fad4. fad'8 fad'4 fad' |
fad'2 r |

