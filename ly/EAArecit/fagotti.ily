\clef "tenor" r8 |
fad'1-\sug\ff |
mid'2-\sug\sf mid'-\sug\sf |
mid' \clef "bass" mid8 dod mid dod |
fad dod fad dod mid dod mid dod |
fad dod la, dod fad, fad fad fad |
mi4.. mi16 mi4.. mi16 |
mi2 r |
R1*3 |
r2 r4 r\fermata |
\clef "tenor" fad'1-\sug\fp |
mid' |
fad'2 r |
R1 |
mid'1-\sug\f~ |
<< mid'1 { s4 <>-\sug\p } >> |
fad'2 r |
R1*2 |
\clef "bass" r2 r8. dod'16 \grace re'8 dod'8 si16 dod' |
re'1 |
R1*3 |
r8. mid16-\sug\f mid2 r4 |
R1*2 |
r4 la(-\sug\f dod' fad') |
R1 |
r8. mid16-\sug\f mid2 r4 |
R1*2 |
r4 r8 fad-\sug\f fad4 mi |
re2 r |
R1*3 |
si2.-\sug\p |
mi'~ |
mi'~ |
mi'~ |
mi' |
re' |
R2.*2 | \allowPageTurn
r8 fad4-\sug\f re8 sol mi |
la2-\sug\p la,4 |
re r r2 |
R1*11 | \allowPageTurn
\clef "tenor" fad'2-\sug\f~ fad'4 r |
R1*3 | \allowPageTurn
mid'1-\sug\f~ |
mid'-\sug\p |
fad'2 r |
R1*2 |
\clef "bass" r2 r8. dod'16 \grace re'8 dod' si16 dod' |
re'1 |
R1*3 |
r8. mid16-\sug\f mid2 r4 |
R1*2 |
r4 la-\sug\f( dod' fad') |
R1 |
r8. mid16-\sug\f mid2 r4 |
R1*2 |
re1\ff~ |
re |
re |
si2.\fermata r4 |
R1 |
dod'1 |
fad8\f dod fad dod mid dod mid dod |
fad dod fad dod mid dod mid dod |
fad dod fad dod re re si, si, |
la, la, re re si, si, dod dod |
fad fad la fad re re si, si, |
la, la, re re si, si, dod dod |
fad,4. fad8 fad4 fad |
fad2 r |
