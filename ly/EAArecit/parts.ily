\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (fagotti #:score-template "score-part-voix" #:clef "tenor")
   (oboi #:score "score-oboi")
   (silence #:score "score-silence")
   (corni #:tag-global ()
          #:tag-notes corni
          #:score-template "score-part-voix"
          #:instrument , #{ \markup\center-column { Cors \small en La } #})
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
