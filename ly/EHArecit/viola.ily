\clef "alto" sib16-\sug\f |
sib2.\fermata r4 |
r2 r8. do'16 do'8. do'16 |
do'1 |
fa4 r r2 |
sol'2:16-\sug\fp sol':16 |
sol':16 sol':16 |
sol':16 sol':16 |
sol'4 r r2 |
r4 re' sol r |
