\clef "alto/G_8" r16 |
r2\fermata <>^\markup\italic { d’un ton décidé } r4 sib8. sib16 |
sib4 sib8 sib mib' mib' r4 |
r r8 mib'16 mib' mib'4 mib'8 la |
sib4\fermata r8 re' re' re' re' re' |
sol'2. re'4 |
si2 r8 si si do' |
re' re' re' mib' fa'4 mib'8 re' |
mib'4 r8 do' fad'8. fad'16 fad'8 sol' |
re'4 r r2 |
