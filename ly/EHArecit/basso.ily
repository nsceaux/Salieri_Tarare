\clef "bass" mib16\f |
mib2.\fermata r4 |
r2 r8. do16 do8. do16 |
do1 |
sib,4 r r2 |
sol,16\fp sol sol sol sol4:16 sol2:16 |
sol:16 sol:16 |
sol:16 sol:16 |
do4 r r2 |
r4 re sol, r |
