\clef "treble" <sol mib'>16\f |
q2.\fermata r4 |
r2 r8. <<
  \tag #'violino1 {
    la''16 la''8. la''16 |
    la''1 |
    sib''4
  }
  \tag #'violino2 {
    mib''16 mib''8. mib''16 |
    mib''1 |
    re''4
  }
>> r4 r2 |
<<
  \tag #'violino1 {
    <si' sol''>2:16\fp q:16 |
    q:16 q:16 |
    q:16 q:16 |
    mib''4
  }
  \tag #'violino2 {
    <re' si'>2:16-\sug\fp q:16 |
    q:16 q:16 |
    q:16 q:16 |
    <mib' do''>4
  }
>> r4 r2 |
r4 <re' la' fad''> <re' sib' sol''> r |
