\clef "soprano/treble" <>^\markup\character-text Astasie à Tarare abimé de douleur ^"Parlé"
sol'8 sol' sol' la' |
sib' sib'16 do'' re''8 r16 re'' sib'4 sib'8 sib' |
sib'4 do''8 re'' sol'4
\ffclef "tenor/G_8" <>^\markup\character-text Tarare vivement
r8 sib |
re' re' r\fermata re'16 mib' fa'8 fa'
\ffclef "soprano/treble" <>^\markup\character-text Astasie se jettant dans ses bras
fa''8. sib'16 |
sib''1 |
sib''4 r r2 |
\ffclef "bass" <>^\markup\character-text Arthénée au Roi
r4 r8 sib sol sol lab sib |
mib4 r
\ffclef "bass" <>^\markup\character-text Atar furieux
r8 mib' mib' mib' |
mib' sib r sib16 sib sol8. sib16 reb'8 reb'16 mib' |
do'4 r^\markup\italic les Soldats s’avancent do'4 r8 do'16 do' |
do'8 sol sol sol mi mi r4 |
do'8 do' do' reb' sib4 sib8 do' |
lab4 r r2 |
do'4. do'16 do' lab8 lab16 lab lab8 sib16 do' |
fa8 fa r fa'16 fa' si4 si16 si si do' |
sol4 r
