\clef "treble" r2 |
R1*2 |
<<
  \tag #'violino1 {
    r8 r16 re'' re''8 r\fermata fa''4 r |
    mib''8\ff sib''([ sol'' mib'']) re''4.( mib''16 fa'') |
    mib''8 sib''([ sol'' mib'']) fa''4.( sol''16 lab'') |
    sol''4 r r2 |
    r4 <sib' sib''>4\f r2 |
    R1 |
    r4 <do'' lab''> <do''' do''> r |
  }
  \tag #'violino2 {
    r8 r16 lab' lab'8 r\fermata lab'4 r |
    sib'2:16-\sug\ff sib':16 |
    sib'2:16 sib':16 |
    sib'4 r r2 |
    r4 <mib' sib' sol''>-\sug\f r2 |
    R1 |
    r4 <mib' mib''> <do'' sol''> r |
  }
>>
R1*2 |
<do'' lab''>2:16\fp q:16 |
q:16 q:16 |
q:16 <si'! re'>4 r |
r <sol' re'' si''!>4
