\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff \with { \flautiInstr \consists "Metronome_mark_engraver" } <<
        \global \includeNotes "flauti"
      >>
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroupNoBar <<
      \new StaffGroupNoBracket <<
        \new GrandStaff \with { \violiniInstr } <<
          \new Staff \with { \consists "Metronome_mark_engraver" } <<
            \global \keepWithTag #'violino1 \includeNotes "violini"
          >>
          \new Staff <<
            \global \keepWithTag #'violino2 \includeNotes "violini"
          >>
        >>
        \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      >>
      \new Staff \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with {
        \bassoInstr
        \consists "Metronome_mark_engraver"
      } <<
        \global \includeNotes "basso"
        \origLayout { s2 s1*2\break s1*5\break s1*4\pageBreak }
      >>
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
