Ne m’im -- pu -- te pas, é -- tran -- ger,
ta mort que je vais par -- ta -- ger.

Qu’en -- tends- je ? As -- ta -- si -- e !

Ah ! Ta -- ra -- re !

Je te l’a -- vais pré -- dit.

Qu’on les sé -- pa -- re.
Qu’un seul coup les fas -- se pé -- rir.
Non… C’est trop tôt bri -- ser leurs chaî -- nes ;
ils se -- raient heu -- reux de mou -- rir.
Ah ! je me sens al -- té -- ré de leurs pei -- nes,
et j’ai soif de les voir souf -- frir.
