\clef "treble" r2 |
R1*3 |
<>^\markup\italic { une flûte avec le \concat { 1 \super er } Violon }
mib''8\ff sib''([ sol'' mib'']) re''4.( mib''16 fa'') |
mib''8 sib''([ sol'' mib'']) fa''4.( sol''16 lab'') |
sol''4 r r2 |
R1*8 |
r2