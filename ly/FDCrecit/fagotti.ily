\clef "tenor" r2 |
R1*3 |
mib'2-\sug\f( fa' |
sol' re') |
mib'4 r r2 |
R1*8 |
r2
