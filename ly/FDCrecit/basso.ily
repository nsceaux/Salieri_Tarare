\clef "bass" r2 |
R1*2 |
r8 r16 fa fa8 r\fermata re4 r |
mib2:8\ff fa:8 |
sol:8 re:8 |
mib4 r r2 |
r4 mib\f r2 |
R1 |
r4 lab mi! r |
R1*2 |
fa2:16\fp fa:16 |
fa:16 fa:16 |
fa:16 fa4 r |
r sol
