\clef "treble" r2 |
R1*3 |
<>\f \twoVoices#'(oboe1 oboe2 oboi) <<
  { sib''1~ | sib''~ | sib''4 }
  { sib'1~ | sib'~ | sib'4 }
>> r4 r2 |
R1*8 |
r2
