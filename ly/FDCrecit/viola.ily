\clef "alto" r2 |
R1*2 |
r8 r16 fa' fa'8 r\fermata sib4 r |
sol'2:16-\sug\ff lab':16 |
sol'16 sib' sib' sib' sib'4:16 fa'2:16 |
sol'4 r r2 |
r4 <sib sol' mib''>4-\sug\f r2 |
R1 |
r4 lab' mi'! r |
R1*2 |
fa'2:16\fp fa':16 |
fa':16 fa':16 |
fa':16 fa'4 r |
r sol'
