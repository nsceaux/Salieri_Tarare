\clef "alto/G_8" <>^\markup\character-text Calpigi à demi voix
sol8 sol |
do'4 do' do' mi'8 do' |
do'2 si4 do'8 la |
sol4 sol sol la8 fad |
sol4 sol8 sol si4 do'8 do' |
re'2. fa4 |
mi mi r sol |
do'8 do' do' do' sol4. sib8 |
la la si! dod' re'4. do'8 |
si4 r
\ffclef "tenor/G_8" <>^\markup\character Tarare
r8 re' re' la |
si4 si
\ffclef "alto/G_8" <>^\markup\character Calpigi
r8 re' re' re' |
si
\ffclef "tenor/G_8" <>^\markup\character Tarare
sol8 fa'4. re'8 si la |
sold4 mi
\ffclef "alto/G_8" r4\fermata <>^\markup\character Calpigi r8 mi |
la4 la la la |
si si8 si si4 mi'8 re' |
dod'2 r |
re'8 la la la la4 sol8 la |
fa2 r8 la re' do' |
si!4 si8 si si4 la8 si |
do'2 do'4 r |
sol sol8 sol sol4. la8 |
sib4 sib8 sib sib4 do'8 sib |
la2 r4 la |
sold sold8 sold sold4 la8 la |
si!4 si r8 si si si |
si4 si si do' |
re'\fermata
\ffclef "tenor/G_8" <>^\markup\character Tarare
r8 si si si16 si mi'8 re'16 mi' |
do'8 do' r4
\ffclef "alto/G_8" r8\fermata <>^\markup\character-text Calpigi Récit la si dod' |
re'4 re' r mi' |
la2 r |
r r4\fermata
