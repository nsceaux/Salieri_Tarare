\clef "alto" r4 |
do'-\sug\p do' do' do' |
re' re' re' do' |
si si si do' |
si si si si |
si si si si |
do' do' do' do' |
mi mi mi mi |
fa fa fa fad |
sol8\noBeam sol16-\sug\f la \grace do' si8 la16 sol fad2 |
sol8\noBeam sol16-\sug\p la \grace do' si8 la16 sol fad4 r |
sol4 re-\sug\f re re-\sug\p |
mi mi'-\sug\f r2\fermata |
la4 la la la |
sold sold sold sold |
sol! sol sol sol |
fa fa dod dod |
re re re re |
sol sol sol sol |
do do' do' do' |
do' do' do' do' |
do' do' do' do' |
la1 |
si!2. la4 |
sold1~ |
sold4 si2 la4 |
sold1\fermata |
r4 mi' mi'\fermata r |
re' r si r |
do' r\fermata r8. la16 \grace si la8\ff sol16 la |
sol!2 r4\fermata
