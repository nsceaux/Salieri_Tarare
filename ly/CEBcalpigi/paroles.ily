Cet -- te belle et tendre As -- ta -- si -- e
que tu vas cher -- cher au ha -- sard
sur le vaste o -- cé -- an d’A -- si -- e,
elle est dans le ser -- rail d’A -- tar,
sous le faux nom d’Ir -- za…

Qui l’a ra -- vi -- e ?

C’est Al -- ta -- mort.

Ô lâ -- che per -- fi -- di -- e !

Le golfe où nos plon -- geurs vont cher -- cher le co -- rail,
bai -- gne les jar -- dins du sé -- rail :
si, dans la nuit, ton cou -- rage in -- fle -- xi -- ble
o -- se de cet -- te route af -- fron -- ter le dan -- ger,
de soie une é -- chelle in -- vi -- si -- ble,
ten -- due à l’an -- gle du ver -- ger…

A -- mi gé -- né -- reux, se -- cou -- ra -- ble…

Le tem -- ple s’ou -- vre, a -- dieu.
