\clef "treble" r4 <<
  \tag #'violino1 {
    mi'8-.\p sol'-. mi'-. sol'-. mi' sol' mi' sol' |
    fa' sol' fa' sol' fa' sol' mi' sol' |
    re' sol' re' sol' re' sol' fad' la' |
    sol' re' sol' re' sol' re' sol' re' |
    sol' re' sol' re' sol' re' sol' re' |
    mi' sol' mi' sol' mi' sol' mi' sol' |
    sol' do'' sol' do'' sol' do'' sol' do'' |
    la' do'' la' dod'' la' re'' la' re'' |
    si'\noBeam 
  }
  \tag #'violino2 {
    do'4-\sug\p do' do' do' |
    re' re' re' do' |
    si si si do' |
    si si si si |
    si si si si |
    do' do' do' do' |
    do' do' do' do' |
    do' fa' re' re' |
    re'8\noBeam
  }
>> sol'16\f la' \grace do'' si'8 la'16 sol' fad'2 |
sol'8\noBeam sol'16\p la' \grace do'' si'8 la'16 sol' <<
  \tag #'violino1 {
    re''4 r |
    si'8 r
  }
  \tag #'violino2 {
    la'4 r |
    re'8 r
  }
>> <re' la' fa''!>4\f r8 re''\p si' la' |
sold'4 <mi' si' sold''>4\f r2\fermata |
<<
  \tag #'violino1 {
    r4 la' la' la' |
    r si' si' si' |
    r4 la' la' la' |
    la'1~ |
    la' |
    si' |
    do''~ |
    do'' |
    mi' |
  }
  \tag #'violino2 {
    do'8 mi' do' mi' do' mi' do' mi' |
    si mi' si mi' si mi' si mi' |
    \rt#2 { dod' mi' } \rt#2 { dod' mi' } |
    re'8 fa' re' fa' mi' sol' mi' sol' |
    fa' re' fa' re' \rt#2 { fa' re' } |
    fa'8 re' fa' re' fa' re' fa' re' |
    \rt#2 { mi' sol' } \rt#2 { mi' sol' } |
    \rt#2 { mi' sol' } mi' sol' fa' la' |
    sol' sib' sol' sib' sol' sib' sol' mi' |
  }
>>
do'8( fa' la' fa') re'( fa' la' re') |
<<
  \tag #'violino1 {
    re'8( mi' sold' mi') sold'( mi' la' mi') |
    si'( sold' mi' sold') si'( sold' mi' sold') |
    si' sold' mi' sold' si' mi' do'' mi' |
    re''1\fermata |
    r4 \grace re''16 do''8 si'16 do'' dod''4\fermata r |
    re''4 r mi'' r |
  }
  \tag #'violino2 {
    re'2. do'4 |
    si1~ |
    si4 re'2 do'4 |
    si1\fermata |
    r4 la' la'\fermata r |
    la'4 r << si' \\ sold' >> r |
  }
>>
la'4 r\fermata r8. la16 \grace si la8\ff sol16 la |
sol2 r4\fermata
