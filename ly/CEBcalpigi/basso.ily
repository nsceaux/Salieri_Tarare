\clef "bass" r4 |
R1*8 |
r8 sol16\f la \grace do' si8 la16 sol fad2 |
sol4 r fad\p r |
sol8 r re4\f re re\p |
mi mi,\f r2\fermata |
R1*5 |
<>^"Violoncelli soli" sol4 sol sol sol |
do do do do |
do do do do |
do do do do |
fa1 |
mi~ |
mi~ |
mi~ |
mi\fermata |
r4 la sol!\fermata r |
fa r mi r |
la r\fermata r la,\ff |
sol,!2 r4\fermata
