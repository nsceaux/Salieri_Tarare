\clef "treble" R1*2 |
r4 <re' la' fad''>4\f r2 |
R1 |
r16 sol\f la si do' re' mi' fad' sol'4 r |
R1 |
r8. <<
  \tag #'violino1 { <do'' sol''>16 q4 }
  \tag #'violino2 { <mi' do''>16 q4 }
>> r2 |
<do'' la''>4 r r2 |
r4 <<
  \tag #'violino1 { fa''4\f mi''2~ | mi''1\p~ | mi''2~ mi''4 }
  \tag #'violino2 { si'4\f si'2~ | si'1\p~ | si'2( do''4) }
>> r4 |
R1 |
<<
  \tag #'violino1 {
    dod''4\p dod''8. dod''16 mi''8 mi''16 mi'' mi''8 dod'' |
    lad'4\fermata
  }
  \tag #'violino2 {
    mi'4-\sug\p mi'8. mi'16 dod'8 dod'16 dod' dod'8 mi' |
    fad'4\fermata
  }
>> fad'8 fad' si'4 si'8 re'' |
dod'' fad' dod'' re'' \grace { dod''16 re'' } mi''4 re''8 dod'' |
re''4. re''8 <<
  \tag #'violino1 {
    re''8 re'16 fad' la'8 si' |
    sol'4 dod''8 re'' \grace { dod''16 re'' } mi''4 sol'8. la'16 |
    fad'4 r8 la' la'( si' dod'' re'') |
    si'( la' si' red'') mi'' si' sol'' mi'' |
    re''2. dod''4 |
    re''1~ |
    re''2( do''!)~ |
    do''1~ |
    do''2 si'4
  }
  \tag #'violino2 {
    la4. fad'8 |
    si4 sol'8 fad' sol'4 mi'8 dod' |
    re'4 r re'2 |
    re'4. la'8 si'4 si' |
    r8 la'( sold' la' sold' la' sol' la') |
    <fad' la'>1 |
    q~ |
    q~ |
    q2 sol'4
  }
>> r4 |
R1 |
r2 <<
  \tag #'violino1 { mi''2~ | mi''1 | dod''4 }
  \tag #'violino2 { si'2~ | si'1 | la'4 }
>> r4 r2 |
<<
  \tag #'violino1 { re''4 }
  \tag #'violino2 { la' }
>> r4 r2 |
r2 <<
  \tag #'violino1 { fa''4 }
  \tag #'violino2 { do'' }
>> r4 |
R1 |
<<
  \tag #'violino1 { fa''4 }
  \tag #'violino2 { re'' }
>> r4 r2 |

