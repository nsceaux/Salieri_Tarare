\clef "alto" R1*2 |
r4 re'-\sug\f r2 |
R1 |
r16 sol\f la si do' re' mi' fad' sol'4 r |
R1 |
r8. mi'16 mi'4 r2 |
fa'!4 r r2 |
r4 re'-\sug\f mi'2~ |
mi'1-\sug\p~ |
mi'2~ mi'4 r |
R1 |
la4-\sug\p la8. la16 la8 la'16 la' la'8 la |
dod'4\fermata r4 re'8 fad' re' si |
lad4 lad8 si \grace { lad16 si } dod'4 si8 lad |
si fad' re' si fad4. re8 |
mi4 mi'8 re' dod' la16 si dod'8 la |
re'4 r fad2 |
sol4. fad8 sol4 sol |
r8 fad'( mid' fad' mid' fad' mi' mi') |
re'1 |
re'~ |
re'~ |
re' |
R1 |
r2 mi'~ |
mi'1 |
mi'4 r r2 |
re'4 r r2 |
r fa'4 r |
R1 |
fa'4 r r2 |
