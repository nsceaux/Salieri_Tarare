Un grand roi vous in -- vi -- te à fai -- re son bon -- heur.
L’a -- mour met à vos pieds le maî -- tre de la ter -- re.
Que de beau -- tés i -- ci bri -- gue -- raient cet hon -- neur !
Loin de s’en a -- lar -- mer, on peut en ê -- tre fiè -- re.

Ah ! vous n’a -- vez pas eu Ta -- ra -- re pour a -- mant !

Je ne le con -- nais point ; j’ai -- me sa re -- nom -- mé -- e ;
mais, pour lui, com -- me vous, si j’é -- tais en -- flam -- mé -- e,
a -- vec le dur A -- tar je fein -- drais un mo -- ment ;
et j’ins -- trui -- rais Ta -- rare au moins de ma souf -- fran -- ce.

À la plus lé -- gère es -- pé -- ran -- ce
le cœur de mal -- heu -- reux s’ou -- vre fa -- ci -- le -- ment.
J’ai -- me ton noble at -- ta -- che -- ment :
eh bien ! fais- lui sa -- voir qu’en cette en -- ceinte hor -- ri -- ble…

Ca -- chez vos pleurs, s’il est pos -- si -- ble.
Des se -- crets plai -- sirs du sul -- tan
je vois venir le mi -- nistre in -- so -- lent.
