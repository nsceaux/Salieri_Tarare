\clef "bass" R1*2 |
r4 re\f r2 |
R1 |
r16 sol,\f la, si, do re mi fad sol4 r |
R1 | 
r8. mi16 mi4 r2 |
fa!4 r r2 |
r4 sol\f sold2~ |
sold1\p~ |
sold2 la4 r |
R1 | \allowPageTurn
sol!4\p sol8. sol16 sol8 sol16 sol sol8 sol |
fad4\fermata r \clef "tenor" re'8 fad' re' si |
lad4 lad8 si \grace { lad16 si } dod'4 si8 lad |
si fad' re' si fad4. re8 |
mi4 mi'8 re' dod' la16 si dod'8 la |
re'4 r fad4 fad |
sol sol8 fad sol4 sol |
la la la la |
\clef "bass" <>_"tutti" re1~ |
re~ |
re~ |
re2 sol |
R1 |
r2 sold~ |
sold1 |
sol!4 r r2 |
fa!4 r r2 |
r2 la,4 r |
R1 |
sib,4 r r2 |
