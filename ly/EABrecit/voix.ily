\clef "soprano/treble" <>^\markup\character-text Spinette parlé
r4 fad'8 sold' la'4 la'8 si' |
dod'' dod'' r dod'' la' la' si' dod'' |
fad'4 r8 la' re'' re'' re'' re'' |
la'4. la'8 do''! do'' do'' re'' |
si' si' r4 r2 |
re''8 re''16 re'' fa''!8 re'' si' si'16 do'' re''8 si'16 sol' |
do''4 r do''8. do''16 do''8 do''16 do'' |
la'4 r8 fa'' fa'' si' si' do'' |
sol' sol' r4
\ffclef "soprano/treble" <>^\markup\character Astasie
si'2 |
si'8 si' si' si' mi''4. si'8 |
re'' re'' re'' mi'' do''4 r |
\ffclef "soprano/treble" <>^\markup\character Spinette
do''8 do''16 do'' re''8 mi'' la'4 r |
<>^\markup\italic à rigueur dod''4 dod''8 dod'' mi''4. dod''8 |
lad'8\fermata lad' fad' fad' si'4 si'8 re'' |
dod''4 dod''8 re'' mi''4 re''8 dod'' |
re'' re'' r re'' re'' la' la' si' |
sol'4 dod''8 re'' mi''4 sol'8 la' |
fad'4 r8 la' la' si' dod'' re'' |
si'4. red''8 mi'' si' sol'' mi'' |
re''2.( mi''4) |
re'' r
\ffclef "soprano/treble" <>^\markup\character-text Astasie sans rigueur
la'8 la' la' la' |
re''4 re''8 mi'' do'' do'' r do'' |
do'' do'' re'' mi'' la'4 r |
do''8 do''16 do'' do''8 re'' si'4 r |
re'' re''8 re'' si' si' si' do'' |
sol'4 r8 si' mi''4 r16 si' si' si' |
sold'4 r8 si' si' si' dod'' re'' |
dod'' dod''
\ffclef "soprano/treble" <>^\markup\character Spinette
r16 la' la' si' dod''8. dod''16 dod''8 re'' |
la' la' r4 la'8 la' la' la' |
re''4 mi''8 fa'' do''4 r16 do'' do'' do'' |
la'4 do''8 re'' mib''4 do''8 re'' |
sib'4 r r2 |

