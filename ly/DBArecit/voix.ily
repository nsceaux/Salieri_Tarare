\clef "bass" r4 r8 re fad fad16 fad fad8 fad16 sol |
la8 la r la re'4 la8 si |
do'4 do'8 re' si4 r8 sol16 la |
si4 r si8 si si sol |
do'4 r16 do' do' mi' do'8 do' do' si |
sol sol r4 r2 |
