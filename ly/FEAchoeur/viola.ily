\clef "alto" <<
  { re'8[ lab'] sol'[ fa' mib' re'] } \\
  { lab[-\sug\mf fa'] mib'[ re'! do' si] }
>> |
do'16-\sug\f <mib' do''> q q q4:16 q2:16 |
q:16 q:16 |
q:16 q:16 |
<mib' sib'>:16 q:16 |
lab':16-\sug\fp lab':16-\sug\fp |
lab':16-\sug\fp lab':16-\sug\fp |
sol':16-\sug\f sol':16 |
fa'8 lab'[ lab' lab'] lab'2:8 |
