\piecePartSpecs
#`((violino1 #:indent 0)
   (violino2 #:indent 0)
   (viola #:indent 0)
   (basso #:indent 0 #:system-count 2)
   (oboi #:score-template "score-oboi" #:indent 0)
   (clarinetti #:score-template "score-oboi" #:indent 0)
   (fagotti #:tag-notes fagotti #:indent 0 #:clef "tenor" #:system-count 2)
   (corni #:score-template "score-part-voix"
          #:tag-notes corni #:tag-global ()
          #:indent 0)
   (silence #:indent 0)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet #}))
