\tag #'(basse astasie tarare) {
  chute, et je mour -- rai con -- tent.
}
\tag #'atar {
  O rage ! af -- freux tour -- ment !
}
\tag #'(vdessus vhaute-contre) { A -- tar, }
\tag #'(vdessus vhaute-contre basse) {
  dé -- fends- nous, sau -- ve- nous.
}
\tag #'(vdessus basse) {
  Du pa -- lais la garde est for -- cé -- e ;
}
\tag #'vhaute-contre {
  Du sé -- rail la porte en -- fon -- cé -- e.
}
\tag #'(vdessus vhaute-contre basse) {
  notre a -- sy -- le est à tes ge -- noux ;
  ta mi -- lice en fu - %- reur re -- de -- man -- de Ta -- ra -- re.
}
