\clef "treble"
<<
  \tag #'violino1 {
    re''8(\mf lab'' sol'' fa'' mib'' re'') |
    do''16\f <do'' lab''> q q q4:16 q2:16 |
    mib'':16 mib'':16 |
    do'':16 do''8 <mib'' mib'>4 q8 |
    q8 q4 q q q8 |
    q8\fp mib''4 mib''8 fa''\fp fa''4 fa''8 |
    mib''\fp mib''4 mib''8 do''\fp do''4 do''8 |
  }
  \tag #'violino2 {
    lab'8-\sug\mf([ fa''] mib''[ re'' do'' si']) |
    <mib' do''>2:16-\sug\f q:16 |
    q:16 q:16 |
    <do' mib'>:16 q8 do''4 do''8 |
    <reb''! mib'>8 q4 q q q8 |
    <mib' do''>8-\sug\fp do''4 do''8 reb''!-\sug\fp reb''4 reb''8 |
    do''-\sug\fp do''4 do''8 mib'-\sug\fp lab'4 lab'8 |
  }
>>
<sib' mi''>8\f q4 q q q8 |
fa''4 fa''16 mi'' fa'' sol'' lab'' sol'' fa'' mi'' fa'' mib'' reb'' do'' |
