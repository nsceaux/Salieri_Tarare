\clef "bass" fa8.\noBeam fa'16\f sol'8. sol16\p sol8. sol16 |
lab2:8\ff lab:8 |
lab:8 lab:8 |
lab:8 lab8 mib lab mib |
sol mib sol mib sol mib sol mib |
lab\fp mib do lab, reb\fp fa lab reb' |
lab\fp mib do lab, lab\fp mib do lab, |
sol,\f sol sol sol sol2:8 |
fa:8 fa:8 |
