<<
  \tag #'(astasie basse) {
    \clef "soprano/treble"
    re''8 lab'' sol'' fa'' mib'' re'' |
    do''4 r r2*1/2
  }
  \tag #'tarare {
    \clef "tenor/G_8"
    fa'8 fa' mib' re' do' si |
    do'4 r r2*1/2
  }
  \tag #'atar {
    \clef "bass"
    r8. fa'16 sol'8. sol16 sol8. sol16 |
    do'4 r r2*1/2
  }
  \tag #'(vdessus1 vdessus2) {
    \clef "soprano/treble"
    r4 r4 r8 sol'' |
    lab''2.
  }
  \tag #'(vhaute-contre1 vhaute-contre2) {
    \clef "alto/G_8" r4 r r8 sol' |
    lab'2.
  }
>>

<<
  \tag #'(vdessus1 basse) {
    \tag #'basse \ffclef "soprano/treble" lab''8 lab'' |
    mib''2. mib''8 mib'' |
    do''2 r4 mib''8 mib'' |
    mib''4 mib'' mib'' mib''8 mib'' |
    mib''4 do'' r2 |
    r r4 do''8 do'' |
    mi''4 mi'' mi''8 mi'' mi'' mi'' |
    fa''4 fa''8 fa'' fa''4 fa''8 fa'' |
  }
  \tag #'vdessus2 {
    lab''8 lab'' |
    mib''2. mib''8 mib'' |
    do''2 r4 do''8 do'' |
    reb''!4 reb'' reb'' reb''8 reb'' |
    do''4 lab' r2 |
    r r4 do''8 do'' |
    sib'4 sib' sib'8 sib' sib' sib' |
    lab'4 lab'8 lab' lab'4 fa'8 fa' |
  }
  \tag #'vhaute-contre1 {
    lab'8 lab' |
    mib'2. mib'8 mib' |
    do'2 r |
    r2 r4 mib'8 mib' |
    mib'4 mib' fa' fa'8 fa' |
    mib'4 do' r do'8 do' |
    mi'4 mi' mi'8 mi' mi' mi' |
    fa'4 fa'8 fa' fa'4 fa'8 fa' |
  }
  \tag #'vhaute-contre2 {
    lab'8 lab' |
    mib'2. mib'8 mib' |
    do'2 r |
    r2 r4 sib8 sib |
    do'4 do' reb'! reb'8 reb' |
    do'4 lab r do'8 do' |
    sib4 sib sib8 sib sib sib |
    lab4 lab8 lab lab4 fa8 fa |
  }
  \tag #'(astasie tarare atar) { s4 | R1*7 | }
>>
