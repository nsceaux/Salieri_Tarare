\clef "treble"
r8 \twoVoices#'(oboe1 oboe2 oboi) <<
  { lab''( sol'' fa'' mib'' re''!) |
    do''4^"tutti" lab''2 lab''4 |
    mib''1 |
    do''2. mib''4 |
    mib''1 |
    mib''2 fa'' |
    mib'' do'' |
    mi''!1 |
    fa''4 }
  { fa''8( mib'' re'' do'' si') |
    do''1~ |
    do'' |
    lab'2. do''4 |
    reb''!1 |
    do''2 reb'' |
    do'' lab' |
    sib'1 |
    lab'4 }
  { s8*5-\sug\mf | s1*4-\sug\f | s2-\sug\fp s-\sug\fp s-\sug\fp s-\sug\fp s-\sug\f }
>>
\tag #'oboi <>^"unis"
fa''16 mi'' fa'' sol'' lab'' sol'' fa'' mi'' fa'' mib'' reb'' do'' |
