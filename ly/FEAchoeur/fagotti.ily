\clef "tenor" r8. fa'16\f sol'8. sol16\p sol8. sol16 |
\clef "bass" lab1\f~ |
lab~ |
lab2 lab8 mib lab mib |
sol mib sol mib sol mib sol mib |
lab\fp mib do lab, reb\fp fa lab reb' |
lab\fp mib do lab, lab\fp mib do lab, |
sol,\f sol sol sol sol2:8 |
fa:8 fa:8 |
