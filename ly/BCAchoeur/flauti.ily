\clef "treble" r2 <>-\sug\f \twoVoices #'(flauto1 flauto2 flauti) <<
  { \grace re'''8 do'''-. si''-. do'''-. re'''-. |
    mi'''2 fa'''4 re''' |
    do'''2 sol''4 do''' |
    la'' si''8 do''' re'''4 mi'''8 fa''' |
    si''4 }
  { \grace fa''8 mi''-. re''-. mi''-. fa''-. |
    sol''2 la''4 fa'' |
    mi''2 mi''4 mi'' |
    fa''4. la''8 la'4 la'' |
    re''4 }
>> r4 \grace si''8 la'' sol'' la'' si'' |
do'''4 
\twoVoices #'(flauto1 flauto2 flauti) <<
  { mi'''4 \grace sol'''8 fa''' mi''' fa''' sol''' | mi'''2 }
  { mi''4 \grace sol''8 fa'' mi'' fa'' sol'' | mi''2 }
>> \grace si''8 la'' sol'' la'' si'' |
do'''4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { mi'''4 re''' sol''' | }
  { do'''4 si'' si'' | }
>>
do'''2 \twoVoices #'(flauto1 flauto2 flauti) <<
  { do'''8 si'' do''' re''' |
    mi'''2 fa'''4 re''' |
    do'''2 sol''4 do''' |
    la''4 si''8 do''' re'''4 mi'''8 fa''' |
    sol''4 }
  { mi''8 re'' mi'' fa'' |
    sol''2 la''4 fa'' |
    mi''2 mi''4 mi'' |
    fa''4. la''8 la'4 la'' |
    re'' }
>> r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { r4 sol''16-\sug\f la'' si'' dod''' |
    re'''4 re'''8 re''' re'''4 mi'''8 do''' |
    si''2. \grace si''8 la'' sol''16 la'' |
    sol''4 }
  { r2 | R1*2 | r4 }
>> r8 <>\f \twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''8 mi'''4 fa''' | sol'''4. fa'''8 mi'''4 re''' | mi''' }
  { si''8 do'''4 re''' | mi'''4. re'''8 do'''4 si'' | do'''4 }
>> r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { r4 mi''16\p sol'' do''' mi''' |
    re'''4 re'''8 re''' re'''4 mi'''8 do''' |
    si''2. \grace si''8 la'' sol''16 la'' |
    sol''4 }
  { r2 | R1*2 | r4 }
>> r8 re''8\f mi''4 fa'' |
sol'' mi''8 fa'' sol''4 la''8 fad'' |
sol''4 mi'' la'' fad'' |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''8 do''' do''' do''' do'''4 do''' |
    do'''2 si'' |
    do''' \grace si''8 la'' sol'' la'' si'' |
    do'''4 mi'' \grace sol''8 fa'' mi'' fa'' sol'' |
    mi''2 \grace si''8 la'' sol'' la'' si'' |
    do'''4 do'' \grace mi''8 re'' do'' re'' sol'' |
    do''2 }
  { sol''8 mi'' mi'' mi'' mi''4 mi'' |
    re''1 |
    do''2 \grace si'8 la' sol' la' si' |
    do''4 mi' \grace sol'8 fa' mi' fa' sol' |
    mi'2 \grace si'8 la' sol' la' si' |
    do''4 do'' \grace do''8 si' la' si' si' |
    do''2 }
>> r4 <>\p \twoVoices #'(flauto1 flauto2 flauti) <<
  { do'''8 re''' |
    mi''' mi''' mi'' mi''' mi'' mi''' mi'' mi''' |
    re''' re''' re'' re''' sol''' re''' si'' sol'' |
    fad''4 fad''16 sol'' la'' si'' do'''4 do'''8 re''' |
    si''2 }
  { sol''4 |
    sol'' sol'8 sol'' sol' sol'' sol' sol'' |
    sol' sol'' sol' sol'' sol' sol'' sol' sol'' |
    do'' re'' do'' re'' la' re'' la' re'' |
    sol'2 }
>> r2 |
R1*17 | \allowPageTurn
r2 <>-\sug\ff \twoVoices #'(flauto1 flauto2 flauti) <<
  { \grace re'''8 do''' si'' do''' re''' |
    mi'''2 fa'''4 re''' |
    do'''2 sol''4 do''' |
    la''4 si''8 do''' re'''4 mi'''8 fa''' |
    si''4 }
  { \grace fa''8 mi'' re'' mi'' fa'' |
    sol''2 la''4 fa'' |
    mi''2 mi''4 mi'' |
    fa''4 la''8 la'' la''4 la''8 la'' |
    re''4 }
>> r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { r4 sol''16\p la'' si'' do''' |
    re'''4 re'''8 re''' re'''4 mi'''8 do''' |
    si''2. \grace si''8 la'' sol''16 la'' |
    sol''4 }
  { r2 | R1*2 | r4 }
>> r8 <>\f \twoVoices #'(flauto1 flauto2 flauti) <<
  { re'''8 mi'''4 fa''' |
    sol'''4. fa'''8 mi'''4 re''' |
    mi'''4 }
  { si''8 do'''4 re''' |
    mi'''4. re'''8 do'''4 si'' |
    do''' }
>> r4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { r4 mi''16\p sol'' do''' mi''' |
    re'''4 re'''8 re''' re'''4 mi'''8 do''' |
    si''2. \grace si''8 la'' sol''16 la'' |
    sol''4 }
  { r2 | R1*2 | r4 }
>> r8 re''\f mi''4 fa''! |
\twoVoices #'(flauto1 flauto2 flauti) <<
  { sol''4 mi''8 fa'' sol''4 la''8 fad'' |
    sol''4 mi'' la'' fad'' |
    sol''8 do''' do''' do''' do'''2:8 |
    do'''2 si'' |
    do''' }
  { sol'4 mi'8 fa' sol'4 la'8 fad' |
    sol'4 mi' la' fad' |
    sol''8 mi'' mi'' mi'' mi''2:8 |
    re''1 |
    do''2 }
>> \grace si''8 la'' sol'' la'' si'' |
do'''4 mi'' \grace sol''8 fa'' mi'' fa'' sol'' |
mi''2 \grace si''8 la'' sol'' la'' si'' |
do'''4 \twoVoices #'(flauto1 flauto2 flauti) <<
  { mi''4 \grace mi''8 re'' do'' re'' sol'' |
    do''2 }
  { do'''4 \grace do''8 si' la' si' si' |
    do''2 }
>> r |
r2\fermata r4\fermata r8 do'\ff |
do'2.\fermata
