\clef "treble"
do'2\f <<
  \tag #'violino1 {
    \grace re''8 do''-. si'-. do''-. re''-. |
    mi''2 fa''4 re'' |
    do''2 sol'4 do'' |
    la' si'8-. do''-. re''4 mi''8-. fa''-. |
    si'4 r \grace si''8 la''-. sol''-. la''-. si''-. |
    do'''4 mi'' \grace sol''8 fa''-. mi''-. fa''-. sol''-. |
    mi''2 \grace si''8 la'' sol'' la'' si'' |
    do'''4 do'' \grace mi''8 re'' do'' re'' sol'' |
    do''2 \grace re''8 do'' si' do'' re'' |
    mi''2 fa''4 re'' |
    do''2 sol'4 do'' |
    la' si'8 do'' re''4 mi''8 fa'' |
    si'4
  }
  \tag #'violino2 {
    \grace fa'8 mi'-. re'-. mi'-. fa'-. |
    sol'2 la'4 fa' |
    <mi' sol>2 q4 q |
    <la fa'>4 la'8 la' la4 la'8 la' |
    re'4 r \grace si'8 la' sol' la' si' |
    do''4 mi' \grace sol'8 fa' mi' fa' sol' |
    mi'2 \grace si'8 la' sol' la' si' |
    do''4 do'' \grace do''8 si' la' si' <si' re'> |
    <mi' do''>2 \grace fa'8 mi' re' mi' fa' |
    sol'2 la'4 fa' |
    <mi' sol>2 q4 q |
    <la fa'>4 la'8 la' la4 la'8 la' |
    re'4
  }
>> <sol re' si' sol''>4 q sol'16\p la' si' dod'' |
<<
  \tag #'violino1 {
    re''4 re''8 re'' re''4 mi''8 do'' |
    si'2. \grace si'8 la' sol'16 la' |
    sol'4 r8 re''\f mi''4 fa'' |
    sol''4. fa''8 mi''4 re'' |
    mi'' <sol'' do'' mi'>4 <mi' do'' mi''> mi''\p |
    re''4 re''8 re'' re''4 mi''8 do'' |
    si'2. \grace si'8 la' sol'16 la' |
    sol'4. re''8\f mi''4 fa'' |
    sol'' mi''8 fa'' sol''4 la''8 fad'' |
    sol''4 mi'' la'' fad'' |
    sol''8
  }
  \tag #'violino2 {
    la'4 la'2 la'4 |
    re' re'2 re'4 |
    re' r8 si'-\sug\f do''4 re'' |
    mi''4. re''8 do''4 si' |
    do'' <mi'' mi' sol>4 <sol mi' do''> sol'-\sug\p |
    la'4 la'2 la'4 |
    re'2. re'4 |
    si4 r8 re'\f mi'4 fa' |
    sol' mi'8 fa' sol'4 la'8 fad' |
    sol'4 mi' la' fad' |
    sol'8
  }
>> <mi'' do'''>8 q q q2:8 |
<re'' do'''>2:8 <re'' si''>:8 |
<do'' do'''>2 <<
  \tag #'violino1 {
    \grace si''8 la'' sol'' la'' si'' |
    do'''4 mi'' \grace sol''8 fa'' mi'' fa'' sol'' |
    mi''2 \grace si''8 la'' sol'' la'' si'' |
    do'''4 do'' \grace mi''8 re'' do'' re'' sol'' |
    do''2 r4 do''8\p re'' |
    mi'' mi'' mi' mi'' mi' mi'' mi' mi'' |
    re''8 re'' re' re'' sol'' re'' si' sol' |
    fad'4 fad'16 sol' la' si' do''4 do''8 re'' |
    si'2
  }
  \tag #'violino2 {
    \grace si'8 la' sol' la' si' |
    do''4 mi' \grace sol'8 fa' mi' fa' sol' |
    mi'2 \grace si'8 la' sol' la' si' |
    do''4 do'' \grace do''8 si' la' si' si' |
    do''2 r4 sol'8-\sug\p sol' |
    sol sol' sol sol' sol sol' sol sol' |
    sol sol' sol sol' sol sol' sol sol' |
    do' re' do' re' la re' la re' |
    re'2
  }
>> sol'8(\f la' si' do'') |
re''4. re''8 re''4 re'' |
<<
  \tag #'violino1 {
    re''2 \once\slurDashed sol''4.(\p la''16 si'') |
    la''4 fad''8. re''16 \once\slurDashed sol''4.( la''16 si'') |
    la''4. si''16 dod''' re'''4
  }
  \tag #'violino2 {
    re'8 fad' la' fad' re'\p sol' si' sol' |
    re' fad' la' fad' re' sol' si' sol' |
    re' fad' la' fad' re'4
  }
>> <re' la' fad''>4\f |
<re' si' sol''>2 si'4. dod''8 |
re''4 re'8. re'16 re'4 re' |
sol2. <<
  \tag #'violino1 {
    re''4\p |
    mi''( re'' \grace re''8 do'' si' do'' re'') |
    si'2 si'4 re'' |
    mi''4( re'' \grace re''8 do'' si' do'' re'') |
    si'2
  }
  \tag #'violino2 {
    si4-\sug\p |
    do'( re' mi' fad') |
    sol'2( re'4 si) |
    do'4( re' mi' fad') |
    sol'2
  }
>> la'8(\f si' do'' re'') |
mi''4. mi''8 mi''4 mi'' |
<<
  \tag #'violino1 {
    mi''2 la''4.(\p si''16 do''') |
    si''4 sold''8. mi''16 la''4.( si''16 do''') |
    si''2
  }
  \tag #'violino2 {
    mi'8 sold' si' sold' mi'\p la' do'' la' |
    mi' sold' si' sold' mi' la' do'' la' |
    mi'8 sold' si' mi''
  }
>> sold'4.\f sold'8 |
la'2 fa''!4. red''8 |
mi''4 mi'8. mi'16 mi'4 mi' |
la2 <<
  \tag #'violino1 {
    \grace re''8 do''\ff si' do'' re'' |
    mi''2 fa''4 re'' |
    do''2 sol'4 do'' |
    la'4 si'8 do'' re''4 mi''8 fa'' |
    si'4
  }
  \tag #'violino2 {
    \grace fa'8 mi'-\sug\ff re' mi' fa' |
    sol'2 la'4 fa' |
    <mi' sol>2 <sol mi'>4 mi' |
    fa'4 la'8 la' la'4 la'8 la' |
    re'4
  }
>> <sol re' si' sol''>4 q sol'16\p la' si' do'' |
<<
  \tag #'violino1 {
    re''4 re''8 re'' re''4 mi''8 do'' |
    si'2. \grace si'8 la' sol'16 la' |
    sol'4 r8 re''\f mi''4 fa''! |
    sol''4. fa''8 mi''4 re'' |
    mi'' <sol'' do'' mi'> <sol mi' do'' mi''> mi''\p |
    re''4 re''8 re'' re''4 mi''8 do'' |
    si'2. \grace si'8 la' sol'16 la' |
    sol'4. re''8\f mi''4 fa''! |
    sol'' mi''8 fa'' sol''4 la''8 fad'' |
    sol''4 mi'' la'' fad'' |
    sol''8
  }
  \tag #'violino2 {
    la'4 la'2 la'4 |
    re' re'2 re'4 |
    si r8 si'-\sug\f do''4 re'' |
    mi''4. re''8 do''4 si' |
    do'' <mi'' do'' mi' sol> <sol mi' do''> sol'-\sug\p |
    la' la'2 la'4 |
    re'2. re'4 |
    si4 r8 re'-\sug\f mi'4 fa'! |
    sol'4 mi'8 fa' sol'4 la'8 fad' |
    sol'4 mi' la' fad' |
    sol'8
  }
>> <mi'' do'''>8 q q q2:8 |
<re'' do'''>2:8 <re'' si''>:8 |
<do'' do'''>2 <<
  \tag #'violino1 {
    \grace si''8 la'' sol'' la'' si'' |
    do'''4 mi'' \grace sol''8 fa'' mi'' fa'' sol'' |
    mi''2 \grace si''8 la'' sol'' la'' si'' |
    do'''4 mi'' \grace mi''8 re'' do'' re'' sol'' |
    do''2
  }
  \tag #'violino2 {
    \grace si'8 la' sol' la' si' |
    do''4 mi' \grace sol'8 fa' mi' fa' sol' |
    mi'2 \grace si'8 la' sol' la' si' |
    do''4 do'' \grace do''8 si' la' si' si' |
    <do'' mi'>2
  }
>> r2 |
r^\fermata r4\fermata r8 do'\ff |
do'2.\fermata

