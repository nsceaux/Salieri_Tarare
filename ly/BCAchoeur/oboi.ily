\clef "treble" r2 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { \grace re''8 do'' si' do'' re'' |
    mi''2 fa''4 re'' |
    do''2 sol'4 do'' |
    la' si'8 do'' re''4 mi''8 fa'' |
    si'4 }
  { \grace fa'8 mi' re' mi' fa' |
    sol'2 la'4 fa' |
    mi'2 mi'4 mi' |
    fa'4. la'8 la'4 fa' |
    re' }
>> r \twoVoices #'(oboe1 oboe2 oboi) <<
  { \grace si''8 la'' sol'' la'' si'' |
    do'''4 mi'' \grace sol''8 fa'' mi'' fa'' sol'' |
    mi''2 \grace si''8 la'' sol'' la'' si'' |
    do'''4 do'' \grace mi''8 re'' do'' re'' sol'' |
    do''2 \grace re''8 do'' si' do'' re'' |
    mi''2 fa''4 re'' |
    do''2 sol'4 do'' |
    la'4 si'8 do'' re''4 mi''8 fa'' |
    si'4 sol'' sol'' }
  { \grace si'8 la' sol' la' si' |
    do''4 mi' \grace sol'8 fa' mi' fa' sol' |
    mi'2 \grace si'8 la' sol' la' si' |
    do''4 do'' \grace do''8 si' la' si' re' |
    mi'2 \grace fa'8 mi' re' mi' fa' |
    sol'2 la'4 fa' |
    mi'2 mi'4 sol' |
    fa' la'8 la' la4 la'8 la' |
    si'4 si' si' }
>> r4 |
R1*2 |
r4 r8 <>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8 mi''4 fa'' |
    sol''4. fa''8 mi''4 re'' |
    mi'' sol'' mi'' }
  { si'8 do''4 re'' |
    mi''4. re''8 do''4 si' |
    do'' mi'' do'' }
>> r4 |
R1*2 |
r4 r8 re''8-\sug\f mi''4 fa'' |
sol'' mi''8 fa'' sol''4 la''8 fad'' |
sol''4 mi'' la'' fad'' |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''8 do'''8 do''' do''' do'''4 do''' |
    do'''2 si'' |
    do''' \grace si''8 la'' sol'' la'' si'' |
    do'''4 mi'' fa''8 mi'' fa'' sol'' |
    mi''2 \grace si''8 la'' sol'' la'' si'' |
    do'''4 do'' \grace mi''8 re'' do'' re'' sol'' |
    do''2 }
  { sol''8 mi''8 mi'' mi'' mi''4 mi'' |
    re''1 |
    do''2 \grace si'8 la' sol' la' si' |
    do''4 mi' fa'8 mi' fa' sol' |
    mi'2 \grace si'8 la' sol' la' si' |
    do''4 do'' \grace do''8 si' la' si' si' |
    do''2 }
>> r2 |
<<
  \tag #'(oboe1 oboi) {
    R1*5 | \allowPageTurn
    r2 <>^\markup\right-align\italic solo sol''4.(\p la''16 si'') |
    la''4 fad''8. re''16 sol''4.( la''16 si'') |
    la''4. si''16 dod''' re'''4
  }
  \tag #'oboe2 { R1*7 | r2 r4 }
>>
<>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''4 | 
    sol''2 si'4. dod''8 |
    re''4 re'8. re'16 re'4 re' |
    sol'2 }
  { la'4 |
    si'2 si'4. dod''8 |
    re''4 re'8. re'16 re'4 re' |
    sol'2 }
>> r2 |
<<
  \tag #'(oboe1 oboi) {
    R1*5 |
    r2 la''4.(-\sug\p si''16 do''') |
    si''4 sold''8. mi''16 la''4.( si''16 do''') |
    si''2 r |
    R1*2 |
  }
  \tag #'oboe2 { R1*10 }
>>
r2 <>-\sug\ff \twoVoices #'(oboe1 oboe2 oboi) <<
  { \grace re''8 do'' si' do'' re'' |
    mi''2 fa''4 re'' |
    do''2 sol'4 do'' |
    la'4 si'8 do'' re''4 mi''8 fa'' |
    si'4 sol'' sol'' }
  { \grace fa'8 mi' re' mi' fa' |
    sol'2 la'4 fa' |
    mi'2 mi'4 mi' |
    fa'4 la'8 la' la'4 la'8 la' |
    si'4 si' si' }
>> r4 |
R1*2 |
r4 r8 <>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8 mi''4 fa'' |
    sol''4. fa''8 mi''4 re'' |
    mi'' sol'' mi'' }
  { si'8 do''4 re'' |
    mi''4. re''8 do''4 si' |
    do'' mi'' do'' }
>> r4 |
R1*2 |
r4 r8 re''8-\sug\f mi''4 fa''! |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4 mi''8 fa'' sol''4 la''8 fad'' |
    sol''4 mi'' la'' fad'' |
    sol''8 do''' do''' do''' do'''2:8 |
    do'''2 si'' |
    do''' \grace si''8 la'' sol'' la'' si'' |
    do'''4 mi'' \grace sol''8 fa'' mi'' fa'' sol'' |
    mi''2 \grace si''8 la'' sol'' la'' si'' |
    do'''4 mi'' \grace mi''8 re'' do'' re'' sol'' |
    do''2 }
  { sol'4 mi'8 fa' sol'4 la'8 fad' |
    sol'4 mi' la' fad' |
    sol''8 mi'' mi'' mi'' mi''2:8 |
    re''1 |
    do''2 \grace si'8 la' sol' la' si' |
    do''4 mi' \grace sol'8 fa' mi' fa' sol' |
    mi'2 \grace si'8 la' sol' la' si' |
    do''4 do'' \grace do''8 si' la' si' si' |
    do''2 }
>> r2 |
r2^\fermata r4\fermata r8 do'-\sug\ff |
do'2.\fermata
