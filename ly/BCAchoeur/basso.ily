\clef "bass" do2\f r |
do8 do' do do' do do' do do' |
do do' do do' do do' do do' |
fa, fa fa, fa fa, fa fa, fa |
sol, sol sol, sol fa, fa fa, fa |
mi mi do do' la, la si, si |
do do' do do' fa, fa fa, fa |
mi sol mi do sol, sol sol, sol |
do do' do do' do do' do do' |
do do' do do' do do' do do' |
do do' do do' do do' do do' |
fa, fa fa, fa fa, fa fa, fa |
sol, sol sol, sol sol, sol\p si sol |
fad la fad la fad la fad la |
sol si re' si sol re fad re |
sol sol,\f sol sol mi sol fa re |
do16 sol, la, si, do8 fa sol sol sol, sol, |
do do' do do' do do' do\p do' |
fad la fad la fad la fad la |
sol si re' si sol re fad re |
sol4 r8 si\f do'4 re' |
mi' do'8 re' mi'4 fa'8 red' |
mi'4 do' fa' red' |
mi' mi' do' do' |
fa fa sol sol |
do8 do' do do' fa, fa fa, fa |
mi mi do do' la, la si, si |
do do' do do' fa, fa fa, fa |
mi sol mi do sol, sol sol, sol |
do do' do do' do4 r |
r4 <>^"Violoncelli" do'\p do' do' |
si si si si |
la la fad fad |
sol2 <>^"tutti" \once\slurDashed sol,8(\f la, si, do) |
re4. re8 re4 re |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    re2 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { si2 | la si | la2. }
      { sol2 | fad sol | fad2. }
      { <>\p }
    >> <>\f
  }
  \tag #'basso {
    re8 fad la fad re\p sol si sol |
    re fad la fad re sol si sol |
    re fad la fad re4\f
  }
>> re'4 |
sol2 si4. dod'8 |
re'4 re8. re16 re4 re |
<<
  \tag #'(fagotto1 fagotto2 fagotti) {
    sol,2. \clef "tenor" si4\p |
    do'4( re' mi' fad') |
    sol'2( re'4 si) |
    do'4( re' mi' fad') |
    sol'2 r |
    R1 |
    \clef "bass" r2 \twoVoices #'(fagotto1 fagotto2 fagotti) <<
      { do'2 | si do' | si }
      { la2 | sold la | sold }
      { <>\p }
    >> r2 |
    R1*3 |
  }
  \tag #'basso {
    sol,8 sol sol, sol sol,\p sol sol, sol |
    sol, sol sol, sol sol, sol sol, sol |
    sol, sol sol, sol sol, sol sol, sol |
    sol, sol sol, sol sol, sol sol, sol |
    sol,2 la,8\f si, do re |
    mi4. mi8 mi4 mi |
    mi8 sold si sold mi-\sug\p la do' la |
    mi sold si sold mi la do' la |
    mi2 sold4.\f sold8 |
    la2 fa!4. red8 |
    mi4 mi8. mi16 mi4 mi |
    la,2 r |
  }
>>
do8\ff do' do do' do do' do do' |
do do' do do' do do' do do' |
fa, fa fa, fa fa, fa fa, fa |
sol, sol sol, sol sol, sol\p si sol |
fad la fad la fad la fad la |
sol si re' si sol re fad re |
sol sol,\f sol sol mi sol fa re |
do16 sol, la, si, do8 fa sol sol sol, sol, |
do do' do do' do do' do\p do' |
fad la fad la fad la fad la |
sol si re' si sol re fad re |
sol4 r8 si\f do'4 re' |
mi'4 do'8 re' mi'4 fa'8 red' |
mi'4 do' fa' red' |
mi' mi' do' do' |
fa fa sol sol |
do8 do' do do' fa, fa fa, fa |
mi mi do do' la, la si, si |
do do' do do' fa, fa fa, fa |
mi sol mi do sol, sol sol, sol |
do2 r |
r r4\fermata r8 do-\sug\ff |
do2.\fermata
