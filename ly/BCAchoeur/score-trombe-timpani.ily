\score {
  <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { Trompettes in Ut }
    } <<
      \new Staff << \global \keepWithTag #'tromba1 \includeNotes "trombe" >>
      \new Staff << \global \keepWithTag #'tromba2 \includeNotes "trombe" >>
    >>
    \new Staff \with { instrumentName = "Timbales" } <<
      \global \includeNotes "timpani"
    >>
  >>
  \layout { indent = \largeindent }
}
