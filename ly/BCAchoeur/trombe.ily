\clef "treble"
\twoVoices #'(tromba1 tromba2 trombe) <<
  { r2 do''8-\sug\f sol' do'' re'' |
    mi''2 fa''4 re'' |
    do''2 sol'4 r | }
  { R1*3 }
>>
R1*2 |
r4 \tag #'trombe <>^"[a 2]" do''8. do''16 do''4 re'' |
sol'2 r |
r4 do'' sol' sol' |
do'1 |
R1*3 |
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re'' re'' }
  { sol' sol' }
>> r4 |
R1*2 |
r8 r4 <>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8 mi''4 fa'' |
    sol''4. fa''8 mi''4 re'' |
    mi'' sol'' mi'' }
  { sol'8 do''4 re'' |
    mi''4. re''8 do''4 sol' |
    do'' mi'' do'' }
>> r4 |
R1*5 |
r8 \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''8 mi'' mi'' mi''2:8 | re''2 re'' | do'' }
  { do''8 do'' do'' do''2:8 | do''2 sol' | do' }
>> r2 |
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''8. do''16 do''4 re'' | sol'2 }
  { do''8. do''16 do''4 re'' | sol'2 }
>> r2 |
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 sol' sol' | do'2 }
  { do''4 sol' sol' | do'2 }
>> r2 |
r4 <>-\sug\p \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''4 mi'' mi'' | }
  { sol'4 sol' sol' | }
>>
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re'' re'' }
  { sol' sol' }
>> r4 |
R1*5 | \allowPageTurn
r2 <>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''4 re'' | sol''2 }
  { re''4 re'' | sol'2 }
>> r2 |
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8. re''16 re''4 re'' |
    sol'1~ | sol'~ | sol' | sol'~ | sol'2 }
  { re''8. re''16 re''4 re'' |
    sol1~ | sol~ | sol | sol~ | sol2 }
  { s2. | s2. s4-\sug\p }
>> r2 |
R1*6 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { r2 do''8-\sug\ff sol' do'' re'' |
    mi''2 fa''4 re'' |
    do''2 sol'4 r | }
  { R1*3 | }
>>
R1 |
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re'' re'' }
  { sol' sol' }
>> r4 |
R1*2 |
r4 r8 <>-\sug\f \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8 mi''4 fa'' |
    sol''4. fa''8 mi''4 re'' |
    mi'' sol'' mi'' }
  { sol'8 do''4 re'' |
    mi''4. re''8 do''4 sol' |
    do'' mi'' do'' }
>> r4 |
R1*5 |
r8 \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''8 mi'' mi'' mi''2:8 | re''1 | do''2 }
  { do''8 do'' do'' do''2:8 | do''2 sol' | do' }
>> r2 |
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''8. do''16 do''4 re'' | sol'2 }
  { do''8. do''16 do''4 re'' | sol'2 }
>> r |
r4 \twoVoices #'(tromba1 tromba2 trombe) <<
  { mi''4 re'' re'' | do''2 }
  { do''4 sol' sol' | mi'2 }
>> r2 |
r2^\fermata r4\fermata r8 <>\ff \twoVoices #'(tromba1 tromba2 trombe) <<
  { do'8 do'2.\fermata }
  { do'8 do'2.\fermata }
>>
