\clef "alto" do'2-\sug\f mi'8-. re'-. mi'-. fa'-. |
sol'2 la'4 fa' |
mi'2 do8 do' do do' |
fa fa' fa fa' fa fa' fa fa' |
sol sol' sol sol' fa fa' fa fa' |
mi' mi' do' do'' la la' si si' |
do' do'' do' do'' fa fa' fa fa' |
mi' sol' mi' do' sol sol' sol sol' |
do' do'' do' do'' do' do'' do' do'' |
do' do'' do' do'' do' do'' do' do'' |
do' do'' do' do'' do' do'' do' do'' |
fa fa' fa fa' fa fa' fa fa' |
sol sol' sol sol' sol sol'\p si' sol' |
fad' la' fad' la' fad' la' fad' la' |
sol si re' si sol re la re |
si4 r8 sol'\f sol'4 fa'! |
mi'4. fa'8 sol' sol' sol sol |
do' do' do do' do do' do-\sug\p do' |
fad la fad la fad la fad la |
sol si re' si sol re la re |
si4 r8 si-\sug\f do'4 re' |
mi' do'8 re' mi'4 fa'8 red' |
mi'4 do' fa' red' |
mi'8 sol' sol' sol' sol'2:8 |
la'2:8 sol':8 |
do'8 do'' do' do'' fa fa' fa fa' |
mi' mi' do' do'' la la' si si' |
do' do'' do' do'' fa fa' fa fa' |
mi' sol' mi' do' sol sol' sol sol' |
do do' do do' do4 mi'8\p si |
do'4 do' do' do' |
si si si si |
la la fad fad |
sol2 sol8(\f la si do') |
re'4. re'8 re'4 re' |
re8 fad la fad re-\sug\p sol si sol |
re fad la fad re sol si sol |
re fad la fad re4 re'\f |
sol2 si4. dod'8 |
re'4 re'8. re'16 re'4 re' |
sol2. si4\p |
do'( re' mi' fad') |
sol'2( re'4 si) |
do'( re' mi' fad') |
sol'2 la8(\f si do' re') |
mi'4. mi'8 mi'4 mi' |
mi8 sold si sold mi\p la do' la |
mi sold si sold mi la do' la |
mi sold si mi' sold4.\f sold8 |
la2 fa'!4. red'8 |
mi'4 mi8. mi16 mi4 mi |
la2 r |
do8 do' do do' do do' do do' |
do8 do' do do' do do' do do' |
fa fa' fa fa' fa fa' fa fa' |
sol sol' sol sol' sol sol\p si sol |
fad la fad la fad la fad la |
sol si re' si sol re fad re |
sol4 r8 sol'\f sol'4 fa'! |
mi'4. fa'8 sol'4 sol |
do'8 do' do do' do do' do\p do' |
fad8 la fad la fad la fad la |
sol si re' si sol re fad re |
sol4 r8 si\f do'4 re' |
mi'4 do'8 re' mi'4 fa'8 red' |
mi'4 do' fa' red' |
mi'8 sol' sol' sol' sol'2:8 |
la':8 sol':8 |
do'8 do'' do' do'' fa fa' fa fa' |
mi' mi' do' do'' la la' si si' |
do' do'' do' do'' fa fa' fa fa' |
mi' sol' mi' do' sol sol' sol sol' |
do'2 r2 |
r^\fermata r4\fermata r8 <do do'>\ff |
q2.\fermata
