\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Dans les plus beaux lieux de l’A -- si -- e,
  a -- vec la su -- prê -- me gran -- deur,
  l’a -- mour met aux pieds d’As -- ta -- si -- e,
  tout ce qui don -- ne le bon -- heur,
  l’a -- mour met aux pieds d’As -- ta -- si -- e,
  tout ce qui don -- ne le bon -- heur,
  tout ce qui don -- ne le bon -- heur.
}
\tag #'(vdessus vhaute-contre vtaille basse) {
  Ce n’est point dans l’hum -- ble re -- trai -- te,
  qu’un cœur gé -- né -- reux le res -- sent ;
  et la beau -- té la plus par -- fai -- te,
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  doit ré -- gner sur le plus puis -- sant.
}
\tag #'(vdessus vhaute-contre basse) {
  Ce n’est point dans l’hum -- ble re -- trai -- te,
  qu’un cœur gé -- né -- reux le res -- sent ;
}
\tag #'(vdessus vhaute-contre vtaille basse) {
  et la beau -- té la plus par -- fai -- te,
  doit ré -- gner sur le plus puis -- sant.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Dans les plus beaux lieux de l’A -- si -- e,
  a -- vec la su -- prê -- me gran -- deur,
  l’a -- mour met aux pieds d’As -- ta -- si -- e,
  tout ce qui don -- ne le bon -- heur,
  l’a -- mour met aux pieds d’As -- ta -- si -- e,
  tout ce qui don -- ne le bon -- heur,
  tout ce qui don -- ne le bon -- heur.
}
\tag #'(recit basse) {
  Que tout s’a -- bais -- se de -- vant el -- le.
}
