\piecePartSpecs
#`((flauti #:score-template "score-flauti")
   (oboi #:score-template "score-oboi")
   (clarinetti #:score-template "score-oboi")
   (fagotti #:tag-notes fagotti #:notes "basso")
   (trombe #:score "score-trombe-timpani")
   (timpani #:score "score-trombe-timpani")
   (violino1)
   (violino2)
   (viola)
   (basso #:tag-notes basso)
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #}))
