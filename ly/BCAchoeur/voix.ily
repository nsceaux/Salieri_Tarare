<<
  \tag #'(vdessus basse) {
    \clef "soprano/treble" R1*8 |
    r2 do''8 si' do'' re'' |
    mi''2 fa''4 re'' |
    do''2 sol'4 do'' |
    la'4 si'8 do'' re''4 mi''8 fa'' |
    si'2 r4 sol'^\p |
    re''4 re''8 re'' re''4 mi''8 do'' |
    si'2.( \grace si'8 la'4) |
    sol'4 r8 re''^\f mi''4 fa'' |
    sol''4. fa''8 mi''4 re'' |
    mi''2 r4 mi''^\p |
    re''4 re''8 re'' re''4 mi''8 do'' |
    si'2.( \grace si'8 la'4) |
    sol' r8 re'' mi''4 fa'' |
    sol'' mi''8[ fa''] sol''4 la''8[ fad''] |
    sol''4 mi'' la'' fad'' |
    sol''2 mi'' |
    re'' sol'' |
    do'' r |
    R1*3 |
    r2 r4\p do''8 re'' |
    mi''4 mi'' mi'' do''8 mi'' |
    re''2 si'4 sol' |
    fad'4 fad'8 la' do''4 do''8 re'' |
    si'2 sol'8[ la'] si' do'' |
    re''4. re''8 re''4 re'' |
    re''1~ |
    re''~ |
    re''4. re''8 fad''4 fad'' |
    sol''2 si'4. dod''8 |
    re''2. re''4 |
    sol'2 r4\p re''8 re'' |
    mi''4 re'' do''8[ si'] do'' re'' |
    si'2 si'4 re'' |
    mi''4 re''8 re'' do''[ si'] do'' re'' |
    si'2 la'8[ si'] do'' re'' |
    mi''4. mi''8 mi''4 mi'' |
    mi''1~ |
    mi''~ |
    mi''4. mi''8 sold'4. sold'8 |
    la'2 fa''!4. red''8 |
    mi''2. mi''4 |
    la'2 do''8^\f si' do'' re'' |
    mi''2 fa''4 re'' |
    do''2 sol'4 do'' |
    la'4 si'8 do'' re''4 mi''8 fa'' |
    si'2 r4\p sol' |
    re''4 re''8 re'' re''4 mi''8 do'' |
    si'2.( \grace si'8 la'4) |
    sol' r8 re''^\sug\f mi''4 fa''! |
    sol''4. fa''8 mi''4 re'' |
    mi''2 r4\p mi'' |
    re'' re''8 re'' re''4 mi''8 do'' |
    si'2.( \grace si'8 la'4) |
    sol'4 r8 re''^\sug\f mi''4 fa'' |
    sol''4 mi''8[ fa''] sol''4 la''8[ fad''] |
    sol''4 mi'' la'' fad'' |
    sol''2 mi'' |
    re'' sol'' |
    do'' r |
    R1*3 |
    \tag #'vdessus { R1*2 | r2 r4 }
  }
  \tag #'vhaute-contre {
    \clef "alto/G_8" R1*8 |
    r2 mi'8 re' mi' fa' |
    sol'2 la'4 fa' |
    mi'2 mi'4 sol' |
    la' la'8 la' la'4 la'8 la' |
    sol'2 r4 sol'^\sug\p |
    re'4 re'8 re' re'4 la8 re' |
    re'2( re') |
    re'4 r8 sol'^\sug\f sol'4 fa'! |
    mi'4. re'8 do'4 si |
    do'2 r4 sol^\sug\p |
    re'4 re'8 re' re'4 la8 re' |
    re'2( re') |
    re'4 r8 re' mi'4 fa' |
    sol' mi'8[ fa'] sol'4 la'8[ fad'] |
    sol'4 mi' la' fad' |
    sol'2 sol' |
    la'2 sol' |
    mi' r |
    R1*3 |
    r2 r4\p sol'8 sol' |
    sol'4 sol' sol' sol'8 sol' |
    sol'2 re'4 re' |
    do' do'8 do' la4 re'8 re' |
    re'2 sol8[ la] si do' |
    re'4. re'8 re'4 re' |
    re'1~ |
    re'~ |
    re'4. re'8 la'4 la' |
    si'2 si4. dod'8 |
    re'2. re'4 |
    sol2 r4\p si8 si |
    do'4 re' mi' fad'8 fad' |
    sol'2 re'4 si |
    do'4 re'8 re' mi'4 fad'8 fad' |
    sol'2 la8[ si] do' re' |
    mi'4. mi'8 mi'4 mi' |
    mi'1~ |
    mi'~ |
    mi'4. mi'8 sold4. sold8 |
    la2 fa'!4. red'8 |
    mi'2. mi'4 |
    la2 mi'8^\f re' mi' fa' |
    sol'2 la'4 fa' |
    mi'2 mi'4 sol' |
    la'4 la'8 la' la'4 la'8 la' |
    sol'2 r4-\sug\p sol' |
    re' re'8 re' re'4 la8 re' |
    re'2( re') |
    mi'4 r8 sol'^\sug\f sol'4 fa'! |
    mi'4. re'8 do'4 si |
    mi'2 r4-\sug\p sol |
    re'4 re'8 re' re'4 la8 re' |
    re'2( re') |
    re'4 r8 re'^\sug\f mi'4 fa' |
    sol'4 mi'8[ fa'] sol'4 la'8[ fad'] |
    sol'4 mi' la' fad' |
    sol'2 sol' |
    la' sol' |
    mi' r |
    R1*5 |
    r2 r4
  }
  \tag #'vtaille {
    \clef "tenor/G_8" R1*8 |
    r2 do'8 si do' re' |
    mi'2 fa'4 re' |
    do'2 sol4 mi' |
    fa' fa'8 fa' fa'4 fa'8 fa' |
    re'2 r4 si4^\sug\p |
    la4 la8 la la4 la8 la |
    sol2.( fad4) |
    sol4 r8 si^\sug\f do'4 re' |
    sol4. la8 sol4 sol |
    sol2 r4 do'^\sug\p |
    la4 la8 la la4 la8 la |
    sol2.( fad4) |
    sol4 r8 si do'4 re' |
    mi' do'8[ re'] mi'4 fa'8[ red'] |
    mi'4 do' fa' red' |
    mi'2 do' |
    do' si |
    do' r |
    R1*3 |
    r2 r4\p mi'8 si |
    do'4 do' do' do'8 do' |
    si2 si4 si |
    la la8 la fad4 fad8 fad |
    sol2 sol8[ la] si do' |
    re'4. re'8 re'4 re' |
    re'1~ |
    re'~ |
    re'4. re'8 re'4 re' |
    re'2 si4. dod'8 |
    re'2. re'4 |
    sol2 r |
    R1*3 |
    r2 la8[ si] do' re' |
    mi'4. mi'8 mi'4 mi' |
    mi'1~ |
    mi'~ |
    mi'4. mi'8 sold4. sold8 |
    la2 fa'!4. red'8 |
    mi'2. mi'4 |
    la2 do'8^\f si do' re' |
    mi'2 fa'4 re' |
    do'2 sol4 mi' |
    fa'4 fa'8 fa' fa'4 fa'8 fa' |
    re'2 r4\p si |
    la la8 la la4 la8 la |
    sol2.( fad4) |
    sol4 r8 si^\sug\f do'4 re' |
    sol4. la8 sol4 sol |
    sol2 r4\p do' |
    la4 la8 la la4 la8 la |
    sol2.( fad4) |
    sol4 r8 si^\sug\f do'4 re' |
    mi'4 do'8[ re'] mi'4 fa'8[ red'] |
    mi'4 do' fa' red' |
    mi'2 do' |
    do' si |
    do' r |
    R1*5 |
    r2 r4
  }
  \tag #'vbasse {
    \clef "bass/bass" R1*8 |
    r2 mi8 re mi fa |
    sol2 la4 si |
    do'2 do'4 do' |
    fa4 fa8 fa fa4 fa8 fa |
    sol2 r4 sol^\sug\p |
    fad4 fad8 fad fad4 fad8 fad |
    sol2.( re4) |
    si,4 r8 sol^\sug\f mi4 re |
    do4. fa8 sol4 sol |
    do2 r4 do'^\sug\p |
    fad4 fad8 fad fad4 fad8 fad |
    sol2.( re4) |
    si,4 r8 si do'4 re' |
    mi' do'8[ re'] mi'4 fa'8[ red'] |
    mi'4 do' fa' red' |
    mi'2 do' |
    fa sol |
    do' r |
    R1*11 |
    r2 re4 re |
    sol2 si4. dod'8 |
    re'2. re'4 |
    sol2 r |
    R1*10 |
    r2 mi8^\f re mi fa |
    sol2 la4 si |
    do'2 do'4 do' |
    fa4 fa8 fa fa4 fa8 fa |
    sol2 r4-\sug\p sol |
    fad fad8 fad fad4 fad8 fad |
    sol2.( re4) |
    si, r8 sol^\sug\f mi4 re |
    do4. fa8 sol4 sol |
    do2 r4-\sug\p do' |
    fad4 fad8 fad fad4 fad8 fad |
    sol2.( re4) |
    si, r8 si^\sug\f do'4 re' |
    mi'4 do'8[ re'] mi'4 fa'8[ red'] |
    mi'4 do' fa' red' |
    mi'2 do' |
    fa sol |
    do' r |
    R1*5 |
    r2 r4
  }
  \tag #'(recit basse) {
    <<
      \tag #'basse {
        s1*72 \ffclef "bass" 
      }
      \tag #'recit {
        \clef "bass" R1*72 |
      }
    >>
    r2 r8 <>^\markup\character Atar sol sol do' |
    do' sol sol sol mi mi r4\fermata |
    r2 r4
  }
>>
