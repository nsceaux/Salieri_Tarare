<<
  \tag #'(urson basse) {
    \clef "bass" fa8 fa |
    sib4. sib8 sol4 sol8. sol16 |
    fa2 r4 fa8 fa |
    la4. sib8 do'4 do'8. do'16 |
    re'4 sib r re'8. re'16 |
    re'4 sib8. la16 sol4. sib8 |
    mi2 r4 sol8. sol16 |
    do'4 do'8. do'16 do'4. do'8 |
    la4 la r r16 la la la |
    la4. la8 la4 la |
    re'2 r8 re' la8. fa16 |
    sib4. sib8 la4 la |
    re re'2 re'8 re' |
    mib'!4. do'8 la!4. mib8 |
    re4 re4 r8\fermata fa fa fa |
    sib sib sib re' sib4 r8 sib16 sib |
    sib8 fa lab lab16 sib sol4 r8 sol |
    sol sol16 sol sol8 sol16 la! si8 si r4 |
    re'8 re' re' re' si4 re'8 sol |
    mib'8 mib' r16 do' do' do' reb'8. sib!16 sib8 do' |
    lab4 r lab8 lab lab fa |
    si4 si8 do' sol4 r |
  }
  \tag #'atar {
    \clef "bass" r4 |
    R1*13 |
    <>^\markup\character Atar r16 re re mib fa8 fa r2 |
    r r16 re mib fa sib,4 |
    R1*6 |
  }
>>
