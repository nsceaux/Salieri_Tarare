\tag #'(urson basse) {
  Ne crains rien, su -- perbe Al -- ta -- mort :
  en -- tre nous la guerre est fi -- ni -- e.
  Si le droit de don -- ner la mort
  est ce -- lui d’ac -- cor -- der la vi -- e,
  je te la lais -- se de grand cœur,
  je te la lais -- se de grand cœur.
  Pleu -- re long -- temps ta per -- fi -- di -- e.
}
\tag #'atar {
  Sa per -- fi -- di -- e ?
}
\tag #'(urson basse) {
  Il s’en é -- loigne a -- vec dou -- leur
}
\tag #'atar {
  Il est ins -- truit.
}
\tag #'(urson basse) {
  I -- nu -- tile et vai -- ne fa -- veur !
  Ce -- lui dont les ar -- mes trop sû -- res,
  ne fi -- rent ja -- mais deux bles -- su -- res,
  à peine, hé -- las ! se re -- ti -- rait,
  que son ad -- ver -- saire ex -- pi -- rait.
}
