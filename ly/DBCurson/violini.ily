\clef "treble" r4 |
<<
  \tag #'violino1 {
    sib'4.\fp sib8 sib4. sib8 |
    sib4 re'8.\f mib'16 fa'4 fa'8.\p fa'16 |
    la'4.\fp sib'8 do''4 do''8. do''16 |
    re''4 sib'8.\f sib'16 sib'4 re'''8.\p re'''16 |
    re'''4\fp sib''8. la''16 sol''4. sib''8 |
    mi''8. do'16 do'8.\trill si32 do' sol8 sol'' sol''8. sol''16 |
    do'''4. do''8 do''4. do''8 |
    la'2 r8. la'16\mf la'8. la'16 |
    la'4. la'8 la'4 <la mi'? dod''>4\f |
    <re'' re'>2 r8. re''16 la'8. fa'16 |
    sib'4. sib'8 la'4 la |
    re'8\p re'' re'' re'' re''2:8\cresc |
    mib''!2\f mib'' |
    re''1\fp~ |
    re''~ |
    re''2 re''~ |
    re''1~ |
    re'' |
    do''2 sib'!\sf |
    lab'4 r r2 |
    si'2\p
  }
  \tag #'violino2 {
    fa'2-\sug\fp sol' |
    fa'4 sib8.-\sug\f do'16 re'4 r |
    mib'4.-\sug\fp( re'8) do'4 fa' |
    fa' re'8.\f re'16 re'4 sib'8.\p sib'16 |
    sib'4-\sug\fp re''8. do''16 sib'4. sib'8 |
    <sol' sib'>1 |
    sib'4 la'8. la'16 sol'4. sol'8 |
    fa'2 r8. la'16-\sug\mf sol'8. fa'16 |
    mi'4. mi'8 fa'4 sol'-\sug\f |
    fa'2 r8. re'16 re'8. re'16 |
    re'4. sol'8 fa'4 mi' |
    re'8-\sug\p <fa' la'> q q q2:8-\sug\cresc |
    q:8-\sug\f q:8 |
    fa'1-\sug\fp~ |
    fa'~ |
    fa'2 <fa' sol>~ |
    q1~ |
    q |
    <sol mib'>2 reb''-\sug\sf |
    do''4 r r2 |
    sol'2-\sug\p
  }
>> r4 <si' re' sol>4 |
