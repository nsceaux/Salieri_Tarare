\clef "bass" r4 |
sib,1\fp~ |
sib, |
fa,-\sug\fp |
sib,4 sib,8.\f sib,16 sib,4 r |
sol,2-\sug\fp~ sol,8 re sib, sol, |
do1~ |
do |
fa2 r8. fa16\mf mi8. re16 |
dod4. dod8 re4 la,\f |
sib,2 r8 r16 fa, fa,8. fa,16 |
sol,4. sol,8 la,4 la |
re8\p re re re re2:8\cresc |
do:8\f do:8 |
sib,1\fp~ |
sib,~ |
sib,2 si,~ |
si,1~ |
si, |
do2 mi\sf |
fa4 r r2 |
fa2-\sug\p r4 sol |
