\clef "treble" r4 |
\tag #'oboi <>^"a 2" sib'1-\sug\fp~ |
sib'4 r r2 |
R1 |
r2 r4 <>-\sug\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8. re''16 |
    re''4 re''8. fad''16 sol''4. fa''8 |
    mi''1 |
    mi''4 fa'' sol''2 |
    la''2 }
  { sib'8. sib'16 |
    sib'4 sib'8. la'16 sib'2 |
    sib'1 |
    sib'4 la' sol'4. mi''!8 |
    fa''2 }
  { s4 | s1-\sug\fp }
>> r2 |
R1*3 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''2 fa''4 | la''1 | sib''4 }
  { la'2 la'4 | mib''!1 | re''4 }
  { s4-\sug\p s2-\sug\cresc | <>-\sug\f }
>> r4 r2 |
R1*7 |
