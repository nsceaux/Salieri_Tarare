\clef "bass" r4 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa2 sol | fa4 re8. mib16 fa4 }
  { re2 mib | re4 sib,8. do16 re4 }
  { s1-\sug\fp | s4 s-\sug\f }
>> r4 |
R1 |
r4 <>-\sug\f \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { fa8.^\markup\tiny\bracket\line { Source : \italic sol } fa16 fa4 }
  { sib,8. sib,16 sib,4 }
>> r4 |
R1 |
r8. do'16 do'8.\trill si32 do' sol4 r |
R1*5 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la1~ | la | }
  { fa1 | mib! | }
  { s2-\sug\p s-\sug\cresc | s-\sug\f }
>>
R1*8 |
