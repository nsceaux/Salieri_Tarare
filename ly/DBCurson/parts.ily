\piecePartSpecs
#`((violino1 #:score-template "score-part-voix")
   (violino2 #:score-template "score-part-voix")
   (viola #:score-template "score-part-voix")
   (basso #:score-template "score-part-voix")
   (oboi #:score-template "score-part-voix")
   (fagotti #:score-template "score-part-voix")
   (corni #:score-template "score-part-voix"
          #:instrument , #{ \markup\center-column { Cors \small en Mi♭ } #}
          #:tag-global ())
   (tromboni #:on-the-fly-markup ,#{\markup\tacet #})
   (timpani #:on-the-fly-markup ,#{\markup\tacet#}))
