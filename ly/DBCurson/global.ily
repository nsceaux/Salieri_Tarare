\tag #'all \key sib \major \tempo "Très Majestueux" \midiTempo#100
\time 2/2 \partial 4 s4 s1*7 s2
\tempo "Plus lent" s2 s1*3
\tempo "con piu moto" s1*10 \bar "|."
