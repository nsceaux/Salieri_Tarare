\clef "treble" \transposition mib
r4 |
<>\fp \twoVoices #'(corno1 corno2 corni) <<
  { re''4. re''8 mi''4. mi''8 | re''4 }
  { sol'4. sol'8 do''4. do''8 | sol'4 }
>> r4 r2 |
R1 |
r4 <>-\sug\f \twoVoices #'(corno1 corno2 corni) <<
  { re''8. re''16 re''4 }
  { sol'8. sol'16 sol'4 }
>> r4 |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol''1 | }
  { mi'' | }
>>
R1*5 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''2 re''4 | }
  { re''2 re''4 | }
  { s4-\sug\p s2-\sug\cresc | <>-\sug\f }
>>
re''1 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 }
  { re''4 }
>> r4 r2 |
R1*7 |
