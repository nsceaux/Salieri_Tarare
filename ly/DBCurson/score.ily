\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with { \oboiInstr \consists "Metronome_mark_engraver" } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
        { \noHaraKiri s4 s1*13 s2 \revertNoHaraKiri }
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Cors \small en Mi♭ }
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
        { \noHaraKiri s4 s1*13 s2 \revertNoHaraKiri }
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
        { \noHaraKiri s4 s1*13 s2 \revertNoHaraKiri }
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff \with { \consists "Metronome_mark_engraver" } <<
          \global \keepWithTag #'violino1 \includeNotes "violini"
        >>
        \new Staff <<
          \global \keepWithTag #'violino2 \includeNotes "violini"
        >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff <<
      \new Staff \with {
        instrumentName = \markup\character Atar
        shortInstrumentName = \markup\character At.
        \haraKiriFirst
      } \withLyrics <<
        \global \keepWithTag #'atar \includeNotes "voix"
      >> \keepWithTag #'atar \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\character Urson
        shortInstrumentName = \markup\character Ur.
      } \withLyrics <<
        \global \keepWithTag #'urson \includeNotes "voix"
      >> \keepWithTag #'urson \includeLyrics "paroles"
    >>
    \new Staff \with {
      \bassoInstr
      \consists "Metronome_mark_engraver"
    } <<
      \global \includeNotes "basso"
      \origLayout {
        s4 s1*4\pageBreak
        s1*4\break s1*5\pageBreak
        s1*2 s2 \bar "" \break s2 s1*2\break
      }
    >>
  >>
  \layout {
    \context { \Score \remove "Metronome_mark_engraver" }
  }
  \midi { }
}
