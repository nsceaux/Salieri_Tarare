\clef "alto" r4 |
re'2\fp mib' |
re'4 sib8.\f sib16 sib4 r |
do'4.-\sug\fp( sib8) la2 |
sib4 fa'8.-\sug\f fa'16 fa'4 r |
sol2-\sug\fp~ sol8 re' sib sol |
r8. do'16 do'8.\trill si32 do' sol4 r |
sol'4 fa'8. fa'16 mi'4. mi'8 |
fa'2 r8. fa'16\mf mi'8. re'16 |
dod'4. dod'8 re'4 la-\sug\f |
sib2 r8. fa16 fa8. fa16 |
sol4. re'8 re'4 dod' |
re'2:8-\sug\p re':8-\sug\cresc |
do'!:8-\sug\f do':8 |
sib1-\sug\fp~ |
sib~ |
sib2 sol'~ |
sol'1~ |
sol' |
sol2 sol'-\sug\sf |
do'4 r r2 |
re'!2-\sug\p r4 sol' |
