<<
  \tag #'(vdessus basse) {
    \clef "soprano/treble" do''2.^\markup\italic chanté mi''4 |
    do'' sol' mi' sol'8 sol' |
    do''4 do' r do''8 do'' |
    la'2 do''4. do''8 |
    fa''2. la'4 |
    sol'2 r4 r8 do'' |
    do''2.^\sf do''4^\p |
    mi''4. mi''8 mi''4. mi''8 |
    fa''4 do'' r2 |
    la''2^\f fa''4 re'' |
    si'!4 si' r2 |
    mi''4.(^\p fa''8) fa''4 sol'' |
    sol''2~ sol''8[ la''] fa''[ re''] |
    do''2. re''4 |
    \appoggiatura re''8 mi''2 r |
    mi''4.( fa''8) fa''4 sol'' |
    sol''2 la'' |
    re''2. sol''4 |
    mi''2 la'' |
    re''2. re''4 |
    sol''2. sol''4 |
    do''2 r |
  }
  \tag #'vhaute-contre {
    \clef "alto/G_8" do'2. mi'4 |
    do' sol mi sol8 sol |
    do'4 do r sol'8 sol' |
    la'2 la'4. la'8 |
    la'2. do'4 |
    do'2 r4 r8 do' |
    do'2.^\sf do'4^\p |
    do'4. do'8 do'4. do'8 |
    do'4 do' r2 |
    fa'2^\f la'4 fa' |
    re'4 re' r2 |
    do'2^\p do'4 do' |
    do'2. la'8[ fa'] |
    mi'2. si4 |
    \appoggiatura si8 do'2 r |
    do'2 do'4 do' |
    do'2 la' |
    sol'2. sol'4 |
    sol'2 la' |
    sol'2. sol'4 |
    sol'2. sol'4 |
    sol'2 r |
  }
  \tag #'vtaille {
    \clef "tenor/G_8" do'2. mi'4 |
    do' sol mi sol8 sol |
    do'4 do r mi'8 mi' |
    fa'2 fa'4. fa'8 |
    fa'2. fa'4 |
    mi'2 r4 r8 sol |
    la2.^\sf la4^\p |
    sib4. sib8 sib4. sib8 |
    la4 la r2 |
    do'2^\p do'4 la |
    re'4 re' r2 |
    do'2^\p do'4 do' |
    do'2. la4 |
    sol2. sol4 |
    sol2 r |
    do'2 do'4 do' |
    sol4\melisma do'2\melismaEnd do'4 |
    si2. si4 |
    do'2 do' |
    si2. si4 |
    re'2. re'4 |
    mi'2 r |
  }
  \tag #'vbasse {
    \clef "bass/bass" do'2. mi'4 |
    do' sol mi sol8 sol |
    do'4 do r do'8 do' |
    do'2 do'4. do'8 |
    do'2. do'4 |
    do'2 r4 r8 do |
    fa2.^\sug\sf fa4^\sug\p |
    sol4. sol8 sol4. sol8 |
    fa4 fa r2 |
    fa2 fa4 fa |
    sol4 sol r2 |
    do'4.(^\sug\p la8) la4 mi |
    mi2. fa4 |
    sol2. sol4 |
    do2 r |
    do'4.( la8) la4 mi |
    mi2 fa |
    sol2. sol4 |
    do'2 fa |
    sol2. sol4 |
    si2. si4 |
    do'2 r |
  }
>>
R1*6 |
R1^\fermataMarkup |
