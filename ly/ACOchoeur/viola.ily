\clef "alto" do'2:16\f do':16 |
do':16 do':16 |
do':16 do':16 |
do':16 do':16 |
do':16 do':16 |
do':16 do':16 |
do'8 do'4-\sug\f do' do'-\sug\p do'8~ |
do' do'4 do' do' do'8 |
do'4 do'16-\sug\f fa' la' fa' do' fa' la' fa' do' fa' la' fa' |
do' fa' la' fa' do' fa' la' fa' la re' fa' re' la re' fa' re' |
re'8 sol sol' sol' sol'2:8 |
do'4.(-\sug\p la8) la4 mi |
mi2~ mi8 fa fa fa |
sol2:8 sol:8 |
sol':8-\sug\mf sol':8 |
do'4.(-\sug\p la8) la4 mi |
mi'2:8-\sug\f fa':8 |
sol':8 sol:8 |
do':8 fa':8 |
sol'4 r r2 |
sol'8 sol sol sol sol2:8 |
sol16 mi' do' mi' do' mi' do' mi' \rt#4 { do' mi' } |
\rt#4 { re' si } \rt#4 { re' si } |
<>-\sug\p\> \rt#4 { do' mi' } \rt#4 { do' mi' } |
\rt#4 { re' si } \rt#4 { re' si } |
\rt#4 { do' mi' } \rt#4 { do' mi' } |
\rt#4 { do' mi' } \rt#4 { do' mi' } |
do'4 do' do' do'\! |
do'1\fermata |
