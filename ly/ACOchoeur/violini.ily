\clef "treble"
<<
  \tag #'violino1 {
    <mi' do''>2:16\f q:16 |
    q:16 q:16 |
    q:16 q:16 |
    <fa' la'>2 do''4. do''8 |
    fa''2. la'4 |
    sol'8 \grace re''8 do''16[ si'] do''8 do'' do''2:8 |
    do''8( do''') do'''2\f la''8(\p do''') |
    \grace do'''8 sib''4 la''8( sol'' fa'' mi'' re'' do'') |
    do''4 la'-!\f do''-! fa''-! |
    la''2 \grace sol''8 fa''4 mi''8 re'' |
    si'!8 \grace la''8 sol''16 fad'' sol''8 \grace do'''8 si''16 la'' si''8 re'''16 do''' re'''8 re''' |
    mi''4.\p( fa''8) fa''4 sol'' |
    sol''2~ sol''8 la''( fa'' re'') |
    r8 do'' do'' do'' do'' do'' re'' re'' |
    re''\mf mi'' mi'' mi'' mi''2:8 |
    mi''4.(\p fa''8) fa''4 sol'' |
    sol''2:8\f la'':8 |
    re''4. mi''16 fa'' sol''8 sol'' sol'' sol'' |
    mi''2:8 la'':8 |
    re''16
  }
  \tag #'violino2 {
    <mi' sol>2:16\f q:16 |
    q:16 q:16 |
    q:16 q:16 |
    <la fa'>2:16 q:16 |
    q:16 q:16 |
    <mi' sol>:16 q:16 |
    <la fa'>8 la' la'2-\sug\f la'4-\sug\p |
    sib'8 <sib' mi''>4 q q q8 |
    <la' fa''>4 do'16-\sug\f fa' la' fa' do' fa' la' fa' do' fa' la' fa' |
    do' fa' la' fa' do' fa' la' fa' la re' fa' re' la re' fa' re' |
    re'8 si'!16 la' <si' re'>8 q q2:8 |
    do''4.(\p la'8) la'4 mi' |
    mi'2~ mi'8 fa'( la' fa') |
    r mi' mi' mi' mi' mi' si si |
    si(-\sug\mf do') do' do' do'2:8 |
    do''4.(\p la'8) la'4 mi' |
    sol'8-\sug\f do'' do'' do'' do''2:8 |
    si':8 si':8 |
    do'':8 do'':8 |
    si'16
  }
>> sol16 la si do' re' mi' fad' sol'8 la'16 si' do'' re'' mi'' fad'' |
sol''( si'' re''' si'' re''' si'' re''' si'' \rt#4 { re''' si'') } |
do'''8 <<
  \tag #'violino1 {
    do'''8[ do''' do'''] do''' si''16 la'' \grace la''8 sol'' fa''16 mi'' |
    fa''8 re'''[ re''' re'''] re''' do'''16 si'' \grace si''8 la'' sol''16 fa'' |
    mi''8\p\> do'''[ do''' do'''] do''' si''16 la'' sol''8 fa''16 mi'' |
    fa''8 re'''[ re''' re'''] re''' do'''16 si'' \grace si''8 la'' sol''16 fa'' |
    mi''4. \grace re'''8 do'''16 si'' do'''4. \grace la''8 sol''16 fad'' |
    sol''4. mi''8 sol'' mi'' sol'' mi'' |
    do''4 sol'8. la'16 sol'4 sol'\! |
    sol'1\fermata |
  }
  \tag #'violino2 {
    mi'16 sol' mi' sol' mi' sol' \rt#4 { mi' sol' } |
    \rt#4 { fa' sol' } \rt#4 { fa' sol' } |
    <>-\sug\p\> \rt#4 { mi' sol' } \rt#4 { mi' sol' } |
    \rt#4 { fa' sol' } \rt#4 { fa' sol' } |
    \rt#4 { mi' sol' } \rt#4 { mi' sol' } |
    \rt#4 { mi' sol' } \rt#4 { mi' sol' } |
    mi'4 mi'8. fa'16 mi'4 mi'\! |
    mi'1\fermata |
  }
>>
