\clef "bass" do2:8\f do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
fa:8\sf fa:8\p |
sol:8 sol:8 |
fa:8\f fa:8 |
fa:8 fa:8 |
sol:8 sol:8 |
do1\p |
do8 do[ do do] do fa fa fa |
sol4 sol sol sol, |
do do-\sug\mf do do |
do1-\sug\p |
mi2:8\f fa:8 |
sol:8 sol,:8 |
do:8 fa:8 |
sol4 r r2 |
sol8 sol, sol, sol, sol,2:8 |
do:8 do:8 |
do:8 do:8 |
do:8\p\> do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8 |
do:8 do:8\! |
do1\fermata |
