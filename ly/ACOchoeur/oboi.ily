\clef "treble" <>-\sug\f <>^"[a 2]" do''2. mi''4 |
do'' sol' mi' sol' |
do'' mi'' sol''  \twoVoices #'(oboe1 oboe2 oboi) <<
   { do'''4 | la''1 | la'' | }
   { mi''4 | fa''1 | fa'' | }
>>
r8 <>^"[a 2]" \grace re''8 do''16 si' do''8 do'' do''2:8 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''8 do''' do'''2 la''8 do''' |
    \grace do'''8 sib''4 la''8 sol'' fa'' mi'' re'' do'' |
    do''4 la'4-! do''-! fa''-! | }
  { do''8 la' la'2 la'4 |
    sib'1 |
    la'4 la'4-! do''-! fa''-! | }
  { s4 s2\f s4\p | s1 | s4 <>\f }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 \grace sol''8 fa''4 mi''8 re'' |
    do''8 si' re''2 re''4 | }
  { do''2 la'4 fa' |
    mi'8 re' si'2 si'4 | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { <>^"Solo" \once\slurDashed mi''4.( fa''8) fa''4 sol'' |
    sol''2~ sol''8 la'' fa'' re'' |
    do''2.( re''4) |
    re''4 }
  { R1*3 r4 }
>>
<>\mf \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''8. sol''16 mi''4 mi'' | }
  { sol'8. mi'16 sol'4 sol' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { <>^"Solo" mi''4.( fa''8) fa''4 sol'' | }
  { R1 }
>>
<>-\sug\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2 la'' | re''4. mi''16 fa'' sol''8 sol'' sol'' sol'' | mi''2 }
  { sol'4 do''2 do''4 | si'1 | do''2 }
>> r2 |
r8
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''16 re'' re''8 re'' re''2:8 | sol''1 | mi''8 }
  { si'16 si' si'8 si' si'2:8 | si'1 | do''8 }
>> <>^"[a 2]" do'''8[ do''' do'''] do''' si''16 la'' \grace la''8 sol'' fa''16 mi'' |
fa''8 re'''[ re''' re'''] re''' do'''16 si'' \grace si''8 la'' sol''16 fa'' |
<>-\sug\p\> \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''1 | fa'' | mi''~ | mi''~ | mi''4 }
  { do''1 | si' | do''~ | do''~ | do''4 }
>> <>\! r4 r2 |
R1^\fermataMarkup |

