\clef "bass" do'2.-\sug\f mi'4 |
do' sol mi sol |
do'1 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la1 | la | sol | la2. \clef "tenor" fa'4 | mi'1 | fa' | \clef "bass" }
  { fa1 | fa | mi1 | fa2. la4 | sib1 | la | }
  { s1*3 | s2.-\sug\sf s4-\sug\p | s1 | s-\sug\f }
>>
fa2 fa4 fa |
sol2:8 sol:8 |
do1-\sug\p |
do8 do[ do do] do fa fa fa |
sol4 sol sol sol, |
do do-\sug\mf do do |
do1-\sug\p |
mi2:8\f fa:8 |
sol:8 sol,:8 |
do:8 fa:8 |
sol4 r r2 |
sol8 sol, sol, sol, sol,2:8 |
\clef "tenor" \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { mi'1 | fa' | mi' | re' | do'4 }
  { sol1 | si | do' | sol | sol4 }
  { s1*2 | s1-\sug\p\>  s1 s4\! }
>> r4 r2 |
R1*2 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'1\fermata }
  { mi1\fermata }
>>
