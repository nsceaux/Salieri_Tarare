\clef "treble" <>\f
\twoVoices #'(tromba1 tromba2 trombe) <<
  { do''2. mi''4 | do'' sol' mi' sol' | }
  { do'2. mi'4 | do' sol' mi' sol' | }
>>
do'' do''8. do''16 do''4 do'' |
do''1 |
do'' |
do''8 do''[ do'' do''] do''2:8 |
<< { s2.-\sug\sf s4-\sug\p } do''1~ >> |
do''~ |
do'' |
do''2-\sug\f do''4 re'' |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { re''4 re''8. re''16 re''2 | }
  { sol'4 sol'8. sol'16 sol'2 | }
>>
R1*3 |
r4 <>\mf \twoVoices #'(tromba1 tromba2 trombe) <<
  { do''4 do'' do'' | }
  { do'4 do' do' | }
>>
R1 |
do''-\sug\f |
r2 r8 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''8 re'' re'' | do''2 }
  { sol'8 sol' sol' | do''2 }
>> r2 |
r8 \twoVoices #'(tromba1 tromba2 trombe) <<
  { re''16 re'' re''8 re'' re''2:8 | re''1 | mi'' | re'' | mi''4 }
  { sol'16 sol' sol'8 sol' sol'2:8 | sol'1 | do'' | sol' | do''4 }
  { s2.. s1*3 s4-\sug\p }
>> r4 r2 |
R1 |
\twoVoices #'(tromba1 tromba2 trombe) <<
  { sol'1~ | sol' | sol' | }
  { mi'1~ | mi' | mi' }
>>
R1^\fermataMarkup |
