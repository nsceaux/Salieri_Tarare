\clef "treble"
do''2. mi''4 |
do'' sol' mi' sol' |
do'' << mi'' \\ { do''8. 16 } >> <sol'' do''>4  do'''4 |
<fa'' la''>2 do''4. do''8 |
<la'' fa''>2. la'4 |
sol'8 \grace re''8 do''16[ si'] do''8 do'' do''2:8 |
do''8( <la'' do'''>) q2\f la''8(\p do''') |
\grace do'''8 sib''4 la''8( sol'' fa'' mi'' re'' do'') |
do''4 la'-!\f do''-! fa''-! |
la''2 \grace sol''8 fa''4 mi''8 re'' |
si'!8 \grace la''8 sol''16 fad'' sol''8 \grace do'''8 si''16 la'' si''8 re'''16 do''' re'''8 re''' |
mi''4.\p( fa''8) fa''4 sol'' |
<mi'' sol''>2~ q8 la''( fa'' re'') |
do'' <mi' do''> q q q q <si' re''> q |
<si' re''>\mf <do'' mi''> <sol' do'' mi''>8. q16 q2:8 |
mi''4.(\p fa''8) fa''4 sol'' |
<< sol''2:8 \\ { sol'8\f do'' do'' do'' } >> <do'' la''>2:8 |
re''4. mi''16 fa'' sol''8 <sol'' re''> q q |
<do'' mi''>2:8 <do'' la''>:8 |
re''16 sol16 la si do' re' mi' fad' sol'8 la'16 si' do'' re'' mi'' fad'' |
sol''( si'' re''' si'' re''' si'' re''' si'' \rt#4 { re''' si'') } |
do'''8 do'''8[ do''' do'''] do''' si''16 la'' \grace la''8 sol'' fa''16 mi'' |
fa''8 re'''[ re''' re'''] re''' do'''16 si'' \grace si''8 la'' sol''16 fa'' |
mi''8\p\> do'''[ do''' do'''] do''' si''16 la'' sol''8 fa''16 mi'' |
fa''8 re'''[ re''' re'''] re''' do'''16 si'' \grace si''8 la'' sol''16 fa'' |
mi''4. \grace re'''8 do'''16 si'' do'''4. \grace la''8 sol''16 fad'' |
sol''4. mi''8 sol'' mi'' sol'' mi'' |
do''4 <mi' sol'>8. <fa' la'>16 <mi' sol'>4 q\! |
q1\fermata |
