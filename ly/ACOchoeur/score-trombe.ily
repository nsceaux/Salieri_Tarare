\score {
  \new GrandStaff \with {
    instrumentName = \markup\center-column { Trompettes en Ut }
  } <<
    \new Staff <<
      \keepWithTag #'() \global
      \keepWithTag #'tromba1 \includeNotes "trombe"
    >>
    \new Staff <<
      \keepWithTag #'() \global
      \keepWithTag #'tromba2 \includeNotes "trombe"
    >>
  >>
  \layout { indent = \largeindent }
}
