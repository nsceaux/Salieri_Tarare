\clef "bass" <do mi sol>2:8 q:8 |
q:8 q:8 |
q:8 q:8 |
<do fa la>:8 q:8 |
q:8 q:8 |
<do mi sol>:8 q:8 |
<<
  { do'8 do'4 do' do' do'8~ | do' <sib do' mi'>4 q q q8 | } \\
  { fa4:8 fa:8 fa:8 fa:8 | sol:8 sol:8 sol:8 sol:8 | }
>>
fa4 do16 fa la fa do fa la fa do fa la fa |
do fa la fa do fa la fa la, re fa re la, re fa re |
<re sol,>8 sol, <sol si> q q2:8 |
<< { do'4. la8 la4 mi } \\ do1 >> |
do8 do[ do do] do fa fa fa |
sol4 sol sol sol, |
do do do do |
<< { do'4. la8 la4 mi } \\ do1 >> |
mi2:8 fa:8 |
<sol si>:8 <sol, si,>:8 |
do:8 fa:8 |
sol8 <sol, si, re>16 q q8 q q2:8 |
sol8 sol, sol, sol, sol,2:8 |
\rt#4 { do'16 mi' } \rt#4 { do'16 mi' } |
\rt#4 { re' si } \rt#4 { re' si } |
\rt#4 { do' mi' } \rt#4 { do' mi' } |
\rt#4 { re' si } \rt#4 { re' si } |
\rt#4 { do' mi' } \rt#4 { do' mi' } |
\rt#4 { do' mi' } \rt#4 { do' mi' } |
do'4 do' do' do'\! |
do1\fermata |
