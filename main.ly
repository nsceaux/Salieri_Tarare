\version "2.19.80"
\include "common.ily"

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = "Tarare"
    date = "1787"
  }
  \markup \null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #8
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}
%% Notes
%\bookpart {
%  \paper { #(define page-breaking ly:minimal-breaking) }
%  \include "notes.ily"
%}
%% Livret
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \include "livret/livret.ily"
}
%% Avis de l’éditeur
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \include "avis.ily"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PROLOGUE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 0-1
\bookpart {
  \paper { systems-per-page = 1 }
  \actn "Prologue"
  \scene "Scène Première" "Scène I"
  \sceneDescription\markup\wordwrap-center {
    La Nature et les Vents déchainés.
  }
  \pieceToc\markup\wordwrap {
    Ouverture – La Nature, chœur des Vents :
    \italic { C’est assez troubler l’univers }
  }
  \includeScore "AAAouverture"
}
%% 0-2
\bookpart {
  \scene "Scène Deuxième" "Scène II"
  \sceneDescription\markup\wordwrap-center {
    Le Génie du Feu, La Nature.
  }
  \pieceToc\markup\wordwrap {
     Le Génie du Feu, la Nature :
    \italic { De l’orbe éclatant du soleil }
  }
  \includeScore "ABAgenieNature"
}
%% 0-3
\bookpart {
  \paper { systems-per-page = 2 }
  \pieceToc\markup\wordwrap {
    La Nature : \italic { Froids humains, non encor vivants }
  }
  \includeScore "ABBnature"
}
%% 0-4
\bookpart {
  \paper { systems-per-page = 1 }
  \scene "Scène Troisième" "Scène III"
  \sceneDescription\markup\center-column {
    \wordwrap-center {
      Le Génie du Feu, la Nature, foule d’Ombres des deux sexes.
    }
    \justify {
      Une foule d’Ombres des deux sexes s’élève de toute part, vêtue
      uniformément en blanc, au bruit d’une Symphonie très douce, et
      forme des danses lentes et froides, marquant le plus grand
      étonnement de ce qu’elles sentent et voyent, puis un chœur
      à demi sort du milieu d’elles.
    }
  }
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Quel charme inconnu nous attire ? }
  }
  \includeScore "ACAchoeur"
}
\bookpart {
  %% 0-5
  \pieceToc\markup\wordwrap {
    Récit : \italic { Privés des doux liens qui donne la naissance }
  }
  \includeScore "ACBrecit"
  %% 0-6
  \pieceToc\markup\wordwrap {
    La Nature : \italic { Brillant Soleil, en vain la Nature est féconde }
  }
  \includeScore "ACCnature"
}
\bookpart {
  %% 0-7
  \pieceToc\markup\wordwrap {
    Le Génie du Feu : \italic { Gloire à l’éternelle sagesse }
  }
  \includeScore "ACDgenie"
}
\bookpart {
  \paper { systems-per-page = 3 }
  %% 0-8
  \pieceToc\markup\wordwrap {
    Récit : \italic { Un mot encor ; c’est une ombre femelle }
  }
  \includeScore "ACErecit"
}
\bookpart {
  %% 0-9
  \pieceToc\markup\wordwrap {
    La Nature : \italic { Qu'un jeune cœur, malaisément }
  }
  \includeScore "ACFnature"
}
\bookpart {
  %% 0-10
  \pieceToc\markup\wordwrap {
    Récit : \italic { Que sont ces deux superbes ombres }
  }
  \includeScore "ACGrecit"
}
\bookpart {
  %% 0-11
  \pieceToc\markup\wordwrap {
    La Nature, chœur des Ombres : \italic { Futurs mortels, prosternez-vous }
  }
  \includeScore "ACHnature"
  \includeScore "ACIchoeur"
}
\bookpart {
  %% 0-12
  \pieceToc\markup\wordwrap {
    Récit : \italic { Sois l'empereur Atar, despote de l'Asie }
  }
  \includeScore "ACJrecit"
}
\bookpart {
  %% 0-13
  \pieceToc\markup\wordwrap {
    La Nature : \italic { Enfants, embrassez-vous, égaux par la nature }
  }
  \includeScore "ACKnature"
}
\bookpart {
  %% 0-14
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Ô bienfaisante déité }
  }
  \includeScore "ACLchoeur"
  \sceneDescription\markup\wordwrap-center {
    L’Ombre seule d’Atar ne chante point, et s’éloigne avec hauteur.
    Le Génie le fait remarquer à la Nature.
  }
}
\bookpart {
  %% 0-15
  \pieceToc\markup\wordwrap {
    Récit : \italic { C'est assez. Éteignons en eux }
  }
  \includeScore "ACMrecit"
  %% 0-16
  \pieceToc\markup\wordwrap {
    La Nature : \italic { Tels qu'une vapeur élancée }
  }
  \includeScore "ACNnature"
}
\bookpart {
  %% 0-17
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Gloire à l'éternelle sagesse }
  }
  \includeScore "ACOchoeur"
  \actEnd Fin du Prologue
  \sceneDescription\markup\wordwrap-center {
    Le Premier Acte de l’Opéra se commence après le Prologue sans aucun intervalle.
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ACTE 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Premier"
  \sceneDescription\markup\override#'(line-width . 120) \center-column {
    \wordwrap-center {
      Nouvelle ouverture d’un genre absolument différent de la première.
    }
    \wordwrap-center {
      Les nuages qui couvrent le Théâtre s’élèvent ;
      on voit une salle du Palais d’Atar.
    }
  }
  \scene "Scène Première" "Scène I"
  \sceneDescription\markup\wordwrap-center {
    Atar et Calpigi.
  }
  %% 1-1
  \pieceToc "Ouverture"
  \includeScore "BAAouverture"
}
\bookpart {
  \paper { max-systems-per-page = 3 }
  %% 1-2
  \pieceToc\markup\wordwrap {
    Récit : \italic { Laisse-moi, Calpigi }
  }
  \includeScore "BABrecit"
}
\bookpart {
  \paper { min-systems-per-page = 2 }
  %% 1-3
  \pieceToc\markup\wordwrap {
    Calpigi : \italic { Il est vrai, son nom adoré. } Récit
  }
  \includeScore "BACcalpigi"
}
\bookpart {
  \scene "Scène Deuxième" "Scène II"
  \sceneDescription\markup\wordwrap-center {
    Les précédents, Altamort.
  }
  %% 1-4
  \pieceToc\markup\wordwrap {
    Recit : \italic { Mais qu’annonce Altamort à mon impatience }
  }
  \includeScore "BBArecit"
}
\bookpart {
  \scene "Scène Troisième" "Scène III"
  \sceneDescription\markup\justify {
    Tous les Acteurs précédents, Spinette, Odalisques, esclaves du sérail
    des deux sexes. Tous le Sérail entre et se range en haie ; quatre esclaves
    noirs portent Astasie couverte d’un grand voile noir, de la tête aux pieds
    (on la dépose au milieu de la salle.)
  }
  %% 1-5
  \pieceToc\markup\wordwrap {
    Chœur d’esclaves du sérail : \italic {
      Dans les plus beaux lieux de l'Asie
    }
  }
  \sceneDescription\markup\wordwrap-center { On danse pendant le Chœur. }
  \includeScore "BCAchoeur"
}
\bookpart {
  \paper { min-systems-per-page = 3 }
  %% 1-6
  \pieceToc\markup\wordwrap {
    Récit : \italic { O sort affreux, dont l’horreur me poursuit }
  }
  \includeScore "BCBrecit"
}
\bookpart {
  \paper { max-systems-per-page = 2 }
  %% 1-7
  \pieceToc\markup\wordwrap {
    Atar : \italic { Je suis heureux, vous êtes ranimée. } Récit
  }
  \includeScore "BCCatar"
}
\bookpart {
  \scene "Scène Quatrième" "Scène IV"
  \sceneDescription\markup\wordwrap-center {
    Atar, Calpigi, Altamort, Spinette.
  }
  %% 1-8
  \pieceToc\markup\wordwrap {
    Récit : \italic {
      Qui voulez-vous, Seigneur, auprès d’elle qu’on mette ?
    }
  }
  \includeScore "BDArecit"
}
\bookpart {
  %% 1-9
  \pieceToc\markup\wordwrap {
    Spinette : \italic { Oui, Seigneur, je veux la réduire. } Récit
  }
  \includeScore "BDBspinette"
}
\bookpart {
  \paper { min-systems-per-page = 3 }
  \scene "Scène Cinquième" "Scène V"
  \sceneDescription\markup\wordwrap-center {
    Urson, Atar, Altamort, esclaves.
  }
  %% 1-10
  \pieceToc\markup\wordwrap {
    Récit : \italic {
      Seigneur, c'est ce guerrier, du peuple la merveille…
    }
  }
  \includeScore "BEArecit"
}

\bookpart {
  \scene "Scène Sixième" "Scène VI"
  \sceneDescription\markup\wordwrap-center {
    Tarare, Altamort, Atar, esclaves.
  }
  %% 1-11
  \pieceToc\markup\wordwrap {
    Atar, Tarare : \italic { Que me veux-tu, brave soldat }
  }
  \includeScore "BFArecit"
}
\bookpart {
  %% 1-12
  \pieceToc\markup\wordwrap {
    Atar : \italic { Reçois en pur don ce palais. } Récit
  }
  \includeScore "BFBatar"
}
\bookpart {
  %% 1-13
  \pieceToc\markup\wordwrap {
    Tarare : \italic { Astasie est une déesse. }
  }
  \includeScore "BFCtarare"
}
\bookpart {
  %% 1-14
  \pieceToc\markup\wordwrap {
    Récit : \italic { Quoi, soldat ! pleurer une femme }
  }
  \includeScore "BFDrecit"
}
\bookpart {
  %% 1-15
  \pieceToc\markup\wordwrap {
    Atar : \italic { Qu’as-tu donc fait de ton mâle courage ? }
  }
  \includeScore "BFEatar"
}
\bookpart {
  %% 1-16
  \pieceToc\markup\wordwrap {
    Tarare : \italic { Seigneur, si j'ai sauvé ta vie }
  }
  \includeScore "BFFtarare"
}

\bookpart {
  \scene "Scène Septième" "Scène VII"
  \sceneDescription\markup\wordwrap-center {
    Calpigi, Atar, Altamort, Tarare.
  }
  %% 1-17
  \pieceToc\markup\wordwrap {
    Récit : \italic { Que veux-tu, Calpigi ? }
  }
  \includeScore "BGArecit"
}
\bookpart {
  %% 1-18
  \pieceToc\markup\wordwrap {
    Tarare : \italic { Charmante Irza, qu’est-ce donc qui t’arrête ? }
  }
  \includeScore "BGBtarare"
}
\bookpart {
  %% 1-19
  \pieceToc\markup\wordwrap {
    Atar, Tarare, Altamort, Calpigi :
    \italic { Brave Altamort, avant le point du jour }
  }
  \includeScore "BGCatar"
}
\bookpart {
  \paper { page-count = 7 }
  \scene "Scène Huitième" "Scène VIII"
  \sceneDescription\markup\wordwrap-center { Atar seul. }
  %% 1-20
  \pieceToc\markup\wordwrap {
    Atar : \italic { Vertu farouche et fière }
  }
  \includeScore "BHAatar"
  \actEnd\markup { Fin du \concat { 1 \super er } Acte }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ACTE 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \paper { max-systems-per-page = 3 }
  \act "Acte Deuxième"
  \scene "Scène Première" "Scène I"
  \sceneDescription\markup\wordwrap-center {
    Atar, Urson, esclaves, gardes, suite nombreuse.
  }
  %% 2-1
  \pieceToc\markup\wordwrap {
    Récit : \italic { Seigneur, le grand-prêtre Arthénée }
  }
  \includeScore "CAArecit"

  \scene "Scène Deuxième" "Scène II"
  \sceneDescription\markup\wordwrap-center {
    Arthénée, Atar.
  }
  %% 2-2
  \pieceToc\markup\wordwrap {
    Récit : \italic { Les sauvages d'un autre monde }
  }
  \includeScore "CBArecit"
}
\bookpart {
  %% 2-3
  \pieceToc\markup\wordwrap {
    Arthénée : \italic { Qu'il faut combattre }
  }
  \includeScore "CBBarthenee"
}
\bookpart {
  %% 2-4
  \pieceToc\markup\wordwrap {
    Récit : \italic { Apprends-moi donc, ô chef des Brames ! }
  }
  \includeScore "CBCrecit"
}
\bookpart {
  \paper { max-systems-per-page = 3 }
  %% 2-5
  \pieceToc\markup\wordwrap {
    Arthénée : \italic { Ah ! d'une antique absurdité. } Récit
  }
  \includeScore "CBDarthenee"
  %% 2-6
  \pieceToc\markup\wordwrap {
    Arthénée : \italic { Crains de payer de ta couronne. } Récit
  }
  \includeScore "CBEarthenee"
}
\bookpart {
  %% 2-7
  \pieceToc\markup\wordwrap {
    Atar : \italic { Apprends-moi donc, ô chef des Brames ! }
  }
  \includeScore "CBFatar"
}
\bookpart {
  \scene "Scène Troisième" "Scène III"
  \sceneDescription\markup\wordwrap-center { Athénée seul. }
  %% 2-8
  \pieceToc\markup\wordwrap {
    Arthénée : \italic { Ô politique consommée }
  }
  \includeScore "CCAarthenee"
}
\bookpart {
  \scene "Scène Quatrième" "Scène IV"
  \sceneDescription\markup\wordwrap-center { Tarare seul. }
  %% 2-9
  \pieceToc\markup\wordwrap {
    Tarare : \italic { De quel nouveau malheur suis-je encor menacé }
  }
  \includeScore "CDAtarare"
}
\bookpart {
  \scene "Scène Cinquième" "Scène V"
  \sceneDescription\markup\wordwrap-center {
    Calpigi déguisé, couvert d’une cape, et Tarare.
  }
  %% 2-10
  \pieceToc\markup\wordwrap {
    Récit : \italic { Tarare ! connais-moi }
  }
  \includeScore "CEArecit"
}
\bookpart {
  \includeScore "CEBcalpigi"
}

\bookpart {
  \scene "Scène Sixième" "Scène VI"
  \sceneDescription\markup\wordwrap-center {
    Tarare seul.
  }
  %% 2-11
  \pieceToc\markup\wordwrap {
    Tarare : \italic { J’irai, oui, j’oserai }
  }
  \includeScore "CFAtarare"
}

\bookpart {
  \scene "Scène Septième" "Scène VII"
  \sceneDescription\markup\column {
    \justify {
      Le fond du théâtre qui représentait le portail du temple de Brama,
      se retire, et laisse voir l’intérieur du temple, qui se forme
      jusqu’au-devant du théâtre.
    }
    \wordwrap-center {
      Arthénée, les Prêtres de Brama, Elamir et les autres enfants des augures.
    }
  }
  %% 2-12
  \pieceToc\markup\wordwrap {
    Récit : \italic { Sur un choix important le ciel est consulté }
  }
  \includeScore "CGArecit"
}
\bookpart {
  %% 2-13
  \pieceToc\markup\wordwrap {
    Arthénée : \italic { Ainsi qu'une abeille }
  }
  \includeScore "CGBarthenee"
}
\bookpart {
  %% 2-14
  \pieceToc\markup\wordwrap {
    Récit : \italic { Tout le peuple, mon fils, sous nos voûtes arrive }
  }
  \includeScore "CGCrecit"
}

\bookpart {
  \scene "Scène Huitième" "Scène VIII"
  \sceneDescription\markup\wordwrap-center {
    Atar, Altamort, Tarare, Urson, Arthénée, Elamir,
    Visirs, Emirs, Suite, Peuple, Soldats, Esclaves.
  }
  %% 2-15
  \pieceToc "Marche"
  \includeScore "CHAmarche"
}
\bookpart {
  %% 2-16
  \pieceToc\markup\wordwrap {
    Arthénée, chœur  : \italic { Prêtres du grand Brama ! Roi du Golfe Persique ! }
  }
  \includeScore "CHBarthenee"
}
\bookpart {
  %% 2-17
  \pieceToc\markup\wordwrap {
    Tarare : \italic { Qui veut la gloire }
  }
  \includeScore "CHCtarare"
}
\bookpart {
  %% 2-18
  \pieceToc\markup\wordwrap {
    Récit : \italic { Je ne puis soutenir la clameur importune }
  }
  \includeScore "CHDrecit"
}
\bookpart {
  %% 2-19
  \pieceToc\markup\wordwrap {
    Tarare : \italic { Apprends, fils orgueilleux des prêtres }
  }
  \includeScore "CHEtarare"
}
\bookpart {
  %% 2-20
  \pieceToc\markup\wordwrap {
    Récit : \italic { Sans le respect d’Atar, vil objet de ma haine }
  }
  \includeScore "CHFrecit"
}
\bookpart {
  %% 2-21
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Brama ! si la vertu t’es chère }
  }
  \includeScore "CHGchoeur"
  \actEnd\markup { Fin du \concat { 2 \super e } Acte }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ACTE 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \paper { max-systems-per-page = 3 }
  \act "Acte Troisième"
  \sceneDescription\markup\wordwrap-center {
    Le théâtre représente les jardins du sérail ; l’appartament d’Irza est à droite ;
    à gauche, et sur le devant, est un grand sopha sous un dais superbe,
    au milieu d’un parterre illuminé. Il est nuit.
  }
  \scene "Scène Première" "Scène I"
  \sceneDescription\markup\wordwrap-center {
    Calpigi, entre d’un coté ; Atar, Urson entrent de l’autre ;
    des Jardiniers ou Bostangis qui allument.
  }
  %% 3-1
  \pieceToc\markup\wordwrap {
    Récit : \italic { Les jardins éclairés ! des bostangis ! pourquoi ? }
  }
  \includeScore "DAArecit"

  \scene "Scène Deuxième" "Scène II"
  \sceneDescription\markup\wordwrap-center { Atar, Urson. }
  %% 3-2
  \pieceToc\markup\wordwrap {
    Récit : \italic { Avant que ma fête commence }
  }
  \includeScore "DBArecit"
}
\bookpart {
  %% 3-3
  \pieceToc\markup\wordwrap {
    Urson : \italic { Tarare seul arrive au rendez-vous }
  }
  \includeScore "DBBurson"
}
\bookpart {
  %% 3-4
  \pieceToc\markup\wordwrap {
    Urson : \italic { Ne crains rien, superbe Altamort }
  }
  \includeScore "DBCurson"
}
\bookpart {
  %% 3-5
  \pieceToc\markup\wordwrap {
    Atar : \italic { Partout il a donc l’avantage }
  }
  \includeScore "DBDatar"
}
\bookpart {
  \scene "Scène Troisième" "Scène III"
  \sceneDescription\markup\justify {
    Atar, Astasie en habit de sultane, soutenue par des esclaves, son
    mouchoir sur les yeux ; Spinette, Calpigi, Eunuques, Esclaves de
    deux sexes. (Atar fait asseoir Astasie sur le grand sopha, près de
    lui.)
  }
  %% 3-6
  \pieceToc ""
  \includeScore "DCAritournelle"
  %% 3-7
  \pieceToc\markup\wordwrap {
    Récit : \italic { Calpigi, quel spectacle ai-je pour ma sultane ? }
  }
  \includeScore "DCBrecit"
}

\bookpart {
  \paper { max-systems-per-page = 2 }
  \scene "Scène Quatrième" "Scène IV"
  \sceneDescription\markup\column {
    \justify {
      Les acteurs précédents, Bergers européans de cour, vêtus galemment
      en habits de taffetas, avec des plumes, ainsi que leurs bergères,
      ayant des houlettes dorées.
      Paysans grossiers, vêtus à l’européane, ainsi que leurs paysannes,
      mais très simplement, tenant des instruments aratoires.
    }
    \justify {
      Marche, dont le dessus léger peint le caratère des bergers de
      cour qui la dansent, et dont la basse peint la lourde gaîté des
      paysans qui la sautent.
    }
  }
  %% 3-8
  \pieceToc "Marche"
  \includeScore "DDAmarche"
}
\bookpart {
  %% 3-9
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Peuple léger mais généreux }
  }
  \includeScore "DDBchoeur"
}
\bookpart {
  \paper { systems-per-page = 2 }
  \sceneDescription\markup\justify {
    Deux jeunes seigneur et dame de la cour commencent une danse assez
    vive ; deux jeunes berger et bergères de la campagne, commencent en
    même temps un pas assez simple. Leur danse est interrompue par une
    bergère coquette et une bergère sensible.
  }
  %% 3-10
  \pieceToc "Minuetto"
  \includeScore "DDCmenuet"
}
\bookpart {
  %% 3-11
  \pieceToc ""
  \includeScore "DDDdanse"
}
\bookpart {
  %% 3-12
  \pieceToc\markup\wordwrap {
    Duo dialogué : \italic { Galants qui courtisez les belles }
  }
  \includeScore "DDEduo"
}
\bookpart {
  \sceneDescription\markup\justify {
    De vieux seigneurs dansent vivement devant des bergères modestes, en
    leur présentant des bouquets ; des jeunes gens fatigués, appuyés sur
    leur houlettes, se meuvent à peine devant de vieilles coquettes qui
    dansent à perdre haleine. Atar se lève, et erre parmi les danseurs.
  }
  %% 3-13
  \pieceToc ""
  \includeScore "DDFdanse"
}
\bookpart {
  %% 3-14
  \pieceToc\markup\wordwrap {
    Spinette, un paysan : \italic { Dans nos vergers délicieux }
  }
  \includeScore "DDGspinette"
  \includeScore "DDHastasie"
}
\bookpart {
  \paper { max-systems-per-page = 2 }
  \sceneDescription\markup\wordwrap-center {
    On danse un grand air.
  }
  %% 3-15
  \pieceToc\markup\wordwrap {
    Air. Astasie : \italic { Ô mon Tarare, ô mon époux }
  }
  \includeScore "DDIair"
}
\bookpart {  
  %% 3-16
  \pieceToc\markup\wordwrap {
    Spinette, un paysan : \italic { Dans nos vergers délicieux }
  }
  \reIncludeScore "DDGspinette" "DDJspinette"
  %% 3-17
  \pieceToc\markup\wordwrap {
    Récit : \italic { Saluez tous la belle Irza }
  }
  \includeScore "DDKrecit"
}
\bookpart {
  %% 3-18
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Saluons tous la belle Irza }
  }
  \includeScore "DDLchoeur"
}
\bookpart {
  %% 3-19
  \pieceToc ""
  \includeScore "DDMdanse"
}
\bookpart {
  \paper { min-systems-per-page = 2 }
  %% 3-20
  \pieceToc\markup\wordwrap {
    Récit : \italic { Calpigi, ta fête est charmante }
  }
  \includeScore "DDNrecit"

  \sceneDescription\markup\justify {
    (La danse figurée cesse; tous les danseurs et danseuses se prennent
    par la main pour danser le refrain de sa chanson.)
  }
  %% 3-21
  \pieceToc\markup\wordwrap {
    Calpigi, chœur : \italic { Je suis natif de Ferrare }
  }
  \includeScore "DDOcalpigi"
}
\bookpart {
  %% 3-22
  \pieceToc\markup\wordwrap {
    Astasie, Spinette, Atar, chœur : \italic { Tarare ! }
  }
  \includeScore "DDPtous"
}
\bookpart {
  \scene "Scène Cinquième" "Scène V"
  \sceneDescription\markup\column {
    \wordwrap-center { Le théâtre est très obscur. }
    \wordwrap-center {
      Calpigi, Tarare, un poignard à la main, prêt à frapper Calpigi qu’il entraîne.
    }
  }
  %% 3-23
  \pieceToc\markup\wordwrap {
    Récit : \italic { Ô Tarare ! }
  }
  \includeScore "DEArecit"
}
\bookpart {
  %% 3-24
  \pieceToc\markup\wordwrap {
    Tarare : \italic { Au sein de la profonde mer }
  }
  \includeScore "DEBtarare"
}
\bookpart {
  %% 3-25
  \pieceToc\markup\wordwrap {
    Tarare, Calpigi : \italic { Je suis sauvé, grâce à ton cœur }
  }
  \includeScore "DECduo"
}
\bookpart {
  \paper { max-systems-per-page = 3 }
  \scene "Scène Sixième" "Scène VI"
  \sceneDescription\markup\wordwrap-center {
    Atar sort de chez Astasie. Tarare, Calpigi.
  }
  %% 3-26
  \pieceToc\markup\wordwrap {
    Atar, Calpigi : \italic { On vient ; c'est le sultan }
  }
  \includeScore "DFArecit"
}
\bookpart {
  \scene "Scène Septième" "Scène VII"
  \sceneDescription\markup\wordwrap-center {
    Tarare seul levant les mains au ciel et avec ardeur.
  }
  %% 3-27
  \pieceToc\markup\wordwrap {
    Tarare : \italic { Dieu tout-puissant ! tu ne trompas jamais }
  }
  \includeScore "DGAtarare"
  \actEnd Fin du troisième Acte
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ACTE 4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \paper {
    system-count = 21
    page-count = 11
  }
  \act "Acte Quatrième"
  \sceneDescription\markup\wordwrap-center {
    Le théâtre représente l’intérieur de l’appartement d’Astasie.
    C’est un salon superbe, garni de sophas et autres meubles orientaux.
  }
  \scene "Scène Première" "Scène I"
  \sceneDescription\markup\wordwrap-center { Astasie, Spinette. }
  %% 4-1
  \pieceToc\markup\wordwrap {
    Astasie, Spinette : \italic { Spinette, comment fuir de cette horrible enceinte ? }
  }
  \includeScore "EAArecit"
}
\bookpart {
  \paper { systems-per-page = 3 }
  %% 4-2
  \pieceToc\markup\wordwrap {
    Récit : \italic { Un grand roi vous invite à faire son bonheur }
  }
  \includeScore "EABrecit"
}
\bookpart {
  \paper { systems-per-page = 3 }
  \scene "Scène Deuxième" "Scène II"
  \sceneDescription\markup\wordwrap-center { Calpigi, Spinette, Astasie. }
  %% 4-3
  \pieceToc\markup\wordwrap {
    Calpigi, Spinette, Astasie : \italic { Belle Irza, l’empereur ordonne }
  }
  \includeScore "EBArecit"
}
\bookpart {
  \scene "Scène Troisième" "Scène III"
  \sceneDescription\markup\wordwrap-center { Astasie, Spinette. }
  %% 4-4
  \pieceToc\markup\wordwrap {
    Astasie, Spinette : \italic { Ô ma compagne ! ô mon amie ! }
  }
  \includeScore "ECArecit"
}
\bookpart {
  \paper { systems-per-page = 3 }
  %% 4-5
  \pieceToc\markup\wordwrap {
    Spinette : \italic { Je partage votre détresse }
  }
  \includeScore "ECBspinette"
}
\bookpart {
  \scene "Scène Quatrième" "Scène IV"
  \sceneDescription\markup\wordwrap-center { Spinette seule. }
  %% 4-6
  \pieceToc\markup\wordwrap {
    Récit : \italic { Spinette, allons, point de faiblesse }
  }
  \includeScore "EDArecit"
}
\bookpart {
  \scene "Scène Cinquième" "Scène V"
  \sceneDescription\markup\wordwrap-center {
    Calpigi, Tarare en muet, Spinette assise, voilée,
    son mouchoir sur les yeux.
  }
  %% 4-7
  \pieceToc\markup\wordwrap {
    Récit : \italic { Cette femme est à toi, muet }
  }
  \includeScore "EEArecit"
}
\bookpart {
  \paper { systems-per-page = 3 }
  \scene "Scène Sixième" "Scène VI"
  \sceneDescription\markup\wordwrap-center { Tarare, Spinette. }
  %% 4-8
  \pieceToc\markup\wordwrap {
    Spinette, Tarare : \italic { Comme il est laid }
  }
  \includeScore "EFArecit"
}
\bookpart {
  \paper { system-count = 6 }
  %% 4-9
  \pieceToc\markup\wordwrap {
    Tarare, Spinette : \italic { Étranger dans Ormus, hier on me vint dire }
  }
  \includeScore "EFBtarare"
}
\bookpart {
  %% 4-10
  \pieceToc\markup\wordwrap {
    Duo : \italic { Ami, ton courage m'éclaire }
  }
  \includeScore "EFCduo"
}
\bookpart {
  \scene "Scène Septième" "Scène VII"
  \sceneDescription\markup\wordwrap-center {
    Tarare démasqué, Urson, Soldats armés de massues, Calpigi, Eunuques
    entrant de l'autre côté.
  }
  %% 4-11
  \pieceToc\markup\wordwrap {
    Urson, Calpigi, chœurs : \italic { Marchez, soldats, doublez le pas }
  }
  \includeScore "EGAchoeur"
}
\bookpart {
  \paper { max-systems-per-page = 3 }
  %% 4-12
  \pieceToc\markup\wordwrap {
    Récit : \italic { Urson, expliquez-vous }
  }
  \includeScore "EGBrecit"
}

\bookpart {
  \scene "Scène Huitième" "Scène VIII"
  \sceneDescription\markup\wordwrap-center {
    Calpigi seul.
  }
  %% 4-13
  \pieceToc\markup\wordwrap {
    Récit : \italic { Sur deux têtes la foudre, et l'on m'ose nommer }
  }
  \includeScore "EHArecit"
}
\bookpart {
  %% 4-14
  \pieceToc\markup\wordwrap {
    Calpigi : \italic { Vas ! l’abus du pouvoir suprême }
  }
  \includeScore "EHBcalpigi"
  \actEnd Fin du quatrième Acte
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ACTE 5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "Acte Cinquième"
  \sceneDescription\markup\wordwrap-center {
    Le théâtre représente une cour intérieur du palais d’Atar.
    Au milieu est un bûcher ; au pied du bûcher, un billot, des chaînes,
    des haches, des massues, et autres instruments d’un supplice.
  }
  \scene "Scène Première" "Scène I"
  \sceneDescription\markup\wordwrap-center { Atar, Eunuques, Suite. }
  %% 5-1
  \pieceToc\markup\wordwrap {
    Atar : \italic { Fantôme vain ! idole populaire }
  }
  \includeScore "FAAatar"
}
\bookpart {
  \scene "Scène Deuxième" "Scène II"
  \sceneDescription\markup\wordwrap-center { Atar, Arthénée. }
  %% 5-2
  \pieceToc\markup\wordwrap {
    Arthénée, Atar : \italic { Que veux-tu, roi d'Ormus ? et quel nouveau malheur }
  }
  \includeScore "FBArecit"
}
\bookpart {
  \scene "Scène Troisième" "Scène III"
  \sceneDescription\markup\wordwrap-center {
    Atar, Tarare enchaîné, esclaves, suite.
  }
  %% 5-3
  \pieceToc\markup\wordwrap {
    Récit : \italic { Approche, malheureux ! viens subir le supplice }
  }
  \includeScore "FCArecit"
}
\bookpart {
  %% 5-4
  \pieceToc\markup\wordwrap {
    Tarare, Atar : \italic { Je ne puis mourir qu'une fois }
  }
  \includeScore "FCBtarare"
}

\bookpart {
  \scene "Scène Quatrième" "Scène IV"
  \sceneDescription\markup\wordwrap-center {
    Astasie voilée, Atar, Arthénée, Tarare, Spinette,
    Esclaves des deux sexes, Soldats.
  }
  %% 5-5
  \pieceToc\markup\wordwrap {
    Récit : \italic { Ainsi donc, abusant de vos charmes }
  }
  \includeScore "FDArecit"
}
\bookpart {
  %% 5-6
  \pieceToc\markup\wordwrap {
    Chœur, Arthénée : \italic { Avec tes décrets infinis }
  }
  \includeScore "FDBchoeur"
}
\bookpart {
  %% 5-7
  \pieceToc\markup\wordwrap {
    Récit : \italic { Ne m’impute pas, étranger }
  }
  \includeScore "FDCrecit"
}
\bookpart {
  %% 5-8
  \pieceToc\markup\wordwrap {
    Astasie, Atar : \italic { Ô tigre ! mes dédains ont trompé ton attente }
  }
  \includeScore "FDDastasie"
}
\bookpart {
  \paper {
    page-count = 4
    system-count = 8
  }
  %% 5-9
  \pieceToc\markup\wordwrap {
    Trio : \italic { Le trépas nous attend }
  }
  \includeScore "FDEtrio"
}

\bookpart {
  \scene "Scène Cinquième" "Scène V"
  \sceneDescription\markup\wordwrap-center {
    Acteurs précédents. Une foule d’esclaves des deux sexes
    accourt avec frayeur et se serre à genoux autour d’Atar.
  }
  %% 5-10
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Atar, défends-nous, sauve-nous }
  }
  \includeScore "FEAchoeur"
}
\bookpart {
  \scene "Scène Sixième" "Scène VI"
  \sceneDescription\markup\wordwrap-center {
    [Les précédents, toute la milice le sabre à la main, Calpigi, Urson.]
  }
  %% 5-11
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Tarare, Tarare, Tarare. } Récit.
  }
  \includeScore "FFAchoeur"

  \scene "Scène Septième" "Scène VII"
  \sceneDescription\markup\wordwrap-center {
    [Les acteurs précédents, excepté Atar et Urson.]
  }
  %% 5-12
  \pieceToc\markup\wordwrap {
    Calpigi, Tarare, chœur :
    \italic { Tous les torts de son règne, un seul mot les répare }
  }
  \includeScore "FGAchoeur"
}
\bookpart {
  \paper { system-count = 4 }
  %% 5-13
  \pieceToc\markup\wordwrap {
    Tarare : \italic { Le trône est pour moi sans appas }
  }
  \includeScore "FGBtarare"
}

\bookpart {
  \scene "Scène Huitième" "Scène VIII"
  \sceneDescription\markup\wordwrap-center {
    Les acteurs précédents, Urson tenant dans sa main la couronne d’Atar.
  }
  %% 5-14
  \pieceToc\markup\wordwrap {
    Urson : \italic { Non, par mes mains, le peuple entier }
  }
  \includeScore "FHArecit"
}
\bookpart {
  %% 5-15
  \pieceToc\markup\wordwrap {
    Arthénée, chœur : \italic { Tarare, il faut céder }
  }
  \includeScore "FHBchoeur"
}

\bookpart {
  \scene "Scène Neuvième" "Scène IV"
  \sceneDescription\markup\wordwrap-center {
    Tous les précédents, excepté le Grand Prêtre.
  }
  %% 5-16
  \pieceToc\markup\wordwrap {
    Tarare : \italic { Enfants, vous m’y forcez, je garderai ces fers }
  }
  \includeScore "FIArecit"
}
\bookpart {
  %% 5-17
  \pieceToc\markup\wordwrap {
    Chœur général : \italic { Quel plaisir de nos cœurs s’empare }
  }
  \includeScore "FIBchoeur"
}

\bookpart {
  \scene "Scène Dernière" "Scène IV"
  \sceneDescription\markup\wordwrap-center {
    Les précédents, la Nature et le Génie du feu sur les nuages.
  }
  %% 5-18
  \pieceToc\markup\wordwrap {
    Récit : \italic { Nature, quel exemple imposant et funeste }
  }
  \includeScore "FJArecit"
}
\bookpart {
  %% 5-19
  \pieceToc\markup\wordwrap {
    Chœur, la Nature, le Génie du feu : \italic { De ce grand bruit, de cet éclat }
  }
  \includeScore "FJBchoeur"
  \actEnd FIN
}
\bookpart {
  \scene "[Annexe]" "[Annexe]"
  \sceneDescription\markup\justify {
    Si l’on voulait finir l’Opéra pour une fête après le Chœur
    \italic { (Quel plaisir de nos cœurs s’empare) }
    [page \page-refIII#'FIBchoeur ]
    sur la dernière note de ce chœur on viendrait à cette variante.
  }
  %% 5-20
  \pieceToc\markup\wordwrap {
    Récit : \italic { Les fiers Européans marchent vers ces états }
  }
  \includeScore "FKArecit"
}
\bookpart {
  %% 5-21
  \pieceToc\markup\wordwrap {
    Urson, Calpigi, deux femmes, chœur : \italic { Roi, nous mettons la liberté }
  }
  \includeScore "FKBchoeur"
  \actEnd FIN
}
