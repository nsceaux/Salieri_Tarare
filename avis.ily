\notesSection "Avis de l’Éditeur [1790]"
\markup\vspace#15
\markuplist\italic\abs-fontsize-lines#10 \with-line-width-ratio #0.7 {
  \livretAct Avis de l’Éditeur [1790]
  \vspace#2
  \paragraph {
    Le grand succès de l’Opéra de \normal-text\smallCaps Tarare nous a
    fait relire avec plus d’attention le discours préliminaire où
    l’Auteur du Poëme a développé ses vues ; où il établit surtout le
    principe de ne point abuser de la Musique en en faisant une œuvre
    à part, et la prodiguant sans effet : mais de s’asservir au
    contraire au sens rigoureux des paroles, avec une telle sévérité
    qu’elle semble à l’auditeur n’en pouvoir être séparée.
  }

  \paragraph {
    L’heureux emploi que \concat { M \super r } Salieri a fait de
    cette théorie, a beaucoup augmenté l’intérêt que tout l’ouvrage
    inspire, en rapprochant sa Scène de la déclamation parlée, et en
    distinguant sur sa partition tous les morceaux de chant obligé,
    par le mot \normal-text Chanté, de ceux qu’il croit susceptibles
    d’une simple déclamation et qu’il désigne par celui de
    \normal-text parlé.  Il nous a fait naître l’idée que toute la
    Scène de Tarare pourrait se déclamer sans Orchestre, sur les
    Théatres où l’on a peu de moyens d’exécuter un grand Opéra ; mais
    où ce mélange adopté de la parole et du chant, que l’on nomme
    Opéra-comique a toujours du succès. Nous invitons les Directeurs
    des Théâtres de la Province à tenter cet essai, en conservant les
    grandes Ritournelles qui remplissent la Pantomime ; persuadés du
    plaisir que peut faire encore ce Spectable, bien qu’il soit dénué
    d’une partie de sa majesté.
  }
}