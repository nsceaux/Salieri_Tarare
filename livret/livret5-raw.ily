\livretAct ACTE CINQUIÈME
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\column {
  \justify {
    Le théâtre représente une cour intérieur du palais d’Atar.
    Au milieu est un bûcher ; au pied du bûcher, un billot, des chaînes,
    des haches, des massues, et autres instruments d’un supplice.
  }
  \wordwrap-center { Atar, eunuques, suite. }
}
\livretDidasP\wordwrap {
  (Atar examine avec avidité le bûcher et tous les apprêts du supplice de Tarare.)
}
\livretPers Atar
\livretRef#'FAAatar
%# Fantôme vain! idole populaire,
%# Dont le nom seul excitait ma colère,
%#10 Tarare !… enfin tu mourras cette fois!
%# Ah! pour Atar, quel bien céleste,
%# D'immoler l'objet qu'il déteste,
%# Avec le fer souple des loix!
\livretPersDidas Atar (aux Eunuques)
%#- Trouve-t-on Calpigi?
\livretPers Un Eunuque
%#= Seigneur, on suit sa trace.
\livretPers Atar
%# À qui l'arrêtera, je donnerai sa place.
\livretDidasPPage\line { (Les Eunuques sortent en courant.) }

\livretScene SCÈNE DEUXIÈME
\livretDescAtt\column {
  \wordwrap-center { Atar, Arthénée. }
  \justify {
    Deux files des prêtres le suivent ; l’une en blanc, dont le premier
    prêtre porte un drapeau blanc, où sont écrits, en lettres d’or,
    ces mots : la vie.
    L’autre file de prêtres est en noir, couverte de crêpes, dont le
    premier prêtre porte un drapeau noir, où sont écrits ces mots,
    en lettres d’argent : la mort
  }
}
\livretPersDidas Arthénée (s’avance, bien sombre)
\livretRef#'FBArecit
%# Que veux-tu, roi d'Ormus? et quel nouveau malheur
%# Te force d'arracher un père à sa douleur?
\livretPers Atar
%# Ah! si l'espoir d'une prompte vengeance
%# Peut l'adoucir, reçois-en l'assurance.
%# Dans mon sérail on a surpris
%# L'affreux meurtri=er de ton fils.
%# Je tiens la victime enchaînée,
%# Et veux que par toi-même elle soit condamnée.
%# Dis un mot, le trépas l'attend.
\livretPers Arthénée
%# Atar, c'était en l'arrêtant…
%# Sans avoir l'air de la connaître,
%# Il fallait poignarder le traître:
%# Je tremble qu'il ne soit trop tard!
%# Chaque instant, le moindre retard,
%# Sur ton bras peut fermer le piège.
\livretPers Atar
%# Quel démon, quel dieu le protège?
%# Tout me confond de cette part!
\livretPers Arthénée
%# Son démon, c'est une âme forte,
%# Un cœur sensible et généreux,
%# Que tout émeut, que rien n'emporte;
%# Un tel homme est bien dangereux!

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center {
  Atar, Arthénée, Tarare enchaîné, Soldats, Esclaves, Suite,
  Prêtres de la vie et de la mort.
}
\livretPers Atar
\livretRef#'FCArecit
%# Approche, malheureux! viens subir le supplice,
%# Qu'un crime irrémissible arrache à ma justice.
\livretPers Tarare
%# Qu'elle soit juste ou non, je demande la mort.
%# De tes plaisirs j'ai vi=olé l'asile,
%# Sans y trouver l'objet d'une audace inutile,
%# Mon Astasie !… =Ô ce fourbe Altamort!
%# Il l'a ravie =à mon séjour champêtre,
%# Sans la présenter à son maître!
%# Trahissant tout, honneur, devoir…
%# Il a pay=é sa double perfidie;
%# Mais ton Irza n'est point mon Astasie.
\livretPersDidas Atar (avec fureur)
%# Elle n'est pas en mon pouvoir?
\livretDidasPPage (Aux Eunuques.)
%# Que l'on m'amène Irza. Si ta bouche en impose,
%# Je la poignarde devant toi.
\livretPers Tarare
%# La voir mourir est peu de chose;
%# Tu te puniras, non pas moi.
\livretPers Atar
%# De sa mort la tienne suivie…
\livretPersDidas Tarare (fièrement)
\livretRef#'FCBtarare
%# Je ne puis mourir qu'une fois.
%# Qu'en je m'engageai sous tes lois,
%# Atar, je te donnai ma vie;
%# Elle est toute entière à mon roi;
%# Au lieu de la perdre pour toi,
%# C'est par toi qu'elle m'est ravie.
%# J'ai rempli mon sort, suis ton choix;
%# Je ne puis mourir qu'une fois.
%# Mais souhaite qu'un jour ton peuple te pardonne.
\livretPers Atar
%#- Une menace?
\livretPers Tarare
%#= Il s'en étonne!
%# Roi féroce! as-tu donc compté,
%# Parmi les droits de ta couronne,
%# Celui du crime et de l'impunité?
%# Ta fureur ne peut se contraindre,
%# Et tu veux n'être pas ha=ï!
%#- Tremble d'ordonner…
\livretPers Atar
%#= Qu'ai-je à craindre?
\livretPers Tarare
%# De te voir toujours obé=i;
%# Jusqu'à l'instant où l'effray=ante somme
%# De tes forfaits déchaînant leur courroux…
%# Tu pouvais tout contre un seul homme;
%# Tu ne pourras rien contre tous.
\livretPers Atar
%#- Qu'on l'entoure!
\livretDidasP\wordwrap {
  (Les Esclaves l'entourent. Tarare va s'assessoir sur le billot,
  au pied du bûcher, la tête appuyé sur ses mains, et ne regard plus rien.)
}

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\wordwrap-center {
  Astasie voilée, Atar, Arthénée, Tarare, Spinette, Esclaves des deux sexes, Soldats.
}
\livretPersDidas Atar (à Astasie)
\livretRef#'FDArecit
%#= Ainsi donc, abusant de vos charmes,
%# Fausse Irza, par de feintes larmes,
%# Vous tri=omphiez de me tromper?
%# Je prétends, avant de frapper,
%# Savoir comment ma puissance jou=ée…
\livretPers Spinette
%# Une esclave fidèle, *hélas! substitu=ée,
%# Innocemment causa le désordre et l'erreur.
\livretPersDidas Tarare \line { (à part, tenant sa tête dans ses mains) }
%# Ah! cette voix me fait horreur!
\livretPers Atar
%# Il est donc vrai, cet échange funeste!
%# J'adorais sous le nom d'Irza…
\livretDidasPPage (À Astasie.)
%# Va, malheureuse, je déteste
%# L'indigne amour qui pour toi m'embrasa.
%# À la rigueur des loix, avec lui, sois livrée!
\livretDidasPPage (Au grand prêtre.)
%# Pontife, décidez leur sort.
\livretPers Arthénée
%# Ils sont jugés: levez l'étendard de la mort.
%# De leurs jours criminels la trame est déchirée.
\livretDidasP\justify { 	
  Le grand-prêtre déchire la bannière de la vie. Le prêtre en deuil
  élève la bannière de la mort. On entend un bruit funèbre d'instruments
  déguisés.	
}	
\livretDidasP\justify {
  (Astasie se jette à genoux, et prie pendant le chœur. On apporte au
  grand-prêtre le livre des arrêts, couvert d'un crêpe. Il signe l'arrêt
  de mort. Deux enfants en deuil lui remettent chacun un
  flambeau. Quatre prêtres en deuil lui présentent deux grands vases
  pleins d'eau lustrale. Il éteint dans ces vases le deux flambeaux en
  les renversant.  Pendant ce temps, les prêtres de la vie se retirent en
  silence. Le drapeau de la vie déchiré, traîne a terre.)
}
\livretPers Chœur funèbre des Esclaves
\livretRef#'FDBchoeur
%# Avec tes décrets infinis,
%# Grand dieu, si ta bonté s'accorde,
%# Ouvre à ces coupables punis
%# Le sein de ta miséricorde!
\livretPersDidas Arthénée (prie)
%# Brama! de ce bûcher, par la mort ré=unis,
%# Ils montent vers le ciel; qu'ils n'en soient point bannis!
\livretDidasP\wordwrap {
  (Astasie se releve, et s'avance au bûcher, où Tarare est abîmé de douleur.)
}
\livretPersDidas Astasie (à Tarare)
\livretRef#'FDCrecit
%# Ne m'impute pas, étranger,
%# Ta mort que je vais partager.
\livretPersDidas Tarare (se relève avec feu)
%#- Qu'entends-je? Astasie!
\livretPers Astasie
%#= Ah! Tarare!
\livretDidasPPage\wordwrap {
  (Ils se jettent dans les bras l'un de l'autre.)
}
\livretPers Arthénée
%#- Je te l'avais prédit.
\livretPersDidas Atar (furieux)
%#= Qu'on les sépare.
%# Qu'un seul coup les fasse périr.
%# Non… C'est trop tôt briser leurs chaînes;
%# Ils seraient heureux de mourir.
%# Ah! je me sens altéré de leurs peines,
%# Et j'ai soif de les voir souffrir.
\livretPersDidas Astasie (avec dédain, au roi)
\livretRef#'FDDastasie
%# Ô tigre! mes dédains ont trompé ton attente,
%# Et, malgré toi, je goûte un instant de bonheur:
%# J'ai bravé ta faim dévorante,
%# Le rugissement de ton cœur.
%# Pour prix de ta lâche entreprise,
%# Vois, Atar, je l'adore, et toi, je te méprise.
\livretDidasPPage (Elle embrasse Tarare.)
\livretPersDidas Atar (vivement aux Soldats)  
%# Arrachez-la tous de ses bras.
%# Courez. Qu'il meure et qu'elle vive!
\livretPersDidas Astasie \line { (tire un poignard, qu’elle approche de son sein) }
%# Si quelqu'un vers lui fait un pas,
%# Je suis morte avant qu'il arrive.
\livretPersDidas Atar (aux Soldats)
%#- Arrêtez-vous!
\livretPers Astasie, Tarare, Atar
\livretRef#'FDEtrio
%#= Le trépas nous attend…
\livretPers Tarare, Astasie
%# Encore une minute,
%# Et notre amour constant
%# Ne sera plus en butte
%# Aux coups d'un noir sultan.
\livretDidasPPage (Les Soldats font un mouvement.)	
\livretPers Atar
%# Arrêtez un moment!
\livretPers Astasie
%# Je me frappe à l'instant
%# Que sa loi s'exécute.
%# Sur ton cœur palpitant,
%# Tu sentiras ma chute,
%# Et tu mourras content.
\livretPers Atar
%# O rage! affreux tourment!
%# C'est moi, c'est moi qui lutte,
%# Et leur cœur est content.
\livretPers Astasie
%# Sur ton cœur palpitant
%# Tu sentiras ma chute,
%# Et tu mourras content.
\livretPers Tarare
%# Sur mon cœur palpitant
%# Je sentirai ta chute,
%# Et je mourrai content.

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center {
  Acteurs précédents. Une foule d’esclaves des deux sexes,
  accourt avec frayeur, et se serre à genoux autour d’Atar.
}
\livretPers Chœur d'esclaves effrayés
\livretRef#'FEAchoeur
%# Atar, défends-nous, sauve-nous.
%# Du palais la garde est forcée;
%# Du sérail la porte enfoncée.
%# Notre asyle est à tes genoux;
%# Ta milice en fureur redemande Tarare.

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center {
  Les précédents, toute la Milice le sabre à la main, Calpigi, Urson.
}
\livretDidasP\wordwrap {
  (Les prêtres de la mort se retirent.)
}
\livretPersDidas Chœur de Soldats (furieux. Ils renversent le bûcher)
\livretRef#'FFAchoeur
%# Tarare, Tarare, Tarare;
%# Rendez-nous notre général.
%# Son trépas, dit-on, se prépare.
%# S'il a reçu le coup fatal,
%# Vengeons sa mort sur ce barbare.
\livretDidasPPage\wordwrap { (Ils s’avancent vers Atar.) }
\livretPersDidas Tarare (enchaîné, écarte les Esclaves)
%# Arrêtez, soldats, arrêtez.
%# Quel ordre ici vous a portés?
%# Ô l'abominable victoire!
%# On sauverait mes jours, en flétrissant ma gloire!
%# Un tas de rebelles mutins
%# De l'État ferait les destins!
%# Est-ce à vous de juger vos maîtres?
%# N'ont-ils soudoy=é que des traîtres?
%# Oubli=ez-vous, soldats, usurpant le pouvoir,
%# Que le respect des rois est le premier devoir?
%# Armes bas, furi=eux! votre empereur vous casse.
\livretDidasPPage\wordwrap { (Ils se jettent tous à genoux.) }
%# Seigneur, ils sont soumis; je demande leur grâce.
\livretPersDidas Atar (hors de lui)
%# Quoi! toujours ce fantôme entre mon peuple et moi!
\livretDidasPPage (aux Soldats.)
%# Défenseurs du sérail, suis-je encor votre roi?
\livretPers Un Eunuque
%#- Oui.
\livretPersDidas Calpigi (le menace du sabre)
%#- Non.
\livretPersDidas Tous les Soldats (se levent)
%#- Non.
\livretPers Tout le Peuple
%#- Non.
\livretPersDidas Calpigi (montrant Tarare)
%#- C'est lui.
\livretPers Tarare
%#- Jamais.
\livretPers Les Soldats
%#- C'est toi.
\livretPers Tout le Peuple
%#= C'est toi.
\livretPersDidas Atar (avec désespoir, à Tarare)
%# Monstre !… Ils te sont vendus… Règne donc à ma place.
\livretDidasPPage\wordwrap { (Il se poignarde, et tombe.) }
\livretPersDidas Tarare (avec douleur)  
%#- Ah! malheureux!
\livretPersDidas Atar (se relève dans les angoisses)
%#= La mort est moins dure à mes yeux…
%# Que de régner par toi… sur ce peuple odi=eux.
\livretDidasPPage\justify {
  (Il tombe mort dans les bras des Eunuques, qui l’emportent. Urson les suit.)
}

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\wordwrap-center {
  Les acteurs précédents, excepté Atar et Urson.
}
\livretPersDidas Calpigi (crie au peuple)
\livretRef#'FGAchoeur
%# Tous les torts de son règne, un seul mot les répare:
%# Il laisse le trône à Tarare.
\livretPersDidas Tarare (vivement)
%# Et moi je ne l'accepte pas.
\livretPersDidas Chœur général (exalté)
%# Tous les torts de son règne, un seul mot les répare:
%# Il laisse le trône à Tarare.
\livretPersDidas Tarare (avec dignité)
\livretRef#'FGBtarare
%# Le trône est pour moi sans appas:
%# Je ne suis point né votre maître.
%# Vouloir être ce qu'on n'est pas,
%# C'est renoncer à tout ce qu'on peut être.
%# Je vous servirai de mon bras:
%# Mais laissez-moi finir en paix ma vie
%# Dans la retraite avec mon Astasie.
\livretDidasPPage\justify {
  (Il lui tend les bras, elle s'y jette.)
}

\livretScene SCÈNE HUITIÈME
\livretDescAtt\wordwrap-center {
  Les acteurs précédents, Urson tenant dans sa main la couronne d’Atar.
}
\livretPersDidas Urson (prend la chaîne de Tarare)
\livretRef#'FHArecit
%# Non, par mes mains, le peuple entier
%# Te fait son noble prisonnier:
%# Il veut que de l'État tu saisisses les rênes.
%# Si tu rejetais notre foi,
%# Nous abuserions de tes chaînes
%# Pour te couronner malgré toi.
\livretDidasPPage (Au grand-prêtre.)
%# Pontife, à ce grand homme, Atar lègue l'Asie;
%# Consacrez le seul bien qu'il ait fait de sa vie:
%# Prenez le di=adème, et réparez l'affront
%# Que le bandeau des rois a reçu de son front.
\livretPersDidas Arthénée (prenant le diadème des mains d’Urson)
\livretRef#'FHBChoeur
%# Tarare, il faut céder!
\livretPersDidas Tout le Peuple (s’écrie)
%# Tarare, il faut céder!
\livretPers Arthénée
%# Leurs désirs sont extrêmes.
\livretPers Tout le Peuple
%# Nos désirs sont extrêmes.
\livretPers Arthénée
%# Sois donc le roi d'Ormus.
\livretPers Tout le Peuple
%# Sois, sois le roi d'Ormus.
\livretPers Arthénée
\livretDidasPPage\justify {
  (lui met la couronne sur la tête au bruit d'une fanfare)
}
%# Il est des dieux suprêmes.
\livretDidasPPage (Il sort.)

\livretScene SCÈNE NEUVIÈME
\livretDescAtt\wordwrap-center {
  Tous les précédents, excepté le grand-prêtre.
}
\livretPersDidas Tarare (pendant qu’on le déchaîne)
\livretRef#'FIArecit
%# Enfants, vous m'y forcez, je garderai ces fers;
%# Ils seront à jamais ma roy=ale ceinture.
%# De tous mes ornements devenus les plus chers,
%# Puissent-ils attester à la race future
%# Que, du grand nom de roi si j'acceptai l'éclat,
%# Ce fut pour m'enchaîner au bonheur de l'État!
\livretDidasPPage\justify { (Il s’enveloppe le corps de ses chaînes.) }
\livretPersDidas Chœur général (avec ivresse)
\livretRef#'FIBchoeur
%# Quel plaisir de nos cœurs s'empare!
%# Vive notre grand roi Tarare!
%# Tarare, Tarare, Tarare!
%# La belle Astasie =et Tarare!
%# Nous avons le meilleur des rois:
%# Jurons de mourir sous ses lois.
\livretDidasP\justify {  
  Des mouvements d‘une joie effrénée, sort une danse tumultueuse,
  pendant que le chœur répète, à grands cris, les vers ci-dessus. Ils
  entourent, ils entraînent Astasie et le roi. La musique diminue de
  bruit, change d’effet, et reprend un caractère aérien. Des nuages
  couvrent le spectacle ; on en voit sortir dans les air, La Nature
  productrice, et le Génie qui préside au soleil.
}

\livretScene SCÈNE DIXIÈME
\livretDescAtt\wordwrap-center {
  Les précédents, La Nature et Le Génie du Feu sur les nuages.
}
\livretPers Le Génie du Feu
\livretRef#'FJArecit
%# Nature, quel exemple imposant et funeste!
%# Le soldat monte au trône, et le tyran est mort!
\livretPers La Nature
%# Les dieux ont fait leur premier sort:
%# Leur caractère a fait le reste.
\livretPers Le Génie du Feu
%# Encore un généreux effort.
%# Dans le cœur des humains, d'un trait inaltérable,
%# Gravons ce précepte admirable.
\livretPersDidas Chœur général (très éloigné)
\livretRef#'FJBchoeur
%# De ce grand bruit, de cet éclat,
%# Ô ciel ! apprends-nous le mystère!
\livretPersDidas La Nature, le Génie du Feu \line { (Dans les nuages, à l’unisson, et parlant fortement.) }
%# Mortel, qui que tu sois, prince, brame ou soldat;
%# Homme ! ta grandeur sur la terre,
%# N'appartient point à ton état;
%# Elle est toute à ton caractère.
\livretDidasP\justify { 	
  À mesure que la Nature et le Génie prononcent les verses ci-dessus,
  ils se peignent en caractéres de feu, dans les nuages.
  Le trompettes sonnent ; le tonnerre reprend ; les nuages les couvrent ;
  il disparaissent. La toile tombe.
}

\livretScene Je proposerais cette fin…
\livretDescAtt\justify {
  Dans un siècle et dans un pays où l’on regarderait comme un manque de respect pour l’opéra,
  de le finir autrement que par une fête, je proposerais cette fin, quoique je préfère la première.
}
\livretDidasP Après le chœur :
%# Quel plaisir de nos cœurs s'empare!
%#8 Vive notre grand roi Tarare! etc.
\livretPersDidas Urson (viendrait dire)
\livretRef#'FKArecit
%# Les fiers Europé=ans marchent vers ces états;
%# Inaugurons Tarare, et courons au combat.
\livretPers Urson, Calpigi
\livretRef#'FKBchoeur
%# Roi, nous mettons la liberté
%# Aux pieds de ta vertu suprême.
%# Règne sur ce peuple qui t'aime,
%# Par les lois et par l'équité.
\livretPers Deux Femmes
%# Et vous, reine, épouse sensible,
%# Qui connûtes l'adversité,
%# Du devoir souvent inflexible
%# Adoucissez l'austérité.
%# Tenez son grand cœur accessible
%# Aux soupirs de l'humanité.
\livretPers Chœur général
%# Roi, nous mettons la liberté
%# Aux pieds de ta vertu suprême;
%# Règne sur ce peuple qui t'aime,
%# Par les lois et par l'équité.
\livretDidasP\wordwrap { 	
  Danse générale, et la toile tomberait.	
}
\livretDidasP\justify {
  Cette fin est mise en musique par M. Salieri. Mais je préfère la première,
  qui est bien plus philosophique, et encadre mieux le sujet.
  Choisissez ; ma tâche est finie.
}
