\livretAct ACTE TROISIÈME
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\column {
  \justify {
    Le théâtre représente les jardins du sérail ; l’appartament d’Irza est à droite ;
    à gauche, et sur le devant, est un grand sopha sous un dais superbe,
    au milieu d’un parterre illuminé. Il est nuit.
  }
  \justify {
    Calpigi, entre d’un coté ; Atar, Urson entrent de l’autre ;
    des Jardiniers ou Bostangis qui allument.
  }
}
\livretPersDidas Calpigi (sans voir Atar)
\livretRef#'DAArecit
%# Les jardins éclairés! des bostangis! pourquoi?
%#- Quel autre ose au sérail donner des ordres?…
\livretPersDidas Atar (lui frappant sur l’épaule)
%#= Moi.
\livretPersDidas Calpigi (troublé)
%#- Seigneur… puis-je savoir?…
\livretPers Atar
%#= Ma fête à ce que j'aime?
\livretPers Calpigi
%# Est fixée =à demain; seigneur, c'est votre loi.
\livretPersDidas Atar (brusquement)
%# Moi, je la veux à l'instant même.
\livretPers Calpigi
%# Tous mes acteurs sont dispersés.
\livretPersDidas Atar (plus brusquement)
%# Du bruit autour d'Irza; qu'on danse, et c'est assez.
\livretPersDidas Calpigi (à part, avec douleur)
%# Ô l'affreux contre-temps! De cet ordre bizarre,
%# Il n'est aucun moy=en de prévenir Tarare!
\livretPersDidas Atar (l’examinant)
%# Quel est donc ce murmure inqui=et et profond?
\livretPersDidas Calpigi (affecte un air gai)
%# Je dis… qu’il semble voir ces spectacles de France,
%# Où tout va bien, pourvu qu'on danse.
\livretPersDidas Atar (en colère)
%# Vil chrétien! obé=is; ou ta tête en répond.
\livretPersDidas Calpigi (à part, en s’en allant)
%#- Tyran féroce!
\livretDidasPPage\line { (Les bostangis se retirent.) }

\livretScene SCÈNE DEUXIÈME
\livretDescAtt\wordwrap-center { Atar, Urson. }
\livretPers Atar
\livretRef#'DBArecit
%#= Avant que ma fête commence,
%# Urson, conte-moi promptement
%# Le détail et l'événement
%# De leur combat à toute outrance.
\livretPers Urson
\livretRef#'DBBurson
%# Tarare le premier arrive au rendez-vous:
%# Par quelques passes dans la plaine,
%# Il met son cheval en haleine,
%# Et vient converser avec nous.
%# Sa contenance est noble et fière.
%# Un long nu=age de poussière
%# S'avance du côté du nord;
%# On croit voir une armée =entière.
%# C'est l'impétu=eux Altamort.
%# D'esclaves armés un grand nombre,
%# Au galop à peine le suit.
%# Son aspect est farouche et sombre,
%# Comme les spectres de la nuit.
%# D'un œil ardent mesurant l'adversaire;
%# Du vaincu décidons le sort.
%#8 « Ma loi », dit Tarare, « est la mort ».
%# L'un sur l'autre à l'instant fond comme le tonnerre.
%# Altamort pare le premier.
%# Un coup affreux de cimeterre
%# Fait voler au loin son cimier.
%# L'acier étincelle,
%# Le casque est brisé,
%# Un noir sang ruisselle.
%# Dieux! je suis blessé.
%# Plus furi=eux que la tempête,
%# A plomb sur la tête,
%# Le coup est rendu,
%# le bras tendu,
%# Tarare pare…
%# Et tient en l'air le trépas suspendu.
\livretPers Atar
%# Je vois qu'Altamort est perdu.
\livretPers Urson
%# Aveuglé par le sang, il s'agite, il chancelle.
%# Tarare, courbé sur sa selle,
%# Pique en avant. Son fier coursier,
%# Sentant l'aiguillon qui le perce,
%# S'élance, et du poitrail renverse
%# Et le cheval et le guerrier.
%# Tarare à l'instant saute à terre,
%# Court à l'ennemi terrassé.
%# Chacun frémit, le cœur glacé
%#8 Du terrible droit de la guerre…
\livretRef#'DBCurson
%# Ne crains rien, superbe Altamort:
%# Entre nous la guerre est finie.
%# Si le droit de donner la mort
%# Est celui d'accorder la vie,
%# Je te la laisse de grand cœur.
%# Pleure longtemps ta perfidie.
\livretPers Atar
%#- Sa perfidie?
\livretPers Urson
%#= Il s'en éloigne avec douleur
\livretPers Atar
%#- Il est instruit.
\livretPers Urson
%#= Inutile et vaine faveur!
%# Celui dont les armes trop sûres,
%# Ne firent jamais deux blessures,
%# A peine, *hélas! se retirait,
%# Que son adversaire expirait.
\livretPers Atar
\livretRef#'DBDatar
%# Partout il a donc l'avantage!
%# Ah! mon cœur en frémit de rage!
%# Quand, par le combat, Altamort
%# Voulut hi=er régler leur sort,
%# Urson, je sentais bien d'avance,
%# Qu'il allait de sa mort
%# Pay=er cette imprudence.
%# Sans les clameurs d'un père épouvanté,
%# Le temple était ensanglanté;
%# Mais son pouvoir força le nôtre
%# D'arrêter un crime opportun,
%# Qui m'offrait, dans le mort de l'un,
%# Un prétexte pour perdre l'autre.
\livretDidasPPage\line { (Il voit entrer les esclaves.)	}
%# Tout le sérail ici porte ses pas.
%# Retire-toi; que cette affreuse image,
%# Se dissipant comme un nu=age,
%# Fasse place aux plaisirs, et ne les trouble pas.
\livretDidasPPage (Urson sort.)

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center {
  Atar, Astasie en habit de sultane, soutenue par des esclaves, son
  mouchoir sur les yeux ; Spinette, Calpigi, Eunuques, Esclaves de
  deux sexes.
}
\livretDidasP\justify { 	
  (Atar fait asseoir Astasie sur le grand sopha, près de lui, et dit
  au chef des eunuques :)
}
\livretPers Atar
\livretRef#'DCBrecit
%# Calpigi, quel spectacle offrai-je à ma sultane?
\livretPers Calpigi
%# C'est une fête europé=ane.
%# Ainsi, quand l'un des rois de ces puissants états,
%# Ordonne qu'on amuse une reine adorée;
%# Des jeux brillants, des mœurs de vos climats,
%# Sa noble fête à l'instant est parée.
\livretDidasPPage (à part)
%# Tarare n'est point prévenu;
%# S'il arrivait, il est perdu.

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\column {
  \justify {
    Les acteurs précédents, Bergers européans de cour, vêtus galemment
    en habits de taffetas, avec des plumes, ainsi que leurs bergères,
    ayant des houlettes dorées.
    Paysans grossiers, vêtus à l’européane, ainsi que leurs paysannes,
    mais très simplement, tenant des instruments aratoires.
  }
  \justify {
    Marche, dont le dessus léger peint le caratère des bergers de
    cour qui la dansent, et dont la basse peint la lourde gaîté des
    paysans qui la sautent.
  }
}
\livretRef#'DDAmarche
\livretDidasPage Marche.
\livretPers Chœur d'Européans
\livretRef#'DDBchoeur
%# Peuple léger mais généreux,
%# Nous blâmons les mœurs de l'Asie:
%# Jamais, dans nos climats heureux,
%# La beauté ne tremble asservie.
%# Chez nos maris, presqu'à leurs yeux,
%# Un galant en fait son amie;
%# La prend, la rend, rit avec eux,
%# Et porte ailleurs sa douce envie.
\livretRef#'DDCmenuet
\livretDidasPPage\justify {
  Deux jeunes seigneur et dame de la cour commencent une danse assez
  vive ; deux jeunes berger et bergère de la campagne, commencent en
  même temps un pas assez simple. Leur danse est interrompue par une
  bergère coquette et une bergère sensible.
}
\livretPersDidas Spinette (en Bergère coquette, aux danseurs)
\livretRef#'DDEduo
%# Galants qui courtisez les belles,
%# Sachez brusquer un doux moment.
\livretPersDidas Une Bergère (sensible)
%# Amants qui soupirez pour elles,
%# Espérez tout du sentiment.
\livretPers (Coquette)
%# Toute occasion non saisie,
%# S'échappe et se perd sans retour.
\livretPers (Sensible)
%# Sans retour pour la fantaisie;
%# Mais elle renaît pour l'amour.
\livretDidasPPage\justify {
  Le pas des quatre danseurs reprend et s'achève.
}
\livretRef#'DDFdanse
\livretDidasPPage\justify {
  De vieux seigneurs dansent vivement devant des bergères modestes, en
  leur présentant des bouquets ; des jeunes gens fatigués, appuyés sur
  leur houlettes, se meuvent à peine devant de vieilles coquettes qui
  dansent à perdre haleine. Atar se lève, et erre parmi les danseurs.
}
\livretPersDidas Spinette (en bergère de cour)
\livretRef#'DDGspinette
%# Dans nos vergers délici=eux,
%# Le mal, le mieux,
%# Tout se balance;
%# Et si nos jeunes gens sont vieux,
%# Tous nos vieillards sont dans l'enfance.
\livretPersDidas Un Paysan (grossier)
%# Chez nous point d'imposture;
%# Enfants de la nature,
%# Nos tendres soins
%# Sont pour les foins,
%# Et notre amour pour la pâture.
\livretDidasPPage (On danse.)	
\livretPersDidas Spinette (en bergère de cour)
%# Quand l'époux devient indolent,
%# Contre un galant
%# L'amour l'échange;
%# Et de ses volages désirs,
%# Par des plaisirs,
%# L'*hymen se venge.
\livretPersDidas Un Paysan (grossier)
%# Chez nous, jamais légère,
%# L'active ménagère,
%# Pour favori
%# N'a qu'un mari;
%# Mais de ses fils chacun est père.
\livretDidasPPage (On danse.)	
\livretPers Spinette
%# Chez nous, sans bruit
%# On se détruit;
%# On brigue, on nuit,
%# Mais sans scandale.
\livretPersDidas Un Paysan (grossier, achevant le couplet)
%# Ma foi, chez nous, tout ce qu'autrui
%# Te fait, fais-lui;
%# C'est la morale.
\livretPers Astasie
\livretRef#'DDHastasie
%# Grands dieux! que la mort d'Astasie
%# L'arrache au tyran de l'Asie!
\livretRef#'DDIair
\livretDidasPPage (On danse.)	
\livretPersDidas Astasie (pendant la danse)
%# Ô mon Tarare, ô mon époux!
%# Dans quel désespoir êtes-vous!
% \livretPers Chœur d'Européans
% %# Aux travaux mêlons la gaîté;
% %# Tout mal guérit par ses contraires.
% %# Nos loix ont de l'austérité;
% %# Mais nos mœurs sont douces, légères.
% %# Si le dur hymen est chez nous
% %# Bien absolu, bien despotique;
% %# L'amour en secret fait de tous
% %# Une charmante république.
% \livretDidasPPage (On danse.)	
% \livretPersDidas Astasie (les bras élevés pendant la danse)
% %# Grands dieux! que la mort d'Astasie
% %# L'arrache au tyran de l'Asie!
\livretRef#'DDJspinette
\livretDidasPPage (La danse continue.)
\livretPersDidas Atar (revient à Astasie, et dit à tout le serrail)
\livretRef#'DDKrecit
%# Salu=ez tous la belle Irza.
%# Je la couronne; elle est sultane.
\livretDidasPPage\justify {
  (Il lui attache au front un diadème de diamants.)
}
\livretPers Chœur Universel
\livretRef#'DDLchoeur
%# Salu=ons tous la belle Irza.
%# Qu'amour, du fond d'une cabane,
%# Au trône d'Ormus éleva.
%# Du grand Atar elle est sultane.
\livretRef#'DDMdanse
\livretDidasPPage (On danse.)
% \livretPersDidas Astasie (pendant la danse)  
% %# Ô mon Tarare, ô mon époux!
% %# Dans quel désespoir êtes-vous!
% \livretDidasPPage\justify {
%   (Spinette la masque de sa personne pour que l'empereur ne la voie pas.)	
% }
% \livretDidasPPage\justify {
%   Ballet général, ou les deux genres de danse se mêlent sans se confondre.
% }
% \livretDidasPPage\justify {
%   Le ballet fini, des esclaves apportent des vases de sorbet, des
%   liqueurs et des fruits devant Atar et la sultane. Spinette reste
%   auprès de sa maîtresse, prête à la servir.
% }
\livretPersDidas Atar (avec joie)
\livretRef#'DDNrecit
%# Calpigi, ta fête est charmante!
%# J’aime un talent vainqueur à qui tout obéit:
%# Ton esprit fertile m’enchante.
%# Des mers d’Europe et contre toute attente,
%# Dis-nous quel *heureux sort en ce lieu t’a conduit.
%# Mais pour amuser mon amante,
%# Anime ton récit d’une gaîté piquante.
\livretPers Calpigi
%# J'y veux mêler un nom qui nous rendra la nuit.
\livretDidasPPage\justify {
  (Il prend une mandoline, et chante sur le tone de la barcarola.)
}
\livretDidasPPage\justify {
  (La danse figurée cesse; tous les danseurs et danseuses se prennent
  par la main pour danser le refrain de sa chanson.)
}
\livretDidasPPage 1er couplet
\livretRef#'DDOcalpigi
%#8 Je suis natif de Ferrare;
%# Là, par les soins d'un père avare,
%# Mon chant s'étant fort embelli;
%# A*hi! povero Calpigi!
%# Je passai du conservatoire,
%# Premier chanteur à l'oratoire
%# Du souverain di Napoli:
%# Ah! bravo, caro Calpigi!
\livretDidasPPage\justify {
  (Le chœur répète le dernier vers. On danse la ritournelle.)
}
\livretDidasPPage\justify {
  (À la fin de chaque couplet, Calpigi se retourne, et regarde avec
  inquiétude du côté par où il craint que Tarare n'arrive.)
}
\livretDidasPPage 2e couplet
%# La plus célèbre cantatrice,
%# De moi fit bientôt par caprice,
%# Un simulacre de mari.
%# A*hi! povero Calpigi!
%# Mes fureurs, ni mes jalousies,
%# N'arrêtant point ses fantaisies,
%# J'étais chez moi comme un zéro:
%# A*hi! Calpigi povero!
\livretDidasPPage\justify {
  (Le chœur répète le dernier vers. On danse la ritournelle.)
}
\livretDidasPPage 3e couplet
%# Je résolus, pour m'en défaire,
%# De la vendre à certain corsaire,
%# Exprès passé de Tripoli:
%# Ah! bravo, caro Calpigi!
%# Le jour venu, mon traître d'homme,
%# Au lieu de me compter la somme,
%# M'enchaîne au pied de leur châlit:
%# A*hi! povero Calpigi!
\livretDidasPPage 4e couplet
%# Le forban en fit sa maîtresse;
%# De moi, l'argus de sa sagesse;
%# Et j'étais là tout comme ici:
%# A*hi! povero Calpigi!
\livretDidasPPage\justify {
  (Spinette, en cet endroit, fait un grand éclat de rire.)
}
\livretPers Atar
%# Qu'avez-vous à rire, Spinette?
\livretPers Calpigi
%# Vous voy=ez ma fausse coquette.
\livretPers Atar
%#- Dit-il vrai?
\livretPers Spinette
%#= Signor, è vero.
\livretPersDidas Calpigi (acheve l’air)
%# A*hi! Calpigi povero!
\livretDidasPPage\justify {
  (Le chœur répète le dernier vers. On danse la ritournelle.)
}
\livretDidasP\justify {
  (Ici l’on voit dans le fond Tarare descendre par une échelle de soie ;
  Calpigi l’aperçoit.)
}
\livretDidasPPage (à part)
%#- C'est Tarare!
\livretDidasPPage 5e couplet, plus vite
%#= Bientôt à travers la Libye,
%# L'Egypte, l'Isthme et l'Arabie,
%# Il allait nous vendre au Sophi:
%# A*hi! povero Calpigi!
%# Nous sommes pris, dit le barbare.
%#8 Qui nous prenait? Ce fut Tarare…
\livretPersDidas Astasie (faisant un cri)
\livretRef#'DDPtous
%#- Tarare!
\livretPersDidas Tout le sérail (s’écrie)
%#- Tarare!
\livretPersDidas Atar (furieux)
%#= Tarare!
\livretDidasPPage\justify {
  (Il renverse la table d'un coup de pied.)
}
\livretDidasP\justify {
  (Astasie se lève troublée. Spinette la soutient. Au bruit qui se
  fait, Tarare, à moitié descendu, se jette en bas dans l'obscurité.)
}
\livretPersDidas Spinette (à Astasie)
%# Dieux! que ce nom l'a courroucé!
\livretPers Atar
%# Que la mort, que l'enfer s'empare
%# Du traitre qui l'a prononcé!
\livretDidasPPage\justify {
  (Il tire son poignard ; tout le monde s’en fuit.)
}
\livretPersDidas Spinette (soutenant Astasie)
%#- Elle expire!
\livretDidasP\justify {
  Atar rappellé à lui par ce cri, laisse aller Calpigi et les autres
  esclaves, et revient vers Astasie, que des femmes emportent chez
  elle. Atar y entre, en jettant à la porte sa simarre et ses
  brodequins, à la manière des orientaux.
}

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\column {
  \wordwrap-center { Le théâtre est très obscur. }
  \wordwrap-center {
    Calpigi, Tarare, un poignard à la main, prêt à frapper Calpigi qu’il entraîne.
  }
}
\livretPersDidas Calpigi (s’écrie)
\livretRef#'DEArecit
%#- Ô Tarare!
\livretPersDidas Tarare (avec un grand trouble)
%#= Ô fureur que j'ab*horre!
%# Mon ami… s'il n'eût pas parlé,
%# De ma main était immolé!
\livretPers Calpigi
%# Tu le devais, Tarare! il le faudrait encore,
%# Si quelque esclave curi=eux…
\livretPersDidas Tarare (troublé)
%# Mille cris de mon nom font retentir ces lieux!
%# Je me crois découvert, et que la jalousie…
%# Mourir sans la revoir, et si près d'Astasie !…
\livretPers Calpigi
%# O mon héros! tes vêtements mouillés,
%#10 D'algues impures et de limon souillés!…
%# Un grand péril a menacé ta vie!
\livretPers Tarare
\livretRef#'DEBtarare
%# Au sein de la profonde mer,
%# Seul dans une barque fragile,
%# Aucun soufle n'agitant l'air,
%# Je sillonnais l'onde tranquille.
%# Des avirons le monotone bruit,
%# Au loin distingué dans la nuit,
%# Soudain a fait sonner l'alarme;
%# J'avais ce poignard pour toute arme.
%# Deux cents rameurs partent du même lieu:
%# On m'enveloppe, on se croise, on rappelle,
%# J'étais pris !… D'un grand coup d'épieu,
%# Je m'abîme avec ma nacelle,
%# Et, me fray=ant sous les vaisseaux,
%# Une route nouvelle et sure;
%# J'arrive à terre entre deux eaux,
%# Dérobé par la nuit obscure.
%# J'entend la cloche du béfroi.
%# Le son bruy=ant de la trompette,
%# Que le fond du golphe répète,
%# Augmente le trouble et l'effroi.
%# On court, on crie =aux sentinelles,
%# Arrête! arrête: on fond sur moi:
%# Mais, s'ils couraient, j'avais des ailes.
%# J'atteins le mur comme un éclair:
%# On cherche au pied; j'étais dans l'air,
%# Sur l'échelle souple et tendue,
%# Que ton zèle avait suspendue.
\livretRef#'DECduo
%# Je suis sauvé, grâce à ton cœur;
%# Et pour pay=er tant de faveur,
%# Ô douleur! ô crime exécrable!
%# Trompé par une aveugle erreur,
%# J'allais, d'une main misérable,
%# Assassiner mon bienfaiteur!
%# Pardonne, ami, ce crime involontaire.
\livretPers Calpigi
%# Ô mon héros! que me dois-tu?
%# Sans force, *hélas! sans caractère,
%# Le faible Calpigi, de tous les vents battu,
%# Serait moins que rien sur la terre,
%# S'il n'était pas épris de ta mâle vertu!
%# Ne perdons point un instant salutaire:
%# Au sérail, la tranquillité
%# Renaît avec l'obscurité.
\livretDidasPPage\wordwrap { (Il prend un paquet dans une touffe d’arbres.) }
%# Sous cet habit d'un noir esclave,
%# Cachons des guerriers le plus brave.
%# D'homme éloquent, deviens un vil mu=et.
\livretDidasPPage\wordwrap { (Il l’habille en muet.) }
%# Que mon héros, surtout, jamais n'oublie
%# Que sous ce masque, un mot est un forfait;
\livretDidasPPage\wordwrap { (Il lui met un masque noir.) }
%# Et qu'en ce lieu de jalousie,
%# Le moindre est pay=é de la vie.
\livretDidasPPage\wordwrap {
  (Ils s’avancent vers l’appartement d’Astasie. L’arrête et recule.)
}
%# N'avançons pas! j'aperçois la simarre,
%# Les brodequins de l'empereur.
\livretPersDidas Tarare (égaré, criant)
%# Atar chez elle! Ah! mal*heureux Tarare!
%# Rien ne retiendra ma fureur:
%#- Brama! Brama!
\livretPersDidas Calpigi (lui fermant la bouche)
%#= Renferme donc ta peine!
\livretPersDidas Tarare (criant plus fort)
%#- Brama! Brama!
\livretPers Calpigi
%#= Notre mort et certaine.

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center {
  Atar sort de chez Astasie. Tarare, Calpigi.
}
\livretPersDidas Calpigi (crie, effrayé)
\livretRef#'DFArecit
%#- On vient; c'est le sultan.
\livretDidasPPage\line { (Tarare tombe la face contre terre.) }
\livretPersDidas Atar (d’un ton terrible)
%#= Quel insolent ici ?…
\livretPersDidas Calpigi (troublé)
%# Un insolent!… C'est Calpigi!
\livretPers Atar
%# D'où vient cette voix déplorable?
\livretPersDidas Calpigi (troublé)
%# Seigneur, c'est… c'est ce misérable.
%# Croy=ant entendre quelque bruit,
%# Nous faisions la ronde de nuit.
%# D'une soudaine frénésie
%# Cette brute à l'instant saisie…
%# Peut-être a-t-il perdu l'esprit!
%# Mais il pleure, il crie, =il s'agite,
%# Parle, parle, parle si vite,
%# Qu'on n'entend rien de ce qu'il dit.
\livretPersDidas Atar (d’un ton terrible)
%#- Il parle, ce mu=et?
\livretPersDidas Calpigi (plus troublé)
%#= Que dis-je!
%# Parler serait un beau prodige!
%# D'affreux sons inarticulés…
\livretDidasPPage\wordwrap {
  (Atar lui prend les bras. Tarare est sans mouvement, prosterné.)
}
\livretPers Atar
%# O bizarre sort de ton maître!
%#8 Tu maudis quelquefois ton être…
%# Je venais, les sens agités,
%# L'*honorer de quelques bontés,
%# Soupirer d'amour auprès d'elle.
%# À peine étais-je à ses côtés,
%# Elle s'échappe, la rebelle!
%# Je l'arrête et saisis sa main:
%# Tu n'as vu chez nulle mortelle
%# L'exemple d'un pareil dédain!
%#10 « Farouche Atar! quelle est donc ton envie?
%#8 « Avant de me ravir l'*honneur,
%#8 « Il faudra m'arracher la vie… »
%# Ses yeux pétillaient de fureur.
%#10 Farouche Atar!… son honneur!… la sauvage,
%# Appelant la mort à grands cris…
%# Atar, enfin, a connu le mépris.
\livretDidasPPage\line { (Il tire son poignard.) }
%# Vingt fois j'ai voulu, dans ma rage,
%# Épargner moi-même à son bras…
%# Allons, Calpigi, suis mes pas.
\livretPersDidas Calpigi (lui présente sa simarre)
%# Seigneur, prenez votre simarre.
\livretPers ATAR
%# Rattache avant, mon brodequin,
%# Sur le corps de cet africain…
\livretDidasPPage\wordwrap { (Il met son pied sur le corps de Tarare.) }
%#8 Je sens que la fureur m'égare !…
\livretDidasPPage (Il regarde Tarare.)
%# Malheureux nègre, abject et nu,
%# Au lieu d'un reptile inconnu,
%# Que du né=ant rien ne sépare,
%# Que n'es-tu l'odi=eux Tarare!
%# Avec quel plaisir, de ce flanc,
%# Ma main épuiserait le sang !…
%# Si l'insolent pouvait jamais connaître
%#8 Quels dédains il vaut à son maître!…
%# Et c'est pour cet indigne objet;
%#8 C'est pour lui seul qu'elle me brave!…
%# Calpigi, je forme un projet:
%# Coupons la tête à cet esclave;
%# Défigure-la tout-à-fait;
%# Porte-la de ma part toi-même.
%# Dis-lui qu'en mes transports jaloux,
%# Surprenant ici son époux…
\livretDidasPPage\justify { (Il tire le sabre de Calpigi.) }
\livretPersDidas Calpigi (l’arrête et l’eloigne de son ami)
%# De cet horrible stratagème,
%# Ah! mon maître, qu'espérez-vous?
%# Quand elle pourrait s'y méprendre,
%# En deviendrait-elle plus tendre?
%# En l'inqui=étant sur ses jours,
%# Vous la ramènerez toujours.
\livretPersDidas Atar (furieux)
%#10 La ramener!… j'adopte une autre idée.
%# Elle me croit l'âme enchantée:
%# Montrons-lui bien le peu de cas
%# Que je fais de ses vains appas.
%# Cette orgueilleuse a dédaigné son maître!
%# Ô le plus charmant des projet!
%# Je punis l'audace d'un traître
%# Qui m'enleva le cœur de mes sujets;
%# Et j'avilis la superbe à jamais.
%#- Calpigi ?…
\livretPersDidas Calpigi (troublé)
%#- Quoi! Seigneur!
\livretPers Atar
%#= Jure-moi sur ton âme,
%#- D'obé=ir.
\livretPersDidas Calpigi (plus troublé)
%#- Oui, seigneur.
\livretPers Atar
%#= Point de zèle indiscret;
%#- Tout à l'heure.
\livretPersDidas Calpigi (presqu'égaré)
%#- À l'instant.
\livretPers Atar
%#= Prends-moi ce vil mu=et;
%# Conduis-le chez elle en secret;
%# Apprends-lui que ma tendre flamme
%# La donne à ce monstre pour femme.
%# Dis-lui bien que j'ai fait serment
%# Qu'elle n'aura jamais d'autre époux, d'autre amant.
%# Je veux que l'hymen s'accomplisse;
%# Et si l'orgueilleuse prétend
%# S'y dérober, prompte justice.
%# Qu'à son lit à l'instant conduit,
%# Avec elle il passe la nuit;
%# Et qu'à tous les yeux exposée,
%# Demain, de mon sérail elle soit la risée!
%# À présent, Calpigi, de moi je suis content.
%# Toi, par tes signes, fais que cette brute apprenne
%# Le sort fortuné qui l'attend.
\livretPersDidas Calpigi (tranquilisé)
%# Ah! seigneur, ce n'est pas la peine;
%# S'il ne parle pas, il entend.
\livretPers Atar
%# Accompagne ton maître à la garde prochaine.
\livretDidasPPage (Il se retourne pour sortir.)
\livretPers Calpigi 
\livretDidasPPage\wordwrap {
  (en se baissant pour ramasser la simarre de l'empereur, dit tout bas à Tarare)
}
%#- Quel heureux dénouement!
\livretDidasPPage (Il suit Atar.)
\livretPersDidas Tarare (se relève à genoux)
%#= Mais quelle *horrible scène!
\livretDidasPPage\wordwrap {
  (Il relève son masque, qui tombe à terre loin de lui.)
}
%#- Ah! respirons.
\livretDidasPPage\wordwrap {
  (Atar revient à l’appartament d’Astasie, d'un air menaçant,
  et dit avec une joie féroce.)
}
\livretPers Atar
%#= Je pense au plaisir que j'aurai,  
%# Superbe! quand je te verrai
%# Au sort d'un vieux nègre li=ée,
%# Et par cent cris humili=ée!
\livretDidasPPage\wordwrap { (Il imite le chant trivial des esclaves.) }
%# Salu=ons tous la fière Irza,
%# Qui, regrettant une cabane,
%# Aux vœux d'un roi se refusa:
%# De ce vieux nègre elle est sultane.
%#- Hein? Calpigi?
\livretPers Calpigi
%#= Ah! quel plaisir mon maître aura!
\livretPers Atar
%#- Hein! Calpigi?
\livretPers Calpigi
%#= Ah! quel plaisir mon maître aura!
%# Quand le sérail retentira…
\livretPers Atar, Calpigi
%# Salu=ons tous la fière Irza,
%# Qui, regrettant une cabane,
%# Aux vœux d'un roi se refusa:
%# De ce vieux nègre elle est sultane.
\livretDidasPPage\wordwrap {
  (Le même jeu de scène continue ; il sortent.)
}

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\wordwrap-center {
  Tarare seul, levant les mains au ciel.
}
\livretPers Tarare
\livretRef#'DGAtarare
%# Dieu tout-puissant! tu ne trompas jamais
%# L'infortuné qui croit à tes bienfaits.
\livretDidasPPage\wordwrap {
  (Il remet son masque, et suit de loin l'empereur.)
}
\sep
