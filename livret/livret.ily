\notesSection "Livret"
\markuplist\abs-fontsize-lines#8 \page-columns-title \act\line { LIVRET } {
\livretAct PROLOGUE
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\column {
  \wordwrap-center {
    La Nature et les Vents déchainés.
  }
  \justify {
    L’ouverture fait entendre un bruit violent dans les airs, un choc
    terrible de tous les éléments. La toile, en se levant, ne montre que
    des nuages qui roulent, se déchirent, et laissent voir les Vents
    déchainés; ils forment, en tourbillonnant, des danses de la plus
    violente agitation.
  }
  \justify {
    La Nature s’avance au milieu d’eux, une baguette à la main, ornée de
    tous les attributs qui la caractérisent, et leur dit impérieusement :
  }
}
\livretPers La Nature
\livretRef#'AAAouverture
\livretVerse#8 { C’est assez troubler l’univers, }
\livretVerse#12 { Vents furieux, cessez d’agiter l’air et l’onde. }
\livretVerse#8 { C’est assez. Reprenez vos fers ; }
\livretVerse#8 { Que le seul Zéphyr règne au monde. }
\livretDidasP\wordwrap {
  L’ouverture, le bruit et le mouvement continuent.
}
\livretPers Chœur des Vents
\livretVerse#8 { Ne tourmentons plus l’univers : }
\livretVerse#8 { Cessons d’agiter l’air et l’onde. }
\livretVerse#8 { Malheureux ! reprenons nos fers, }
\livretVerse#8 { L’heureux Zéphyr seul règne au monde. }
\livretDidasP\wordwrap {
  Ils se précipitent dans les nuages inférieurs. Le Zéphir s’élève dans
  les airs. L’ouverture et le bruit s’appaisent par degrés ; les nuages
  se dissipent ; tout devient harmonieux et calme. On voit une campagne
  superbe, et Le Génie du Feu descend dans un nuage brillant, du côté
  de l’orient.
}
\livretScene SCÈNE DEUXIÈME
\livretDescAtt\wordwrap-center {
  Le Génie du Feu, La Nature.
}
\livretPers Le Génie du Feu
\livretRef#'ABAgenieNature
\livretVerse#8 { De l’orbe éclatant du soleil, }
\livretVerse#8 { Admirant des cieux la structure, }
\livretVerse#8 { Je vous ai vue, belle Nature, }
\livretVerse#12 { Disposer sur la terre un superbe appareil. }
\livretPers La Nature
\livretVerse#9 { Génie ardent de la sphère enflammée, }
\livretVerse#8 { Par qui la mienne est animée, }
\livretVerse#10 { À mes travaux donnez quelques moments. }
\livretVerse#8 { De toutes les races passées, }
\livretVerse#8 { Dans l’immensité dispersées, }
\livretVerse#8 { Je rassemble les éléments, }
\livretVerse#10 { Pour en former une race prochaine }
\livretVerse#8 { De la risible espèce humaine, }
\livretVerse#8 { Aux dépens des êtres vivants. }
\livretPers Le Génie du Feu
\livretVerse#12 { Ce pouvoir absolu que vous avez sur elle, }
\livretVerse#12 { L’exercez-vous aussi sur les individus ? }
\livretPers La Nature
\livretVerse#12 { Oui, si je descendais à quelque soins perdus ! }
\livretVerse#8 { Mais, pour moi, qu’est une parcelle, }
\livretVerse#8 { À travers ces foules d’humains, }
\livretVerse#8 { Que je répands à pleines mains }
\livretVerse#8 { Sur cette terre, pour y naître, }
\livretVerse#8 { Briller un instant, disparaître, }
\livretVerse#8 { Laissant à des hommes nouveaux }
\livretVerse#8 { Pressés comme eux, dans la carrière, }
\livretVerse#8 { De main en main, les courts flambeaux }
\livretVerse#8 { De leur existence éphémère ? }
\livretPers Le Génie du Feu
\livretVerse#12 { Au moins, vous employez des éléments plus purs, }
\livretVerse#12 { Pour former les puissants et les grands d’un empire ? }
\livretPers La Nature
\livretVerse#10 { C’est leur langage, il faut bien en sourire : }
\livretVerse#10 { Un noble orgueil les en rend presque surs. }
\livretVerse#8 { Mais voyez comme la Nature }
\livretVerse#12 { Les verse par milliers, sans choix et sans mesure. }
\livretDidasP\wordwrap { (Elle fait une espece de conjuration.) }
\livretRef #'ABBnature
\livretVerse#8 { Froids humains, non encor vivants ; }
\livretVerse#8 { Atômes perdus dans l’espace : }
\livretVerse#8 { Que chacun de vos éléments, }
\livretVerse#8 { Se rapproche et prenne sa place, }
\livretVerse#8 { Suivant l’ordre, la pesanteur, }
\livretVerse#8 { Et toutes les loix immuables }
\livretVerse#8 { Que l’éternel dispensateur }
\livretVerse#8 { Impose aux êtres vos semblables. }
\livretVerse#8 { Humains, non encore existants, }
\livretVerse#8 { À mes yeux paraissez vivants. }

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center {
  Le Génie du Feu, la Nature, foule d’Ombres des deux sexes.
}
\livretPers Chœur d'Ombres
\livretRef#'ACAchoeur
\livretVerse#8 { Quel charme inconnu nous attire ? }
\livretVerse#8 { Nos cœurs en sont épanouis. }
\livretVerse#8 { D’un plaisir vague je soupire ; }
\livretVerse#8 { Je veux l’exprimer ; je ne puis. }
\livretVerse#10 { En jouissant, je sens que je désire ; }
\livretVerse#10 { En désirant, je sens que je jouis. }
\livretVerse#8 { Quel charme inconnu nous attire ? }
\livretVerse#8 { Nos cœurs en sont épanouis. }
\livretPersDidas Le Génie du Feu (à la Nature)
\livretRef#'ACBrecit
\livretVerse#12 { Privés des doux liens que donne la naissance ; }
\livretVerse#8 { Quels seront leurs rangs et leurs soins ? }
\livretVerse#8 { Et comment pourvoir aux besoins }
\livretVerse#8 { D’une aussi soudaine croissance ? }
\livretPers La Nature
\livretVerse#8 { J’amuse vos yeux un moment, }
\livretVerse#8 { De leur forme prématurée ; }
\livretVerse#8 { S’ils pouvaient aimer seulement, }
\livretVerse#10 { Vous reverriez le règne heureux d’Astrée. }
\livretPers Le Génie du Feu
\livretVerse#10 { Quel intérêt peut les occuper tous ? }
\livretPers La Nature
\livretVerse#12 { Nul, je crois. }
\livretPersDidas Le Génie du Feu (s'adressant aux ombres)
\livretVerse#12 { \transparent { Nul, je crois. } Qu’êtes-vous ? et que demandez-vous ? }
\livretPersDidas Altamort (l'Ombre)
\livretVerse#8 { Nous ne demandons pas, nous sommes. }
\livretPers Le Génie du Feu
\livretVerse#8 { Qui vous a mis au rang des hommes ? }
\livretPersDidas Arthénée (l'Ombre)
\livretVerse#10 { Qui l’a voulu, que nous importe à nous ? }
\livretPers Le Génie du Feu
\livretVerse#10 { Comme ils sont froids, sans passions, sans gouts ! }
\livretVerse#8 { Que leur ignorance est profonde ! }
\livretPers La Nature
\livretVerse#8 { Ah ! je les ai formés sans vous. }
\livretRef#'ACCnature
\livretVerse#12 { Brillant Soleil, en vain la Nature est féconde ; }
\livretVerse#10 { Sans un rayon de votre feu sacré, }
\livretVerse#10 { Mon œuvre est morte et son but égaré. }
\livretPers Le Génie du Feu
\livretRef#'ACDgenie
\livretVerse#8 { Gloire à l’éternelle sagesse, }
\livretVerse#8 { Qui, créant l’immortel amour, }
\livretVerse#8 { Voulut que, par sa seule ivresse, }
\livretVerse#8 { L’être sensible obtînt le jour. }
\livretVerse#8 { Ah ! si ma flamme ardente et pure }
\livretVerse#8 { N’eût pas embrasé votre sein, }
\livretVerse#8 { Stérile amant de la Nature, }
\livretVerse#8 { J’eusse été formé sans dessein. }
\livretRef #'ACErecit
\livretVerse#10 { Un mot encor ; c’est une ombre femelle. }
\livretDidasPPage (à l’ombre)
\livretVerse#10 { Aimable enfant, voulez-vous être belle ? }
\livretPers Une Ombre femelle
\livretVerse#12 { Belle ? }
\livretPers Le Génie du Feu
\livretVerse#12 { \transparent { Belle ? } Vous rougissez ! }
\livretPers Une Ombre femelle
\livretVerse#12 { \transparent { Belle ? Vous rougissez ! } Suis-je donc sans appas ? }
\livretPers Le Génie du Feu
\livretVerse#12 { Son instinct la trahit, mais ne la trompe pas ! }
\livretPersDidas La Nature (souriant)
\livretVerse#8 { Il peut au moins la compromettre. }
\livretPersDidas Le Génie du Feu (à l'ombre de Spinette)
\livretVerse#12 { Et vous dont les regards causeront cent débats ? }
\livretPersDidas L'Ombre de Spinette (avec feu)
\livretVerse#12 { Je voudrais… je voudrais… je voudrais tout soumettre. }
\livretPers Le Génie du Feu
\livretVerse#12 { Ô ! Nature ! }
\livretPersDidas La Nature (souriant)
\livretVerse#12 { \transparent { Ô ! Nature ! } J’ai tort ; devant vous j’ai trahi, }
\livretVerse#12 { Sur ses plus doux secrets, mon sexe favori. }
\livretPersDidas Le Génie du Feu (à l'ombre d'Astasie)
\livretVerse#12 { Mais vous, jeune beauté, qui semblez animée, }
\livretVerse#12 { Voudriez-vous à tous donner aussi la loi ? }
\livretPers L'Ombre d'Astasie
\livretVerse#8 { Que je sois seulement aimée… }
\livretVerse#8 { Il n’est que ce bonheur pour moi. }
\livretPers La Nature
\livretVerse#10 { Tu le seras, sous le nom d’Astasie, }
\livretVerse#8 { Et Tarare obtiendra ta foi. }
\livretPersDidas Astasie (émue, la main sur son cœur)
\livretVerse#12 { Tarare ! }
\livretPers La Nature
\livretVerse#12 { \transparent { Tarare ! } Je te fais un sort digne d’envie. }
\livretPers Astasie
\livretVerse#10 { Je n’en sais rien. }
\livretPers La Nature
\livretVerse#10 { \transparent { Je n’en sais rien. } Moi, je le sais pour toi. }
\livretPers Le Génie du Feu
\livretVerse#12 { Voyez quelle rougeur à ce nom l’a saisie ! }
\livretPersDidas La Nature (au Génie)
\livretRef#'ACFnature
\livretVerse#8 { Qu’un jeune cœur, malaisément, }
\livretVerse#8 { Voile son trouble au doux moment }
\livretVerse#8 { Où l’amour va s’en rendre maître ! }
\livretVerse#8 { Moi-même, après de longs hivers, }
\livretVerse#8 { Quand vous ranimez l’univers, }
\livretVerse#8 { Mes premiers soupirs font renaître }
\livretVerse#8 { Les fleurs qui parfument les airs. }
\livretPersDidas Le Génie du Feu (montrant les deux ombres d'Atar et de Tarare)
\livretRef#'ACGrecit
\livretVerse#8 { Que sont ces deux superbes ombres, }
\livretVerse#12 { Qui semblent menacer, taciturnes et sombres ? }
\livretPers La Nature
\livretVerse#12 { Rien : mais dites un mot ; assignant leur état, }
\livretVerse#12 { Je fais un roi de l’une et de l’autre un soldat. }
\livretPers Le Génie du Feu
\livretVerse#12 { Permettez ; ce grand choix les touchera peut-être. }
\livretPers La Nature
\livretVerse#13 { J’en doute. }
\livretPers Le Génie du Feu
\livretVerse#13 { \transparent { J’en doute. } Un de vous deux est roi : lequel veut l’être ? }
\livretPers Atar
\livretVerse#12 { Roi ? }
\livretPers Tarare
\livretVerse#12 { \transparent { Roi ? } Roi ? }
\livretPers Atar et Tarare
\livretVerse#12 { \transparent { Roi ? Roi ? } Je ne m’y sens aucun empressement. }
\livretPers La Nature
\livretVerse#8 { Enfants, il vous manque de naître, }
\livretVerse#8 { Pour penser bien différemment. }
\livretPersDidas Le Génie du Feu (les examine)
\livretVerse#10 { Mon œil, entr’eux, cherche un roi préférable ; }
\livretVerse#8 { Mais que je crains mon jugement ! }
\livretVerse#8 { Nature, l’erreur d’un moment }
\livretVerse#8 { peut rendre un siècle misérable. }
\livretPersDidas La Nature (aux deux ombres)
\livretRef#'ACHnature
\livretVerse#8 { Futurs mortels, prosternez-vous, }
\livretVerse#10 { Avec respect attendez en silence }
\livretVerse#8 { Le rang qu’avant votre naissance, }
\livretVerse#8 { Vous allez recevoir de nous. }
\livretDidasP\justify {
  (Les deux ombres se prosternent ; et pendant que le Génie hésite
  dans son choix, toutes les ombres curieuses chantent le chœur
  suivant, en les enveloppant.)
}
\livretPers Chœur d'Ombres
\livretRef#'ACIchoeur
\livretVerse#8 { Quittons nos jeux, accourrons tous : }
\livretVerse#8 { Deux de nos frères à genoux }
\livretVerse#8 { Reçoivent l’arrêt de leur vie. }
\livretPersDidas Le Génie du Feu (impose les mains à l'une des deux ombres)
\livretRef#'ACJrecit
\livretVerse#12 { Sois l’empereur Atar ; despote de l’Asie, }
\livretVerse#10 { Règne à ton gré dans le palais d’Ormus. }
\livretDidasPPage (À l'autre ombre.)
\livretVerse#12 { Et toi, soldat, formé de parents inconnus, }
\livretVerse#10 { Gémis longtemps de notre fantaisie. }
\livretPers La Nature
\livretVerse#12 { Vous l’avez fait soldat ; mais n’allez pas plus loin : }
\livretVerse#12 { C’est Tarare. Bientôt vous serez le témoin }
\livretVerse#8 { De leur dissemblance future. }
\livretDidasPPage (Aux deux ombres.)
\livretRef#'ACKnature
\livretVerse#12 { Enfants, embrassez-vous : égaux par la nature, }
\livretVerse#12 { Que vous en serez loin dans la société : }
\livretVerse#12 { De la grandeur altière à l’humble pauvreté, }
\livretVerse#12 { Cet intervalle immense est désormais le vôtre. }
\livretVerse#12 { À moins que de Brama la puissante bonté, }
\livretVerse#8 { Par un décret prémédité, }
\livretVerse#8 { Ne vous rapproche l’un de l’autre, }
\livretVerse#12 { Pour l’exemple des rois et de l’humanité. }
\livretPersDidas Quatre Ombres principales en chœur
\livretRef#'ACLchoeur
\livretVerse#8 { Ô bienfaisante déité ! }
\livretVerse#8 { Ne souffrez pas que rien altère }
\livretVerse#8 { Notre touchante égalité ; }
\livretVerse#8 { Qu’un homme commande à son frère ! }
\livretDidasP\justify {
  (L'ombre d'Atar seule ne chante pas, et s'éloigne avec hauteur ;
  le Génie du Feu la fait remarquer à la Nature.)
}
\livretPersDidas La Nature (au Génie du Feu)
\livretRef#'ACMrecit
\livretVerse#8 { C’est assez. Éteignons en eux, }
\livretVerse#8 { Ce germe d’une grande idée, }
\livretVerse#12 { Faite pour des climats et des temps plus heureux. }
\livretDidasPPage (À toutes les ombres.)
\livretRef#'ACNnature
\livretVerse#8 { Tels qu’une vapeur élancée, }
\livretVerse#8 { Par le froid en eau condensée, }
\livretVerse#8 { Tombe et se perd dans l’océan ; }
\livretVerse#10 { Futurs mortels, rentrez dans le néant. }
\livretVerse#12 { Disparaissez. }
\livretDidasPPage (Au Génie du Feu.)
\livretVerse#12 { \transparent { Disparaissez. } Et nous, dont l’essence profonde }
\livretVerse#8 { Dévore l’espace et le temps ; }
\livretVerse#12 { Laissons en un clin d’œil écouler quarante ans ; }
\livretVerse#12 { Et voyons-les agir sur la scène du monde. }
\livretDidasP\justify {
  La Nature et le Génie du Feu s’élèvent dans les nuages,
  dont la masse redescend et couvre toute la scène.
}
\livretPers Chœur d'Esprits aériens
\livretRef#'ACOchoeur
\livretVerse#8 { Gloire à l’éternelle sagesse, }
\livretVerse#8 { Qui, créant l’éternel amour, }
\livretVerse#8 { Voulut que par se seule ivresse }
\livretVerse#8 { L’être sensible obtînt le jour. }
\sep
\livretAct ACTE PREMIER
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\column {
  \justify {
    Les nuages qui couvrent le thèâtre s'élèvent ;
    on voit une salle du palais d'Atar.
  }
  \wordwrap-center { Atar, Calpigi. }
}
\livretPersDidas Atar (en entrant, violemment)
\livretRef#'BABrecit
\livretVerse#12 { Laisse-moi, Calpigi ! }
\livretPers Calpigi
\livretVerse#12 { \transparent { Laisse-moi, Calpigi ! } La fureur vous égare. }
\livretVerse#12 { Mon maître ! ô roi d’Ormus ! grâce, grâce à Tarare ! }
\livretPers Atar
\livretVerse#12 { Tarare ! encor Tarare ! Un nom abject et bas, }
\livretVerse#12 { Pour ton organe impur, a donc bien des appas ! }
\livretPers Calpigi
%%# Quand sa troupe nous prit, au fond d'un antre sombre,
%%# Je défendais mes jours contre ces inhumains,
%%# Blessé, prêt à périr, accablé par le nombre;
%%# Cet homme généreux m'arracha de leurs mains.
\livretVerse#12 { Je lui dois d’être à vous, seigneur, faites-lui grâce. }
\livretPers Atar
\livretVerse#12 { Qui, moi, je souffrirais qu’un soldat eût l’audace }
\livretVerse#12 { D’être toujours heureux, quand son roi ne l’est pas ! }
\livretPers Calpigi
\livretVerse#8 { A travers le torrent d’Arsace, }
\livretVerse#8 { Il vous a sauvé du trépas ; }
\livretVerse#12 { Et vous l’avez nommé chef de votre milice. }
\livretPers Atar
\livretVerse#8 { Ah ! combien je l’ai regretté ! }
\livretVerse#8 { Son orgueilleuse humilité, }
\livretVerse#8 { Le respect d’un peuple hébété, }
\livretVerse#12 { Son air, jusqu’à son nom… Cet homme est mon supplice. }
\livretVerse#12 { Où trouve-t-il, dis-moi, cette félicité ? }
\livretVerse#12 { Est-ce dans le travail, ou dans la pauvreté ? }
\livretPers Calpigi
\livretVerse#12 { Dans son devoir. Il sert avec simplicité }
\livretVerse#12 { Le ciel, les malheureux, la patrie et son maître. }
\livretPers Atar
\livretVerse#8 { Lui ? c’est un humble fastueux, }
\livretVerse#8 { Dont l’orgueil est de le paraître : }
\livretVerse#8 { L’honneur d’être cru vertueux }
\livretVerse#8 { Lui tient lieu du bonheur de l’être : }
\livretVerse#8 { Il n’a jamais trompé mes yeux. }
\livretPers Calpigi
\livretVerse#13 { Vous tromper, lui, Tarare ! }
\livretPers Atar
\livretVerse#13 { \transparent { Vous tromper, lui, Tarare ! } Ici la loi des Brames, }
\livretVerse#10 { Permet à tous un grand nombre de femmes ; }
\livretVerse#10 { Il n’en a qu’une, et s’en croit plus heureux, }
\livretVerse#10 { Mais nous l’aurons cet objet de ses vœux ; }
\livretVerse#10 { En la perdant il gémira peut-être. }
\livretPers Calpigi
\livretVerse#12 { Il en mourra ! }
\livretPers Atar
\livretVerse#12 { \transparent { Il en mourra ! } Tant mieux. Oui, le fils du grand-prêtre, }
\livretVerse#12 { Altamort a reçu mon ordre cette nuit. }
\livretVerse#8 { Il vole à la rive opposée, }
\livretVerse#8 { Avec sa troupe déguisée : }
\livretVerse#12 { En son absence il va dévaster son réduit. }
\livretVerse#10 { Il ravira sur-tout son Astasie, }
\livretVerse#12 { Ce miracle, dit-on, des beautés de l’Asie. }
\livretPers Calpigi
\livretVerse#8 { Eh ! quel est donc son crime, hélas ! }
\livretPers Atar
\livretVerse#12 { D’être heureux, Calpigi, quand son roi ne l’est pas, }
\livretVerse#8 { De faire partout ses conquêtes }
\livretVerse#8 { Des cœurs que j’avais autrefois… }
\livretPers Calpigi
\livretVerse#8 { Ah ! pour tourner toutes les têtes, }
\livretVerse#8 { Il faut si peu de chose aux rois ! }
\livretPers Atar
\livretVerse#8 { D’avoir, par un manège habile, }
\livretVerse#8 { Entraîné le peuple imbécile. }
\livretPers Calpigi
\livretRef#'BACcalpigi
\livretVerse#8 { Il est vrai, son nom adoré, }
\livretVerse#8 { Dans la bouche de tout le monde, }
\livretVerse#8 { Est un proverbe révéré. }
\livretVerse#8 { Parle-t-on des fureurs de l’onde, }
\livretVerse#8 { Ou du fléau le plus fatal ; }
\livretVerse#8 { Tarare ! est l’écho général : }
\livretVerse#8 { Comme si ce nom secourable }
\livretVerse#8 { Eloignait, rendait incroyable }
\livretVerse#8 { Le mal, hélas ! le plus certain… }
\livretPersDidas Atar (en colère)
\livretVerse#10 { Finiras-tu, méprisable chrétien ? }
\livretVerse#8 { Eunuque vil et détestable ; }
\livretVerse#12 { La mort devrait… }
\livretPers Calpigi
\livretVerse#12 { \transparent { La mort devrait… } La mort, la mort, toujours la mort ! }
\livretVerse#8 { Ce mot éternel me désole : }
\livretVerse#8 { Terminez une fois mon sort ; }
\livretVerse#8 { Et puis cherchez qui vous console }
\livretVerse#9 { Du triste ennui de la satiété, }
\livretVerse#5 { De l’oisiveté, }
\livretVerse#5 { De la royauté. }
\livretDidasPPage (Il s'eloigne.)
\livretPersDidas Atar (furieux)
\livretVerse#10 { Je punirai cet excès d’arrogance. }

\livretScene SCÈNE DEUXIÈME
\livretDescAtt\wordwrap-center { Les précédents, Altamort. }
\livretPers Atar
\livretRef#'BBArecit
\livretVerse#12 { Mais qu’annonce Altamort, à mon impatience ? }
\livretPers Altamort
\livretVerse#12 { Mon maître est obéi ; tout est fait, rien n’est su. }
\livretPers Atar
\livretVerse#12 { Astasie ? }
\livretPers Altamort
\livretVerse#12 { \transparent { Astasie ? } Est à toi, sans qu’on m’ait apperçu, }
\livretVerse#12 { Sans qu’elle ait deviné qui la veut, qui l’enlève. }
\livretPers Atar
\livretVerse#12 { Au rang de mes vizirs, Altamort, je t’élève. }
\livretDidasPPage (à Calpigi)
\livretVerse#12 { Pour la bien recevoir sont-ils tous préparés ? }
\livretVerse#12 { Le serrail est-il prêt, les jardins décorés, }
\livretVerse#12 { Calpigi ? }
\livretPers Calpigi
\livretVerse#12 { \transparent { Calpigi ? } Tout, Seigneur. }
\livretPers Atar
\livretVerse#12 { \transparent { Calpigi ? Tout, Seigneur. } Qu’une superbe fête, }
\livretVerse#12 { Demain, de ma grandeur enivre ma conquête. }
\livretPers Calpigi
\livretVerse#12 { Demain ? Le terme est court. }
\livretPersDidas Atar (en colère)
\livretVerse#12 { \transparent { Demain ? Le terme est court. } Malheureux ! }
\livretPersDidas Calpigi (víte)
\livretVerse#12 { \transparent { Demain ? Le terme est court. Malheureux ! } Vous l’aurez. }
\livretPers Atar
\livretVerse#13 { J’ai parlé : tu m’entends ? S’il manque quelque chose… }
\livretPers Calpigi
\livretVerse#12 { Manquer ! chacun sait trop à quel mal il s’expose. }

\livretScene SCÈNE TROISIÈME
\livretDescAtt\column {
  \justify {
    Tous les acteurs précédents, Spinette, Odalisques, Esclaves du
    serrail des deux sexes.
  }
  \justify {
    Tout le serrail entre et se range en haie; quatre esclaves noir
    portent Astasie couverte d'un grand voile noir, de la tête aux pieds.
  }
}
\livretPers Chœur d'esclaves du serrail
\livretDidasPPage (On dance pendant le chœur.)
\livretRef#'BCAchoeur
\livretVerse#8 { Dans les plus beaux lieux de l’Asie, }
\livretVerse#8 { Avec la suprême grandeur, }
\livretVerse#8 { L’amour met aux pieds d’Astasie, }
\livretVerse#8 { Tout ce qui donne le bonheur. }
\livretVerse#8 { Ce n’est point dans l’humble retraite, }
\livretVerse#8 { Qu’un cœur généreux le ressent ; }
\livretVerse#8 { Et la beauté la plus parfaite, }
\livretVerse#8 { Doit régner sur le plus puissant. }
\livretPers Atar
\livretDidasPPage (On la dévoile.)
\livretVerse#8 { Que tout s’abaisse devant elle. }
\livretPers Astasie
\livretRef#'BCBrecit
\livretDidasPPage (On se prosterne.)
\livretVerse#10 { O sort affreux, dont l’horreur me poursuit ! }
\livretVerse#8 { Du sein d’une profonde nuit, }
\livretVerse#8 { Quelle clarté triste et nouvelle… }
\livretVerse#8 { Où suis-je ? Tout mon corps chancelle. }
\livretPers Spinette
\livretVerse#12 { Dans le palais d’Atar. }
\livretPers Atar
\livretVerse#12 { \transparent { Dans le palais d’Atar. } Calpigi, qu’elle est belle ! }
\livretPersDidas Astasie (se levant)
\livretVerse#12 { Dans le palais d’Atar ! Ah ! quelle indignité ! }
\livretPersDidas Atar (s’approche)
\livretVerse#13 { D’Atar qui vous adore. }
\livretPers Astasie
\livretVerse#13 { \transparent { D’Atar qui vous adore. } Et c’est la récompense, }
\livretVerse#10 { Ô mon époux, de ta fidélité ! }
\livretPers Atar
\livretVerse#12 { Mes bienfaits laveront cette légère offense. }
\livretPers Astasie
\livretVerse#8 { Quoi, cruel ! par cet attentat, }
\livretVerse#8 { Vous payez la foi d’un soldat }
\livretVerse#8 { Qui vous a conservé la vie ! }
\livretVerse#8 { Vous lui ravissez Astasie ! }
\livretDidasPPage (Levant les mains au ciel.)
\livretVerse#8 { Grand Dieu ! ton pouvoir infini, }
\livretVerse#8 { Laissera-t-il donc impuni }
\livretVerse#8 { Ce crime atroce d’un parjure, }
\livretVerse#8 { Et la plus exécrable injure ! }
\livretVerse#12 { Ô Brama ! Dieu vengeur !… }
\livretDidasPPage\wordwrap {
  (Elle s'évanouit. Des femmes la soutiennent. On l'assied.)
}
\livretPers Calpigi
\livretVerse#12 { \transparent { Ô Brama ! Dieu vengeur !… } Quel effrayant transport ! }
\livretPersDidas Un esclave (accourant)
\livretVerse#12 { Le voile de la mort a couvert sa paupière. }
\livretPers Atar
\livretVerse#10 { Quoi ! malheureux ! tu m’annonces sa mort ! }
\livretVerse#13 { Meurs, toi-même. }
\livretDidasPPage\wordwrap {
  (Il le poignard. Courant vers Astasie.)
}
\livretVerse#13 { \transparent { Meurs, toi-même. } Et vous tous, rendez à la lumière }
\livretVerse#8 { L’objet de mon funeste amour. }
\livretVerse#10 { À sa douleur tremblez qu’il ne succombe ; }
\livretVerse#8 { Répondez-moi de son retour, }
\livretVerse#12 { Ou je lui fais de tous une horrible hécatombe. }
\livretDidasPPage\wordwrap {
  (Revenant à elle apperçoit l'esclave renversé, qu'on enlève.)
}
\livretPers Astasie
\livretVerse#10 { Dieux ! quel spectacle a glacé mes esprits ! }
\livretPers Atar
\livretRef#'BCCatar
\livretVerse#10 { Je suis heureux, vous êtes ranimée. }
\livretVerse#8 { Un lâche esclave par ses cris, }
\livretVerse#8 { M’alarmait sur ma bien-aimée ; }
\livretVerse#10 { De son vil sang la terre est arrosée : }
\livretVerse#8 { Un coup de poignard est le prix }
\livretVerse#8 { De la frayeur qu’il m’a causée. }
\livretPersDidas Astasie (joignant les mains)
\livretVerse#8 { Ô Tarare ! ô Brama ! Brama ! }
\livretDidasPPage (Elle retombe, on l’assied.)
\livretPers Atar
\livretVerse#8 { Dans le serrail qu’on la transporte : }
\livretVerse#8 { Que cent eunuques, à sa porte, }
\livretVerse#8 { Attendent les ordres d’Irza. }
\livretDidasP\justify {
  (Le nom d'Irza signifie: « La plus belle fleur des plus belles fleurs
  ècloses aux premiers soleils du primtems de l'Orient de l'Asie ». Tant
  les langues orientales ont d'avantages sur les nôtres. Lisez les Mille
  et une nuit, et tous le contes arabes.)
}
\livretVerse#10 { C’est le doux nom qu’à ma belle j’impose ; }
\livretVerse#10 { C’est mon Irza, plus fraîche que la rose }
\livretVerse#10 { Que je tenais lorqu’elle m’embrasa. }
\livretDidasPPage\wordwrap {
  (Les esclaves noirs portent Astasie dans le serrail; tous la suivent.)
}

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\wordwrap-center {
  Atar, Calpigi, Altamort, Spinette.
}
\livretPers Calpigi
\livretRef#'BDArecit
\livretVerse#12 { Qui voulez-vous, Seigneur, auprès d’elle qu’on mette ? }
\livretPers Atar
\livretVerse#12 { L’Européane ; allez. }
\livretPers Calpigi
\livretVerse#12 { \transparent { L’Européane ; allez. } L’intrigante Spinette ? }
\livretPers Atar
\livretVerse#13 { Elle-même. }
\livretPers Calpigi
\livretVerse#13 { \transparent { Elle-même. } En effet, nulle ici ne sait mieux }
\livretVerse#12 { Comment il faut réduire un cœur né scrupuleux. }
\livretPers Spinette
\livretRef#'BDBspinette
\livretVerse#8 { Oui, Seigneur, je veux la réduire, }
\livretVerse#8 { Vous livrer son cœur, et l’instruire }
\livretVerse#12 { Du respect, du retour qu’elle doit à vos feux. }
\livretDidasPPage (Montrant Calpigi.)
\livretVerse#8 { Et… si ce grand succès consterne }
\livretVerse#8 { Le chef… puissant qui vous gouverne, }
\livretVerse#12 { Mon maître appréciera le zèle de tous deux. }
\livretPers Atar
\livretVerse#12 { Je l’enchaîne à tes pieds, si tu remplis mes vœux. }
\livretDidasPPage\justify {
  (Spinette et Calpigi sortent en se menaçant.)
}

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center {
  Urson, Atar, Altamort, Esclaves.
}
\livretPers Urson
\livretRef#'BEArecit
\livretVerse#12 { Seigneur, c’est ce guerrier, du peuple la merveille… }
\livretPers Atar
\livretVerse#12 { Garde-toi que son nom offense mon oreille ! }
\livretPers Urson
\livretVerse#12 { Il pleure ; autour de lui tout le peuple empressé }
\livretVerse#12 { Dit tout haut, qu’en ses vœux il doit être exaucé. }
\livretPers Atar
\livretVerse#8 { Tu dis qu’il pleure, qu’il soupire ? }
\livretPers Urson
\livretVerse#8 { Ses traits en sont presque effacés. }
\livretPers Atar
\livretVerse#8 { Urson, qu’il entre ; c’est assez. }
\livretDidasPPage (à Altamort)
\livretVerse#8 { Il est malheureux… Je respire. }

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center {
  Tarare, Altamort, Atar.
}
\livretPers Atar
\livretRef#'BFArecit
\livretVerse#8 { Que me veux-tu, brave soldat ? }
\livretPersDidas Tarare (avec un grand trouble)
\livretVerse#12 { Ô mon roi ! prends pitié de mon affreux état. }
\livretVerse#10 { En pleine paix, un avare corsaire }
\livretVerse#10 { Comble sur moi les horreurs de la guerre. }
\livretVerse#8 { Tous mes jardins sont ravagés, }
\livretVerse#8 { Mes esclaves sont égorgés, }
\livretVerse#8 { L’humble toit de mon Astasie }
\livretVerse#8 { Est consumé par l’incendie… }
\livretPers Atar
\livretVerse#12 { Grâce au Ciel, mes serments vont être dégagés ! }
\livretVerse#8 { Soldat qui m’as sauvé la vie, }
\livretRef#'BFBatar
\livretVerse#8 { Reçois en pur don ce palais }
\livretVerse#8 { Que dix mille esclaves malais }
\livretVerse#8 { Ont construit d’ivoire et d’ébène : }
\livretVerse#8 { Ce palais, dont l’aspect riant }
\livretVerse#8 { Domine la fertile plaine, }
\livretVerse#7 { Et la vaste mer d’orient. }
\livretVerse#8 { Là, cent femmes de Circassie, }
\livretVerse#8 { Pleines d’attraits et de pudeur, }
\livretVerse#8 { Attendront l’ordre de ton cœur, }
\livretVerse#10 { Pour t’enivrer des trésors de l’Asie. }
\livretVerse#12 { Puisse de ton bonheur l’envieux s’irriter ! }
\livretVerse#8 { Puisse l’infame calomnie, }
\livretVerse#8 { Pour te perdre, en vain s’agiter !… }
\livretPersDidas Altamort (bas)
\livretVerse#8 { Mais, seigneur, ta hautesse oublie… }
\livretPersDidas Atar (bas)
\livretVerse#12 { Je l’élève, Altamort, pour le précipiter. }
\livretDidasPPage (haut)
\livretVerse#8 { Allez, vizir, que l’on publie… }
\livretPers Tarare
\livretVerse#12 { Ô mon roi ! ta bonté doit se faire adorer. }
\livretVerse#10 { Des maux du sort mon âme est peu saisie ; }
\livretVerse#12 { Mais celui de mon cœur ne peut se réparer, }
\livretVerse#8 { Le barbare emmène Astasie. }
\livretPersDidas Atar (avec un signe d'intelligence)
\livretVerse#8 { Quelle est cette femme, Altamort ? }
\livretPers Altamort
\livretVerse#8 { Seigneur, si j’en crois son transport, }
\livretVerse#8 { Quelque esclave jeune et jolie. }
\livretPers Tarare
\livretVerse#12 { Une esclave ! une esclave ! excuse, ô roi d’Ormus ! }
\livretVerse#12 { A ce nom odieux tous mes sens sont émus. }
\livretRef#'BFCtarare
\livretVerse#8 { Astasie est une déesse. }
\livretVerse#8 { Dans mon cœur souvent combattu, }
\livretVerse#8 { Sa voix sensible, enchanteresse, }
\livretVerse#8 { Faisait triompher la vertu. }
\livretVerse#8 { D’une ardeur toujours renaissante, }
\livretVerse#8 { J’offrais sans cesse à sa beauté, }
\livretVerse#8 { Sans cesse à sa beauté touchante, }
\livretVerse#8 { L’encens pur de la volupté. }
\livretVerse#8 { Elle tenait mon âme active }
\livretVerse#8 { Jusque dans le sein du repos : }
\livretVerse#8 { Ah ! faut-il que ma voix plaintive }
\livretVerse#8 { En vain la demande aux échos ? }
\livretPers Atar
\livretRef#'BFDrecit
\livretVerse#8 { Quoi ! soldat ! pleurer une femme ! }
\livretVerse#8 { Ton roi ne te reconnaît pas. }
\livretVerse#8 { Si tu perds l’objet de ta flamme, }
\livretVerse#8 { Tout un serrail t’ouvre ses bras. }
\livretVerse#8 { Pour une beauté, quelques charmes, }
\livretVerse#8 { On peut retrouver mille attraits, }
\livretVerse#8 { Mais l’honneur qu’on perd dans les larmes, }
\livretVerse#8 { On ne le retrouve jamais ! }
\livretPersDidas Tarare (suppliant)
\livretVerse#12 { Seigneur ! }
\livretPers Atar
\livretRef#'BFEatar
\livretVerse#12 { \transparent { Seigneur ! } Qu’as-tu donc fait de ton mâle courage ? }
\livretVerse#10 { Toi qu’on voyait rugir dans les combats, }
\livretVerse#10 { Toi qui forças un torrent à la nage, }
\livretVerse#10 { En transportant ton maître dans tes bras ! }
\livretVerse#10 { Le fer, le feu, le sang et le carnage }
\livretVerse#10 { N’ont jamais pu t’arracher un soupir ; }
\livretVerse#10 { Et l’abandon d’une esclave volage }
\livretVerse#10 { Abat ton âme et la force à gémir ! }
\livretPersDidas Tarare (vivement)
\livretRef#'BFFtarare
\livretVerse#8 { Seigneur, si j’ai sauvé ta vie, }
\livretVerse#8 { Si tu daignes t’en souvenir, }
\livretVerse#8 { Laisse-moi venger Astasie }
\livretVerse#8 { Du traître qui l’osa ravir. }
\livretVerse#8 { Permets que, déployant ses ailes, }
\livretVerse#8 { Un léger vaisseau de transport }
\livretVerse#8 { Me mène vers ces infidèles, }
\livretVerse#8 { Chercher Astasie ou la mort. }

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\wordwrap-center {
  Calpigi, Atar, Altamort, Tarare.
}
\livretPers Atar
\livretRef#'BGArecit
\livretVerse#12 { Que veux-tu, Calpigi ? }
\livretDidasPPage (bas)
\livretVerse#12 { \transparent { Que veux-tu, Calpigi ? } Sois inintelligible. }
\livretPers Calpigi
\livretVerse#12 { Mon maître, cette Irza si chère à ton amour… }
\livretPers Atar
\livretVerse#12 { Eh bien ? }
\livretPers Calpigi
\livretVerse#12 { \transparent { Eh bien ? } Elle est rendue à la clarté du jour. }
\livretPersDidas Tarare (exalté)
\livretVerse#8 { Atar, ta grande âme est sensible, }
\livretVerse#8 { La joie a brillé dans tes yeux. }
\livretDidasPPage (Un genou en terre.)
\livretVerse#10 { Par cette Irza, sultan, sois généreux, }
\livretVerse#8 { À mes maux deviens accessible. }
\livretPers Atar
\livretVerse#10 { Dis-moi, Tarare, es-tu bien malheureux ? }
\livretPers Tarare
\livretVerse#10 { Si je le suis ! ah ! peut-être elle expire ! }
\livretPers Atar
\livretVerse#12 { Souhaite devant moi qu’Irza cède à mes vœux : }
\livretVerse#8 { Je fais ce que ton cœur désire. }
\livretPersDidas Calpigi (à part)
\livretVerse#8 { Grand dieux ! je sers un homme affreux ! }
\livretPersDidas Tarare (se levant, dit avec feu)
\livretRef#'BGBtarare
\livretVerse#10 { Charmante Irza, qu’est-ce donc qui t’arrête ? }
\livretVerse#10 { Le fils des dieux n’est-il pas ta conquête ? }
\livretVerse#8 { Puisse-t-il trouver dans tes yeux }
\livretVerse#8 { Ce pur feu dont il étincelle ! }
\livretVerse#8 { Rends, Irza, rends mon maître heureux… }
\livretDidasPPage\wordwrap {
  (Calpigi lui fait un signe négatif pour qu'il n'achève pas son vœu.)
}
\livretVerse#10 { …si tu le peux sans etre criminelle. }
\livretPers Atar
\livretRef#'BGCatar
\livretVerse#10 { Brave Altamort, avant le point du jour, }
\livretVerse#8 { Demain qu’une escadre soit prête }
\livretVerse#8 { À partir du pied de la tour. }
\livretVerse#8 { Suis mon soldat, sers son amour }
\livretVerse#8 { Dans les combats, dans la tempête. }
\livretDidasPPage (Bas à Altamort.)
\livretVerse#8 { S’il revoit jamais ce séjour, }
\livretVerse#8 { Tu m’en répondras sur ta tete. }
\livretDidasPPage (à Tarare.)
\livretVerse#8 { Et toi, jusqu’à cette conquete, }
\livretVerse#8 { De tout service envers ton roi, }
\livretVerse#8 { Soldat, je dégage ta foi ; }
\livretVerse#12 { J’en jure par Brama. }
\livretPersDidas Tarare (la main au sabre)
\livretVerse#12 { \transparent { J’en jure par Brama. } Je jure en sa présence, }
\livretVerse#8 { De ne poser ce fer sanglant, }
\livretVerse#10 { Qu’après avoir, du plus lâche brigand, }
\livretVerse#10 { Puni le crime, et vengé mon offense. }
\livretPersDidas Atar (à Atamort)
\livretVerse#8 { Tu viens d’entendre son serment ; }
\livretVerse#8 { Il touche a plus d’une existence : }
\livretVerse#10 { Vole, Altamort, et plus prompt que le vent, }
\livretVerse#10 { Reviens jouir de ma reconnaissance. }
\livretPers Altamort
\livretVerse#8 { Noble roi, reçois le serment }
\livretVerse#8 { De ma plus prompte obéissance. }
\livretVerse#10 { Commande, Atar, je cours aveuglément }
\livretVerse#10 { Servir l’amour, la haine ou la vengeance. }
\livretPersDidas Calpigi à part
\livretVerse#8 { De son danger, secrètement, }
\livretVerse#8 { Il faut lui donner connaissance. }
\livretDidasPPage\wordwrap {
  (Atar le regarde. Calpigi dit d'un ton courtisan.)
}
\livretVerse#10 { Qui sert mon maître, et le sert prudemment, }
\livretVerse#10 { peut bien compter sur sa munificence. }
\livretDidasPPage (Ils sortent tous.)

\livretScene SCÈNE HUITIÈME
\livretDescAtt\wordwrap-center { Atar seul. }
\livretRef#'BHAatar
\livretVerse#6 { Vertu farouche et fière, }
\livretVerse#6 { Qui jetait trop d’éclat, }
\livretVerse#6 { Rentre dans la poussière, }
\livretVerse#6 { Faite pour un soldat. }
\livretVerse#12 { Du crime d’Altamort je vois la mer chargée, }
\livretVerse#12 { Rendre à ton corps sanglant les funèbres honneurs. }
\livretVerse#12 { Et nous, heureux Atar, de ma belle affligée, }
\livretVerse#12 { Dans la joie et l’amour, nous sécherons les pleurs. }
\livretDidasPPage (Il sort.)
\sep
\livretAct ACTE DEUXIÈME
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\column {
  \justify {
    Le théâtre représente la place publique. Le palais d’Atar est sur le côté ;
    le temple de Brama, dans le fond. Atar sort de son palais avec toute sa suite.
    Urson sort du temple, suivi d’Arthenée en habits pontificaux.
  }
  \wordwrap-center { Urson, Atar. }
}
\livretPers Urson
\livretRef#'CAArecit
\livretVerse#8 { Seigneur, le grand-prêtre Arthénée }
\livretVerse#8 { Demande un entretien secret. }
\livretPersDidas Atar (à sa suite)
\livretVerse#12 { Eloignez-vous… Qu’il vienne. Urson, que nul sujet, }
\livretVerse#8 { Dans cette agréable journée, }
\livretVerse#12 { D’un seul refus d’Atar n’emporte le regret. }

\livretScene SCÈNE DEUXIÈME
\livretDescAtt\wordwrap-center { Arthénée, Atar. }
\livretDidasP\wordwrap { Tout le monde s’éloigne du roi. }
\livretPersDidas Arthénée (s’avance)
\livretRef#'CBArecit
\livretVerse#8 { Les sauvages d’un autre monde, }
\livretVerse#8 { Menacent d’envahir ces lieux ; }
\livretVerse#8 { Au loin déjà la foudre gronde ; }
\livretVerse#7 { Ton peuple superstitieux, }
\livretVerse#8 { Pressé comme les flots, innonde }
\livretVerse#8 { Le parvis sacré de nos dieux. }
\livretPers Atar
\livretVerse#8 { De vils brigands une poignée, }
\livretVerse#8 { Sortant d’une terre éloignée, }
\livretVerse#8 { Pourrait-elle envahir ces lieux ? }
\livretVerse#8 { Pontife, votre âme étonnée… }
\livretVerse#8 { Cependant, parlez, Arthénée, }
\livretVerse#8 { Que dit l’interprète des dieux ? }
\livretPersDidas Arthénée (vivement)
\livretRef#'CBBarthenee
\livretVerse#4 { Qu’il faut combattre, }
\livretVerse#4 { Qu’il faut abattre }
\livretVerse#8 { Un ennemi présomptueux : }
\livretVerse#4 { Le sol aride }
\livretVerse#4 { De la Torride }
\livretVerse#8 { A soif de son sang odieux. }
\livretVerse#4 { Par des mesures }
\livretVerse#4 { Promptes et sûres, }
\livretVerse#8 { Que l’armée ait un commandant, }
\livretVerse#4 { Vaillant, fidèle, }
\livretVerse#4 { Rempli de zèle : }
\livretVerse#8 { Mais sur ce devoir important, }
\livretVerse#4 { Que le caprice }
\livretVerse#4 { De ta milice }
\livretVerse#8 { Ne règle point le choix d’Atar : }
\livretVerse#4 { Que le murmure, }
\livretVerse#4 { Comme une injure, }
\livretVerse#8 { Soit puni d’un coup de poignard. }
\livretPers Atar
\livretRef#'CBCrecit
\livretVerse#8 { Apprends-moi donc, ô chef des Brames ! }
\livretVerse#8 { Ce qu’Atar doit penser de toi. }
\livretVerse#8 { Ardent zélateur de la foi }
\livretVerse#8 { Du passage éternel des âmes ! }
\livretVerse#12 { Le plus vil animal est nourri de ta main ; }
\livretVerse#8 { Tu craindrais d’en purger la terre ! }
\livretVerse#10 { Et cependant, tu brûles, dans la guerre, }
\livretVerse#10 { De voir couler des flots de sang humain ! }
\livretPers Arthénée
\livretRef#'CBDarthenee
\livretVerse#8 { Ah ! d’une antique absurdité, }
\livretVerse#8 { Laissons à l’hindou les chimères. }
\livretVerse#8 { Brame et Soudan doivent en frères }
\livretVerse#8 { Soutenir leur autorité. }
\livretVerse#8 { Tant qu’ils s’accordent bien ensemble, }
\livretVerse#8 { Que l’esclave ainsi garrotté, }
\livretVerse#8 { Souffre, obéit, et croit, et tremble, }
\livretVerse#8 { Le pouvoir est en sûreté. }
\livretPers Atar
\livretVerse#8 { Dans ta politique nouvelle, }
\livretVerse#12 { Comment mes intérêts sont-ils unis aux tiens ? }
\livretPers Arthénée
\livretVerse#8 { Ah ! si ta couronne chancelle, }
\livretVerse#8 { Mon temple, à moi, tombe avec elle. }
\livretVerse#8 { Atar, ces farouches chrétiens }
\livretVerse#8 { Auront des dieux jaloux des miens : }
\livretVerse#8 { Ainsi qu’au trône, tout partage, }
\livretVerse#8 { En fait de culte, est un outrage. }
\livretVerse#10 { Pour les dompter, fais que nos Indiens }
\livretVerse#12 { Pensent que le ciel même a conduit nos mesures : }
\livretVerse#10 { Le nom du chef dont nous serons d’accord, }
\livretVerse#10 { Je l’insinue aux enfants des augures. }
\livretVerse#8 { Qui veux-tu nommer ? }
\livretPers Atar
\livretVerse#8 { \transparent { Qui veux-tu nommer ? } Altamort. }
\livretPers Arthénée
\livretVerse#8 { Mon fils ! }
\livretPers Atar
\livretVerse#8 { \transparent { Mon fils ! } J’acquitte un grand service. }
\livretPers Arthénée
\livretVerse#9 { Que devient Tarare ? }
\livretPers Atar
\livretVerse#9 { \transparent { Que devient Tarare ? } Il est mort. }
\livretPers Arthénée
\livretVerse#12 { Il est mort ! }
\livretPers Atar
\livretVerse#12 { \transparent { Il est mort ! } Oui, demain, j’ordonne qu’il périsse. }
\livretPers Arthénée
\livretVerse#6 { Juste ciel ! crains, Atar… }
\livretPers Atar
\livretVerse#6 { Quoi craindre ? mes remords ? }
\livretPers Arthénée
\livretRef#'CBEarthenee
\livretVerse#8 { Crains de payer de ta couronne, }
\livretVerse#8 { Un attentat sur sa personne. }
\livretVerse#8 { Ses soldats seraient les plus forts. }
\livretVerse#8 { Si, sur un prétexte frivole, }
\livretVerse#8 { Tu les prives de leur idole, }
\livretVerse#8 { Cette milice, en sa fureur, }
\livretVerse#11 { Peut, oubliant ton rang et ta naissance… }
\livretPers Atar
\livretVerse#10 { J’ai tout prévu ; Tarare, dans l’erreur, }
\livretVerse#10 { Court à sa perte en cherchant la vengeance. }
\livretRef#'CBFatar
\livretVerse#8 { Qu’une grande solennité }
\livretVerse#8 { Rassemble ce peuple agité ; }
\livretVerse#8 { De ses cris et de ses murmures }
\livretVerse#8 { Montre-lui le ciel irrité. }
\livretVerse#8 { Prépare ensuite les augures ; }
\livretVerse#8 { Et par d’utiles impostures }
\livretVerse#8 { Consacrons notre autorité. }
\livretDidasPPage (Il sort.)

\livretScene SCÈNE TROISIÈME
\livretPersDidas Arthénée seul
\livretRef#'CCAarthenee
\livretVerse#8 { Ô politique consommée ! }
\livretVerse#8 { Je tiens le secret de l’État ; }
\livretVerse#8 { Je fais mon fils chef de l’armée ; }
\livretVerse#8 { À mon temple je rend l’éclat, }
\livretVerse#8 { Aux augures leur renommée. }
\livretVerse#8 { Pontifes, pontifes adroits ! }
\livretVerse#8 { Remuez le cœur de vos rois. }
\livretVerse#5 { Quand les rois craignent, }
\livretVerse#5 { Les Brames règnent ; }
\livretVerse#8 { La tiare agrandit ses droits. }
\livretVerse#12 { Eh ! qui sait si mon fils, un jour maître du monde… }
\livretDidasPPage\wordwrap {
  (Il voit arriver Tarare; il rentre dans le temple.)
}

\livretScene SCÈNE QUATRIÈME
\livretPersDidas Tarare seul (il rêve).
\livretRef#'CDAtarare
\livretVerse#12 { De quel nouveau malheur suis-je encor menacé ? }
\livretVerse#12 { Ô Brama ! tire-moi de cette nuit profonde. }
\livretVerse#8 { Ce matin, quand j’ai prononcé : }
\livretVerse#8 { Qu’à son amour Irza réponde, }
\livretVerse#8 { Un signe effrayant m’a glacé… }
\livretVerse#12 { De quel nouveau malheur suis-je encor menacé ? }
\livretVerse#12 { Ô Brama ! tire-moi de cette nuit profonde. }

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center { Calpigi, Tarare. }
\livretPersDidas Calpigi \line { (déguisé, couvert d’une cape, l’ouvre) }
\livretRef#'CEArecit
\livretVerse#12 { Tarare ! connais-moi. }
\livretPers Tarare
\livretVerse#12 { \transparent { Tarare ! connais-moi. } Calpigi ! }
\livretPersDidas Calpigi (vivement)
\livretVerse#12 { \transparent { Tarare ! connais-moi. Calpigi ! } Mon héros ! }
\livretVerse#12 { Je te dois mon bonheur, ma fortune, ma vie. }
\livretVerse#12 { Que ne puis-je à mon tour te rendre le repos ! }
\livretRef#'CEBcalpigi
\livretVerse#8 { Cette belle et tendre Astasie }
\livretVerse#8 { Que tu vas chercher au hasard }
\livretVerse#8 { Sur le vaste océan d’Asie, }
\livretVerse#8 { Elle est dans le serrail d’Atar, }
\livretVerse#10 { Sous le faux nom d’Irza… }
\livretPers Tarare
\livretVerse#10 { \transparent { Sous le faux nom d’Irza… } Qui l’a ravie ? }
\livretPers Calpigi
\livretVerse#10 { C’est Altamort. }
\livretPers Tarare
\livretVerse#10 { \transparent { C’est Altamort. } Ô lâche perfidie ! }
\livretPers Calpigi
\livretVerse#12 { Le golfe où nos plongeurs vont chercher le corail, }
\livretVerse#8 { Baigne les jardins du serrail : }
\livretVerse#10 { Si, dans la nuit, ton courage inflexible }
\livretVerse#12 { Ose de cette route affronter le danger, }
\livretVerse#8 { De soie une échelle invisible, }
\livretVerse#8 { Tendue à l’angle du verger… }
\livretPers Tarare
\livretVerse#8 { Ami généreux, secourable… }
\livretPers Calpigi
\livretVerse#8 { Le temple s’ouvre, adieu. }
\livretDidasPPage\line { (Il s’enveloppe et s’enfuit.) }

\livretScene SCÈNE SIXIÈME
\livretPersDidas Tarare seul.
\livretRef#'CFAtarare
\livretVerse#8 { \transparent { Le temple s’ouvre, adieu. } J’irai : }
\livretVerse#4 { Oui, j’oserai : }
\livretVerse#8 { Pour la revoir je franchirai }
\livretVerse#8 { Cette barrière impénétrable. }
\livretVerse#8 { De ton repaire, affreux vautour ! }
\livretVerse#8 { J’irai l’arracher morte ou vive ; }
\livretVerse#8 { Et si je succombe au retour, }
\livretVerse#10 { Ne me plains pas, tyran, quoiqu’il m’arrive : }
\livretVerse#8 { Celui qui te sauva le jour }
\livretVerse#8 { A bien mérité qu’on l’en prive ! }

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\column {
  \justify {
    Le fond du théâtre qui représentait le portail du temple de Brama,
    se retire, et laisse voir l’intérieur du temple, qui se forme
    jusqu’au-devant du théâtre.
  }
  \wordwrap-center {
    Arthénée, les Prêtres de Brama, Elamir et les autres enfants des augures.
  }
}
\livretPersDidas Arthénée (aux Prêtres)
\livretRef#'CGArecit
\livretVerse#12 { Sur un choix important le ciel est consulté. }
\livretVerse#12 { Vous, préparez l’autel ; vous, les fleurs les plus pures ; }
\livretVerse#12 { Vous, choisissez parmi les enfants des augures, }
\livretVerse#12 { Celui pour qui Brama s’est plus manifesté, }
\livretVerse#12 { En le douant d’un cœur plein de simplicité. }
\livretPers Un Prêtre
\livretVerse#12 { C’est le jeune Elamir. Il vient à vous. }
\livretPersDidas Elamir (accourant)
\livretVerse#12 { \transparent { C’est le jeune Elamir. Il vient à vous. } Mon père ! }
\livretPersDidas Arthénée (s’assied)
\livretVerse#12 { Approchez-vous, mon fils ; un grand jour vous éclaire. }
\livretVerse#12 { Croyez-vous que Brama vous parle par ma voix, }
\livretVerse#12 { Et qu’il parle à moi seul ? }
\livretPers Elamir
\livretVerse#12 { \transparent { Et qu’il parle à moi seul ? } Mon père, oui, je le crois. }
\livretPersDidas Arthénée (sévèrement)
\livretVerse#12 { Le Ciel choisit par vous un vengeur à l’empire : }
\livretVerse#12 { Ne dites rien, mon fils, que ce qu’il vous inspire. }
\livretDidasPPage\line { (d’un ton caressant) }
\livretVerse#12 { Ah ! s’il vous inspirait de nommer Altamort ! }
\livretVerse#12 { L’état serait vainqueur, il vous devrait son sort ! }
\livretPersDidas Elamir \line { (les mains croisées sur sa poitrine) }
\livretVerse#8 { Je l’en supplierai tant, mon père, }
\livretVerse#8 { Qu’il me l’inspirera, j’espère. }
\livretPers Arthénée
\livretVerse#12 { Moi je l’espère aussi : priez-le avec transport. }
\livretDidasPPage\line { (Elamir se prosterne.)	}
\livretRef#'CGBarthenee
\livretVerse#5 { Ainsi qu’une abeille, }
\livretVerse#5 { Qu’un beau jour éveille, }
\livretVerse#5 { De la fleur vermeille }
\livretVerse#5 { Attire le miel ; }
\livretVerse#5 { Un enfant fidèle, }
\livretVerse#5 { Quand Brama l’appelle, }
\livretVerse#5 { S’il prie avec zèle, }
\livretVerse#5 { Obtient tout du ciel. }
\livretRef#'CGCrecit
\livretDidasPPage\line { (Il relève l’enfant.) }
\livretVerse#12 { Tout le peuple, mon fils, sous nos voûtes arrive. }
\livretVerse#8 { Avant de nommer son vengeur, }
\livretVerse#12 { Vous le ferez rougir de sa vaine terreur. }
\livretVerse#8 { Il croit les chrétiens sur la rive ; }
\livretVerse#8 { Assurez-le qu’ils sont bien loin ; }
\livretVerse#12 { Et du reste, mon fils, Brama prendra le soin. }

\livretScene SCÈNE HUITIÈME
\livretDescAtt\wordwrap-center {
  Atar, Altamort, Tarare, Urson, Arthénée, Elamir, Prètres, Enfants,
  Visirs, Emirs, Suite, Peuple, Saldats, Esclaves.
}
\livretRef#'CHAmarche
\livretDidasPage Grande marche.
\livretPersDidas Arthénée (majestueusement)
\livretRef#'CHBarthenee
\livretVerse#12 { Prêtres du grand Brama ! Roi du Golfe Persique ! }
\livretVerse#12 { Grands de l’empire ! peuple inondant le portique ! }
\livretVerse#12 { La nation, l’armée attend un général. }
\livretPers Chœur universel
\livretVerse#8 { Pour nous préserver d’un grand mal, }
\livretVerse#8 { Que le choix de Brama s’explique ! }
\livretPers Arthénée
\livretVerse#8 { Vous promettez tous d’obéir }
\livretVerse#8 { Au chef que Brama va choisir ? }
\livretPers Chœur universel
\livretVerse#10 { Nous le jurons sur cet autel antique. }
\livretPersDidas Arthénée (d’un ton inspiré)
\livretVerse#8 { Dieu sublime dans le repos, }
\livretVerse#8 { Magnifique dans la tempête, }
\livretVerse#10 { Soit que ton souffle élève aux cieux les flots, }
\livretVerse#8 { Soit que ton regard les arrête ; }
\livretVerse#8 { Permets que le nom d’un héros, }
\livretVerse#8 { Sortant d’une bouche innocente, }
\livretVerse#8 { Devienne cher à ses rivaux ; }
\livretVerse#12 { Et porte à l’ennemi le trouble et l’épouvante ! }
\livretDidasPPage (à Elamir.)
\livretVerse#10 { Et vous, enfant, par le ciel inspiré ! }
\livretVerse#12 { Nommez, nommez sans crainte un héros préféré. }
\livretDidasPPage\line { (On élève Elamir sur des pavois.) }
\livretPersDidas Elamir (avec enthusiasme)
\livretVerse#8 { Peuple que la terreur égare, }
\livretVerse#12 { Qui vous fait redouter ces sauvages chrétiens ? }
\livretVerse#8 { L’État manque-t-il de soutiens ? }
\livretVerse#13 { Comptez, aux pieds du roi, vos défenseurs, Tarare… }
\livretPers Chœur
\livretVerse#8 { Tarare ! Tarare ! Tarare ! }
\livretVerse#8 { Ah ! pour nous Brama se déclare : }
\livretVerse#8 { L’enfant vient de nommer Tarare. }
\livretVerse#8 { Tarare ! Tarare ! Tarare ! }
\livretPersDidas Altamort (en colère)
\livretVerse#8 { Arrêtez ce fougueux transport ! }
\livretPers Arthénée
\livretVerse#6 { Peuple, c’est une erreur ! }
\livretDidasPPage (à Elamir)
\livretVerse#6 { Mon fils, que Dieu vous touche ! }
\livretPers Elamir
\livretVerse#8 { Le ciel m’inspirait Altamort ; }
\livretVerse#8 { Tarare est sorti de ma bouche. }
\livretPers Deux Coryphées de soldats
\livretVerse#8 { Par l’enfant, Tarare indiqué, }
\livretVerse#8 { N’est point un hasard sans mystère. }
\livretVerse#8 { Plus son choix est involontaire, }
\livretVerse#8 { Plus le vœu du ciel est marqué. }
\livretVerse#8 { Oui, pour nous Brama se déclare ; }
\livretVerse#8 { L’enfant vient de nommer Tarare. }
\livretPers Chœur du peuple et des soldats
\livretVerse#8 { Tarare ! Tarare ! Tarare ! }
\livretDidasPPage (On redescend Elamir.)
\livretPersDidas Atar (se lève)
\livretVerse#12 { Tarare est retenu par un premier serment : }
\livretVerse#8 { Son grand cœur s’est lié d’avance }
\livretVerse#8 { À suivre une juste vengeance. }
\livretPersDidas Tarare (la main sur sa poitrine)
\livretVerse#12 { Seigneur, je remplirai le double engagement }
\livretVerse#10 { De la vengeance et du commandement. }
\livretDidasPPage (au peuple)
\livretRef#'CHCtarare
\livretVerse#4 { Qui veut la gloire, }
\livretVerse#4 { À la victoire }
\livretVerse#4 { Vole avec moi. }
\livretPers Tous
\livretVerse#4 { C’est moi, c’est moi. }
\livretPers Tarare
\livretVerse#4 { Sujets, esclaves, }
\livretVerse#4 { Que les plus braves }
\livretVerse#4 { Donnent leur foi. }
\livretPers Tous
\livretVerse#4 { C’est moi, c’est moi. }
\livretPers Tarare
\livretVerse#4 { Ni paix, ni trêve, }
\livretVerse#4 { L’horreur du glaive }
\livretVerse#4 { Fera la loi. }
\livretPers Tous
\livretVerse#4 { C’est moi, c’est moi. }
\livretPers Tarare
\livretVerse#4 { Qui veut la gloire, }
\livretVerse#4 { À la victoire }
\livretVerse#4 { Vole avec moi. }
\livretPers Tous
\livretVerse#4 { C’est moi, c’est moi. }
\livretPersDidas Atar (à part)
\livretRef#'CHDrecit
\livretVerse#12 { Je ne puis soutenir la clameur importune }
\livretVerse#8 { D’un peuple entier sourd à ma voix. }
\livretDidasPPage (Il veut descendre.)
\livretPersDidas Altamort (l’arrête)
\livretVerse#12 { Ce choix est une injure à tous tes chefs commune ; }
\livretVerse#8 { Il attaque nos premiers droits. }
\livretVerse#8 { L’arrogant soldat de fortune }
\livretVerse#8 { Doit-il aux grands dicter des lois ? }
\livretPersDidas Tarare (fièrement)
\livretRef#'CHEtarare
\livretVerse#8 { Apprends, fils orgueilleux des prêtres ! }
\livretVerse#8 { Qu’élevé parmi les soldats, }
\livretVerse#8 { Tarare avait, au lieu d’ancêtres, }
\livretVerse#8 { Déjà vaincu dans cent combats ; }
\livretDidasPPage (avec un grand dédain.)
\livretVerse#8 { Qu’Altamort enfant, dans la plaine, }
\livretVerse#8 { Poursuivait les fleurs des chardons, }
\livretVerse#8 { Que les zéphyrs, de leur haleine, }
\livretVerse#8 { Font voler au sommet des monts. }
\livretPersDidas Altamort (la main au sabre)
\livretRef#'CHFrecit
\livretVerse#13 { Sans le respect d’Atar, vil objet de ma haine… }
\livretPersDidas Tarare (bien dédaigneux)
\livretVerse#12 { Du destin de l’État tu prétends décider ! }
\livretVerse#12 { Fougueux adolescent, qui veux nous commander ! }
\livretVerse#10 { Pour titre ici n’as-tu que des injures ? }
\livretVerse#10 { Quels ennemis t’a-t-on vu terrasser ? }
\livretVerse#8 { Quels torrents osas-tu passer ? }
\livretVerse#8 { Où sont tes exploits, tes blessures ? }
\livretPersDidas Altamort (en fureur)
\livretVerse#12 { Toi, qui de ce haut rang brûles de t’approcher, }
\livretVerse#12 { Apprends que sur mon corps il te faudra marcher. }
\livretDidasPPage (Il tire son sabre.)
\livretPersDidas Arthénée (troublé)
\livretVerse#8 { Ô désespoir ! ô frénésie ! }
\livretVerse#12 { Mon fils !… }
\livretPersDidas Altamort (plus furieux)
\livretVerse#12 { \transparent { Mon fils !… } A ce brigand j’arracherai la vie. }
\livretPersDidas Tarare (froidement)
\livretVerse#8 { Calme ta fureur, Altamort. }
\livretVerse#8 { Ce sombre feu, quand il s’allume, }
\livretVerse#8 { Détruit les forces, nous consume : }
\livretVerse#8 { Le guerrier, en colère, est mort. }
\livretDidasPPage (Il tire son sabre)
\livretPersDidas Arthénée (s’écrie)
\livretVerse#12 { Le temple de nos dieux est-il donc une arène ? }
\livretPersDidas Atar (se lève)
\livretVerse#12 { Arrêtez. }
\livretPers Tarare
\livretVerse#12 { \transparent { Arrêtez. } J’obéis… }
\livretDidasPPage\line { (à Altamort, lui prenant la main.) }
\livretVerse#12 { \transparent { Arrêtez. J’obéis… } Toi, ce soir, à la plaine. }
\livretDidasPPage\wordwrap {
  (à Calpigi, à part, pendant qu’Atar descend de son trône)
}
\livretVerse#12 { Et toi, fidèle ami, sans fanal et sans bruit, }
\livretVerse#12 { Au verger du sérail attends-moi cette nuit. }
\livretDidasPPage\wordwrap {
  Atar lui remet le bâton de commandement, au bruit d'une fanfare.
}
\livretDidas Grande Marche pour sortir.
\livretPersDidas Chœur général (sur le chant de la marche)
\livretRef#'CHGchoeur
\livretVerse#8 { Brama ! si la vertu t’es chère, }
\livretVerse#8 { Si la voix du peuple est ta voix, }
\livretVerse#8 { Par des succès soutiens le choix }
\livretVerse#8 { Que le peuple entier vient de faire ! }
\livretVerse#4 { Que sur ses pas }
\livretVerse#4 { Tous nos soldats }
\livretVerse#8 { Marchent d’une audace plus fière ! }
\livretVerse#8 { Que l’ennemi, triste, abattu, }
\livretVerse#8 { Par son aspect déjà vaincu, }
\livretVerse#8 { Sous nos coups morde la poussière ! }
\sep
\livretAct ACTE TROISIÈME
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\column {
  \justify {
    Le théâtre représente les jardins du sérail ; l’appartament d’Irza est à droite ;
    à gauche, et sur le devant, est un grand sopha sous un dais superbe,
    au milieu d’un parterre illuminé. Il est nuit.
  }
  \justify {
    Calpigi, entre d’un coté ; Atar, Urson entrent de l’autre ;
    des Jardiniers ou Bostangis qui allument.
  }
}
\livretPersDidas Calpigi (sans voir Atar)
\livretRef#'DAArecit
\livretVerse#12 { Les jardins éclairés ! des bostangis ! pourquoi ? }
\livretVerse#12 { Quel autre ose au sérail donner des ordres ?… }
\livretPersDidas Atar (lui frappant sur l’épaule)
\livretVerse#12 { \transparent { Quel autre ose au sérail donner des ordres ?… } Moi. }
\livretPersDidas Calpigi (troublé)
\livretVerse#12 { Seigneur… puis-je savoir ?… }
\livretPers Atar
\livretVerse#12 { \transparent { Seigneur… puis-je savoir ?… } Ma fête à ce que j’aime ? }
\livretPers Calpigi
\livretVerse#12 { Est fixée à demain ; seigneur, c’est votre loi. }
\livretPersDidas Atar (brusquement)
\livretVerse#8 { Moi, je la veux à l’instant même. }
\livretPers Calpigi
\livretVerse#8 { Tous mes acteurs sont dispersés. }
\livretPersDidas Atar (plus brusquement)
\livretVerse#12 { Du bruit autour d’Irza ; qu’on danse, et c’est assez. }
\livretPersDidas Calpigi (à part, avec douleur)
\livretVerse#12 { Ô l’affreux contre-temps ! De cet ordre bizarre, }
\livretVerse#12 { Il n’est aucun moyen de prévenir Tarare ! }
\livretPersDidas Atar (l’examinant)
\livretVerse#12 { Quel est donc ce murmure inquiet et profond ? }
\livretPersDidas Calpigi (affecte un air gai)
\livretVerse#12 { Je dis… qu’il semble voir ces spectacles de France, }
\livretVerse#8 { Où tout va bien, pourvu qu’on danse. }
\livretPersDidas Atar (en colère)
\livretVerse#12 { Vil chrétien ! obéis ; ou ta tête en répond. }
\livretPersDidas Calpigi (à part, en s’en allant)
\livretVerse#13 { Tyran féroce ! }
\livretDidasPPage\line { (Les bostangis se retirent.) }

\livretScene SCÈNE DEUXIÈME
\livretDescAtt\wordwrap-center { Atar, Urson. }
\livretPers Atar
\livretRef#'DBArecit
\livretVerse#13 { \transparent { Tyran féroce ! } Avant que ma fête commence, }
\livretVerse#8 { Urson, conte-moi promptement }
\livretVerse#8 { Le détail et l’événement }
\livretVerse#8 { De leur combat à toute outrance. }
\livretPers Urson
\livretRef#'DBBurson
\livretVerse#12 { Tarare le premier arrive au rendez-vous : }
\livretVerse#8 { Par quelques passes dans la plaine, }
\livretVerse#8 { Il met son cheval en haleine, }
\livretVerse#8 { Et vient converser avec nous. }
\livretVerse#8 { Sa contenance est noble et fière. }
\livretVerse#8 { Un long nuage de poussière }
\livretVerse#8 { S’avance du côté du nord ; }
\livretVerse#8 { On croit voir une armée entière. }
\livretVerse#8 { C’est l’impétueux Altamort. }
\livretVerse#8 { D’esclaves armés un grand nombre, }
\livretVerse#8 { Au galop à peine le suit. }
\livretVerse#8 { Son aspect est farouche et sombre, }
\livretVerse#8 { Comme les spectres de la nuit. }
\livretVerse#10 { D’un œil ardent mesurant l’adversaire ; }
\livretVerse#8 { Du vaincu décidons le sort. }
\livretVerse#8 { « Ma loi », dit Tarare, « est la mort ». }
\livretVerse#12 { L’un sur l’autre à l’instant fond comme le tonnerre. }
\livretVerse#8 { Altamort pare le premier. }
\livretVerse#8 { Un coup affreux de cimeterre }
\livretVerse#8 { Fait voler au loin son cimier. }
\livretVerse#5 { L’acier étincelle, }
\livretVerse#5 { Le casque est brisé, }
\livretVerse#5 { Un noir sang ruisselle. }
\livretVerse#5 { Dieux ! je suis blessé. }
\livretVerse#8 { Plus furieux que la tempête, }
\livretVerse#5 { A plomb sur la tête, }
\livretVerse#5 { Le coup est rendu, }
\livretVerse#4 { le bras tendu, }
\livretVerse#5 { Tarare pare… }
\livretVerse#10 { Et tient en l’air le trépas suspendu. }
\livretPers Atar
\livretVerse#8 { Je vois qu’Altamort est perdu. }
\livretPers Urson
\livretVerse#12 { Aveuglé par le sang, il s’agite, il chancelle. }
\livretVerse#8 { Tarare, courbé sur sa selle, }
\livretVerse#8 { Pique en avant. Son fier coursier, }
\livretVerse#8 { Sentant l’aiguillon qui le perce, }
\livretVerse#8 { S’élance, et du poitrail renverse }
\livretVerse#8 { Et le cheval et le guerrier. }
\livretVerse#8 { Tarare à l’instant saute à terre, }
\livretVerse#8 { Court à l’ennemi terrassé. }
\livretVerse#8 { Chacun frémit, le cœur glacé }
\livretVerse#8 { Du terrible droit de la guerre… }
\livretRef#'DBCurson
\livretVerse#8 { Ne crains rien, superbe Altamort : }
\livretVerse#8 { Entre nous la guerre est finie. }
\livretVerse#8 { Si le droit de donner la mort }
\livretVerse#8 { Est celui d’accorder la vie, }
\livretVerse#8 { Je te la laisse de grand cœur. }
\livretVerse#8 { Pleure longtemps ta perfidie. }
\livretPers Atar
\livretVerse#12 { Sa perfidie ? }
\livretPers Urson
\livretVerse#12 { \transparent { Sa perfidie ? } Il s’en éloigne avec douleur }
\livretPers Atar
\livretVerse#12 { Il est instruit. }
\livretPers Urson
\livretVerse#12 { \transparent { Il est instruit. } Inutile et vaine faveur ! }
\livretVerse#8 { Celui dont les armes trop sûres, }
\livretVerse#8 { Ne firent jamais deux blessures, }
\livretVerse#8 { A peine, hélas ! se retirait, }
\livretVerse#8 { Que son adversaire expirait. }
\livretPers Atar
\livretRef#'DBDatar
\livretVerse#8 { Partout il a donc l’avantage ! }
\livretVerse#8 { Ah ! mon cœur en frémit de rage ! }
\livretVerse#8 { Quand, par le combat, Altamort }
\livretVerse#8 { Voulut hier régler leur sort, }
\livretVerse#8 { Urson, je sentais bien d’avance, }
\livretVerse#6 { Qu’il allait de sa mort }
\livretVerse#6 { Payer cette imprudence. }
\livretVerse#10 { Sans les clameurs d’un père épouvanté, }
\livretVerse#8 { Le temple était ensanglanté ; }
\livretVerse#8 { Mais son pouvoir força le nôtre }
\livretVerse#8 { D’arrêter un crime opportun, }
\livretVerse#8 { Qui m’offrait, dans le mort de l’un, }
\livretVerse#8 { Un prétexte pour perdre l’autre. }
\livretDidasPPage\line { (Il voit entrer les esclaves.)	}
\livretVerse#10 { Tout le sérail ici porte ses pas. }
\livretVerse#10 { Retire-toi ; que cette affreuse image, }
\livretVerse#8 { Se dissipant comme un nuage, }
\livretVerse#12 { Fasse place aux plaisirs, et ne les trouble pas. }
\livretDidasPPage (Urson sort.)

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center {
  Atar, Astasie en habit de sultane, soutenue par des esclaves, son
  mouchoir sur les yeux ; Spinette, Calpigi, Eunuques, Esclaves de
  deux sexes.
}
\livretDidasP\justify {
  (Atar fait asseoir Astasie sur le grand sopha, près de lui, et dit
  au chef des eunuques :)
}
\livretPers Atar
\livretRef#'DCBrecit
\livretVerse#12 { Calpigi, quel spectacle offrai-je à ma sultane ? }
\livretPers Calpigi
\livretVerse#8 { C’est une fête européane. }
\livretVerse#12 { Ainsi, quand l’un des rois de ces puissants états, }
\livretVerse#12 { Ordonne qu’on amuse une reine adorée ; }
\livretVerse#10 { Des jeux brillants, des mœurs de vos climats, }
\livretVerse#10 { Sa noble fête à l’instant est parée. }
\livretDidasPPage (à part)
\livretVerse#8 { Tarare n’est point prévenu ; }
\livretVerse#8 { S’il arrivait, il est perdu. }

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\column {
  \justify {
    Les acteurs précédents, Bergers européans de cour, vêtus galemment
    en habits de taffetas, avec des plumes, ainsi que leurs bergères,
    ayant des houlettes dorées.
    Paysans grossiers, vêtus à l’européane, ainsi que leurs paysannes,
    mais très simplement, tenant des instruments aratoires.
  }
  \justify {
    Marche, dont le dessus léger peint le caratère des bergers de
    cour qui la dansent, et dont la basse peint la lourde gaîté des
    paysans qui la sautent.
  }
}
\livretRef#'DDAmarche
\livretDidasPage Marche.
\livretPers Chœur d'Européans
\livretRef#'DDBchoeur
\livretVerse#8 { Peuple léger mais généreux, }
\livretVerse#8 { Nous blâmons les mœurs de l’Asie : }
\livretVerse#8 { Jamais, dans nos climats heureux, }
\livretVerse#8 { La beauté ne tremble asservie. }
\livretVerse#8 { Chez nos maris, presqu’à leurs yeux, }
\livretVerse#8 { Un galant en fait son amie ; }
\livretVerse#8 { La prend, la rend, rit avec eux, }
\livretVerse#8 { Et porte ailleurs sa douce envie. }
\livretRef#'DDCmenuet
\livretDidasPPage\justify {
  Deux jeunes seigneur et dame de la cour commencent une danse assez
  vive ; deux jeunes berger et bergère de la campagne, commencent en
  même temps un pas assez simple. Leur danse est interrompue par une
  bergère coquette et une bergère sensible.
}
\livretPersDidas Spinette (en Bergère coquette, aux danseurs)
\livretRef#'DDEduo
\livretVerse#8 { Galants qui courtisez les belles, }
\livretVerse#8 { Sachez brusquer un doux moment. }
\livretPersDidas Une Bergère (sensible)
\livretVerse#8 { Amants qui soupirez pour elles, }
\livretVerse#8 { Espérez tout du sentiment. }
\livretPers (Coquette)
\livretVerse#7 { Toute occasion non saisie, }
\livretVerse#8 { S’échappe et se perd sans retour. }
\livretPers (Sensible)
\livretVerse#8 { Sans retour pour la fantaisie ; }
\livretVerse#8 { Mais elle renaît pour l’amour. }
\livretDidasPPage\justify {
  Le pas des quatre danseurs reprend et s'achève.
}
\livretRef#'DDFdanse
\livretDidasPPage\justify {
  De vieux seigneurs dansent vivement devant des bergères modestes, en
  leur présentant des bouquets ; des jeunes gens fatigués, appuyés sur
  leur houlettes, se meuvent à peine devant de vieilles coquettes qui
  dansent à perdre haleine. Atar se lève, et erre parmi les danseurs.
}
\livretPersDidas Spinette (en bergère de cour)
\livretRef#'DDGspinette
\livretVerse#8 { Dans nos vergers délicieux, }
\livretVerse#4 { Le mal, le mieux, }
\livretVerse#4 { Tout se balance ; }
\livretVerse#8 { Et si nos jeunes gens sont vieux, }
\livretVerse#8 { Tous nos vieillards sont dans l’enfance. }
\livretPersDidas Un Paysan (grossier)
\livretVerse#6 { Chez nous point d’imposture ; }
\livretVerse#6 { Enfants de la nature, }
\livretVerse#4 { Nos tendres soins }
\livretVerse#4 { Sont pour les foins, }
\livretVerse#8 { Et notre amour pour la pâture. }
\livretDidasPPage (On danse.)
\livretPersDidas Spinette (en bergère de cour)
\livretVerse#8 { Quand l’époux devient indolent, }
\livretVerse#4 { Contre un galant }
\livretVerse#4 { L’amour l’échange ; }
\livretVerse#8 { Et de ses volages désirs, }
\livretVerse#4 { Par des plaisirs, }
\livretVerse#4 { L’hymen se venge. }
\livretPersDidas Un Paysan (grossier)
\livretVerse#6 { Chez nous, jamais légère, }
\livretVerse#6 { L’active ménagère, }
\livretVerse#4 { Pour favori }
\livretVerse#4 { N’a qu’un mari ; }
\livretVerse#8 { Mais de ses fils chacun est père. }
\livretDidasPPage (On danse.)
\livretPers Spinette
\livretVerse#4 { Chez nous, sans bruit }
\livretVerse#4 { On se détruit ; }
\livretVerse#4 { On brigue, on nuit, }
\livretVerse#4 { Mais sans scandale. }
\livretPersDidas Un Paysan (grossier, achevant le couplet)
\livretVerse#8 { Ma foi, chez nous, tout ce qu’autrui }
\livretVerse#4 { Te fait, fais-lui ; }
\livretVerse#4 { C’est la morale. }
\livretPers Astasie
\livretRef#'DDHastasie
\livretVerse#8 { Grands dieux ! que la mort d’Astasie }
\livretVerse#8 { L’arrache au tyran de l’Asie ! }
\livretRef#'DDIair
\livretDidasPPage (On danse.)
\livretPersDidas Astasie (pendant la danse)
\livretVerse#8 { Ô mon Tarare, ô mon époux ! }
\livretVerse#8 { Dans quel désespoir êtes-vous ! }
% \livretPers Chœur d'Européans
% %# Aux travaux mêlons la gaîté;
% %# Tout mal guérit par ses contraires.
% %# Nos loix ont de l'austérité;
% %# Mais nos mœurs sont douces, légères.
% %# Si le dur hymen est chez nous
% %# Bien absolu, bien despotique;
% %# L'amour en secret fait de tous
% %# Une charmante république.
% \livretDidasPPage (On danse.)
% \livretPersDidas Astasie (les bras élevés pendant la danse)
% %# Grands dieux! que la mort d'Astasie
% %# L'arrache au tyran de l'Asie!
\livretRef#'DDJspinette
\livretDidasPPage (La danse continue.)
\livretPersDidas Atar (revient à Astasie, et dit à tout le serrail)
\livretRef#'DDKrecit
\livretVerse#8 { Saluez tous la belle Irza. }
\livretVerse#8 { Je la couronne ; elle est sultane. }
\livretDidasPPage\justify {
  (Il lui attache au front un diadème de diamants.)
}
\livretPers Chœur Universel
\livretRef#'DDLchoeur
\livretVerse#8 { Saluons tous la belle Irza. }
\livretVerse#8 { Qu’amour, du fond d’une cabane, }
\livretVerse#8 { Au trône d’Ormus éleva. }
\livretVerse#8 { Du grand Atar elle est sultane. }
\livretRef#'DDMdanse
\livretDidasPPage (On danse.)
% \livretPersDidas Astasie (pendant la danse)
% %# Ô mon Tarare, ô mon époux!
% %# Dans quel désespoir êtes-vous!
% \livretDidasPPage\justify {
%   (Spinette la masque de sa personne pour que l'empereur ne la voie pas.)
% }
% \livretDidasPPage\justify {
%   Ballet général, ou les deux genres de danse se mêlent sans se confondre.
% }
% \livretDidasPPage\justify {
%   Le ballet fini, des esclaves apportent des vases de sorbet, des
%   liqueurs et des fruits devant Atar et la sultane. Spinette reste
%   auprès de sa maîtresse, prête à la servir.
% }
\livretPersDidas Atar (avec joie)
\livretRef#'DDNrecit
\livretVerse#8 { Calpigi, ta fête est charmante ! }
\livretVerse#11 { J’aime un talent vainqueur à qui tout obéit : }
\livretVerse#8 { Ton esprit fertile m’enchante. }
\livretVerse#10 { Des mers d’Europe et contre toute attente, }
\livretVerse#12 { Dis-nous quel heureux sort en ce lieu t’a conduit. }
\livretVerse#8 { Mais pour amuser mon amante, }
\livretVerse#12 { Anime ton récit d’une gaîté piquante. }
\livretPers Calpigi
\livretVerse#12 { J’y veux mêler un nom qui nous rendra la nuit. }
\livretDidasPPage\justify {
  (Il prend une mandoline, et chante sur le tone de la barcarola.)
}
\livretDidasPPage\justify {
  (La danse figurée cesse; tous les danseurs et danseuses se prennent
  par la main pour danser le refrain de sa chanson.)
}
\livretDidasPPage 1er couplet
\livretRef#'DDOcalpigi
\livretVerse#8 { Je suis natif de Ferrare ; }
\livretVerse#8 { Là, par les soins d’un père avare, }
\livretVerse#8 { Mon chant s’étant fort embelli ; }
\livretVerse#7 { Ahi ! povero Calpigi ! }
\livretVerse#8 { Je passai du conservatoire, }
\livretVerse#8 { Premier chanteur à l’oratoire }
\livretVerse#8 { Du souverain di Napoli : }
\livretVerse#8 { Ah ! bravo, caro Calpigi ! }
\livretDidasPPage\justify {
  (Le chœur répète le dernier vers. On danse la ritournelle.)
}
\livretDidasPPage\justify {
  (À la fin de chaque couplet, Calpigi se retourne, et regarde avec
  inquiétude du côté par où il craint que Tarare n'arrive.)
}
\livretDidasPPage 2e couplet
\livretVerse#8 { La plus célèbre cantatrice, }
\livretVerse#8 { De moi fit bientôt par caprice, }
\livretVerse#8 { Un simulacre de mari. }
\livretVerse#7 { Ahi ! povero Calpigi ! }
\livretVerse#8 { Mes fureurs, ni mes jalousies, }
\livretVerse#8 { N’arrêtant point ses fantaisies, }
\livretVerse#8 { J’étais chez moi comme un zéro : }
\livretVerse#7 { Ahi ! Calpigi povero ! }
\livretDidasPPage\justify {
  (Le chœur répète le dernier vers. On danse la ritournelle.)
}
\livretDidasPPage 3e couplet
\livretVerse#8 { Je résolus, pour m’en défaire, }
\livretVerse#8 { De la vendre à certain corsaire, }
\livretVerse#8 { Exprès passé de Tripoli : }
\livretVerse#8 { Ah ! bravo, caro Calpigi ! }
\livretVerse#8 { Le jour venu, mon traître d’homme, }
\livretVerse#8 { Au lieu de me compter la somme, }
\livretVerse#8 { M’enchaîne au pied de leur châlit : }
\livretVerse#7 { Ahi ! povero Calpigi ! }
\livretDidasPPage 4e couplet
\livretVerse#8 { Le forban en fit sa maîtresse ; }
\livretVerse#8 { De moi, l’argus de sa sagesse ; }
\livretVerse#8 { Et j’étais là tout comme ici : }
\livretVerse#7 { Ahi ! povero Calpigi ! }
\livretDidasPPage\justify {
  (Spinette, en cet endroit, fait un grand éclat de rire.)
}
\livretPers Atar
\livretVerse#8 { Qu’avez-vous à rire, Spinette ? }
\livretPers Calpigi
\livretVerse#8 { Vous voyez ma fausse coquette. }
\livretPers Atar
\livretVerse#8 { Dit-il vrai ? }
\livretPers Spinette
\livretVerse#8 { \transparent { Dit-il vrai ? } Signor, è vero. }
\livretPersDidas Calpigi (acheve l’air)
\livretVerse#7 { Ahi ! Calpigi povero ! }
\livretDidasPPage\justify {
  (Le chœur répète le dernier vers. On danse la ritournelle.)
}
\livretDidasP\justify {
  (Ici l’on voit dans le fond Tarare descendre par une échelle de soie ;
  Calpigi l’aperçoit.)
}
\livretDidasPPage (à part)
\livretVerse#12 { C’est Tarare ! }
\livretDidasPPage 5e couplet, plus vite
\livretVerse#12 { \transparent { C’est Tarare ! } Bientôt à travers la Libye, }
\livretVerse#8 { L’Egypte, l’Isthme et l’Arabie, }
\livretVerse#8 { Il allait nous vendre au Sophi : }
\livretVerse#7 { Ahi ! povero Calpigi ! }
\livretVerse#8 { Nous sommes pris, dit le barbare. }
\livretVerse#8 { Qui nous prenait ? Ce fut Tarare… }
\livretPersDidas Astasie (faisant un cri)
\livretRef#'DDPtous
\livretVerse#8 { Tarare ! }
\livretPersDidas Tout le sérail (s’écrie)
\livretVerse#8 { \transparent { Tarare ! } Tarare ! }
\livretPersDidas Atar (furieux)
\livretVerse#8 { \transparent { Tarare ! Tarare ! } Tarare ! }
\livretDidasPPage\justify {
  (Il renverse la table d'un coup de pied.)
}
\livretDidasP\justify {
  (Astasie se lève troublée. Spinette la soutient. Au bruit qui se
  fait, Tarare, à moitié descendu, se jette en bas dans l'obscurité.)
}
\livretPersDidas Spinette (à Astasie)
\livretVerse#8 { Dieux ! que ce nom l’a courroucé ! }
\livretPers Atar
\livretVerse#8 { Que la mort, que l’enfer s’empare }
\livretVerse#8 { Du traitre qui l’a prononcé ! }
\livretDidasPPage\justify {
  (Il tire son poignard ; tout le monde s’en fuit.)
}
\livretPersDidas Spinette (soutenant Astasie)
\livretVerse#14 { Elle expire ! }
\livretDidasP\justify {
  Atar rappellé à lui par ce cri, laisse aller Calpigi et les autres
  esclaves, et revient vers Astasie, que des femmes emportent chez
  elle. Atar y entre, en jettant à la porte sa simarre et ses
  brodequins, à la manière des orientaux.
}

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\column {
  \wordwrap-center { Le théâtre est très obscur. }
  \wordwrap-center {
    Calpigi, Tarare, un poignard à la main, prêt à frapper Calpigi qu’il entraîne.
  }
}
\livretPersDidas Calpigi (s’écrie)
\livretRef#'DEArecit
\livretVerse#14 { \transparent { Elle expire ! } Ô Tarare ! }
\livretPersDidas Tarare (avec un grand trouble)
\livretVerse#14 { \transparent { Elle expire ! Ô Tarare ! } Ô fureur que j’abhorre ! }
\livretVerse#8 { Mon ami… s’il n’eût pas parlé, }
\livretVerse#8 { De ma main était immolé ! }
\livretPers Calpigi
\livretVerse#12 { Tu le devais, Tarare ! il le faudrait encore, }
\livretVerse#8 { Si quelque esclave curieux… }
\livretPersDidas Tarare (troublé)
\livretVerse#12 { Mille cris de mon nom font retentir ces lieux ! }
\livretVerse#12 { Je me crois découvert, et que la jalousie… }
\livretVerse#12 { Mourir sans la revoir, et si près d’Astasie !… }
\livretPers Calpigi
\livretVerse#10 { O mon héros ! tes vêtements mouillés, }
\livretVerse#10 { D’algues impures et de limon souillés !… }
\livretVerse#10 { Un grand péril a menacé ta vie ! }
\livretPers Tarare
\livretRef#'DEBtarare
\livretVerse#8 { Au sein de la profonde mer, }
\livretVerse#8 { Seul dans une barque fragile, }
\livretVerse#8 { Aucun soufle n’agitant l’air, }
\livretVerse#8 { Je sillonnais l’onde tranquille. }
\livretVerse#10 { Des avirons le monotone bruit, }
\livretVerse#8 { Au loin distingué dans la nuit, }
\livretVerse#8 { Soudain a fait sonner l’alarme ; }
\livretVerse#8 { J’avais ce poignard pour toute arme. }
\livretVerse#10 { Deux cents rameurs partent du même lieu : }
\livretVerse#10 { On m’enveloppe, on se croise, on rappelle, }
\livretVerse#8 { J’étais pris !… D’un grand coup d’épieu, }
\livretVerse#8 { Je m’abîme avec ma nacelle, }
\livretVerse#8 { Et, me frayant sous les vaisseaux, }
\livretVerse#8 { Une route nouvelle et sure ; }
\livretVerse#8 { J’arrive à terre entre deux eaux, }
\livretVerse#8 { Dérobé par la nuit obscure. }
\livretVerse#8 { J’entend la cloche du béfroi. }
\livretVerse#8 { Le son bruyant de la trompette, }
\livretVerse#8 { Que le fond du golphe répète, }
\livretVerse#8 { Augmente le trouble et l’effroi. }
\livretVerse#8 { On court, on crie aux sentinelles, }
\livretVerse#8 { Arrête ! arrête : on fond sur moi : }
\livretVerse#8 { Mais, s’ils couraient, j’avais des ailes. }
\livretVerse#8 { J’atteins le mur comme un éclair : }
\livretVerse#8 { On cherche au pied ; j’étais dans l’air, }
\livretVerse#8 { Sur l’échelle souple et tendue, }
\livretVerse#8 { Que ton zèle avait suspendue. }
\livretRef#'DECduo
\livretVerse#8 { Je suis sauvé, grâce à ton cœur ; }
\livretVerse#8 { Et pour payer tant de faveur, }
\livretVerse#8 { Ô douleur ! ô crime exécrable ! }
\livretVerse#8 { Trompé par une aveugle erreur, }
\livretVerse#8 { J’allais, d’une main misérable, }
\livretVerse#8 { Assassiner mon bienfaiteur ! }
\livretVerse#10 { Pardonne, ami, ce crime involontaire. }
\livretPers Calpigi
\livretVerse#8 { Ô mon héros ! que me dois-tu ? }
\livretVerse#8 { Sans force, hélas ! sans caractère, }
\livretVerse#12 { Le faible Calpigi, de tous les vents battu, }
\livretVerse#8 { Serait moins que rien sur la terre, }
\livretVerse#12 { S’il n’était pas épris de ta mâle vertu ! }
\livretVerse#10 { Ne perdons point un instant salutaire : }
\livretVerse#8 { Au sérail, la tranquillité }
\livretVerse#8 { Renaît avec l’obscurité. }
\livretDidasPPage\wordwrap { (Il prend un paquet dans une touffe d’arbres.) }
\livretVerse#8 { Sous cet habit d’un noir esclave, }
\livretVerse#8 { Cachons des guerriers le plus brave. }
\livretVerse#10 { D’homme éloquent, deviens un vil muet. }
\livretDidasPPage\wordwrap { (Il l’habille en muet.) }
\livretVerse#10 { Que mon héros, surtout, jamais n’oublie }
\livretVerse#10 { Que sous ce masque, un mot est un forfait ; }
\livretDidasPPage\wordwrap { (Il lui met un masque noir.) }
\livretVerse#8 { Et qu’en ce lieu de jalousie, }
\livretVerse#8 { Le moindre est payé de la vie. }
\livretDidasPPage\wordwrap {
  (Ils s’avancent vers l’appartement d’Astasie. L’arrête et recule.)
}
\livretVerse#10 { N’avançons pas ! j’aperçois la simarre, }
\livretVerse#8 { Les brodequins de l’empereur. }
\livretPersDidas Tarare (égaré, criant)
\livretVerse#10 { Atar chez elle ! Ah ! malheureux Tarare ! }
\livretVerse#8 { Rien ne retiendra ma fureur : }
\livretVerse#10 { Brama ! Brama ! }
\livretPersDidas Calpigi (lui fermant la bouche)
\livretVerse#10 { \transparent { Brama ! Brama ! } Renferme donc ta peine ! }
\livretPersDidas Tarare (criant plus fort)
\livretVerse#10 { Brama ! Brama ! }
\livretPers Calpigi
\livretVerse#10 { \transparent { Brama ! Brama ! } Notre mort et certaine. }

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center {
  Atar sort de chez Astasie. Tarare, Calpigi.
}
\livretPersDidas Calpigi (crie, effrayé)
\livretRef#'DFArecit
\livretVerse#12 { On vient ; c’est le sultan. }
\livretDidasPPage\line { (Tarare tombe la face contre terre.) }
\livretPersDidas Atar (d’un ton terrible)
\livretVerse#12 { \transparent { On vient ; c’est le sultan. } Quel insolent ici ?… }
\livretPersDidas Calpigi (troublé)
\livretVerse#8 { Un insolent !… C’est Calpigi ! }
\livretPers Atar
\livretVerse#8 { D’où vient cette voix déplorable ? }
\livretPersDidas Calpigi (troublé)
\livretVerse#8 { Seigneur, c’est… c’est ce misérable. }
\livretVerse#8 { Croyant entendre quelque bruit, }
\livretVerse#8 { Nous faisions la ronde de nuit. }
\livretVerse#8 { D’une soudaine frénésie }
\livretVerse#8 { Cette brute à l’instant saisie… }
\livretVerse#8 { Peut-être a-t-il perdu l’esprit ! }
\livretVerse#8 { Mais il pleure, il crie, il s’agite, }
\livretVerse#8 { Parle, parle, parle si vite, }
\livretVerse#8 { Qu’on n’entend rien de ce qu’il dit. }
\livretPersDidas Atar (d’un ton terrible)
\livretVerse#8 { Il parle, ce muet ? }
\livretPersDidas Calpigi (plus troublé)
\livretVerse#8 { \transparent { Il parle, ce muet ? } Que dis-je ! }
\livretVerse#8 { Parler serait un beau prodige ! }
\livretVerse#8 { D’affreux sons inarticulés… }
\livretDidasPPage\wordwrap {
  (Atar lui prend les bras. Tarare est sans mouvement, prosterné.)
}
\livretPers Atar
\livretVerse#8 { O bizarre sort de ton maître ! }
\livretVerse#8 { Tu maudis quelquefois ton être… }
\livretVerse#8 { Je venais, les sens agités, }
\livretVerse#8 { L’honorer de quelques bontés, }
\livretVerse#8 { Soupirer d’amour auprès d’elle. }
\livretVerse#8 { À peine étais-je à ses côtés, }
\livretVerse#8 { Elle s’échappe, la rebelle ! }
\livretVerse#8 { Je l’arrête et saisis sa main : }
\livretVerse#8 { Tu n’as vu chez nulle mortelle }
\livretVerse#8 { L’exemple d’un pareil dédain ! }
\livretVerse#10 { « Farouche Atar ! quelle est donc ton envie ? }
\livretVerse#8 { « Avant de me ravir l’honneur, }
\livretVerse#8 { « Il faudra m’arracher la vie… » }
\livretVerse#8 { Ses yeux pétillaient de fureur. }
\livretVerse#10 { Farouche Atar !… son honneur !… la sauvage, }
\livretVerse#8 { Appelant la mort à grands cris… }
\livretVerse#10 { Atar, enfin, a connu le mépris. }
\livretDidasPPage\line { (Il tire son poignard.) }
\livretVerse#8 { Vingt fois j’ai voulu, dans ma rage, }
\livretVerse#8 { Épargner moi-même à son bras… }
\livretVerse#8 { Allons, Calpigi, suis mes pas. }
\livretPersDidas Calpigi (lui présente sa simarre)
\livretVerse#8 { Seigneur, prenez votre simarre. }
\livretPers ATAR
\livretVerse#8 { Rattache avant, mon brodequin, }
\livretVerse#8 { Sur le corps de cet africain… }
\livretDidasPPage\wordwrap { (Il met son pied sur le corps de Tarare.) }
\livretVerse#8 { Je sens que la fureur m’égare !… }
\livretDidasPPage (Il regarde Tarare.)
\livretVerse#8 { Malheureux nègre, abject et nu, }
\livretVerse#8 { Au lieu d’un reptile inconnu, }
\livretVerse#8 { Que du néant rien ne sépare, }
\livretVerse#8 { Que n’es-tu l’odieux Tarare ! }
\livretVerse#8 { Avec quel plaisir, de ce flanc, }
\livretVerse#8 { Ma main épuiserait le sang !… }
\livretVerse#10 { Si l’insolent pouvait jamais connaître }
\livretVerse#8 { Quels dédains il vaut à son maître !… }
\livretVerse#8 { Et c’est pour cet indigne objet ; }
\livretVerse#8 { C’est pour lui seul qu’elle me brave !… }
\livretVerse#8 { Calpigi, je forme un projet : }
\livretVerse#8 { Coupons la tête à cet esclave ; }
\livretVerse#8 { Défigure-la tout-à-fait ; }
\livretVerse#8 { Porte-la de ma part toi-même. }
\livretVerse#8 { Dis-lui qu’en mes transports jaloux, }
\livretVerse#8 { Surprenant ici son époux… }
\livretDidasPPage\justify { (Il tire le sabre de Calpigi.) }
\livretPersDidas Calpigi (l’arrête et l’eloigne de son ami)
\livretVerse#8 { De cet horrible stratagème, }
\livretVerse#8 { Ah ! mon maître, qu’espérez-vous ? }
\livretVerse#8 { Quand elle pourrait s’y méprendre, }
\livretVerse#8 { En deviendrait-elle plus tendre ? }
\livretVerse#8 { En l’inquiétant sur ses jours, }
\livretVerse#8 { Vous la ramènerez toujours. }
\livretPersDidas Atar (furieux)
\livretVerse#10 { La ramener !… j’adopte une autre idée. }
\livretVerse#8 { Elle me croit l’âme enchantée : }
\livretVerse#8 { Montrons-lui bien le peu de cas }
\livretVerse#8 { Que je fais de ses vains appas. }
\livretVerse#10 { Cette orgueilleuse a dédaigné son maître ! }
\livretVerse#8 { Ô le plus charmant des projet ! }
\livretVerse#8 { Je punis l’audace d’un traître }
\livretVerse#10 { Qui m’enleva le cœur de mes sujets ; }
\livretVerse#10 { Et j’avilis la superbe à jamais. }
\livretVerse#12 { Calpigi ?… }
\livretPersDidas Calpigi (troublé)
\livretVerse#12 { \transparent { Calpigi ?… } Quoi ! Seigneur ! }
\livretPers Atar
\livretVerse#12 { \transparent { Calpigi ?… Quoi ! Seigneur ! } Jure-moi sur ton âme, }
\livretVerse#12 { D’obéir. }
\livretPersDidas Calpigi (plus troublé)
\livretVerse#12 { \transparent { D’obéir. } Oui, seigneur. }
\livretPers Atar
\livretVerse#12 { \transparent { D’obéir. Oui, seigneur. } Point de zèle indiscret ; }
\livretVerse#13 { Tout à l’heure. }
\livretPersDidas Calpigi (presqu'égaré)
\livretVerse#13 { \transparent { Tout à l’heure. } À l’instant. }
\livretPers Atar
\livretVerse#13 { \transparent { Tout à l’heure. À l’instant. } Prends-moi ce vil muet ; }
\livretVerse#8 { Conduis-le chez elle en secret ; }
\livretVerse#8 { Apprends-lui que ma tendre flamme }
\livretVerse#8 { La donne à ce monstre pour femme. }
\livretVerse#8 { Dis-lui bien que j’ai fait serment }
\livretVerse#12 { Qu’elle n’aura jamais d’autre époux, d’autre amant. }
\livretVerse#8 { Je veux que l’hymen s’accomplisse ; }
\livretVerse#8 { Et si l’orgueilleuse prétend }
\livretVerse#8 { S’y dérober, prompte justice. }
\livretVerse#8 { Qu’à son lit à l’instant conduit, }
\livretVerse#8 { Avec elle il passe la nuit ; }
\livretVerse#8 { Et qu’à tous les yeux exposée, }
\livretVerse#12 { Demain, de mon sérail elle soit la risée ! }
\livretVerse#12 { À présent, Calpigi, de moi je suis content. }
\livretVerse#12 { Toi, par tes signes, fais que cette brute apprenne }
\livretVerse#8 { Le sort fortuné qui l’attend. }
\livretPersDidas Calpigi (tranquilisé)
\livretVerse#8 { Ah ! seigneur, ce n’est pas la peine ; }
\livretVerse#8 { S’il ne parle pas, il entend. }
\livretPers Atar
\livretVerse#12 { Accompagne ton maître à la garde prochaine. }
\livretDidasPPage (Il se retourne pour sortir.)
\livretPers Calpigi
\livretDidasPPage\wordwrap {
  (en se baissant pour ramasser la simarre de l'empereur, dit tout bas à Tarare)
}
\livretVerse#12 { Quel heureux dénouement ! }
\livretDidasPPage (Il suit Atar.)
\livretPersDidas Tarare (se relève à genoux)
\livretVerse#12 { \transparent { Quel heureux dénouement ! } Mais quelle horrible scène ! }
\livretDidasPPage\wordwrap {
  (Il relève son masque, qui tombe à terre loin de lui.)
}
\livretVerse#12 { Ah ! respirons. }
\livretDidasPPage\wordwrap {
  (Atar revient à l’appartament d’Astasie, d'un air menaçant,
  et dit avec une joie féroce.)
}
\livretPers Atar
\livretVerse#12 { \transparent { Ah ! respirons. } Je pense au plaisir que j’aurai, }
\livretVerse#8 { Superbe ! quand je te verrai }
\livretVerse#8 { Au sort d’un vieux nègre liée, }
\livretVerse#8 { Et par cent cris humiliée ! }
\livretDidasPPage\wordwrap { (Il imite le chant trivial des esclaves.) }
\livretVerse#8 { Saluons tous la fière Irza, }
\livretVerse#8 { Qui, regrettant une cabane, }
\livretVerse#8 { Aux vœux d’un roi se refusa : }
\livretVerse#8 { De ce vieux nègre elle est sultane. }
\livretVerse#12 { Hein ? Calpigi ? }
\livretPers Calpigi
\livretVerse#12 { \transparent { Hein ? Calpigi ? } Ah ! quel plaisir mon maître aura ! }
\livretPers Atar
\livretVerse#12 { Hein ! Calpigi ? }
\livretPers Calpigi
\livretVerse#12 { \transparent { Hein ! Calpigi ? } Ah ! quel plaisir mon maître aura ! }
\livretVerse#8 { Quand le sérail retentira… }
\livretPers Atar, Calpigi
\livretVerse#8 { Saluons tous la fière Irza, }
\livretVerse#8 { Qui, regrettant une cabane, }
\livretVerse#8 { Aux vœux d’un roi se refusa : }
\livretVerse#8 { De ce vieux nègre elle est sultane. }
\livretDidasPPage\wordwrap {
  (Le même jeu de scène continue ; il sortent.)
}

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\wordwrap-center {
  Tarare seul, levant les mains au ciel.
}
\livretPers Tarare
\livretRef#'DGAtarare
\livretVerse#10 { Dieu tout-puissant ! tu ne trompas jamais }
\livretVerse#10 { L’infortuné qui croit à tes bienfaits. }
\livretDidasPPage\wordwrap {
  (Il remet son masque, et suit de loin l'empereur.)
}
\sep
\livretAct ACTE QUATRIÈME
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\column {
  \justify {
    Le théâtre représente l’intérieur de l’appartement d’Astasie.
    C’est un salon superbe, garni de sophas et autres meubles orientaux.
  }
  \wordwrap-center {
    Astasie, Spinette.
  }
}
\livretRef#'EAArecit
\livretDidasPPage\line { (Astasie entre, en grand désordre.) }
\livretPers Astasie
\livretVerse#12 { Spinette, comment fuir de cette horrible enceinte ? }
\livretPers Spinette
\livretVerse#12 { Calmez le désespoir dont votre âme est atteinte. }
\livretPersDidas Astasie (égarée, les bras élevés)
\livretVerse#8 { Ô mort ! termine mes douleurs ; }
\livretVerse#6 { Le crime se prépare. }
\livretVerse#8 { Arrache au plus grand des malheurs, }
\livretVerse#6 { L’épouse de Tarare. }
\livretVerse#8 { Il semblait que je pressentais }
\livretVerse#6 { Leur entreprise infâme ! }
\livretVerse#8 { Quand il partit, je répétais, }
\livretVerse#6 { Hélas ! l’effroi dans l’âme ! }
\livretVerse#8 { Cruel ! pour qui j’ai tant souffert, }
\livretVerse#6 { C’est trop que ton absence }
\livretVerse#8 { Laisse Astasie en un désert, }
\livretVerse#6 { Sans joie et sans défense ! }
\livretVerse#8 { L’imprudent n’a pas écouté }
\livretVerse#6 { Sa compagne éplorée : }
\livretVerse#8 { Aux mains d’un brigand détesté }
\livretVerse#6 { Des brigands l’ont livrée, }
\livretVerse#8 { Ô mort ! termine mes douleurs : }
\livretVerse#6 { Le crime se prépare. }
\livretVerse#8 { Arrache au plus grand des malheurs, }
\livretVerse#6 { L’épouse de Tarare. }
\livretPers Spinette
\livretRef#'EABrecit
\livretVerse#12 { Un grand roi vous invite à faire son bonheur. }
\livretVerse#12 { L’amour met à vos pieds le maître de la terre. }
\livretVerse#12 { Que de beautés ici brigueraient cet honneur ! }
\livretVerse#12 { Loin de s’en alarmer, on peut en être fière. }
\livretPersDidas Astasie (pleurant)
\livretVerse#12 { Ah ! vous n’avez pas eu Tarare pour amant ! }
\livretPers Spinette
\livretVerse#12 { Je ne le connais point ; j’aime sa renommée ; }
\livretVerse#12 { Mais, pour lui, comme vous, si j’étais enflammée, }
\livretVerse#12 { Avec le dur Atar je feindrais un moment ; }
\livretVerse#12 { Et j’instruirais Tarare au moins de ma souffrance. }
\livretPers Astasie
\livretVerse#8 { À la plus légère espérance }
\livretVerse#12 { Le cœur des malheureux s’ouvre facilement. }
\livretVerse#8 { J’aime ton noble attachement : }
\livretVerse#13 { He bien ! fais-lui savoir qu’en cette enceinte horrible… }
\livretPers Spinette
\livretVerse#8 { Cachez vos pleurs, s’il est possible. }
\livretVerse#8 { Des secrets plaisirs du sultan }
\livretVerse#10 { Je vois venir le ministre insolent. }
\livretDidasPPage\justify {
  (Astasie essuie ses yeux, et se remet de son mieux.)
}

\livretScene SCÈNE DEUXIÈME
\livretDescAtt\wordwrap-center {
  Calpigi, Spinette, Astasie.
}
\livretPersDidas Calpigi (d’un ton dur)
\livretRef#'EBArecit
\livretVerse#8 { Belle Irza, l’empereur ordonne }
\livretVerse#10 { Qu’en ce moment vous receviez la foi }
\livretVerse#8 { D’un nouvel époux qu’il vous donne. }
\livretPers Astasie
\livretVerse#8 { Un époux ! un époux à moi ? }
\livretPersDidas Spinette (le contrefait)
\livretVerse#8 { Commandant d’un corps ridicule ! }
\livretVerse#10 { Abrège-nous ton grave préambule. }
\livretVerse#8 { Ce nouvel époux, quel est-il ? }
\livretPers Calpigi
\livretVerse#10 { C’est du sérail le muet le plus vil. }
\livretPers Astasie
\livretVerse#8 { Un muet ! }
\livretPers Spinette
\livretVerse#8 { \transparent { Un muet ! } Un muet ! }
\livretPers Astasie
\livretVerse#8 { \transparent { Un muet ! Un muet ! } J’expire. }
\livretPers Calpigi
\livretVerse#8 { L’ordre est que chacun se retire. }
\livretPers Spinette
\livretVerse#12 { Moi ? }
\livretPers Calpigi
\livretVerse#12 { \transparent { Moi ? } Vous. }
\livretPers Spinette
\livretVerse#12 { \transparent { Moi ? Vous. } Moi ? }
\livretPers Calpigi
\livretVerse#12 { \transparent { Moi ? Vous. Moi ? } Vous ; vous, Spinette ; il y va des jours }
\livretVerse#8 { De qui troublerait leurs amours. }
\livretPers Astasie
\livretVerse#8 { O juste ciel ! }
\livretPersDidas Spinette (raillant)
\livretVerse#8 { \transparent { O juste ciel ! } Dis à ton maître }
\livretVerse#4 { Que le grand-prêtre }
\livretVerse#8 { Sera sans doute assez surpris, }
\livretVerse#8 { Qu’à la pluralité des femmes, }
\livretVerse#8 { On ose ajouter, chez les Brames, }
\livretVerse#8 { La pluralité des maris. }
\livretPersDidas Calpigi (ironiquement)
\livretVerse#12 { Votre conseil au roi paraîtra d’un grand prix. }
\livretVerse#12 { J’en ferai votre cour. }
\livretPersDidas Spinette (du même ton)
\livretVerse#12 { \transparent { J’en ferai votre cour. } Vous l’oublierez peut-être ? }
\livretPers Calpigi
\livretVerse#12 { Non. }
\livretPers Spinette
\livretVerse#12 { \transparent { Non. } Vous le rendrez mieux, l’ayant deux fois appris. }
\livretDidasPPage (elle répete :)
\livretVerse#4 { Dis à ton maître, }
\livretVerse#4 { Que le grand-prêtre }
\livretVerse#8 { Sera sans doute assez surpris, }
\livretVerse#8 { Qu’à la pluralité des femmes, }
\livretVerse#8 { On ose ajouter, chez les Brames, }
\livretVerse#8 { La pluralité des maris. }
\livretDidasPPage (Calpigi sort.)

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center { Astasie, Spinette. }
\livretPersDidas Astasie (au désespoir)
\livretRef#'ECArecit
\livretVerse#8 { Ô ma compagne ! ô mon amie ! }
\livretVerse#8 { Sauve-moi de cette infamie. }
\livretPers Spinette
\livretVerse#8 { Eh ! comment vous prouver ma foi ? }
\livretPers Astasie
\livretVerse#8 { Prends mes diamants, ma parure : }
\livretVerse#8 { Je te les donne, ils sont à toi. }
\livretDidasPPage (Elle les détache.)
\livretVerse#8 { Ah ! dans cette horrible aventure, }
\livretVerse#8 { Sois Irza, représente-moi ; }
\livretVerse#8 { On réprime un muet sans peine. }
\livretPers Spinette
\livretVerse#8 { Si c’est Calpigi qui l’amène, }
\livretVerse#8 { Madame, il me reconnaîtra. }
\livretPersDidas Astasie (ôte son manteau royal)
\livretVerse#8 { Ce long manteau te couvrira. }
\livretVerse#12 { Souviens-toi de Tarare, et nomme-le sans cesse ; }
\livretVerse#8 { Son nom seul te garantira. }
\livretPersDidas Spinette (pendant qu’on l’habille)
\livretRef#'ECBspinette
\livretVerse#8 { Je partage votre détresse. }
\livretVerse#8 { Hélas ! que ne ferais-je pas, }
\livretVerse#8 { Pour sauver d’un dangereux pas, }
\livretVerse#8 { Mon incomparable maîtresse ! }
\livretDidasPPage (Astasie sort.)

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\wordwrap-center { Spinette seule. }
\livretRef#'EDArecit
\livretVerse#8 { Spinette, allons, point de faiblesse ! }
\livretVerse#8 { Le roi dans peu te sera gré, }
\livretVerse#8 { D’avoir adroitement paré }
\livretVerse#8 { Le coup qu’il porte à sa maîtresse. }
\livretDidasPPage\line { (Elle s’assied sur un sopha.) }
\livretVerse#8 { Surcroît d’honneur et de richesse ! }

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center {
  Calpigi, Tarare en muet, Spinette assise, voilée, son mouchoir sur les yeux.
}
\livretPersDidas Calpigi (à Tarare d’un ton sévère)
\livretRef#'EEArecit
\livretVerse#8 { Cette femme est à toi, muet ! }
\livretDidasPPage (Il sort.)

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center { Tarare, Spinette. }
\livretPersDidas Spinette (à part, voilée)
\livretRef#'EFArecit
\livretVerse#4 { Comme il est laid !… }
\livretVerse#8 { Cependant il n’est point mal fait. }
\livretDidasPPage\wordwrap { (Tarare se met à genoux à six pas d’elle.) }
\livretVerse#10 { Il se prosterne ! il n’a point l’air farouche }
\livretVerse#8 { Des autres monstres de ces lieux. }
\livretVerse#8 { Muet, votre respect me touche ; }
\livretVerse#8 { Je lis votre amour dans vos yeux : }
\livretVerse#8 { Un tendre aveu de votre bouche, }
\livretVerse#8 { Ne pourrait me l’exprimer mieux. }
\livretPersDidas Tarare (à part, se relevant)
\livretVerse#8 { Grands dieux ! ce n’est point Astasie, }
\livretVerse#8 { Et mon cœur allait s’exhaler ! }
\livretVerse#8 { De m’être abstenu de parler, }
\livretVerse#8 { Ô Brama ! je te remercie. }
\livretPersDidas Spinette (à part)
\livretVerse#8 { On croirait qu’il se parle bas. }
\livretVerse#8 { Chaque animal a son langage. }
\livretDidasPPage\wordwrap { (Elle se dévoile ; Tarare la regarde.) }
\livretVerse#12 { De loin, je le veux bien, contemplez mes appas. }
\livretVerse#8 { Je voudrais pouvoir davantage ; }
\livretVerse#10 { Mais un monarque, un calife, un sultan, }
\livretVerse#10 { Le plus parfait, comme le plus puissant, }
\livretVerse#12 { Ne peut rien sur mon cœur, il est tout à Tarare. }
\livretPersDidas Tarare (s’écrie)
\livretVerse#14 { A Tarare !… }
\livretPers Spinette
\livretVerse#14 { \transparent { A Tarare !… } Il me parle ! }
\livretPers Tarare
\livretVerse#14 { \transparent { A Tarare !… Il me parle ! } Ô transport qui m’égare ! }
\livretVerse#8 { Étonnement trop indiscret ! }
\livretPers Spinette
\livretVerse#8 { Un mot a trahi ton secret ! }
\livretVerse#8 { Tu n’es pas muet ? téméraire ! }
\livretDidasPPage\wordwrap { (Elle lui enlève son masque.) }
\livretPersDidas Tarare (à ses pieds)
\livretVerse#12 { Madame, hélas ! calmez une juste colère ! }
\livretPersDidas Spinette (d’un ton plus doux)
\livretVerse#12 { Imprudent ! quel espoir a pu te faire oser… }
\livretPersDidas Tarare (timidement)
\livretVerse#12 { Ah ! c’est en m’accusant, que je dois m’excuser. }
\livretRef#'EFBtarare
\livretVerse#12 { Étranger dans Ormus, hier on me vint dire }
\livretVerse#8 { Que le maître de cet empire }
\livretVerse#12 { Donnait à son amante une fête au sérail… }
\livretVerse#8 { J’ai cru, sous ce vile attirail, }
\livretVerse#8 { Dans la nuit pouvoir m’introduire… }
\livretPers Spinette
\livretVerse#12 { Ah ! quel bonheur ! Eh bien, curieux étranger, }
\livretVerse#8 { Quand le désir de me connaître }
\livretVerse#8 { T’engage en un si grand danger, }
\livretVerse#8 { À mes yeux crains-tu de paraître ? }
\livretVerse#8 { Ce n’est point sous ce masque affreux }
\livretVerse#8 { Qu’un imprudent peut être heureux. }
\livretVerse#12 { C’est un homme charmant. }
\livretPers Tarare
\livretVerse#12 { \transparent { C’est un homme charmant. } Ah ! fuyons de ces lieux. }
\livretPersDidas Spinette (légèrement)
\livretRef#'EFCduo
\livretVerse#8 { Ami, ton courage m’éclaire. }
\livretVerse#8 { Si Tarare aimait à me plaire, }
\livretVerse#8 { Il eût tout bravé comme toi. }
\livretVerse#8 { J’oublierai qu’il obtint ma foi : }
\livretVerse#8 { C’en est fait, mon cœur te préfère ; }
\livretVerse#8 { Tu seras Tarare pour moi. }
\livretPersDidas Tarare (troublé)
\livretVerse#8 { Quoi ! Tarare obtint votre foi ! }
\livretPers Spinette
\livretVerse#8 { C’en est fait, mon cœur te préfère. }
\livretPers Tarare
\livretVerse#8 { C’est moi que votre cœur préfère ? }
\livretPers Spinette
\livretVerse#8 { Tu seras Tarare pour moi. }
\livretPers Tarare
\livretVerse#8 { Est-ce un songe ! ô Brama, veillé-je ? }
\livretVerse#8 { Tout ce que j’entends me confond. }
\livretVerse#8 { Atar, toi que la haine assiège, }
\livretVerse#8 { M’as-tu conduit de piège en piège }
\livretVerse#8 { Dans un abîme aussi profond ! }
\livretPers Spinette
\livretVerse#8 { Ce n’est point un piège ; non, non : }
\livretVerse#4 { De son pardon }
\livretVerse#4 { Je te réponds. }
\livretDidasPPage\wordwrap { (Elle voit entrer des soldats.) }
\livretVerse#6 { Ciel ! on vient l’arrêter ! }
\livretPers TARARE
\livretVerse#6 { Tout espoir m’abandonne. }
\livretDidasPPage\wordwrap { (Elle se voile, et rentre précipitament.) }

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\wordwrap-center {
  Tarare démasqué, Urson, Soldats armés de massues, Calpigi, Eunuques
  entrant de l'autre côté.
}
\livretPers Urson
\livretRef#'EGAchoeur
\livretVerse#8 { Marchez, soldats, doublez le pas. }
\livretPers Calpigi
\livretVerse#8 { Quoi ! des soldats ! n’avancez pas. }
\livretPersDidas Urson (aux soldats)
\livretVerse#8 { Suivez l’ordre que je vous donne. }
\livretPersDidas Calpigi (aux eunuques)
\livretVerse#8 { Ne laissez avancer personne. }
\livretPers Chœur de Soldats
\livretVerse#4 { Doublons le pas. }
\livretPers Urson
\livretVerse#4 { Doublez le pas. }
\livretPers Chœur d'Eunuques
\livretVerse#4 { N’avancez pas. }
\livretVerse#8 { Pour tous, cette enceinte est sacrée. }
\livretPers Chœur de Soldats
\livretVerse#8 { Notre ordre est d’en forcer l’entrée. }
\livretPers Calpigi
\livretRef#'EGBrecit
\livretVerse#12 { Urson, expliquez-vous. }
\livretPers Urson
\livretVerse#12 { \transparent { Urson, expliquez-vous. } Le sultan agité, }
\livretVerse#12 { Sur l’effet d’un courroux qu’il a trop écouté, }
\livretVerse#10 { Veut que l’affreux muet soit massolé, }
\livretVerse#10 { Jeté dans la mer, et pour sépulture, }
\livretVerse#8 { Y serve aux monstres de pâture. }
\livretPers Calpigi
\livretVerse#12 { Le voici : de sa mort, Urson, je prends le soin. }
\livretVerse#12 { Les jardins du sérail sont commis à ma garde ; }
\livretVerse#12 { Mes eunuques sont prêts. }
\livretPers Urson
\livretVerse#12 { \transparent { Mes eunuques sont prêts. } Pour que rien ne retarde, }
\livretVerse#8 { Son ordre est que j’en sois témoin. }
\livretVerse#8 { Marchez soldats, qu’on s’en empare. }
\livretDidasPPage (Les soldats lèvent la massue.)
\livretPers Calpigi
\livretVerse#12 { Ce n’est point un muet. }
\livretPers Urson
\livretVerse#12 { \transparent { Ce n’est point un muet. } Quel qu’il soit. }
\livretPersDidas Calpigi (crie)
\livretVerse#12 { \transparent { Ce n’est point un muet. Quel qu’il soit. } C’est Tarare. }
\livretPers Urson
\livretVerse#8 { Tarare !… }
\livretDidasPPage\wordwrap {
  (Les Soldats et les Eunuques reculent par respect.)
}
\livretPers Chœur de Soldats et d'Eunuques
\livretVerse#8 { \transparent { Tarare !… } Tarare ! Tarare ! }
\livretPers Calpigi
\livretVerse#12 { Un tel coupable, Urson, devient trop important, }
\livretVerse#12 { Pour qu’on l’ose frapper sans l’ordre du sultan. }
\livretDidasPPage (À Tarare, à part.)
\livretVerse#12 { En suspendant leurs coups, je te sauve peut-être. }
\livretPersDidas Urson (avec douleur)
\livretVerse#12 { Tarare infortuné ! qui peut le désarmer ? }
\livretVerse#12 { Nos larmes contre toi vont encor l’animer ! }
\livretPers Chœur
\livretVerse#12 { Tarare infortuné ! qui peut le désarmer ? }
\livretVerse#12 { Nos larmes, contre toi, vont encor l’animer ! }
\livretPers Tarare
\livretVerse#12 { Ne plaignez point mon sort, respectez votre maître ; }
\livretVerse#8 { Puissiez-vous un jour l’estimer ! }
\livretDidasPPage (On emmene Tarare.)
\livretPersDidas Urson (bas à Calpigi)
\livretVerse#12 { Calpigi, songe à toi ; la foudre est sur deux têtes. }
\livretDidasPPage (Il sort.)

\livretScene SCÈNE HUITIÈME
\livretDescAtt\wordwrap-center {
  Calpigi seul, d’un ton décidé.
}
\livretPers Calpigi
\livretRef#'EHArecit
\livretVerse#12 { Sur deux têtes la foudre, et l’on m’ose nommer ! }
\livretVerse#12 { Elle en menace trois, Atar, et ces tempêtes, }
\livretVerse#12 { Que ta haine alluma, pourront te consumer. }
\null
\livretRef#'EHBcalpigi
\livretVerse#8 { Va ! l’abus du pouvoir suprême, }
\livretVerse#8 { Finit toujours par l’ébranler : }
\livretVerse#8 { Le méchant, qui fait tout trembler, }
\livretVerse#8 { Est bien près de trembler lui-même. }
\livretVerse#8 { Cette nuit, despote inhumain, }
\livretVerse#8 { Tarare excitait ta furie ; }
\livretVerse#8 { Ta haine menaçait sa vie, }
\livretVerse#8 { Quand la tienne était dans sa main ! }
\livretVerse#8 { Vas ! l’abus du pouvoir suprême }
\livretVerse#8 { Finit toujours par l’ébranler : }
\livretVerse#8 { Le méchant qui fait tout trembler }
\livretVerse#8 { Est bien près de trembler lui-même. }
\livretDidasPPage (Il sort.)
\sep
\livretAct ACTE CINQUIÈME
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\column {
  \justify {
    Le théâtre représente une cour intérieur du palais d’Atar.
    Au milieu est un bûcher ; au pied du bûcher, un billot, des chaînes,
    des haches, des massues, et autres instruments d’un supplice.
  }
  \wordwrap-center { Atar, eunuques, suite. }
}
\livretDidasP\wordwrap {
  (Atar examine avec avidité le bûcher et tous les apprêts du supplice de Tarare.)
}
\livretPers Atar
\livretRef#'FAAatar
\livretVerse#10 { Fantôme vain ! idole populaire, }
\livretVerse#10 { Dont le nom seul excitait ma colère, }
\livretVerse#10 { Tarare !… enfin tu mourras cette fois ! }
\livretVerse#8 { Ah ! pour Atar, quel bien céleste, }
\livretVerse#8 { D’immoler l’objet qu’il déteste, }
\livretVerse#8 { Avec le fer souple des loix ! }
\livretPersDidas Atar (aux Eunuques)
\livretVerse#12 { Trouve-t-on Calpigi ? }
\livretPers Un Eunuque
\livretVerse#12 { \transparent { Trouve-t-on Calpigi ? } Seigneur, on suit sa trace. }
\livretPers Atar
\livretVerse#12 { À qui l’arrêtera, je donnerai sa place. }
\livretDidasPPage\line { (Les Eunuques sortent en courant.) }

\livretScene SCÈNE DEUXIÈME
\livretDescAtt\column {
  \wordwrap-center { Atar, Arthénée. }
  \justify {
    Deux files des prêtres le suivent ; l’une en blanc, dont le premier
    prêtre porte un drapeau blanc, où sont écrits, en lettres d’or,
    ces mots : la vie.
    L’autre file de prêtres est en noir, couverte de crêpes, dont le
    premier prêtre porte un drapeau noir, où sont écrits ces mots,
    en lettres d’argent : la mort
  }
}
\livretPersDidas Arthénée (s’avance, bien sombre)
\livretRef#'FBArecit
\livretVerse#12 { Que veux-tu, roi d’Ormus ? et quel nouveau malheur }
\livretVerse#12 { Te force d’arracher un père à sa douleur ? }
\livretPers Atar
\livretVerse#10 { Ah ! si l’espoir d’une prompte vengeance }
\livretVerse#10 { Peut l’adoucir, reçois-en l’assurance. }
\livretVerse#8 { Dans mon sérail on a surpris }
\livretVerse#8 { L’affreux meurtrier de ton fils. }
\livretVerse#8 { Je tiens la victime enchaînée, }
\livretVerse#12 { Et veux que par toi-même elle soit condamnée. }
\livretVerse#8 { Dis un mot, le trépas l’attend. }
\livretPers Arthénée
\livretVerse#8 { Atar, c’était en l’arrêtant… }
\livretVerse#8 { Sans avoir l’air de la connaître, }
\livretVerse#8 { Il fallait poignarder le traître : }
\livretVerse#8 { Je tremble qu’il ne soit trop tard ! }
\livretVerse#8 { Chaque instant, le moindre retard, }
\livretVerse#8 { Sur ton bras peut fermer le piège. }
\livretPers Atar
\livretVerse#8 { Quel démon, quel dieu le protège ? }
\livretVerse#8 { Tout me confond de cette part ! }
\livretPers Arthénée
\livretVerse#8 { Son démon, c’est une âme forte, }
\livretVerse#8 { Un cœur sensible et généreux, }
\livretVerse#8 { Que tout émeut, que rien n’emporte ; }
\livretVerse#8 { Un tel homme est bien dangereux ! }

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center {
  Atar, Arthénée, Tarare enchaîné, Soldats, Esclaves, Suite,
  Prêtres de la vie et de la mort.
}
\livretPers Atar
\livretRef#'FCArecit
\livretVerse#12 { Approche, malheureux ! viens subir le supplice, }
\livretVerse#12 { Qu’un crime irrémissible arrache à ma justice. }
\livretPers Tarare
\livretVerse#12 { Qu’elle soit juste ou non, je demande la mort. }
\livretVerse#10 { De tes plaisirs j’ai violé l’asile, }
\livretVerse#12 { Sans y trouver l’objet d’une audace inutile, }
\livretVerse#10 { Mon Astasie !… Ô ce fourbe Altamort ! }
\livretVerse#10 { Il l’a ravie à mon séjour champêtre, }
\livretVerse#8 { Sans la présenter à son maître ! }
\livretVerse#8 { Trahissant tout, honneur, devoir… }
\livretVerse#10 { Il a payé sa double perfidie ; }
\livretVerse#10 { Mais ton Irza n’est point mon Astasie. }
\livretPersDidas Atar (avec fureur)
\livretVerse#8 { Elle n’est pas en mon pouvoir ? }
\livretDidasPPage (Aux Eunuques.)
\livretVerse#12 { Que l’on m’amène Irza. Si ta bouche en impose, }
\livretVerse#8 { Je la poignarde devant toi. }
\livretPers Tarare
\livretVerse#8 { La voir mourir est peu de chose ; }
\livretVerse#8 { Tu te puniras, non pas moi. }
\livretPers Atar
\livretVerse#8 { De sa mort la tienne suivie… }
\livretPersDidas Tarare (fièrement)
\livretRef#'FCBtarare
\livretVerse#8 { Je ne puis mourir qu’une fois. }
\livretVerse#8 { Qu’en je m’engageai sous tes lois, }
\livretVerse#8 { Atar, je te donnai ma vie ; }
\livretVerse#8 { Elle est toute entière à mon roi ; }
\livretVerse#8 { Au lieu de la perdre pour toi, }
\livretVerse#8 { C’est par toi qu’elle m’est ravie. }
\livretVerse#8 { J’ai rempli mon sort, suis ton choix ; }
\livretVerse#8 { Je ne puis mourir qu’une fois. }
\livretVerse#12 { Mais souhaite qu’un jour ton peuple te pardonne. }
\livretPers Atar
\livretVerse#9 { Une menace ? }
\livretPers Tarare
\livretVerse#9 { \transparent { Une menace ? } Il s’en étonne ! }
\livretVerse#8 { Roi féroce ! as-tu donc compté, }
\livretVerse#8 { Parmi les droits de ta couronne, }
\livretVerse#10 { Celui du crime et de l’impunité ? }
\livretVerse#8 { Ta fureur ne peut se contraindre, }
\livretVerse#8 { Et tu veux n’être pas haï ! }
\livretVerse#8 { Tremble d’ordonner… }
\livretPers Atar
\livretVerse#8 { \transparent { Tremble d’ordonner… } Qu’ai-je à craindre ? }
\livretPers Tarare
\livretVerse#8 { De te voir toujours obéi ; }
\livretVerse#10 { Jusqu’à l’instant où l’effrayante somme }
\livretVerse#10 { De tes forfaits déchaînant leur courroux… }
\livretVerse#8 { Tu pouvais tout contre un seul homme ; }
\livretVerse#8 { Tu ne pourras rien contre tous. }
\livretPers Atar
\livretVerse#13 { Qu’on l’entoure ! }
\livretDidasP\wordwrap {
  (Les Esclaves l'entourent. Tarare va s'assessoir sur le billot,
  au pied du bûcher, la tête appuyé sur ses mains, et ne regard plus rien.)
}

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\wordwrap-center {
  Astasie voilée, Atar, Arthénée, Tarare, Spinette, Esclaves des deux sexes, Soldats.
}
\livretPersDidas Atar (à Astasie)
\livretRef#'FDArecit
\livretVerse#13 { \transparent { Qu’on l’entoure ! } Ainsi donc, abusant de vos charmes, }
\livretVerse#8 { Fausse Irza, par de feintes larmes, }
\livretVerse#8 { Vous triomphiez de me tromper ? }
\livretVerse#8 { Je prétends, avant de frapper, }
\livretVerse#10 { Savoir comment ma puissance jouée… }
\livretPers Spinette
\livretVerse#12 { Une esclave fidèle, hélas ! substituée, }
\livretVerse#12 { Innocemment causa le désordre et l’erreur. }
\livretPersDidas Tarare \line { (à part, tenant sa tête dans ses mains) }
\livretVerse#8 { Ah ! cette voix me fait horreur ! }
\livretPers Atar
\livretVerse#10 { Il est donc vrai, cet échange funeste ! }
\livretVerse#8 { J’adorais sous le nom d’Irza… }
\livretDidasPPage (À Astasie.)
\livretVerse#8 { Va, malheureuse, je déteste }
\livretVerse#10 { L’indigne amour qui pour toi m’embrasa. }
\livretVerse#12 { À la rigueur des loix, avec lui, sois livrée ! }
\livretDidasPPage (Au grand prêtre.)
\livretVerse#8 { Pontife, décidez leur sort. }
\livretPers Arthénée
\livretVerse#12 { Ils sont jugés : levez l’étendard de la mort. }
\livretVerse#12 { De leurs jours criminels la trame est déchirée. }
\livretDidasP\justify {
  Le grand-prêtre déchire la bannière de la vie. Le prêtre en deuil
  élève la bannière de la mort. On entend un bruit funèbre d'instruments
  déguisés.
}
\livretDidasP\justify {
  (Astasie se jette à genoux, et prie pendant le chœur. On apporte au
  grand-prêtre le livre des arrêts, couvert d'un crêpe. Il signe l'arrêt
  de mort. Deux enfants en deuil lui remettent chacun un
  flambeau. Quatre prêtres en deuil lui présentent deux grands vases
  pleins d'eau lustrale. Il éteint dans ces vases le deux flambeaux en
  les renversant.  Pendant ce temps, les prêtres de la vie se retirent en
  silence. Le drapeau de la vie déchiré, traîne a terre.)
}
\livretPers Chœur funèbre des Esclaves
\livretRef#'FDBchoeur
\livretVerse#8 { Avec tes décrets infinis, }
\livretVerse#8 { Grand dieu, si ta bonté s’accorde, }
\livretVerse#8 { Ouvre à ces coupables punis }
\livretVerse#8 { Le sein de ta miséricorde ! }
\livretPersDidas Arthénée (prie)
\livretVerse#12 { Brama ! de ce bûcher, par la mort réunis, }
\livretVerse#12 { Ils montent vers le ciel ; qu’ils n’en soient point bannis ! }
\livretDidasP\wordwrap {
  (Astasie se releve, et s'avance au bûcher, où Tarare est abîmé de douleur.)
}
\livretPersDidas Astasie (à Tarare)
\livretRef#'FDCrecit
\livretVerse#8 { Ne m’impute pas, étranger, }
\livretVerse#8 { Ta mort que je vais partager. }
\livretPersDidas Tarare (se relève avec feu)
\livretVerse#8 { Qu’entends-je ? Astasie ! }
\livretPers Astasie
\livretVerse#8 { \transparent { Qu’entends-je ? Astasie ! } Ah ! Tarare ! }
\livretDidasPPage\wordwrap {
  (Ils se jettent dans les bras l'un de l'autre.)
}
\livretPers Arthénée
\livretVerse#10 { Je te l’avais prédit. }
\livretPersDidas Atar (furieux)
\livretVerse#10 { \transparent { Je te l’avais prédit. } Qu’on les sépare. }
\livretVerse#8 { Qu’un seul coup les fasse périr. }
\livretVerse#8 { Non… C’est trop tôt briser leurs chaînes ; }
\livretVerse#8 { Ils seraient heureux de mourir. }
\livretVerse#10 { Ah ! je me sens altéré de leurs peines, }
\livretVerse#8 { Et j’ai soif de les voir souffrir. }
\livretPersDidas Astasie (avec dédain, au roi)
\livretRef#'FDDastasie
\livretVerse#12 { Ô tigre ! mes dédains ont trompé ton attente, }
\livretVerse#12 { Et, malgré toi, je goûte un instant de bonheur : }
\livretVerse#8 { J’ai bravé ta faim dévorante, }
\livretVerse#8 { Le rugissement de ton cœur. }
\livretVerse#8 { Pour prix de ta lâche entreprise, }
\livretVerse#12 { Vois, Atar, je l’adore, et toi, je te méprise. }
\livretDidasPPage (Elle embrasse Tarare.)
\livretPersDidas Atar (vivement aux Soldats)
\livretVerse#8 { Arrachez-la tous de ses bras. }
\livretVerse#8 { Courez. Qu’il meure et qu’elle vive ! }
\livretPersDidas Astasie \line { (tire un poignard, qu’elle approche de son sein) }
\livretVerse#8 { Si quelqu’un vers lui fait un pas, }
\livretVerse#8 { Je suis morte avant qu’il arrive. }
\livretPersDidas Atar (aux Soldats)
\livretVerse#10 { Arrêtez-vous ! }
\livretPers Astasie, Tarare, Atar
\livretRef#'FDEtrio
\livretVerse#10 { \transparent { Arrêtez-vous ! } Le trépas nous attend… }
\livretPers Tarare, Astasie
\livretVerse#6 { Encore une minute, }
\livretVerse#6 { Et notre amour constant }
\livretVerse#6 { Ne sera plus en butte }
\livretVerse#6 { Aux coups d’un noir sultan. }
\livretDidasPPage (Les Soldats font un mouvement.)
\livretPers Atar
\livretVerse#6 { Arrêtez un moment ! }
\livretPers Astasie
\livretVerse#6 { Je me frappe à l’instant }
\livretVerse#6 { Que sa loi s’exécute. }
\livretVerse#6 { Sur ton cœur palpitant, }
\livretVerse#6 { Tu sentiras ma chute, }
\livretVerse#6 { Et tu mourras content. }
\livretPers Atar
\livretVerse#6 { O rage ! affreux tourment ! }
\livretVerse#6 { C’est moi, c’est moi qui lutte, }
\livretVerse#6 { Et leur cœur est content. }
\livretPers Astasie
\livretVerse#6 { Sur ton cœur palpitant }
\livretVerse#6 { Tu sentiras ma chute, }
\livretVerse#6 { Et tu mourras content. }
\livretPers Tarare
\livretVerse#6 { Sur mon cœur palpitant }
\livretVerse#6 { Je sentirai ta chute, }
\livretVerse#6 { Et je mourrai content. }

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center {
  Acteurs précédents. Une foule d’esclaves des deux sexes,
  accourt avec frayeur, et se serre à genoux autour d’Atar.
}
\livretPers Chœur d'esclaves effrayés
\livretRef#'FEAchoeur
\livretVerse#8 { Atar, défends-nous, sauve-nous. }
\livretVerse#8 { Du palais la garde est forcée ; }
\livretVerse#8 { Du sérail la porte enfoncée. }
\livretVerse#8 { Notre asyle est à tes genoux ; }
\livretVerse#12 { Ta milice en fureur redemande Tarare. }

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center {
  Les précédents, toute la Milice le sabre à la main, Calpigi, Urson.
}
\livretDidasP\wordwrap {
  (Les prêtres de la mort se retirent.)
}
\livretPersDidas Chœur de Soldats (furieux. Ils renversent le bûcher)
\livretRef#'FFAchoeur
\livretVerse#8 { Tarare, Tarare, Tarare ; }
\livretVerse#8 { Rendez-nous notre général. }
\livretVerse#8 { Son trépas, dit-on, se prépare. }
\livretVerse#8 { S’il a reçu le coup fatal, }
\livretVerse#8 { Vengeons sa mort sur ce barbare. }
\livretDidasPPage\wordwrap { (Ils s’avancent vers Atar.) }
\livretPersDidas Tarare (enchaîné, écarte les Esclaves)
\livretVerse#8 { Arrêtez, soldats, arrêtez. }
\livretVerse#8 { Quel ordre ici vous a portés ? }
\livretVerse#8 { Ô l’abominable victoire ! }
\livretVerse#12 { On sauverait mes jours, en flétrissant ma gloire ! }
\livretVerse#8 { Un tas de rebelles mutins }
\livretVerse#8 { De l’État ferait les destins ! }
\livretVerse#8 { Est-ce à vous de juger vos maîtres ? }
\livretVerse#8 { N’ont-ils soudoyé que des traîtres ? }
\livretVerse#12 { Oubliez-vous, soldats, usurpant le pouvoir, }
\livretVerse#12 { Que le respect des rois est le premier devoir ? }
\livretVerse#12 { Armes bas, furieux ! votre empereur vous casse. }
\livretDidasPPage\wordwrap { (Ils se jettent tous à genoux.) }
\livretVerse#12 { Seigneur, ils sont soumis ; je demande leur grâce. }
\livretPersDidas Atar (hors de lui)
\livretVerse#12 { Quoi ! toujours ce fantôme entre mon peuple et moi ! }
\livretDidasPPage (aux Soldats.)
\livretVerse#12 { Défenseurs du sérail, suis-je encor votre roi ? }
\livretPers Un Eunuque
\livretVerse#12 { Oui. }
\livretPersDidas Calpigi (le menace du sabre)
\livretVerse#12 { \transparent { Oui. } Non. }
\livretPersDidas Tous les Soldats (se levent)
\livretVerse#12 { \transparent { Oui. Non. } Non. }
\livretPers Tout le Peuple
\livretVerse#12 { \transparent { Oui. Non. Non. } Non. }
\livretPersDidas Calpigi (montrant Tarare)
\livretVerse#12 { \transparent { Oui. Non. Non. Non. } C’est lui. }
\livretPers Tarare
\livretVerse#12 { \transparent { Oui. Non. Non. Non. C’est lui. } Jamais. }
\livretPers Les Soldats
\livretVerse#12 { \transparent { Oui. Non. Non. Non. C’est lui. Jamais. } C’est toi. }
\livretPers Tout le Peuple
\livretVerse#12 { \transparent { Oui. Non. Non. Non. C’est lui. Jamais. C’est toi. } C’est toi. }
\livretPersDidas Atar (avec désespoir, à Tarare)
\livretVerse#12 { Monstre !… Ils te sont vendus… Règne donc à ma place. }
\livretDidasPPage\wordwrap { (Il se poignarde, et tombe.) }
\livretPersDidas Tarare (avec douleur)
\livretVerse#12 { Ah ! malheureux ! }
\livretPersDidas Atar (se relève dans les angoisses)
\livretVerse#12 { \transparent { Ah ! malheureux ! } La mort est moins dure à mes yeux… }
\livretVerse#12 { Que de régner par toi… sur ce peuple odieux. }
\livretDidasPPage\justify {
  (Il tombe mort dans les bras des Eunuques, qui l’emportent. Urson les suit.)
}

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\wordwrap-center {
  Les acteurs précédents, excepté Atar et Urson.
}
\livretPersDidas Calpigi (crie au peuple)
\livretRef#'FGAchoeur
\livretVerse#12 { Tous les torts de son règne, un seul mot les répare : }
\livretVerse#8 { Il laisse le trône à Tarare. }
\livretPersDidas Tarare (vivement)
\livretVerse#8 { Et moi je ne l’accepte pas. }
\livretPersDidas Chœur général (exalté)
\livretVerse#12 { Tous les torts de son règne, un seul mot les répare : }
\livretVerse#8 { Il laisse le trône à Tarare. }
\livretPersDidas Tarare (avec dignité)
\livretRef#'FGBtarare
\livretVerse#8 { Le trône est pour moi sans appas : }
\livretVerse#8 { Je ne suis point né votre maître. }
\livretVerse#8 { Vouloir être ce qu’on n’est pas, }
\livretVerse#10 { C’est renoncer à tout ce qu’on peut être. }
\livretVerse#8 { Je vous servirai de mon bras : }
\livretVerse#10 { Mais laissez-moi finir en paix ma vie }
\livretVerse#10 { Dans la retraite avec mon Astasie. }
\livretDidasPPage\justify {
  (Il lui tend les bras, elle s'y jette.)
}

\livretScene SCÈNE HUITIÈME
\livretDescAtt\wordwrap-center {
  Les acteurs précédents, Urson tenant dans sa main la couronne d’Atar.
}
\livretPersDidas Urson (prend la chaîne de Tarare)
\livretRef#'FHArecit
\livretVerse#8 { Non, par mes mains, le peuple entier }
\livretVerse#8 { Te fait son noble prisonnier : }
\livretVerse#12 { Il veut que de l’État tu saisisses les rênes. }
\livretVerse#8 { Si tu rejetais notre foi, }
\livretVerse#8 { Nous abuserions de tes chaînes }
\livretVerse#8 { Pour te couronner malgré toi. }
\livretDidasPPage (Au grand-prêtre.)
\livretVerse#12 { Pontife, à ce grand homme, Atar lègue l’Asie ; }
\livretVerse#12 { Consacrez le seul bien qu’il ait fait de sa vie : }
\livretVerse#12 { Prenez le diadème, et réparez l’affront }
\livretVerse#12 { Que le bandeau des rois a reçu de son front. }
\livretPersDidas Arthénée (prenant le diadème des mains d’Urson)
\livretRef#'FHBChoeur
\livretVerse#6 { Tarare, il faut céder ! }
\livretPersDidas Tout le Peuple (s’écrie)
\livretVerse#6 { Tarare, il faut céder ! }
\livretPers Arthénée
\livretVerse#6 { Leurs désirs sont extrêmes. }
\livretPers Tout le Peuple
\livretVerse#6 { Nos désirs sont extrêmes. }
\livretPers Arthénée
\livretVerse#6 { Sois donc le roi d’Ormus. }
\livretPers Tout le Peuple
\livretVerse#6 { Sois, sois le roi d’Ormus. }
\livretPers Arthénée
\livretDidasPPage\justify {
  (lui met la couronne sur la tête au bruit d'une fanfare)
}
\livretVerse#6 { Il est des dieux suprêmes. }
\livretDidasPPage (Il sort.)

\livretScene SCÈNE NEUVIÈME
\livretDescAtt\wordwrap-center {
  Tous les précédents, excepté le grand-prêtre.
}
\livretPersDidas Tarare (pendant qu’on le déchaîne)
\livretRef#'FIArecit
\livretVerse#12 { Enfants, vous m’y forcez, je garderai ces fers ; }
\livretVerse#12 { Ils seront à jamais ma royale ceinture. }
\livretVerse#12 { De tous mes ornements devenus les plus chers, }
\livretVerse#12 { Puissent-ils attester à la race future }
\livretVerse#12 { Que, du grand nom de roi si j’acceptai l’éclat, }
\livretVerse#12 { Ce fut pour m’enchaîner au bonheur de l’État ! }
\livretDidasPPage\justify { (Il s’enveloppe le corps de ses chaînes.) }
\livretPersDidas Chœur général (avec ivresse)
\livretRef#'FIBchoeur
\livretVerse#8 { Quel plaisir de nos cœurs s’empare ! }
\livretVerse#8 { Vive notre grand roi Tarare ! }
\livretVerse#8 { Tarare, Tarare, Tarare ! }
\livretVerse#8 { La belle Astasie et Tarare ! }
\livretVerse#8 { Nous avons le meilleur des rois : }
\livretVerse#8 { Jurons de mourir sous ses lois. }
\livretDidasP\justify {
  Des mouvements d‘une joie effrénée, sort une danse tumultueuse,
  pendant que le chœur répète, à grands cris, les vers ci-dessus. Ils
  entourent, ils entraînent Astasie et le roi. La musique diminue de
  bruit, change d’effet, et reprend un caractère aérien. Des nuages
  couvrent le spectacle ; on en voit sortir dans les air, La Nature
  productrice, et le Génie qui préside au soleil.
}

\livretScene SCÈNE DIXIÈME
\livretDescAtt\wordwrap-center {
  Les précédents, La Nature et Le Génie du Feu sur les nuages.
}
\livretPers Le Génie du Feu
\livretRef#'FJArecit
\livretVerse#12 { Nature, quel exemple imposant et funeste ! }
\livretVerse#12 { Le soldat monte au trône, et le tyran est mort ! }
\livretPers La Nature
\livretVerse#8 { Les dieux ont fait leur premier sort : }
\livretVerse#8 { Leur caractère a fait le reste. }
\livretPers Le Génie du Feu
\livretVerse#8 { Encore un généreux effort. }
\livretVerse#12 { Dans le cœur des humains, d’un trait inaltérable, }
\livretVerse#8 { Gravons ce précepte admirable. }
\livretPersDidas Chœur général (très éloigné)
\livretRef#'FJBchoeur
\livretVerse#8 { De ce grand bruit, de cet éclat, }
\livretVerse#8 { Ô ciel ! apprends-nous le mystère ! }
\livretPersDidas La Nature, le Génie du Feu \line { (Dans les nuages, à l’unisson, et parlant fortement.) }
\livretVerse#12 { Mortel, qui que tu sois, prince, brame ou soldat ; }
\livretVerse#8 { Homme ! ta grandeur sur la terre, }
\livretVerse#8 { N’appartient point à ton état ; }
\livretVerse#8 { Elle est toute à ton caractère. }
\livretDidasP\justify {
  À mesure que la Nature et le Génie prononcent les verses ci-dessus,
  ils se peignent en caractéres de feu, dans les nuages.
  Le trompettes sonnent ; le tonnerre reprend ; les nuages les couvrent ;
  il disparaissent. La toile tombe.
}

\livretScene Je proposerais cette fin…
\livretDescAtt\justify {
  Dans un siècle et dans un pays où l’on regarderait comme un manque de respect pour l’opéra,
  de le finir autrement que par une fête, je proposerais cette fin, quoique je préfère la première.
}
\livretDidasP Après le chœur :
\livretVerse#8 { Quel plaisir de nos cœurs s’empare ! }
\livretVerse#8 { Vive notre grand roi Tarare ! etc. }
\livretPersDidas Urson (viendrait dire)
\livretRef#'FKArecit
\livretVerse#12 { Les fiers Européans marchent vers ces états ; }
\livretVerse#12 { Inaugurons Tarare, et courons au combat. }
\livretPers Urson, Calpigi
\livretRef#'FKBchoeur
\livretVerse#8 { Roi, nous mettons la liberté }
\livretVerse#8 { Aux pieds de ta vertu suprême. }
\livretVerse#8 { Règne sur ce peuple qui t’aime, }
\livretVerse#8 { Par les lois et par l’équité. }
\livretPers Deux Femmes
\livretVerse#8 { Et vous, reine, épouse sensible, }
\livretVerse#8 { Qui connûtes l’adversité, }
\livretVerse#8 { Du devoir souvent inflexible }
\livretVerse#8 { Adoucissez l’austérité. }
\livretVerse#8 { Tenez son grand cœur accessible }
\livretVerse#8 { Aux soupirs de l’humanité. }
\livretPers Chœur général
\livretVerse#8 { Roi, nous mettons la liberté }
\livretVerse#8 { Aux pieds de ta vertu suprême ; }
\livretVerse#8 { Règne sur ce peuple qui t’aime, }
\livretVerse#8 { Par les lois et par l’équité. }
\livretDidasP\wordwrap {
  Danse générale, et la toile tomberait.
}
\livretDidasP\justify {
  Cette fin est mise en musique par M. Salieri. Mais je préfère la première,
  qui est bien plus philosophique, et encadre mieux le sujet.
  Choisissez ; ma tâche est finie.
}
}
