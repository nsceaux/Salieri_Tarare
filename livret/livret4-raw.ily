\livretAct ACTE QUATRIÈME
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\column {
  \justify {
    Le théâtre représente l’intérieur de l’appartement d’Astasie.
    C’est un salon superbe, garni de sophas et autres meubles orientaux.
  }
  \wordwrap-center {
    Astasie, Spinette.
  }
}
\livretRef#'EAArecit
\livretDidasPPage\line { (Astasie entre, en grand désordre.) }
\livretPers Astasie
%# Spinette, comment fuir de cette *horrible enceinte?
\livretPers Spinette
%# Calmez le désespoir dont votre âme est atteinte.
\livretPersDidas Astasie (égarée, les bras élevés)
%# Ô mort! termine mes douleurs;
%# Le crime se prépare.
%# Arrache au plus grand des malheurs,
%# L'épouse de Tarare.
%# Il semblait que je pressentais
%# Leur entreprise infâme!
%# Quand il partit, je répétais,
%# Hélas! l'effroi dans l'âme!
%# Cru=el! pour qui j'ai tant souffert,
%# C'est trop que ton absence
%# Laisse Astasie =en un désert,
%# Sans joie =et sans défense!
%# L'imprudent n'a pas écouté
%# Sa compagne éplorée:
%# Aux mains d'un brigand détesté
%# Des brigands l'ont livrée,
%# Ô mort! termine mes douleurs:
%# Le crime se prépare.
%# Arrache au plus grand des malheurs,
%# L'épouse de Tarare.
\livretPers Spinette
\livretRef#'EABrecit
%# Un grand roi vous invite à faire son bonheur.
%# L'amour met à vos pieds le maître de la terre.
%# Que de beautés ici brigueraient cet honneur!
%# Loin de s'en alarmer, on peut en être fière.
\livretPersDidas Astasie (pleurant)
%# Ah! vous n'avez pas eu Tarare pour amant!
\livretPers Spinette
%# Je ne le connais point; j'aime sa renommée;
%# Mais, pour lui, comme vous, si j'étais enflammée,
%# Avec le dur Atar je feindrais un moment;
%# Et j'instruirais Tarare au moins de ma souffrance.
\livretPers Astasie
%# À la plus légère espérance
%# Le cœur des malheureux s'ouvre facilement.
%# J'aime ton noble attachement:
%# He bien! fais-lui savoir qu'en cette enceinte *horrible…
\livretPers Spinette
%# Cachez vos pleurs, s'il est possible.
%# Des secrets plaisirs du sultan
%# Je vois venir le ministre insolent.
\livretDidasPPage\justify {
  (Astasie essuie ses yeux, et se remet de son mieux.)
}

\livretScene SCÈNE DEUXIÈME
\livretDescAtt\wordwrap-center {
  Calpigi, Spinette, Astasie.
}
\livretPersDidas Calpigi (d’un ton dur)
\livretRef#'EBArecit
%# Belle Irza, l'empereur ordonne
%# Qu'en ce moment vous receviez la foi
%# D'un nouvel époux qu'il vous donne.
\livretPers Astasie
%# Un époux! un époux à moi?
\livretPersDidas Spinette (le contrefait)
%# Commandant d'un corps ridicule!
%# Abrège-nous ton grave pré=ambule.
%# Ce nouvel époux, quel est-il?
\livretPers Calpigi
%# C'est du sérail le mu=et le plus vil.
\livretPers Astasie
%#- Un mu=et!
\livretPers Spinette
%#- Un mu=et!
\livretPers Astasie
%#= J'expire.
\livretPers Calpigi
%# L'ordre est que chacun se retire.
\livretPers Spinette
%#- Moi?
\livretPers Calpigi
%#- Vous.
\livretPers Spinette
%#- Moi?
\livretPers Calpigi
%#= Vous; vous, Spinette; il y va des jours
%# De qui troublerait leurs amours.
\livretPers Astasie
%#- O juste ciel!
\livretPersDidas Spinette (raillant)  
%#= Dis à ton maître
%# Que le grand-prêtre
%# Sera sans doute assez surpris,
%# Qu'à la pluralité des femmes,
%# On ose ajouter, chez les Brames,
%# La pluralité des maris.
\livretPersDidas Calpigi (ironiquement)  
%# Votre conseil au roi paraîtra d'un grand prix.
%#- J'en ferai votre cour.
\livretPersDidas Spinette (du même ton)
%#= Vous l'oublierez peut-être?
\livretPers Calpigi
%#- Non.
\livretPers Spinette
%#= Vous le rendrez mieux, l'ay=ant deux fois appris.
\livretDidasPPage (elle répete :)
%# Dis à ton maître,
%# Que le grand-prêtre
%# Sera sans doute assez surpris,
%# Qu'à la pluralité des femmes,
%# On ose ajouter, chez les Brames,
%# La pluralité des maris.
\livretDidasPPage (Calpigi sort.)

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center { Astasie, Spinette. }
\livretPersDidas Astasie (au désespoir)
\livretRef#'ECArecit
%# Ô ma compagne! ô mon amie!
%# Sauve-moi de cette infamie.
\livretPers Spinette
%# Eh! comment vous prouver ma foi?
\livretPers Astasie
%# Prends mes di=amants, ma parure:
%# Je te les donne, ils sont à toi.
\livretDidasPPage (Elle les détache.)
%# Ah! dans cette *horrible aventure,
%# Sois Irza, représente-moi;
%# On réprime un mu=et sans peine.
\livretPers Spinette
%# Si c'est Calpigi qui l'amène,
%# Madame, il me reconnaîtra.
\livretPersDidas Astasie (ôte son manteau royal)
%# Ce long manteau te couvrira.
%# Souviens-toi de Tarare, et nomme-le sans cesse;
%# Son nom seul te garantira.
\livretPersDidas Spinette (pendant qu’on l’habille)  
\livretRef#'ECBspinette
%# Je partage votre détresse.
%# Hélas! que ne ferais-je pas,
%# Pour sauver d'un dangereux pas,
%# Mon incomparable maîtresse!
\livretDidasPPage (Astasie sort.)

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\wordwrap-center { Spinette seule. }
\livretRef#'EDArecit
%# Spinette, allons, point de faiblesse!
%# Le roi dans peu te sera gré,
%# D'avoir adroitement paré
%# Le coup qu'il porte à sa maîtresse.
\livretDidasPPage\line { (Elle s’assied sur un sopha.) }
%# Surcroît d'*honneur et de richesse!

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center {
  Calpigi, Tarare en muet, Spinette assise, voilée, son mouchoir sur les yeux.
}
\livretPersDidas Calpigi (à Tarare d’un ton sévère)
\livretRef#'EEArecit
%# Cette femme est à toi, mu=et!
\livretDidasPPage (Il sort.)

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center { Tarare, Spinette. }
\livretPersDidas Spinette (à part, voilée)  
\livretRef#'EFArecit
%# Comme il est laid !…
%# Cependant il n'est point mal fait.
\livretDidasPPage\wordwrap { (Tarare se met à genoux à six pas d’elle.) }
%# Il se prosterne! il n'a point l'air farouche
%# Des autres monstres de ces lieux.
%# Mu=et, votre respect me touche;
%# Je lis votre amour dans vos yeux:
%# Un tendre aveu de votre bouche,
%# Ne pourrait me l'exprimer mieux.
\livretPersDidas Tarare (à part, se relevant)
%# Grands dieux! ce n'est point Astasie,
%# Et mon cœur allait s'exhaler!
%# De m'être abstenu de parler,
%# Ô Brama ! je te remercie.
\livretPersDidas Spinette (à part)
%# On croirait qu'il se parle bas.
%# Chaque animal a son langage.
\livretDidasPPage\wordwrap { (Elle se dévoile ; Tarare la regarde.) }
%# De loin, je le veux bien, contemplez mes appas.
%# Je voudrais pouvoir davantage;
%# Mais un monarque, un calife, un sultan,
%# Le plus parfait, comme le plus puissant,
%# Ne peut rien sur mon cœur, il est tout à Tarare.
\livretPersDidas Tarare (s’écrie)  
%#- A Tarare!…
\livretPers Spinette
%#- Il me parle!
\livretPers Tarare
%#= Ô transport qui m'égare!
%# Étonnement trop indiscret!
\livretPers Spinette
%# Un mot a trahi ton secret!
%# Tu n'es pas mu=et? téméraire!
\livretDidasPPage\wordwrap { (Elle lui enlève son masque.) }
\livretPersDidas Tarare (à ses pieds)
%# Madame, *hélas! calmez une juste colère!
\livretPersDidas Spinette (d’un ton plus doux)
%# Imprudent! quel espoir a pu te faire oser…
\livretPersDidas Tarare (timidement)
%# Ah! c'est en m'accusant, que je dois m'excuser.
\livretRef#'EFBtarare
%# Étranger dans Ormus, hi=er on me vint dire
%# Que le maître de cet empire
%# Donnait à son amante une fête au sérail…
%# J'ai cru, sous ce vile attirail,
%#8 Dans la nuit pouvoir m'introduire…
\livretPers Spinette
%# Ah! quel bonheur! Eh bien, curi=eux étranger,
%# Quand le désir de me connaître
%# T'engage en un si grand danger,
%# À mes yeux crains-tu de paraître?
%# Ce n'est point sous ce masque affreux
%# Qu'un imprudent peut être *heureux.
%#- C'est un homme charmant.
\livretPers Tarare
%#= Ah! fuy=ons de ces lieux.
\livretPersDidas Spinette (légèrement)
\livretRef#'EFCduo
%# Ami, ton courage m'éclaire.
%# Si Tarare aimait à me plaire,
%# Il eût tout bravé comme toi.
%# J'oublierai qu'il obtint ma foi:
%# C'en est fait, mon cœur te préfère;
%# Tu seras Tarare pour moi.
\livretPersDidas Tarare (troublé)
%# Quoi! Tarare obtint votre foi!
\livretPers Spinette
%# C'en est fait, mon cœur te préfère.
\livretPers Tarare
%# C'est moi que votre cœur préfère?
\livretPers Spinette
%# Tu seras Tarare pour moi.
\livretPers Tarare
%# Est-ce un songe! ô Brama, veillé-je?
%# Tout ce que j'entends me confond.
%# Atar, toi que la haine assiège,
%# M'as-tu conduit de piège en piège
%# Dans un abîme aussi profond!
\livretPers Spinette
%# Ce n'est point un piège; non, non:
%# De son pardon
%# Je te réponds.
\livretDidasPPage\wordwrap { (Elle voit entrer des soldats.) }
%# Ciel! on vient l'arrêter!  
\livretPers TARARE
%# Tout espoir m'abandonne.
\livretDidasPPage\wordwrap { (Elle se voile, et rentre précipitament.) }

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\wordwrap-center {
  Tarare démasqué, Urson, Soldats armés de massues, Calpigi, Eunuques
  entrant de l'autre côté.
}
\livretPers Urson
\livretRef#'EGAchoeur
%# Marchez, soldats, doublez le pas.
\livretPers Calpigi
%# Quoi! des soldats! n'avancez pas.
\livretPersDidas Urson (aux soldats)
%# Suivez l'ordre que je vous donne.
\livretPersDidas Calpigi (aux eunuques)
%# Ne laissez avancer personne.
\livretPers Chœur de Soldats
%# Doublons le pas.
\livretPers Urson
%# Doublez le pas.
\livretPers Chœur d'Eunuques
%# N'avancez pas.
%# Pour tous, cette enceinte est sacrée.
\livretPers Chœur de Soldats
%# Notre ordre est d'en forcer l'entrée.
\livretPers Calpigi
\livretRef#'EGBrecit
%#- Urson, expliquez-vous.
\livretPers Urson
%#= Le sultan agité,
%# Sur l'effet d'un courroux qu'il a trop écouté,
%# Veut que l'affreux mu=et soit massolé,
%# Jeté dans la mer, et pour sépulture,
%# Y serve aux monstres de pâture.
\livretPers Calpigi
%# Le voici: de sa mort, Urson, je prends le soin.
%# Les jardins du sérail sont commis à ma garde;
%#- Mes eunuques sont prêts.
\livretPers Urson
%#= Pour que rien ne retarde,
%# Son ordre est que j'en sois témoin.
%# Marchez soldats, qu'on s'en empare.
\livretDidasPPage (Les soldats lèvent la massue.)
\livretPers Calpigi
%#- Ce n'est point un mu=et.
\livretPers Urson
%#- Quel qu'il soit.
\livretPersDidas Calpigi (crie)
%#= C'est Tarare.
\livretPers Urson
%#- Tarare !…
\livretDidasPPage\wordwrap {
  (Les Soldats et les Eunuques reculent par respect.)	
}
\livretPers Chœur de Soldats et d'Eunuques
%#= Tarare! Tarare!
\livretPers Calpigi
%# Un tel coupable, Urson, devient trop important,
%# Pour qu'on l'ose frapper sans l'ordre du sultan.
\livretDidasPPage (À Tarare, à part.)
%# En suspendant leurs coups, je te sauve peut-être.
\livretPersDidas Urson (avec douleur)
%# Tarare infortuné! qui peut le désarmer?
%# Nos larmes contre toi vont encor l'animer!
\livretPers Chœur
%# Tarare infortuné! qui peut le désarmer?
%# Nos larmes, contre toi, vont encor l'animer!
\livretPers Tarare
%# Ne plaignez point mon sort, respectez votre maître;
%# Puissiez-vous un jour l'estimer!
\livretDidasPPage (On emmene Tarare.)	
\livretPersDidas Urson (bas à Calpigi)
%# Calpigi, songe à toi; la foudre est sur deux têtes.
\livretDidasPPage (Il sort.)

\livretScene SCÈNE HUITIÈME
\livretDescAtt\wordwrap-center {
  Calpigi seul, d’un ton décidé.
}
\livretPers Calpigi
\livretRef#'EHArecit
%# Sur deux têtes la foudre, et l'on m'ose nommer!
%# Elle en menace trois, Atar, et ces tempêtes,
%# Que ta haine alluma, pourront te consumer.
\null
\livretRef#'EHBcalpigi
%# Va! l'abus du pouvoir suprême,
%# Finit toujours par l'ébranler:
%# Le méchant, qui fait tout trembler,
%# Est bien près de trembler lui-même.
%# Cette nuit, despote inhumain,
%# Tarare excitait ta furie;
%# Ta haine menaçait sa vie,
%# Quand la tienne était dans sa main!
%# Vas! l'abus du pouvoir suprême
%# Finit toujours par l'ébranler:
%# Le méchant qui fait tout trembler
%# Est bien près de trembler lui-même.
\livretDidasPPage (Il sort.)
\sep
