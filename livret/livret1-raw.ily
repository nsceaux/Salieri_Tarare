\livretAct ACTE PREMIER
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\column {
  \justify {
    Les nuages qui couvrent le thèâtre s'élèvent ;
    on voit une salle du palais d'Atar.
  }
  \wordwrap-center { Atar, Calpigi. }
}
\livretPersDidas Atar (en entrant, violemment)
\livretRef#'BABrecit
%#- Laisse-moi, Calpigi!
\livretPers Calpigi
%#= La fureur vous égare.
%# Mon maître! ô roi d'Ormus! grâce, grâce à Tarare!
\livretPers Atar
%# Tarare! encor Tarare! Un nom abject et bas,
%# Pour ton organe impur, a donc bien des appas!
\livretPers Calpigi
%%# Quand sa troupe nous prit, au fond d'un antre sombre,
%%# Je défendais mes jours contre ces inhumains,
%%# Blessé, prêt à périr, accablé par le nombre;
%%# Cet homme généreux m'arracha de leurs mains.
%# Je lui dois d'être à vous, seigneur, faites-lui grâce.
\livretPers Atar
%# Qui, moi, je souffrirais qu'un soldat eût l'audace
%# D'être toujours heureux, quand son roi ne l'est pas!
\livretPers Calpigi
%# A travers le torrent d'Arsace,
%# Il vous a sauvé du trépas;
%# Et vous l'avez nommé chef de votre milice.
\livretPers Atar
%# Ah! combien je l'ai regretté!
%# Son orgueilleuse *humilité,
%# Le respect d'un peuple *hébété,
%# Son air, jusqu'à son nom… Cet homme est mon supplice.
%# Où trouve-t-il, dis-moi, cette félicité?
%# Est-ce dans le travail, ou dans la pauvreté?
\livretPers Calpigi
%# Dans son devoir. Il sert avec simplicité
%# Le ciel, les malheureux, la patrie =et son maître.
\livretPers Atar
%# Lui? c'est un humble fastu=eux,
%# Dont l'orgueil est de le paraître:
%# L'*honneur d'être cru vertu=eux
%# Lui tient lieu du bonheur de l'être:
%# Il n'a jamais trompé mes yeux.
\livretPers Calpigi
%#- Vous tromper, lui, Tarare!
\livretPers Atar
%#= Ici la loi des Brames,
%# Permet à tous un grand nombre de femmes;
%# Il n'en a qu'une, et s'en croit plus *heureux,
%# Mais nous l'aurons cet objet de ses vœux;
%# En la perdant il gémira peut-être.
\livretPers Calpigi
%#- Il en mourra!
\livretPers Atar
%#= Tant mieux. Oui, le fils du grand-prêtre,
%# Altamort a reçu mon ordre cette nuit.
%# Il vole à la rive opposée,
%# Avec sa troupe déguisée:
%# En son absence il va dévaster son réduit.
%# Il ravira sur-tout son Astasie,
%# Ce miracle, dit-on, des beautés de l'Asie.
\livretPers Calpigi
%# Eh! quel est donc son crime, *hélas!
\livretPers Atar
%# D'être *heureux, Calpigi, quand son roi ne l'est pas,
%# De faire partout ses conquêtes
%# Des cœurs que j'avais autrefois…
\livretPers Calpigi
%# Ah! pour tourner toutes les têtes,
%# Il faut si peu de chose aux rois!
\livretPers Atar
%# D'avoir, par un manège *habile,
%# Entraîné le peuple imbécile.
\livretPers Calpigi
\livretRef#'BACcalpigi
%# Il est vrai, son nom adoré,
%# Dans la bouche de tout le monde,
%# Est un proverbe révéré.
%# Parle-t-on des fureurs de l'onde,
%# Ou du flé=au le plus fatal;
%# Tarare! est l'écho général:
%# Comme si ce nom secourable
%# Eloignait, rendait incroy=able
%# Le mal, *hélas! le plus certain…
\livretPersDidas Atar (en colère)
%# Finiras-tu, méprisable chrétien?
%# Eunuque vil et détestable;
%#- La mort devrait…
\livretPers Calpigi
%#= La mort, la mort, toujours la mort!
%# Ce mot éternel me désole:
%# Terminez une fois mon sort;
%# Et puis cherchez qui vous console
%# Du triste ennui de la satiété,
%# De l'oisiveté,
%# De la roy=auté.
\livretDidasPPage (Il s'eloigne.)
\livretPersDidas Atar (furieux)
%# Je punirai cet excès d'arrogance.

\livretScene SCÈNE DEUXIÈME
\livretDescAtt\wordwrap-center { Les précédents, Altamort. }
\livretPers Atar
\livretRef#'BBArecit
%# Mais qu'annonce Altamort, à mon impati=ence?  
\livretPers Altamort
%# Mon maître est obé=i; tout est fait, rien n'est su.
\livretPers Atar
%#- Astasie?
\livretPers Altamort
%#= Est à toi, sans qu'on m'ait apperçu,
%# Sans qu'elle ait deviné qui la veut, qui l'enlève.
\livretPers Atar
%# Au rang de mes vizirs, Altamort, je t'élève.
\livretDidasPPage (à Calpigi)
%# Pour la bien recevoir sont-ils tous préparés?
%# Le serrail est-il prêt, les jardins décorés,
%#- Calpigi?
\livretPers Calpigi
%#- Tout, Seigneur.
\livretPers Atar
%#= Qu'une superbe fête,
%# Demain, de ma grandeur enivre ma conquête.
\livretPers Calpigi
%#- Demain? Le terme est court.
\livretPersDidas Atar (en colère)
%#- Malheureux!
\livretPersDidas Calpigi (víte)
%#= Vous l'aurez.
\livretPers Atar
%# J'ai parlé: tu m'entends? S'il manque quelque chose…
\livretPers Calpigi
%# Manquer! chacun sait trop à quel mal il s'expose.

\livretScene SCÈNE TROISIÈME
\livretDescAtt\column {
  \justify {
    Tous les acteurs précédents, Spinette, Odalisques, Esclaves du
    serrail des deux sexes.
  }
  \justify {
    Tout le serrail entre et se range en haie; quatre esclaves noir
    portent Astasie couverte d'un grand voile noir, de la tête aux pieds.
  }
}
\livretPers Chœur d'esclaves du serrail
\livretDidasPPage (On dance pendant le chœur.)
\livretRef#'BCAchoeur
%# Dans les plus beaux lieux de l'Asie,
%# Avec la suprême grandeur,
%# L'amour met aux pieds d'Astasie,
%# Tout ce qui donne le bonheur.
%# Ce n'est point dans l'*humble retraite,
%# Qu'un cœur généreux le ressent;
%# Et la beauté la plus parfaite,
%# Doit régner sur le plus puissant.
\livretPers Atar
\livretDidasPPage (On la dévoile.)
%# Que tout s'abaisse devant elle.
\livretPers Astasie
\livretRef#'BCBrecit
\livretDidasPPage (On se prosterne.)
%# O sort affreux, dont l'*horreur me poursuit!
%# Du sein d'une profonde nuit,
%#8 Quelle clarté triste et nouvelle…
%# Où suis-je? Tout mon corps chancelle.
\livretPers Spinette
%#- Dans le palais d'Atar.
\livretPers Atar
%#= Calpigi, qu'elle est belle!
\livretPersDidas Astasie (se levant)
%# Dans le palais d'Atar! Ah! quelle indignité!
\livretPersDidas Atar (s’approche)
%#- D'Atar qui vous adore.
\livretPers Astasie
%#= Et c'est la récompense,
%# Ô mon époux, de ta fidélité!
\livretPers Atar
%# Mes bienfaits laveront cette légère offense.
\livretPers Astasie
%# Quoi, cru=el! par cet attentat,
%# Vous pay=ez la foi d'un soldat
%# Qui vous a conservé la vie!
%# Vous lui ravissez Astasie!
\livretDidasPPage (Levant les mains au ciel.)
%# Grand Dieu! ton pouvoir infini,
%# Laissera-t-il donc impuni
%# Ce crime atroce d'un parjure,
%# Et la plus exécrable injure!
%#- Ô Brama! Dieu vengeur!…
\livretDidasPPage\wordwrap {
  (Elle s'évanouit. Des femmes la soutiennent. On l'assied.)
}
\livretPers Calpigi
%#= Quel effray=ant transport!
\livretPersDidas Un esclave (accourant)
%# Le voile de la mort a couvert sa paupière.
\livretPers Atar
%# Quoi! malheureux! tu m'annonces sa mort!
%#- Meurs, toi-même.
\livretDidasPPage\wordwrap {
  (Il le poignard. Courant vers Astasie.)
}
%#= Et vous tous, rendez à la lumière
%# L'objet de mon funeste amour.
%# À sa douleur tremblez qu'il ne succombe;
%# Répondez-moi de son retour,
%# Ou je lui fais de tous une *horrible *hécatombe.
\livretDidasPPage\wordwrap {
  (Revenant à elle apperçoit l'esclave renversé, qu'on enlève.)
}
\livretPers Astasie
%# Dieux! quel spectacle a glacé mes esprits!
\livretPers Atar
\livretRef#'BCCatar
%# Je suis heureux, vous êtes ranimée.
%# Un lâche esclave par ses cris,
%# M'alarmait sur ma bien-aimée;
%# De son vil sang la terre est arrosée:
%# Un coup de poignard est le prix
%# De la fray=eur qu'il m'a causée.
\livretPersDidas Astasie (joignant les mains)  
%# Ô Tarare! ô Brama! Brama!
\livretDidasPPage (Elle retombe, on l’assied.)	
\livretPers Atar
%# Dans le serrail qu'on la transporte:
%# Que cent eunuques, à sa porte,
%# Attendent les ordres d'Irza.
\livretDidasP\justify {
  (Le nom d'Irza signifie: « La plus belle fleur des plus belles fleurs
  ècloses aux premiers soleils du primtems de l'Orient de l'Asie ». Tant
  les langues orientales ont d'avantages sur les nôtres. Lisez les Mille
  et une nuit, et tous le contes arabes.)
}
%# C'est le doux nom qu'à ma belle j'impose;  
%# C'est mon Irza, plus fraîche que la rose
%# Que je tenais lorqu'elle m'embrasa.
\livretDidasPPage\wordwrap { 	
  (Les esclaves noirs portent Astasie dans le serrail; tous la suivent.)
}

\livretScene SCÈNE QUATRIÈME
\livretDescAtt\wordwrap-center {
  Atar, Calpigi, Altamort, Spinette.
}
\livretPers Calpigi
\livretRef#'BDArecit
%# Qui voulez-vous, Seigneur, auprès d'elle qu'on mette?
\livretPers Atar
%#- L'Europé=ane; allez.
\livretPers Calpigi
%#= L'intrigante Spinette?
\livretPers Atar
%#- Elle-même.
\livretPers Calpigi
%#= En effet, nulle ici ne sait mieux
%# Comment il faut réduire un cœur né scrupuleux.
\livretPers Spinette
\livretRef#'BDBspinette
%# Oui, Seigneur, je veux la réduire,  
%# Vous livrer son cœur, et l'instruire
%# Du respect, du retour qu'elle doit à vos feux.
\livretDidasPPage (Montrant Calpigi.)
%# Et… si ce grand succès consterne
%# Le chef… puissant qui vous gouverne,
%# Mon maître appréciera le zèle de tous deux.
\livretPers Atar
%# Je l'enchaîne à tes pieds, si tu remplis mes vœux.
\livretDidasPPage\justify {	
  (Spinette et Calpigi sortent en se menaçant.)
}

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center {
  Urson, Atar, Altamort, Esclaves.
}
\livretPers Urson
\livretRef#'BEArecit
%#12 Seigneur, c'est ce guerrier, du peuple la merveille…
\livretPers Atar
%# Garde-toi que son nom offense mon oreille!
\livretPers Urson
%# Il pleure; autour de lui tout le peuple empressé
%# Dit tout haut, qu'en ses vœux il doit être exaucé.
\livretPers Atar
%# Tu dis qu'il pleure, qu'il soupire?
\livretPers Urson
%# Ses traits en sont presque effacés.
\livretPers Atar
%# Urson, qu'il entre; c'est assez.
\livretDidasPPage (à Altamort)
%# Il est malheureux… Je respire.

\livretScene SCÈNE SIXIÈME
\livretDescAtt\wordwrap-center {
  Tarare, Altamort, Atar.
}
\livretPers Atar
\livretRef#'BFArecit
%# Que me veux-tu, brave soldat?  
\livretPersDidas Tarare (avec un grand trouble)
%# Ô mon roi! prends pitié de mon affreux état.
%# En pleine paix, un avare corsaire
%# Comble sur moi les horreurs de la guerre.
%# Tous mes jardins sont ravagés,
%# Mes esclaves sont égorgés,
%# L'*humble toit de mon Astasie
%# Est consumé par l'incendie…
\livretPers Atar
%# Grâce au Ciel, mes serments vont être dégagés!
%# Soldat qui m'as sauvé la vie,
\livretRef#'BFBatar
%# Reçois en pur don ce palais
%# Que dix mille esclaves malais
%# Ont construit d'ivoire et d'ébène:
%# Ce palais, dont l'aspect ri=ant
%# Domine la fertile plaine,
%# Et la vaste mer d'orient.
%# Là, cent femmes de Circassie,
%# Pleines d'attraits et de pudeur,
%# Attendront l'ordre de ton cœur,
%# Pour t'enivrer des trésors de l'Asie.
%# Puisse de ton bonheur l'envi=eux s'irriter!
%# Puisse l'infame calomnie,
%# Pour te perdre, en vain s'agiter!…
\livretPersDidas Altamort (bas)  
%# Mais, seigneur, ta hautesse oublie…
\livretPersDidas Atar (bas)
%# Je l'élève, Altamort, pour le précipiter.
\livretDidasPPage (haut)
%# Allez, vizir, que l'on publie…
\livretPers Tarare
%# Ô mon roi! ta bonté doit se faire adorer.
%# Des maux du sort mon âme est peu saisie;
%# Mais celui de mon cœur ne peut se réparer,
%# Le barbare emmène Astasie.
\livretPersDidas Atar (avec un signe d'intelligence)
%# Quelle est cette femme, Altamort?
\livretPers Altamort
%# Seigneur, si j'en crois son transport,
%# Quelque esclave jeune et jolie.
\livretPers Tarare
%# Une esclave! une esclave! excuse, ô roi d'Ormus!
%# A ce nom odi=eux tous mes sens sont émus.
\livretRef#'BFCtarare
%# Astasie =est une dé=esse.
%# Dans mon cœur souvent combattu,
%# Sa voix sensible, enchanteresse,
%# Faisait tri=ompher la vertu.
%# D'une ardeur toujours renaissante,
%# J'offrais sans cesse à sa beauté,
%# Sans cesse à sa beauté touchante,
%# L'encens pur de la volupté.
%# Elle tenait mon âme active
%# Jusque dans le sein du repos:
%# Ah! faut-il que ma voix plaintive
%# En vain la demande aux échos?
\livretPers Atar
\livretRef#'BFDrecit
%# Quoi! soldat! pleurer une femme!
%# Ton roi ne te reconnaît pas.
%# Si tu perds l'objet de ta flamme,
%# Tout un serrail t'ouvre ses bras.
%# Pour une beauté, quelques charmes,
%# On peut retrouver mille attraits,
%# Mais l'*honneur qu'on perd dans les larmes,
%# On ne le retrouve jamais!
\livretPersDidas Tarare (suppliant)
%#- Seigneur!
\livretPers Atar
\livretRef#'BFEatar
%#= Qu'as-tu donc fait de ton mâle courage?
%# Toi qu'on voy=ait rugir dans les combats,
%# Toi qui forças un torrent à la nage,
%# En transportant ton maître dans tes bras!
%# Le fer, le feu, le sang et le carnage
%# N'ont jamais pu t'arracher un soupir;
%# Et l'abandon d'une esclave volage
%# Abat ton âme et la force à gémir!
\livretPersDidas Tarare (vivement)
\livretRef#'BFFtarare
%# Seigneur, si j'ai sauvé ta vie,
%# Si tu daignes t'en souvenir,
%# Laisse-moi venger Astasie
%# Du traître qui l'osa ravir.
%# Permets que, déploy=ant ses ailes,
%# Un léger vaisseau de transport
%# Me mène vers ces infidèles,
%# Chercher Astasie =ou la mort.

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\wordwrap-center {
  Calpigi, Atar, Altamort, Tarare.
}
\livretPers Atar
\livretRef#'BGArecit
%#- Que veux-tu, Calpigi?
\livretDidasPPage (bas)
%#= Sois inintelligible.
\livretPers Calpigi
%# Mon maître, cette Irza si chère à ton amour…
\livretPers Atar
%#- Eh bien?
\livretPers Calpigi
%#= Elle est rendue =à la clarté du jour.
\livretPersDidas Tarare (exalté)
%# Atar, ta grande âme est sensible,
%# La joie =a brillé dans tes yeux.
\livretDidasPPage (Un genou en terre.)
%# Par cette Irza, sultan, sois généreux,
%# À mes maux deviens accessible.
\livretPers Atar
%# Dis-moi, Tarare, es-tu bien malheureux?
\livretPers Tarare
%# Si je le suis! ah! peut-être elle expire!
\livretPers Atar
%# Souhaite devant moi qu'Irza cède à mes vœux:
%# Je fais ce que ton cœur désire.
\livretPersDidas Calpigi (à part)
%# Grand dieux! je sers un *homme affreux!
\livretPersDidas Tarare (se levant, dit avec feu)
\livretRef#'BGBtarare
%# Charmante Irza, qu'est-ce donc qui t'arrête?
%# Le fils des dieux n'est-il pas ta conquête?
%# Puisse-t-il trouver dans tes yeux
%# Ce pur feu dont il étincelle!
%# Rends, Irza, rends mon maître *heureux…
\livretDidasPPage\wordwrap {
  (Calpigi lui fait un signe négatif pour qu'il n'achève pas son vœu.)
}
%#10 …si tu le peux sans etre criminelle.
\livretPers Atar
\livretRef#'BGCatar
%# Brave Altamort, avant le point du jour,
%# Demain qu'une escadre soit prête
%# À partir du pied de la tour.
%# Suis mon soldat, sers son amour
%# Dans les combats, dans la tempête.
\livretDidasPPage (Bas à Altamort.)
%# S'il revoit jamais ce séjour,
%# Tu m'en répondras sur ta tete.
\livretDidasPPage (à Tarare.)
%# Et toi, jusqu'à cette conquete,
%# De tout service envers ton roi,
%# Soldat, je dégage ta foi;
%#- J'en jure par Brama.
\livretPersDidas Tarare (la main au sabre)
%#= Je jure en sa présence,
%# De ne poser ce fer sanglant,
%# Qu'après avoir, du plus lâche brigand,
%# Puni le crime, et vengé mon offense.
\livretPersDidas Atar (à Atamort)
%# Tu viens d'entendre son serment;
%# Il touche a plus d'une existence:
%# Vole, Altamort, et plus prompt que le vent,
%# Reviens jou=ir de ma reconnaissance.
\livretPers Altamort
%# Noble roi, reçois le serment
%# De ma plus prompte obé=issance.
%# Commande, Atar, je cours aveuglément
%# Servir l'amour, la haine ou la vengeance.
\livretPersDidas Calpigi à part
%# De son danger, secrètement,
%# Il faut lui donner connaissance.
\livretDidasPPage\wordwrap {
  (Atar le regarde. Calpigi dit d'un ton courtisan.)
}
%# Qui sert mon maître, et le sert prudemment,
%# peut bien compter sur sa munificence.
\livretDidasPPage (Ils sortent tous.)

\livretScene SCÈNE HUITIÈME
\livretDescAtt\wordwrap-center { Atar seul. }
\livretRef#'BHAatar
%# Vertu farouche et fière,
%# Qui jetait trop d'éclat,
%# Rentre dans la poussière,
%# Faite pour un soldat.
%# Du crime d'Altamort je vois la mer chargée,
%# Rendre à ton corps sanglant les funèbres honneurs.
%# Et nous, heureux Atar, de ma belle affligée,
%# Dans la joie =et l'amour, nous sécherons les pleurs.
\livretDidasPPage (Il sort.)
\sep
