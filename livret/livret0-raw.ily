\livretAct PROLOGUE
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\column {
  \wordwrap-center {
    La Nature et les Vents déchainés.
  }
  \justify {
    L’ouverture fait entendre un bruit violent dans les airs, un choc
    terrible de tous les éléments. La toile, en se levant, ne montre que
    des nuages qui roulent, se déchirent, et laissent voir les Vents
    déchainés; ils forment, en tourbillonnant, des danses de la plus
    violente agitation.
  }
  \justify {
    La Nature s’avance au milieu d’eux, une baguette à la main, ornée de
    tous les attributs qui la caractérisent, et leur dit impérieusement :
  }
}
\livretPers La Nature
\livretRef#'AAAouverture
%# C'est assez troubler l'univers,  
%# Vents furi=eux, cessez d'agiter l'air et l'onde.
%# C'est assez. Reprenez vos fers;
%# Que le seul Zéphyr règne au monde.
\livretDidasP\wordwrap {
  L’ouverture, le bruit et le mouvement continuent.
}
\livretPers Chœur des Vents
%# Ne tourmentons plus l'univers:  
%# Cessons d'agiter l'air et l'onde.
%# Malheureux! reprenons nos fers,
%# L'*heureux Zéphyr seul règne au monde.
\livretDidasP\wordwrap {
  Ils se précipitent dans les nuages inférieurs. Le Zéphir s’élève dans
  les airs. L’ouverture et le bruit s’appaisent par degrés ; les nuages
  se dissipent ; tout devient harmonieux et calme. On voit une campagne
  superbe, et Le Génie du Feu descend dans un nuage brillant, du côté
  de l’orient.
}
\livretScene SCÈNE DEUXIÈME
\livretDescAtt\wordwrap-center {
  Le Génie du Feu, La Nature.
}
\livretPers Le Génie du Feu
\livretRef#'ABAgenieNature
%# De l'orbe éclatant du soleil,
%# Admirant des cieux la structure,
%# Je vous ai vue, belle Nature,
%# Disposer sur la terre un superbe appareil.
\livretPers La Nature
%# Génie ardent de la sphère enflammée,
%# Par qui la mienne est animée,
%# À mes travaux donnez quelques moments.
%# De toutes les races passées,
%# Dans l'immensité dispersées,
%# Je rassemble les éléments,
%# Pour en former une race prochaine
%# De la risible espèce *humaine,
%# Aux dépens des êtres vivants.
\livretPers Le Génie du Feu
%# Ce pouvoir absolu que vous avez sur elle,
%# L'exercez-vous aussi sur les individus?
\livretPers La Nature
%# Oui, si je descendais à quelque soins perdus !
%# Mais, pour moi, qu'est une parcelle,
%# À travers ces foules d'humains,
%# Que je répands à pleines mains
%# Sur cette terre, pour y naître,
%# Briller un instant, disparaître,
%# Laissant à des hommes nouveaux
%# Pressés comme eux, dans la carrière,
%# De main en main, les courts flambeaux
%# De leur existence éphémère ?
\livretPers Le Génie du Feu
%# Au moins, vous employ=ez des éléments plus purs,
%# Pour former les puissants et les grands d'un empire ?
\livretPers La Nature
%# C'est leur langage, il faut bien en sourire:
%# Un noble orgueil les en rend presque surs.
%# Mais voy=ez comme la Nature
%# Les verse par milliers, sans choix et sans mesure.
\livretDidasP\wordwrap { (Elle fait une espece de conjuration.) }
\livretRef #'ABBnature
%# Froids humains, non encor vivants;
%# Atômes perdus dans l'espace:
%# Que chacun de vos éléments,
%# Se rapproche et prenne sa place,
%# Suivant l'ordre, la pesanteur,
%# Et toutes les loix immu=ables
%# Que l'éternel dispensateur
%# Impose aux êtres vos semblables.
%# Humains, non encore existants,
%# À mes yeux paraissez vivants.

\livretScene SCÈNE TROISIÈME
\livretDescAtt\wordwrap-center {
  Le Génie du Feu, la Nature, foule d’Ombres des deux sexes.
}
\livretPers Chœur d'Ombres
\livretRef#'ACAchoeur
%# Quel charme inconnu nous attire?
%# Nos cœurs en sont épanou=is.
%# D'un plaisir vague je soupire;
%# Je veux l'exprimer; je ne puis.
%# En jou=issant, je sens que je désire;
%# En désirant, je sens que je jou=is.
%# Quel charme inconnu nous attire?
%# Nos cœurs en sont épanou=is.
\livretPersDidas Le Génie du Feu (à la Nature)
\livretRef#'ACBrecit
%# Privés des doux li=ens que donne la naissance;
%# Quels seront leurs rangs et leurs soins?
%# Et comment pourvoir aux besoins
%# D'une aussi soudaine croissance?
\livretPers La Nature
%# J'amuse vos yeux un moment,
%# De leur forme prématurée;
%# S'ils pouvaient aimer seulement,
%# Vous reverriez le règne *heureux d'Astrée.
\livretPers Le Génie du Feu
%# Quel intérêt peut les occuper tous?
\livretPers La Nature
%#- Nul, je crois.
\livretPersDidas Le Génie du Feu (s'adressant aux ombres)
%#= Qu'êtes-vous? et que demandez-vous?
\livretPersDidas Altamort (l'Ombre)
%# Nous ne demandons pas, nous sommes.
\livretPers Le Génie du Feu
%# Qui vous a mis au rang des hommes?
\livretPersDidas Arthénée (l'Ombre)
%# Qui l'a voulu, que nous importe à nous?
\livretPers Le Génie du Feu
%# Comme ils sont froids, sans passi=ons, sans gouts!
%# Que leur ignorance est profonde!
\livretPers La Nature
%# Ah! je les ai formés sans vous.
\livretRef#'ACCnature
%# Brillant Soleil, en vain la Nature est féconde;
%# Sans un ray=on de votre feu sacré,
%# Mon œuvre est morte et son but égaré.
\livretPers Le Génie du Feu
\livretRef#'ACDgenie
%# Gloire à l'éternelle sagesse,
%# Qui, cré=ant l'immortel amour,
%# Voulut que, par sa seule ivresse,
%# L'être sensible obtînt le jour.
%# Ah! si ma flamme ardente et pure
%# N'eût pas embrasé votre sein,
%# Stérile amant de la Nature,
%# J'eusse été formé sans dessein.
\livretRef #'ACErecit
%# Un mot encor; c'est une ombre femelle.
\livretDidasPPage (à l’ombre)
%# Aimable enfant, voulez-vous être belle?
\livretPers Une Ombre femelle
%#- Belle?
\livretPers Le Génie du Feu
%#- Vous rougissez!
\livretPers Une Ombre femelle
%#= Suis-je donc sans appas?
\livretPers Le Génie du Feu
%# Son instinct la trahit, mais ne la trompe pas!
\livretPersDidas La Nature (souriant)
%# Il peut au moins la compromettre.
\livretPersDidas Le Génie du Feu (à l'ombre de Spinette)
%# Et vous dont les regards causeront cent débats?
\livretPersDidas L'Ombre de Spinette (avec feu)
%# Je voudrais… je voudrais… je voudrais tout soumettre.
\livretPers Le Génie du Feu
%#- Ô! Nature!
\livretPersDidas La Nature (souriant)
%#= J'ai tort; devant vous j'ai trahi,
%# Sur ses plus doux secrets, mon sexe favori.
\livretPersDidas Le Génie du Feu (à l'ombre d'Astasie)
%# Mais vous, jeune beauté, qui semblez animée,
%# Voudri=ez-vous à tous donner aussi la loi?
\livretPers L'Ombre d'Astasie
%# Que je sois seulement aimée…
%# Il n'est que ce bonheur pour moi.
\livretPers La Nature
%# Tu le seras, sous le nom d'Astasie,
%# Et Tarare obtiendra ta foi.
\livretPersDidas Astasie (émue, la main sur son cœur)
%#- Tarare!
\livretPers La Nature
%#= Je te fais un sort digne d'envie.
\livretPers Astasie
%#- Je n'en sais rien.
\livretPers La Nature
%#= Moi, je le sais pour toi.
\livretPers Le Génie du Feu
%# Voy=ez quelle rougeur à ce nom l'a saisie!
\livretPersDidas La Nature (au Génie)
\livretRef#'ACFnature
%# Qu'un jeune cœur, malaisément,
%# Voile son trouble au doux moment
%# Où l'amour va s'en rendre maître!
%# Moi-même, après de longs hivers,
%# Quand vous ranimez l'univers,
%# Mes premiers soupirs font renaître
%# Les fleurs qui parfument les airs.	
\livretPersDidas Le Génie du Feu (montrant les deux ombres d'Atar et de Tarare)
\livretRef#'ACGrecit
%# Que sont ces deux superbes ombres,
%# Qui semblent menacer, taciturnes et sombres?
\livretPers La Nature
%# Rien: mais dites un mot; assignant leur état,
%# Je fais un roi de l'une et de l'autre un soldat.
\livretPers Le Génie du Feu
%# Permettez; ce grand choix les touchera peut-être.
\livretPers La Nature
%#- J'en doute.
\livretPers Le Génie du Feu
%#= Un de vous deux est roi: lequel veut l'être?
\livretPers Atar
%#- Roi ?
\livretPers Tarare
%#- Roi ?
\livretPers Atar et Tarare
%#= Je ne m'y sens aucun empressement.
\livretPers La Nature
%# Enfants, il vous manque de naître,
%# Pour penser bien différemment.
\livretPersDidas Le Génie du Feu (les examine)
%# Mon œil, entr'eux, cherche un roi préférable;
%# Mais que je crains mon jugement!
%# Nature, l'erreur d'un moment
%# peut rendre un siècle misérable.
\livretPersDidas La Nature (aux deux ombres)
\livretRef#'ACHnature
%# Futurs mortels, prosternez-vous,
%# Avec respect attendez en silence
%# Le rang qu'avant votre naissance,
%# Vous allez recevoir de nous.
\livretDidasP\justify {
  (Les deux ombres se prosternent ; et pendant que le Génie hésite
  dans son choix, toutes les ombres curieuses chantent le chœur
  suivant, en les enveloppant.)	
}
\livretPers Chœur d'Ombres
\livretRef#'ACIchoeur
%# Quittons nos jeux, accourrons tous:  
%# Deux de nos frères à genoux
%# Reçoivent l'arrêt de leur vie. 	
\livretPersDidas Le Génie du Feu (impose les mains à l'une des deux ombres)
\livretRef#'ACJrecit
%# Sois l'empereur Atar; despote de l'Asie,
%# Règne à ton gré dans le palais d'Ormus.
\livretDidasPPage (À l'autre ombre.)
%# Et toi, soldat, formé de parents inconnus,
%# Gémis longtemps de notre fantaisie.
\livretPers La Nature
%# Vous l'avez fait soldat; mais n'allez pas plus loin:
%# C'est Tarare. Bientôt vous serez le témoin
%# De leur dissemblance future.
\livretDidasPPage (Aux deux ombres.)
\livretRef#'ACKnature
%# Enfants, embrassez-vous: égaux par la nature,
%# Que vous en serez loin dans la soci=été:
%# De la grandeur altière à l'*humble pauvreté,
%# Cet intervalle immense est désormais le vôtre.
%# À moins que de Brama la puissante bonté,
%# Par un décret prémédité,
%# Ne vous rapproche l'un de l'autre,
%# Pour l'exemple des rois et de l'*humanité.
\livretPersDidas Quatre Ombres principales en chœur
\livretRef#'ACLchoeur
%# Ô bienfaisante dé=ité!
%# Ne souffrez pas que rien altère
%# Notre touchante égalité;
%# Qu'un homme commande à son frère!
\livretDidasP\justify {
  (L'ombre d'Atar seule ne chante pas, et s'éloigne avec hauteur ;
  le Génie du Feu la fait remarquer à la Nature.)	
}
\livretPersDidas La Nature (au Génie du Feu)
\livretRef#'ACMrecit
%# C'est assez. Éteignons en eux,
%# Ce germe d'une grande idée,
%# Faite pour des climats et des temps plus heureux.
\livretDidasPPage (À toutes les ombres.)
\livretRef#'ACNnature
%# Tels qu'une vapeur élancée,
%# Par le froid en eau condensée,
%# Tombe et se perd dans l'océ=an;
%# Futurs mortels, rentrez dans le né=ant.
%#- Disparaissez.
\livretDidasPPage (Au Génie du Feu.)
%#= Et nous, dont l'essence profonde
%# Dévore l'espace et le temps;
%# Laissons en un clin d'œil écouler quarante ans;
%# Et voy=ons-les agir sur la scène du monde.
\livretDidasP\justify {
  La Nature et le Génie du Feu s’élèvent dans les nuages,
  dont la masse redescend et couvre toute la scène.	
}
\livretPers Chœur d'Esprits aériens
\livretRef#'ACOchoeur
%# Gloire à l'éternelle sagesse,  
%# Qui, cré=ant l'éternel amour,
%# Voulut que par se seule ivresse
%# L'être sensible obtînt le jour.
\sep