\livretAct ACTE DEUXIÈME
\livretScene SCÈNE PREMIÈRE
\livretDescAtt\column {
  \justify {
    Le théâtre représente la place publique. Le palais d’Atar est sur le côté ;
    le temple de Brama, dans le fond. Atar sort de son palais avec toute sa suite.
    Urson sort du temple, suivi d’Arthenée en habits pontificaux.
  }
  \wordwrap-center { Urson, Atar. }
}
\livretPers Urson
\livretRef#'CAArecit
%# Seigneur, le grand-prêtre Arthénée  
%# Demande un entretien secret.
\livretPersDidas Atar (à sa suite)
%# Eloignez-vous… Qu'il vienne. Urson, que nul sujet,
%# Dans cette agré=able journée,
%# D'un seul refus d'Atar n'emporte le regret.

\livretScene SCÈNE DEUXIÈME
\livretDescAtt\wordwrap-center { Arthénée, Atar. }
\livretDidasP\wordwrap { Tout le monde s’éloigne du roi. }
\livretPersDidas Arthénée (s’avance)
\livretRef#'CBArecit
%# Les sauvages d'un autre monde,
%# Menacent d'envahir ces lieux;
%# Au loin déjà la foudre gronde;
%# Ton peuple superstitieux,
%# Pressé comme les flots, innonde
%# Le parvis sacré de nos dieux.
\livretPers Atar
%# De vils brigands une poignée,
%# Sortant d'une terre éloignée,
%# Pourrait-elle envahir ces lieux?
%# Pontife, votre âme étonnée…
%# Cependant, parlez, Arthénée,
%# Que dit l'interprète des dieux ?
\livretPersDidas Arthénée (vivement)
\livretRef#'CBBarthenee
%# Qu'il faut combattre,
%# Qu'il faut abattre
%# Un ennemi présomptu=eux:
%# Le sol aride
%# De la Torride
%# A soif de son sang odi=eux.
%# Par des mesures
%# Promptes et sûres,
%# Que l'armée =ait un commandant,
%# Vaillant, fidèle,
%# Rempli de zèle:
%# Mais sur ce devoir important,
%# Que le caprice
%# De ta milice
%# Ne règle point le choix d'Atar:
%# Que le murmure,
%# Comme une injure,
%# Soit puni d'un coup de poignard.
\livretPers Atar
\livretRef#'CBCrecit
%# Apprends-moi donc, ô chef des Brames!
%# Ce qu'Atar doit penser de toi.
%# Ardent zélateur de la foi
%# Du passage éternel des âmes!
%# Le plus vil animal est nourri de ta main;
%# Tu craindrais d'en purger la terre!
%# Et cependant, tu brûles, dans la guerre,
%# De voir couler des flots de sang humain!
\livretPers Arthénée
\livretRef#'CBDarthenee
%# Ah! d'une antique absurdité,
%# Laissons à l'*hindou les chimères.
%# Brame et Soudan doivent en frères
%# Soutenir leur autorité.
%# Tant qu'ils s'accordent bien ensemble,
%# Que l'esclave ainsi garrotté,
%# Souffre, obé=it, et croit, et tremble,
%# Le pouvoir est en sûreté.
\livretPers Atar
%# Dans ta politique nouvelle,  
%# Comment mes intérêts sont-ils unis aux tiens ?
\livretPers Arthénée
%# Ah! si ta couronne chancelle,
%# Mon temple, à moi, tombe avec elle.
%# Atar, ces farouches chrétiens
%# Auront des dieux jaloux des miens:
%# Ainsi qu'au trône, tout partage,
%# En fait de culte, est un outrage.
%# Pour les dompter, fais que nos Indi=ens
%# Pensent que le ciel même a conduit nos mesures:
%# Le nom du chef dont nous serons d'accord,
%# Je l'insinue =aux enfants des augures.
%#- Qui veux-tu nommer ?
\livretPers Atar
%#= Altamort.
\livretPers Arthénée
%#- Mon fils!
\livretPers Atar
%#= J'acquitte un grand service.
\livretPers Arthénée
%#- Que devient Tarare?
\livretPers Atar
%#= Il est mort.
\livretPers Arthénée
%#- Il est mort!
\livretPers Atar
%#= Oui, demain, j'ordonne qu'il périsse.
\livretPers Arthénée
%# Juste ciel! crains, Atar…
\livretPers Atar
%# Quoi craindre? mes remords?
\livretPers Arthénée
\livretRef#'CBEarthenee
%# Crains de pay=er de ta couronne,  
%# Un attentat sur sa personne.
%# Ses soldats seraient les plus forts.
%# Si, sur un prétexte frivole,
%# Tu les prives de leur idole,
%# Cette milice, en sa fureur,
%# Peut, oubli=ant ton rang et ta naissance…
\livretPers Atar
%# J'ai tout prévu; Tarare, dans l'erreur,
%# Court à sa perte en cherchant la vengeance.
\livretRef#'CBFatar
%# Qu'une grande solennité
%# Rassemble ce peuple agité;
%# De ses cris et de ses murmures
%# Montre-lui le ciel irrité.
%# Prépare ensuite les augures;
%# Et par d'utiles impostures
%# Consacrons notre autorité.
\livretDidasPPage (Il sort.)

\livretScene SCÈNE TROISIÈME
\livretPersDidas Arthénée seul
\livretRef#'CCAarthenee
%# Ô politique consommée!
%# Je tiens le secret de l'État;
%# Je fais mon fils chef de l'armée;
%# À mon temple je rend l'éclat,
%# Aux augures leur renommée.
%# Pontifes, pontifes adroits!
%# Remu=ez le cœur de vos rois.
%# Quand les rois craignent,
%# Les Brames règnent;
%# La ti=are agrandit ses droits.
%#12 Eh! qui sait si mon fils, un jour maître du monde…
\livretDidasPPage\wordwrap {
  (Il voit arriver Tarare; il rentre dans le temple.)
}

\livretScene SCÈNE QUATRIÈME
\livretPersDidas Tarare seul (il rêve).
\livretRef#'CDAtarare
%# De quel nouveau malheur suis-je encor menacé?
%# Ô Brama! tire-moi de cette nuit profonde.
%# Ce matin, quand j'ai prononcé:
%# Qu'à son amour Irza réponde,
%# Un signe effray=ant m'a glacé…
%# De quel nouveau malheur suis-je encor menacé?
%# Ô Brama! tire-moi de cette nuit profonde.

\livretScene SCÈNE CINQUIÈME
\livretDescAtt\wordwrap-center { Calpigi, Tarare. }
\livretPersDidas Calpigi \line { (déguisé, couvert d’une cape, l’ouvre) }
\livretRef#'CEArecit
%#- Tarare! connais-moi.
\livretPers Tarare
%#- Calpigi!
\livretPersDidas Calpigi (vivement)
%#= Mon héros!
%# Je te dois mon bonheur, ma fortune, ma vie.
%# Que ne puis-je à mon tour te rendre le repos!
\livretRef#'CEBcalpigi
%# Cette belle et tendre Astasie
%# Que tu vas chercher au hasard
%# Sur le vaste océ=an d'Asie,
%# Elle est dans le serrail d'Atar,
%#- Sous le faux nom d'Irza…
\livretPers Tarare
%#= Qui l'a ravie?
\livretPers Calpigi
%#- C'est Altamort.
\livretPers Tarare
%#= Ô lâche perfidie!
\livretPers Calpigi
%# Le golfe où nos plongeurs vont chercher le corail,
%# Baigne les jardins du serrail:
%# Si, dans la nuit, ton courage inflexible
%# Ose de cette route affronter le danger,
%# De soie =une échelle invisible,
%# Tendue =à l'angle du verger…
\livretPers Tarare
%#8 Ami généreux, secourable…
\livretPers Calpigi
%#- Le temple s'ouvre, adieu.
\livretDidasPPage\line { (Il s’enveloppe et s’enfuit.) }

\livretScene SCÈNE SIXIÈME
\livretPersDidas Tarare seul.
\livretRef#'CFAtarare
%#= J'irai:
%# Oui, j'oserai:
%# Pour la revoir je franchirai
%# Cette barrière impénétrable.
%# De ton repaire, affreux vautour!
%# J'irai l'arracher morte ou vive;
%# Et si je succombe au retour,
%# Ne me plains pas, tyran, quoiqu'il m'arrive:
%# Celui qui te sauva le jour
%# A bien mérité qu'on l'en prive!

\livretScene SCÈNE SEPTIÈME
\livretDescAtt\column {
  \justify {
    Le fond du théâtre qui représentait le portail du temple de Brama,
    se retire, et laisse voir l’intérieur du temple, qui se forme
    jusqu’au-devant du théâtre.
  }
  \wordwrap-center {
    Arthénée, les Prêtres de Brama, Elamir et les autres enfants des augures.
  }
}
\livretPersDidas Arthénée (aux Prêtres)
\livretRef#'CGArecit
%# Sur un choix important le ciel est consulté.
%# Vous, préparez l'autel; vous, les fleurs les plus pures;
%# Vous, choisissez parmi les enfants des augures,
%# Celui pour qui Brama s'est plus manifesté,
%# En le dou=ant d'un cœur plein de simplicité.
\livretPers Un Prêtre
%#- C'est le jeune Elamir. Il vient à vous.
\livretPersDidas Elamir (accourant)
%#= Mon père !
\livretPersDidas Arthénée (s’assied)
%# Approchez-vous, mon fils; un grand jour vous éclaire.
%# Croy=ez-vous que Brama vous parle par ma voix,
%#- Et qu'il parle à moi seul?
\livretPers Elamir
%#= Mon père, oui, je le crois.
\livretPersDidas Arthénée (sévèrement)
%# Le Ciel choisit par vous un vengeur à l'empire:
%# Ne dites rien, mon fils, que ce qu'il vous inspire.
\livretDidasPPage\line { (d’un ton caressant) }
%# Ah! s'il vous inspirait de nommer Altamort!
%# L'état serait vainqueur, il vous devrait son sort!
\livretPersDidas Elamir \line { (les mains croisées sur sa poitrine) }
%# Je l'en supplierai tant, mon père,
%# Qu'il me l'inspirera, j'espère.
\livretPers Arthénée
%# Moi je l'espère aussi: pri=ez-le avec transport.
\livretDidasPPage\line { (Elamir se prosterne.)	}
\livretRef#'CGBarthenee
%# Ainsi qu'une abeille,
%# Qu'un beau jour éveille,
%# De la fleur vermeille
%# Attire le miel;
%# Un enfant fidèle,
%# Quand Brama l'appelle,
%# S'il prie =avec zèle,
%# Obtient tout du ciel.
\livretRef#'CGCrecit
\livretDidasPPage\line { (Il relève l’enfant.) }
%# Tout le peuple, mon fils, sous nos voûtes arrive.
%# Avant de nommer son vengeur,
%# Vous le ferez rougir de sa vaine terreur.
%# Il croit les chrétiens sur la rive;
%# Assurez-le qu'ils sont bien loin;
%# Et du reste, mon fils, Brama prendra le soin.

\livretScene SCÈNE HUITIÈME
\livretDescAtt\wordwrap-center {
  Atar, Altamort, Tarare, Urson, Arthénée, Elamir, Prètres, Enfants,
  Visirs, Emirs, Suite, Peuple, Saldats, Esclaves.
}
\livretRef#'CHAmarche
\livretDidasPage Grande marche.
\livretPersDidas Arthénée (majestueusement)
\livretRef#'CHBarthenee
%# Prêtres du grand Brama! Roi du Golfe Persique!
%# Grands de l'empire! peuple inondant le portique!
%# La nati=on, l'armée =attend un général.
\livretPers Chœur universel
%# Pour nous préserver d'un grand mal,  
%# Que le choix de Brama s'explique!
\livretPers Arthénée
%# Vous promettez tous d'obé=ir  
%# Au chef que Brama va choisir ?
\livretPers Chœur universel
%# Nous le jurons sur cet autel antique.
\livretPersDidas Arthénée (d’un ton inspiré)  
%# Dieu sublime dans le repos,
%# Magnifique dans la tempête,
%# Soit que ton souffle élève aux cieux les flots,
%# Soit que ton regard les arrête;
%# Permets que le nom d'un héros,
%# Sortant d'une bouche innocente,
%# Devienne cher à ses rivaux;
%# Et porte à l'ennemi le trouble et l'épouvante!
\livretDidasPPage (à Elamir.)
%# Et vous, enfant, par le ciel inspiré!
%# Nommez, nommez sans crainte un héros préféré.
\livretDidasPPage\line { (On élève Elamir sur des pavois.) }
\livretPersDidas Elamir (avec enthusiasme)
%# Peuple que la terreur égare,
%# Qui vous fait redouter ces sauvages chrétiens?
%# L'État manque-t-il de soutiens?
%# Comptez, aux pieds du roi, vos défenseurs, Tarare…
\livretPers Chœur
%# Tarare! Tarare! Tarare!
%# Ah! pour nous Brama se déclare:
%# L'enfant vient de nommer Tarare.
%# Tarare! Tarare! Tarare!
\livretPersDidas Altamort (en colère)
%# Arrêtez ce fougueux transport!
\livretPers Arthénée
%# Peuple, c'est une erreur!
\livretDidasPPage (à Elamir)
%# Mon fils, que Dieu vous touche!
\livretPers Elamir
%# Le ciel m'inspirait Altamort;
%# Tarare est sorti de ma bouche.
\livretPers Deux Coryphées de soldats
%# Par l'enfant, Tarare indiqué,
%# N'est point un hasard sans mystère.
%# Plus son choix est involontaire,
%# Plus le vœu du ciel est marqué.
%# Oui, pour nous Brama se déclare;
%# L'enfant vient de nommer Tarare.
\livretPers Chœur du peuple et des soldats
%# Tarare! Tarare! Tarare!
\livretDidasPPage (On redescend Elamir.)	
\livretPersDidas Atar (se lève)
%# Tarare est retenu par un premier serment:
%# Son grand cœur s'est li=é d'avance
%# À suivre une juste vengeance.
\livretPersDidas Tarare (la main sur sa poitrine)
%# Seigneur, je remplirai le double engagement
%# De la vengeance et du commandement.
\livretDidasPPage (au peuple)
\livretRef#'CHCtarare
%# Qui veut la gloire,  
%# À la victoire
%# Vole avec moi.
\livretPers Tous
%# C'est moi, c'est moi.
\livretPers Tarare
%# Sujets, esclaves,
%# Que les plus braves
%# Donnent leur foi.
\livretPers Tous
%# C'est moi, c'est moi.
\livretPers Tarare
%# Ni paix, ni trêve,
%# L'*horreur du glaive
%# Fera la loi.
\livretPers Tous
%# C'est moi, c'est moi.
\livretPers Tarare
%# Qui veut la gloire,
%# À la victoire
%# Vole avec moi.
\livretPers Tous
%# C'est moi, c'est moi.
\livretPersDidas Atar (à part)
\livretRef#'CHDrecit
%# Je ne puis soutenir la clameur importune
%# D'un peuple entier sourd à ma voix.
\livretDidasPPage (Il veut descendre.)
\livretPersDidas Altamort (l’arrête)
%# Ce choix est une injure à tous tes chefs commune;
%# Il attaque nos premiers droits.
%# L'arrogant soldat de fortune
%# Doit-il aux grands dicter des lois ?
\livretPersDidas Tarare (fièrement)
\livretRef#'CHEtarare
%# Apprends, fils orgueilleux des prêtres!
%# Qu'élevé parmi les soldats,
%# Tarare avait, au lieu d'ancêtres,
%# Déjà vaincu dans cent combats;
\livretDidasPPage (avec un grand dédain.)
%# Qu'Altamort enfant, dans la plaine,
%# Poursuivait les fleurs des chardons,
%# Que les zéphyrs, de leur haleine,
%# Font voler au sommet des monts.
\livretPersDidas Altamort (la main au sabre)
\livretRef#'CHFrecit
%# Sans le respect d'Atar, vil objet de ma haine…
\livretPersDidas Tarare (bien dédaigneux)
%# Du destin de l'État tu prétends décider!
%# Fougueux adolescent, qui veux nous commander!
%# Pour titre ici n'as-tu que des injures?
%# Quels ennemis t'a-t-on vu terrasser?
%# Quels torrents osas-tu passer?
%# Où sont tes exploits, tes blessures?
\livretPersDidas Altamort (en fureur)
%# Toi, qui de ce haut rang brûles de t'approcher,
%# Apprends que sur mon corps il te faudra marcher.
\livretDidasPPage (Il tire son sabre.)
\livretPersDidas Arthénée (troublé)
%# Ô désespoir! ô frénésie!
%#- Mon fils !…
\livretPersDidas Altamort (plus furieux)
%#= A ce brigand j'arracherai la vie.
\livretPersDidas Tarare (froidement)
%# Calme ta fureur, Altamort.
%# Ce sombre feu, quand il s'allume,
%# Détruit les forces, nous consume:
%# Le guerrier, en colère, est mort.
\livretDidasPPage (Il tire son sabre)
\livretPersDidas Arthénée (s’écrie)
%# Le temple de nos dieux est-il donc une arène?
\livretPersDidas Atar (se lève)
%#- Arrêtez.
\livretPers Tarare
%#- J'obé=is…
\livretDidasPPage\line { (à Altamort, lui prenant la main.) }
%#= Toi, ce soir, à la plaine.
\livretDidasPPage\wordwrap {
  (à Calpigi, à part, pendant qu’Atar descend de son trône)
}
%# Et toi, fidèle ami, sans fanal et sans bruit,
%# Au verger du sérail attends-moi cette nuit.
\livretDidasPPage\wordwrap {
  Atar lui remet le bâton de commandement, au bruit d'une fanfare.	
}
\livretDidas Grande Marche pour sortir.
\livretPersDidas Chœur général (sur le chant de la marche)
\livretRef#'CHGchoeur
%# Brama! si la vertu t'es chère,
%# Si la voix du peuple est ta voix,
%# Par des succès soutiens le choix
%# Que le peuple entier vient de faire!
%# Que sur ses pas
%# Tous nos soldats
%# Marchent d'une audace plus fière!
%# Que l'ennemi, triste, abattu,
%# Par son aspect déjà vaincu,
%# Sous nos coups morde la poussière!
\sep
