\score {
  \new GrandStaff <<
    \new Staff <<
      $(if (*instrument-name*)
           (make-music 'ContextSpeccedMusic
              'context-type 'GrandStaff
              'element (make-music 'PropertySet
                         'value (markup #:large (*instrument-name*))
                         'symbol 'instrumentName))
           (make-music 'Music))
      \keepWithTag #'trompettes \global
      \keepWithTag #'tromba1 \includeNotes "trombe"
    >>
    \new Staff <<
      \keepWithTag #'trompettes \global
      \keepWithTag #'tromba2 \includeNotes "trombe"
    >>
  >>
  \layout {
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
