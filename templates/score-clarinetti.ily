\score {
  \new GrandStaff <<
    \new Staff <<
      $(if (*instrument-name*)
           (make-music 'ContextSpeccedMusic
             'context-type 'GrandStaff
             'element (make-music 'PropertySet
                        'value (make-large-markup (*instrument-name*))
                        'symbol 'instrumentName))
           (make-music 'Music))
      \global \keepWithTag #'clarinetto1 \includeNotes "clarinetti"
    >>
    \new Staff <<
      \global \keepWithTag #'clarinetto2 \includeNotes "clarinetti"
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
  }
}
