\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } <<
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #(*tag-notes*) \includeNotes #(*note-filename*)
      \clef #(*clef*)
      $(if (*instrument-name*)
                      (make-music 'ContextSpeccedMusic
                                  'context-type 'Staff
                                  'element (make-music 'PropertySet
                                                       'value (markup #:large (*instrument-name*))
                                                       'symbol 'instrumentName))
                      (make-music 'Music))
    >>
    \new Staff \with {
      \tinyStaff
      \haraKiriFirst
      \remove "Page_turn_engraver"
      instrumentName = "Violino I"
    } <<
      \new Voice \with {
        \remove "Dynamic_engraver"
      } <<
        \global
        \keepWithTag #'violino1 \includeNotes "violini"
        $(or (*score-extra-music*) (make-music 'Music))
      >>
    >>
  >>
  \layout {
    system-count = #(or (*system-count*) #f)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
    ragged-last = #(*score-ragged*)
  }
}
