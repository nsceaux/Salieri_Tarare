\score {
  \new GrandStaff <<
    \new Staff << \global \keepWithTag #'fagotto1 \includeNotes "fagotti" >>
    \new Staff << \global \keepWithTag #'fagotto2 \includeNotes "fagotti" >>
  >>
  \layout { }
}
