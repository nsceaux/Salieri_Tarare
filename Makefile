OUTPUT_DIR=out
DELIVERY_DIR=delivery
#DELIVERY_DIR=/Users/nicolas/Dropbox/TalensLyriques/Tarare
NENUVAR_LIB_PATH=$$(pwd)/../nenuvar-lib
LILYPOND_CMD=lilypond -I$$(pwd) -I$(NENUVAR_LIB_PATH) \
  --loglevel=WARN -ddelete-intermediate-files \
  -dno-protected-scheme-parsing
PROJECT=Salieri_Tarare

# Partition complète
conducteur:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT) main.ly
.PHONY: $(PROJECT)
# Partition réduite
reduction:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-reduction -dpart=reduction part.ly
.PHONY: $(PROJECT)
# Oboi
oboi:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-oboi -dpart=oboi part.ly
.PHONY: oboi
# Clarinetti
clarinetti:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-clarinetti -dpart=clarinetti part.ly
.PHONY: clarinetti
# Flauti
flauti:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-flauti -dpart=flauti part.ly
.PHONY: flauti
# Fagotti
fagotti:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-fagotti -dpart=fagotti part.ly
.PHONY: fagotti

# Violino I
violino1:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-violino1 -dpart=violino1 part.ly
.PHONY: violino1
# Violino II
violino2:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-violino2 -dpart=violino2 part.ly
.PHONY: violino2
# Viola
viola:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-viola -dpart=viola part.ly
.PHONY: viola
# Bassi
basso:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-basso -dpart=basso part.ly
.PHONY: basso

# Corni
corni:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-corni -dpart=corni part.ly
.PHONY: corni
# Trombe
trombe:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-trombe -dpart=trombe part.ly
.PHONY: trombe
# Tromboni
tromboni:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-tromboni -dpart=tromboni part.ly
.PHONY: tromboni
# Timpani
timpani:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-timpani -dpart=timpani part.ly
.PHONY: timpani

delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@if [ -e $(OUTPUT_DIR)/$(PROJECT).pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT).pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-reduction.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-reduction.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-oboi.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-oboi.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-clarinetti.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-clarinetti.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-flauti.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-flauti.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-fagotti.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-fagotti.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-violino1.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-violino1.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-violino2.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-violino2.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-viola.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-viola.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-basso.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-basso.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-corni.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-corni.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-trombe.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-trombe.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-tromboni.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-tromboni.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-timpani.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-timpani.pdf $(DELIVERY_DIR)/; fi
#	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-1.midi ]; then tar zcf $(DELIVERY_DIR)/$(PROJECT)-midi.tar.gz $(OUTPUT_DIR)/$(PROJECT).midi $(OUTPUT_DIR)/$(PROJECT)-[0-9]*.midi; fi
#	git archive --prefix=$(PROJECT)/ HEAD . | gzip > $(DELIVERY_DIR)/$(PROJECT).tar.gz
.PHONY: delivery

clean:
	@rm -f $(OUTPUT_DIR)/$(PROJECT)-* $(OUTPUT_DIR)/$(PROJECT).*
.PHONY: clean

parts: oboi clarinetti flauti fagotti \
	violino1 violino2 viola basso \
	corni trombe tromboni timpani #reduction
.PHONY: parts

all: check parts conducteur delivery clean
.PHONY: all

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in parent directory:"; \
	  echo " cd .. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
.PHONY: check
